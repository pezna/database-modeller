import type { Array, GenericData } from "./import-me";

export interface ErrorState {
	[yolo: number]: GenericData<'apple'> | Array;
	state: null;
}

export type Test = {
	yolo: string;
}