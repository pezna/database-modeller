export interface First<T> {
	name: string;
	age: number;
	type: T;
}

export interface Second {
	hello: string;
	first: First<string>;
	rec: Record<number, First<string>>;
	hi: Record<keyof First<any>, Omit<First<number>, 'age'>>;
	onion: First<{ hi: string }>;
}

export type Apple<T> = T;
