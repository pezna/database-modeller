export interface GenericData<T extends string> {
	message: T;
	hello: Array;
}

export enum Order {
	first,
	second,
	third,
}

export type Array = [number, number];