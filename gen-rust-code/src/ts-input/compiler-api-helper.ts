import ts, { forEachChild, TypeFlags, unescapeLeadingUnderscores } from 'typescript';
import type { ArrayAtLeastN, Result, ResultNg } from '../util';
import { getPathInfo, isNg, isOk, ng, ok, switchExpression } from '../util';
import {
	dangerouslyDeclarationToType,
	dangerouslyDeclareToEscapedText,
	dangerouslyExportSpecifierToEscapedName,
	dangerouslyNodeToSymbol,
	dangerouslySymbolToEscapedName,
	dangerouslyTypeToNode,
	dangerouslyTypeToReferenceType,
	dangerouslyTypeToResolvedTypeArguments,
	dangerouslyTypeToTypes
} from './extract';
import type * as to from './type-object';
import { isNamedTypeObject, primitive, PRIMITIVE_TYPE_TEXTS, PROMISE_SYMBOL_NAMES, special, SPECIAL_TYPE_TEXTS } from './type-object';

type TypeHasCallSignature = {
	getCallSignatures: () => readonly [ts.Signature, ...ts.Signature[]]
} & ts.Type

export interface ExtractOptions {
	skipUnresolved?: boolean;
	typeAliasDeclaration?: boolean;
	interfaceDeclaration?: boolean;
	enumDeclaration?: boolean;
	variableDeclaration?: boolean;
	variableStatement?: boolean;
	exportDeclaration?: boolean;
	classDeclaration?: boolean;
}

const defaultOptions: ExtractOptions = {
	typeAliasDeclaration: true,
	interfaceDeclaration: true,
	enumDeclaration: true,
	classDeclaration: true,
}

function isExported(node: any): boolean {
	// Check if the node has 'export' or 'default' modifiers
	const isExportedModifier = node.modifiers?.some(
		(modifier: any) =>
		modifier.kind === ts.SyntaxKind.ExportKeyword || 
		modifier.kind === ts.SyntaxKind.DefaultKeyword
	);

	if (isExportedModifier) {
		return true;
	}
	
	// 2. Check if the class is included in a named export block (e.g., `export { MyClass }`).
	const symbol = node.name && node.name.getText();
	const parent = node.parent;
	
	if (symbol && ts.isSourceFile(parent)) {
		return parent.statements.some(stmt =>
			ts.isExportDeclaration(stmt) && 
			stmt.exportClause && 
			ts.isNamedExports(stmt.exportClause) &&
			stmt.exportClause.elements.some(e => e.name.text === symbol)
		);
	}

	// Check if the node is part of an 'export {...}' declaration
	return Boolean(
		ts.isExportDeclaration(parent) || 
		(ts.isVariableStatement(parent) && parent.modifiers?.some(
			(modifier) => modifier.kind === ts.SyntaxKind.ExportKeyword
		))
	);
}

export class CompilerApiHelper {
	#program: ts.Program;
	#typeChecker: ts.TypeChecker;
	#containerTypeStore: Record<string, {
		type: ts.Type;
		typeObject: to.StoreKeyTypeObject;
		members?: Array<to.TypeProperty>;
	}>;
	#basePath: string;

	public constructor(program: ts.Program, basePath: string) {
		this.#basePath = basePath;
		this.#program = program;
		this.#typeChecker = this.#program.getTypeChecker();
		this.#containerTypeStore = {};
	}

	public updateProgram(program: ts.Program): void {
		this.#program = program;
		this.#typeChecker = this.#program.getTypeChecker();
	}

	public extractTypes(
		filePath: string,
		options: ExtractOptions = {},
	): Result<
		Record<string, to.TypeDeclaration>,
		| { reason: 'fileNotFound' }
		| {
			reason: 'exportError'
			meta:
				| 'fileNotFound'
				| 'resolvedModulesNotFound'
				| 'moduleNotFound'
				| 'moduleFileNotFound'
				| 'notNamedExport'
				| 'unknown'
		  }
	> {
		options = {
			...defaultOptions,
			...options,
		};
		const sourceFile = this.#program.getSourceFile(filePath);

		if (!sourceFile) {
			return ng({
				reason: 'fileNotFound',
			});
		}

		const relevantTypes: Record<string, to.TypeDeclaration> = {};
		
		this.#extractNodes(sourceFile)
			.filter(
				(node): node is
					| ts.TypeAliasDeclaration
					| ts.InterfaceDeclaration
					| ts.EnumDeclaration
					| ts.VariableDeclaration
					| ts.VariableStatement
					| ts.ExportDeclaration =>
						Boolean(
							(ts.isVariableStatement(node) && options.variableStatement) ||
							(ts.isVariableDeclaration(node) && options.variableDeclaration) ||
							(ts.isExportDeclaration(node) && options.exportDeclaration) ||
							(ts.isEnumDeclaration(node) && options.enumDeclaration) ||
							(ts.isClassDeclaration(node) && options.classDeclaration) ||
							(ts.isInterfaceDeclaration(node) && options.interfaceDeclaration) ||
							(ts.isTypeAliasDeclaration(node) && options.typeAliasDeclaration)
						)
			)
			.filter((node) => {
				return isExported(node) && (
					!options.skipUnresolved ||
					this.#isTypeParametersResolved(this.#typeChecker.getTypeAtLocation(node))
				)
			})
			.flatMap((decl): to.TypeDeclaration | Array<to.TypeDeclaration> | ResultNg<any> => {
				// export declaration
				if (ts.isExportDeclaration(decl)) {
					const nodes = this.extractTypesFromExportDeclaration(decl, options);
					if (isOk(nodes)) {
						return nodes.ok;
					} else {
						return ng({
							reason: 'exportError' as const,
							meta: nodes.ng.reason,
						});
					}
				}

				// variable declaration
				if (ts.isVariableStatement(decl)) {
					const declare = decl.declarationList.declarations[0]
					if (declare === undefined) {
						throw new TypeError(
							'In variable statement, declarations must have at least 1 item.',
						);
					}

					const type = this.#typeChecker.getTypeAtLocation(declare);
					
					return {
						...this.#declareToString(type, declare),
						type: this._convertType(type, declare, declare.type),
					};
				}

				// type declaration
				const type = this.#typeChecker.getTypeAtLocation(decl);
				let aliasTypeNode;
				if (ts.isTypeAliasDeclaration(decl)) {
					aliasTypeNode = decl.type;
				}
				
				return {
					...this.#typeToString(type, decl),
					type: this._convertType(type, decl, aliasTypeNode),
				};
			})
			.filter(
				(result): result is to.TypeDeclaration => {
					if ('__type' in result && isNg(result)) {
						return false;
					}

					return (
						'filePath' in result &&
						'typeName' in result &&
						'type' in result
					);
				},
			)
			.forEach((decl, index) => {
				relevantTypes[decl.typeName ?? `unnamed ${decl.type.__type} type [${index}]`] = decl;
			});
		
		return ok(relevantTypes);
	}

	// Only support named-export
	public extractTypesFromExportDeclaration(
		declare: ts.ExportDeclaration,
		options: ExtractOptions,
	): Result<
		to.TypeDeclaration[],
		{
			reason:
				| 'fileNotFound'
				| 'resolvedModulesNotFound'
				| 'moduleNotFound'
				| 'moduleFileNotFound'
				| 'notNamedExport'
				| 'unknown'
		}
	> {
		const path = declare.moduleSpecifier?.getText();
		if (!path) {
			return ng({
				reason: 'fileNotFound',
			});
		}

		const sourceFile = declare.getSourceFile();

		// for >= TS 5.0
		const symbol: ts.Symbol | undefined =
			// @ts-expect-error: type def wrong
			declare.exportClause?.elements?.at(0)?.symbol;
		if (symbol !== undefined) {
			const tsType = this.#typeChecker.getDeclaredTypeOfSymbol(symbol);
			const typeDeclaration: to.TypeDeclaration = {
				...this.#typeToString(tsType, declare),
				type: this._convertType(tsType, declare),
			};
			return ok([typeDeclaration]);
		}

		// for < TS 5.0
		const moduleMap =
			// @ts-expect-error: type def wrong
			sourceFile.resolvedModules as
				| ts.UnderscoreEscapedMap<ts.ResolvedModule>
				| undefined;

		if (!moduleMap) {
			return ng({
				reason: 'resolvedModulesNotFound',
			});
		}

		const module = moduleMap.get(
			ts.escapeLeadingUnderscores(path.replace(/'/g, '').replace(/"/g, '')),
		);

		if (!module) {
			return ng({
				reason: 'moduleNotFound',
			});
		}

		const types = this.extractTypes(module.resolvedFileName, options);
		if (isNg(types)) {
			return ng({ reason: 'moduleFileNotFound' });
		}

		const clause = declare.exportClause;
		if (!clause) {
			return ng({
				reason: 'unknown',
			});
		}

		if (ts.isNamedExports(clause)) {
			return ok(
				clause.elements
					.map(dangerouslyExportSpecifierToEscapedName)
					.flatMap((str) =>
						typeof str === 'undefined'
							? []
							: types.ok[ts.unescapeLeadingUnderscores(str)] ?? [],
					),
			);
		}

		return ng({
			reason: 'notNamedExport',
		});
	}
	
	#getEnumProps(enumObject: to.EnumTO, type: ts.Type): to.TypeProperty[] {
		const props: to.TypeProperty[] = [];
		
		type.symbol.exports?.forEach((symbol, key) => {
			const valueDeclare = symbol.valueDeclaration;
			if (valueDeclare) {
				const valType = this._convertType(
					this.#typeChecker.getTypeAtLocation(valueDeclare),
				);

				if (valType.__type === 'LiteralTO') {
					props.push({
						propName: unescapeLeadingUnderscores(key),
						type: valType,
					});
				}
			}
		});
		
		this.#containerTypeStore[enumObject.storeKey].members = props;

		return props;
	}
	
	#getObjectProps(object: to.ObjectTO, type: ts.Type): to.TypeProperty[] {
		const indexInfos = this.#convertIndexTypes(this.#typeChecker.getIndexInfosOfType(type))
			.map(type => ({
				propName: '[]',
				type,
			}));
		const props = this.#typeChecker.getPropertiesOfType(type).map(
			(symbol): {
				propName: string
				type: to.TypeObject
			} => {
				const mappedType: ts.MappedTypeNode | undefined =
					// @ts-expect-error: wrong type def
					symbol.links?.mappedType /* >= 5.0 */ ?? symbol.mappedType /* 4.9.5 */;

				if (mappedType) {
					// @ts-expect-error: wrong type def
					const templateType: ts.Type | undefined = mappedType.templateType;
					if (templateType !== undefined) {
						const valueType = this._convertType(templateType);
						return {
							propName: dangerouslySymbolToEscapedName(symbol) ?? 'unknown',
							type: valueType,
						};
					}
				}

				const typeNode = symbol.valueDeclaration
					? dangerouslyDeclarationToType(symbol.valueDeclaration)
					: undefined;

				const propName = dangerouslySymbolToEscapedName(symbol) ?? 'UNEXPECTED_UNDEFINED_SYMBOL';
				if (typeNode && ts.isArrayTypeNode(typeNode)) {
					const type = this.#typeChecker.getTypeFromTypeNode(typeNode);
					return {
						propName,
						type: {
							__type: 'ArrayTO',
							...this.#typeToString(type),
							typeArguments: [this.#extractArrayTFromTypeNode(typeNode)],
						},
					};
				}
				
				const declare = (symbol.declarations ?? [])[0];
				const type = declare
					? this.#typeChecker.getTypeOfSymbolAtLocation(symbol, declare)
					: undefined;

				return {
					propName,
					type: (
						type ? (
							this.#isCallable(type)
							? this._convertTypeFromCallableSignature(type.getCallSignatures()[0])
							: this._convertType(type)
						)
						: {
							__type: 'UnsupportedTO',
							kind: 'prop',
						}
					)
				};
			},
		);
		
		props.push(...indexInfos);
		
		this.#containerTypeStore[object.storeKey].members = props;

		return props;
	}

	public getStoredType(key: string): to.StoreKeyTypeObject {
		return this.#containerTypeStore[key].typeObject;
	}

	public getStoredTypeMembers(object: to.StoreKeyTypeObject): to.TypeProperty[] {
		const store = this.#containerTypeStore[object.storeKey];
		if (!store) {
			throw new Error(`No container in the store ${object.storeKey}`);
		}
		
		const {type, members} = store;
		if (!type) {
			return [
				{
					propName: 'unknown',
					type: {
						__type: 'UnsupportedTO',
						kind: 'prop',
					},
				},
			];
		}
		
		if (members) {
			return members;
		}
		
		const props = object.__type === 'EnumTO'
			? this.#getEnumProps(object, type)
			: this.#getObjectProps(object, type);
		
		this.#containerTypeStore[object.storeKey].members = props;
		
		return props;
	}
	
	#convertIndexTypes(indexInfos: readonly ts.IndexInfo[]): to.IndexTO[] {
		return indexInfos.map(ii => {
			const keyType = this._convertType(ii.keyType);
			const valueType = this._convertType(ii.type);
			
			return {
				__type: 'IndexTO',
				keyType,
				valueType,
			};
		});
	}
	
	public _convertType(type: ts.Type, decl?: ts.Declaration, typeNode?: ts.TypeNode): to.TypeObject {
		const info = this.#typeToString(type, decl);
		
		return switchExpression({
			type,
			typeNode,
			...info,
		})
			.case<to.EnumTO>(
				({ type }) =>
					type.isUnion() &&
					type.types.length > 0 &&
					typeof type.symbol !== 'undefined', // only enum declare have symbol
				({ type }) => this.#storeContainerType(type, 'EnumTO'),
			)
			.case<to.UnionTO>(
				({ type, typeText }) => {
					// typescript sends boolean as a union type of literals true | false
					// this will also skip booleans so that the appropriate PrimitiveTO can find it
					return type.isUnion() && type.types.length > 0 && typeText !== 'boolean';
				},
				({ typeText, typeName, type, filePath }) => {
					// A type like State | boolean will still return 'true' and 'false' as separate types,
					// so we need to check for both true and false and create a primitive boolean
					const booleanIndices: Set<number> = new Set([]);
					let types = dangerouslyTypeToTypes(type).map((type) =>
						this._convertType(type),
					) as ArrayAtLeastN<to.TypeObject, 2>;
					
					types.forEach((obj, i) => {
						if (obj.__type !== 'LiteralTO') {
							return;
						}
						
						if (obj.value === true || obj.value === false) {
							booleanIndices.add(i);
						}
					});
					// only remove if there are 2 values, which will mean true and false are present
					if (booleanIndices.size === 2) {
						types = types.filter((_, i) => !booleanIndices.has(i)) as any;
						types.push(primitive('boolean'));
					}
					
					const typeArguments = dangerouslyTypeToResolvedTypeArguments(type)?.map(t => this._convertType(t));
					
					return {
						__type: 'UnionTO',
						typeName,
						typeText,
						filePath,
						types,
						typeArguments,
					};
				},
			)
			.case<to.TupleTO>(
				({ type }) => this.#typeChecker.isTupleType(type),
				({ typeText, typeName, type, filePath }) => {
					const node = dangerouslyTypeToNode(type) as ts.TupleTypeNode;
					let items = node?.elements?.map((typeNode) =>
						this._convertType(this.#typeChecker.getTypeFromTypeNode(typeNode)),
					);
					
					if (!node) {
						items = dangerouslyTypeToResolvedTypeArguments(type)?.map(t => this._convertType(t)) ?? [];
					}
					const target: ts.TupleType = dangerouslyTypeToReferenceType(type) ?? type as any;
					const hasRestItem = Boolean(target.combinedFlags & ts.ElementFlags.Variable);
					
					return {
						__type: 'TupleTO',
						typeName,
						typeText,
						filePath,
						items,
						hasRestItem,
					}
				},
			)
			.case<to.LiteralTO>(
				({ type }) => type.isLiteral(),
				({ type }) => ({
					__type: 'LiteralTO',
					value: type.isLiteral() ? type.value : undefined,
				}),
			)
			.case<to.LiteralTO>(
				({ type }) => (type.flags & ts.TypeFlags.TemplateLiteral) > 0,
				({ typeText }) => ({
					__type: 'LiteralTO',
					value: typeText,
				}),
			)
			.case<to.LiteralTO>(
				({ typeText }) => typeText === 'true' || typeText === 'false',
				({ typeText }) => ({
					__type: 'LiteralTO',
					value: typeText === 'true' ? true : false,
				}),
			)
			.case<to.PrimitiveTO>(
				({ typeText }) => PRIMITIVE_TYPE_TEXTS.has(typeText as any),
				({ typeText }) => primitive(typeText as any),
			)
			.case<to.SpecialTO>(
				({ typeText }) => SPECIAL_TYPE_TEXTS.has(typeText as any),
				({ typeText }) => special(typeText as any),
			)
			.case<to.SpecialTO>(
				({ typeText }) => typeText === 'unique symbol' || typeText === 'Symbol' || typeText === 'symbol',
				() => special('symbol'),
			)
			.case<to.ArrayTO>(
				({ type }) => (
					this.#typeChecker.isArrayType(type)
				),
				({ type, typeText, typeName, typeNode, filePath }) => ({
					__type: 'ArrayTO',
					typeName,
					typeText,
					filePath,
					typeArguments: ((): to.TypeObject[] => {
						const resultT = this.#extractArrayT(type, typeNode)
						return isOk(resultT)
							? [resultT.ok]
							: [({ __type: 'UnsupportedTO', kind: 'arrayT' } as const)];
					})(),
				}),
			)
			.case<to.CallableTO, { type: TypeHasCallSignature }>(
				({ type }) => this.#isCallable(type),
				({ type }) =>
					this._convertTypeFromCallableSignature(type.getCallSignatures()[0]),
			)
			.case<to.PromiseTO>(
				({ type }) => PROMISE_SYMBOL_NAMES.has(dangerouslySymbolToEscapedName(type.symbol)!),
				({ type }) => {
					const typeArgResult = this.#extractTypeArguments(type);
					const typeArg: to.TypeObject = isOk(typeArgResult)
						? typeArgResult.ok[0]
						: {
							__type: 'UnsupportedTO',
							kind: 'promiseNoArgument',
						};

					return {
						__type: 'PromiseTO',
						child: typeArg,
					};
				},
			)
			.case<to.ObjectTO>(
				({ type }) => (
					(type.flags & TypeFlags.Object) === TypeFlags.Object ||
					('objectFlags' in type && type.getProperties().length > 0)
				),
				({ type }) => this.#storeContainerType(type, 'ObjectTO'),
			)
			.case<to.UnsupportedTO>(
				({ type }) => type.isTypeParameter(),
				({ typeText }) => ({
					__type: 'UnsupportedTO',
					kind: 'unresolvedTypeParameter',
					typeText,
				}),
			)
			.default<to.UnsupportedTO>(({ typeText }) => {
				return {
					__type: 'UnsupportedTO',
					kind: 'convert',
					typeText,
				};
			});
	}

	public _convertTypeFromCallableSignature(
		signature: ts.Signature,
	): to.CallableTO {
		return {
			__type: 'CallableTO',
			argTypes: signature
				.getParameters()
				.map((argSymbol): to.CallableArgument | undefined => {
					const declare = (argSymbol.getDeclarations() ?? [])[0];

					return typeof declare !== 'undefined'
						? {
							name: argSymbol.getName(),
							type: this._convertType(
								this.#typeChecker.getTypeOfSymbolAtLocation(
									argSymbol,
									declare,
								),
							),
						}
						: undefined;
				})
				.filter((arg): arg is to.CallableArgument => arg !== undefined),
			returnType: this._convertType(
				this.#typeChecker.getReturnTypeOfSignature(signature),
			),
		};
	}

	#extractNodes(sourceFile: ts.SourceFile): ts.Node[] {
		const nodes: ts.Node[] = [];
		forEachChild(sourceFile, (node) => {
			nodes.push(node);
		});

		return nodes;
	}

	#storeContainerType<T extends 'ObjectTO' | 'EnumTO', U extends Extract<to.TypeObject, {__type: T}>>(type: ts.Type, objectType: T): U {
		const {typeText, typeName, filePath} = this.#typeToString(type);
		const storeKey = `${typeText}::${filePath}`;
		
		if (storeKey in this.#containerTypeStore) {
			return this.#containerTypeStore[storeKey].typeObject as any;
		}
		
		const typeArgResult = this.#extractTypeArguments(type);
		const typeObject: to.ObjectTO | to.EnumTO = {
			__type: objectType,
			typeName,
			typeText,
			filePath,
			storeKey,
			baseStoreKey: '',
			typeArguments: isOk(typeArgResult) ? typeArgResult.ok : undefined,
		};
		
		this.#containerTypeStore[storeKey] = {type, typeObject};
		
		let baseStoreKey = storeKey; // this will include the type parameters because the typeText has them
		
		if (!this.#hasUnresolvedTypeParameter(typeObject)) {
			if ('target' in type) {
				const target = type.target as ts.Type;
				if ((type as any).id !== (target as any).id) {
					// const obj = this.#storeContainerType(target, objectType);
					const {typeText, filePath} = this.#typeToString(target);
					const pathInfo = getPathInfo(filePath ?? '');
					if (!pathInfo.isInNodeModules) {
						baseStoreKey = `${typeText}::${filePath}`;
						this._convertType(target);
					}
				}
			}
		}
		
		typeObject.baseStoreKey = baseStoreKey;
		
		return typeObject as any;
	}

	#extractArrayTFromTypeNode(typeNode: ts.ArrayTypeNode): to.TypeObject {
		return this._convertType(
			this.#typeChecker.getTypeAtLocation(typeNode.elementType),
			undefined,
			typeNode.elementType,
		);
	}

	#extractArrayT(type: ts.Type, typeNode?: ts.TypeNode): Result<
		to.TypeObject,
		{ reason: 'node_not_defined' | 'not_array_type_node' | 'cannot_resolve' }
	> {
		const maybeArrayT = dangerouslyTypeToResolvedTypeArguments(type)?.[0];
		if (
			dangerouslyNodeToSymbol(type)?.getEscapedName() === 'Array' &&
			typeof maybeArrayT !== 'undefined'
		) {
			return ok(this._convertType(maybeArrayT));
		}

		const maybeNode = typeNode ?? dangerouslyTypeToNode(type) ?? this.#typeChecker.typeToTypeNode(type, undefined, undefined);
		if (!maybeNode) {
			return ng({
				reason: 'node_not_defined',
			});
		}

		if (ts.isTypeReferenceNode(maybeNode)) {
			const [typeArg1] = this.#extractTypeArgumentsFromTypeRefNode(maybeNode);

			return typeof typeArg1 !== 'undefined'
				? ok(typeArg1)
				: ng({
					reason: 'cannot_resolve',
				});
		}

		if (!ts.isArrayTypeNode(maybeNode)) {
			return ng({
				reason: 'not_array_type_node',
			});
		}

		return ok(this.#extractArrayTFromTypeNode(maybeNode));
	}

	#extractTypeArguments(type: ts.Type): Result<
		[to.TypeObject, ...to.TypeObject[]],
		{ reason: 'node_not_found' | 'not_type_ref_node' | 'no_type_argument' }
	> {
		const resolvedTypeArguments = dangerouslyTypeToResolvedTypeArguments(type);
		if (resolvedTypeArguments?.length) {
			const typeArgs = resolvedTypeArguments.map((tsType) =>
				this._convertType(tsType),
			);
			if (typeArgs.length >= 1) {
				return ok(typeArgs as ArrayAtLeastN<to.TypeObject, 1>);
			}
		}

		const maybeDeclare = (type.aliasSymbol?.declarations ?? [])[0];
		const maybeTypeRefNode = maybeDeclare
			? dangerouslyDeclarationToType(maybeDeclare)
			: undefined;

		if (!maybeTypeRefNode) {
			return ng({
				reason: 'node_not_found',
			});
		}

		if (!ts.isTypeReferenceNode(maybeTypeRefNode)) {
			return ng({
				reason: 'not_type_ref_node',
			});
		}

		const args = this.#extractTypeArgumentsFromTypeRefNode(maybeTypeRefNode);

		return args.length > 0
			? ok(args as [to.TypeObject, ...to.TypeObject[]])
			: ng({
				reason: 'no_type_argument',
			});
	}

	#extractTypeArgumentsFromTypeRefNode(
		node: ts.TypeReferenceNode,
	): to.TypeObject[] {
		return Array.from(node.typeArguments ?? []).map((arg) =>
			this._convertType(this.#typeChecker.getTypeFromTypeNode(arg)),
		);
	}

	#hasUnresolvedTypeParameter(type: to.TypeObject): boolean {
		if (!isNamedTypeObject(type)) {
			return (
				type.__type === 'UnsupportedTO' &&
				type.kind === 'unresolvedTypeParameter'
			);
		}
		
		const deps: to.TypeObject[] = type.typeArguments ?? [];
		if (type.__type === 'UnionTO') {
			deps.push(...type.types);
		}

		return deps.reduce(
			(s: boolean, t: to.TypeObject) =>
				s ||
				(t.__type === 'UnsupportedTO' &&
					t.kind === 'unresolvedTypeParameter') ||
				(isNamedTypeObject(t) &&
					t.typeName !== type.typeName &&
					this.#hasUnresolvedTypeParameter(t)),
			false,
		);
	}

	#isCallable(type: ts.Type): type is TypeHasCallSignature {
		return type.getCallSignatures().length > 0;
	}

	#isTypeParametersResolved(type: ts.Type): boolean {
		return (
			(type.aliasTypeArguments ?? []).length === 0 ||
			// @ts-expect-error: wrong type def
			type.typeParameter !== undefined
		);
	}
	
	#getFilePathForType(type: ts.Type, decl?: ts.Declaration): string | undefined {
		if (!decl) {
			const symbol = (
				type.aliasSymbol ??
				type.symbol ??
				dangerouslyNodeToSymbol(
					this.#typeChecker.typeToTypeNode(type, undefined, undefined)!
				)
			);
			
			decl = symbol?.declarations?.[0];
		}
		
		if (decl) {
			return decl.getSourceFile().fileName.slice(this.#basePath.length);
		}
		
		return undefined;
	}
	
	#typeToString(type: ts.Type, decl?: ts.Declaration): TypeTextInfo {
		const symbol = dangerouslyNodeToSymbol(decl ?? type);
		let typeName = dangerouslySymbolToEscapedName(symbol);
		if (!typeName && 'intrinsicName' in type) {
			typeName = type.intrinsicName as string;
		}
		const typeText = this.#typeChecker.typeToString(type).replace('typeof ', '');
		const filePath = this.#getFilePathForType(type, decl);
		
		return {
			typeName,
			typeText,
			filePath,
		};
	}
	
	#declareToString(type: ts.Type, declare: ts.VariableDeclaration): TypeTextInfo {
		const typeName = dangerouslyDeclareToEscapedText(declare);
		const typeText = this.#typeChecker.typeToString(type).replace('typeof ', '');
		const filePath = this.#getFilePathForType(type, declare);
		
		return {
			typeName,
			typeText,
			filePath,
		};
	}
}

type TypeTextInfo = {
	typeName?: string;
	typeText: string;
	filePath?: string;
}