import type { Declaration, ExportSpecifier, Node, ObjectType, Symbol, Type, TypeReference, VariableDeclaration, __String } from 'typescript';
import { ObjectFlags, TypeFlags, unescapeLeadingUnderscores } from 'typescript';

export class ExtractError extends Error {
	public constructor(public fn: string) {
		super();
	}
}

export const dangerouslySymbolToEscapedName = (
	symbol: Symbol | undefined,
): string | undefined => {
	if (symbol !== undefined) {
		if ('escapedName' in symbol) {
			return unescapeLeadingUnderscores(symbol.escapedName);
		}
		
		return (symbol as any).name;
	}

	return undefined;
}

export const dangerouslyDeclareToEscapedText = (
	declare: VariableDeclaration,
): string => {
	if (
		'escapedText' in declare.name &&
		typeof declare.name['escapedText'] === 'string'
	) {
		return declare.name['escapedText'];
	}

	throw new ExtractError('dangerouslyDeclareToEscapedText');
}

// eslint-disable-next-line @typescript-eslint/ban-types
export const dangerouslyNodeToSymbol = (node: Node | Type): Symbol | undefined => {
	if (!node) {
		return undefined;
	}
	
	if ('aliasSymbol' in node && node.aliasSymbol) {
		return node.aliasSymbol as Symbol;
	}
	
	if ('symbol' in node && node.symbol) {
		return node.symbol as Symbol;
	}
	
	if ('localSymbol' in node && node.localSymbol) {
		return node.localSymbol as Symbol;
	}

	return undefined;
}

export const dangerouslyExportSpecifierToEscapedName = (
	element: ExportSpecifier,
): __String | undefined => {
	// @ts-expect-error -- Because the type definition side is in error.
	if ('symbol' in element && 'getEscapedName' in element['symbol']) {
		// @ts-expect-error Because the type definition side is in error.
		return element.symbol.getEscapedName() as __String;
	}

	return undefined;
}

export const dangerouslyDeclarationToType = (
	declare: Declaration,
): Node | undefined => {
	if ('type' in declare) {
		return declare.type as Node;
	}

	return undefined;
}

export const dangerouslyTypeToNode = (type: Type): Node | undefined => {
	if ('node' in type) {
		return type.node as Node;
	}
	
	return undefined;
}

export const dangerouslyTypeToTypes = (type: Type): Type[] => {
	if ('origin' in type && type.origin) {
		const origin = type.origin as Type;
		if ('types' in origin && Array.isArray(origin.types)) {
			return origin.types as Type[];
		}
	}
	
	return 'types' in type ? (type.types as Type[]) : [];
}

export const dangerouslyTypeToResolvedTypeArguments = (type: Type): Type[] | undefined => {
	if ('resolvedTypeArguments' in type && type.resolvedTypeArguments) {
		return type.resolvedTypeArguments as Type[];
	}
	
	if ('aliasTypeArguments' in type && type.aliasTypeArguments) {
		return type.aliasTypeArguments as Type[];
	}
	
	if ('typeArguments' in type && type.typeArguments) {
		return type.typeArguments as Type[];
	}
	
	return undefined;
}

export const dangerouslyTypeToReferenceType = (type: Type): TypeReference | undefined => {
	if (!isTypeReference(type)) {
		return undefined;
	}
	
	return type.target;
}

function isTypeReference(type: Type): type is TypeReference {
	return (
		(type.flags & TypeFlags.Object) !== 0 &&
		((type as ObjectType).objectFlags & ObjectFlags.Reference) !== 0
	);
}