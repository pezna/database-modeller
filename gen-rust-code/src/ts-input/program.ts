import { resolve } from 'path';
import type { CompilerOptions, Program } from 'typescript';
import {
	createProgram as baseCreateProgram,
	createCompilerHost,
	createSourceFile,
	ModuleDetectionKind,
	ModuleKind,
	ModuleResolutionKind,
	parseJsonConfigFileContent,
	readConfigFile,
	ScriptTarget,
	sys,
} from 'typescript';
import { codeFileAbsolutePath } from '../util';

type ProgramReturn = {
	basePath: string;
	program: Program;
}

export const createTypescriptProgram = (tsConfigPath?: string): ProgramReturn => {
	tsConfigPath ??= codeFileAbsolutePath('./tsconfig.json');
	
	const configFile = readConfigFile(tsConfigPath, sys.readFile);
	if (typeof configFile.error !== 'undefined') {
		throw new Error(`Failed to load tsconfig: ${configFile.error}`)
	}

	const basePath = resolve(tsConfigPath, '..');
	const { options, fileNames } = parseJsonConfigFileContent(
		configFile.config,
		{
			fileExists: sys.fileExists,
			readFile: sys.readFile,
			readDirectory: sys.readDirectory,
			useCaseSensitiveFileNames: true,
		},
		basePath,
	);

	return {
		basePath,
		program: baseCreateProgram({
			rootNames: fileNames,
			options,
		}),
	};
}

const SCRIPT_FILE_NAME = 'script.ts';

export const createTypescriptProgramForCode = (code: string): ProgramReturn => {
	const options: CompilerOptions = {
		lib: ["ESNext", "DOM"],
		target: ScriptTarget.ESNext,
		module: ModuleKind.ESNext,
		strict: true,
		// noEmit: true,
		moduleDetection: ModuleDetectionKind.Force,
		moduleResolution: ModuleResolutionKind.NodeNext,
		allowJs: true,
	};
	const compilerHost = createCompilerHost(options);
	compilerHost.getSourceFile = (fileName, languageVersion) => {
		if (fileName === SCRIPT_FILE_NAME) {
			return createSourceFile(fileName, code, languageVersion, true);
		}
		
		const contents = sys.readFile(fileName);
		if (contents) {
			return createSourceFile(fileName, contents, languageVersion, true);
		}
		
		return undefined;
	};
	
	// Create the program
	const program = baseCreateProgram([SCRIPT_FILE_NAME], options, compilerHost);
	
	return {
		basePath: '',
		program,
	};
}