import type { ArrayAtLeastN } from '../util';

export type TypeObject =
	| PrimitiveTO
	| LiteralTO
	| SpecialTO
	| ArrayTO
	| TupleTO
	| UnionTO
	| EnumTO
	| CallableTO
	| PromiseTO
	| UnsupportedTO
	| ObjectTO
	| IndexTO

type WithTypeName = {
	typeName?: string;
	typeText: string;
	filePath?: string;
	typeArguments?: TypeObject[];
}

export type NamedTypeObject = Extract<TypeObject, WithTypeName>;

export type PrimitiveTO = {
	__type: 'PrimitiveTO';
	kind: 'string' | 'number' | 'bigint' | 'boolean';
}

export const PRIMITIVE_TYPE_TEXTS = new Set<PrimitiveTO['kind']>([
	'string',
	'number',
	'bigint',
	'boolean',
]);

export type SpecialTO = {
	__type: 'SpecialTO';
	kind:
		| 'null'
		| 'undefined'
		| 'any'
		| 'unknown'
		| 'never'
		| 'void'
		| 'Date'
		| 'symbol'
		;
}

export const SPECIAL_TYPE_TEXTS = new Set<SpecialTO['kind']>([
	'null',
	'undefined',
	'any',
	'unknown',
	'never',
	'void',
	'Date',
	'symbol',
]);

export type LiteralTO = {
	__type: 'LiteralTO';
	value: unknown;
}

export type ArrayTO = WithTypeName & {
	__type: 'ArrayTO';
}

export type TupleTO = WithTypeName & {
	__type: 'TupleTO';
	hasRestItem: boolean;
	items: TypeObject[];
}

type WithStoreKey = {
	storeKey: string;
	baseStoreKey: string;
}

export type StoreKeyTypeObject = Extract<TypeObject, WithStoreKey>

export type ObjectTO = WithTypeName & WithStoreKey & {
	__type: 'ObjectTO';
}

export type IndexTO = {
	__type: 'IndexTO';
	keyType: TypeObject;
	valueType: TypeObject;
}

export type UnionTO = WithTypeName & {
	__type: 'UnionTO';
	types: ArrayAtLeastN<TypeObject, 2>;
}

export type EnumTO = WithTypeName & WithStoreKey & {
	__type: 'EnumTO';
}

export type CallableArgument = {
	name: string;
	type: TypeObject;
}

export type CallableTO = {
	__type: 'CallableTO';
	argTypes: {
		name: string;
		type: TypeObject;
		// should support optional arguments?
	}[]
	returnType: TypeObject;
}

export type PromiseTO = {
	__type: 'PromiseTO';
	child: TypeObject;
}

export const PROMISE_SYMBOL_NAMES = new Set(['Promise', 'PromiseLike']);

/**
 * @property kind -- identifier of why converted as unsupported
 */
export type UnsupportedTO = {
	__type: 'UnsupportedTO';
	kind:
		| 'arrayT'
		| 'prop'
		| 'convert'
		| 'function'
		| 'unresolvedTypeParameter'
		| 'promiseNoArgument'
		| 'enumValNotFound'
		;
	typeText?: string;
}

export type TypeProperty = { propName: string; type: TypeObject }

export type TypeDeclaration = WithTypeName & { type: TypeObject }

export function primitive(kind: PrimitiveTO['kind']): PrimitiveTO {
	return {
		__type: 'PrimitiveTO',
		kind,
	};
}

export function special(kind: SpecialTO['kind']): SpecialTO {
	return {
		__type: 'SpecialTO',
		kind,
	};
}

export function isNamedTypeObject(type: TypeObject): type is NamedTypeObject {
	return 'typeName' in type;
}

export function isStoreKeyType(type: TypeObject): type is StoreKeyTypeObject {
	return 'storeKey' in type;
}

export function getTypeChildren(type: TypeObject): TypeObject[] | undefined {
	switch (type.__type) {
		case 'ObjectTO':
		case 'EnumTO':
		case 'ArrayTO':
			return type.typeArguments;
		case 'TupleTO':
			return [...type.items, ...(type.typeArguments ?? [])];
		case 'UnionTO':
			return [...type.types, ...(type.typeArguments ?? [])];
		case 'CallableTO':
			// return [...type.argTypes.map(x => x.type), type.returnType];
		case 'PromiseTO':
			// return [type.child];
			return undefined;
		case 'IndexTO':
			return [type.keyType, type.valueType];
		case 'UnsupportedTO':
		case 'PrimitiveTO':
		case 'LiteralTO':
		case 'SpecialTO':
			return undefined;
	}
}