import { pascalCase, snakeCase } from "change-case";
import { find, isEqual, uniqWith } from "lodash";
import { MultiMap, MultiSet } from "mnemonist";
import { CompilerApiHelper } from "../ts-input/compiler-api-helper";
import { getTypeChildren, isNamedTypeObject, isStoreKeyType, type EnumTO, type LiteralTO, type NamedTypeObject, type ObjectTO, type TupleTO, type TypeDeclaration, type TypeObject, type TypeProperty } from "../ts-input/type-object";
import { getPathInfo, type TypePathInfo } from "../util";
import { CASE_OPTIONS, generateIdentifierForType, hashString } from "./util";

export type RustObject = RustStruct | RustNewtype | RustEnum | RustAlias

type ObjectInfo = {
	name: string;
	filePath: string;
}

export type RustStruct = ObjectInfo & {
	type: 'struct';
	props: RustStructProp[];
}

export interface RustStructProp {
	name: string;
	typeName: string;
	isTypeParam: boolean;
	literal?: string;
	isExtraMap?: boolean;
}

export type RustNewtype = ObjectInfo & {
	type: 'newtype';
	innerTypeName: string;
}

export type RustAlias = ObjectInfo & {
	type: 'alias';
	aliasTypeName: string;
}

export type RustEnum = ObjectInfo & {
	type: 'enum';
	tag?: string;
	isNumeric?: boolean;
	props: RustEnumProp[];
}

export interface RustEnumProp {
	name: string;
	typeName: string;
	serdeRename?: string;
	isTypeParam: boolean;
	ignoreTypeName?: boolean;
}

class FileInfo {
	path: string;
	namedTypes: Set<string> = new Set();
	rustObjects: Record<string, RustObject> = {};
	importedTypes: Record<string, Set<string>> = {};
	declaredNames: Set<string> = new Set();
	
	constructor(path: string) {
		this.path = path;
	}
	
	addDeclaredName(name: string) {
		this.declaredNames.add(name);
	}
	
	hasNamedType(name: string): boolean {
		return this.namedTypes.has(name);
	}
	
	addRustObject(struct: RustObject) {
		this.rustObjects[struct.name] = struct;
		this.namedTypes.add(struct.name);
	}
	
	// prevent nested recursive types from causing stack overflow
	holdName(name: string) {
		this.namedTypes.add(name);
	}
	
	addImportedType(name: string, path: string) {
		const paths = this.importedTypes[name] ?? new Set();
		paths.add(path);
		this.importedTypes[name] = paths;
	}
	
	getImportedNamedType(name: string): Set<string> | undefined {
		return this.importedTypes[name];
	}
	
	getNamedType(name: string): RustObject | undefined {
		return this.rustObjects[name];
	}
}

export class RustTypeContext {
	baseFolder: string;
	tsCompiler: CompilerApiHelper;
	filesInfo: Record<string, FileInfo> = {};
	typeStoreToTypeName: Record<string, ResolvedTypeName> = {};
	
	constructor(baseFolder: string, tsCompiler: CompilerApiHelper) {
		this.baseFolder = baseFolder;
		this.tsCompiler = tsCompiler;
	}
	
	#ensureFilePathInfo(filePath: string): FileInfo {
		let fileInfo = this.filesInfo[filePath];
		if (!fileInfo) {
			this.filesInfo[filePath] = fileInfo = new FileInfo(filePath);
		}
		
		return fileInfo;
	}
	
	getAllFiles() {
		return this.filesInfo;
	}
	
	public addDeclarations(decls: TypeDeclaration[]) {
		decls.forEach(d => this.addDeclaration(d));
	}
	
	public addDeclaration(decl: TypeDeclaration) {
		const filePath = decl.filePath ?? (isNamedTypeObject(decl.type) ? decl.type.filePath : undefined);
		let typeName = decl.typeName;
		
		if (!filePath) {
			console.log('No file path for ' + typeName);
			return;
		}
		
		if (!typeName) {
			console.log('No type name for decl in path ' + filePath);
			return;
		}
		
		const type = decl.type;
		if (isNamedTypeObject(type)) {
			if (
				typeName === type.typeName &&
				typeName !== type.typeText &&
				type.typeText.startsWith(typeName)
			) {
				// this would only happen if the typetext includes a generic
				// switch the struct name to the generic
				typeName = type.typeText;
			}
		}
		
		const fileInfo = this.#ensureFilePathInfo(filePath);
		fileInfo.addDeclaredName(typeName);
		this.addNamedType(typeName, decl.type, fileInfo, true);
	}
	
	#getFileInfoForType(type: TypeObject, fallbackFileInfo: FileInfo): FileInfo {
		if (isNamedTypeObject(type) && type.filePath) {
			const {isInNodeModules} = getTypePathInfo(type);
			if (isInNodeModules) {
				return fallbackFileInfo;
			}
			
			return this.#ensureFilePathInfo(type.filePath);
		}
		
		return fallbackFileInfo;
	}
	
	public addNamedType(declName: string | undefined, type: TypeObject, fileInfo: FileInfo, isDeclaration?: boolean): string | null {
		let skipIfNoUnresolvedParams = false;
		if (isStoreKeyType(type)) {
			const baseResolvedName = this.typeStoreToTypeName[type.baseStoreKey];
			const baseType = this.tsCompiler.getStoredType(type.baseStoreKey);
			const fi = this.#getFileInfoForType(baseType, fileInfo);
			let fileInfoHasName = false;
			
			if (baseResolvedName) {
				const baseNameText = convertResolvedNameToString(baseResolvedName);
				fileInfoHasName = (baseResolvedName && fi.hasNamedType(baseNameText));
				skipIfNoUnresolvedParams = fileInfoHasName;
			}
			
			if (!fileInfoHasName && type.storeKey !== type.baseStoreKey) {
				if (fi.path !== fileInfo.path) {
					fileInfo.addImportedType(baseType.typeName!, fi.path);
				}
				this.addNamedType(undefined, baseType, fi);
				skipIfNoUnresolvedParams = true;
			}
		}
		
		const hasUnresolved = hasUnresolvedTypes(type);
		if (skipIfNoUnresolvedParams && !hasUnresolved) {
			return null;
		}
		
		let resolvedNameData: ResolvedTypeName | undefined;
		let name = declName;
		if (!name) {
			resolvedNameData = this.#resolveTypeName(type);
			name = convertResolvedNameToString(resolvedNameData);
		}
		
		if (isNamedTypeObject(type)) {
			if (!isDeclaration) {
				const properFileInfo = this.#properFileInfoForType(type, getTypePathInfo(type), fileInfo);
				if (properFileInfo !== fileInfo) {
					fileInfo.addImportedType(name, properFileInfo.path);
					return this.addNamedType(name, type, properFileInfo);
				}
			}
			
			if (fileInfo.hasNamedType(name)) {
				// console.log(`${name} was already added to ${fileInfo.path}`);
				return name;
			}
			
			// prevent nested recursive types from causing stack overflow
			fileInfo.holdName(name);
			
			if (type.typeArguments?.length) {
				type.typeArguments
					.filter(arg => isNamedTypeObject(arg))
					.forEach(arg => {
						this.#processType(arg, fileInfo);
					});
			}
			
			switch (type.__type) {
				case 'ObjectTO': {
					let numIndexTypes = 0;
					const props = this.tsCompiler
						.getStoredTypeMembers(type)
						.map(prop => {
							if (prop.type.__type === 'IndexTO') {
								numIndexTypes += 1;
							}
							return this.#createProp(prop, fileInfo);
						});
					
					// If the object is just a hashmap type, then ignore the wrapping object
					const object: RustAlias | RustStruct = (numIndexTypes === props.length)
						? {
							type: 'alias',
							name,
							filePath: fileInfo.path,
							aliasTypeName: props[0].typeName,
						}
						: {
							type: 'struct',
							name,
							filePath: fileInfo.path,
							props,
						};
					
					fileInfo.addRustObject(object);
					break;
				}
				case 'EnumTO': {
					this.#createEnumType(name, type, fileInfo);
					break;
				}
				case 'UnionTO': {
					this.#createUnionEnumType(name, type.types, fileInfo);
					break;
				}
				case 'TupleTO': {
					this.#createTupleType(name, type, fileInfo);
					break;
				}
				case 'ArrayTO': {
					const inner = this.#resolveTypeName(type.typeArguments![0]);
					const object: RustAlias = {
						type: 'alias',
						name,
						filePath: fileInfo.path,
						aliasTypeName: 'Vec<' + convertResolvedNameToString(inner) + '>',
					};
					
					fileInfo.addRustObject(object);
					break;
				}
			}
		}
		else if (!isUnresolvedType(type) || isDeclaration) {
			// create a newtype
			const object: RustNewtype = {
				type: 'newtype',
				name,
				filePath: fileInfo.path,
				innerTypeName: (resolvedNameData ?? this.#resolveTypeName(type)).typeName,
			};
			fileInfo.addRustObject(object);
		}
		
		return name;
	}
	
	#processType(type: TypeObject, callerFileInfo: FileInfo): ProcessTypeResult {
		const resolved = this.#resolveTypeName(type);
		const typeName = convertResolvedNameToString(resolved);
		
		const properFileInfo = this.#getFileInfoForType(type, callerFileInfo);
		if (properFileInfo.path !== callerFileInfo.path) {
			callerFileInfo.addImportedType(typeName, properFileInfo.path);
		}
		
		getTypeChildren(type)
			?.filter(arg => isNamedTypeObject(arg))
			.forEach(arg => {
				this.#processType(arg, callerFileInfo);
			});
		
		if (this.#shouldMakeNewRustType(type, resolved, typeName, callerFileInfo)) {
			this.addNamedType(typeName, type, properFileInfo);
		}
		
		return {
			resolved,
			typeName,
			type,
		};
	}
	
	#createProp(prop: TypeProperty, fileInfo: FileInfo): RustStructProp {
		const {propName, type} = prop;
		const name = propName.startsWith('[')
			? generateIdentifierForType(type, { case: 'snakeCase' })
			: snakeCase(propName, CASE_OPTIONS);
		
		const {resolved, typeName} = this.#processType(type, fileInfo);
		
		return {
			name,
			typeName,
			isTypeParam: Boolean(resolved.isUnresolved),
			literal: resolved.literal,
			isExtraMap: type.__type === 'IndexTO',
		};
	}
	
	#createTupleType(name: string, tuple: TupleTO, fileInfo: FileInfo) {
		if (tuple.hasRestItem) {
			// create enum
			const uniqueItems = uniqWith(tuple.items, isEqual);
			const enumName = 'Enum' + name.length + name;
			this.#createUnionEnumType(enumName, uniqueItems, fileInfo);
			
			const alias: RustAlias = {
				type: 'alias',
				name,
				filePath: fileInfo.path,
				aliasTypeName: 'Vec<' + enumName + '>',
			};
			fileInfo.addRustObject(alias);
		}
		else {
			const itemNames = tuple.items.map(item => this.#processType(item, fileInfo).typeName);
			const aliasTypeName = (itemNames.length === 1)
				? '[' + itemNames[0] + '; 1]'
				: '(' + itemNames.join(', ') + ')';
			
			const alias: RustAlias = {
				type: 'alias',
				name,
				filePath: fileInfo.path,
				aliasTypeName,
			};
			fileInfo.addRustObject(alias);
		}
	}
	
	#findStructProps(object: ObjectTO, fallbackFileInfo: FileInfo): RustStructProp[] | undefined {
		const resolved = this.typeStoreToTypeName[object.storeKey];
		const fileInfo = this.#getFileInfoForType(object, fallbackFileInfo);
		const stringName = convertResolvedNameToString(resolved);
		const rustObject = fileInfo.getNamedType(stringName);
		if (!rustObject) {
			console.log('Could not find object with name:', stringName, 'in path', fileInfo.path);
			return undefined;
		}
		
		if (rustObject.type !== 'struct') {
			console.log('Rust object for', stringName, 'in path', fileInfo.path, 'must be string not', rustObject.type);
			return undefined;
		}
		
		return rustObject.props;
	}
	
	#createEnumType(name: string, type: EnumTO, fileInfo: FileInfo) {
		let isNumeric: boolean | undefined;
		const props: RustEnumProp[] = this.tsCompiler
			.getStoredTypeMembers(type)
			.map(p => {
				if (p.type.__type !== 'LiteralTO') {
					throw new Error('Enum prop type must be LiteralTO not ' + p.type.__type);
				}
				
				isNumeric = (isNumeric ?? true) && (typeof p.type.value === 'number');
				return {
					name: pascalCase(p.propName),
					typeName: '',
					serdeRename: String((p.type as LiteralTO).value),
					ignoreTypeName: true,
					isTypeParam: false,
				};
			});
		const rustEnum: RustEnum = {
			type: 'enum',
			name,
			isNumeric,
			props,
			filePath: fileInfo.path,
		};
		fileInfo.addRustObject(rustEnum);
	}
	
	#createUnionEnumType(name: string, types: TypeObject[], fileInfo: FileInfo) {
		const literalStructPropSet = new MultiMap<string, string>(Set);
		const typeOccurrences = new MultiSet<string>();
		let structValueFound = false;
		let structTagProp: string | undefined;
		
		const props: ProcessTypeResult[] = types.map(type => this.#processType(type, fileInfo));
		const nonNullProps: RustEnumProp[] = (
			props
			.filter((p) => !p.resolved.isNoneType)
			.map((p) => {
				const {type, resolved, typeName} = p;
				const name = generateIdentifierForType(type, { case: 'pascalCase' });
				let serdeRename = resolved.literal;
				
				typeOccurrences.add(type.__type);
				
				if (type.__type === 'ObjectTO') {
					structValueFound = true;
					const literalProp = this.#findStructProps(type, fileInfo)?.find(p => p.literal);
					if (literalProp) {
						literalStructPropSet.set(literalProp.name, literalProp.literal!) // add the prop name for the tag selection
						serdeRename = literalProp.literal;
					}
				}
				const ignoreTypeName = Boolean(serdeRename) && !isNamedTypeObject(type);
				
				return {
					name,
					serdeRename,
					typeName,
					isTypeParam: Boolean(resolved.isUnresolved),
					ignoreTypeName,
				};
			})
		);
		
		if (typeOccurrences.dimension >= 3) {
			throw new Error(`${name}: Cannot have an enum with 3 or more types: ` + typeOccurrences.toJSON());
		}
		else if (typeOccurrences.dimension > 1) {
			const allowedDifferentTypes = ['PrimitiveTO', 'SpecialTO', 'UnsupportedTO', 'UnionTO'];
			const [first, second] = typeOccurrences.top(2);
			if (!allowedDifferentTypes.includes(first[0]) || !allowedDifferentTypes.includes(second[0])) {
				// TODO ignore for now until we find a good way of handling this case
				// throw new Error(`${name}: The only 2 types allowed in the same enum are PrimitiveTO and SpecialTO, not ${first[0]} and ${second[0]}`);
			}
		}
		
		if (literalStructPropSet.dimension > 0) {
			const assocs = [...literalStructPropSet.associations()];
			for (let [prop, values] of assocs) {
				if (values.size === nonNullProps.length) {
					// if there are enough values to supply each object, then use this prop as the tag.
					structTagProp = prop;
				}
			}
			
			if (!structTagProp) {
				// TODO ignore for now
				// throw new Error(`${name}: Could not find a tag property to use for enum. ` + JSON.stringify(assocs));
			}
		}
		
		if (nonNullProps.length !== props.length) {
			if (nonNullProps.length === 1) {
				// create a new type wrapped
				const resolvedInner = nonNullProps[0];
				
				const object: RustNewtype = {
					type: 'newtype',
					name,
					filePath: fileInfo.path,
					innerTypeName: 'Option<' + convertResolvedNameToString(resolvedInner) + '>',
				};
				fileInfo.addRustObject(object);
			}
			else if (nonNullProps.length > 1) {
				// nonNullProps > Option<EnumStruct>
				// wrapper
				const innerName = 'Inner' + hashString(name) + name;
				const objectWrapper: RustNewtype = {
					type: 'newtype',
					name,
					filePath: fileInfo.path,
					innerTypeName: 'Option<' + innerName + '>',
				};
				fileInfo.addRustObject(objectWrapper);
				
				const object: RustEnum = {
					type: 'enum',
					name: innerName,
					filePath: fileInfo.path,
					tag: structTagProp,
					props: nonNullProps,
				};
				fileInfo.addRustObject(object);
			}
		}
		else {
			// create enum
			const object: RustEnum = {
				type: 'enum',
				name,
				filePath: fileInfo.path,
				tag: structTagProp,
				props: nonNullProps,
			};
			fileInfo.addRustObject(object);
		}
	}
	
	#resolveTypeName(type: TypeObject): ResolvedTypeName {
		if (isNamedTypeObject(type)) {
			const hasStoreKey = isStoreKeyType(type);
			if (hasStoreKey) {
				const resolved = this.typeStoreToTypeName[type.storeKey];
				if (resolved) {
					return resolved;
				}
			}
			
			const {
				substituteTypeName,
				isCollection,
				isTypescriptPath,
				isInNodeModules,
			} = getResolveOptionsForType(type);
			
			const forceNewNameAndFlattenGenerics = (isInNodeModules || isTypescriptPath) && !isCollection;
			let typeName = substituteTypeName;
			
			if (forceNewNameAndFlattenGenerics || !typeName) {
				typeName = generateIdentifierForType(type, {
					case: 'pascalCase',
					forceNewName: forceNewNameAndFlattenGenerics,
				});
			}
			
			const resolved: ResolvedTypeName = {
				typeName,
				isBuiltInType: isCollection,
			};
			
			if (forceNewNameAndFlattenGenerics) {
				resolved.generics = type.typeArguments?.filter(t => hasUnresolvedTypes(t)).map(t => this.#resolveTypeName(t)) ?? [];
			}
			else {
				resolved.generics = type.typeArguments?.map(arg => this.#resolveTypeName(arg)) ?? [];
			}
			
			if (type.__type === 'UnionTO') {
				for (const t of type.types) {
					if (!hasUnresolvedTypes(t)) {
						continue;
					}
					
					if (find(resolved.generics!, t)) {
						continue;
					}
					
					resolved.generics!.push(this.#resolveTypeName(t));
				}
			}
			
			if (hasStoreKey) {
				this.typeStoreToTypeName[type.storeKey] = resolved;
			}
			
			return resolved;
		}
		else {
			switch (type.__type) {
				case 'PrimitiveTO':
				case 'SpecialTO': {
					switch (type.kind) {
						case 'Date':
						case 'bigint':
						case 'string':
						case 'symbol':
							return { typeName: 'String', isBuiltInType: true };
						case 'boolean':
							return { typeName: 'bool', isBuiltInType: true };
						case 'number':
							return { typeName: 'i64', isBuiltInType: true };
						case 'any':
						case 'unknown':
							return { typeName: 'Box<dyn std::any::Any>', isBuiltInType: true };
						case 'never':
						case 'null':
						case 'undefined':
						case 'void':
							return { typeName: 'Box<dyn std::any::Any>', isNoneType: true, isBuiltInType: true };
					}
				}
				case 'LiteralTO': {
					const to = typeof type.value;
					switch (to) {
						case 'bigint':
						case 'string':
						case 'symbol':
							return { typeName: 'String', isBuiltInType: true, literal: String(type.value) };
						case 'boolean':
							return { typeName: 'bool', isBuiltInType: true, literal: String(type.value) };
						case 'number':
							return { typeName: 'i64', isBuiltInType: true, literal: String(type.value) };
						case 'undefined':
							return { typeName: 'Box<dyn std::any::Any>', isNoneType: true, isBuiltInType: true };
						default:
							throw new Error('Cannot resolve name of literal: ' + to);
					}
				}
				case 'IndexTO': {
					const generics = [
						this.#resolveTypeName(type.keyType),
						this.#resolveTypeName(type.valueType),
					];
					return { typeName: 'std::collections::HashMap', generics, isBuiltInType: true };
				}
				case 'UnsupportedTO': {
					// if (!type.typeText) {
					// 	throw new Error('Unsupported type needs `typeText`');
					// }
					return {
						typeName: type.typeText ?? type.kind,
						isBuiltInType: false,
						isUnresolved: true,
					};
				}
				default: {
					const typeName = generateIdentifierForType(type, { case: 'pascalCase' });
					return { typeName, isBuiltInType: false };
					// throw new Error('Cannot resolve the type name for ' + type.__type);
				}
			}
		}
	}
	
	/**
	 * Whether a TypeObject should result in a corresponding Rust type that can be instantiated in rust
	 * @param type 
	 * @returns 
	 */
	#shouldMakeNewRustType(type: TypeObject, resolved: ResolvedTypeName, typeName: string, fileInfo: FileInfo): boolean {
		if (hasUnresolvedTypes(type)) {
			return true;
		}
		
		if (isNamedTypeObject(type)) {
			const pathInfo = getTypePathInfo(type);
			if (pathInfo.isInNodeModules) {
				if (!pathInfo.isTypescriptPath) {
					return true;
				}
				
				return false;
			}
			
			const properFileInfo = this.#properFileInfoForType(type, pathInfo, fileInfo);
			
			if (!properFileInfo.hasNamedType(typeName)) {
				return !resolved.isBuiltInType;
			}
		}
		
		return false;
	}
	
	#properFileInfoForType(type: NamedTypeObject, pathInfo: TypePathInfo, callerFileInfo: FileInfo): FileInfo {
		if (pathInfo.isInNodeModules) {
			return callerFileInfo;
		}
		
		const different = Boolean(type.filePath && (type.filePath !== callerFileInfo.path));
		const properFileInfo = different ? this.#ensureFilePathInfo(type.filePath!) : callerFileInfo;
		
		return properFileInfo;
	}
}

type ProcessTypeResult = {
	type: TypeObject;
	resolved: ResolvedTypeName;
	typeName: string;
}

type ResolveTypeNameOptions = {
	substituteTypeName?: string;
	isCollection: boolean;
	isInNodeModules: boolean;
	isTypescriptPath: boolean;
}

type ResolvedTypeName = {
	wrapper?: string;
	typeName: string;
	generics?: Array<ResolvedTypeName>;
	isNoneType?: boolean;
	isBuiltInType?: boolean;
	isUnresolved?: boolean;
	literal?: string;
}

function convertResolvedNameToString(resolved: ResolvedTypeName): string {
	const {
		wrapper,
		typeName,
		generics,
	} = resolved;
	
	let text = typeName;
	
	if (generics?.length) {
		const args = generics.map(x => convertResolvedNameToString(x));
		text += '<' + args.join(', ') + '>';
	}
	
	if (wrapper) {
		text = wrapper + '<' + text + '>';
	}
	
	return text;
}

function getResolveOptionsForType(type: NamedTypeObject): ResolveTypeNameOptions {
	let substituteTypeName: string | undefined;
	let isCollection = false;
	const {isInNodeModules, isTypescriptPath} = getTypePathInfo(type);
	
	if (isTypescriptPath) {
		if (type.typeName === 'Record') {
			substituteTypeName = 'std::collections::HashMap';
			isCollection = true;
		}
		else if (type.typeName === 'Set') {
			substituteTypeName = 'std::collections::HashSet';
			isCollection = true;
		}
	}
	
	if (type.__type === 'ArrayTO') {
		if (type.typeName === 'Array' && isTypescriptPath) {
			substituteTypeName = 'Vec';
		}
		isCollection = true;
	}
	
	return {
		substituteTypeName,
		isCollection,
		isInNodeModules,
		isTypescriptPath,
	};
}

function hasUnresolvedTypes(type: TypeObject): boolean {
	let unresolved = isUnresolvedType(type);
	if (isNamedTypeObject(type)) {
		unresolved = type.typeArguments?.some(x => hasUnresolvedTypes(x)) ?? false;
	}
	
	if (type.__type === 'UnionTO') {
		unresolved ||= type.types.some(x => hasUnresolvedTypes(x));
	}
	
	return unresolved;
}

function isUnresolvedType(type: TypeObject): boolean {
	return type.__type === 'UnsupportedTO' && type.kind === 'unresolvedTypeParameter';
}

function getTypePathInfo(type: NamedTypeObject): TypePathInfo {
	return getPathInfo(type.filePath ?? '');
}