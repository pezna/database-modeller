import { describe, expect, it } from "vitest";
import type { IndexTO, UnionTO } from "../ts-input/type-object";
import { generateIdentifierForType } from "./util";

describe('type to identifier', () => {
	it('index type with named object', () => {
		const indexType: IndexTO = {
			__type: 'IndexTO',
			keyType: {
				__type: 'UnsupportedTO',
				kind: 'convert',
				typeText: '`Sway${string}Hi`',
			},
			valueType: {
				__type: 'ObjectTO',
				typeName: 'GenericData',
				typeText: 'GenericData',
				storeKey: 'GenericData::/src/examples/import-me',
				baseStoreKey: 'GenericData::/src/examples/import-me',
			},
		};
		
		const snakeIdent = generateIdentifierForType(indexType, { case: 'snakeCase' });
		expect(snakeIdent).toEqual('sway_string_hi_to_generic_data');
		const pascalIdent = generateIdentifierForType(indexType, { case: 'pascalCase' });
		expect(pascalIdent).toEqual('SwayStringHiToGenericData');
	})
	
	it('index type with unnamed object', () => {
		const indexType: IndexTO = {
			__type: 'IndexTO',
			keyType: { __type: 'PrimitiveTO', kind: 'number' },
			valueType: {
				__type: 'ObjectTO',
				typeName: '{ person: string; order: Order; }',
				typeText: '{ person: string; order: Order; }',
				filePath: '/src/examples/example.ts',
				storeKey: '{ person: string; order: Order; }::/src/examples/example.ts',
				baseStoreKey: '{ person: string; order: Order; }::/src/examples/example.ts',
			}
		};
		
		const snakeIdent = generateIdentifierForType(indexType, { case: 'snakeCase' });
		expect(snakeIdent).toEqual('number_to_object_person_string_order339233');
		const pascalIdent = generateIdentifierForType(indexType, { case: 'pascalCase' });
		expect(pascalIdent).toEqual('NumberToObjectPersonStringOrder339233');
	})
	
	it('nullable enum', () => {
		const unionType: UnionTO = {
			__type: 'UnionTO',
			typeName: undefined,
			typeText: 'StateType | null',
			types: [
				{ __type: 'SpecialTO', kind: 'null' },
				{
					__type: 'EnumTO',
					typeName: 'StateType',
					typeText: 'StateType',
					filePath: '/src/examples/example.ts',
					storeKey: 'StateType::/src/examples/example.ts',
					baseStoreKey: 'StateType::/src/examples/example.ts',
				},
			],
		};
		
		const snakeIdent = generateIdentifierForType(unionType, { case: 'snakeCase' });
		expect(snakeIdent).toEqual('union_state_type_null188516');
		const pascalIdent = generateIdentifierForType(unionType, { case: 'pascalCase' });
		expect(pascalIdent).toEqual('UnionStateTypeNull188516');
	})
})