import { describe, expect, it } from "vitest";
import { CompilerApiHelper } from "../ts-input/compiler-api-helper";
import { createTypescriptProgram, createTypescriptProgramForCode } from "../ts-input/program";
import { codeFileAbsolutePath, isOk } from "../util";
import { RustTypeContext } from "./rust-types";

describe('convert between objects', () => {
	describe('simple objects and types', () => {
		it('creates a simple struct with generics', () => {
			const {program, basePath} = createTypescriptProgramForCode(`
				export interface First<T> {
					name: string;
					age: number;
					type: T;
				}
			`);
			const compiler = new CompilerApiHelper(program, basePath);
			const typesResult = compiler.extractTypes('script.ts');
			if (!isOk(typesResult)) {
				throw new TypeError('Types result is not ok');
			}
			
			expect(typesResult).toEqual({
				__type: 'ok',
				ok: {
					First: {
						typeName: 'First',
						typeText: 'First<T>',
						filePath: 'script.ts',
						type: {
							__type: 'ObjectTO',
							typeName: 'First',
							typeText: 'First<T>',
							filePath: 'script.ts',
							storeKey: 'First<T>::script.ts',
							baseStoreKey: 'First<T>::script.ts',
							typeArguments: [
								{
									__type: 'UnsupportedTO',
									kind: 'unresolvedTypeParameter',
									typeText: 'T',
								},
							],
						},
					},
				},
			});
			
			const rustContext = new RustTypeContext(basePath, compiler);
			
			const {First} = typesResult.ok;
			const firstMembers = compiler.getStoredTypeMembers(First.type as any);
			expect(firstMembers).toEqual([
				{
					propName: 'name',
					type: { __type: 'PrimitiveTO', kind: 'string' },
				},
				{
					propName: 'age',
					type: { __type: 'PrimitiveTO', kind: 'number' },
				},
				{
					propName: 'type',
					type: {
						__type: 'UnsupportedTO',
						kind: 'unresolvedTypeParameter',
						typeText: 'T',
					},
				},
			]);
			rustContext.addDeclaration(First);
			
			const fileInfos = rustContext.getAllFiles();
			expect(Object.values(fileInfos)).toHaveLength(1);
			
			const fi = fileInfos['script.ts'];
			expect(fi.path).toEqual('script.ts');
			expect(fi.namedTypes).toHaveLength(1);
			expect(fi.namedTypes).toContain('First<T>');
			expect(fi.importedTypes).toEqual({});
			
			const firstStruct = fi.rustObjects['First<T>'];
			expect(firstStruct).toMatchObject({
				type: 'struct',
				name: 'First<T>',
				filePath: 'script.ts',
				props: [
					{ name: 'name', typeName: 'String', isTypeParam: false },
					{ name: 'age', typeName: 'i64', isTypeParam: false },
					{ name: 'type', typeName: 'T', isTypeParam: true },
				],
			});
		})
		
		it('declaration with built-in types', () => {
			const {program, basePath} = createTypescriptProgramForCode(`
				export type A = any
				export type D = B | C | undefined
				export type C = false | null
				export type B = number
			`);
			const compiler = new CompilerApiHelper(program, basePath);
			const typesResult = compiler.extractTypes('script.ts');
			if (!isOk(typesResult)) {
				throw new TypeError('Types result is not ok');
			}
			
			const rustContext = new RustTypeContext(basePath, compiler);
			rustContext.addDeclarations(Object.values(typesResult.ok));
			
			const fileInfos = rustContext.getAllFiles();
			expect(Object.values(fileInfos)).toHaveLength(1);
			
			const fi = fileInfos['script.ts'];
			
			expect(fi.path).toEqual('script.ts');
			expect(fi.namedTypes).toHaveLength(5);
			expect(fi.importedTypes).toEqual({});
			
			expect(fi.rustObjects).toMatchObject({
				A: {
					type: 'newtype',
					name: 'A',
					filePath: 'script.ts',
					innerTypeName: 'Box<dyn std::any::Any>',
				},
				B: {
					type: 'newtype',
					name: 'B',
					filePath: 'script.ts',
					innerTypeName: 'i64',
				},
				C: {
					type: 'newtype',
					name: 'C',
					filePath: 'script.ts',
					innerTypeName: 'Option<bool>',
				},
				D: {
					type: 'newtype',
					name: 'D',
					filePath: 'script.ts',
					innerTypeName: 'Option<Inner136D>',
				},
				Inner136D: {
					type: 'enum',
					name: 'Inner136D',
					filePath: 'script.ts',
					tag: undefined,
					props: [
						{ name: 'Number', typeName: 'i64', isTypeParam: false, ignoreTypeName: false },
						{ name: 'C', typeName: 'C', isTypeParam: false, ignoreTypeName: false },
					],
				},
			});
		})
	})
	
	describe('union types', () => {
		it('union property type creates new object', () => {
			const {program, basePath} = createTypescriptProgramForCode(`
				export interface First<T extends {hi: string}, U> {
					type: T;
					other: U;
					both: T | U;
				}
			`);
			const compiler = new CompilerApiHelper(program, basePath);
			const typesResult = compiler.extractTypes('script.ts');
			if (!isOk(typesResult)) {
				throw new TypeError('Types result is not ok');
			}
			expect(typesResult).toEqual({
				__type: 'ok',
				ok: {
					First: {
						typeName: 'First',
						typeText: 'First<T, U>',
						filePath: 'script.ts',
						type: {
							__type: 'ObjectTO',
							typeName: 'First',
							typeText: 'First<T, U>',
							filePath: 'script.ts',
							storeKey: 'First<T, U>::script.ts',
							baseStoreKey: 'First<T, U>::script.ts',
							typeArguments: [
								{
									__type: 'UnsupportedTO',
									kind: 'unresolvedTypeParameter',
									typeText: 'T',
								},
								{
									__type: 'UnsupportedTO',
									kind: 'unresolvedTypeParameter',
									typeText: 'U',
								},
							],
						},
					},
				},
			});
			
			const rustContext = new RustTypeContext(basePath, compiler);
			
			const {First} = typesResult.ok;
			const firstMembers = compiler.getStoredTypeMembers(First.type as any);
			expect(firstMembers).toEqual([
				{
					propName: 'type',
					type: {
						__type: 'UnsupportedTO',
						kind: 'unresolvedTypeParameter',
						typeText: 'T'
					}
				},
				{
					propName: 'other',
					type: {
						__type: 'UnsupportedTO',
						kind: 'unresolvedTypeParameter',
						typeText: 'U'
					}
				},
				{
					propName: 'both',
					type: {
						__type: 'UnionTO',
						typeName: undefined,
						typeText: 'T | U',
						filePath: undefined,
						types: [
							{
								__type: 'UnsupportedTO',
								kind: 'unresolvedTypeParameter',
								typeText: 'T'
							},
							{
								__type: 'UnsupportedTO',
								kind: 'unresolvedTypeParameter',
								typeText: 'U'
							}
						]
					}
				}
			]);
			
			rustContext.addDeclaration(First);
			
			const fileInfos = rustContext.getAllFiles();
			expect(Object.values(fileInfos)).toHaveLength(1);
			
			const fi = fileInfos['script.ts'];
			
			expect(fi.path).toEqual('script.ts');
			expect(fi.namedTypes).toHaveLength(2);
			expect(fi.namedTypes).toEqual(new Set(['First<T, U>', 'UnionX5235<T, U>']));
			expect(fi.declaredNames).toEqual(new Set(['First<T, U>']));
			expect(fi.importedTypes).toEqual({});
			expect(fi.rustObjects).toMatchObject({
				'UnionX5235<T, U>': {
					type: 'enum',
					name: 'UnionX5235<T, U>',
					filePath: 'script.ts',
					tag: undefined,
					props: [
						{ name: 'T', typeName: 'T', isTypeParam: true, ignoreTypeName: false },
						{ name: 'U', typeName: 'U', isTypeParam: true, ignoreTypeName: false },
					],
				},
				'First<T, U>': {
					type: 'struct',
					name: 'First<T, U>',
					filePath: 'script.ts',
					props: [
						{ name: 'type', typeName: 'T', isTypeParam: true },
						{ name: 'other', typeName: 'U', isTypeParam: true },
						{
							name: 'both',
							typeName: 'UnionX5235<T, U>',
							isTypeParam: false,
						},
					],
				},
			});
		})
		
		it('named union type', () => {
			const {program, basePath} = createTypescriptProgramForCode(`
				export interface First<T> {
					type: T;
					both: T | string;
				}
					
				export type Hello = { type: 'one', hello: string } | { type: 'two', bye: number }
			`);
			const compiler = new CompilerApiHelper(program, basePath);
			const typesResult = compiler.extractTypes('script.ts');
			if (!isOk(typesResult)) {
				throw new TypeError('Types result is not ok');
			}
			expect(typesResult).toEqual({
				__type: 'ok',
				ok: {
					First: {
						typeName: 'First',
						typeText: 'First<T>',
						filePath: 'script.ts',
						type: {
							__type: 'ObjectTO',
							typeName: 'First',
							typeText: 'First<T>',
							filePath: 'script.ts',
							storeKey: 'First<T>::script.ts',
							baseStoreKey: 'First<T>::script.ts',
							typeArguments: [
								{
									__type: 'UnsupportedTO',
									kind: 'unresolvedTypeParameter',
									typeText: 'T',
								},
							],
						},
					},
					Hello: {
						typeName: "Hello",
						typeText: "Hello",
						filePath: 'script.ts',
						type: {
							__type: 'UnionTO',
							filePath: 'script.ts',
							typeName: 'Hello',
							typeText: 'Hello',
							types: [
								{
									__type: 'ObjectTO',
									baseStoreKey: '{ type: "one"; hello: string; }::script.ts',
									filePath: 'script.ts',
									storeKey: '{ type: "one"; hello: string; }::script.ts',
									typeArguments: undefined,
									typeName: '__type',
									typeText: '{ type: "one"; hello: string; }',
								},
								{
									__type: 'ObjectTO',
									baseStoreKey: '{ type: "two"; bye: number; }::script.ts',
									filePath: 'script.ts',
									storeKey: '{ type: "two"; bye: number; }::script.ts',
									typeArguments: undefined,
									typeName: '__type',
									typeText: '{ type: "two"; bye: number; }',
								},
							],
						},
					},
				},
			});
			
			const rustContext = new RustTypeContext(basePath, compiler);
			
			/// Add Hello object only
			rustContext.addDeclaration(typesResult.ok.Hello);
			
			let fileInfos = rustContext.getAllFiles();
			expect(Object.values(fileInfos)).toHaveLength(1);
			
			let fi = fileInfos['script.ts'];
			
			expect(fi.path).toEqual('script.ts');
			expect(fi.namedTypes).toHaveLength(3);
			expect(fi.importedTypes).toEqual({});
			
			const firstCreatedStructs = {
				ObjectTypeOneHello303531: {
					type: 'struct',
					name: 'ObjectTypeOneHello303531',
					filePath: 'script.ts',
					props: [
						{ name: 'type', typeName: 'String', isTypeParam: false, literal: 'one' },
						{ name: 'hello', typeName: 'String', isTypeParam: false },
					],
				},
				ObjectTypeTwoBye282829: {
					type: 'struct',
					name: 'ObjectTypeTwoBye282829',
					filePath: 'script.ts',
					props: [
						{ name: 'type', typeName: 'String', isTypeParam: false, literal: 'two' },
						{ name: 'bye', typeName: 'i64', isTypeParam: false },
					],
				},
				Hello: {
					type: 'enum',
					name: 'Hello',
					filePath: 'script.ts',
					tag: 'type',
					props: [
						{
							name: 'ObjectTypeOneHello303531',
							typeName: 'ObjectTypeOneHello303531',
							isTypeParam: false,
							ignoreTypeName: false,
							serdeRename: 'one',
						},
						{
							name: 'ObjectTypeTwoBye282829',
							typeName: 'ObjectTypeTwoBye282829',
							isTypeParam: false,
							ignoreTypeName: false,
							serdeRename: 'two',
						}
					],
				},
			};
			expect(fi.rustObjects).toMatchObject(firstCreatedStructs);
			
			/// Add First object
			rustContext.addDeclaration(typesResult.ok.First);
			
			fileInfos = rustContext.getAllFiles();
			expect(Object.values(fileInfos)).toHaveLength(1);
			
			fi = fileInfos['script.ts'];
			
			expect(fi.path).toEqual('script.ts');
			expect(fi.namedTypes).toHaveLength(5); // 2 more have been added
			expect(fi.importedTypes).toEqual({});
			// console.dir(fi.rustObjects, {depth: null});
			
			expect(fi.rustObjects).toMatchObject({
				...firstCreatedStructs,
				'UnionString124110<T>': {
					type: 'enum',
					name: 'UnionString124110<T>',
					filePath: 'script.ts',
					props: [
						{ name: 'String', typeName: 'String', isTypeParam: false, ignoreTypeName: false },
						{ name: 'T', typeName: 'T', isTypeParam: true, ignoreTypeName: false },
					],
				},
				'First<T>': {
					type: 'struct',
					name: 'First<T>',
					filePath: 'script.ts',
					props: [
						{ name: 'type', typeName: 'T', isTypeParam: true },
						{
							name: 'both',
							typeName: 'UnionString124110<T>',
							isTypeParam: false,
						},
					],
				},
			});
		})
		
		it('nested named unions with objects', () => {
			const {program, basePath} = createTypescriptProgramForCode(`
				export interface Second {
					nested: Apple<Apple<{ yes: boolean }>>;
					apple: Apple<{ pie: 'yolo' }>;
					other: Apple<{ pie: 'yolo' }, { bye: number }>;
					next: Apple<{ pie: 'yolo' }, { pie: 'yolo' }>;
				}
				
				export type Apple<T, U = string> = T | U;
			`);
			const compiler = new CompilerApiHelper(program, basePath);
			const typesResult = compiler.extractTypes('script.ts');
			if (!isOk(typesResult)) {
				throw new TypeError('Types result is not ok');
			}
			// console.dir(typesResult, {depth: null});
			
			const rustContext = new RustTypeContext(basePath, compiler);
			rustContext.addDeclarations(Object.values(typesResult.ok));
			
			const fileInfos = rustContext.getAllFiles();
			expect(Object.values(fileInfos)).toHaveLength(1);
			
			const fi = fileInfos['script.ts'];
			// console.dir(fi.rustObjects, {depth: null});
			expect(fi.rustObjects).toMatchObject({
				ObjectYesBoolean185217: {
					type: 'struct',
					name: 'ObjectYesBoolean185217',
					filePath: 'script.ts',
					props: [ { name: 'yes', typeName: 'bool', isTypeParam: false } ],
				},
				ObjectPieYolo159416: {
					type: 'struct',
					name: 'ObjectPieYolo159416',
					filePath: 'script.ts',
					props: [ { name: 'pie', typeName: 'String', isTypeParam: false, literal: 'yolo' } ],
				},
				ObjectByeNumber173716: {
					type: 'struct',
					name: 'ObjectByeNumber173716',
					filePath: 'script.ts',
					props: [ { name: 'bye', typeName: 'i64', isTypeParam: false } ],
				},
				Second: {
					type: 'struct',
					name: 'Second',
					filePath: 'script.ts',
					props: [
						{
							name: 'nested',
							typeName: 'Apple<Apple<ObjectYesBoolean185217, String>, String>',
							isTypeParam: false,
						},
						{
							name: 'apple',
							typeName: 'Apple<ObjectPieYolo159416, String>',
							isTypeParam: false,
						},
						{
							name: 'other',
							typeName: 'Apple<ObjectPieYolo159416, ObjectByeNumber173716>',
							isTypeParam: false,
						},
						{
							name: 'next',
							typeName: 'Apple<ObjectPieYolo159416, ObjectPieYolo159416>',
							isTypeParam: false,
						},
					],
				},
				'Apple<T, U>': {
					type: 'enum',
					name: 'Apple<T, U>',
					filePath: 'script.ts',
					tag: undefined,
					props: [
						{ name: 'T', typeName: 'T', isTypeParam: true, ignoreTypeName: false },
						{ name: 'U', typeName: 'U', isTypeParam: true, ignoreTypeName: false },
					],
				},
			});
		})
		
		it('union with different circular named types with similar properties', () => {
			const {program, basePath} = createTypescriptProgramForCode(`
				export type TypeObject =
					| PrimitiveTO
					| SpecialTO
					| EnumTO
					| ObjectTO

				type WithTypeName = {
					typeName?: string;
					typeText: string;
					filePath?: string;
					typeArguments?: TypeObject[];
				}

				export type NamedTypeObject = Extract<TypeObject, WithTypeName>;

				export type PrimitiveTO = {
					__type: 'PrimitiveTO';
					kind: 'string' | 'number' | 'bigint' | 'boolean';
				}

				export type SpecialTO = {
					__type: 'SpecialTO';
					kind:
						| 'null'
						| 'undefined'
						| 'any'
						| 'unknown'
						| 'never'
						| 'void'
						| 'Date'
						| 'unique symbol'
						| 'Symbol'
						;
				}

				type WithStoreKey = {
					storeKey: string;
					baseStoreKey: string;
				}

				export type ObjectTO = WithTypeName & WithStoreKey & {
					__type: 'ObjectTO';
				}

				export type EnumTO = WithTypeName & WithStoreKey & {
					__type: 'EnumTO';
				}
			`);
			const compiler = new CompilerApiHelper(program, basePath);
			const typesResult = compiler.extractTypes('script.ts');
			if (!isOk(typesResult)) {
				throw new TypeError('Types result is not ok');
			}
			// console.dir(typesResult, {depth: null});
			
			const rustContext = new RustTypeContext(basePath, compiler);
			rustContext.addDeclarations(Object.values(typesResult.ok));
			
			const fileInfos = rustContext.getAllFiles();
			expect(Object.values(fileInfos)).toHaveLength(1);
			
			const fi = fileInfos['script.ts'];
			expect(fi.importedTypes).toEqual({});
			expect(fi.declaredNames).toEqual(new Set([
				'TypeObject',
				'NamedTypeObject',
				'PrimitiveTO',
				'SpecialTO',
				'ObjectTO',
				'EnumTO',
			]));
			// console.dir(fi.rustObjects, {depth: null});
			expect(fi.rustObjects).toMatchObject({
				UnionStringNumberBigint384642: {
					type: 'enum',
					name: 'UnionStringNumberBigint384642',
					filePath: 'script.ts',
					tag: undefined,
					props: [
						{
							name: 'String',
							serdeRename: 'string',
							typeName: 'String',
							isTypeParam: false,
							ignoreTypeName: true,
						},
						{
							name: 'Number',
							serdeRename: 'number',
							typeName: 'String',
							isTypeParam: false,
							ignoreTypeName: true,
						},
						{
							name: 'Bigint',
							serdeRename: 'bigint',
							typeName: 'String',
							isTypeParam: false,
							ignoreTypeName: true,
						},
						{
							name: 'Boolean',
							serdeRename: 'boolean',
							typeName: 'String',
							isTypeParam: false,
							ignoreTypeName: true,
						},
					],
				},
				PrimitiveTo: {
					type: 'struct',
					name: 'PrimitiveTo',
					filePath: 'script.ts',
					props: [
						{
							name: '__type',
							typeName: 'String',
							isTypeParam: false,
							literal: 'PrimitiveTO',
						},
						{
							name: 'kind',
							typeName: 'UnionStringNumberBigint384642',
							isTypeParam: false,
						},
					],
				},
				UnionUndefinedNullAny833697: {
					type: 'enum',
					name: 'UnionUndefinedNullAny833697',
					filePath: 'script.ts',
					tag: undefined,
					props: [
						{
							name: 'Undefined',
							serdeRename: 'undefined',
							typeName: 'String',
							isTypeParam: false,
							ignoreTypeName: true,
						},
						{
							name: 'Null',
							serdeRename: 'null',
							typeName: 'String',
							isTypeParam: false,
							ignoreTypeName: true,
						},
						{
							name: 'Any',
							serdeRename: 'any',
							typeName: 'String',
							isTypeParam: false,
							ignoreTypeName: true,
						},
						{
							name: 'Unknown',
							serdeRename: 'unknown',
							typeName: 'String',
							isTypeParam: false,
							ignoreTypeName: true,
						},
						{
							name: 'Never',
							serdeRename: 'never',
							typeName: 'String',
							isTypeParam: false,
							ignoreTypeName: true,
						},
						{
							name: 'Void',
							serdeRename: 'void',
							typeName: 'String',
							isTypeParam: false,
							ignoreTypeName: true,
						},
						{
							name: 'Date',
							serdeRename: 'Date',
							typeName: 'String',
							isTypeParam: false,
							ignoreTypeName: true,
						},
						{
							name: 'UniqueSymbol',
							serdeRename: 'unique symbol',
							typeName: 'String',
							isTypeParam: false,
							ignoreTypeName: true,
						},
						{
							name: 'Symbol',
							serdeRename: 'Symbol',
							typeName: 'String',
							isTypeParam: false,
							ignoreTypeName: true,
						},
					],
				},
				SpecialTo: {
					type: 'struct',
					name: 'SpecialTo',
					filePath: 'script.ts',
					props: [
						{
							name: '__type',
							typeName: 'String',
							isTypeParam: false,
							literal: 'SpecialTO',
						},
						{
							name: 'kind',
							typeName: 'UnionUndefinedNullAny833697',
							isTypeParam: false,
						},
					],
				},
				UnionStringUndefined216118: {
					type: 'newtype',
					name: 'UnionStringUndefined216118',
					filePath: 'script.ts',
					innerTypeName: 'Option<String>',
				},
				EnumTo: {
					type: 'struct',
					name: 'EnumTo',
					filePath: 'script.ts',
					props: [
						{
							name: 'type_name',
							typeName: 'UnionStringUndefined216118',
							isTypeParam: false,
						},
						{
							name: 'type_text',
							typeName: 'String',
							isTypeParam: false,
						},
						{
							name: 'file_path',
							typeName: 'UnionStringUndefined216118',
							isTypeParam: false,
						},
						{
							name: 'type_arguments',
							typeName: 'Vec<TypeObject>',
							isTypeParam: false,
						},
						{
							name: 'store_key',
							typeName: 'String',
							isTypeParam: false,
						},
						{
							name: 'base_store_key',
							typeName: 'String',
							isTypeParam: false,
						},
						{
							name: '__type',
							typeName: 'String',
							isTypeParam: false,
							literal: 'EnumTO',
						},
					],
				},
				ObjectTo: {
					type: 'struct',
					name: 'ObjectTo',
					filePath: 'script.ts',
					props: [
						{
							name: 'type_name',
							typeName: 'UnionStringUndefined216118',
							isTypeParam: false,
						},
						{
							name: 'type_text',
							typeName: 'String',
							isTypeParam: false,
						},
						{
							name: 'file_path',
							typeName: 'UnionStringUndefined216118',
							isTypeParam: false,
						},
						{
							name: 'type_arguments',
							typeName: 'Vec<TypeObject>',
							isTypeParam: false,
						},
						{
							name: 'store_key',
							typeName: 'String',
							isTypeParam: false,
						},
						{
							name: 'base_store_key',
							typeName: 'String',
							isTypeParam: false,
						},
						{
							name: '__type',
							typeName: 'String',
							isTypeParam: false,
							literal: 'ObjectTO',
						},
					],
				},
				TypeObject: {
					type: 'enum',
					name: 'TypeObject',
					filePath: 'script.ts',
					tag: '__type',
					props: [
						{
							name: 'PrimitiveTo',
							serdeRename: 'PrimitiveTO',
							typeName: 'PrimitiveTo',
							isTypeParam: false,
							ignoreTypeName: false,
						},
						{
							name: 'SpecialTo',
							serdeRename: 'SpecialTO',
							typeName: 'SpecialTo',
							isTypeParam: false,
							ignoreTypeName: false,
						},
						{
							name: 'EnumTo',
							serdeRename: 'EnumTO',
							typeName: 'EnumTo',
							isTypeParam: false,
							ignoreTypeName: false,
						},
						{
							name: 'ObjectTo',
							serdeRename: 'ObjectTO',
							typeName: 'ObjectTo',
							isTypeParam: false,
							ignoreTypeName: false,
						},
					],
				},
				NamedTypeObject: {
					type: 'enum',
					name: 'NamedTypeObject',
					filePath: 'script.ts',
					tag: '__type',
					props: [
						{
							name: 'EnumTo',
							serdeRename: 'EnumTO',
							typeName: 'EnumTo',
							isTypeParam: false,
							ignoreTypeName: false,
						},
						{
							name: 'ObjectTo',
							serdeRename: 'ObjectTO',
							typeName: 'ObjectTo',
							isTypeParam: false,
							ignoreTypeName: false,
						},
					],
				},
			});
		})
	})
	
	describe('intersection types', () => {
		it('simple intersections', () => {
			const {program, basePath} = createTypescriptProgramForCode(`
				export type Hello = { age: number } & string
				export type Merge = { type: 'one' } & { type: string }
			`);
			const compiler = new CompilerApiHelper(program, basePath);
			const typesResult = compiler.extractTypes('script.ts');
			if (!isOk(typesResult)) {
				throw new TypeError('Types result is not ok');
			}
			// console.dir(typesResult, {depth: null});
			
			const rustContext = new RustTypeContext(basePath, compiler);
			rustContext.addDeclarations(Object.values(typesResult.ok));
			
			const fileInfos = rustContext.getAllFiles();
			expect(Object.values(fileInfos)).toHaveLength(1);
			
			const fi = fileInfos['script.ts'];
			expect(fi.rustObjects).toEqual({
				Hello: {
					type: 'struct',
					name: 'Hello',
					filePath: 'script.ts',
					props: expect.arrayContaining([
						{ name: 'age', typeName: 'i64', isTypeParam: false, literal: undefined, isExtraMap: false },
						{ name: 'to_string', typeName: '!Invalid!', isTypeParam: false, literal: undefined, isExtraMap: false },
						{ name: 'char_at', typeName: '!Invalid!', isTypeParam: false, literal: undefined, isExtraMap: false },
						{
							name: 'char_code_at',
							typeName: '!Invalid!',
							isTypeParam: false,
							literal: undefined,
							isExtraMap: false,
						},
						{ name: 'concat', typeName: '!Invalid!', isTypeParam: false, literal: undefined, isExtraMap: false },
						{ name: 'index_of', typeName: '!Invalid!', isTypeParam: false, literal: undefined, isExtraMap: false },
						{
							name: 'last_index_of',
							typeName: '!Invalid!',
							isTypeParam: false,
							literal: undefined,
							isExtraMap: false,
						},
						{ name: 'at', typeName: '!Invalid!', isTypeParam: false, literal: undefined, isExtraMap: false },
						{
							name: 'number_to_string',
							typeName: 'std::collections::HashMap<i64, String>',
							isTypeParam: false,
							literal: undefined,
							isExtraMap: true,
						},
					]),
				},
				Merge: {
					type: 'struct',
					name: 'Merge',
					filePath: 'script.ts',
					props: [ { name: 'type', typeName: 'String', isTypeParam: false, literal: 'one', isExtraMap: false } ],
				},
			});
			// console.dir(fi.rustObjects, {depth: null});
		})
		
		it('resolves bad intersections to never', () => {
			const {program, basePath} = createTypescriptProgramForCode(`
				export type Never = number & string
			`);
			const compiler = new CompilerApiHelper(program, basePath);
			const typesResult = compiler.extractTypes('script.ts');
			if (!isOk(typesResult)) {
				throw new TypeError('Types result is not ok');
			}
			// console.dir(typesResult, {depth: null});
			expect(typesResult).toEqual({
				__type: 'ok',
				ok: {
					Never: {
						typeName: 'Never',
						typeText: 'never',
						filePath: 'script.ts',
						type: { __type: 'SpecialTO', kind: 'never' },
					},
				},
			});
			
			const rustContext = new RustTypeContext(basePath, compiler);
			rustContext.addDeclarations(Object.values(typesResult.ok));
			
			const fileInfos = rustContext.getAllFiles();
			expect(Object.values(fileInfos)).toHaveLength(1);
			
			const fi = fileInfos['script.ts'];
			// console.dir(fi.rustObjects, {depth: null});
			expect(fi.rustObjects).toMatchObject({
				Never: {
					type: 'newtype',
					name: 'Never',
					filePath: 'script.ts',
					innerTypeName: 'Box<dyn std::any::Any>',
				},
			});
		})
	})
	
	describe('arrays and tuples', () => {
		it('simple tuples', () => {
			const {program, basePath} = createTypescriptProgramForCode(`
				export type One = [number];
				export type Three = [number, string, number]
				export type Rest = [number, string, ...(One | Three)] // => [number, string, number] | [number, string, number, string, number]
				export type ObjRest = [number, string, ...(One | Three)[]]
			`);
			const compiler = new CompilerApiHelper(program, basePath);
			const typesResult = compiler.extractTypes('script.ts');
			if (!isOk(typesResult)) {
				throw new TypeError('Types result is not ok');
			}
			// console.dir(typesResult, {depth: null});
			expect(typesResult).toEqual({
				__type: 'ok',
				ok: {
					One: {
						typeName: 'One',
						typeText: 'One',
						filePath: 'script.ts',
						type: {
							__type: 'TupleTO',
							typeName: 'One',
							typeText: 'One',
							filePath: 'script.ts',
							items: [ { __type: 'PrimitiveTO', kind: 'number' } ],
							hasRestItem: false,
						},
					},
					Three: {
						typeName: 'Three',
						typeText: 'Three',
						filePath: 'script.ts',
						type: {
							__type: 'TupleTO',
							typeName: 'Three',
							typeText: 'Three',
							filePath: 'script.ts',
							items: [
								{ __type: 'PrimitiveTO', kind: 'number' },
								{ __type: 'PrimitiveTO', kind: 'string' },
								{ __type: 'PrimitiveTO', kind: 'number' },
							],
							hasRestItem: false,
						},
					},
					Rest: {
						typeName: 'Rest',
						typeText: '[number, string, number] | [number, string, number, string, number]',
						filePath: 'script.ts',
						type: {
							__type: 'UnionTO',
							typeName: 'Rest',
							typeText: '[number, string, number] | [number, string, number, string, number]',
							filePath: 'script.ts',
							types: [
								{
									__type: 'TupleTO',
									typeName: undefined,
									typeText: '[number, string, number]',
									filePath: undefined,
									items: [
										{ __type: 'PrimitiveTO', kind: 'number' },
										{ __type: 'PrimitiveTO', kind: 'string' },
										{ __type: 'PrimitiveTO', kind: 'number' },
									],
									hasRestItem: false,
								},
								{
									__type: 'TupleTO',
									typeName: undefined,
									typeText: '[number, string, number, string, number]',
									filePath: undefined,
									items: [
										{ __type: 'PrimitiveTO', kind: 'number' },
										{ __type: 'PrimitiveTO', kind: 'string' },
										{ __type: 'PrimitiveTO', kind: 'number' },
										{ __type: 'PrimitiveTO', kind: 'string' },
										{ __type: 'PrimitiveTO', kind: 'number' },
									],
									hasRestItem: false,
								},
							],
						},
					},
					ObjRest: {
						typeName: 'ObjRest',
						typeText: 'ObjRest',
						filePath: 'script.ts',
						type: {
							__type: 'TupleTO',
							typeName: 'ObjRest',
							typeText: 'ObjRest',
							filePath: 'script.ts',
							items: [
								{ __type: 'PrimitiveTO', kind: 'number' },
								{ __type: 'PrimitiveTO', kind: 'string' },
								{
									__type: 'UnionTO',
									typeName: undefined,
									typeText: 'One | Three',
									filePath: undefined,
									types: [
										{
											__type: 'TupleTO',
											typeName: 'One',
											typeText: 'One',
											filePath: 'script.ts',
											items: [ { __type: 'PrimitiveTO', kind: 'number' } ],
											hasRestItem: false,
										},
										{
											__type: 'TupleTO',
											typeName: 'Three',
											typeText: 'Three',
											filePath: 'script.ts',
											items: [
												{ __type: 'PrimitiveTO', kind: 'number' },
												{ __type: 'PrimitiveTO', kind: 'string' },
												{ __type: 'PrimitiveTO', kind: 'number' },
											],
											hasRestItem: false,
										},
									],
								},
							],
							hasRestItem: true,
						},
					},
				},
			});
			
			const rustContext = new RustTypeContext(basePath, compiler);
			rustContext.addDeclarations(Object.values(typesResult.ok));
			
			const fileInfos = rustContext.getAllFiles();
			const fi = fileInfos['script.ts'];
			// console.dir(fi.rustObjects, {depth: null});
			expect(fi.rustObjects).toMatchObject({
				One: {
					type: 'alias',
					name: 'One',
					filePath: 'script.ts',
					aliasTypeName: '[i64; 1]',
				},
				Three: {
					type: 'alias',
					name: 'Three',
					filePath: 'script.ts',
					aliasTypeName: '(i64, String, i64)',
				},
				TupleNumberStringNumber265424: {
					type: 'alias',
					name: 'TupleNumberStringNumber265424',
					filePath: 'script.ts',
					aliasTypeName: '(i64, String, i64)',
				},
				TupleNumberStringNumber415640: {
					type: 'alias',
					name: 'TupleNumberStringNumber415640',
					filePath: 'script.ts',
					aliasTypeName: '(i64, String, i64, String, i64)',
				},
				Rest: {
					type: 'enum',
					name: 'Rest',
					filePath: 'script.ts',
					tag: undefined,
					props: [
						{
							name: 'TupleNumberStringNumber265424',
							typeName: 'TupleNumberStringNumber265424',
							isTypeParam: false,
							ignoreTypeName: false,
						},
						{
							name: 'TupleNumberStringNumber415640',
							typeName: 'TupleNumberStringNumber415640',
							isTypeParam: false,
							ignoreTypeName: false,
						},
					],
				},
				UnionOneThree124211: {
					type: 'enum',
					name: 'UnionOneThree124211',
					filePath: 'script.ts',
					tag: undefined,
					props: [
						{ name: 'One', typeName: 'One', isTypeParam: false, ignoreTypeName: false },
						{ name: 'Three', typeName: 'Three', isTypeParam: false, ignoreTypeName: false },
					],
				},
				Enum7ObjRest: {
					type: 'enum',
					name: 'Enum7ObjRest',
					filePath: 'script.ts',
					tag: undefined,
					props: [
						{ name: 'Number', typeName: 'i64', isTypeParam: false, ignoreTypeName: false },
						{ name: 'String', typeName: 'String', isTypeParam: false, ignoreTypeName: false },
						{
							name: 'UnionOneThree124211',
							typeName: 'UnionOneThree124211',
							isTypeParam: false,
							ignoreTypeName: false,
						},
					],
				},
				ObjRest: {
					type: 'alias',
					name: 'ObjRest',
					filePath: 'script.ts',
					aliasTypeName: 'Vec<Enum7ObjRest>',
				},
			});
		})
		
		it('complex tuples', () => {
			const {program, basePath} = createTypescriptProgramForCode(`
				export type Boo = [number; string]
				type Append<Item, Tuple extends unknown[]> = [Item, ...Tuple]
				type TupleN<Num extends number, T, TupleT extends T[] = []> = {
					current: TupleT
					next: TupleN<Num, T, Append<T, TupleT>>
				}[TupleT extends { length: Num } ? 'current' : 'next']
				type ArrayAtLeastN<
					T,
					N extends number = 1,
					Tuple = TupleN<N, T>,
				> = Tuple extends T[] ? [...Tuple, ...T[]] : never
				type Obj = {name: string; age: number}
				export type Hello = {
					hi: string;
					types: ArrayAtLeastN<{yolo: number}, 2>;
					other: [Obj; Obj];
				}
			`);
			const compiler = new CompilerApiHelper(program, basePath);
			const typesResult = compiler.extractTypes('script.ts');
			if (!isOk(typesResult)) {
				throw new TypeError('Types result is not ok');
			}
			expect(typesResult).toEqual({
				__type: 'ok',
				ok: {
					Boo: {
						typeName: 'Boo',
						typeText: 'Boo',
						filePath: 'script.ts',
						type: {
							__type: 'TupleTO',
							typeName: 'Boo',
							typeText: 'Boo',
							filePath: 'script.ts',
							hasRestItem: false,
							items: [
								{ __type: 'PrimitiveTO', kind: 'number' },
								{ __type: 'PrimitiveTO', kind: 'string' },
							],
						},
					},
					Hello: {
						typeName: 'Hello',
						typeText: 'Hello',
						filePath: 'script.ts',
						type: {
							__type: 'ObjectTO',
							typeName: 'Hello',
							typeText: 'Hello',
							filePath: 'script.ts',
							storeKey: 'Hello::script.ts',
							baseStoreKey: 'Hello::script.ts',
							typeArguments: undefined,
						},
					},
				},
			});
			
			const rustContext = new RustTypeContext(basePath, compiler);
			rustContext.addDeclarations(Object.values(typesResult.ok));
			
			const fileInfos = rustContext.getAllFiles();
			const fi = fileInfos['script.ts'];
			
			expect(fi.namedTypes).toHaveLength(7);
			expect(fi.importedTypes).toEqual({});
			expect(fi.declaredNames).toEqual(new Set(['Hello', 'Boo']));
			// console.dir(fi.rustObjects, {depth: null});
			expect(fi.rustObjects).toMatchObject({
				Boo: {
					type: 'alias',
					name: 'Boo',
					filePath: 'script.ts',
					aliasTypeName: '(i64, String)',
				},
				ObjectYoloNumber188517: {
					type: 'struct',
					name: 'ObjectYoloNumber188517',
					filePath: 'script.ts',
					props: [ { name: 'yolo', typeName: 'i64', isTypeParam: false } ],
				},
				Enum25TupleYoloNumberYolo574162: {
					type: 'enum',
					name: 'Enum25TupleYoloNumberYolo574162',
					filePath: 'script.ts',
					tag: undefined,
					props: [
						{
							name: 'ObjectYoloNumber188517',
							typeName: 'ObjectYoloNumber188517',
							isTypeParam: false,
							ignoreTypeName: false,
						},
					],
				},
				TupleYoloNumberYolo574162: {
					type: 'alias',
					name: 'TupleYoloNumberYolo574162',
					filePath: 'script.ts',
					aliasTypeName: 'Vec<Enum25TupleYoloNumberYolo574162>',
				},
				Obj: {
					type: 'struct',
					name: 'Obj',
					filePath: 'script.ts',
					props: [
						{ name: 'name', typeName: 'String', isTypeParam: false },
						{ name: 'age', typeName: 'i64', isTypeParam: false },
					],
				},
				TupleObjObj107010: {
					type: 'alias',
					name: 'TupleObjObj107010',
					filePath: 'script.ts',
					aliasTypeName: '(Obj, Obj)',
				},
				Hello: {
					type: 'struct',
					name: 'Hello',
					filePath: 'script.ts',
					props: [
						{ name: 'hi', typeName: 'String', isTypeParam: false },
						{
							name: 'types',
							typeName: 'TupleYoloNumberYolo574162',
							isTypeParam: false,
						},
						{
							name: 'other',
							typeName: 'TupleObjObj107010',
							isTypeParam: false,
						},
					],
				},
			});
		})
		
		it('simple arrays', () => {
			const {program, basePath} = createTypescriptProgramForCode(`
				export type Lol<T> = Array<T>[];
				export type Obj = {name: string[]; address: Lol<number>[];}
				export type Second = Lol<string>[];
			`);
			const compiler = new CompilerApiHelper(program, basePath);
			const typesResult = compiler.extractTypes('script.ts');
			if (!isOk(typesResult)) {
				throw new TypeError('Types result is not ok');
			}
			// console.dir(typesResult, {depth: null});
			
			const rustContext = new RustTypeContext(basePath, compiler);
			rustContext.addDeclarations(Object.values(typesResult.ok));
			
			const fileInfos = rustContext.getAllFiles();
			expect(Object.values(fileInfos)).toHaveLength(1);
			
			const fi = fileInfos['script.ts'];
			// console.dir(fi.rustObjects, {depth: null});
			expect(fi.rustObjects).toMatchObject({
				'Lol<T>': {
					type: 'alias',
					name: 'Lol<T>',
					filePath: 'script.ts',
					aliasTypeName: 'Vec<Vec<T>>',
				},
				Obj: {
					type: 'struct',
					name: 'Obj',
					filePath: 'script.ts',
					props: [
						{ name: 'name', typeName: 'Vec<String>', isTypeParam: false },
						{
							name: 'address',
							typeName: 'Vec<Lol<i64>>',
							isTypeParam: false,
						},
					],
				},
				Second: {
					type: 'alias',
					name: 'Second',
					filePath: 'script.ts',
					aliasTypeName: 'Vec<Lol<String>>',
				},
			});
		})
	})
	
	describe('enum types', () => {
		it('string value enum', () => {
			const {program, basePath} = createTypescriptProgramForCode(`
				export enum Country {
					us = 'us',
					ca = 'ca',
					uk = 'uk',
				}
			`);
			const compiler = new CompilerApiHelper(program, basePath);
			const typesResult = compiler.extractTypes('script.ts');
			if (!isOk(typesResult)) {
				throw new TypeError('Types result is not ok');
			}
			// console.dir(typesResult, {depth: null});
			
			const rustContext = new RustTypeContext(basePath, compiler);
			rustContext.addDeclarations(Object.values(typesResult.ok));
			
			const fileInfos = rustContext.getAllFiles();
			expect(Object.values(fileInfos)).toHaveLength(1);
			
			const fi = fileInfos['script.ts'];
			// console.dir(fi.rustObjects, {depth: null});
			expect(fi.rustObjects).toMatchObject({
				Country: {
					type: 'enum',
					name: 'Country',
					isNumeric: false,
					props: [
						{
							name: 'Us',
							typeName: '',
							serdeRename: 'us',
							ignoreTypeName: true,
							isTypeParam: false,
						},
						{
							name: 'Ca',
							typeName: '',
							serdeRename: 'ca',
							ignoreTypeName: true,
							isTypeParam: false,
						},
						{
							name: 'Uk',
							typeName: '',
							serdeRename: 'uk',
							ignoreTypeName: true,
							isTypeParam: false,
						},
					],
					filePath: 'script.ts',
				},
			});
		})
		
		it('default number value enum', () => {
			const {program, basePath} = createTypescriptProgramForCode(`
				export enum Num {
					zero,
					one,
					two,
				}
			`);
			const compiler = new CompilerApiHelper(program, basePath);
			const typesResult = compiler.extractTypes('script.ts');
			if (!isOk(typesResult)) {
				throw new TypeError('Types result is not ok');
			}
			
			const members = compiler.getStoredTypeMembers(typesResult.ok.Num.type as any);
			expect(members).toEqual([
				{ propName: 'zero', type: { __type: 'LiteralTO', value: 0 } },
				{ propName: 'one', type: { __type: 'LiteralTO', value: 1 } },
				{ propName: 'two', type: { __type: 'LiteralTO', value: 2 } },
			]);
			
			const rustContext = new RustTypeContext(basePath, compiler);
			rustContext.addDeclarations(Object.values(typesResult.ok));
			
			const fileInfos = rustContext.getAllFiles();
			expect(Object.values(fileInfos)).toHaveLength(1);
			
			const fi = fileInfos['script.ts'];
			// console.dir(fi.rustObjects, {depth: null});
			expect(fi.rustObjects).toMatchObject({
				Num: {
					type: 'enum',
					name: 'Num',
					isNumeric: true,
					props: [
						{
							name: 'Zero',
							typeName: '',
							serdeRename: '0',
							ignoreTypeName: true,
							isTypeParam: false,
						},
						{
							name: 'One',
							typeName: '',
							serdeRename: '1',
							ignoreTypeName: true,
							isTypeParam: false,
						},
						{
							name: 'Two',
							typeName: '',
							serdeRename: '2',
							ignoreTypeName: true,
							isTypeParam: false,
						},
					],
					filePath: 'script.ts',
				},
			});
		})
		
		it('manual number value enum', () => {
			const {program, basePath} = createTypescriptProgramForCode(`
				export enum Num {
					zero = 500,
					one = 12,
					two = 800,
				}
			`);
			const compiler = new CompilerApiHelper(program, basePath);
			const typesResult = compiler.extractTypes('script.ts');
			if (!isOk(typesResult)) {
				throw new TypeError('Types result is not ok');
			}
			
			const members = compiler.getStoredTypeMembers(typesResult.ok.Num.type as any);
			expect(members).toEqual([
				{ propName: 'zero', type: { __type: 'LiteralTO', value: 500 } },
				{ propName: 'one', type: { __type: 'LiteralTO', value: 12 } },
				{ propName: 'two', type: { __type: 'LiteralTO', value: 800 } },
			]);
			
			const rustContext = new RustTypeContext(basePath, compiler);
			rustContext.addDeclarations(Object.values(typesResult.ok));
			
			const fileInfos = rustContext.getAllFiles();
			expect(Object.values(fileInfos)).toHaveLength(1);
			
			const fi = fileInfos['script.ts'];
			// console.dir(fi.rustObjects, {depth: null});
			expect(fi.rustObjects).toMatchObject({
				Num: {
					type: 'enum',
					name: 'Num',
					isNumeric: true,
					props: [
						{
							name: 'Zero',
							typeName: '',
							serdeRename: '500',
							ignoreTypeName: true,
							isTypeParam: false,
						},
						{
							name: 'One',
							typeName: '',
							serdeRename: '12',
							ignoreTypeName: true,
							isTypeParam: false,
						},
						{
							name: 'Two',
							typeName: '',
							serdeRename: '800',
							ignoreTypeName: true,
							isTypeParam: false,
						},
					],
					filePath: 'script.ts',
				},
			});
		})
		
		it('mixed number and string value enum', () => {
			const {program, basePath} = createTypescriptProgramForCode(`
				export enum Num {
					two = 2,
					sweet = 'sweet',
					one = 1,
				}
			`);
			const compiler = new CompilerApiHelper(program, basePath);
			const typesResult = compiler.extractTypes('script.ts');
			if (!isOk(typesResult)) {
				throw new TypeError('Types result is not ok');
			}
			
			const members = compiler.getStoredTypeMembers(typesResult.ok.Num.type as any);
			expect(members).toEqual([
				{ propName: 'two', type: { __type: 'LiteralTO', value: 2 } },
				{ propName: 'sweet', type: { __type: 'LiteralTO', value: 'sweet' } },
				{ propName: 'one', type: { __type: 'LiteralTO', value: 1 } },
			]);
			
			const rustContext = new RustTypeContext(basePath, compiler);
			rustContext.addDeclarations(Object.values(typesResult.ok));
			
			const fileInfos = rustContext.getAllFiles();
			expect(Object.values(fileInfos)).toHaveLength(1);
			
			const fi = fileInfos['script.ts'];
			// console.dir(fi.rustObjects, {depth: null});
			expect(fi.rustObjects).toMatchObject({
				Num: {
					type: 'enum',
					name: 'Num',
					isNumeric: false,
					props: [
						{
							name: 'Two',
							typeName: '',
							serdeRename: '2',
							ignoreTypeName: true,
							isTypeParam: false,
						},
						{
							name: 'Sweet',
							typeName: '',
							serdeRename: 'sweet',
							ignoreTypeName: true,
							isTypeParam: false,
						},
						{
							name: 'One',
							typeName: '',
							serdeRename: '1',
							ignoreTypeName: true,
							isTypeParam: false,
						},
					],
					filePath: 'script.ts',
				},
			});
		})
	})
	
	describe('complex objects', () => {
		it('structs with index types', () => {
			const {program, basePath} = createTypescriptProgramForCode(`
				export type Yo = {
					[x in keyof First as \`Sway\${x}Hi\`]: symbol;
				}
				
				export interface First {
					primaryKey: string;
					relationship: string;
					index: string;
					unique: string;
					[x: number]: Swag;
				}
					
				interface Swag {
					[key: string]: boolean;
				}
			`);
			const compiler = new CompilerApiHelper(program, basePath);
			const typesResult = compiler.extractTypes('script.ts');
			if (!isOk(typesResult)) {
				throw new TypeError('Types result is not ok');
			}
			
			const rustContext = new RustTypeContext(basePath, compiler);
			rustContext.addDeclarations(Object.values(typesResult.ok));
			
			const fileInfos = rustContext.getAllFiles();
			expect(Object.values(fileInfos)).toHaveLength(1);
			 
			const fi = fileInfos['script.ts'];
			// console.dir(fi.rustObjects, {depth: null});
			expect(fi.rustObjects).toMatchObject({
				Yo: {
					type: 'struct',
					name: 'Yo',
					filePath: 'script.ts',
					props: [
						{
							name: 'swayprimary_key_hi',
							typeName: 'String',
							isTypeParam: false,
							isExtraMap: false,
						},
						{
							name: 'swayrelationship_hi',
							typeName: 'String',
							isTypeParam: false,
							isExtraMap: false,
						},
						{
							name: 'swayindex_hi',
							typeName: 'String',
							isTypeParam: false,
							isExtraMap: false,
						},
						{
							name: 'swayunique_hi',
							typeName: 'String',
							isTypeParam: false,
							isExtraMap: false,
						},
						{
							name: 'sway_number_hi_to_symbol',
							typeName: 'std::collections::HashMap<String, String>',
							isTypeParam: false,
							isExtraMap: true,
						},
					],
				},
				Swag: {
					type: 'alias',
					name: 'Swag',
					filePath: 'script.ts',
					aliasTypeName: 'std::collections::HashMap<String, bool>',
				},
				First: {
					type: 'struct',
					name: 'First',
					filePath: 'script.ts',
					props: [
						{
							name: 'primary_key',
							typeName: 'String',
							isTypeParam: false,
							isExtraMap: false,
						},
						{
							name: 'relationship',
							typeName: 'String',
							isTypeParam: false,
							isExtraMap: false,
						},
						{
							name: 'index',
							typeName: 'String',
							isTypeParam: false,
							isExtraMap: false,
						},
						{
							name: 'unique',
							typeName: 'String',
							isTypeParam: false,
							isExtraMap: false,
						},
						{
							name: 'number_to_swag',
							typeName: 'std::collections::HashMap<i64, Swag>',
							isTypeParam: false,
							isExtraMap: true,
						},
					],
				},
			});
		})
		
		it('nested objects', () => {
			const {program, basePath} = createTypescriptProgramForCode(`
				export interface SchemaSettingsData {
					schema: {
						mixinTables: {
							defaultMixinTableIds: string[];
						};
						generatorCode: {
							primaryKeyNameGeneratorCode: string;
							relationshipNameGeneratorCode: string;
							indexNameGeneratorCode: string;
							uniqueNameGeneratorCode: string;
						};
					};
					files: {
						saveFiles: {
							useSeparateSaveFiles: boolean;
						};
					};
					export: {
						sql: {
							type: symbol;
							fileNameTemplate: string;
							separateDirectionFolders: boolean;
							forwardFolderPath: string;
							backwardFolderPath: string;
						};
					};
				}
			`);
			const compiler = new CompilerApiHelper(program, basePath);
			const typesResult = compiler.extractTypes('script.ts');
			if (!isOk(typesResult)) {
				throw new TypeError('Types result is not ok');
			}
			
			const rustContext = new RustTypeContext(basePath, compiler);
			rustContext.addDeclarations(Object.values(typesResult.ok));
			
			const fileInfos = rustContext.getAllFiles();
			expect(Object.values(fileInfos)).toHaveLength(1);
			 
			const fi = fileInfos['script.ts'];
			expect(fi.declaredNames).toEqual(new Set(['SchemaSettingsData']));
			// console.dir(fi.rustObjects, {depth: null});
			expect(fi.rustObjects).toEqual({
				ObjectDefaultMixinTableIds372835: {
					type: 'struct',
					name: 'ObjectDefaultMixinTableIds372835',
					filePath: 'script.ts',
					props: [
						{
							name: 'default_mixin_table_ids',
							typeName: 'Vec<String>',
							isTypeParam: false,
							literal: undefined,
							isExtraMap: false,
						},
					],
				},
				ObjectPrimaryKeyNameGenera14498144: {
					type: 'struct',
					name: 'ObjectPrimaryKeyNameGenera14498144',
					filePath: 'script.ts',
					props: [
						{
							name: 'primary_key_name_generator_code',
							typeName: 'String',
							isTypeParam: false,
							literal: undefined,
							isExtraMap: false,
						},
						{
							name: 'relationship_name_generator_code',
							typeName: 'String',
							isTypeParam: false,
							literal: undefined,
							isExtraMap: false,
						},
						{
							name: 'index_name_generator_code',
							typeName: 'String',
							isTypeParam: false,
							literal: undefined,
							isExtraMap: false,
						},
						{
							name: 'unique_name_generator_code',
							typeName: 'String',
							isTypeParam: false,
							literal: undefined,
							isExtraMap: false,
						},
					],
				},
				ObjectMixinTablesDefaultM20960214: {
					type: 'struct',
					name: 'ObjectMixinTablesDefaultM20960214',
					filePath: 'script.ts',
					props: [
						{
							name: 'mixin_tables',
							typeName: 'ObjectDefaultMixinTableIds372835',
							isTypeParam: false,
							literal: undefined,
							isExtraMap: false,
						},
						{
							name: 'generator_code',
							typeName: 'ObjectPrimaryKeyNameGenera14498144',
							isTypeParam: false,
							literal: undefined,
							isExtraMap: false,
						},
					],
				},
				ObjectUseSeparateSaveFiles363634: {
					type: 'struct',
					name: 'ObjectUseSeparateSaveFiles363634',
					filePath: 'script.ts',
					props: [
						{
							name: 'use_separate_save_files',
							typeName: 'bool',
							isTypeParam: false,
							literal: undefined,
							isExtraMap: false,
						},
					],
				},
				ObjectSaveFilesUseSeparat504950: {
					type: 'struct',
					name: 'ObjectSaveFilesUseSeparat504950',
					filePath: 'script.ts',
					props: [
						{
							name: 'save_files',
							typeName: 'ObjectUseSeparateSaveFiles363634',
							isTypeParam: false,
							literal: undefined,
							isExtraMap: false,
						},
					],
				},
				ObjectTypeSymbolFileName13204133: {
					type: 'struct',
					name: 'ObjectTypeSymbolFileName13204133',
					filePath: 'script.ts',
					props: [
						{
							name: 'type',
							typeName: 'String',
							isTypeParam: false,
							literal: undefined,
							isExtraMap: false,
						},
						{
							name: 'file_name_template',
							typeName: 'String',
							isTypeParam: false,
							literal: undefined,
							isExtraMap: false,
						},
						{
							name: 'separate_direction_folders',
							typeName: 'bool',
							isTypeParam: false,
							literal: undefined,
							isExtraMap: false,
						},
						{
							name: 'forward_folder_path',
							typeName: 'String',
							isTypeParam: false,
							literal: undefined,
							isExtraMap: false,
						},
						{
							name: 'backward_folder_path',
							typeName: 'String',
							isTypeParam: false,
							literal: undefined,
							isExtraMap: false,
						},
					],
				},
				ObjectSqlTypeSymbol13993143: {
					type: 'struct',
					name: 'ObjectSqlTypeSymbol13993143',
					filePath: 'script.ts',
					props: [
						{
							name: 'sql',
							typeName: 'ObjectTypeSymbolFileName13204133',
							isTypeParam: false,
							literal: undefined,
							isExtraMap: false,
						},
					],
				},
				SchemaSettingsData: {
					type: 'struct',
					name: 'SchemaSettingsData',
					filePath: 'script.ts',
					props: [
						{
							name: 'schema',
							typeName: 'ObjectMixinTablesDefaultM20960214',
							isTypeParam: false,
							literal: undefined,
							isExtraMap: false,
						},
						{
							name: 'files',
							typeName: 'ObjectSaveFilesUseSeparat504950',
							isTypeParam: false,
							literal: undefined,
							isExtraMap: false,
						},
						{
							name: 'export',
							typeName: 'ObjectSqlTypeSymbol13993143',
							isTypeParam: false,
							literal: undefined,
							isExtraMap: false,
						},
					],
				},
			});
		})
		
		it('classes with extends', () => {
			const {program, basePath} = createTypescriptProgramForCode(`
				export class Base {
					name: string;
					
					constructor() {
						this.name = "John";
					}
				}

				export class Second extends Base {
					age: number = 0;
				}
			`);
			const compiler = new CompilerApiHelper(program, basePath);
			const typesResult = compiler.extractTypes('script.ts');
			if (!isOk(typesResult)) {
				throw new TypeError('Types result is not ok');
			}
			
			const rustContext = new RustTypeContext(basePath, compiler);
			rustContext.addDeclarations(Object.values(typesResult.ok));
			
			const fileInfos = rustContext.getAllFiles();
			expect(Object.values(fileInfos)).toHaveLength(1);
			 
			const fi = fileInfos['script.ts'];
			// console.dir(fi.rustObjects, {depth: null});
			expect(fi.rustObjects).toEqual({
				Base: {
					type: 'struct',
					name: 'Base',
					filePath: 'script.ts',
					props: [
						{
							name: 'name',
							typeName: 'String',
							isTypeParam: false,
							literal: undefined,
							isExtraMap: false,
						},
					],
				},
				Second: {
					type: 'struct',
					name: 'Second',
					filePath: 'script.ts',
					props: [
						{
							name: 'age',
							typeName: 'i64',
							isTypeParam: false,
							literal: undefined,
							isExtraMap: false,
						},
						{
							name: 'name',
							typeName: 'String',
							isTypeParam: false,
							literal: undefined,
							isExtraMap: false,
						},
					],
				},
			});
		})
		
		it('non-exported classes with extends', () => {
			const {program, basePath} = createTypescriptProgramForCode(`
				class Base {
					name: string;
					
					constructor() {
						this.name = "John";
					}
				}

				export class Second extends Base {
					age: number = 0;
				}
			`);
			const compiler = new CompilerApiHelper(program, basePath);
			const typesResult = compiler.extractTypes('script.ts');
			if (!isOk(typesResult)) {
				throw new TypeError('Types result is not ok');
			}
			
			const rustContext = new RustTypeContext(basePath, compiler);
			rustContext.addDeclarations(Object.values(typesResult.ok));
			
			const fileInfos = rustContext.getAllFiles();
			expect(Object.values(fileInfos)).toHaveLength(1);
			 
			const fi = fileInfos['script.ts'];
			// console.dir(fi.rustObjects, {depth: null});
			expect(fi.rustObjects).toEqual({
				Second: {
					type: 'struct',
					name: 'Second',
					filePath: 'script.ts',
					props: [
						{
							name: 'age',
							typeName: 'i64',
							isTypeParam: false,
							literal: undefined,
							isExtraMap: false,
						},
						{
							name: 'name',
							typeName: 'String',
							isTypeParam: false,
							literal: undefined,
							isExtraMap: false,
						},
					],
				},
			});
		})
		
		it('derived object values', () => {
			const {program, basePath} = createTypescriptProgramForCode(`
				export enum SqlExportType {
					sqlite = "sqlite",
					postgres = "postgres",
					mysql = "mysql",
				}

				export const sqlExportDisplayName = {
					[SqlExportType.sqlite]: "SQLite",
					[SqlExportType.postgres]: "Postgres",
					[SqlExportType.mysql]: "MySQL",
				} as const;

				export const sqlExportSelectData = Object.entries(SqlExportType).map(([k, v]) => ({
					label: k,
					value: v,
				}));
			`);
			const compiler = new CompilerApiHelper(program, basePath);
			const typesResult = compiler.extractTypes('script.ts', {variableStatement: true});
			if (!isOk(typesResult)) {
				throw new TypeError('Types result is not ok');
			}
			
			const rustContext = new RustTypeContext(basePath, compiler);
			rustContext.addDeclarations(Object.values(typesResult.ok));
			
			const fileInfos = rustContext.getAllFiles();
			expect(Object.values(fileInfos)).toHaveLength(1);
			 
			const fi = fileInfos['script.ts'];
			expect(fi.rustObjects).toEqual({
				SqlExportType: {
					type: 'enum',
					name: 'SqlExportType',
					isNumeric: false,
					props: [
						{
							name: 'Sqlite',
							typeName: '',
							serdeRename: 'sqlite',
							ignoreTypeName: true,
							isTypeParam: false,
						},
						{
							name: 'Postgres',
							typeName: '',
							serdeRename: 'postgres',
							ignoreTypeName: true,
							isTypeParam: false,
						},
						{
							name: 'Mysql',
							typeName: '',
							serdeRename: 'mysql',
							ignoreTypeName: true,
							isTypeParam: false,
						},
					],
					filePath: 'script.ts'
				},
				sqlExportDisplayName: {
					type: 'struct',
					name: 'sqlExportDisplayName',
					filePath: 'script.ts',
					props: [
						{
							name: 'sqlite',
							typeName: 'String',
							isTypeParam: false,
							literal: 'SQLite',
							isExtraMap: false,
						},
						{
							name: 'postgres',
							typeName: 'String',
							isTypeParam: false,
							literal: 'Postgres',
							isExtraMap: false,
						},
						{
							name: 'mysql',
							typeName: 'String',
							isTypeParam: false,
							literal: 'MySQL',
							isExtraMap: false,
						},
					],
				},
				ObjectLabelStringValue410740: {
					type: 'struct',
					name: 'ObjectLabelStringValue410740',
					filePath: 'script.ts',
					props: [
						{
							name: 'label',
							typeName: 'String',
							isTypeParam: false,
							literal: undefined,
							isExtraMap: false,
						},
						{
							name: 'value',
							typeName: 'SqlExportType',
							isTypeParam: false,
							literal: undefined,
							isExtraMap: false,
						},
					],
				},
				sqlExportSelectData: {
					type: 'alias',
					name: 'sqlExportSelectData',
					filePath: 'script.ts',
					aliasTypeName: 'Vec<ObjectLabelStringValue410740>',
				},
			});
		})
	})
	
	describe('load real files', () => {
		it('processes import types', () => {
			const {program, basePath} = createTypescriptProgram();
			const compiler = new CompilerApiHelper(program, basePath);
			const typesResult = compiler.extractTypes(
				codeFileAbsolutePath('./src/examples/example.ts'),
				{typeAliasDeclaration: false},
			);
			if (!isOk(typesResult)) {
				throw new TypeError('Unexpected file');
			}
			const rustContext = new RustTypeContext(basePath, compiler);
			rustContext.addDeclarations(Object.values(typesResult.ok));
			
			const fileInfos = rustContext.getAllFiles();
			// expect(Object.values(fileInfos)).toHaveLength(2);
			 
			const fi1 = fileInfos['/src/examples/example.ts'];
			// console.dir(fileInfos, {depth: null});
			
			expect(fi1.rustObjects).toEqual({
				UnionGenericDataAppleAr283628: {
					type: 'enum',
					name: 'UnionGenericDataAppleAr283628',
					filePath: '/src/examples/example.ts',
					tag: undefined,
					props: [
						{
							name: 'GenericData',
							serdeRename: undefined,
							typeName: 'GenericData<String>',
							isTypeParam: false,
							ignoreTypeName: false,
						},
						{
							name: 'Array',
							serdeRename: undefined,
							typeName: 'Array',
							isTypeParam: false,
							ignoreTypeName: false,
						},
					],
				},
				ErrorState: {
					type: 'struct',
					name: 'ErrorState',
					filePath: '/src/examples/example.ts',
					props: [
						{
							name: 'state',
							typeName: 'Box<dyn std::any::Any>',
							isTypeParam: false,
							literal: undefined,
							isExtraMap: false,
						},
						{
							name: 'number_to_union_generic_data_apple_ar283628',
							typeName: 'std::collections::HashMap<i64, UnionGenericDataAppleAr283628>',
							isTypeParam: false,
							literal: undefined,
							isExtraMap: true,
						},
					],
				},
			});
			expect(fi1.importedTypes).toEqual({
				'GenericData<String>': new Set(['/src/examples/import-me.ts']),
				Array: new Set(['/src/examples/import-me.ts']),
			});
			expect(fi1.namedTypes).toEqual(new Set(['ErrorState', 'UnionGenericDataAppleAr283628']));
			expect(fi1.declaredNames).toEqual(new Set(['ErrorState']));
			
			const fi2 = fileInfos['/src/examples/import-me.ts'];
			
			expect(fi2.rustObjects).toMatchObject({
				Array: {
					type: 'alias',
					name: 'Array',
					filePath: '/src/examples/import-me.ts',
					aliasTypeName: '(i64, i64)',
				},
				'GenericData<T>': {
					type: 'struct',
					name: 'GenericData<T>',
					filePath: '/src/examples/import-me.ts',
					props: [
						{
							name: 'message',
							typeName: 'T',
							isTypeParam: true,
							literal: undefined,
							isExtraMap: false,
						},
						{
							name: 'hello',
							typeName: 'Array',
							isTypeParam: false,
							literal: undefined,
							isExtraMap: false,
						},
					],
				},
			});
			expect(fi2.importedTypes).toEqual({});
			expect(fi2.namedTypes).toEqual(new Set(['GenericData<T>', 'Array']));
			expect(fi2.declaredNames).toEqual(new Set());
		})
		
		it('adds more types to existing file', () => {
			const {program, basePath} = createTypescriptProgram(codeFileAbsolutePath('../frontend/tsconfig.json'));
			const compiler = new CompilerApiHelper(program, basePath);
			let typesResult = compiler.extractTypes(
				codeFileAbsolutePath('../frontend/src/data/schema/schema-setting-data.ts'),
				{variableStatement: true},
			);
			if (!isOk(typesResult)) {
				throw new TypeError('Unexpected file');
			}
			const rustContext = new RustTypeContext(basePath, compiler);
			rustContext.addDeclarations(Object.values(typesResult.ok));
			
			const fileInfos = rustContext.getAllFiles();
			expect(Object.values(fileInfos)).toHaveLength(2);
			
			const file1 = fileInfos['/src/data/schema/schema-setting-data.ts'];
			const file2 = fileInfos['/src/data/export/export-enums.ts'];
			
			expect(file1.declaredNames).toEqual(new Set([
				'SchemaSettingsData',
				'SchemaSettingsDataPath',
				'schemaSpecificSettingsPath',
			]));
			expect(file1.importedTypes).toEqual({
				SqlExportType: new Set([
					'/src/data/export/export-enums.ts',
				]),
			});
			// imports show no declared names because the file hasn't been proccessed
			expect(file2.declaredNames).toEqual(new Set());
			expect(file2.importedTypes).toEqual({});
			
			typesResult = compiler.extractTypes(
				codeFileAbsolutePath('../frontend/src/data/export/export-enums.ts'),
				{variableStatement: true},
			);
			if (!isOk(typesResult)) {
				throw new TypeError('Unexpected file');
			}
			rustContext.addDeclarations(Object.values(typesResult.ok));
			
			expect(Object.values(rustContext.getAllFiles())).toHaveLength(2);
			expect(file1.declaredNames).toEqual(new Set([
				'SchemaSettingsData',
				'SchemaSettingsDataPath',
				'schemaSpecificSettingsPath',
			]));
			expect(file1.importedTypes).toEqual({
				SqlExportType: new Set([
					'/src/data/export/export-enums.ts',
				]),
			});
			expect(file2.declaredNames).toEqual(new Set([
				'SqlExportType',
				'sqlExportDisplayName',
				'sqlExportSelectData',
			]));
			expect(file2.importedTypes).toEqual({});
		})
		
		it('big derived zod type', () => {
			const {program, basePath} = createTypescriptProgram(codeFileAbsolutePath('../frontend/tsconfig.json'));
			const compiler = new CompilerApiHelper(program, basePath);
			const typesResult = compiler.extractTypes(
				codeFileAbsolutePath('../frontend/src/data/state/settings-state.ts'),
			);
			if (!isOk(typesResult)) {
				throw new TypeError('Unexpected file');
			}
			const rustContext = new RustTypeContext(basePath, compiler);
			rustContext.addDeclarations(Object.values(typesResult.ok));
			
			const fileInfos = rustContext.getAllFiles();
			const file = fileInfos['/src/data/state/settings-state.ts'];
			
			expect(file.declaredNames).toEqual(new Set([
				'SettingsState',
				'SettingsStatePath',
				'SettingsStateArrayPath',
			]));
			const namedTypes = [...file.namedTypes];
			expect(namedTypes.length).toBeGreaterThan(3);
			
			const textFormattingEnum = namedTypes.find(x => x.includes('TextFormatting'));
			const relationshipEnum = namedTypes.find(x => x.includes('RelationshipConnect'));
			expect(textFormattingEnum).toBeTruthy();
			expect(relationshipEnum).toBeTruthy();
		})
	})
})