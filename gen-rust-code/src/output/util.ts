import * as changeCase from "change-case";
import { isNamedTypeObject, type NamedTypeObject, type TypeObject } from "../ts-input/type-object";

export const CASE_OPTIONS: changeCase.Options = {
	prefixCharacters: '_',
	suffixCharacters: '_',
};

type Options = {
	case?: 'snakeCase' | 'pascalCase';
	forceNewName?: boolean;
}

export function generateIdentifierForType(type: TypeObject, options: Options): string {
	let isInvalid = false;
	let name = '';
	if (isNamedTypeObject(type)) {
		name = type.typeName ?? type.typeText;
		
		if (options.forceNewName || typeNeedsNewName(type)) {
			const text = type.typeText;
			const filtered = text.split(/\W+/g)
				.filter(x => x.length > 1)
				.slice(0, 3)
				.join('_')
				.slice(0, 20);
			const num = hashString(text);
			const part = '_' + (filtered || 'X') + num;
			name = (type.__type.slice(0, -2) + part) + text.length;
		}
	}
	else {
		switch (type.__type) {
			case 'IndexTO': {
				name = `${generateIdentifierForType(type.keyType, options)}_to_${generateIdentifierForType(type.valueType, options)}`;
				break;
			}
			case 'LiteralTO': {
				name = String(type.value);
				break;
			}
			case 'PrimitiveTO':
			case 'SpecialTO': {
				name = type.kind;
				break;
			}
			case 'UnsupportedTO': {
				name = type.typeText ?? type.kind;
				break;
			}
			case 'CallableTO':
			case 'PromiseTO':
				name = 'invalid';
				isInvalid = true;
				break;
		}
	}
	
	if (options.case) {
		name = changeCase[options.case](name, CASE_OPTIONS);
	}
	
	if (isInvalid) {
		name = `!${name}!`;
	}
	
	return name;
}

export function typeNeedsNewName(type: NamedTypeObject): boolean {
	if (!type.typeName || type.typeName.startsWith('__')) {
		return true;
	}
	
	const name = type.typeName;
	return name.startsWith('{') || name.startsWith('[') || name.startsWith('readonly [');
}

export function hashString(text: string): string {
	let num = 0;
	for (let i = 0; i < text.length; ++i) {
		const code = text.charCodeAt(i);
		num += code + Math.trunc(code / (i + 1));
	}
	
	return num.toString();
}