$script_dir = $PSScriptRoot
$frontend_project_dir = "$script_dir\frontend"
$frontend_build_dir = "$frontend_project_dir\dist"
$frontend_build_destination = "$script_dir\desktop\frontend_build"
$desktop_project_dir = "$script_dir\desktop"


Write-Host 'Building frontend project'
# Build frontend project
Set-Location $frontend_project_dir
npm run build

Write-Host 'Copying files'
# If a previous build of the frontend is in the desktop build location,
# delete it
if (Test-Path $frontend_build_destination) {
	Remove-Item $frontend_build_destination -Recurse
}

Copy-Item $frontend_build_dir $frontend_build_destination -Recurse -Force

if ($args[0] -eq "production") {
	Remove-Item "$frontend_build_destination\*" -Recurse -Include *.map
}

# Build desktop project
Write-Host 'Building desktop project'
Set-Location $desktop_project_dir
cargo tauri build

Write-Host 'Done'