use std::collections::HashMap;
use crate::code_executor::code_container::{CodeContainer, CodeContainerType};
use crate::util::item_id::ItemId;

pub fn create_struct_outputs() -> HashMap<ItemId, CodeContainer> {
    let cpp = CodeContainer::new(
        "cpp".into(),
        include_str!("output_scripts/cpp.js").into(),
        CodeContainerType::StructOutput
    );
    
    let csharp = CodeContainer::new(
        "csharp".into(),
        include_str!("output_scripts/csharp.js").into(),
        CodeContainerType::StructOutput
    );
    
    let java = CodeContainer::new(
        "java".into(),
        include_str!("output_scripts/java.js").into(),
        CodeContainerType::StructOutput
    );
    
    let javascript = CodeContainer::new(
        "javascript".into(),
        include_str!("output_scripts/javascript.js").into(),
        CodeContainerType::StructOutput
    );
    
    let typescript = CodeContainer::new(
        "typescript".into(),
        include_str!("output_scripts/typescript.js").into(),
        CodeContainerType::StructOutput
    );
    
    let rust = CodeContainer::new(
        "rust".into(),
        include_str!("output_scripts/rust.js").into(),
        CodeContainerType::StructOutput
    );
    
    let python = CodeContainer::new(
        "python".into(),
        include_str!("output_scripts/python.js").into(),
        CodeContainerType::StructOutput
    );
    
    HashMap::from([
        (cpp.id.clone(), cpp),
        (csharp.id.clone(), csharp),
        (java.id.clone(), java),
        (javascript.id.clone(), javascript),
        (typescript.id.clone(), typescript),
        (rust.id.clone(), rust),
        (python.id.clone(), python),
    ])
}