const langName = "csharp";
const reservedKeywords = new Set([
    "abstract", "as", "base", "bool", "break", "byte", "case", "catch", "char", "checked",
    "class", "const", "continue", "decimal", "default", "delegate", "do", "double", "else", "enum",
    "event", "explicit", "extern", "false", "finally", "fixed", "float", "for", "foreach", "goto",
    "if", "implicit", "in", "int", "interface", "internal", "is", "lock", "long", "namespace",
    "new", "null", "object", "operator", "out", "override", "params", "private", "protected", "public",
    "readonly", "ref", "return", "sbyte", "sealed", "short", "sizeof", "stackalloc", "static", "string",
    "struct", "switch", "this", "throw", "true", "try", "typeof", "uint", "ulong", "unchecked",
    "unsafe", "ushort", "using", "virtual", "void", "volatile", "while"
]);
const { name, singleFileOutput } = data;
const {
    classNamePrefix,
    classNameSuffix,
    variablesNotProperties,
    propertyInitNotSet,
    makeFieldsVirtual,
    objectKeyword,
    useRecordShorthand,
    addConstructor,
} = data.options;

const classNameConvert = convertCase.pascalCase;
const variableNameConvert = variablesNotProperties ? convertCase.camelCase : convertCase.pascalCase;
const fileNameConvert = classNameConvert;

const columnInfos = columns.map(columnInfo);

function getFileName() {
    return fileNameConvert(singleFileOutput ? name : table.name) + ".cs";
}

function generate() {
    const firstPart = `public ${objectKeyword} ${genClassName()}`;

    if (useRecordShorthand && objectKeyword.includes("record")) {
        return `${firstPart}(${columns.map(c => recordParam(c)).join(", ")});`;
    }

    const cons = addConstructor ? genConstructor() : "";

    return `${firstPart} {
${columnInfos.map(c => c.declaration).join("\n")}${cons}
}`.replaceAll("\r", "");
}

function genClassName() {
    return classNamePrefix + classNameConvert(table.name) + classNameSuffix;
}

function genConstructor() {
    const parameters = [];
    const assignments = [];
    for (const colInfo of columnInfos) {
        let camelName = convertCase.camelCase(colInfo.name);
        if (reservedKeywords.has(camelName)) {
            camelName += "_";
        }
        assignments.push(`        this.${colInfo.name} = ${camelName};`);
        parameters.push(`${colInfo.type} ${camelName}`);
    }

    return `
    public ${genClassName()}(${parameters.join(", ")}) {
${assignments.join("\n")}
    }`;
}

function columnInfo(column) {
    const lang = column.type?.languages.find(x => x.name === langName);
    if (!lang) {
        throw new Error(`Column "${column.name}" on table "${table.name}" does not have a type with a language named "${langName}".`)
    }

    let varName = variableNameConvert(column.name);
    if (reservedKeywords.has(varName)) {
        varName += "_";
    }
    const firstPart = `    public ${makeFieldsVirtual ? "virtual " : ""}${lang.value} ${varName}`;

    return {
        name: varName,
        type: lang.value,
        declaration: firstPart + (
            variablesNotProperties
                ? ";"
                : ` { get; ${propertyInitNotSet ? "init" : "set"}; }`
        ),
    };
}

function recordParam(column) {
    const lang = column.type?.languages.find(x => x.name === langName);
    if (!lang) {
        throw new Error(`Column "${column.name}" on table "${table.name}" does not have a type with a language named "${langName}".`)
    }

    return `${lang.value} ${convertCase.pascalCase(column.name)}`;
}
