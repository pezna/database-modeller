const langName = "java";
const reservedKeywords = new Set([
    "abstract", "assert", "boolean", "break", "byte", "case", "catch", "char", "class", "const",
    "continue", "default", "do", "double", "else", "enum", "extends", "final", "finally", "float",
    "for", "goto", "if", "implements", "import", "instanceof", "int", "interface", "long", "native",
    "new", "package", "private", "protected", "public", "return", "short", "static", "strictfp", "super",
    "switch", "synchronized", "this", "throw", "throws", "transient", "try", "void", "volatile", "while"
]);
const { name, singleFileOutput } = data;
const {
    classNamePrefix,
    classNameSuffix,
    objectKeyword,
    makeStatic,
    addConstructor,
} = data.options;

const classNameConvert = convertCase.pascalCase;
const variableNameConvert = convertCase.camelCase;
const fileNameConvert = classNameConvert;
const isRecord = objectKeyword === "record";

const columnInfos = columns.map(columnInfo);

function getFileName() {
    return fileNameConvert(genClassName(singleFileOutput ? name : table.name)) + ".java";
}

function generate() {
    const firstPart = `public ${makeStatic ? "static " : ""}${objectKeyword} ${genClassName(table.name)}`;

    if (isRecord) {
        return `${firstPart}(${columnInfos.map(c => c.str).join(", ")}) {}`;
    }

    const cons = addConstructor ? genConstructor() : "";

    return `${firstPart} {
${columnInfos.map(c => `    public ${c.str};`).join("\n")}${cons}
}`.replaceAll("\r", "");
}

function genClassName(name) {
    return singleFileOutput
        ? classNameConvert(name)
        : classNamePrefix + classNameConvert(name) + classNameSuffix;
}

function genConstructor() {
    const parameters = [];
    const assignments = [];
    for (const colInfo of columnInfos) {
        assignments.push(`        this.${colInfo.name} = ${colInfo.name};`);
        parameters.push(colInfo.str);
    }

    return `
    public ${genClassName(table.name)}(${parameters.join(", ")}) {
${assignments.join("\n")}
    }`;
}

function columnInfo(column) {
    const lang = column.type?.languages.find(x => x.name === langName);
    if (!lang) {
        throw new Error(`Column "${column.name}" on table "${table.name}" does not have a type with a language named "${langName}".`)
    }

    let name = variableNameConvert(column.name);
    if (reservedKeywords.has(name)) {
        name += "_";
    }

    return {
        name,
        type: lang.value,
        str: `${lang.value} ${name}`,
    };
}
