//const langName = "javascript";
const reservedKeywords = new Set([
    "await", "break", "case", "catch", "class", "const", "continue", "debugger", "default", "delete",
    "do", "else", "export", "extends", "finally", "for", "function", "if", "import", "in",
    "instanceof", "new", "return", "super", "switch", "this", "throw", "try", "typeof", "var",
    "void", "while", "with", "yield"
]);
const noChange = (x) => x;
const { name, singleFileOutput } = data;
const {
    classNamePrefix,
    classNameSuffix,
    variableCase,
    fileNameCase,
    fileNameSuffix,
    addConstructor,
} = data.options;

const classNameConvert = convertCase.pascalCase;
const variableNameConvert = variableCase ? convertCase[variableCase] : noChange;
const fileNameConvert = fileNameCase ? convertCase[fileNameCase] : noChange;

const columnInfos = columns.map(columnInfo);

function getFileName() {
    return fileNameConvert(singleFileOutput ? name : table.name) + fileNameSuffix + ".js";
}

function generate() {
    const cons = addConstructor ? genConstructor() : "";
    return `export class ${genClassName(table.name)} {
${columnInfos.map(c => `    ${c.name};`).join("\n")}${cons}
}`.replaceAll("\r", "");
}

function genConstructor() {
    const parameters = [];
    const assignments = [];
    for (const colInfo of columnInfos) {
        assignments.push(`        this.${colInfo.name} = ${colInfo.name};`);
        parameters.push(colInfo.name);
    }

    return `
    constructor(${parameters.join(", ")}) {
${assignments.join("\n")}
    }`;
}

function columnInfo(column) {
    let name = variableNameConvert(column.name);
    if (reservedKeywords.has(name)) {
        name += "_";
    }

    return {
        name,
    };
}

function genClassName(name) {
    return classNamePrefix + classNameConvert(name) + classNameSuffix;
}
