const langName = "python";
const reservedKeywords = new Set([
    "False", "None", "True", "and", "as", "assert", "async", "await", "break", "class",
    "continue", "def", "del", "elif", "else", "except", "finally", "for", "from", "global",
    "if", "import", "in", "is", "lambda", "nonlocal", "not", "or", "pass", "raise",
    "return", "try", "while", "with", "yield"
]);
const { name, singleFileOutput } = data;
const {
    classNamePrefix,
    classNameSuffix,
    fileNameSuffix,
    makeDataclass,
} = data.options;

const columnInfos = columns.map(columnInfo);

function getFileName() {
    return convertCase.snakeCase(singleFileOutput ? name : table.name) + fileNameSuffix + ".py";
}

function generate() {
    return (
        makeDataclass
        ? genDataclass()
        : genNormalClass()
    ).replaceAll("\r", "");
}

function genDataclass() {
    return `@dataclass\nclass ${genClassName(table.name)}:
${columnInfos.map(c => "    " + c.str).join("\n")}
`;
}

function genNormalClass() {
    return `class ${genClassName(table.name)}:
${genConstructor()}`;
}

function genConstructor() {
    const parameters = [];
    const assignments = [];
    for (const colInfo of columnInfos) {
        assignments.push(`        self.${colInfo.name} = ${colInfo.name}`);
        parameters.push(colInfo.str);
    }
    return `    def __init__(self, ${parameters.join(", ")}):
${assignments.join("\n")}
`;
}

function columnInfo(column) {
    const lang = column.type?.languages.find(x => x.name === langName);
    if (!lang) {
        throw new Error(`Column "${column.name}" on table "${table.name}" does not have a type with a language named "${langName}".`)
    }
    let name = convertCase.snakeCase(column.name);
    if (reservedKeywords.has(name)) {
        name += "_";
    }

    return {
        name,
        type: lang.value,
        str: `${name}: ${lang.value}`,
    };
}

function genClassName(name) {
    return classNamePrefix + convertCase.pascalCase(name) + classNameSuffix;
}
