const langName = "rust";
const reservedKeywords = new Set([
    "as", "break", "const", "continue", "crate", "dyn", "else", "enum", "extern", "false",
    "fn", "for", "if", "impl", "in", "let", "loop", "match", "mod", "move",
    "mut", "pub", "ref", "return", "self", "Self", "static", "struct", "super", "trait",
    "true", "type", "unsafe", "use", "where", "while", "async", "await", "try"
]);
const { name, singleFileOutput } = data;
const {
    structNamePrefix,
    structNameSuffix,
    fileNameSuffix,
    addConstructor,
} = data.options;

const columnInfos = columns.map(columnOutput);

function getFileName() {
    return convertCase.snakeCase(singleFileOutput ? name : table.name) + fileNameSuffix + ".rs";
}

function generate() {
    const struct = `pub struct ${genClassName(table.name)} {
${columnInfos.map(c => `    pub ${c.str},`).join("\n")}
}`.replaceAll("\r", "");
    if (addConstructor) {
        return struct + "\n\n" + genConstructor();
    }

    return struct;
}

function genConstructor() {
    return `
impl ${genClassName(table.name)} {
    pub fn new(${columnInfos.map(c => c.str).join(", ")}) -> Self {
        Self {
${columnInfos.map(c => "            " + c.name + ",").join("\n")}
        }
    }
}`.replaceAll("\r", "");
}

function columnOutput(column) {
    const lang = column.type?.languages.find(x => x.name === langName);
    if (!lang) {
        throw new Error(`Column "${column.name}" on table "${table.name}" does not have a type with a language named "${langName}".`)
    }
    let name = convertCase.snakeCase(column.name);
    if (reservedKeywords.has(name)) {
        name += "_";
    }

    return {
        name,
        type: lang.value,
        str: `${name}: ${lang.value}`,
    };
}

function genClassName(name) {
    return structNamePrefix + convertCase.pascalCase(name) + structNameSuffix;
}