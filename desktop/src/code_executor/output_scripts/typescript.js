const langName = "typescript";
const reservedKeywords = new Set([
    "await", "break", "case", "catch", "class", "const", "continue", "debugger", "default", "delete",
    "do", "else", "export", "extends", "finally", "for", "function", "if", "import", "in",
    "instanceof", "new", "return", "super", "switch", "this", "throw", "try", "typeof", "var",
    "void", "while", "with", "yield",
    "type", "interface", "extends", "implements", "private", "protected", "public", "readonly", "abstract", "declare",
    "enum", "namespace", "module", "export", "import", "any", "number", "boolean", "string", "symbol", "never", "void",
    "unknown", "as", "is", "keyof", "typeof", "infer"
]);
const noChange = (x) => x;
const { name, singleFileOutput } = data;
const {
    classNamePrefix,
    classNameSuffix,
    variableCase,
    fileNameCase,
    fileNameSuffix,
    objectKeyword,
} = data.options;

const classNameConvert = convertCase.pascalCase;
const variableNameConvert = variableCase ? convertCase[variableCase] : noChange;
const fileNameConvert = fileNameCase ? convertCase[fileNameCase] : noChange;

const columnInfos = columns.map(columnInfo);

function getFileName() {
    return fileNameConvert(singleFileOutput ? name : table.name) + fileNameSuffix + ".ts";
}

function generate() {
    const firstPart = getObjectDeclarationPart();
    const cons = objectKeyword === "class" ? genConstructor() : "";
    return `${firstPart}
${columnInfos.map(c => `    ${c.str};`).join("\n")}${cons}
}`.replaceAll("\r", "");
}

function getObjectDeclarationPart() {
    const tableName = genClassName(table.name);
    if (objectKeyword === "interface") {
        return `export interface ${tableName} {`;
    }
    else if (objectKeyword === "type") {
        return `export type ${tableName} = {`;
    }
    else if (objectKeyword === "class") {
        return `export class ${tableName} {`;
    }

    throw new Error(`Expected object keyword to be interface, type, or class; not ${objectKeyword}. This should never happen.`);
}

function genConstructor() {
    const parameters = [];
    const assignments = [];
    for (const colInfo of columnInfos) {
        assignments.push(`        this.${colInfo.name} = ${colInfo.name};`);
        parameters.push(colInfo.str);
    }

    return `
    constructor(${parameters.join(", ")}) {
${assignments.join("\n")}
    }`;
}

function columnInfo(column) {
    const lang = column.type?.languages.find(x => x.name === langName);
    if (!lang) {
        throw new Error(`Column "${column.name}" on table "${table.name}" does not have a type with a language named "${langName}".`)
    }
    let name = variableNameConvert(column.name);
    if (reservedKeywords.has(name)) {
        name += "_";
    }

    return {
        name,
        type: lang.value,
        str: `${name}: ${lang.value}`,
    };
}

function genClassName(name) {
    return classNamePrefix + classNameConvert(name) + classNameSuffix;
}

