const langName = "cpp";
const reservedKeywords = new Set([
    "alignas", "alignof", "asm", "auto", "bool", "break", "case", "catch", "char",
    "class", "const", "constexpr", "const_cast", "continue", "decltype", "default",
    "delete", "do", "double", "dynamic_cast", "else", "enum", "explicit", "export",
    "extern", "false", "float", "for", "friend", "goto", "if", "inline", "int",
    "long", "mutable", "namespace", "new", "noexcept", "nullptr", "operator",
    "private", "protected", "public", "register", "reinterpret_cast", "return",
    "short", "signed", "sizeof", "static", "static_assert", "static_cast",
    "struct", "switch", "template", "this", "thread_local", "throw", "true",
    "try", "typedef", "typeid", "typename", "union", "unsigned", "using",
    "virtual", "void", "volatile", "wchar_t", "while"
]);
const noChange = (x) => x;
const { name, singleFileOutput } = data;
const {
    classNamePrefix,
    classNameSuffix,
    classNameCase,
    variableCase,
    fileNameCase,
    fileNameSuffix,
    fileExtension,
    makeStruct,
    addConstructor,
} = data.options;

const classNameConvert = classNameCase ? convertCase[classNameCase] : noChange;
const variableNameConvert = variableCase ? convertCase[variableCase] : noChange;
const fileNameConvert = fileNameCase ? convertCase[fileNameCase] : noChange;

const columnInfos = columns.map(columnInfo);

function getFileName() {
    return fileNameConvert(singleFileOutput ? name : table.name) + fileNameSuffix + "." + fileExtension;
}

function generate() {
    const cons = addConstructor ? genConstructor() : "";

    return `${makeStruct ? "struct" : "class"} ${genClassName()} {${makeStruct ? "" : "\npublic:"}
${columnInfos.map(c => `    ${c.str};`).join("\n")}${cons}
}`.replaceAll("\r", "");
}

function genConstructor() {
    const parameters = [];
    const assignments = [];
    for (const colInfo of columnInfos) {
        assignments.push(`        ${colInfo.name}(${colInfo.name}_)`);
        parameters.push(`${colInfo.type} ${colInfo.name}_`);
    }
    const className = genClassName();

    return `
    ${className}(${parameters.join(", ")}):
${assignments.join(",\n")}
    {}
    ~${className}() = default;`;
}

function columnInfo(column) {
    const lang = column.type?.languages.find(x => x.name === langName);
    if (!lang) {
        throw new Error(`Column "${column.name}" on table "${table.name}" does not have a type with a language named "${langName}".`)
    }
    let name = variableNameConvert(column.name);
    if (reservedKeywords.has(name)) {
        name += "_";
    }

    return {
        name,
        type: lang.value,
        str: `${lang.value} ${name}`,
    };
}

function genClassName() {
    return classNamePrefix + classNameConvert(table.name) + classNameSuffix;
}
