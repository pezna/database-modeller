use serde::{Deserialize, Serialize};
use ts_rs::TS;
use crate::code_executor::error::{report_exceptions, CodeError};
use crate::js_string;
use crate::util::item_id::ItemId;

#[derive(TS, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./code-service-data.ts", export)]
pub struct RunCodeContainer {
    pub id: ItemId,
    pub name_options: Option<RunNameGenOptions>,
    pub export_options: Option<RunCustomExportOptions>,
    pub struct_output_options: Option<RunStructOutputOptions>,
}

#[derive(TS, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./code-service-data.ts", export)]
pub struct RunNameGenOptions {
    // string ID in quotes. ex: "id_here"
    pub first_table_id: Option<ItemId>,
    // JSON array of string ID in quotes. ex: ["id_here"]
    pub first_column_ids: Option<Vec<ItemId>>,
    pub second_table_id: Option<ItemId>,
    pub second_column_ids: Option<Vec<ItemId>>,
}

#[derive(TS, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./code-service-data.ts", export)]
pub struct RunCustomExportOptions {
    pub custom_export_id: ItemId,
    pub table_id: Option<ItemId>,
    pub output_to_file: bool,
}

#[derive(TS, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./code-service-data.ts", export)]
pub struct RunStructOutputOptions {
    pub struct_output_id: ItemId,
    pub table_id: ItemId,
    pub output_to_file: bool,
}

#[derive(TS, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./code-service-data.ts", export)]
pub struct CodeSource {
    pub id: ItemId,
    pub code: String,
}

#[derive(TS, Deserialize, Debug)]
#[serde(rename_all = "camelCase", rename_all_fields = "camelCase")]
#[ts(export_to = "./code-service-data.ts", export)]
pub enum CodeContainer {
    SchemaItemName(CodeSource),
    RelationshipName(CodeSource),
    ExportTable(CodeSource),
    ExportSchema(CodeSource),
    StructOutput(CodeSource),
}

impl AsRef<CodeSource> for CodeContainer {
    fn as_ref(&self) -> &CodeSource {
        match self {
            Self::SchemaItemName(c) |
            Self::RelationshipName(c) |
            Self::ExportTable(c) |
            Self::ExportSchema(c) |
            Self::StructOutput(c) => c
        }
    }
}

impl CodeContainer {
    pub fn get_necessary_functions(&self) -> &[CallFunction] {
        match self {
            Self::SchemaItemName(_) => &SCHEMA_ITEM,
            Self::RelationshipName(_) => &RELATIONSHIP,
            Self::ExportTable(_) => &EXPORT_TABLE,
            Self::ExportSchema(_) => &EXPORT_SCHEMA,
            Self::StructOutput(_) => &STRUCT_OUTPUT,
        }
    }
    
    pub fn get_file_name(&self) -> String {
        match self {
            Self::SchemaItemName(c) => format!("schema_item_name_{}.ts", c.id),
            Self::RelationshipName(c) => format!("relationship_name_{}.ts", c.id),
            Self::ExportTable(c) => format!("export_table_{}.ts", c.id),
            Self::ExportSchema(c) => format!("export_schema_{}.ts", c.id),
            Self::StructOutput(c) => format!("struct_output_{}.ts", c.id),
        }
    }
}

const SCHEMA_ITEM: [CallFunction; 1] = [CallFunction::new("generate", "GenSchemaItemNameParam", "string", false)];
const RELATIONSHIP: [CallFunction; 1] = [CallFunction::new("generate", "GenRelationshipNameParam", "string", false)];
const EXPORT_TABLE: [CallFunction; 2] = [
    CallFunction::new("generate", "GenExportTableParam", "string", false),
    CallFunction::new("getFileName", "GenExportTableParam", "string", true),
];
const EXPORT_SCHEMA: [CallFunction; 2] = [
    CallFunction::new("generate", "GenExportSchemaParam", "string", false),
    CallFunction::new("getFileName", "GenExportSchemaParam", "string", true),
];
const STRUCT_OUTPUT: [CallFunction; 2] = [
    CallFunction::new("generate", "GenStructOutputParam", "string", false),
    CallFunction::new("getFileName", "GenStructOutputParam", "string", true),
];

#[derive(Clone, Debug)]
pub struct CallFunction {
    pub func_name: &'static str,
    pub param_type_name: &'static str,
    pub return_type_name: &'static str,
    for_file_name: bool,
}

impl CallFunction {
    pub const fn new(
        func_name: &'static str,
        arg_type_name: &'static str,
        return_type_name: &'static str,
        for_file_name: bool,
    ) -> Self {
        Self {
            func_name,
            param_type_name: arg_type_name,
            return_type_name,
            for_file_name,
        }
    }
    
    pub fn verify<'s>(
        &self,
        scope: &mut v8::HandleScope<'s>,
        global: v8::Local<'s, v8::Object>,
    ) -> Result<(), CodeError> {
        let name = self.func_name;
        let js_name = js_string!(scope, name);
        let obj = global.get(scope, js_name.into()).unwrap();
        if !obj.is_function() {
            return Err(CodeError::MissingFunction(name.into()));
        }
        
        if obj.is_async_function() || obj.is_generator_function() {
            return Err(CodeError::AsyncOrGenerator(name.into()));
        }
        
        Ok(())
    }
    
    pub fn run<'s, 't>(
        &self,
        scope: &mut v8::TryCatch<'t, v8::HandleScope<'s>>,
        global: v8::Local<'s, v8::Object>,
        script_result: &mut ScriptResult,
    ) -> Result<(), CodeError> {
        let name = self.func_name;
        let js_name = js_string!(scope, name);
        let func = global.get(scope, js_name.into()).unwrap().cast::<v8::Function>();
        let Some(ret_val) = func.call(scope, global.into(), &[]) else {
            return Err(report_exceptions(scope).into());
        };
        
        if !ret_val.is_string() {
            return Err(CodeError::WrongReturnType {
                func_name: name.to_owned(),
                expected: "string".to_owned(),
                given: ret_val.type_repr().to_owned(),
            });
        }
        
        let value = ret_val.to_rust_string_lossy(scope);
        if self.for_file_name {
            script_result.file_name = Some(value);
        }
        else {
            script_result.value = value;
        }
        
        Ok(())
    }
}


#[derive(TS, Serialize, Deserialize, Debug, Clone, Default)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./code-service-data.ts", export)]
pub struct ScriptResult {
    pub file_name: Option<String>,
    pub value: String,
}

impl ScriptResult {
    pub fn new(file_name: Option<String>, value: String) -> Self {
        Self {
            file_name,
            value,
        }
    }
}
