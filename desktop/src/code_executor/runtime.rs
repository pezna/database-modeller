use std::collections::HashMap;
use crate::code_executor::convert_case_functions::create_case_utils_template;
use crate::code_executor::data::{CallFunction, CodeContainer, ScriptResult};
use crate::code_executor::error::CodeError;
use crate::code_executor::error::report_exceptions;
use crate::code_executor::ts_transpile::transpile_source;
use crate::js_string;
use crate::util::item_id::ItemId;

pub struct CachedScript {
    script: v8::Global<v8::UnboundScript>,
    functions: Vec<CallFunction>,
    text: String,
    source_map: String,
}

pub struct V8Runtime {
    isolate: Option<v8::OwnedIsolate>,
    case_utils_template: v8::Global<v8::ObjectTemplate>,
    cached_scripts: HashMap<ItemId, CachedScript>,
}

impl V8Runtime {
    /// Create a new runtime, storing an isolate and our case-utils template once.
    pub fn new() -> Self {
        let mut isolate = v8::Isolate::new(Default::default());
        let case_utils_template = {
            // Use a temporary handle scope to create the template
            let handle_scope = &mut v8::HandleScope::new(&mut isolate);
            let global_tmpl = v8::ObjectTemplate::new(handle_scope);
            create_case_utils_template(handle_scope, global_tmpl);
            
            // Promote local template to a global template so we can reuse it later
            v8::Global::new(handle_scope, global_tmpl)
        };
        
        Self {
            isolate: Some(isolate),
            case_utils_template,
            cached_scripts: HashMap::new(),
        }
    }
    
    /// Create a new V8 context that has `CaseUtils` in its global object.
    /// Reuses the same `case_utils_template` each time.
    fn create_context(&mut self) -> v8::Global<v8::Context> {
        let isolate = self.isolate.as_mut().unwrap();
        let handle_scope = &mut v8::HandleScope::new(isolate);
        let template = v8::Local::new(handle_scope, &self.case_utils_template);
        // Create the context from the global template
        let context = v8::Context::new(handle_scope, v8::ContextOptions {
            global_template: Some(template),
            ..Default::default()
        });
        v8::Global::new(handle_scope, context)
    }
    
    /// Compiles a piece of JavaScript code and caches the resulting unbound script
    /// under the given `key`.
    pub fn compile_script(&mut self, container: &CodeContainer) -> Result<(), CodeError> {
        let context = self.create_context();
        let isolate = self.isolate.as_mut().unwrap();
        let handle_scope = &mut v8::HandleScope::with_context(isolate, &context);
        // let l = v8::Local::new(handle_scope, context);
        // let context_scope = &mut v8::ContextScope::new(handle_scope, l);
        let scope = &mut v8::TryCatch::new(handle_scope);
        
        let transpiled = transpile_source(container)?;
        let code_id = &container.as_ref().id;
        
        let js_file_name = js_string!(scope, &container.get_file_name());
        let origin = v8::ScriptOrigin::new(
            scope, js_file_name.into(),
            0, 0, false, 0, None, false,
            false, false, None,
        );
        
        // Convert code to a V8 String
        let source = js_string!(scope, &transpiled.text);
        // Compile the script
        let script = match v8::Script::compile(scope, source, Some(&origin)) {
            Some(s) => s,
            None => {
                eprintln!("Failed to compile script for key `{}`", code_id);
                return Err(report_exceptions(scope).into());
            }
        };
        
        // it must be run so that the functions enter scope
        if script.run(scope).is_none() {
            eprintln!("Error while running compiled script `{}`", code_id);
            return Err(report_exceptions(scope).into());
        };
        
        // make sure the necessary functions are defined
        verify_context_variables(scope, container)?;
        
        // Extract the unbound script from the compiled script
        let unbound_script = script.get_unbound_script(scope);
        // Promote the unbound script to a global handle so we can cache it
        let global_unbound = v8::Global::new(scope, unbound_script);
        
        let cached = CachedScript {
            script: global_unbound,
            functions: container.get_necessary_functions().to_vec(),
            text: transpiled.text,
            source_map: transpiled.source_map.unwrap(),
        };
        self.cached_scripts.insert(code_id.clone(), cached);
        Ok(())
    }
    
    /// Runs the previously cached script identified by `key`.
    /// Creates a *new context* each time, then binds & executes the script.
    pub fn run_cached_script(&mut self, key: &ItemId) -> Result<ScriptResult, CodeError> {
        let context = self.create_context();
        // Look up the cached unbound script
        let cached = match self.cached_scripts.get(key) {
            Some(s) => s,
            None => {
                eprintln!("No cached script found for key {key}");
                return Err(CodeError::NoCachedScript(key.clone()));
            }
        };
        let isolate = self.isolate.as_mut().unwrap();
        let scope = &mut v8::HandleScope::with_context(isolate, context);
        let scope = &mut v8::TryCatch::new(scope);
        
        // Open the global handle to get the local UnboundScript
        let unbound_local = cached.script.open(scope);
        // Bind the unbound script to the current context
        let script = unbound_local.bind_to_current_context(scope);
        
        // Execute the script
        if script.run(scope).is_none() {
            eprintln!("Error while running cached script `{}`", key);
            return Err(report_exceptions(scope).into());
        }
        
        Ok(run_functions(scope, &cached.functions)?)
    }
}

fn verify_context_variables<'s>(scope: &mut v8::HandleScope<'s>, container: &CodeContainer) -> Result<(), CodeError> {
    let context = scope.get_current_context();
    let global = context.global(scope);
    
    for call in container.get_necessary_functions() {
        call.verify(scope, global)?;
    }
    
    Ok(())
}

fn run_functions<'s, 't>(
    scope: &mut v8::TryCatch<'t, v8::HandleScope<'s>>,
    calls: &Vec<CallFunction>,
) -> Result<ScriptResult, CodeError> {
    let context = scope.get_current_context();
    let global = context.global(scope);
    let mut script_result = ScriptResult::default();
    
    for call in calls {
        call.run(scope, global, &mut script_result)?;
    }
    
    Ok(script_result)
}

#[cfg(test)]
mod test {
    use sourcemap::SourceMap;
    use crate::code_executor::data::CodeSource;
    use crate::initialize_v8;
    use super::*;
    
    #[test]
    fn test_sourcemap_transpile_error() {
        initialize_v8();
        let mut runtime = V8Runtime::new();
        assert_eq!(runtime.cached_scripts.len(), 0);
        let code = r#"
        interface Data {
            id: string;
            message: string;
        }
        let count = 0;
        function generate(arg: GenSchemaItemNameParam): string {
            count++;
            return `${count.value()}`; // after transpiling, this will be line 4
        }
        "#;
        let id = ItemId("id_test".into());
        let code_source = CodeSource {
            id: id.clone(),
            code: code.to_owned(),
        };
        let container = CodeContainer::SchemaItemName(code_source);
        if let Err(e) = runtime.compile_script(&container) {
            panic!("Expected successful compilation. {e:?}");
        }
        assert_eq!(runtime.cached_scripts.len(), 1);
        
        let res = runtime.run_cached_script(&container.as_ref().id);
        let Err(code_err) = res else {
            panic!("Should have failed run. {res:?}");
        };
        let CodeError::Script(err) = code_err else {
            panic!("Should have been Script error. {code_err:?}");
        };
        assert_eq!(err.line, 4);
        assert_eq!(err.start_column, 19);
        
        let cached = runtime.cached_scripts.get(&id).unwrap();
        assert_eq!(cached.functions.len(), 1);
        
        let sm = SourceMap::from_slice(cached.source_map.as_bytes()).unwrap();
        let token = sm.lookup_token(
            err.line.saturating_sub(1) as u32,
            err.start_column.saturating_sub(1) as u32,
        ).unwrap();
        let src_line = token.get_src_line() + 1;
        let src_col = token.get_src_col() + 1;
        assert_eq!(src_line, 9);
        assert_eq!(src_col, 29);
    }
    
    #[test]
    fn test_v8_runtime() {
        initialize_v8();
        let mut runtime = V8Runtime::new();
        assert_eq!(runtime.cached_scripts.len(), 0);
        let code = r#"
        let count = 0;
        function generate() {
            count++;
            return `${count}`;
        }
        generate()
        "#;
        let id = ItemId("id_test".into());
        let code_source = CodeSource {
            id: id.clone(),
            code: code.to_owned(),
        };
        let container = CodeContainer::SchemaItemName(code_source);
        if let Err(e) = runtime.compile_script(&container) {
            panic!("{e:?}");
        }
        assert_eq!(runtime.cached_scripts.len(), 1);
        let cached = runtime.cached_scripts.get(&id).unwrap();
        assert_eq!(cached.functions.len(), 1);
        
        let res = runtime.run_cached_script(&container.as_ref().id);
        let Ok(script_result) = res else {
            panic!("{res:?}");
        };
        assert!(script_result.file_name.is_none());
        assert_eq!(script_result.value, "2", "{script_result:?}");
    }
}
