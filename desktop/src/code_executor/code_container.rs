use serde::{Deserialize, Serialize};
use ts_rs::TS;
use v8::{Context, ContextScope, Function, HandleScope, Isolate, Local, Script, ScriptOrigin, TryCatch};
use crate::code_executor::data::{RunCodeContainer, ScriptResult};
use crate::code_executor::error::ScriptError;
use crate::code_executor::error::report_exceptions;
use crate::data::root_state::BackendRootState;
use crate::util::item_id::{ItemId, ToJsonRepr};

static GENERATE_FUNC_NAME: &str = "generate";
static GET_FILE_NAME_FUNC_NAME: &str = "getFileName";
static CUSTOM_EXPORTER_FUNCTIONS: [&str; 2] = [GENERATE_FUNC_NAME, GET_FILE_NAME_FUNC_NAME];
static SCHEMA_NAME_FUNCTIONS: [&str; 1] = [GENERATE_FUNC_NAME];

#[derive(TS, Serialize, Deserialize, Copy, Clone, Debug)]
#[serde(rename_all = "camelCase")]
#[ts(use_ts_enum, export_to = "./code-data.ts", export)]
pub enum CodeContainerType {
    SchemaItemName,
    RelationshipName,
    PerTableGenerator,
    PerSchemaGenerator,
    StructOutput,
}

#[derive(TS, Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./code-data.ts", export)]
pub struct CodeContainer {
    pub id: ItemId,
    pub code: String,
    pub type_: CodeContainerType,
}

impl CodeContainer {
    pub fn new(id: ItemId, code: String, type_: CodeContainerType) -> Self {
        Self {
            id,
            code,
            type_,
        }
    }

    pub fn compile<'s>(&self, try_catch: &mut TryCatch<HandleScope<'s>>) -> Result<Local<'s, Script>, ScriptError> {
        let filename = v8::String::new(try_catch, self.id.as_ref()).unwrap();
        let origin = ScriptOrigin::new(
            try_catch,
            filename.into(),
            0,
            0,
            false,
            0,
            None,
            false,
            false,
            false,
            None,
        );
        let source = v8::String::new(try_catch, self.code.as_str()).unwrap();
        
        match Script::compile(try_catch, source, Some(&origin)) {
            Some(script) => Ok(script),
            None => Err(report_exceptions(try_catch))
        }
    }
    
    pub fn execute_code_container(&self, run: &RunCodeContainer, root_state: &BackendRootState) -> Result<ScriptResult, ScriptError> {
        let isolate = &mut Isolate::new(Default::default());
        let handle_scope = &mut HandleScope::new(isolate);
        let context = Context::new(handle_scope, Default::default());
        let scope = &mut ContextScope::new(handle_scope, context);
        let scope = &mut TryCatch::new(scope);
        // setup_convert_case(scope, &context);
        
        let precursor = match self.create_precursor_container(run, root_state) {
            Ok(container) => container,
            Err(msg) => return Err(ScriptError::new_simple(msg)),
        };
        let script = precursor.compile(scope)?;
        if script.run(scope).is_none() {
            return Err(report_exceptions(scope));
        }
        let script = self.compile(scope)?;
        if script.run(scope).is_none() {
            return Err(report_exceptions(scope));
        }
        
        if let Err(msg) = verify_context_variables(&context, scope, self) {
            return Err(ScriptError::new_simple(msg));
        }
        
        let global = context.global(scope);
        // run generate function
        let generate_name = v8::String::new(scope, GENERATE_FUNC_NAME).unwrap();
        let obj = global.get(scope, generate_name.into()).unwrap();
        let Ok(func) = Local::<Function>::try_from(obj) else {
            return Err(ScriptError::new_simple("generate() is not a function. This should never happen.".to_owned()));
        };
        let Some(result_value) = func.call(scope, global.into(), &[]) else {
            return Err(report_exceptions(scope));
        };
        
        if !result_value.is_string() {
            return Err(ScriptError::new_simple("generate() did not return a string.".to_owned()));
        }
        
        // run getFileName if the container is required to have that function
        let file_name = match self.type_ {
            CodeContainerType::StructOutput |
            CodeContainerType::PerSchemaGenerator |
            CodeContainerType::PerTableGenerator => self.execute_file_name(scope, &context)?,
            _ => "".to_owned()
        };
        
        let value = result_value.to_rust_string_lossy(scope);
        Ok(ScriptResult::new(Some(file_name), value))
    }
    
    fn execute_file_name(&self, scope: &mut TryCatch<HandleScope>, context: &Local<Context>) -> Result<String, ScriptError> {
        // run getFileName function
        let global = context.global(scope);
        let filename_name = v8::String::new(scope, GET_FILE_NAME_FUNC_NAME).unwrap();
        let obj = global.get(scope, filename_name.into()).unwrap();
        let Ok(func) = Local::<Function>::try_from(obj) else {
            return Err(ScriptError::new_simple("getFileName() is not a function. This should never happen.".to_owned()));
        };
        let Some(file_name_value) = func.call(scope, global.into(), &[]) else {
            return Err(ScriptError::new_simple("getFileName() did not return anything. This should never happen.".to_owned()));
        };
        
        if !file_name_value.is_string() {
            return Err(ScriptError::new_simple("getFileName() did not return a string.".to_owned()));
        }
        
        let value = file_name_value.to_rust_string_lossy(scope);
        if value.is_empty() {
            return Err(ScriptError::new_simple("getFileName() returned an empty string.".to_owned()));
        }
        Ok(value)
    }
    
    fn create_precursor_container(&self, run: &RunCodeContainer, root_state: &BackendRootState) -> Result<CodeContainer, String> {
        let code = match self.type_ {
            CodeContainerType::SchemaItemName => generate_schema_item_name_code(run, root_state)?,
            CodeContainerType::RelationshipName => generate_relationship_name_code(run, root_state)?,
            CodeContainerType::PerTableGenerator => generate_per_table_code(run, root_state)?,
            CodeContainerType::PerSchemaGenerator => generate_per_schema_code(root_state)?,
            CodeContainerType::StructOutput => generate_struct_output_code(run, root_state)?,
        };
        let cc = CodeContainer::new("fake_id".into(), code, self.type_);
        Ok(cc)
    }
}

fn generate_schema_item_name_code(run: &RunCodeContainer, root_state: &BackendRootState) -> Result<String, String> {
    let Some(options) = &run.name_options else {
        return Err("name_options not given".to_owned());
    };
    let Some(table_id) = &options.first_table_id else {
        return Err("Schema item name requires first table id".to_owned())
    };
    let Some(column_ids) = &options.first_column_ids else {
        return Err("Schema item name requires first column ids".to_owned())
    };
    Ok(format!(
        r#"
            const schema = {};
            const table = schema.tables[{}];
            const columns = {}.map(id => schema.columns[id]);
        "#,
        root_state.schema_state_json(),
        table_id.to_json_repr(),
        column_ids.to_json_repr(),
    ))
}

fn generate_relationship_name_code(run: &RunCodeContainer, root_state: &BackendRootState) -> Result<String, String> {
    let Some(options) = &run.name_options else {
        return Err("name_options not given".to_owned());
    };
    let Some(first_table_id) = &options.first_table_id else {
        return Err("Relationship name requires first table id".to_owned())
    };
    let Some(first_column_ids) = &options.first_column_ids else {
        return Err("Relationship name requires first column ids".to_owned())
    };
    let Some(second_table_id) = &options.second_table_id else {
        return Err("Relationship name requires second table id".to_owned())
    };
    let Some(second_column_ids) = &options.second_column_ids else {
        return Err("Relationship name requires second column ids".to_owned())
    };
    Ok(format!(
        r#"
            const schema = {};
            const childTable = schema.tables[{}];
            const childColumns = {}.map(id => schema.columns[id]);
            const parentTable = schema.tables[{}];
            const parentColumns = {}.map(id => schema.columns[id]);
        "#,
        root_state.schema_state_json(),
        first_table_id.to_json_repr(),
        first_column_ids.to_json_repr(),
        second_table_id.to_json_repr(),
        second_column_ids.to_json_repr(),
    ))
}

fn generate_per_table_code(run: &RunCodeContainer, root_state: &BackendRootState) -> Result<String, String> {
    let Some(options) = &run.export_options else {
        return Err("name_options not given".to_owned());
    };
    let Some(table_id) = &options.table_id else {
        return Err("Table export requires table export id".to_owned())
    };
    Ok(format!(
        r#"
            const schema = {};
            const table = schema.tables[{}];
            const primaryKey = table.primaryKeyId ? schema.primaryKeys[table.primaryKeyId] : null;
            const columns = table.columnIds.map(id => schema.columns[id]);
            const columnTypes = columns.map(c => schema.columnTypes[c.typeId]);
            for (let i = 0; i < columns.length; ++i) {{
                columns[i].type = columnTypes[i];
            }}
            const relationships = table.relationshipIds.map(id => schema.relationships[id]).filter(rel => rel.childTableId === table.id);
            const indexes = Object.values(schema.indexes).filter(x => x.tableId === table.id);
            const uniques = Object.values(schema.uniques).filter(x => x.tableId === table.id);
        "#,
        root_state.schema_state_json(),
        table_id.to_json_repr(),
    ))
}

fn generate_per_schema_code(root_state: &BackendRootState) -> Result<String, String> {
    Ok(format!(
        r#"
            const schema = {};
            const {{ tables, columns, columnTypes, primaryKeys, relationships, indexes, uniques }} = schema;
        "#,
        root_state.schema_state_json(),
    ))
}

fn generate_struct_output_code(run: &RunCodeContainer, root_state: &BackendRootState) -> Result<String, String> {
    let Some(options) = &run.struct_output_options else {
        return Err("Struct output requires struct_output ".to_owned())
    };
    let data = root_state.get_struct_output(&options.struct_output_id).unwrap();
    Ok(format!(
        r#"
            const schema = {};
            const table = schema.tables[{}];
            const columns = table.columnIds.map(id => schema.columns[id]);
            const columnTypes = columns.map(c => schema.columnTypes[c.typeId]);
            for (let i = 0; i < columns.length; ++i) {{
                columns[i].type = columnTypes[i];
            }}
            const data = {};
        "#,
        root_state.schema_state_json(),
        options.table_id.to_json_repr(),
        serde_json::to_string(data).unwrap(),
    ))
}

pub fn verify_context_variables<'s>(context: &Context, scope: &mut HandleScope<'s>, container: &CodeContainer) -> Result<(), String> {
    let global = context.global(scope);
    let mut missing_names: Vec<&'static str> = vec![];
    let expected_function_names = match container.type_ {
        CodeContainerType::StructOutput |
        CodeContainerType::PerTableGenerator |
        CodeContainerType::PerSchemaGenerator
        => Vec::from(CUSTOM_EXPORTER_FUNCTIONS),
        CodeContainerType::RelationshipName |
        CodeContainerType::SchemaItemName
        => Vec::from(SCHEMA_NAME_FUNCTIONS),
    };
    
    for name in expected_function_names {
        let js_name = v8::String::new(scope, name).unwrap();
        let obj = global.get(scope, js_name.into()).unwrap();
        
        if !obj.is_function() {
            missing_names.push(name);
        }
    }
    
    if missing_names.len() > 0 {
        let mn = missing_names.join(", ");
        return Err(format!("Missing function definition for: {mn}"));
    }
    
    Ok(())
}

#[cfg(test)]
mod test {
    use v8::{Context, ContextScope, Isolate, TryCatch};
    use crate::initialize_v8;
    use super::*;
    
    #[test]
    fn compile_success() {
        initialize_v8();
        let isolate = &mut Isolate::new(Default::default());
        let handle_scope = &mut HandleScope::new(isolate);
        let context = Context::new(handle_scope, Default::default());
        let scope = &mut ContextScope::new(handle_scope, context);
        let scope = &mut TryCatch::new(scope);
        
        let cc = CodeContainer::new(
            "main".into(),
            "(() => {})".to_owned(),
            CodeContainerType::SchemaItemName,
        );
        let res = cc.compile(scope);
        assert!(res.is_ok());
    }
    
    #[test]
    fn compile_failure() {
        initialize_v8();
        let isolate = &mut Isolate::new(Default::default());
        let handle_scope = &mut HandleScope::new(isolate);
        let context = Context::new(handle_scope, Default::default());
        let scope = &mut ContextScope::new(handle_scope, context);
        let scope = &mut TryCatch::new(scope);
        
        let cc = CodeContainer::new(
            "main".into(),
            "true / false.".to_owned(),
            CodeContainerType::SchemaItemName,
        );
        let res = cc.compile(scope);
        assert!(res.is_err());
        if let Err(code_error) = res {
            assert_eq!(code_error.file_name.as_str(), "main");
        }
    }
    
    #[test]
    fn missing_generate_function() {
        initialize_v8();
        let isolate = &mut Isolate::new(Default::default());
        let handle_scope = &mut HandleScope::new(isolate);
        let context = Context::new(handle_scope, Default::default());
        let scope = &mut ContextScope::new(handle_scope, context);
        let scope = &mut TryCatch::new(scope);
        
        let cc = CodeContainer::new(
            "main".into(),
            r#"const a = 40;
            function generate() {}"#.to_owned(),
            CodeContainerType::PerTableGenerator,
        );
        let script = cc.compile(scope).unwrap();
        script.run(scope).unwrap(); // must run the script to populate the scope
        let res = verify_context_variables(&context, scope, &cc);
        assert!(res.is_err());
        if let Err(err) = res {
            assert!(err.contains("getFileName"));
            assert!(!err.contains("generate"));
        }
    }
    
    #[test]
    fn missing_get_file_name_function() {
        initialize_v8();
        let isolate = &mut Isolate::new(Default::default());
        let handle_scope = &mut HandleScope::new(isolate);
        let context = Context::new(handle_scope, Default::default());
        let scope = &mut ContextScope::new(handle_scope, context);
        let scope = &mut TryCatch::new(scope);
        
        let cc = CodeContainer::new(
            "main".into(),
            r#"const a = 40;
            function getFileName() {}"#.to_owned(),
            CodeContainerType::PerTableGenerator,
        );
        let script = cc.compile(scope).unwrap();
        script.run(scope).unwrap(); // must run the script to populate the scope
        let res = verify_context_variables(&context, scope, &cc);
        assert!(res.is_err());
        if let Err(err) = res {
            assert!(err.contains("generate"));
            assert!(!err.contains("getFileName"));
        }
    }
    
    #[test]
    fn no_missing_functions() {
        initialize_v8();
        let isolate = &mut Isolate::new(Default::default());
        let handle_scope = &mut HandleScope::new(isolate);
        let context = Context::new(handle_scope, Default::default());
        let scope = &mut ContextScope::new(handle_scope, context);
        let scope = &mut TryCatch::new(scope);
        
        let cc = CodeContainer::new(
            "main".into(),
            r#"function generate() {}
            function getFileName() {}"#.to_owned(),
            CodeContainerType::PerTableGenerator,
        );
        let script = cc.compile(scope).unwrap();
        script.run(scope).unwrap(); // must run the script to populate the scope
        let res = verify_context_variables(&context, scope, &cc);
        assert!(res.is_ok());
    }
}
