use std::sync::Once;
use log::info;

pub mod data;
pub mod runtime;
pub mod utils;
pub mod error;
pub mod ts_transpile;
pub mod convert_case_functions;
pub mod code_container;
pub mod struct_output;

pub fn initialize_v8() {
    static ONCE: Once = Once::new();
    ONCE.call_once(|| {
        info!("Initializing V8");
        let platform = v8::new_default_platform(0, false).make_shared();
        v8::V8::initialize_platform(platform);
        v8::V8::initialize();
    })
}
