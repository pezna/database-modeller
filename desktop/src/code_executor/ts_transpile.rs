use std::collections::HashSet;
use deno_ast::{parse_script, EmitOptions, EmittedSourceText, ImportsNotUsedAsValues, MediaType, ModuleSpecifier, ParsedSource, SourceRangedForSpanned, TranspileModuleOptions, TranspileOptions};
use deno_ast::ParseParams;
use deno_ast::SourceMapOption;
use deno_ast::swc::ast::{Decl, Pat, Stmt};
use crate::code_executor::data::CodeContainer;
use crate::code_executor::error::CodeError;

pub fn transpile_source(code_container: &CodeContainer) -> Result<EmittedSourceText, CodeError> {
    let code_source = code_container.as_ref();
    let file_spec = format!("file://{}", code_container.get_file_name());
    let source = code_source.code.as_str();
    
    let parsed = parse_script(ParseParams {
        specifier: ModuleSpecifier::parse(&file_spec).unwrap(),
        text: source.into(),
        media_type: MediaType::TypeScript,
        capture_tokens: false,
        scope_analysis: false,
        maybe_syntax: None,
    }).map_err(|e| CodeError::Script(e.into()))?;
    
    ensure_proper_functions(code_container, &parsed)?;
    
    let transpiled_source = parsed
        .transpile(
            &TranspileOptions {
                imports_not_used_as_values: ImportsNotUsedAsValues::Remove,
                use_decorators_proposal: true,
                ..Default::default()
            },
            &TranspileModuleOptions { module_kind: None },
            &EmitOptions {
                source_map: SourceMapOption::Separate,
                inline_sources: false,
                ..Default::default()
            },
        )
        .map_err(CodeError::from)?
        .into_source();
    Ok(transpiled_source)
}

fn ensure_proper_functions(code_container: &CodeContainer, parsed: &ParsedSource) -> Result<(), CodeError> {
    let program = parsed.program();
    let Some(script) = program.as_script() else {
        return Err(CodeError::Other("Expected a script program".to_owned()));
    };
    let needed_functions = code_container.get_necessary_functions().to_vec();
    let mut needed_names = needed_functions
        .iter()
        .map(|f| f.func_name)
        .collect::<HashSet<_>>();
    
    for stmt in &script.body {
        if let Stmt::Decl(Decl::Fn(fn_decl)) = stmt {
            // println!("{fn_decl:#?}");
            let fn_name = fn_decl.ident.sym.as_ref();
            if fn_decl.function.is_async || fn_decl.function.is_generator {
                return Err(CodeError::AsyncOrGenerator(fn_name.to_owned()));
            }
            
            if needed_names.contains(fn_name) {
                needed_names.remove(&fn_name);
                let func = needed_functions.iter()
                    .find(|f| f.func_name == fn_name)
                    .unwrap();
                
                if fn_decl.function.params.len() > 1 {
                    return Err(CodeError::WrongParamCount {
                        func_name: fn_name.to_owned(),
                        expected: 1,
                        given: fn_decl.function.params.len(),
                    });
                }
                
                // ensure that the function has the correct parameter name
                let mut param_type = "";
                if let Some(param) = fn_decl.function.params.first() {
                    match &param.pat {
                        Pat::Ident(ident) if ident.type_ann.is_some() => {
                            param_type = ident.type_ann.as_ref().unwrap()
                                .type_ann.text_fast(parsed);
                        },
                        Pat::Array(arr) if arr.type_ann.is_some() => {
                            param_type = arr.type_ann.as_ref().unwrap()
                                .type_ann.text_fast(parsed);
                        },
                        Pat::Object(object) => {
                            param_type = object.type_ann.as_ref().unwrap()
                                .type_ann.text_fast(parsed);
                        },
                        _ => {},
                    }
                }
                
                if param_type != func.param_type_name {
                    return Err(CodeError::WrongParamType {
                        func_name: fn_name.to_owned(),
                        expected: func.param_type_name.to_owned(),
                        given: param_type.to_owned(),
                    });
                }
                
                // ensure correct return type
                if let Some(ret) = &fn_decl.function.return_type {
                    let ret_text = ret.type_ann.text_fast(parsed);
                    if ret_text != func.return_type_name {
                        return Err(CodeError::WrongReturnType {
                            func_name: fn_name.to_owned(),
                            expected: func.return_type_name.to_owned(),
                            given: ret_text.to_owned(),
                        });
                    }
                }
            }
        }
    }
    
    if needed_names.len() > 0 {
        let names = needed_names.iter()
            .cloned()
            .collect::<Vec<_>>()
            .join(", ");
        return Err(CodeError::MissingFunction(names));
    }
    
    Ok(())
}

#[cfg(test)]
mod tests {
    use crate::code_executor::data::CodeSource;
    use super::*;
    
    #[test]
    fn test_transpile_success() {
        let file_name = "yolo.js";
        let source = r#"
        "use strict";
        function generate({ table }: GenSchemaItemNameParam): string {
            // here's a comment for you
            const a: string = "Hello";
        }
        "#;
        let container = CodeContainer::SchemaItemName(CodeSource {
            id: file_name.into(),
            code: source.to_owned(),
        });
        let result = transpile_source(&container);
        let Ok(trans_source) = result else {
            panic!("{:?}", result);
        };
        assert!(trans_source.text.contains("function generate({ table }) {"), "{:?}", trans_source);
    }
    
    #[test]
    fn test_transpile_wrong_param_type() {
        let file_name = "yolo.js";
        let source = r#"
        "use strict";
        function generate({ table }: ArgName): boolean {
            // here's a comment for you
            const a: string = "Hello";
        }
        "#;
        let container = CodeContainer::SchemaItemName(CodeSource {
            id: file_name.into(),
            code: source.to_owned(),
        });
        let result = transpile_source(&container);
        let Err(code_err) = result else {
            panic!("{:#?}", result);
        };
        let CodeError::WrongParamType { func_name, expected, given } = code_err else {
            panic!("Expected WrongParamType error, not {:#?}", code_err);
        };
        assert_eq!(func_name, "generate");
        assert_eq!(expected, "GenSchemaItemNameParam");
    }
    
    #[test]
    fn test_transpile_wrong_return_type() {
        let file_name = "yolo.js";
        let source = r#"
        "use strict";
        function generate({ table }: GenSchemaItemNameParam): boolean {
            // here's a comment for you
            const a: string = "Hello";
        }
        "#;
        let container = CodeContainer::SchemaItemName(CodeSource {
            id: file_name.into(),
            code: source.to_owned(),
        });
        let result = transpile_source(&container);
        let Err(code_err) = result else {
            panic!("{:#?}", result);
        };
        let CodeError::WrongReturnType { func_name, expected, given } = code_err else {
            panic!("Expected WrongParamType error, not {:#?}", code_err);
        };
        assert_eq!(func_name, "generate");
        assert_eq!(expected, "string");
        assert_eq!(given, "boolean");
    }
    
    #[test]
    fn test_transpile_syntax_error() {
        let file_name = "yolo.js";
        let source = r#"
        function generate(
            name: string,
            age: number,
        ) boolean { // <-- no colon before type
            const a: string = "Hello";
        }
        "#;
        let container = CodeContainer::SchemaItemName(CodeSource {
            id: file_name.into(),
            code: source.to_owned(),
        });
        let result = transpile_source(&container);
        let Err(code_err) = result else {
            panic!("{:#?}", result);
        };
        let CodeError::Script(err) = code_err else {
            panic!("Expected Script error, not {:#?}", code_err);
        };
        assert!(err.message.contains("boolean"), "{}", err.message);
        assert_eq!(err.line, 5);
        assert_eq!(err.start_column, 11);
    }
    
    #[test]
    fn test_transpile_missing_functions() {
        let file_name = "yolo.js";
        let source = r#"
        function yolo() {
            const a: string = "Hello";
        }
        "#;
        let container = CodeContainer::StructOutput(CodeSource {
            id: file_name.into(),
            code: source.to_owned(),
        });
        let result = transpile_source(&container);
        let Err(code_err) = result else {
            panic!("{:#?}", result);
        };
        let CodeError::MissingFunction(names) = code_err else {
            panic!("Expected MissingFunction error, not {:#?}", code_err);
        };
        assert!(names.contains("generate"), "{}", names);
        assert!(names.contains("getFileName"), "{}", names);
    }
}