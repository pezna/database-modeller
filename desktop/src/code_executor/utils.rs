#[macro_export]
macro_rules! js_string {
    ($scope:expr, $e:expr) => {
        v8::String::new($scope, $e).unwrap()
    };
}
