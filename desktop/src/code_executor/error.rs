use serde::{Deserialize, Serialize};
use thiserror::Error;
use ts_rs::TS;
use std::fmt::{Debug, Display, Formatter};
use deno_ast::diagnostics::Diagnostic;
use deno_ast::{ParseDiagnostic, SourceRanged, TranspileError};
use crate::util::item_id::ItemId;

#[derive(Error, TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase", rename_all_fields = "camelCase", tag = "type", content = "data")]
#[ts(export_to = "./code-service-data.ts", export)]
pub enum CodeError {
    #[error("Script error")]
    Script(#[from] ScriptError),
    #[error("No cached script with id {0}")]
    NoCachedScript(ItemId),
    #[error("Missing function definition: {0}")]
    MissingFunction(String),
    #[error("Function '{0}' cannot be async or a generator")]
    AsyncOrGenerator(String),
    #[error("Function '{func_name}' should return {expected}, not {given}")]
    WrongReturnType { func_name: String, expected: String, given: String },
    #[error("Function '{func_name}' parameter should be {expected}, not {given}")]
    WrongParamType { func_name: String, expected: String, given: String },
    #[error("Function '{func_name}' must have {expected} parameters, not {given}")]
    WrongParamCount { func_name: String, expected: usize, given: usize },
    #[error("Other error: {0}")]
    Other(String),
}

#[derive(TS, Serialize, Deserialize, Debug, Clone, Error)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./code-service-data.ts", export)]
pub struct ScriptError {
    pub file_name: String,
    pub message: String,
    pub stack_trace: String,
    /// 1-based line number
    pub line: usize,
    /// 1-based columne on the line
    pub start_column: usize,
    pub end_column: usize,
    pub start_pos: u32,
    pub end_pos: u32,
}

impl ScriptError {
    pub fn new(
        file_name: String,
        message: String,
        stack_trace: String,
        line: usize,
        start_column: usize,
        end_column: usize,
        start_pos: u32,
        end_pos: u32,
    ) -> Self {
        Self {
            file_name,
            message,
            stack_trace,
            line,
            start_column,
            end_column,
            start_pos,
            end_pos,
        }
    }
    
    pub fn new_simple(message: String) -> Self {
        Self {
            file_name: String::new(),
            message,
            stack_trace: String::new(),
            line: 0,
            start_column: 0,
            end_column: 0,
            start_pos: 0,
            end_pos: 0,
        }
    }
}

impl From<ParseDiagnostic> for ScriptError {
    fn from(err: ParseDiagnostic) -> Self {
        let start_pos = err.range.start().as_byte_pos().0;
        let end_pos = err.range.end().as_byte_pos().0;
        let display_position = err.display_position();
        let message = err.message().to_string();
        let stack_trace = err.snippet().unwrap().source.text_str().to_owned();
        let file_name = err.specifier.as_str().to_owned();
        let line_number = display_position.line_number;
        let column = display_position.column_number;
        
        Self::new(
            file_name,
            message,
            stack_trace,
            line_number,
            column,
            column,
            start_pos,
            end_pos,
        )
    }
}

impl From<TranspileError> for CodeError {
    fn from(err: TranspileError) -> Self {
        match err {
            TranspileError::ParseErrors(e) => Self::Script(e.0[0].clone().into()),
            _ => Self::Other(err.to_string()),
        }
    }
}

impl Display for ScriptError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        Debug::fmt(&self, f)
    }
}

pub fn report_exceptions(try_catch: &mut v8::TryCatch<v8::HandleScope>) -> ScriptError {
    let exception = try_catch.exception().unwrap();
    let exception_string = exception
        .to_string(try_catch)
        .unwrap()
        .to_rust_string_lossy(try_catch);
    // print stack trace
    let stack_trace = if let Some(st) = try_catch.stack_trace() {
        st
    }
    else {
        return ScriptError::new("(unknown)".to_owned(), exception_string, "".to_owned(), 0, 0, 0, 0, 0);
    };
    let trace = stack_trace.to_rust_string_lossy(try_catch);
    
    let message = if let Some(msg) = try_catch.message() {
        msg
    }
    else {
        // eprintln!("{}", exception_string);
        return ScriptError::new("(unknown)".to_owned(), exception_string, trace, 0, 0, 0, 0, 0);
    };
    
    let filename = message
        .get_script_resource_name(try_catch)
        .map_or_else(|| "(unknown)".to_owned(), |s| {
            s.to_string(try_catch).unwrap().to_rust_string_lossy(try_catch)
        });
    let start_pos = message.get_start_position() as u32;
    let end_pos = message.get_end_position() as u32;
    let line_number = message.get_line_number(try_catch).unwrap_or_default();
    // eprintln!("{}:{}: {}", filename, line_number, exception_string);
    
    // print line of the source code
    // let source_line = message.get_source_line(try_catch)
    //     .map(|s| {
    //         s.to_string(try_catch)
    //             .unwrap()
    //             .to_rust_string_lossy(try_catch)
    //     })
    //     .unwrap();
    // eprintln!("{}", source_line);
    
    // print wavy underline
    // make the columns 1-based like the line number
    let start_column = message.get_start_column() + 1;
    let end_column = message.get_end_column() + 1;
    
    // for _ in 0..start_column {
    //     eprint!(" ");
    // }
    //
    // for _ in start_column..end_column {
    //     eprint!("^");
    // }
    // eprintln!();
    
    ScriptError::new(filename, exception_string, trace, line_number, start_column, end_column, start_pos, end_pos)
}