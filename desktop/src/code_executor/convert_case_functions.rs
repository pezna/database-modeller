use convert_case::{Case, Casing};
use v8::{FunctionCallbackArguments, HandleScope, Local, ReturnValue, PropertyAttribute, ObjectTemplate, FunctionTemplate};
use paste::paste;

macro_rules! add_case {
    ($variant:ident, $tmpl_name:ident, $scope:ident) => {
        paste! {
            fn [<$variant:snake _case>](
                sc: &mut HandleScope,
                args: FunctionCallbackArguments,
                mut rv: ReturnValue
            ) {
                let text = args.get(0)
                    .to_rust_string_lossy(sc)
                    .to_case(Case::$variant);
                let s = v8::String::new(sc, &text).unwrap();
                rv.set(s.into());
            }
            
            let [<$variant:snake _fn>] = FunctionTemplate::new($scope, [<$variant:snake _case>]);
            $tmpl_name.set_with_attr(
                v8::String::new($scope, stringify!([<to $variant>])).unwrap().into(),
                [<$variant:snake _fn>].into(),
                PropertyAttribute::READ_ONLY | PropertyAttribute::DONT_DELETE,
            );
        }
    };
}

pub fn create_case_utils_template<'s>(
    scope: &mut HandleScope<'s, ()>,
    global_tmpl: Local<'s, ObjectTemplate>,
) {
    let object_tmpl = ObjectTemplate::new(scope);
    add_case!(Upper, object_tmpl, scope);
    add_case!(Lower, object_tmpl, scope);
    add_case!(Title, object_tmpl, scope);
    add_case!(Toggle, object_tmpl, scope);
    add_case!(Camel, object_tmpl, scope);
    add_case!(UpperCamel, object_tmpl, scope);
    add_case!(Pascal, object_tmpl, scope);
    add_case!(Snake, object_tmpl, scope);
    add_case!(UpperSnake, object_tmpl, scope);
    add_case!(Constant, object_tmpl, scope);
    add_case!(Kebab, object_tmpl, scope);
    add_case!(UpperKebab, object_tmpl, scope);
    add_case!(Cobol, object_tmpl, scope);
    add_case!(Train, object_tmpl, scope);
    add_case!(Flat, object_tmpl, scope);
    add_case!(UpperFlat, object_tmpl, scope);
    add_case!(Alternating, object_tmpl, scope);
    
    global_tmpl.set_with_attr(
        v8::String::new(scope, "convertCase").unwrap().into(),
        object_tmpl.into(),
        PropertyAttribute::READ_ONLY | PropertyAttribute::DONT_DELETE,
    );
}

#[cfg(test)]
mod test {
    use v8::{Context, ContextScope, Isolate, Script, ScriptOrigin, TryCatch};
    use crate::initialize_v8;
    use crate::code_executor::error::report_exceptions;
    use super::*;
    
    #[test]
    fn test_cases() {
        initialize_v8();
        let isolate = &mut Isolate::new(Default::default());
        let handle_scope = &mut HandleScope::new(isolate);
        let global_tmpl = ObjectTemplate::new(handle_scope);
        create_case_utils_template(handle_scope, global_tmpl);
        
        let context = Context::new(handle_scope, v8::ContextOptions {
            global_template: Some(global_tmpl),
            ..Default::default()
        });
        let scope = &mut ContextScope::new(handle_scope, context);
        let scope = &mut TryCatch::new(scope);
        let filename = v8::String::new(scope, "yolo.js").unwrap();
        let origin = ScriptOrigin::new(
            scope, filename.into(), 0, 0, false,
            0, None, false, false, false, None,
        );
        let source = v8::String::new(scope, r#"
            let upper_case = convertCase.toUpper('yolo swag');
            let lower_case = convertCase.toLower('YOLO SWAG');
            let title_case = convertCase.toTitle('yolo swag');
            let toggle_case = convertCase.toToggle('yolo swag');
            let camel_case = convertCase.toCamel('yolo swag');
            let pascal_case = convertCase.toUpperCamel('yolo swag');
            let upper_camel_case = convertCase.toPascal('yolo swag');
            let snake_case = convertCase.toSnake('yolo swag');
            let upper_snake_case = convertCase.toUpperSnake('yolo swag');
            let constant_case = convertCase.toConstant('yolo swag');
            let kebab_case = convertCase.toKebab('yolo swag');
            let cobol_case = convertCase.toUpperKebab('yolo swag');
            let upper_kebab_case = convertCase.toCobol('yolo swag');
            let train_case = convertCase.toTrain('yolo swag');
            let flat_case = convertCase.toFlat('yolo swag');
            let upper_flat_case = convertCase.toUpperFlat('yolo swag');
            let alternating_case = convertCase.toAlternating('yolo swag');
            ({
                upper_case,lower_case,title_case,toggle_case,camel_case,
                pascal_case,upper_camel_case,snake_case,upper_snake_case,
                constant_case,kebab_case,cobol_case,upper_kebab_case,
                train_case,flat_case,upper_flat_case,alternating_case,
            })
            "#).unwrap();
        if let Some(script) = Script::compile(scope, source, Some(&origin)) {
            let run_res = script.run(scope);
            assert!(run_res.is_some(), "{:#?}", report_exceptions(scope));
            let result = run_res.unwrap();
            let object = result
                .to_object(scope)
                .expect("Expected an object");
            let upper_case_name = v8::String::new(scope, "upper_case").unwrap();
            let lower_case_name = v8::String::new(scope, "lower_case").unwrap();
            let title_case_name = v8::String::new(scope, "title_case").unwrap();
            let toggle_case_name = v8::String::new(scope, "toggle_case").unwrap();
            let camel_case_name = v8::String::new(scope, "camel_case").unwrap();
            let pascal_case_name = v8::String::new(scope, "pascal_case").unwrap();
            let upper_camel_case_name = v8::String::new(scope, "upper_camel_case").unwrap();
            let snake_case_name = v8::String::new(scope, "snake_case").unwrap();
            let upper_snake_case_name = v8::String::new(scope, "upper_snake_case").unwrap();
            let constant_case_name = v8::String::new(scope, "constant_case").unwrap();
            let kebab_case_name = v8::String::new(scope, "kebab_case").unwrap();
            let cobol_case_name = v8::String::new(scope, "cobol_case").unwrap();
            let upper_kebab_case_name = v8::String::new(scope, "upper_kebab_case").unwrap();
            let train_case_name = v8::String::new(scope, "train_case").unwrap();
            let flat_case_name = v8::String::new(scope, "flat_case").unwrap();
            let upper_flat_case_name = v8::String::new(scope, "upper_flat_case").unwrap();
            let alternating_case_name = v8::String::new(scope, "alternating_case").unwrap();

            let upper_case_value = object.get(scope, upper_case_name.into()).expect("Could not get upper_case_name from object");
            let lower_case_value = object.get(scope, lower_case_name.into()).expect("Could not get lower_case_name from object");
            let title_case_value = object.get(scope, title_case_name.into()).expect("Could not get title_case_name from object");
            let toggle_case_value = object.get(scope, toggle_case_name.into()).expect("Could not get toggle_case_name from object");
            let camel_case_value = object.get(scope, camel_case_name.into()).expect("Could not get camel_case_name from object");
            let pascal_case_value = object.get(scope, pascal_case_name.into()).expect("Could not get pascal_case_name from object");
            let upper_camel_case_value = object.get(scope, upper_camel_case_name.into()).expect("Could not get upper_camel_case_name from object");
            let snake_case_value = object.get(scope, snake_case_name.into()).expect("Could not get snake_case_name from object");
            let upper_snake_case_value = object.get(scope, upper_snake_case_name.into()).expect("Could not get upper_snake_case_name from object");
            let constant_case_value = object.get(scope, constant_case_name.into()).expect("Could not get constant_case_name from object");
            let kebab_case_value = object.get(scope, kebab_case_name.into()).expect("Could not get kebab_case_name from object");
            let cobol_case_value = object.get(scope, cobol_case_name.into()).expect("Could not get cobol_case_name from object");
            let upper_kebab_case_value = object.get(scope, upper_kebab_case_name.into()).expect("Could not get upper_kebab_case_name from object");
            let train_case_value = object.get(scope, train_case_name.into()).expect("Could not get train_case_name from object");
            let flat_case_value = object.get(scope, flat_case_name.into()).expect("Could not get flat_case_name from object");
            let upper_flat_case_value = object.get(scope, upper_flat_case_name.into()).expect("Could not get upper_flat_case_name from object");
            let alternating_case_value = object.get(scope, alternating_case_name.into()).expect("Could not get alternating_case_name from object");

            assert!(upper_case_value.is_string());
            assert_eq!(upper_case_value.to_rust_string_lossy(scope), "YOLO SWAG");
            assert!(lower_case_value.is_string());
            assert_eq!(lower_case_value.to_rust_string_lossy(scope), "yolo swag");
            assert!(title_case_value.is_string());
            assert_eq!(title_case_value.to_rust_string_lossy(scope), "Yolo Swag");
            assert!(toggle_case_value.is_string());
            assert_eq!(toggle_case_value.to_rust_string_lossy(scope), "yOLO sWAG");
            assert!(camel_case_value.is_string());
            assert_eq!(camel_case_value.to_rust_string_lossy(scope), "yoloSwag");
            assert!(pascal_case_value.is_string());
            assert_eq!(pascal_case_value.to_rust_string_lossy(scope), "YoloSwag");
            assert!(upper_camel_case_value.is_string());
            assert_eq!(upper_camel_case_value.to_rust_string_lossy(scope), "YoloSwag");
            assert!(snake_case_value.is_string());
            assert_eq!(snake_case_value.to_rust_string_lossy(scope), "yolo_swag");
            assert!(upper_snake_case_value.is_string());
            assert_eq!(upper_snake_case_value.to_rust_string_lossy(scope), "YOLO_SWAG");
            assert!(constant_case_value.is_string());
            assert_eq!(constant_case_value.to_rust_string_lossy(scope), "YOLO_SWAG");
            assert!(kebab_case_value.is_string());
            assert_eq!(kebab_case_value.to_rust_string_lossy(scope), "yolo-swag");
            assert!(cobol_case_value.is_string());
            assert_eq!(cobol_case_value.to_rust_string_lossy(scope), "YOLO-SWAG");
            assert!(upper_kebab_case_value.is_string());
            assert_eq!(upper_kebab_case_value.to_rust_string_lossy(scope), "YOLO-SWAG");
            assert!(train_case_value.is_string());
            assert_eq!(train_case_value.to_rust_string_lossy(scope), "Yolo-Swag");
            assert!(flat_case_value.is_string());
            assert_eq!(flat_case_value.to_rust_string_lossy(scope), "yoloswag");
            assert!(upper_flat_case_value.is_string());
            assert_eq!(upper_flat_case_value.to_rust_string_lossy(scope), "YOLOSWAG");
            assert!(alternating_case_value.is_string());
            assert_eq!(alternating_case_value.to_rust_string_lossy(scope), "yOlO sWaG");
        }
        else {
            assert!(scope.has_caught());
            report_exceptions(scope);
        }
    }
}
