use std::fmt::{Debug, Display};
use serde::{Deserialize, Serialize};
use smol_str::SmolStr;
use ts_rs::TS;

#[derive(TS, Serialize, Deserialize, Clone, PartialEq, Eq, Hash, Ord, PartialOrd)]
#[ts(export_to = "./utils.ts", export)]
pub struct ItemId(pub SmolStr);

pub trait ToJsonRepr {
    fn to_json_repr(&self) -> String;
}

impl ToJsonRepr for ItemId {
    fn to_json_repr(&self) -> String {
        format!("\"{}\"", self.0.as_str())
    }
}

impl ToJsonRepr for Vec<ItemId> {
    fn to_json_repr(&self) -> String {
        let mut buf = String::with_capacity(self.len() * 10);
        buf.push_str("[");
        self.iter().enumerate().for_each(|(i, item_id)| {
            buf.push_str(&item_id.to_json_repr());
            if i != self.len() - 1 {
                buf.push_str(", ");
            }
        });
        buf.push_str("]");
        buf
    }
}

impl Debug for ItemId {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        Debug::fmt(self.0.as_str(), f)
    }
}

impl Display for ItemId {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        Display::fmt(self.0.as_str(), f)
    }
}

impl AsRef<str> for ItemId {
    fn as_ref(&self) -> &str {
        self.0.as_str()
    }
}

impl From<&str> for ItemId {
    fn from(value: &str) -> Self {
        Self(value.into())
    }
}

impl From<&SmolStr> for ItemId {
    fn from(value: &SmolStr) -> Self {
        Self(value.clone())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_item_id_display_and_debug() {
        let item_id = ItemId::from("yolo");
        let display = format!("{}", item_id);
        let debug = format!("{:?}", item_id);
        assert_eq!(display, "yolo");
        assert_eq!(debug, "\"yolo\"");
    }
}