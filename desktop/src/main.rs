#![cfg_attr(
    all(not(debug_assertions), target_os = "windows"),
    windows_subsystem = "windows"
)]

use log::info;
use fast_log::consts::LogSize;
use fast_log::plugin::file_split::{KeepType, Rolling, RollingType};
use fast_log::plugin::packer::LogPacker;
use fast_log::Config;
use tauri::{Manager, WindowEvent};
use crate::code_executor::initialize_v8;
use crate::service::{register_handlers, register_states};

mod code_executor;
mod data;
mod service;
mod util;

fn main() {
    init_logger();
    initialize_v8();

    let builder = tauri::Builder::default()
        .plugin(tauri_plugin_global_shortcut::Builder::new().build())
        .plugin(tauri_plugin_dialog::init())
        .setup(|app| {
            register_states(app);
            Ok(())
        })
        .on_window_event(|window, event| match event {
            WindowEvent::Destroyed => {
                let win_label = window.label();
                let num = window.app_handle().webview_windows().len();
                info!("Window '{win_label}' destroyed. Windows left: {num}");
            }
            _ => {}
        });
    register_handlers(builder)
        .run(tauri::generate_context!())
        .expect("Error while running Pezna Modeller application");
}

fn init_logger() {
    fast_log::init(Config::new().file_split(
        "target/logs/",
        Rolling::new(RollingType::BySize(LogSize::MB(2))),
        KeepType::KeepNum(10),
        LogPacker {},
    ))
    .unwrap();
}
