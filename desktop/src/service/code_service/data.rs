use serde::{Deserialize, Serialize};
use ts_rs::TS;
use std::collections::HashSet;
use crate::code_executor::error::ScriptError;
use crate::code_executor::data::ScriptResult;
use crate::util::item_id::ItemId;

#[derive(TS, Serialize)]
#[serde(rename_all = "camelCase", rename_all_fields = "camelCase", tag = "type")]
#[ts(export_to = "./code-service-data.ts", export)]
pub enum CodeServiceSuccess {
    RunSuccess {
        id: ItemId,
        result: ScriptResult,
    },
}

#[derive(TS, Serialize)]
#[serde(rename_all = "camelCase", rename_all_fields = "camelCase", tag = "type")]
#[ts(export_to = "./code-service-data.ts", export)]
pub enum CodeServiceError {
    RunFailure {
        id: ItemId,
        error: ScriptError,
    },
    NoCodeFound {
        id: ItemId,
    },
}

#[derive(TS, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./code-service-data.ts", export)]
pub struct SetExportCodeContainerIds {
    pub export_id: ItemId,
    pub code_container_ids: HashSet<ItemId>,
}
