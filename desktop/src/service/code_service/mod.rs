pub mod data;

use std::collections::{HashMap, HashSet};
use std::time::Instant;
use log::{debug, error};
use parking_lot::RwLock;
use tauri::{App, Manager, State};
use v8::{Context, ContextScope, HandleScope, Isolate, TryCatch};
use crate::code_executor::struct_output::create_struct_outputs;
use crate::code_executor::data::RunCodeContainer;
use crate::code_executor::code_container::CodeContainer;
use crate::service::code_service::data::{CodeServiceError, CodeServiceSuccess, SetExportCodeContainerIds};
use crate::service::model_service::ModelServiceState;
use crate::util::item_id::ItemId;

pub struct CodeState {
    containers: RwLock<HashMap<ItemId, CodeContainer>>,
    export_code_container_ids: RwLock<HashMap<ItemId, HashSet<ItemId>>>,
}

pub fn register_state(app: &mut App) {
    let containers = create_struct_outputs();
    app.manage(CodeState {
        containers: RwLock::new(containers),
        export_code_container_ids: RwLock::new(Default::default()),
    });
}

#[tauri::command(async)]
pub fn set_code_container(container: CodeContainer, code_state: State<CodeState>) -> Result<(), CodeServiceError> {
    debug!("set_code_container: {:#?}", container);
    let mut state_containers = code_state.containers.write();
    let isolate = &mut Isolate::new(Default::default());
    let handle_scope = &mut HandleScope::new(isolate);
    let context = Context::new(handle_scope, Default::default());
    let scope = &mut ContextScope::new(handle_scope, context);
    let scope = &mut TryCatch::new(scope);
    
    let cc = CodeContainer::new(container.id.clone(), container.code, container.type_);
    // with this, we only bother checking for compiletime errors, not runtime errors
    let res = cc.compile(scope);
    if let Err(err) = res {
        Err(CodeServiceError::RunFailure { id: container.id, error: err })
    }
    else {
        state_containers.insert(container.id.clone(), cc);
        Ok(())
    }
}

#[tauri::command(async)]
pub fn delete_code_container(id: ItemId, code_state: State<CodeState>) {
    debug!("delete_code_container: {:#?}", id);
    let mut state_containers = code_state.containers.write();
    state_containers.remove(&id);
}

#[tauri::command(async)]
pub fn run_code_container(
    code_state: State<CodeState>,
    model_state: State<ModelServiceState>,
    data: RunCodeContainer,
) -> Result<CodeServiceSuccess, CodeServiceError> {
    let track_id = rand::random::<u32>();
    debug!("run_code_container {track_id}: {:#?}", data);
    let start = Instant::now();
    
    let state_containers = code_state.containers.read();
    let Some(cc) = state_containers.get(&data.id) else {
        error!("No code container found for id {}", data.id);
        return Err(CodeServiceError::NoCodeFound { id: data.id });
    };
    
    let root_state_guard = model_state.root_state.read();
    let res = cc.execute_code_container(&data, &*root_state_guard);
    
    let duration = start.elapsed().as_millis();
    debug!("finish run_code_container {track_id}: {:?} elapsed: {duration}ms", data.id);
    
    match res {
        Ok(result) => Ok(CodeServiceSuccess::RunSuccess {
            id: data.id,
            result,
        }),
        Err(error) => Err(CodeServiceError::RunFailure {
            id: data.id,
            error,
        }),
    }
}

#[tauri::command(async)]
pub fn set_export_code_container_ids(data: SetExportCodeContainerIds, code_state: State<CodeState>) {
    debug!("set_export_code_container_ids: {:#?}", data);
    let code_container_ids = HashSet::from_iter(data.code_container_ids);
    let mut export_code_container_ids = code_state.export_code_container_ids.write();
    if let Some(existing_ids) = export_code_container_ids.get(&data.export_id) {
        let mut containers = code_state.containers.write();
        // If there is a code id in the existing ids, but not in the new ids,
        // it should be removed to avoid memory leaks.
        let remove_ids = existing_ids - &code_container_ids;
        for id in remove_ids {
            containers.remove(&id);
        }
    }
    
    export_code_container_ids.insert(data.export_id.clone(), code_container_ids);
}

#[tauri::command(async)]
pub fn delete_export_code_containers(export_id: ItemId, code_state: State<CodeState>) {
    debug!("delete_export_code_containers: {:#?}", export_id);
    let mut containers = code_state.containers.write();
    let mut export_code_container_ids = code_state.export_code_container_ids.write();
    if let Some(existing_ids) = export_code_container_ids.get(&export_id) {
        debug!("export container existing ids: {:#?}", existing_ids);
        for id in existing_ids {
            containers.remove(id);
        }
    }
    
    export_code_container_ids.remove(&export_id);
}
