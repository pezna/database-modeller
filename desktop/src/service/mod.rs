use std::fmt::Display;
use serde::{Serialize, Deserialize};
use tauri::{App, Runtime};
use ts_rs::TS;

mod code_service;
mod model_service;

pub fn register_states(app: &mut App) {
    model_service::register_state(app);
    code_service::register_state(app);
}

pub fn register_handlers<R: Runtime>(builder: tauri::Builder<R>) -> tauri::Builder<R> {
    builder.invoke_handler(tauri::generate_handler![
        code_service::set_code_container,
        code_service::run_code_container,
        code_service::delete_code_container,
        code_service::set_export_code_container_ids,
        code_service::delete_export_code_containers,
        model_service::new_model_state,
        model_service::save_model_file,
        model_service::open_model_file,
        model_service::save_app_settings,
        model_service::load_app_settings,
        model_service::update_ui_state,
    ])
}

#[derive(TS, Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase", tag = "type", content = "data")]
#[ts(export_to = "./service-data.ts", export)]
pub enum ServiceError {
    Io(String),
    Serde(String),
    Tauri(String),
}

impl From<serde_yaml::Error> for ServiceError {
    fn from(e: serde_yaml::Error) -> Self {
        Self::Serde(e.to_string())
    }
}

impl From<tauri::Error> for ServiceError {
    fn from(e: tauri::Error) -> Self {
        Self::Tauri(e.to_string())
    }
}

impl From<std::io::Error> for ServiceError {
    fn from(e: std::io::Error) -> Self {
        Self::Io(e.to_string())
    }
}

impl Display for ServiceError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let str = match self {
            Self::Serde(e) => e.to_string(),
            Self::Tauri(e) => e.to_string(),
            Self::Io(e) => e.to_string(),
        };
        write!(f, "{}", str)
    }
}