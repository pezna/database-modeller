use std::fs;
use std::path::PathBuf;
use parking_lot::RwLock;
use tauri::{App, Manager, State};
use crate::data::board_state::{DiagramData};
use crate::data::root_state::BackendRootState;
use crate::data::model_save_data::{ModelSaveData};
use crate::data::settings_state::SettingsState;
use crate::data::ui_state::UiState;
use crate::service::model_service::data::{OpenModelError, OpenModelSuccess, SaveModelError, SaveModelSuccess};
use crate::service::ServiceError;
use crate::util::item_id::ItemId;

pub mod data;

pub struct ModelServiceState {
    settings_file_path: PathBuf,
    ui_state_dir: PathBuf,
    pub root_state: RwLock<BackendRootState>,
}

pub fn register_state(app: &mut App) {
    let config_dir = app.path().app_config_dir().unwrap();
    ensure_dir_exists(&config_dir);
    
    let mut settings_file_path = config_dir.clone();
    settings_file_path.push("settings.yaml");
    
    let mut ui_state_dir = config_dir.clone();
    ui_state_dir.push("us");
    ensure_dir_exists(&ui_state_dir);
    
    app.manage(ModelServiceState {
        settings_file_path,
        ui_state_dir,
        root_state: RwLock::new(BackendRootState::default()),
    });
}

fn ensure_dir_exists(dir: &PathBuf) {
    if dir.exists() {
        return;
    }
    
    fs::create_dir(&dir).unwrap_or(());
}

fn get_ui_state_save_path(dir: &PathBuf, project_id: &ItemId) -> PathBuf {
    dir.join(project_id.as_ref())
}

#[tauri::command(async)]
pub fn new_model_state(
    state: State<ModelServiceState>,
    project_id: ItemId,
    copy_schema_settings: bool,
) -> BackendRootState {
    log::debug!("new_model_state. {project_id} {copy_schema_settings}");
    let mut state_guard = state.root_state.write();
    let root_state = &mut *state_guard;
    let mut new_state = BackendRootState::default();
    new_state.general.project_id = Some(project_id);
    if copy_schema_settings {
        new_state.schema.settings = root_state.schema.settings.clone();
    }
    *root_state = new_state;
    (&*root_state).clone()
}

#[tauri::command(async)]
pub fn save_model_file(
    state: State<ModelServiceState>,
    mut root_state: BackendRootState,
    diagram_data: DiagramData,
    new_file_path: Option<String>,
) -> Result<SaveModelSuccess, SaveModelError> {
    let mut state_guard = state.root_state.write();
    let file_path = match new_file_path {
        Some(fp) => PathBuf::from(fp),
        None => match root_state.get_model_file_path() {
            Some(fp) => PathBuf::from(fp),
            None => {
                return Err(SaveModelError::Io("No file path given to save the model".to_owned()));
            }
        }
    };
    let Some(project_id) = &root_state.get_project_id() else {
        return Err(SaveModelError::NoProjectId);
    };
    
    let save_data = ModelSaveData::new(
        &root_state,
        &diagram_data,
    );
    // now save to file
    if let Err(e) = save_data.save(file_path.clone()) {
        return Err(SaveModelError::Io(e.to_string()));
    }
    
    // update path in the root state
    root_state.just_saved(file_path.to_str().unwrap().to_string());
    
    // Now save the UiState
    let ui_state_path = get_ui_state_save_path(&state.ui_state_dir, &project_id);
    let ui_state_bytes = serde_yaml::to_string(&root_state.ui).expect("Could not serialize UiState");
    if let Err(e) = fs::write(ui_state_path, ui_state_bytes) {
        return Err(SaveModelError::Io(e.to_string()));
    }
    
    let general_state = root_state.general.clone();
    *state_guard = root_state;
    
    Ok(SaveModelSuccess {
        general_state,
    })
}

#[tauri::command(async)]
pub fn open_model_file(
    state: State<ModelServiceState>,
    file_path: String,
) -> Result<OpenModelSuccess, OpenModelError> {
    let mut state_guard = state.root_state.write();
    let root_state = &mut *state_guard;
    let file_path = PathBuf::from(file_path);
    if !file_path.exists() {
        return Err(OpenModelError::Io(format!("File {file_path:?} does not exist")));
    }
    
    let save_data = ModelSaveData::load(file_path.clone())?;
    let ui_state_path = get_ui_state_save_path(&state.ui_state_dir, &save_data.meta.project_id);
    let ui_state_bytes = fs::read(&ui_state_path)?;
    let ui_state: UiState = serde_yaml::from_slice(&ui_state_bytes).unwrap_or_default();
    // fill the root state with all the new data to retrieve the diagram data
    let diagram_data = root_state.replace_with(save_data, ui_state);
    let file_path = file_path.to_str().unwrap().to_string();
    root_state.just_saved(file_path);
    
    Ok(OpenModelSuccess {
        root_state: root_state.clone(),
        diagram_data,
    })
}

#[tauri::command(async)]
pub fn save_app_settings(state: State<ModelServiceState>, settings: SettingsState) -> Result<(), ServiceError> {
    let mut root_guard = state.root_state.write();
    let root_state = &mut *root_guard;
    root_state.settings = settings;
    let bytes = serde_yaml::to_string(&root_state.settings)?;
    fs::write(&state.settings_file_path, &bytes)?;
    Ok(())
}

#[tauri::command(async)]
pub fn load_app_settings(state: State<ModelServiceState>) -> SettingsState {
    let mut root_guard = state.root_state.write();
    let root_state = &mut *root_guard;
    let Ok(bytes) = fs::read(&state.settings_file_path) else {
        log::error!("Could not open settings file: {:?}", &state.settings_file_path);
        return root_state.settings.clone();
    };
    match serde_yaml::from_slice::<SettingsState>(bytes.as_slice()) {
        Ok(state) => {
            root_state.settings = state.clone();
            state
        },
        Err(e) => {
            log::error!("Could not deserialize settings file: {}", e);
            root_state.settings.clone()
        },
    }
}

#[tauri::command(async)]
pub fn update_ui_state(state: State<ModelServiceState>, ui: UiState) {
    let mut root_guard = state.root_state.write();
    let root_state = &mut *root_guard;
    root_state.ui = ui;
}
