use std::io;
use serde::{Deserialize, Serialize};
use ts_rs::TS;
use crate::data::board_state::DiagramData;
use crate::data::general_state::GeneralState;
use crate::data::root_state::BackendRootState;
use crate::service::ServiceError;

#[derive(TS, Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase", tag = "type", content = "data")]
#[ts(export_to = "./model-service-data.ts", export)]
pub enum SaveModelError {
    Io(String),
    NoProjectId,
    UserCancelled,
}

impl From<io::Error> for SaveModelError {
    fn from(value: io::Error) -> Self {
        Self::Io(value.to_string())
    }
}

#[derive(TS, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./model-service-data.ts", export)]
pub struct SaveModelSuccess {
    pub general_state: GeneralState,
}

#[derive(TS, Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase", tag = "type", content = "data")]
#[ts(export_to = "./model-service-data.ts", export)]
pub enum OpenModelError {
    Io(String),
    Other(String),
    UserCancelled,
}

impl From<ServiceError> for OpenModelError {
    fn from(value: ServiceError) -> Self {
        match value {
            ServiceError::Io(err) => Self::Io(err),
            _ => Self::Other(value.to_string()),
        }
    }
}

impl From<io::Error> for OpenModelError {
    fn from(value: io::Error) -> Self {
        Self::Io(value.to_string())
    }
}

#[derive(TS, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./model-service-data.ts", export)]
pub struct OpenModelSuccess {
    pub root_state: BackendRootState,
    pub diagram_data: DiagramData,
}
