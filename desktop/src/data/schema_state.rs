use std::collections::{HashMap, HashSet};
use serde::{Deserialize, Serialize};
use smol_str::SmolStr;
use ts_rs::TS;
use crate::data::export_state::SqlExportOptions;
use crate::data::HasId;
use crate::util::item_id::ItemId;

#[derive(TS, Serialize, Deserialize, Debug, Clone, Default)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./schema-state.ts", export)]
pub struct SchemaState {
    pub namespaces: HashMap<ItemId, NamespaceData>,
    pub tables: HashMap<ItemId, TableData>,
    pub columns: HashMap<ItemId, ColumnData>,
    pub column_types: HashMap<ItemId, ColumnTypeData>,
    pub relationships: HashMap<ItemId, RelationshipData>,
    pub indexes: HashMap<ItemId, IndexData>,
    pub uniques: HashMap<ItemId, UniqueData>,
    pub primary_keys: HashMap<ItemId, PrimaryKeyData>,
    pub settings: SchemaSettingsData,
}

impl SchemaState {
    pub fn save_in_multiple_files(&self) -> bool {
        self.settings.files.save_files.use_separate_save_files
    }
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./schema-state.ts", export)]
pub struct MixinInfo {
    #[ts(optional)]
    pub belongs_to_mixin_table: Option<ItemId>,
    #[ts(optional)]
    pub belongs_to_mixin_schema_item: Option<ItemId>,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./schema-state.ts", export)]
pub struct NamespaceData {
    pub id: ItemId,
    pub name: SmolStr,
    #[ts(optional)]
    pub background_color: Option<SmolStr>,
    #[ts(optional)]
    pub foreground_color: Option<SmolStr>,
    #[ts(optional)]
    pub border_color: Option<SmolStr>,
}

impl HasId for NamespaceData {
    fn id(&self) -> &ItemId {
        &self.id
    }
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./schema-state.ts", export)]
pub struct TableData {
    pub id: ItemId,
    pub name: SmolStr,
    pub namespace_id: ItemId,
    pub column_ids: Vec<ItemId>,
    pub relationship_ids: Vec<ItemId>,
    pub primary_key_id: Option<ItemId>,
    pub mixin_table_ids: Vec<ItemId>,
    pub is_mixin: bool,
}

impl HasId for TableData {
    fn id(&self) -> &ItemId {
        &self.id
    }
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./schema-state.ts", export)]
pub struct ColumnData {
    pub id: ItemId,
    pub name: SmolStr,
    pub table_id: ItemId,
    pub type_id: ItemId,
    pub options: ColumnOptionValueContainer,
    #[serde(flatten)]
    pub mixin_info: MixinInfo,
}

impl HasId for ColumnData {
    fn id(&self) -> &ItemId {
        &self.id
    }
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
#[ts(use_ts_enum, export_to = "./schema-state.ts", export)]
pub enum ColumnOptionType {
    DefaultValue,
    Index,
    NotNull,
    Unique,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./schema-state.ts", export)]
pub struct ColumnOptionValueContainer {
    #[ts(optional)]
    pub default_value: Option<String>,
    #[ts(optional)]
    pub index: Option<IndexOptionData>,
    #[ts(optional)]
    pub not_null: Option<bool>,
    #[ts(optional)]
    pub unique: Option<UniqueOptionData>,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./schema-state.ts", export)]
pub struct IndexOptionData {
    pub index_ids: Vec<String>,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./schema-state.ts", export)]
pub struct UniqueOptionData {
    pub unique_ids: Vec<String>,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./schema-state.ts", export)]
pub struct ColumnTypeData {
    pub id: ItemId,
    pub name: SmolStr,
    pub column_ids: Vec<ItemId>,
    pub languages: Vec<ColumnLanguageData>,
}

impl HasId for ColumnTypeData {
    fn id(&self) -> &ItemId {
        &self.id
    }
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./schema-state.ts", export)]
pub struct ColumnLanguageData {
    pub name: SmolStr,
    pub value: SmolStr,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./schema-state.ts", export)]
pub struct RelationshipData {
    pub id: ItemId,
    pub name: SmolStr,
    pub child_table_id: ItemId,
    pub child_column_ids: Vec<ItemId>,
    pub parent_table_id: ItemId,
    pub parent_column_ids: Vec<ItemId>,
    #[ts(optional)]
    pub on_update: Option<ForeignKeyChangeAction>,
    #[ts(optional)]
    pub on_delete: Option<ForeignKeyChangeAction>,
    #[ts(optional)]
    pub deferrability: Option<ForeignKeyDeferrability>,
    #[ts(optional)]
    pub is_not_exported: Option<bool>,
    #[serde(flatten)]
    pub mixin_info: MixinInfo,
}

impl HasId for RelationshipData {
    fn id(&self) -> &ItemId {
        &self.id
    }
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
#[ts(use_ts_enum, export_to = "./schema-state.ts", export)]
pub enum ForeignKeyChangeAction {
    SetNull,
    SetDefault,
    Cascade,
    Restrict,
    NoAction,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
#[ts(use_ts_enum, export_to = "./schema-state.ts", export)]
pub enum ForeignKeyDeferrability {
    NotDeferrable,
    InitiallyDeferred,
    InitiallyImmediate,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./schema-state.ts", export)]
pub struct IndexData {
    pub id: ItemId,
    pub name: SmolStr,
    pub table_id: ItemId,
    pub column_ids: Vec<ItemId>,
    pub directions: Vec<ColumnIndexDirection>,
    #[serde(flatten)]
    pub mixin_info: MixinInfo,
}

impl HasId for IndexData {
    fn id(&self) -> &ItemId {
        &self.id
    }
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
#[ts(use_ts_enum, export_to = "./schema-state.ts", export)]
pub enum ColumnIndexDirection {
    Ascending,
    Descending,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./schema-state.ts", export)]
pub struct UniqueData {
    pub id: ItemId,
    pub name: SmolStr,
    pub table_id: ItemId,
    pub column_ids: Vec<ItemId>,
    pub directions: Vec<ColumnIndexDirection>,
    pub is_index: bool,
    #[ts(optional)]
    pub on_conflict: Option<ConflictClause>,
    #[serde(flatten)]
    pub mixin_info: MixinInfo,
}

impl HasId for UniqueData {
    fn id(&self) -> &ItemId {
        &self.id
    }
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
#[ts(use_ts_enum, export_to = "./schema-state.ts", export)]
pub enum ConflictClause {
    Rollback,
    Abort,
    Fail,
    Ignore,
    Replace,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./schema-state.ts", export)]
pub struct PrimaryKeyData {
    pub id: ItemId,
    pub name: SmolStr,
    pub table_id: ItemId,
    pub column_ids: Vec<ItemId>,
    pub directions: Vec<ColumnIndexDirection>,
    #[ts(optional)]
    pub on_conflict: Option<ConflictClause>,
    #[serde(flatten)]
    pub mixin_info: MixinInfo,
}

impl HasId for PrimaryKeyData {
    fn id(&self) -> &ItemId {
        &self.id
    }
}

#[derive(TS, Serialize, Deserialize, Debug, Clone, Default)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./schema-state.ts", export)]
pub struct SchemaSettingsData {
    #[ts(inline)]
    pub schema: ObjSchema,
    #[ts(inline)]
    pub files: ObjFiles,
    #[ts(inline)]
    pub export: ObjExport,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone, Default)]
#[serde(rename_all = "camelCase")]
pub struct ObjSchema {
    #[ts(inline)]
    pub mixin_tables: ObjMixinTables,
    #[ts(inline)]
    pub generator_code: ObjGenCode,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone, Default)]
#[serde(rename_all = "camelCase")]
pub struct ObjMixinTables {
    pub default_mixin_table_ids: HashSet<ItemId>,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct ObjGenCode {
    pub primary_key_name_generator_code: String,
    pub relationship_name_generator_code: String,
    pub index_name_generator_code: String,
    pub unique_name_generator_code: String,
}

impl Default for ObjGenCode {
    fn default() -> Self {
        Self {
            primary_key_name_generator_code: String::from(
r#"function generate() {
    return `pk_${table.name}_${columns.map(c => c.name).join('_')}`;
}"#
            ),
            relationship_name_generator_code: String::from(
r#"function generate() {
    return `fk_${childTable.name}_${childColumns.map(c => c.name).join('_')}_${parentTable.name}`;
}"#
            ),
            index_name_generator_code: String::from(
r#"function generate() {
    return `ix_${table.name}_${columns.map(c => c.name).join('_')}`;
}"#
            ),
            unique_name_generator_code: String::from(
r#"function generate() {
    return `uq_${table.name}_${columns.map(c => c.name).join('_')}`;
}"#
            ),
        }
    }
}

#[derive(TS, Serialize, Deserialize, Debug, Clone, Default)]
#[serde(rename_all = "camelCase")]
pub struct ObjFiles {
    #[ts(inline)]
    pub save_files: ObjSaveFiles,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone, Default)]
#[serde(rename_all = "camelCase")]
pub struct ObjSaveFiles {
    pub use_separate_save_files: bool,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone, Default)]
#[serde(rename_all = "camelCase")]
pub struct ObjExport {
    #[ts(inline)]
    pub sql: SqlExportOptions,
}
