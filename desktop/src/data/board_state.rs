use std::collections::HashMap;
use serde::{Deserialize, Serialize};
use smol_str::SmolStr;
use ts_rs::TS;
use crate::data::general_state::AppMode;
use crate::data::HasId;
use crate::util::item_id::ItemId;

#[derive(TS, Serialize, Deserialize, Debug, Clone, Default)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./board-state.ts", export)]
pub struct BoardState {
    pub notes: HashMap<ItemId, NoteData>,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./board-state.ts", export)]
pub struct NoteData {
    pub id: ItemId,
    pub app_mode: AppMode,
    pub section_id: ItemId, // namespace or machine
    pub text: String,
}

impl HasId for NoteData {
    fn id(&self) -> &ItemId {
        &self.id
    }
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "../board/board-data.ts", export)]
pub struct DiagramData {
    pub item_dimensions: HashMap<ItemId, DimensionsData>,
    pub board_dimensions: HashMap<ItemId, DimensionsData>,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "../board/board-data.ts", export)]
pub struct DimensionsData {
    pub x: i32,
    pub y: i32,
    pub width: i32,
    pub height: i32,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "../board/board-data.ts", export)]
pub struct ColorData {
    #[ts(optional)]
    pub background_color: Option<SmolStr>,
    #[ts(optional)]
    pub foreground_color: Option<SmolStr>,
    #[ts(optional)]
    pub border_color: Option<SmolStr>,
}
