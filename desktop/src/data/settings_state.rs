use serde::{Deserialize, Serialize};
use smol_str::SmolStr;
use ts_rs::TS;

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./settings-state.ts", export)]
pub struct SettingsState {
    #[ts(inline)]
    pub general: ObjGeneral,
    #[ts(inline)]
    database_board: ObjDatabaseBoard,
    #[ts(inline)]
    machine_board: ObjMachineBoard,
}

impl Default for SettingsState {
    fn default() -> Self {
        Self {
            general: ObjGeneral {
                files: ObjFiles { most_recent_model_files: vec![] },
                start_up: ObjStartUp { reopen_last_model_file: false },
            },
            database_board: ObjDatabaseBoard {
                table: ObjTable {
                    show_keys: true,
                    background_color: "#ffffff".into(),
                    foreground_color: "#171717".into(),
                    border_color: "#111111".into(),
                },
                column: ObjColumn {
                    show_type: false,
                    style_primary_keys: "none".into(),
                    style_foreign_keys: "none".into(),
                    indicate_primary_key: true,
                    indicate_foreign_key: true,
                    indicate_not_null: true,
                    indicate_unique: true,
                    indicate_index: true,
                },
                relationships: ObjRelationship {
                    show_lines: true,
                    line_color: "#ff0000".into(),
                    not_exported_line_color: "#ff00ff".into(),
                    line_width: 2,
                    line_connect_location: "column".into(),
                    show_other_namespace_table_name: true,
                },
                mixins: ObjMixin {
                    mixin_background_color: "#cbe8fb".into(),
                    apply_color_to_columns: true,
                },
            },
            machine_board: ObjMachineBoard {
                components: ObjComponent {
                    background_color: "#ffffff".into(),
                    foreground_color: "#2e2ed1".into(),
                },
                connectors: ObjConnector {
                    line_color: "#5e6097".into(),
                    line_width: 2,
                },
            },
        }
    }
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct ObjGeneral {
    #[ts(inline)]
    pub files: ObjFiles,
    #[ts(inline)]
    pub start_up: ObjStartUp,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct ObjFiles {
    pub most_recent_model_files: Vec<String>,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct ObjStartUp {
    pub reopen_last_model_file: bool,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
struct ObjDatabaseBoard {
    #[ts(inline)]
    table: ObjTable,
    #[ts(inline)]
    column: ObjColumn,
    #[ts(inline)]
    relationships: ObjRelationship,
    #[ts(inline)]
    mixins: ObjMixin,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
struct ObjTable {
    show_keys: bool,
    background_color: SmolStr,
    foreground_color: SmolStr,
    border_color: SmolStr,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
struct ObjColumn {
    show_type: bool,
    style_primary_keys: SmolStr,
    style_foreign_keys: SmolStr,
    indicate_primary_key: bool,
    indicate_foreign_key: bool,
    indicate_not_null: bool,
    indicate_unique: bool,
    indicate_index: bool,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
struct ObjRelationship {
    show_lines: bool,
    line_color: SmolStr,
    not_exported_line_color: SmolStr,
    line_width: u32,
    line_connect_location: SmolStr,
    show_other_namespace_table_name: bool,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
struct ObjMixin {
    mixin_background_color: SmolStr,
    apply_color_to_columns: bool,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
struct ObjMachineBoard {
    #[ts(inline)]
    components: ObjComponent,
    #[ts(inline)]
    connectors: ObjConnector,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
struct ObjComponent {
    background_color: SmolStr,
    foreground_color: SmolStr,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
struct ObjConnector {
    line_color: SmolStr,
    line_width: u32,
}
