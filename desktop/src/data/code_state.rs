use std::collections::HashMap;
use serde::{Deserialize, Serialize};
use smol_str::SmolStr;
use ts_rs::TS;
use crate::data::HasId;
use crate::util::item_id::ItemId;

#[derive(TS, Serialize, Deserialize, Debug, Clone, Default)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./code-state.ts", export)]
pub struct CodeState {
    pub sql_documents: HashMap<ItemId, SqlDocumentData>,
    pub struct_outputs: HashMap<ItemId, StructOutputData>,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./code-state.ts", export)]
pub struct SqlDocumentData {
    pub id: ItemId,
    pub name: SmolStr,
    pub folder_path: String,
    pub code: String,
    pub schema_usages: Vec<SqlDocumentSchemaUsage>,
}

impl HasId for SqlDocumentData {
    fn id(&self) -> &ItemId {
        &self.id
    }
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase", rename_all_fields = "camelCase", tag = "type")]
#[ts(export_to = "./code-state.ts", export)]
pub enum SqlDocumentSchemaUsage {
    Table { table_id: ItemId, name: SmolStr, usage: SchemaUseItem },
    Column { column_id: ItemId, table_id: ItemId, name: SmolStr, usage: SchemaUseItem },
    // when an unknown identifier occurs
    Identifier { potential_schema_type: Option<SmolStr>, usage: SchemaUseItem },
    BindParameter { usage: SchemaUseItem },
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./code-state.ts", export)]
pub struct SchemaUseItem {
    pub identifier: Option<SmolStr>,
    pub line_number: u32,
    pub column_number: u32,
    pub location_start_index: u32,
    pub location_stop_index: u32,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./code-state.ts", export)]
pub struct StructOutputData {
    pub id: ItemId,
    pub name: SmolStr,
    pub folder_path: String,
    pub code: String,
    pub included_table_ids: Vec<ItemId>,
    pub excluded_table_ids: Vec<ItemId>,
    pub single_file_output: bool,
    pub options: OutputLanguageOptions,
}

impl HasId for StructOutputData {
    fn id(&self) -> &ItemId {
        &self.id
    }
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./code-state.ts", export)]
pub enum CaseChoice {
    CamelCase,
    PascalCase,
    SnakeCase,
    UpperSnakeCase,
    KebabCase,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase", tag = "type")]
#[ts(export_to = "./code-state.ts", export)]
pub enum OutputLanguageOptions {
    Cpp(CppOutputOptions),
    Csharp(CsharpOutputOptions),
    Java(JavaOutputOptions),
    Javascript(JavascriptOutputOptions),
    Python(PythonOutputOptions),
    Rust(RustOutputOptions),
    Typescript(TypescriptOutputOptions),
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./code-state.ts", export)]
pub struct CppOutputOptions {
    pub class_name_prefix: SmolStr,
    pub class_name_suffix: SmolStr,
    pub class_name_case: Option<CaseChoice>,
    pub variable_case: Option<CaseChoice>,
    pub file_name_case: Option<CaseChoice>,
    pub file_name_suffix: SmolStr,
    pub file_extension: SmolStr,
    pub make_struct: bool,
    pub add_constructor: bool,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./code-state.ts", export)]
pub struct CsharpOutputOptions {
    pub class_name_prefix: SmolStr,
    pub class_name_suffix: SmolStr,
    pub variables_not_properties: bool,
    pub property_init_not_set: bool,
    pub make_fields_virtual: bool,
    pub object_keyword: SmolStr,
    pub use_record_shorthand: bool,
    pub add_constructor: bool,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./code-state.ts", export)]
pub struct JavaOutputOptions {
    pub class_name_prefix: SmolStr,
    pub class_name_suffix: SmolStr,
    pub object_keyword: SmolStr,
    pub make_static: bool,
    pub add_constructor: bool,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./code-state.ts", export)]
pub struct JavascriptOutputOptions {
    pub class_name_prefix: SmolStr,
    pub class_name_suffix: SmolStr,
    pub variable_case: Option<CaseChoice>,
    pub file_name_case: Option<CaseChoice>,
    pub file_name_suffix: SmolStr,
    pub add_constructor: bool,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./code-state.ts", export)]
pub struct TypescriptOutputOptions {
    pub class_name_prefix: SmolStr,
    pub class_name_suffix: SmolStr,
    pub variable_case: Option<CaseChoice>,
    pub file_name_case: Option<CaseChoice>,
    pub file_name_suffix: SmolStr,
    pub object_keyword: SmolStr,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./code-state.ts", export)]
pub struct RustOutputOptions {
    pub struct_name_prefix: SmolStr,
    pub struct_name_suffix: SmolStr,
    pub file_name_suffix: SmolStr,
    pub add_constructor: bool,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./code-state.ts", export)]
pub struct PythonOutputOptions {
    pub class_name_prefix: SmolStr,
    pub class_name_suffix: SmolStr,
    pub file_name_suffix: SmolStr,
    pub make_dataclass: bool,
}
