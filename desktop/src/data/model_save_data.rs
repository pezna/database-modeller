use std::fs;
use std::collections::HashMap;
use std::path::{Path, PathBuf};
use serde::{Deserialize, Serialize};
use serde::de::DeserializeOwned;
use smol_str::SmolStr;
use crate::data::board_state::{BoardState, DiagramData, DimensionsData, NoteData};
use crate::data::code_state::{CodeState, SqlDocumentData, StructOutputData};
use crate::data::export_state::{CustomExport, ExportState, SqlExportHistoryItem};
use crate::data::root_state::BackendRootState;
use crate::data::schema_state::{
    ColumnData, ColumnTypeData, IndexData, NamespaceData, PrimaryKeyData,
    RelationshipData, SchemaSettingsData, SchemaState, TableData, UniqueData,
};
use crate::data::utils::{item_array_to_container, item_container_to_array};
use crate::service::ServiceError;
use crate::util::item_id::ItemId;

pub const MODEL_EXT: &str = "pmodel";
const SCHEMA_MODEL_EXT: &str = "pmodel-schema";
const BOARD_MODEL_EXT: &str = "pmodel-board";
const EXPORT_MODEL_EXT: &str = "pmodel-export";
const CODE_MODEL_EXT: &str = "pmodel-code";

#[derive(Serialize, Deserialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct Meta {
    version: SmolStr,
    pub project_id: ItemId,
    use_multiple_files: bool,
}

impl Meta {
    pub fn new(project_id: ItemId, use_multiple_files: bool) -> Self {
        Self {
            version: "0.1.0".into(),
            project_id,
            use_multiple_files,
        }
    }
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ModelSaveData {
    #[serde(flatten)]
    pub meta: Meta,
    schema: SchemaSaveData,
    board: BoardSaveData,
    export: ExportSaveData,
    code: CodeSaveData,
}

impl ModelSaveData {
    pub fn new(
        root_state: &BackendRootState,
        diagram_data: &DiagramData,
    ) -> Self {
        Self {
            meta: Meta::new(
                root_state.get_project_id().expect("Cannot save without project ID"),
                root_state.schema.save_in_multiple_files(),
            ),
            schema: (&root_state.schema).into(),
            board: BoardSaveData::new(diagram_data, &root_state.board),
            export: (&root_state.export).into(),
            code: (&root_state.code).into(),
        }
    }
    
    pub fn to_states(&self) -> (SchemaState, ExportState, CodeState, BoardState, DiagramData) {
        let schema_state = self.schema.to_state();
        let export_state = self.export.to_state();
        let code_state = self.code.to_state();
        let (board_state, diagram_data) = self.board.to_state();
        
        (schema_state, export_state, code_state, board_state, diagram_data)
    }
    
    pub fn save(&self, file_path: PathBuf) -> Result<(), ServiceError> {
        if self.meta.use_multiple_files {
            self.save_multiple_files(file_path)
        }
        else {
            self.save_single_file(&file_path, self)
        }
    }
    
    fn save_single_file<T: Serialize>(&self, file_path: impl AsRef<Path>, obj: T) -> Result<(), ServiceError> {
        let content = serde_yaml::to_string(&obj)?;
        Ok(fs::write(file_path, &content)?)
    }
    
    fn save_multiple_files(&self, file_path: PathBuf) -> Result<(), ServiceError> {
        let file_path = change_extension(file_path, MODEL_EXT);
        self.save_single_file(&file_path, &self.meta)?;
        
        let file_path = change_extension(file_path, SCHEMA_MODEL_EXT);
        self.save_single_file(&file_path, &self.schema)?;
        
        let file_path = change_extension(file_path, BOARD_MODEL_EXT);
        self.save_single_file(&file_path, &self.board)?;
        
        let file_path = change_extension(file_path, EXPORT_MODEL_EXT);
        self.save_single_file(&file_path, &self.export)?;
        
        let file_path = change_extension(file_path, CODE_MODEL_EXT);
        self.save_single_file(&file_path, &self.code)?;
        
        Ok(())
    }
    
    pub fn load(file_path: PathBuf) -> Result<Self, ServiceError> {
        let content = fs::read(&file_path)?;
        match serde_yaml::from_slice::<ModelSaveData>(&content) {
            Ok(data) => Ok(data),
            Err(_) => {
                let meta = serde_yaml::from_slice::<Meta>(&content)?;
                if !meta.use_multiple_files {
                    return Err(ServiceError::Serde(
                        "Expected use_multiple_files to be true in the model file".to_owned()
                    ))
                }
                Self::load_multiple_files(file_path)
            },
        }
    }
    
    fn load_single_file<T: DeserializeOwned>(file_path: impl AsRef<Path>) -> Result<T, ServiceError> {
        let content = fs::read(&file_path)?;
        let data: T = serde_yaml::from_slice(&content)?;
        Ok(data)
    }
    
    fn load_multiple_files(file_path: PathBuf) -> Result<Self, ServiceError> {
        fn error_return(ext: &str, id: ItemId) -> Result<ModelSaveData, ServiceError> {
            Err(ServiceError::Serde(
                format!("Expected {ext} file to have projectId of {id}")
            ))
        }
        let file_path = change_extension(file_path, MODEL_EXT);
        let meta: Meta = Self::load_single_file(&file_path)?;
        
        let file_path = change_extension(file_path, SCHEMA_MODEL_EXT);
        let schema: SchemaSaveData = Self::load_single_file(&file_path)?;
        match &schema.meta {
            None => return error_return(SCHEMA_MODEL_EXT, meta.project_id),
            Some(x) if x.project_id != meta.project_id => return error_return(SCHEMA_MODEL_EXT, meta.project_id),
            _ => {},
        }
        
        let file_path = change_extension(file_path, BOARD_MODEL_EXT);
        let board: BoardSaveData = Self::load_single_file(&file_path)?;
        match &board.meta {
            None => return error_return(BOARD_MODEL_EXT, meta.project_id),
            Some(x) if x.project_id != meta.project_id => return error_return(BOARD_MODEL_EXT, meta.project_id),
            _ => {},
        }
        
        let file_path = change_extension(file_path, EXPORT_MODEL_EXT);
        let export: ExportSaveData = Self::load_single_file(&file_path)?;
        match &export.meta {
            None => return error_return(EXPORT_MODEL_EXT, meta.project_id),
            Some(x) if x.project_id != meta.project_id => return error_return(EXPORT_MODEL_EXT, meta.project_id),
            _ => {},
        }
        
        let file_path = change_extension(file_path, CODE_MODEL_EXT);
        let code: CodeSaveData = Self::load_single_file(&file_path)?;
        match &code.meta {
            None => return error_return(CODE_MODEL_EXT, meta.project_id),
            Some(x) if x.project_id != meta.project_id => return error_return(CODE_MODEL_EXT, meta.project_id),
            _ => {},
        }
        
        Ok(ModelSaveData {
            meta,
            schema,
            board,
            export,
            code,
        })
    }
}

fn change_extension(file_path: PathBuf, ext: &str) -> PathBuf {
    let file_stem = file_path.file_stem()
        .unwrap_or_default()
        .to_string_lossy()
        .to_string();
    file_path.with_file_name(format!("{file_stem}.{ext}"))
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
struct SchemaSaveData {
    #[serde(flatten, skip_serializing_if = "Option::is_none")]
    meta: Option<Meta>,
    namespaces: Vec<NamespaceData>,
    tables: Vec<TableData>,
    columns: Vec<ColumnData>,
    column_types: Vec<ColumnTypeData>,
    primary_keys: Vec<PrimaryKeyData>,
    relationships: Vec<RelationshipData>,
    indexes: Vec<IndexData>,
    uniques: Vec<UniqueData>,
    settings: SchemaSettingsData,
}

impl SchemaSaveData {
    pub fn to_state(&self) -> SchemaState {
        let namespaces = item_array_to_container(&self.namespaces);
        let tables = item_array_to_container(&self.tables);
        let columns = item_array_to_container(&self.columns);
        let column_types = item_array_to_container(&self.column_types);
        let primary_keys = item_array_to_container(&self.primary_keys);
        let relationships = item_array_to_container(&self.relationships);
        let indexes = item_array_to_container(&self.indexes);
        let uniques = item_array_to_container(&self.uniques);
        let settings = self.settings.clone();
        
        SchemaState {
            namespaces,
            tables,
            columns,
            column_types,
            primary_keys,
            relationships,
            indexes,
            uniques,
            settings,
        }
    }
}

impl From<&SchemaState> for SchemaSaveData {
    fn from(schema_state: &SchemaState) -> Self {
        let namespaces = item_container_to_array(&schema_state.namespaces);
        let tables = item_container_to_array(&schema_state.tables);
        let columns = item_container_to_array(&schema_state.columns);
        let column_types = item_container_to_array(&schema_state.column_types);
        let primary_keys = item_container_to_array(&schema_state.primary_keys);
        let relationships = item_container_to_array(&schema_state.relationships);
        let indexes = item_container_to_array(&schema_state.indexes);
        let uniques = item_container_to_array(&schema_state.uniques);
        let settings = schema_state.settings.clone();
        
        Self {
            meta: None,
            namespaces,
            tables,
            columns,
            column_types,
            primary_keys,
            relationships,
            indexes,
            uniques,
            settings,
        }
    }
}

impl From<&SchemaSaveData> for SchemaState {
    fn from(save_data: &SchemaSaveData) -> Self {
        save_data.to_state()
    }
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
struct BoardSaveData {
    #[serde(flatten, skip_serializing_if = "Option::is_none")]
    meta: Option<Meta>,
    item_dimensions: Vec<SaveDimensionsData>,
    board_dimensions: Vec<SaveDimensionsData>,
    notes: Vec<NoteData>,
}

impl BoardSaveData {
    pub fn new(diagram_data: &DiagramData, board_state: &BoardState) -> Self {
        let notes = item_container_to_array(&board_state.notes);
        let item_dimensions = diagram_data.item_dimensions.iter()
            .map(SaveDimensionsData::new_for_item)
            .collect();
        let board_dimensions = diagram_data.board_dimensions.iter()
            .map(SaveDimensionsData::new_for_board)
            .collect();
        Self {
            meta: None,
            item_dimensions,
            board_dimensions,
            notes,
        }
    }
    
    pub fn to_state(&self) -> (BoardState, DiagramData) {
        let notes = item_array_to_container(&self.notes);
        let item_dimensions = HashMap::from_iter(self.item_dimensions.iter().map(SaveDimensionsData::to_dimension));
        let board_dimensions = HashMap::from_iter(self.board_dimensions.iter().map(SaveDimensionsData::to_dimension));
        (
            BoardState {
                notes,
            },
            DiagramData {
                item_dimensions,
                board_dimensions,
            },
        )
    }
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
struct SaveDimensionsData {
    id: ItemId,
    #[serde(skip_serializing_if = "Option::is_none")]
    x: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    y: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    width: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    height: Option<i32>,
}

impl SaveDimensionsData {
    fn new_for_item(item: (&ItemId, &DimensionsData)) -> Self {
        Self {
            id: item.0.clone(),
            x: Some(item.1.x),
            y: Some(item.1.y),
            width: None,
            height: None,
        }
    }
    
    fn new_for_board(item: (&ItemId, &DimensionsData)) -> Self {
        Self {
            id: item.0.clone(),
            x: None,
            y: None,
            width: Some(item.1.width),
            height: Some(item.1.height),
        }
    }
    
    fn to_dimension(&self) -> (ItemId, DimensionsData) {
        (
            self.id.clone(),
            DimensionsData {
                x: self.x.unwrap_or_default(),
                y: self.y.unwrap_or_default(),
                height: self.height.unwrap_or_default(),
                width: self.width.unwrap_or_default(),
            },
        )
    }
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
struct ExportSaveData {
    #[serde(flatten, skip_serializing_if = "Option::is_none")]
    meta: Option<Meta>,
    sql_export_history: Vec<SqlExportHistoryItem>,
    custom_exports: Vec<CustomExport>,
}

impl ExportSaveData {
    pub fn to_state(&self) -> ExportState {
        let sql_export_history = item_array_to_container(&self.sql_export_history);
        let custom_exports = item_array_to_container(&self.custom_exports);
        
        ExportState {
            sql_export_history,
            custom_exports,
        }
    }
}

impl From<&ExportState> for ExportSaveData {
    fn from(export_state: &ExportState) -> Self {
        let sql_export_history = item_container_to_array(&export_state.sql_export_history);
        let custom_exports = item_container_to_array(&export_state.custom_exports);
        
        Self {
            meta: None,
            sql_export_history,
            custom_exports,
        }
    }
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
struct CodeSaveData {
    #[serde(flatten, skip_serializing_if = "Option::is_none")]
    meta: Option<Meta>,
    sql_documents: Vec<SqlDocumentData>,
    struct_outputs: Vec<StructOutputData>,
}

impl CodeSaveData {
    fn to_state(&self) -> CodeState {
        let sql_documents = item_array_to_container(&self.sql_documents);
        let struct_outputs = item_array_to_container(&self.struct_outputs);
        
        CodeState {
            sql_documents,
            struct_outputs,
        }
    }
}

impl From<&CodeState> for CodeSaveData {
    fn from(code_state: &CodeState) -> Self {
        let sql_documents = item_container_to_array(&code_state.sql_documents);
        let struct_outputs = item_container_to_array(&code_state.struct_outputs);
        
        Self {
            meta: None,
            sql_documents,
            struct_outputs,
        }
    }
}
