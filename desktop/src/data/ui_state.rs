use serde::{Deserialize, Serialize};
use smol_str::SmolStr;
use ts_rs::TS;

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./ui-state.ts", export)]
pub struct UiState {
    pub board_selection: BoardSelectionUiState,
    pub table_sidebar: TableSidebarUiState,
    pub column_sidebar: ColumnSidebarUiState,
    pub relationship_sidebar: RelationshipSidebarUiState,
    pub settings_sidebar: SettingsSidebarUiState,
    pub manage_schema_sidebar: ManageSchemaSidebarUiState,
    pub console_sidebar: ConsoleSidebarUiState,
    pub export_sidebar: ExportSidebarUiState,
    pub code_sidebar: CodeSidebarUiState,
    pub namespace_sidebar: NamespaceSidebarUiState,
    pub general_app_ui: GeneralAppUiState,
    pub database_mode_ui: DatabaseModeUiState,
    pub machine_mode_ui: MachineModeUiState,
    pub machine_sidebar: MachineSidebarUiState,
    pub component_sidebar: ComponentSidebarUiState,
    pub connector_sidebar: ConnectorSidebarUiState,
}

impl Default for UiState {
    fn default() -> Self {
        Self {
            board_selection: BoardSelectionUiState {
                selected_table_id: None,
                selected_column_id: None,
                selected_relationship_id: None,
                selected_note_id: None,
                selected_namespace_id: None,
                selected_machine_id: None,
                selected_component_id: None,
                selected_connector_id: None,
            },
            table_sidebar: TableSidebarUiState {
                scroll_amount: 0,
                is_column_group_expanded: false,
                is_primary_key_expanded: false,
                is_foreign_key_group_expanded: false,
                is_index_group_expanded: false,
                is_unique_group_expanded: false,
                is_mixin_group_expanded: false,
                is_mixin_group_usages_expanded: false,
            },
            column_sidebar: ColumnSidebarUiState {
                scroll_amount: 0,
                is_foreign_key_group_expanded: false,
                is_index_group_expanded: false,
                is_unique_group_expanded: false,
            },
            relationship_sidebar: RelationshipSidebarUiState { scroll_amount: 0 },
            settings_sidebar: SettingsSidebarUiState {
                general_settings_expanded: false,
                database_board_settings_expanded: false,
                schema_schema_settings_expanded: false,
                files_schema_settings_expanded: false,
                machine_board_settings_expanded: false,
                export_schema_settings_expanded: false,
                active_tab: "appSettings".into(),
                app_settings_scroll_amount: 0,
                schema_settings_scroll_amount: 0,
            },
            manage_schema_sidebar: ManageSchemaSidebarUiState {
                active_tab: "tables".into(),
                open_table_ids: vec![],
                open_column_type_ids: vec![],
                open_machine_ids: vec![],
                search_scroll_amount: 0,
                types_scroll_amount: 0,
            },
            console_sidebar: ConsoleSidebarUiState { scroll_amount: 0 },
            export_sidebar: ExportSidebarUiState {
                active_tab: "exportHistory".into(),
                history_scroll_amount: 0,
                history_open_export_ids: vec![],
                templates_scroll_amount: 0,
                templates_open_export_id: SmolStr::default(),
            },
            code_sidebar: CodeSidebarUiState {
                scroll_amount: 0,
                active_document_id: SmolStr::default(),
                split_panel_divider_position: 75,
                expanded_tree_ids: vec![],
            },
            namespace_sidebar: NamespaceSidebarUiState { scroll_amount: 0 },
            general_app_ui: GeneralAppUiState {
                options_sidebar: ObjOptionsSidebar {
                    visible_section: None,
                    sidebar_width: 0,
                },
                property_sidebar: ObjPropertySidebar {
                    visible_section: None,
                    sidebar_width: 0,
                },
            },
            database_mode_ui: DatabaseModeUiState {
                open_namespace_tabs: vec![],
                namespace_list_scroll_amount: 0,
            },
            machine_mode_ui: MachineModeUiState {
                open_machine_tabs: vec![],
                machine_list_scroll_amount: 0,
            },
            machine_sidebar: MachineSidebarUiState { scroll_amount: 0 },
            component_sidebar: ComponentSidebarUiState {
                producer_scroll_amount: 0,
                bucket_scroll_amount: 0,
                destroyer_scroll_amount: 0,
                converter_scroll_amount: 0,
                lsystem_scroll_amount: 0,
            },
            connector_sidebar: ConnectorSidebarUiState {
                channel_scroll_amount: 0,
                signal_scroll_amount: 0,
            },
        }
    }
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./ui-state.ts", export)]
pub struct BoardSelectionUiState {
    pub selected_table_id: Option<SmolStr>,
    pub selected_column_id: Option<SmolStr>,
    pub selected_relationship_id: Option<SmolStr>,
    pub selected_note_id: Option<SmolStr>,
    pub selected_namespace_id: Option<SmolStr>,
    pub selected_machine_id: Option<SmolStr>,
    pub selected_component_id: Option<SmolStr>,
    pub selected_connector_id: Option<SmolStr>,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./ui-state.ts", export)]
pub struct TableSidebarUiState {
    pub scroll_amount: i32,
    pub is_column_group_expanded: bool,
    pub is_primary_key_expanded: bool,
    pub is_foreign_key_group_expanded: bool,
    pub is_index_group_expanded: bool,
    pub is_unique_group_expanded: bool,
    pub is_mixin_group_expanded: bool,
    pub is_mixin_group_usages_expanded: bool,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./ui-state.ts", export)]
pub struct ColumnSidebarUiState {
    pub scroll_amount: i32,
    pub is_foreign_key_group_expanded: bool,
    pub is_index_group_expanded: bool,
    pub is_unique_group_expanded: bool,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./ui-state.ts", export)]
pub struct RelationshipSidebarUiState {
    pub scroll_amount: i32,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./ui-state.ts", export)]
pub struct SettingsSidebarUiState {
    // app settings
    pub general_settings_expanded: bool,
    pub database_board_settings_expanded: bool,
    pub machine_board_settings_expanded: bool,
    // schema settings
    pub schema_schema_settings_expanded: bool,
    pub files_schema_settings_expanded: bool,
    pub export_schema_settings_expanded: bool,
    // Unknown enum, so just use a SmolStr
    pub active_tab: SmolStr,
    pub app_settings_scroll_amount: i32,
    pub schema_settings_scroll_amount: i32,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./ui-state.ts", export)]
pub struct ManageSchemaSidebarUiState {
    // Unknown enum, so just use a SmolStr
    pub active_tab: SmolStr,
    pub open_table_ids: Vec<SmolStr>,
    pub open_column_type_ids: Vec<SmolStr>,
    pub open_machine_ids: Vec<SmolStr>,
    pub search_scroll_amount: i32,
    pub types_scroll_amount: i32,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./ui-state.ts", export)]
pub struct ConsoleSidebarUiState {
    pub scroll_amount: i32,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./ui-state.ts", export)]
pub struct ExportSidebarUiState {
    // Unknown enum, so just use a SmolStr
    pub active_tab: SmolStr,
    pub history_scroll_amount: i32,
    pub history_open_export_ids: Vec<SmolStr>,
    pub templates_scroll_amount: i32,
    pub templates_open_export_id: SmolStr,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./ui-state.ts", export)]
pub struct CodeSidebarUiState {
    pub scroll_amount: i32,
    pub active_document_id: SmolStr,
    pub split_panel_divider_position: i32,
    pub expanded_tree_ids: Vec<SmolStr>,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./ui-state.ts", export)]
pub struct NamespaceSidebarUiState {
    pub scroll_amount: i32,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./ui-state.ts", export)]
pub struct GeneralAppUiState {
    #[ts(inline)]
    pub options_sidebar: ObjOptionsSidebar,
    #[ts(inline)]
    pub property_sidebar: ObjPropertySidebar,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct ObjOptionsSidebar {
    pub visible_section: Option<SmolStr>,
    pub sidebar_width: i32,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct ObjPropertySidebar {
    pub visible_section: Option<SmolStr>,
    pub sidebar_width: i32,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./ui-state.ts", export)]
pub struct DatabaseModeUiState {
    pub open_namespace_tabs: Vec<SmolStr>,
    pub namespace_list_scroll_amount: i32,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./ui-state.ts", export)]
pub struct MachineModeUiState {
    pub open_machine_tabs: Vec<SmolStr>,
    pub machine_list_scroll_amount: i32,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./ui-state.ts", export)]
pub struct MachineSidebarUiState {
    pub scroll_amount: i32,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./ui-state.ts", export)]
pub struct ComponentSidebarUiState {
    pub producer_scroll_amount: i32,
    pub bucket_scroll_amount: i32,
    pub destroyer_scroll_amount: i32,
    pub converter_scroll_amount: i32,
    pub lsystem_scroll_amount: i32,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./ui-state.ts", export)]
pub struct ConnectorSidebarUiState {
    pub channel_scroll_amount: i32,
    pub signal_scroll_amount: i32,
}