use std::collections::HashMap;
use serde::{Deserialize, Serialize};
use smol_str::SmolStr;
use ts_rs::TS;
use crate::data::HasId;
use crate::data::schema_state::SchemaState;
use crate::util::item_id::ItemId;

#[derive(TS, Serialize, Deserialize, Debug, Clone, Default)]
#[serde(rename_all = "camelCase")]
#[ts(use_ts_enum, export_to = "./export-state.ts", export)]
pub enum SqlExportType {
    #[default]
    Sqlite,
    Postgres,
    Mysql,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone, Default)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./export-state.ts", export)]
pub struct SqlExportOptions {
    #[serde(rename = "type")]
    pub type_: SqlExportType,
    pub file_name_template: String,
    pub separate_direction_folders: bool,
    pub forward_folder_path: String,
    pub backward_folder_path: String,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone, Default)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./export-state.ts", export)]
pub struct ExportState {
    pub sql_export_history: HashMap<ItemId, SqlExportHistoryItem>,
    pub custom_exports: HashMap<ItemId, CustomExport>,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./export-state.ts", export)]
pub struct SqlExportHistoryItem {
    pub id: ItemId,
    pub name: SmolStr,
    pub time: u32,
    pub parent_id: Option<ItemId>,
    #[serde(rename = "type")]
    pub type_: SqlExportType,
    #[serde(flatten)]
    pub schema_state: SchemaState,
}

impl HasId for SqlExportHistoryItem {
    fn id(&self) -> &ItemId {
        &self.id
    }
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./export-state.ts", export)]
pub struct CustomExport {
    pub id: ItemId,
    pub name: SmolStr,
    pub base_export_directory: String,
    pub table_exports: Vec<TableExportItem>,
    pub full_schema_exports: Vec<SchemaExportItem>,
}

impl HasId for CustomExport {
    fn id(&self) -> &ItemId {
        &self.id
    }
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./export-state.ts", export)]
pub struct TableExportItem {
    pub id: ItemId,
    pub name: SmolStr,
    pub code: String,
    pub included_table_ids: Vec<ItemId>,
    pub excluded_table_ids: Vec<ItemId>,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./export-state.ts", export)]
pub struct SchemaExportItem {
    pub id: ItemId,
    pub name: SmolStr,
    pub code: String,
}
