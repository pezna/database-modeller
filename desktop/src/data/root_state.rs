use serde::{Deserialize, Serialize};
use ts_rs::TS;
use crate::data::board_state::{BoardState, DiagramData};
use crate::data::code_state::{CodeState, StructOutputData};
use crate::data::export_state::{CustomExport, ExportState};
use crate::data::general_state::GeneralState;
use crate::data::model_save_data::ModelSaveData;
use crate::data::schema_state::SchemaState;
use crate::data::settings_state::SettingsState;
use crate::data::ui_state::UiState;
use crate::util::item_id::ItemId;

#[derive(TS, Serialize, Deserialize, Debug, Clone, Default)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./root-state.ts", export)]
pub struct BackendRootState {
    pub board: BoardState,
    pub schema: SchemaState,
    pub export: ExportState,
    pub code: CodeState,
    pub general: GeneralState,
    pub ui: UiState,
    // machine: MachineState,
    pub settings: SettingsState,
}

impl BackendRootState {
    pub fn replace_with(&mut self, save_data: ModelSaveData, ui_state: UiState) -> DiagramData {
        let (
            schema_state,
            export_state,
            code_state,
            board_state,
            diagram_data,
        ) = save_data.to_states();
        
        self.schema = schema_state;
        self.export = export_state;
        self.code = code_state;
        self.board = board_state;
        self.ui = ui_state;
        diagram_data
    }
    
    pub fn just_saved(&mut self, path: String) {
        self.general.model_file_path = Some(path.clone());
        self.general.is_unsaved = false;
        let recents = self.get_recent_model_files();
        for i in 0..recents.len() {
            if recents[i] == path {
                recents.remove(i);
                break;
            }
        }
        recents.push(path);
    }
    
    pub fn get_model_file_path(&self) -> Option<String> {
        self.general.model_file_path.clone()
    }
    
    pub fn get_recent_model_files(&mut self) -> &mut Vec<String> {
        &mut self.settings.general.files.most_recent_model_files
    }
    
    pub fn get_project_id(&self) -> Option<ItemId> {
        self.general.project_id.clone()
    }
    
    pub fn get_struct_output(&self, id: &ItemId) -> Option<&StructOutputData> {
        self.code.struct_outputs.get(id)
    }
    
    pub fn get_custom_export(&self, id: &ItemId) -> Option<&CustomExport> {
        self.export.custom_exports.get(id)
    }
    
    pub fn schema_state_json(&self) -> String {
        serde_json::to_string(&self.schema).unwrap()
    }
}
