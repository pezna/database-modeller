use crate::util::item_id::ItemId;

pub mod schema_state;
pub mod export_state;
pub mod board_state;
pub mod general_state;
pub mod code_state;
pub mod root_state;
pub mod model_save_data;
pub mod utils;
pub mod settings_state;
pub mod ui_state;

pub trait HasId {
    fn id(&self) -> &ItemId;
}