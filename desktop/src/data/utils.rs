use std::cmp::Ordering;
use std::collections::HashMap;
use crate::data::HasId;
use crate::util::item_id::ItemId;

fn id_sort<T: HasId>(a: &T, b: &T) -> Ordering {
    a.id().cmp(b.id())
}

pub fn item_container_to_array<T>(items: &HashMap<ItemId, T>) -> Vec<T>
where
    T: HasId + Clone,
{
    // Collect all values from the map into a vector.
    let mut values: Vec<T> = items.values().cloned().collect();
    // Sort by `id` using the helper function.
    values.sort_by(id_sort);
    values
}

pub fn item_array_to_container<T>(items: &Vec<T>) -> HashMap<ItemId, T>
where
    T: HasId + Clone,
{
    HashMap::from_iter(
        items.iter().map(|item| (item.id().clone(), item.clone()))
    )
}