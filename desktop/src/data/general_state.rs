use std::collections::HashMap;
use serde::{Deserialize, Serialize};
use ts_rs::TS;
use crate::util::item_id::ItemId;

#[derive(TS, Serialize, Deserialize, Debug, Clone, Default)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./general-state.ts", export)]
pub struct GeneralState {
    pub project_id: Option<ItemId>,
    pub app_mode: AppMode,
    pub model_file_path: Option<String>,
    pub notifications: HashMap<ItemId, NotificationData>,
    pub is_unsaved: bool,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone, Default)]
#[serde(rename_all = "camelCase")]
#[ts(use_ts_enum, export_to = "./general-state.ts", export)]
pub enum AppMode {
    #[default]
    Database,
    Machine,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
#[ts(export_to = "./general-state.ts", export)]
pub struct NotificationData {
    pub id: ItemId,
    pub alert_type: MessageAlertType,
    pub title: String,
    pub message: String,
}

#[derive(TS, Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
#[ts(use_ts_enum, export_to = "./general-state.ts", export)]
pub enum MessageAlertType {
    Success,
    Info,
    Error,
}
