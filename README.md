# Database Modeller
This app enables you to model out an entity-relationship diagram for multiple databases and export the SQL code. All in the browser.

Create custom column types for output into various programming languages. Output structs/classes for python, rust, c#, javascript, typescript, and c++.

### Database Types
There are currently 3 database types supported: SQLite, Postgres, and MySQL.

![Image 1](./screenshot-01.png)
![Image 2](./screenclip-01.gif)