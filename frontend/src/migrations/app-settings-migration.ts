import { getDefaultSettingsState } from "@/data";

const migrationOrder = [
	appSettingsMigration20240625,
	appSettingsMigration20240706,
	appSettingsMigration20240811,
];

export function applyAppSettingsMigration(settings: any) {
	migrationOrder.forEach(m => m(settings));
}

function appSettingsMigration20240625(settings: any) {
	const def = getDefaultSettingsState();
	if (!settings.databaseBoard.relationships.showOtherNamespaceTableName) {
		settings.databaseBoard.relationships.showOtherNamespaceTableName = def.databaseBoard.relationships.showOtherNamespaceTableName;
	}
}

function appSettingsMigration20240706(settings: any) {
	const def = getDefaultSettingsState();
	if (settings.databaseBoard.mixins.lineColor && settings.databaseBoard.table.mixinBackgroundColor) {
		settings.databaseBoard.mixins = def.databaseBoard.mixins;
		settings.databaseBoard.mixins.mixinBackgroundColor = settings.databaseBoard.table.mixinBackgroundColor;
		delete settings.databaseBoard.table.mixinBackgroundColor;
	}
}

function appSettingsMigration20240811(settings: any) {
	const def = getDefaultSettingsState();
	if (def.databaseBoard.relationships.notExportedLineColor && !settings.databaseBoard.relationships.notExportedLineColor) {
		settings.databaseBoard.relationships.notExportedLineColor = def.databaseBoard.relationships.notExportedLineColor;
	}
}
