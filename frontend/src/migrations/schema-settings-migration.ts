import { defaultSchemaSettings } from "@/data";

const migrationOrder = [
	schemaMigration20240819,
];

export function applySchemaSettingsMigration(settings: any) {
	migrationOrder.forEach(m => m(settings));
}

function schemaMigration20240819(settings: any) {
	const defaultSettings = defaultSchemaSettings();
	if (!settings.export) {
		settings.export = defaultSettings.export;
	}
}