import enUS from "rsuite/locales/en_US";

export const locale = {
	...enUS,
	Plaintext: {
		...enUS.Plaintext,
		unfilled: " ", // &#8192 En Quad
	},
};
