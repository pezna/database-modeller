import type { RootState } from "@/data/state/root-state";
import { addErrorKeyMiddleware } from "@/middlewares/add-error-key-middleware";
import { backendMiddleware } from "@/middlewares/backend-middleware";
import { codeMiddleware } from "@/middlewares/code-middleware";
import { columnMiddleware } from "@/middlewares/column-middleware";
import { columnTypeMiddleware } from "@/middlewares/column-type-middleware";
import { componentMiddleware } from "@/middlewares/component-middleware";
import { exportMiddleware } from "@/middlewares/export-middleware";
import { indexMiddleware } from "@/middlewares/index-middleware";
import { machineMiddleware } from "@/middlewares/machine-middleware";
import { namespaceMiddleware } from "@/middlewares/namespace-middleware";
import { noteMiddleware } from "@/middlewares/note-middleware";
import { primaryKeyMiddleware } from "@/middlewares/primary-key-middleware";
import { relationshipMiddleware } from "@/middlewares/relationship-middleware";
import { schemaMiddleware } from "@/middlewares/schema-middleware";
import { tableMiddleware } from "@/middlewares/table-middleware";
import { uiMiddleware } from "@/middlewares/ui-middleware";
import { uniqueMiddleware } from "@/middlewares/unique-middleware";
import rootReducer from "@/reducers/root-reducer";
import { configureStore } from "@reduxjs/toolkit";
import { TypedUseSelectorHook, shallowEqual, useDispatch as useReduxDispatch, useSelector as useReduxSelector } from "react-redux";
import { thunk } from "redux-thunk";

export const store = configureStore({
	reducer: rootReducer,
	middleware: (gdm) => {
		return gdm().concat(
			thunk,
			addErrorKeyMiddleware, // must be first
			backendMiddleware, // must be second
			schemaMiddleware,
			machineMiddleware,
			componentMiddleware,
			exportMiddleware,
			uiMiddleware,
			tableMiddleware,
			columnMiddleware,
			columnTypeMiddleware,
			relationshipMiddleware,
			indexMiddleware,
			uniqueMiddleware,
			primaryKeyMiddleware,
			noteMiddleware,
			namespaceMiddleware,
			codeMiddleware,
		);
	},
	devTools: true,
});
export type Dispatch = typeof store.dispatch;
export const useDispatch = () => useReduxDispatch<Dispatch>();
export const useSelector: TypedUseSelectorHook<RootState> = (selector) => useReduxSelector(selector, shallowEqual);

