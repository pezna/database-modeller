import { customAlphabet } from "nanoid";

export enum IdPrefix {
	project = "pj",
	table = "tb",
	column = "cl",
	columnType = "ct",
	relationship = "rs",
	index = "ix",
	unique = "uq",
	primaryKey = "pk",
	namespace = "ns",
	note = "no",
	code = "co",
	export = "ex",
	machine = "mc",
	producer = "pr",
	bucket = "bu",
	lsystem = "ls",
	channel = "ch",
	signal = "si",
	unknown = "xx",
};

const nanoid = customAlphabet("23456789ABCDEFGHJKLNPQRSTUVXYZabcdefghjknpqrstuvxyz", 12);

export function generateId(prefix: IdPrefix) {
	return `${prefix}_${nanoid()}`;
}
