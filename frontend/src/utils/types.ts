export type ReturnPromiseType<T extends (...args: any) => Promise<any>> = T extends (...args: any) => Promise<infer R> ? R : any;

/**
 * Used to ensure that SettingsState and SchemaSettingsData have the same depth
 */
export type SettingsPathDepth = 3

export type IfKey<T, K> = [K] extends [keyof T] ? T[K] : T
export type NextKey<T, K = keyof any> = [K] extends [undefined]
	? undefined
	: [keyof T | undefined] extends [K]
		? keyof any
		: (keyof T | undefined)

export type ArrayPropertyNames<T> = {
    [K in keyof T]: T[K] extends any[] ? K : never
}[keyof T];
