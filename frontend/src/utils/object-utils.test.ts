import { describe, expect, test } from "vitest";
import { getByPath, replaceObjectValue } from ".";

function getNestedObject() {
	return {
		apple: {
			bike: false,
		},
		car: {
			daycare: {
				elephant: "yolo",
			},
			fish: 42,
		},
	};
}

describe("Object Utils", () => {
	test("getByPath with no default", () => {
		const obj = getNestedObject();
		expect(getByPath(obj, ["apple", "bike"])).toEqual(false);
		expect(getByPath(obj, ["apple"])).toMatchObject({ bike: false});
		expect(getByPath(obj, ["car", "daycare", "elephant"])).toEqual("yolo");
	});
	test("getByPath with default", () => {
		const obj = getNestedObject();
		obj.apple = undefined as any;
		const def = true;
		expect(getByPath(obj, ["apple", "bike"], def)).toEqual(def);
	});
	test("replaceObjectValue", () => {
		const destination = getNestedObject();
		const replaceFrom = getNestedObject();
		replaceFrom.apple.bike = true;
		replaceFrom.car.daycare.elephant = "swag";
		
		expect(destination.apple.bike).toBe(false);
		expect(destination.car.daycare.elephant).toBe("yolo");
		
		replaceObjectValue(destination, replaceFrom, ["apple", "bike"]);
		replaceObjectValue(destination, replaceFrom, ["car", "daycare", "elephant"]);
		
		expect(destination.apple.bike).toBe(true);
		expect(destination.car.daycare.elephant).toBe("swag");
	});
});
