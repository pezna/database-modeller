import { ColumnData, IndexData, NamespaceData, PrimaryKeyData, RelationshipData, SimpleSchemaState, TableData, UniqueData, allSchemaItemKeys } from "@/data";

export function mergeSchema(schema1: SimpleSchemaState, schema2: SimpleSchemaState): SimpleSchemaState {
	const s: any = {
		...schema1,
	};
	for (const k of allSchemaItemKeys) {
		s[k] = {
			...schema1[k],
			...schema2[k],
		};
	}
	
	return s;
}

export function nameSort(a: {name: string}, b: {name: string}) {
	return a.name.localeCompare(b.name);
}

export function idSort(a: {id: string}, b: {id: string}) {
	return a.id.localeCompare(b.id);
}

export function findTableNamespace(table: TableData | undefined, state: SimpleSchemaState): NamespaceData | undefined {
	return table?.namespaceId ? state.namespaces[table.namespaceId] : undefined;
}

export function findColumnNamespace(column: ColumnData, state: SimpleSchemaState): NamespaceData | undefined {
	return findTableNamespace(state.tables[column.tableId], state);
}

export function findIndexNamespace(index: IndexData, state: SimpleSchemaState) {
	return findTableNamespace(state.tables[index.tableId], state);
}

export function findUniqueNamespace(unique: UniqueData, state: SimpleSchemaState) {
	return findTableNamespace(state.tables[unique.tableId], state);
}

export function findPrimaryKeyNamespace(primaryKey: PrimaryKeyData, state: SimpleSchemaState) {
	return findTableNamespace(state.tables[primaryKey.tableId], state);
}

export function findRelationshipNamespace(relationship: RelationshipData, state: SimpleSchemaState) {
	return findTableNamespace(state.tables[relationship.childTableId], state);
}
