type IfKey<T, K> = [K] extends [keyof T] ? T[K] : T
type NextKey<T, K = keyof any> = [K] extends [undefined] ? undefined :
  [keyof T | undefined] extends [K] ? keyof any : (keyof T | undefined)

export function getByPath<T0,
  K1 extends NextKey<T0>, T1 extends IfKey<T0, K1>,
  K2 extends NextKey<T1, K1>, T2 extends IfKey<T1, K2>,
  K3 extends NextKey<T2, K2>, T3 extends IfKey<T2, K3>,
  K4 extends NextKey<T3, K3>, T4 extends IfKey<T3, K4>,
  K5 extends NextKey<T4, K4>, T5 extends IfKey<T4, K5>,
  K6 extends NextKey<T5, K5>, T6 extends IfKey<T5, K6>
>(state: T0, path: [K1?, K2?, K3?, K4?, K5?, K6?], defaultValue?: T6): T6 {
	state = state as any;
	try {
		return state && path.reduce((result, prop) => (result as any)[prop], state) as any;
	}
	catch (ex) {
		return defaultValue as any;
	}
}

type PrevIndex = [never, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
	11, 12, 13, 14, 15, 16, 17, 18, 19, 20, ...0[]]

type ArrayJoin<K, P> = K extends Array<any>
	? (
		P extends Array<any>
		? [...K, ...P]
		: [...K, P]
	)
	: (
		P extends Array<any>
		? [K, ...P]
		: (
			"" extends K
			? (
				"" extends P
				? []
				: [P]
			)
			: (
				"" extends P
				? [K]
				: [K, P]
			)
		)
	)
	
type Join<K, P> = K extends string | number
	? (
		P extends string | number
		? `${K}${"" extends P ? "" : "."}${P}`
		: never
	)
	: never;

export type ObjectPaths<T, D extends number = 10> = [D] extends [never]
	? never
	: (
		T extends object
		? (
			T extends Array<any>
			? ""
			: { [K in keyof T]-?: (
					K extends string | number
					? `${K}` | Join<K, ObjectPaths<T[K], PrevIndex[D]>>
					: never
				)
			}[keyof T]
		)
		: ""
	)

export type ArrayObjectPaths<T, D extends number = 10> = [D] extends [never]
	? never
	: (
		T extends object
		? (
			T extends Array<any>
			? ""
			: { [K in keyof T]-?: (
					K extends string | number
					? [`${K}`] | ArrayJoin<K, ArrayObjectPaths<T[K], PrevIndex[D]>>
					: never
				)
			}[keyof T]
		)
		: ""
	)

export type ObjectLeaves<T, D extends number = 10> = [D] extends [never]
	? never
	: (
		T extends object
		? (
			T extends Array<any>
			? ""
			: { [K in keyof T]-?: Join<K, ObjectLeaves<T[K], PrevIndex[D]>> }[keyof T]
		)
		: ""
	)

export type ArrayObjectLeaves<T, D extends number = 10> = [D] extends [never]
	? never
	: (
		T extends object
		? (
			T extends Array<any>
			? ""
			: { [K in keyof T]-?: ArrayJoin<K, ArrayObjectLeaves<T[K], PrevIndex[D]>> }[keyof T]
		)
		: ""
	)

export function replaceObjectValue<T0,
K1 extends NextKey<T0>, T1 extends IfKey<T0, K1>,
K2 extends NextKey<T1, K1>, T2 extends IfKey<T1, K2>,
K3 extends NextKey<T2, K2>, T3 extends IfKey<T2, K3>,
K4 extends NextKey<T3, K3>, T4 extends IfKey<T3, K4>,
K5 extends NextKey<T4, K4>, T5 extends IfKey<T4, K5>,
K6 extends NextKey<T5, K5>, T6 extends IfKey<T5, K6>
>(destination: T0, replaceFrom: T0, path: [K1?, K2?, K3?, K4?, K5?, K6?]) {
	let dest = destination as any;
	let rep = replaceFrom as any;
	
	for (let i = 0; i < path.length - 1; ++i) {
		const k = path[i] as any;
		dest = dest[k];
		rep = rep[k];
	}
	
	const k = path[path.length - 1];
	dest[k] = rep[k];
}
