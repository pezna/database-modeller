export function getLineAndColumn(text: string, index: number) {
	let line = 1;
	let column = 1;
	for (let i = 0; i < text.length; i++) {
		column++;
		
		if (i === index) {
			break;
		}
		
		if (text[i] === '\n') {
			line++;
			column = 1;
		}
	}
	
	return [line, column];
}

export function prependToEveryLine(original: string, prefix: string): string {
	return original.split("\n").map(line => prefix + line).join("\n");
}