export const VERSION = "0.1.0";

export const MODELLER_FILE_EXTENSION = "pmodel";
export const EXPORT_FILE_EXTENSION = `${MODELLER_FILE_EXTENSION}-export`;
export const DIAGRAM_FILE_EXTENSION = `${MODELLER_FILE_EXTENSION}-diagram`;
export const SETTINGS_FILE_EXTENSION = `${MODELLER_FILE_EXTENSION}-settings`;
export const CODE_FILE_EXTENSION = `${MODELLER_FILE_EXTENSION}-code`;

// Note that any code containers created with these IDs must never be deleted in the app's lifetime
export const INDEX_CODE_ID = "index-name-gen-code";
export const PRIMARY_KEY_CODE_ID = "pk-name-gen-code";
export const RELATIONSHIP_CODE_ID = "rel-name-gen-code";
export const UNIQUE_CODE_ID = "unique-name-gen-code";

export const ALLOWED_SCHEMA_NAME_CODE_ID = new Set([
	INDEX_CODE_ID,
	PRIMARY_KEY_CODE_ID,
	RELATIONSHIP_CODE_ID,
	UNIQUE_CODE_ID,
]);
