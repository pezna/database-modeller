import { ColumnData, ColumnDataProperty, ColumnOptionType, ColumnOptionValueContainer, IndexData, IndexDataProperty, NamespaceData, NamespaceDataProperty, PrimaryKeyData, PrimaryKeyDataProperty, RelationshipData, RelationshipDataProperty, SimpleSchemaState, TableData, TableDataProperty, UniqueData, UniqueDataProperty } from "@/data";
import { diff } from "deep-object-diff";


export type PerTableSchemaDiff = { [tableId: string]: SchemaDiff }

export type SchemaDiff = {
	namespaces: DiffObject<NamespaceDiff>;
	tables: DiffObject<TableDiff>;
	columns: DiffObject<ColumnDiff>;
	relationships: DiffObject<RelationshipDiff>;
	indexes: DiffObject<IndexDiff>;
	uniques: DiffObject<UniqueDiff>;
	primaryKeys: DiffObject<PrimaryKeyDiff>;
}

export type Diff<T extends string> = {
	id: string;
	isNew: boolean;
	isDeleted: boolean;
	propertyChanges: T[];
}

export type DiffObject<T extends Diff<string>> = { [id: string]: T }

export type NamespaceDiff = Diff<NamespaceDataProperty>

export type TableDiff = Diff<TableDataProperty>

export type ColumnDiff = Diff<ColumnDataProperty>

export type ColumnOptionDiff = Diff<keyof ColumnOptionValueContainer>

export type RelationshipDiff = Diff<RelationshipDataProperty>

export type IndexDiff = Diff<IndexDataProperty>

export type UniqueDiff = Diff<UniqueDataProperty>

export type PrimaryKeyDiff = Diff<PrimaryKeyDataProperty>

export function createSchemaDiff(oldState: SimpleSchemaState, newState: SimpleSchemaState): SchemaDiff {
	return {
		namespaces: createAllSchemaItemDiffs<NamespaceData, NamespaceDataProperty>(oldState.namespaces, newState.namespaces),
		tables: createAllSchemaItemDiffs<TableData, TableDataProperty>(oldState.tables, newState.tables),
		columns: createAllSchemaItemDiffs<ColumnData, ColumnDataProperty>(oldState.columns, newState.columns),
		relationships: createAllSchemaItemDiffs<RelationshipData, RelationshipDataProperty>(oldState.relationships, newState.relationships),
		indexes: createAllSchemaItemDiffs<IndexData, IndexDataProperty>(oldState.indexes, newState.indexes),
		uniques: createAllSchemaItemDiffs<UniqueData, UniqueDataProperty>(oldState.uniques, newState.uniques),
		primaryKeys: createAllSchemaItemDiffs<PrimaryKeyData, PrimaryKeyDataProperty>(oldState.primaryKeys, newState.primaryKeys),
	};
}

export function createAllSchemaItemDiffs<T extends {id: string}, S extends string>(
	oldItems: {[id: string]: T},
	newItems: {[id: string]: T},
): DiffObject<Diff<S>>
{
	const diffs: DiffObject<Diff<S>> = {};
	const itemIds = new Set<string>([...Object.keys(oldItems), ...Object.keys(newItems)]);
	for (const itemId of itemIds) {
		diffs[itemId] = createSchemaItemDiff<T, S>(oldItems[itemId], newItems[itemId]);
	}
	
	return diffs;
}

export function createSchemaItemDiff<T extends {id: string}, S extends string>(oldItem: T | undefined, newItem: T | undefined): Diff<S> {
	if (oldItem && newItem) {
		const differences = diff(oldItem, newItem);
		return {
			id: newItem.id,
			isNew: false,
			isDeleted: false,
			propertyChanges: Object.keys(differences) as S[],
		};
	}
	else if (oldItem && !newItem) {
		return {
			id: oldItem.id,
			isNew: false,
			isDeleted: true,
			propertyChanges: Object.keys(oldItem) as S[],
		};
	}
	else if (!oldItem && newItem) {
		return {
			id: newItem.id,
			isNew: true,
			isDeleted: false,
			propertyChanges: Object.keys(newItem) as S[],
		};
	}
	
	throw new Error("Cannot diff 2 undefined schema items");
}

export function createColumnOptionDiff(oldItem: ColumnOptionValueContainer | undefined, newItem: ColumnOptionValueContainer | undefined): ColumnOptionDiff {
	if (oldItem && newItem) {
		const differences = diff(oldItem, newItem);
		return {
			id: '',
			isNew: false,
			isDeleted: false,
			propertyChanges: Object.keys(differences) as ColumnOptionType[],
		};
	}
	else if (oldItem && !newItem) {
		return {
			id: '',
			isNew: false,
			isDeleted: true,
			propertyChanges: Object.keys(oldItem) as ColumnOptionType[],
		};
	}
	else if (!oldItem && newItem) {
		return {
			id: '',
			isNew: true,
			isDeleted: false,
			propertyChanges: Object.keys(newItem) as ColumnOptionType[],
		};
	}
	
	throw new Error("Cannot diff 2 undefined ColumnOptionValueContainer");
}

export function createPerTableSchemaDiff(schemaDiff: SchemaDiff, schema: SimpleSchemaState): PerTableSchemaDiff {
	const detail: PerTableSchemaDiff = {};
	Object.values(schemaDiff.tables).forEach(td => {
		detail[td.id] = {
			tables: {[td.id]: td},
			columns: {},
			relationships: {},
			primaryKeys: {},
			indexes: {},
			uniques: {},
			namespaces: {},
		};
		
		const nsId = schema.tables[td.id].namespaceId;
		if (nsId) {
			const nsDiff = schemaDiff.namespaces[nsId];
			if (nsDiff) {
				detail[td.id].namespaces[nsId] = nsDiff;
			}
		}
	});
	
	Object.values(schemaDiff.columns).forEach(cd => {
		const tableId = schema.columns[cd.id].tableId;
		detail[tableId].columns[cd.id] = cd;
	});
	Object.values(schemaDiff.primaryKeys).forEach(cd => {
		const tableId = schema.primaryKeys[cd.id].tableId;
		detail[tableId].primaryKeys[cd.id] = cd;
	});
	Object.values(schemaDiff.relationships).forEach(cd => {
		const tableId = schema.relationships[cd.id].childTableId;
		detail[tableId].relationships[cd.id] = cd;
	});
	Object.values(schemaDiff.indexes).forEach(cd => {
		const tableId = schema.indexes[cd.id].tableId;
		detail[tableId].indexes[cd.id] = cd;
	});
	Object.values(schemaDiff.uniques).forEach(cd => {
		const tableId = schema.uniques[cd.id].tableId;
		detail[tableId].uniques[cd.id] = cd;
	});
	
	return detail;
}
