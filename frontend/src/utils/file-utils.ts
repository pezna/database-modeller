import { OpenDialogOptions, SaveDialogOptions } from "@tauri-apps/plugin-dialog";
import { MODELLER_FILE_EXTENSION } from "./constants";

export function modellerSaveDialogOptions(defaultPath?: string): SaveDialogOptions {
	return {
		title: "Save Modeller File",
		filters: [
			{ name: "Pezna Modeller File", extensions: [MODELLER_FILE_EXTENSION] },
			{ name: "All Files", extensions: ["*"] },
		],
		defaultPath,
	};
}

export function modellerOpenDialogOptions(defaultPath?: string): OpenDialogOptions {
	return {
		title: "Open Modeller File",
		filters: [
			{ name: "Pezna Modeller File", extensions: [MODELLER_FILE_EXTENSION] },
			{ name: "All Files", extensions: ["*"] },
		],
		defaultPath,
	};
}

export function sqlSaveDialogOptions(): SaveDialogOptions {
	return {
		title: "Save SQL File",
		filters: [
			{ name: "SQL File", extensions: ["sql"] },
			{ name: "All Files", extensions: ["*"] },
		],
	};
}

export class ParsedPath {
	separator: string;
	directory: string;
	name: string;
	nameNoExtension: string;
	extension?: string;
	
	constructor(path: string) {
		const split = path.split(pathSeparatorRegex);
		this.name = split.at(-1)!;
		this.directory = path.substring(0, path.length - this.name.length);
		const dotIndex = this.name.lastIndexOf(".");
		this.extension = dotIndex > -1 ? this.name.substring(dotIndex) : "";
		this.nameNoExtension = this.name.substring(0, this.name.length - this.extension.length);
		this.separator = split.length > 1 ? path[split[0].length] : "";
	}
	
	joinPathChangeExtension(ext: string): string {
		const newExt = (ext[0] === ".") ? ext : `.${ext}`;
		return `${this.directory}${this.nameNoExtension}${newExt}`;
	}
}

const pathSeparatorRegex = /[\\\/]/g;

export function pathSeparator() {
	return window.navigator.userAgent.toLowerCase().includes("windows") ? "\\" : "/";
}