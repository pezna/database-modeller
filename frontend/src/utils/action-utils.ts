import { ActionData } from "@/data";
import { Dispatch } from "redux";

export function isActionObject(action: unknown): action is ActionData {
	return (typeof action === "object") && Boolean((action as any)?.type);
}

export function isActionFunction(action: unknown): action is ActionData {
	return (typeof action === "function");
}

export function isNextFunction(next: unknown): next is Dispatch<any> {
	return (typeof next === "function");
}