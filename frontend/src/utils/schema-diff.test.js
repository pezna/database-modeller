import { createColumnOptionDiff } from "./schema-diff";

describe('schema utils', () => {
	test('column option diff NEW', () => {
		const newOptions = {
			characterSet: 'utf-8',
			collate: '',
		};
		
		const diff = createColumnOptionDiff(undefined, newOptions);
		expect(diff.isNew).toEqual(true);
		expect(diff.isDeleted).toEqual(false);
		expect(diff.propertyChanges).toContainEqual('collate');
		expect(diff.propertyChanges).toContainEqual('characterSet');
	});
	
	test('column option diff DELETED', () => {
		const oldOptions = {
			characterSet: undefined,
			collate: '',
			default: '\'hello\'', 
		};
		
		const diff = createColumnOptionDiff(oldOptions, undefined);
		expect(diff.isNew).toEqual(false);
		expect(diff.isDeleted).toEqual(true);
		expect(diff.propertyChanges).toContainEqual('default');
		expect(diff.propertyChanges).toContainEqual('characterSet');
		expect(diff.propertyChanges).toContainEqual('collate');
	});
	
	test('column option diff CHANGED', () => {
		const oldOptions = {
			characterSet: undefined,
			collate: '',
			default: '\'hello\'', 
		};
		
		const newOptions = {
			characterSet: 'utf-8',
			collate: '',
		};
		
		const diff = createColumnOptionDiff(oldOptions, newOptions);
		expect(diff.isNew).toEqual(false);
		expect(diff.isDeleted).toEqual(false);
		expect(diff.propertyChanges).toContainEqual('default');
		expect(diff.propertyChanges).toContainEqual('characterSet');
	});
});