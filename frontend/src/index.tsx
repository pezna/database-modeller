import { App } from "@/components/app/App";
import { ModalProvider } from "@/contexts";
import { lightTheme } from "@/data/theme";
import { locale } from "@/i18n";
import { store } from "@/store";
import { ThemeProvider } from "@emotion/react";
import React from "react";
import { createRoot } from "react-dom/client";
import { Provider } from "react-redux";
import { CustomProvider } from "rsuite";
import "./index.scss";

const root = createRoot(document.getElementById("root")!)
root.render(
	<React.StrictMode>
		<Provider store={store}>
			<CustomProvider locale={locale} theme="dark">
				<ThemeProvider theme={lightTheme}>
					<ModalProvider>
						<App />
					</ModalProvider>
				</ThemeProvider>
			</CustomProvider>
		</Provider>
	</React.StrictMode>
);
