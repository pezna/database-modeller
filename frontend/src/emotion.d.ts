import "@emotion/react";

declare module "@emotion/react" {
	export interface Theme {
		header: {
			backgroundColor: string;
			foregroundColor: string;
		};
		sidebar: {
			expandedBackgroundColor: string;
			buttonBarBackgroundColor: string;
			foregroundColor: string;
			minWidth: string;
			resizeBorderSize: string;
			plaintextBackground: string;
		};
		board: {
			backgroundColor: string;
			headerBackgroundColor: string;
		};
		table: {
			backgroundColor: string;
			foregroundColor: string;
			borderColor: string;
		};
		accordion: {
			bodyBackgroundColor: string;
			alternateBodyBackgroundColor: string;
		};
		colors: {
			baseBackgroundColor: string;
			altBackgroundColor: string;
		};
	}
}