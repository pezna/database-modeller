import { createDimensionsObject, DiagramData, DimensionsData } from "@/data";

type Callback = () => void;

export class DiagramService {
	universalListeners: Record<string, Callback> = {};
	itemListeners: {[itemId: string]: {[key: string]: Callback}} = {};
	itemDimensions: Record<string, DimensionsData> = {};
	boardDimensions: Record<string, DimensionsData> = {};
	connectorDrawOrder: Record<string, string[]> = {};
	
	loadFromSaveData(data: DiagramData) {
		this.itemDimensions = data.itemDimensions;
		this.boardDimensions = data.boardDimensions;
		this.connectorDrawOrder = {};
		this.itemListeners = {};
		this.universalListeners = {};
	}
	
	getDiagramData(): DiagramData {
		return {
			itemDimensions: this.itemDimensions,
			boardDimensions: this.boardDimensions,
		};
	}
	
	addBoard(boardId: string) {
		this.boardDimensions[boardId] = createDimensionsObject(0, 0, 0, 0);
	}
	
	removeBoard(boardId: string) {
		delete this.boardDimensions[boardId];
	}
	
	addItemListener(itemId: string, key: string, func: Callback) {
		let keyToFunc = this.itemListeners[itemId];
		if (keyToFunc === undefined) {
			keyToFunc = {};
			this.itemListeners[itemId] = keyToFunc;
		}
		
		keyToFunc[key] = func;
		const existingDimensions = this.itemDimensions[itemId];
		if (existingDimensions) {
			func();
		}
	}
	
	removeItemListener(itemId: string, key: string) {
		const keyToFunc = this.itemListeners[itemId];
		if (keyToFunc !== undefined) {
			delete keyToFunc[key];
		}
	}
	
	addUniversalListener(key: string, func: Callback) {
		this.universalListeners[key] = func;
	}
	
	removeUniversalListener(key: string) {
		delete this.universalListeners[key];
	}
	
	sendItemDimensions(itemId: string, boardId: string, dimensions: DimensionsData) {
		this.itemDimensions[itemId] = dimensions;
		
		this.calculateBoardSize(boardId, dimensions);
		Object.values(this.universalListeners).forEach(f => f());
		
		const keyToFunc = this.itemListeners[itemId];
		if (typeof keyToFunc !== 'object') {
			return;
		}
		Object.values(keyToFunc).forEach(f => f());
	}
	
	getItemDimensions(itemId: string): DimensionsData | undefined {
		return this.itemDimensions[itemId];
	}
	
	deleteItem(itemId: string) {
		delete this.itemListeners[itemId];
		delete this.itemDimensions[itemId];
	}
	
	private calculateBoardSize(boardId: string, dimensions: DimensionsData) {
		const boardDims = this.boardDimensions[boardId];
		boardDims.height = Math.max(boardDims.height, dimensions.y + dimensions.height);
		boardDims.width = Math.max(boardDims.width, dimensions.x + dimensions.width);
	}
	
	getBoardDimensions(boardId: string): DimensionsData | undefined {
		return this.boardDimensions[boardId];
	}
	
	addConnector(key: string, connectorId: string) {
		let ids = this.connectorDrawOrder[key];
		if (!ids) {
			ids = [];
			this.connectorDrawOrder[key] = ids;
		}
		
		ids.push(connectorId);
	}
	
	removeConnector(key: string, connectorId: string) {
		const ids = this.connectorDrawOrder[key];
		if (!ids) {
			return;
		}
		
		const index = ids.indexOf(connectorId);
		if (index > -1) {
			ids.splice(index, 1);
		}
	}
	
	getConnectorDrawOrder(key: string) {
		return this.connectorDrawOrder[key];
	}
	
	clearAll() {
		this.itemDimensions = {};
		this.boardDimensions = {};
		this.connectorDrawOrder = {};
		this.itemListeners = {};
		this.universalListeners = {};
	}
}

export const diagramService = new DiagramService();
