import { BackendRootState, OpenModelError, OpenModelSuccess, SaveModelError, SaveModelSuccess, ServiceError, SettingsState, UiState } from "@/data";
import { diagramService } from "@/services/diagram-service";
import { ServiceResult } from "@/services/service-data";
import { modellerOpenDialogOptions, modellerSaveDialogOptions } from "@/utils";
import { invoke } from "@tauri-apps/api/core";
import * as dialog from "@tauri-apps/plugin-dialog";

const commands = {
	newModelState: 'new_model_state',
	saveModelFile: 'save_model_file',
	openModelFile: 'open_model_file',
	saveAppSettings: 'save_app_settings',
	loadAppSettings: 'load_app_settings',
	updateUiState: 'update_ui_state',
};

export const fileService = {
	async newModelState(projectId: string, copySchemaSettings: boolean): Promise<BackendRootState> {
		return invoke(commands.newModelState, { projectId, copySchemaSettings });
	},

	async saveModellerFile(
		rootState: BackendRootState,
		filePath?: string,
	): Promise<ServiceResult<SaveModelSuccess, SaveModelError>> {
		let newFilePath: string | null = null;
		try {
			if (!filePath) {
				newFilePath = await dialog.save(modellerSaveDialogOptions(filePath));
				if (!newFilePath) {
					return { err: { type: 'userCancelled' }};
				}
			}
		}
		catch (ex) {
			console.error('Dialog error:', ex);
			return { err: { type: 'io', data: String(ex) }};
		}
		
		const diagramData = diagramService.getDiagramData();
		try {
			const ok = await invoke<SaveModelSuccess>(commands.saveModelFile, { rootState, diagramData, newFilePath });
			return { ok };
		}
		catch (err: any) {
			console.error('saveModelFile error:', err);
			return { err };
		}
	},

	async openModellerFile(filePath: string | null): Promise<ServiceResult<OpenModelSuccess, OpenModelError>> {
		try {
			if (!filePath) {
				filePath = await dialog.open(modellerOpenDialogOptions(filePath ?? undefined));
				if (!filePath) {
					return { err: { type: 'userCancelled' }};
				}
			}
		}
		catch (ex) {
			console.error('Dialog error:', ex);
			return { err: { type: 'io', data: String(ex) }};
		}
		
		try {
			const ok = await invoke<OpenModelSuccess>(commands.openModelFile, { filePath });
			return { ok };
		}
		catch (err: any) {
			console.error('saveModelFile error:', err);
			return { err };
		}
	},

	async saveAppSettings(settings: SettingsState): Promise<ServiceResult<true, ServiceError>> {
		try {
			await invoke(commands.saveAppSettings, { settings });
			return { ok: true };
		}
		catch (err: any) {
			return { err };
		}
	},

	async loadAppSettings(): Promise<SettingsState> {
		return invoke(commands.loadAppSettings);
	},

	async updateUiState(ui: UiState): Promise<void> {
		return invoke(commands.updateUiState, { ui });
	},

	async openFolder(defaultPath?: string): Promise<string | null> {
		return await dialog.open({
			title: "Select Folder",
			directory: true,
			defaultPath,
		});
	},
}
