export * from "./code-service";
export * from "./diagram-service";
export * from "./export-service";
export * from "./file-service";
export * from "./sql-service";

if (!(window as any).__TAURI_IPC__) {
	(window as any).__TAURI_IPC__ = (...args: any[]) => {
		console.warn("Tauri IPC does not exist.", args);
	};
}