import { FindSchemaUsesServiceResponse, SchemaState, ServiceResponseStatus } from "@/data";

export const sqlService = {
	async findSchemaUses(sqlCodes: string[], schemaState: SchemaState): Promise<FindSchemaUsesServiceResponse> {
		return {
			uses: [],
			status: ServiceResponseStatus.success,
		};
	}
};
