export type ServiceResult<T, E> = {
	ok?: T;
	err?: E;
}