import { createJavascriptError, deleteJavascriptError } from "@/actions";
import { CodeContainer, CodeServiceError, CodeServiceSuccess, RunCodeContainer, SetExportCodeContainerIds } from "@/data/backend";
import { ServiceResult } from "@/services/service-data";
import { Dispatch } from "@/store";
import { ALLOWED_SCHEMA_NAME_CODE_ID } from "@/utils";
import { invoke } from "@tauri-apps/api/core";

const commands = {
	setCodeContainer: "set_code_container",
	deleteCodeContainer: "delete_code_container",
	runCodeContainer: "run_code_container",
	setExportCodeContainerIds: "set_export_code_container_ids",
	deleteExportCodeContainers: "delete_export_code_containers",
};

export const codeService = {
	async setCodeContainer(container: CodeContainer): Promise<ServiceResult<true, CodeServiceError>> {
		try {
			await invoke(commands.setCodeContainer, { container });
			return { ok: true };
		}
		catch (err: any) {
			console.error('setCodeContainer error:', err);
			return { err };
		}
	},
	async deleteCodeContainer(id: string): Promise<void> {
		try {
			await invoke(commands.deleteCodeContainer, { id });
		}
		catch (err: any) {
			console.error('setCodeContainer error:', err);
		}
	},
	async runCodeContainer(data: RunCodeContainer): Promise<ServiceResult<CodeServiceSuccess, CodeServiceError>> {
		try {
			const ok = await invoke<CodeServiceSuccess>(commands.runCodeContainer, { data });
			return { ok };
		}
		catch (err: any) {
			console.error('runCodeContainer error:', err);
			return { err };
		}
	},
	async setExportCodeContainerIds(data: SetExportCodeContainerIds): Promise<void> {
		try {
			await invoke(commands.setExportCodeContainerIds, { data });
		}
		catch (err: any) {
			console.error('setExportCodeContainerIds error:', err);
		}
	},
	async deleteExportCodeContainers(exportId: string): Promise<void> {
		try {
			await invoke(commands.deleteExportCodeContainers, { exportId });
		}
		catch (err: any) {
			console.error('deleteExportCodeContainers error:', err);
		}
	},
};

export async function updateBackendCode(dispatch: Dispatch, checkAllowedNames: boolean, cc: CodeContainer) {
	if (checkAllowedNames && !ALLOWED_SCHEMA_NAME_CODE_ID.has(cc.id)) {
		throw new Error(`ALLOWED_SCHEMA_NAME_CODE_ID does not have ${cc.id}`);
	}
	
	return (
		codeService
		.setCodeContainer(cc)
		.then(res => {
			if (res.err && res.err.type === 'runFailure') {
				dispatch(createJavascriptError(res.err.error, res.err.id));
			}
			else if (res.ok) {
				dispatch(deleteJavascriptError(cc.id));
			}
		})
	);
};
