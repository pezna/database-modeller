import { GenerateSqlMigrationServiceResponse, MigrationResult, RootState, ServiceResponseStatus, SimpleSchemaState, SqlExportOptions, SqlExportType, convertTemplateString } from "@/data";
import { MysqlGenerator, PostgresGenerator, SqliteGenerator } from "../generators";

function createGenerator(type: SqlExportType, oldSchemaState: SimpleSchemaState, newState: RootState) {
	switch (type) {
		case SqlExportType.postgres:
			return new PostgresGenerator(oldSchemaState, newState);
		case SqlExportType.sqlite:
			return new SqliteGenerator(oldSchemaState, newState);
		case SqlExportType.mysql:
			return new MysqlGenerator(oldSchemaState, newState);
		default:
			throw new Error(`No sql generator for ${type}`);
	}
}

async function createSqlMigration(
	options: SqlExportOptions,
	oldSchemaState: SimpleSchemaState,
	newState: RootState,
	direction: "forward" | "backward",
	fileNamePrefix: string
): Promise<MigrationResult>
{
	let folderPath = options.forwardFolderPath;
	if (options.separateDirectionFolders) {
		folderPath = direction === "forward" ? options.forwardFolderPath : options.backwardFolderPath;
	}
	
	const generator = createGenerator(options.type, oldSchemaState, newState);
	const result = generator.generateSql();
	let filePath: string | undefined;
	
	if (result.sql) {
		/* 
		const separator = pathSeparator();
		filePath = folderPath + separator + `${fileNamePrefix}_${direction}.sql`;
		const res = await fileService.writeFile(filePath, result.sql);
		if (res.status === ServiceResponseStatus.fail) {
			throw new Error(res.statusMessage);
		}
		 */
	}
	
	return {
		sql: result.sql,
		filePath,
	};
}

export const exportService = {
	async generateSqlMigration(options: SqlExportOptions, oldSchemaState: SimpleSchemaState, newState: RootState): Promise<GenerateSqlMigrationServiceResponse> {
		const date = new Date();
		const fileNamePrefix = convertTemplateString(options.fileNameTemplate, {
			date,
			type: options.type,
		});
		const forwardResult = await createSqlMigration(options, oldSchemaState, newState, "forward", fileNamePrefix);
		
		const downOldSchemaState = newState.schema;
		const downNewState: RootState = {
			...newState,
			schema: {
				...oldSchemaState,
				settings: newState.schema.settings,
			},
		};
		const backwardResult = await createSqlMigration(options, downOldSchemaState, downNewState, "backward", fileNamePrefix);
		
		return {
			time: date.getTime(),
			status: ServiceResponseStatus.success,
			forwardResult,
			backwardResult,
		};
	},
}
