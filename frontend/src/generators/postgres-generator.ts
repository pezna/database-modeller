import { ColumnData, ColumnOptionType, ColumnTypeData, IndexData, NamespaceData, PrimaryKeyData, RelationshipData, TableData, UniqueData } from "@/data";
import { getColumn, getTable } from "@/selectors";
import { ColumnDiff, IndexDiff, NamespaceDiff, PrimaryKeyDiff, RelationshipDiff, TableDiff, UniqueDiff, createColumnOptionDiff, findIndexNamespace, findTableNamespace } from "@/utils";
import { SqlData, SqlGenerator } from "./sql-generator-base";
import { generateForeignKeyChangeSql, generateForeignKeyDeferrability, getIndexDirectionSql, quoteName, quoteObjectName } from "./sql-utils";

export class PostgresGenerator extends SqlGenerator {
	generateNamespaceDifference(diff: NamespaceDiff, oldNamespace: NamespaceData | undefined, newNamespace: NamespaceData | undefined): SqlData {
		const state = this.newState;
		
		if (diff.isDeleted) {
			return this.deleteNamespace(oldNamespace!);
		}
		else if (diff.isNew) {
			return this.createNamespace(newNamespace!);
		}
		
		oldNamespace = oldNamespace!;
		newNamespace = newNamespace!;
		
		if (diff.propertyChanges.includes('name')) {
			return {
				text: `ALTER SCHEMA ${quoteName(oldNamespace.name, state)} RENAME TO ${quoteName(newNamespace.name, state)};`,
				requiresNewTable: true,
			};
		}
		
		return {
			text: '',
			isCompleteSqlStatement: true,
		};
	}
	
	createNamespace(namespace: NamespaceData): SqlData {
		return {
			text: `CREATE SCHEMA ${quoteName(namespace.name, this.newState)};`,
			isCompleteSqlStatement: true,
			createdNewNamespace: true,
		};
	}
	
	deleteNamespace(namespace: NamespaceData): SqlData {
		return {
			text: `DROP SCHEMA ${quoteName(namespace.name, this.newState)};`,
			isCompleteSqlStatement: true,
			requiresNewTable: true,
		};
	}
	
	generateTableDifference(diff: TableDiff, oldTable: TableData | undefined, newTable: TableData | undefined): SqlData {
		const state = this.newState;
		
		if (diff.isDeleted) {
			return this.deleteTable(oldTable!);
		}
		else if (diff.isNew) {
			return this.createTable(newTable!);
		}
		
		oldTable = oldTable!;
		newTable = newTable!;
		
		if (diff.propertyChanges.includes('name')) {
			const namespace = findTableNamespace(oldTable, this.oldSchemaState)?.name ?? '';
			
			return {
				text: `ALTER TABLE ${quoteObjectName(namespace, oldTable.name, state)} RENAME TO ${quoteName(newTable.name, state)};`,
				isCompleteSqlStatement: true,
			};
		}
		
		// every other change will be handled by the respective functions
		
		return {
			text: '',
			isCompleteSqlStatement: true,
			createdNewTable: false,
		};
	}
	
	generateColumnDifference(diff: ColumnDiff, oldColumn: ColumnData | undefined, newColumn: ColumnData | undefined): SqlData {
		const state = this.newState;
		
		if (diff.isDeleted) {
			return this.deleteColumn(oldColumn!);
		}
		else if (diff.isNew) {
			return this.createColumn(newColumn!);
		}
		else if (diff.propertyChanges.length === 0) {
			return {
				text: '',
			};
		}
		
		oldColumn = oldColumn!;
		newColumn = newColumn!;
		const table = getTable(state, newColumn.tableId);
		const namespace = findTableNamespace(table, state.schema)?.name ?? '';
		const tableName = quoteObjectName(namespace, table.name, state);
		const oldColumnName = quoteName(oldColumn.name, state);
		const newColumnName = quoteName(newColumn.name, state);
		const oldColumnType = this.oldSchemaState.columnTypes[oldColumn.typeId];
		const newColumnType = state.schema.columnTypes[newColumn.typeId];
		const typeTextChanged = postgresColumnLanguage(oldColumnType) !== postgresColumnLanguage(newColumnType);
		const optionDiff = createColumnOptionDiff(oldColumn.options, newColumn.options);
		
		const data: SqlData = {
			text: '',
			isCompleteSqlStatement: true,
		};
		
		if (diff.propertyChanges.includes('name')) {
			data.text += `ALTER TABLE ${tableName} RENAME COLUMN ${oldColumnName} TO ${newColumnName};\n`;
		}
		
		if (typeTextChanged) {
			const typeText = postgresColumnLanguage(newColumnType);
			
			data.text += `ALTER TABLE ${tableName} ALTER COLUMN ${newColumnName} TYPE ${typeText} USING CAST(${newColumnName} AS ${typeText});\n`;
		}
		
		if (diff.propertyChanges.includes('options')) {
			if (optionDiff.propertyChanges.includes(ColumnOptionType.defaultValue)) {
				if (newColumn.options.defaultValue) {
					data.text += `ALTER TABLE ${tableName} ALTER COLUMN ${newColumnName} SET DEFAULT ${newColumn.options.defaultValue};\n`;
				}
				else {
					data.text += `ALTER TABLE ${tableName} ALTER COLUMN ${newColumnName} DROP DEFAULT;\n`;
				}
			}
			
			if (optionDiff.propertyChanges.includes(ColumnOptionType.notNull)) {
				if (newColumn.options.notNull) {
					data.text += `ALTER TABLE ${tableName} ALTER COLUMN ${newColumnName} SET NOT NULL;\n`;
				}
				else {
					data.text += `ALTER TABLE ${tableName} ALTER COLUMN ${newColumnName} DROP NOT NULL;\n`;
				}
			}
		}
		
		return data;
	}
	
	createColumn(column: ColumnData, forNewTable?: boolean): SqlData {
		forNewTable = Boolean(forNewTable) || this.isTableNew(column.tableId);
		const state = this.newState;
		const columnTypeText = postgresColumnLanguage(state.schema.columnTypes[column.typeId]);
		let text = `${quoteName(column.name, state)} ${columnTypeText}`;
		
		if (column.options.notNull) {
			text += ' NOT NULL';
		}
		
		if (column.options.defaultValue) {
			text += ` DEFAULT ${column.options.defaultValue}`;
		}
		
		if (!forNewTable) {
			const table = getTable(state, column.tableId);
			const namespace = findTableNamespace(table, state.schema)?.name ?? '';
			
			return {
				text: `ALTER TABLE ${quoteObjectName(namespace, table.name, state)} ADD COLUMN ${text};`,
				isCompleteSqlStatement: true,
				createdNewColumn: true,
			};
		}
		
		return {
			text,
			requiresNewTable: true,
			createdNewColumn: true,
		};
	}
	
	deleteColumn(column: ColumnData): SqlData {
		const state = this.newState;
		const table = this.oldSchemaState.tables[column.tableId];
		const namespace = findTableNamespace(table, state.schema)?.name ?? '';
		const tableName = quoteObjectName(namespace, table.name, state);
		
		return {
			text: `ALTER TABLE ${tableName} DROP COLUMN ${quoteName(column.name, state)} CASCADE;`,
			isCompleteSqlStatement: true,
		};
	}
	
	generateRelationshipDifference(diff: RelationshipDiff, oldRelationship: RelationshipData | undefined, newRelationship: RelationshipData | undefined): SqlData {
		const state = this.newState;
		
		if (diff.isDeleted) {
			return this.deleteRelationship(oldRelationship!);
		}
		else if (diff.isNew) {
			const rel = newRelationship!;
			const isNewTable = !(rel.childTableId in this.oldSchemaState.tables);
			return this.createRelationship(newRelationship!, isNewTable);
		}
		
		oldRelationship = oldRelationship!;
		newRelationship = newRelationship!;
		
		if (diff.propertyChanges.includes('isNotExported')) {
			if (newRelationship.isNotExported) {
				return this.deleteRelationship(oldRelationship);
			}
			else {
				return this.createRelationship(newRelationship);
			}
		}
		
		const table = getTable(state, newRelationship.childTableId);
		const namespace = findTableNamespace(table, state.schema)?.name ?? '';
		const tableName = quoteObjectName(namespace, table.name, state);
		const oldRelationshipName = quoteObjectName(namespace, oldRelationship.name, state);
		const newRelationshipName = quoteObjectName(namespace, newRelationship.name, state);
		
		const hasNameChange = diff.propertyChanges.includes('name');
		const hasDeferrabilityChange = diff.propertyChanges.includes('deferrability');
		const shouldRecreateRelationship = (hasNameChange && hasDeferrabilityChange && diff.propertyChanges.length === 2) ||
			(hasDeferrabilityChange && diff.propertyChanges.length >= 1) ||
			(diff.propertyChanges.includes('childTableId') || diff.propertyChanges.includes('childColumnIds') || diff.propertyChanges.includes('parentTableId') || diff.propertyChanges.includes('parentColumnIds'));
		
		const data: SqlData = {
			text: '',
			isCompleteSqlStatement: true,
		};
		
		if (shouldRecreateRelationship) {
			data.text += this.deleteRelationship(oldRelationship).text + '\n' + this.createRelationship(newRelationship).text;
		}
		else {
			if (hasNameChange) {
				data.text += `ALTER TABLE ${tableName} RENAME CONSTRAINT ${oldRelationshipName} TO ${newRelationshipName};`;
			}
			
			if (hasDeferrabilityChange && newRelationship.deferrability) {
				data.text += `ALTER TABLE ${tableName} ALTER CONSTRAINT ${newRelationshipName} ${generateForeignKeyDeferrability(newRelationship.deferrability)};`;
			}
		}
		
		return data;
	}
	
	createRelationship(relationship: RelationshipData, forNewTable?: boolean): SqlData {
		forNewTable = Boolean(forNewTable) || this.isTableNew(relationship.childTableId);
		const state = this.newState;
		const childTable = state.schema.tables[relationship.childTableId];
		const childNamespace = findTableNamespace(childTable, state.schema)?.name ?? '';
		const parentTable = state.schema.tables[relationship.parentTableId];
		const parentNamespace = findTableNamespace(parentTable, state.schema)?.name ?? '';
		const childColumnNames = relationship.childColumnIds.map(id => state.schema.columns[id].name);
		const parentColumnNames = relationship.parentColumnIds.map(id => state.schema.columns[id].name);
		
		const constraintChildColumns = childColumnNames.map(n => quoteName(n, state)).join(', ');
		const constraintParentColumns = parentColumnNames.map(n => quoteName(n, state)).join(', ');
		const constraintName = quoteName(relationship.name, state);
		const parentTableName = quoteObjectName(parentNamespace, parentTable.name, state);
		let text = `CONSTRAINT ${constraintName} FOREIGN KEY (${constraintChildColumns}) REFERENCES ${parentTableName} (${constraintParentColumns})`;
		
		if (relationship.onUpdate) {
			text += ` ON UPDATE ${generateForeignKeyChangeSql(relationship.onUpdate)}`;
		}
		
		if (relationship.onDelete) {
			text += ` ON DELETE ${generateForeignKeyChangeSql(relationship.onDelete)}`;
		}
		
		if (relationship.deferrability) {
			text += ` ${generateForeignKeyDeferrability(relationship.deferrability)}`;
		}
		
		return {
			text: !forNewTable ? `ALTER TABLE ${quoteObjectName(childNamespace, childTable.name, state)} ADD ${text};` : text,
			requiresNewTable: forNewTable,
			isCompleteSqlStatement: !forNewTable,
		};
	}
	
	deleteRelationship(relationship: RelationshipData): SqlData {
		const state = this.newState;
		const childTable = this.oldSchemaState.tables[relationship.childTableId];
		const childNamespace = findTableNamespace(childTable, state.schema)?.name ?? '';
		
		return {
			text: `ALTER TABLE ${quoteObjectName(childNamespace, childTable.name, state)} DROP CONSTRAINT ${quoteName(relationship.name, state)};`,
			isCompleteSqlStatement: true,
		};
	}
	
	generateIndexDifference(diff: IndexDiff, oldIndex: IndexData | undefined, newIndex: IndexData | undefined): SqlData {
		const state = this.newState;
		
		if (diff.isDeleted) {
			return this.deleteIndex(oldIndex!);
		}
		else if (diff.isNew) {
			return this.createIndex(newIndex!);
		}
		
		oldIndex = oldIndex!;
		newIndex = newIndex!;
		
		const data: SqlData = {
			text: '',
			isCompleteSqlStatement: true,
		};
		
		if (diff.propertyChanges.includes('name') && diff.propertyChanges.length === 1) {
			const table = getTable(state, newIndex.tableId);
			const namespace = findTableNamespace(table, state.schema)?.name ?? '';
			const tableName = quoteObjectName(namespace, table.name, state);
			const oldIndexName = quoteName(oldIndex.name, state);
			const newIndexName = quoteName(newIndex.name, state);
			data.text += `ALTER TABLE ${tableName} RENAME CONSTRAINT ${oldIndexName} TO ${newIndexName};`;
		}
		else {
			data.text += this.deleteIndex(oldIndex).text + '\n' + this.createIndex(newIndex).text;
		}
		
		return data;
	}
	
	createIndex(index: IndexData): SqlData {
		const state = this.newState;
		const table = getTable(state, index.tableId);
		const namespace = findTableNamespace(table, state.schema)?.name ?? '';
		const columnNames = index.columnIds.map(id => getColumn(state, id).name);
		const columnDefTexts = [];
		for (let i = 0; i < columnNames.length; ++i) {
			columnDefTexts.push(`${quoteName(columnNames[i], state)} ${getIndexDirectionSql(index.directions[i])}`);
		}
		
		return {
			text: `CREATE INDEX ${quoteObjectName(namespace, index.name, state)} ON ${quoteObjectName(namespace, table.name, state)} (${columnDefTexts.join(', ')});`,
			isCompleteSqlStatement: true,
		};
	}
	
	deleteIndex(index: IndexData): SqlData {
		const state = this.newState;
		const namespace = findIndexNamespace(index, state.schema)?.name ?? '';
		
		return {
			text: `DROP INDEX ${quoteObjectName(namespace, index.name, state)} CASCADE;`,
			isCompleteSqlStatement: true,
		};
	}
	
	generateUniqueDifference(diff: UniqueDiff, oldUnique: UniqueData | undefined, newUnique: UniqueData | undefined): SqlData {
		const state = this.newState;
		if (diff.isDeleted) {
			return this.deleteUnique(oldUnique!);
		}
		else if (diff.isNew) {
			return this.createUnique(newUnique!);
		}
		
		oldUnique = oldUnique!;
		newUnique = newUnique!;
		
		const data: SqlData = {
			text: '',
			isCompleteSqlStatement: true,
		};
		
		if (diff.propertyChanges.includes('name') && diff.propertyChanges.length === 1) {
			const table = getTable(state, newUnique.tableId);
			const namespace = findTableNamespace(table, state.schema)?.name ?? '';
			const tableName = quoteObjectName(namespace, table.name, state);
			const oldUniqueName = quoteName(oldUnique.name, state);
			const newUniqueName = quoteName(newUnique.name, state);
			data.text += `ALTER TABLE ${tableName} RENAME CONSTRAINT ${oldUniqueName} TO ${newUniqueName};`;
		}
		else {
			data.text += this.deleteUnique(oldUnique).text + '\n' + this.createUnique(newUnique).text;
		}
		
		return data;
	}
	
	createUnique(unique: UniqueData, forNewTable?: boolean): SqlData {
		forNewTable = Boolean(forNewTable) || this.isTableNew(unique.tableId);
		const state = this.newState;
		const table = getTable(state, unique.tableId);
		const namespace = findTableNamespace(table, state.schema)?.name ?? '';
		const columnNames = unique.columnIds.map(id => getColumn(state, id).name);
		const columnNameDefs = [];
		
		for (let i = 0; i < unique.directions.length; ++i) {
			const direction = unique.isIndex ? ` ${getIndexDirectionSql(unique.directions[i])}` : '';
			columnNameDefs.push(`${quoteName(columnNames[i], state)}${direction}`);
		}
		
		if (unique.isIndex) {
			return {
				text: `CREATE UNIQUE INDEX ${quoteObjectName(namespace, unique.name, state)} ON ${quoteObjectName(namespace, table.name, state)} (${columnNameDefs.join(', ')});`,
				isCompleteSqlStatement: true,
			};
		}
		
		const text = `CONSTRAINT ${quoteName(unique.name, state)} UNIQUE (${columnNameDefs.join(', ')})`;
		return {
			text: !forNewTable ? `ALTER TABLE ${quoteObjectName(namespace, table.name, state)} ADD ${text};` : text,
			requiresNewTable: forNewTable,
			isCompleteSqlStatement: !forNewTable,
		};
	}
	
	deleteUnique(unique: UniqueData): SqlData {
		const state = this.newState;
		const table = this.oldSchemaState.tables[unique.tableId];
		const namespace = findTableNamespace(table, state.schema)?.name ?? '';
		
		if (unique.isIndex) {
			return {
				text: `DROP INDEX ${quoteObjectName(namespace, unique.name, state)} CASCADE;`,
				isCompleteSqlStatement: true,
			};
		}
		
		return {
			text: `ALTER TABLE ${quoteObjectName(namespace, table.name, state)} DROP CONSTRAINT ${quoteName(unique.name, state)};`,
			isCompleteSqlStatement: true,
		};
	}
	
	generatePrimaryKeyDifference(diff: PrimaryKeyDiff, oldPrimaryKey: PrimaryKeyData | undefined, newPrimaryKey: PrimaryKeyData | undefined): SqlData {
		const state = this.newState;
		if (diff.isDeleted) {
			return this.deletePrimaryKey(oldPrimaryKey!);
		}
		else if (diff.isNew) {
			const pk = newPrimaryKey!;
			const isNewTable = !(pk.tableId in this.oldSchemaState.tables);
			return this.createPrimaryKey(newPrimaryKey!, isNewTable);
		}
		
		oldPrimaryKey = oldPrimaryKey!;
		newPrimaryKey = newPrimaryKey!;
		
		const data: SqlData = {
			text: '',
			isCompleteSqlStatement: true,
		};
		
		if (diff.propertyChanges.includes('name') && diff.propertyChanges.length === 1) {
			const table = getTable(state, newPrimaryKey.tableId);
			const namespace = findTableNamespace(table, state.schema)?.name ?? '';
			const tableName = quoteObjectName(namespace, table.name, state);
			const oldPrimaryKeyName = quoteName(oldPrimaryKey.name, state);
			const newPrimaryKeyName = quoteName(newPrimaryKey.name, state);
			data.text += `ALTER TABLE ${tableName} RENAME CONSTRAINT ${oldPrimaryKeyName} TO ${newPrimaryKeyName};`;
		}
		else {
			data.text += this.deletePrimaryKey(oldPrimaryKey).text + '\n' + this.createPrimaryKey(newPrimaryKey).text;
		}
		
		return data;
	}
	
	createPrimaryKey(primaryKey: PrimaryKeyData, forNewTable?: boolean): SqlData {
		forNewTable = Boolean(forNewTable) || this.isTableNew(primaryKey.tableId);
		const state = this.newState;
		const table = getTable(state, primaryKey.tableId);
		const namespace = findTableNamespace(table, state.schema)?.name ?? '';
		const columnNames = primaryKey.columnIds.map(id => getColumn(state, id).name);
		const columnDefTexts = [];
		for (let i = 0; i < columnNames.length; ++i) {
			columnDefTexts.push(`${quoteName(columnNames[i], state)}`);
		}
		
		const text = `CONSTRAINT ${quoteName(primaryKey.name, state)} PRIMARY KEY (${columnDefTexts.join(', ')})`;
		return {
			text: !forNewTable ? `ALTER TABLE ${quoteObjectName(namespace, table.name, state)} ADD ${text};` : text,
			requiresNewTable: forNewTable,
			isCompleteSqlStatement: !forNewTable,
		};
	}
	
	deletePrimaryKey(primaryKey: PrimaryKeyData): SqlData {
		const state = this.newState;
		const table = this.oldSchemaState.tables[primaryKey.tableId];
		const namespace = findTableNamespace(table, state.schema)?.name ?? '';
		
		return {
			text: `ALTER TABLE ${quoteObjectName(namespace, table.name, state)} DROP CONSTRAINT ${quoteName(primaryKey.name, state)};`,
			isCompleteSqlStatement: true,
		};
	}
}

function postgresColumnLanguage(type: ColumnTypeData): string {
	const lang = type.languages.find(l => l.name === "postgres");
	if (!lang) {
		throw new Error("No column language named 'postgres'");
	}
	return lang.value;
}
