import { createNotification } from "@/actions";
import { MessageAlertType, RootState, RunCodeContainer, StructOutputData } from "@/data";
import { getTables } from "@/selectors";
import { codeService } from "@/services";
import { Dispatch } from "@/store";

export class StructOutputGenerator {
	structOutput: StructOutputData;
	state: RootState;
	
	constructor(structOutput: StructOutputData, state: RootState) {
		this.structOutput = structOutput;
		this.state = state;
	}
	
	async run(dispatch: Dispatch) {
		const runContainers = this.#createStructOutputRunContainer();
		const results = await Promise.all(runContainers.map(codeService.runCodeContainer));
		for (const res of results) {
			if (res.ok) {
				dispatch(createNotification(
					MessageAlertType.success,
					"Successfully output data structure.",
					res.ok.result.fileName,
				));
			}
			else if (res.err) {
				dispatch(createNotification(
					MessageAlertType.error,
					"Could not perform data output",
					res.err.type === 'runFailure' ? res.err.error.message : '',
				));
			}
		}
	}
	
	#createStructOutputRunContainer(): RunCodeContainer[] {
		const excludedTableIds = this.structOutput.excludedTableIds;
		const includedTableIds = this.structOutput.includedTableIds;
		const ret: RunCodeContainer[] = [];
		
		for (const table of Object.values(getTables(this.state))) {
			if (excludedTableIds.includes(table.id)) {
				continue;
			}
			if (includedTableIds.length > 0 && !includedTableIds.includes(table.id)) {
				continue;
			}
			
			ret.push({
				id: this.structOutput.options.type,
				structOutputOptions: {
					structOutputId: this.structOutput.id,
					tableId: table.id,
					outputToFile: true,
				},
				nameOptions: null,
				exportOptions: null,
			});
		}
		return ret;
	}
}
/* 
async function performOutput(structOutput: StructOutputData, fileName: string, folderPath: string, code: string) {
	const filePath = folderPath + pathSeparator() + fileName;
	const replaceData = {
		code,
		outputFileId: (fileName.replaceAll(/[^\w]/g, "_") + "_" + structOutput.id).toLocaleUpperCase(),
	};
	
	return performCodeReplaceAndWrite(structOutput, filePath, replaceData);
}

async function performCodeReplaceAndWrite(structOutput: StructOutputData, filePath: string, replace: TemplateReplaceData) {
	let contents = convertTemplateString(structOutput.code, replace);
	if (structOutput.options.type === 'python' && structOutput.options.makeDataclass) {
		contents = "from dataclasses import dataclass\n\n" + contents;
	}
	
	return fileService.writeFile(filePath, contents);
}
 */