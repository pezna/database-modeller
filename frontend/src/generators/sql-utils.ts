import { ColumnIndexDirection, ConflictClause, ForeignKeyChangeAction, ForeignKeyDeferrability, RootState, SimpleSchemaState } from "@/data";

export function quoteName(name: string, _: RootState) {
	return `"${name}"`;
}

export function quoteObjectName(schema: string | null, name: string, state: RootState) {
	if (schema) {
		return `${quoteName(schema, state)}.${quoteName(name, state)}`;
	}
	
	return quoteName(name, state);
}

export function getIndexDirectionSql(direction: ColumnIndexDirection): string {
	return direction === ColumnIndexDirection.ascending ? 'ASC' : 'DESC';
}

export function generateForeignKeyChangeSql(change: ForeignKeyChangeAction): string {
	switch (change) {
		case ForeignKeyChangeAction.setNull:
			return 'SET NULL';
		case ForeignKeyChangeAction.setDefault:
			return 'SET DEFAULT';
		case ForeignKeyChangeAction.cascade:
			return 'CASCADE';
		case ForeignKeyChangeAction.restrict:
			return 'RESTRICT';
		case ForeignKeyChangeAction.noAction:
			return 'NO ACTION';
	}
}

export function generateConflictClauseSql(clause: ConflictClause): string {
	switch (clause) {
		case ConflictClause.rollback:
			return 'ROLLBACK';
		case ConflictClause.abort:
			return 'ABORT';
		case ConflictClause.fail:
			return 'FAIL';
		case ConflictClause.ignore:
			return 'IGNORE';
		case ConflictClause.replace:
			return 'REPLACE';
	}
}

export function generateForeignKeyDeferrability(defer: ForeignKeyDeferrability): string {
	switch (defer) {
		case ForeignKeyDeferrability.notDeferrable:
			return 'NOT DEFERRABLE';
		case ForeignKeyDeferrability.initiallyDeferred:
			return 'DEFERRABLE INITIALLY DEFERRED';
		case ForeignKeyDeferrability.initiallyImmediate:
			return 'DEFERRABLE INITIALLY IMMEDIATE';
	}
}

export function createTableIdOrderForSql(schemaState: SimpleSchemaState): string[] {
	const ids: {[id: string]: number} = {};
	let nextNewIndex = 100; // arbitrary
	
	for (const relationship of Object.values(schemaState.relationships)) {
		const { childTableId, parentTableId } = relationship;
		let firstIndex = ids[parentTableId];
		let secondIndex = ids[childTableId];
		
		if (firstIndex !== undefined && secondIndex === undefined) {
			secondIndex = firstIndex + 1;
			ids[childTableId] = secondIndex;
		}
		else if (firstIndex === undefined && secondIndex !== undefined) {
			firstIndex = secondIndex - 1;
			ids[parentTableId] = firstIndex;
		}
		else if (firstIndex === undefined && secondIndex === undefined) {
			firstIndex = nextNewIndex;
			secondIndex = firstIndex + 50; // arbitrary
			ids[parentTableId] = firstIndex;
			ids[childTableId] = secondIndex;
		}
		
		nextNewIndex += 100; // arbitrary
	}
	
	return Object.entries(ids).sort((a, b) => a[1] - b[1]).map(a => a[0]);
}