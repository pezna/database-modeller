import { AngleUnit, LSystemComponentData } from "@/data";
import { IRandom, XsAdd } from "@thi.ng/random";
import LSystem from "lindenmayer";

export class LSystemGenerator {
	lsystem!: LSystem;
	results!: string[];
	maxIterations!: number;
	movement: LSystemMovementTurtle;
	
	constructor(data: LSystemComponentData) {
		this.movement = new LSystemMovementTurtle(data);
		this.updateData(data);
	}
	
	updateData(data: LSystemComponentData) {
		this.lsystem = new LSystem({
			axiom: data.rules["s"],
			productions: data.rules,
		});
		this.results = [this.lsystem.axiom];
		this.maxIterations = data.maxIterations;
		this.movement.resetContext(data);
	}
	
	getStep(step: number) {
		if (step > this.maxIterations) {
			throw new Error(`Cannot get step ${step} when max iterations is ${this.maxIterations}`);
		}
		
		if (step < this.results.length) {
			this.lsystem.setAxiom(this.results[step]);
			console.log(step + " already exists");
			return this.results[step];
		}
		
		const startingLen = this.results.length;
		for (let i = step; i >= startingLen; --i) {
			console.log("iterating lsystem " + i);
			this.results.push(this.lsystem.iterate());
		}
		
		this.lsystem.setAxiom(this.results[step]);
		return this.results[step];
	}
	
	runFinals(): LSystemMovementContext {
		this.lsystem.setFinals(this.movement.finals);
		this.lsystem.final();
		this.lsystem.setFinals({});
		
		return this.movement.context;
	}
}

type Vec = [number, number]

interface LSystemMovementContext {
	/**
	 * Current position. Default: [0,0]
	 */
	pos: Vec;
	/**
	 * Current direction (in radians)
	 */
	theta: number;
	/**
	 * Rotation angle for "+" / "-" symbols
	 */
	delta: number;
	/**
	 * Max. random direction change when processing "/" symbol.
	 * Normalized percentage of `delta`. Default: 0.25 (25%)
	 */
	jitter: number;
	/**
	 * Step distance. Default: 1
	 */
	step: number;
	/**
	 * Probability to keep current branch alive when processing "k"
	 * symbol. Default: 0.99
	 */
	aliveProb: number;
	/**
	 * Decay factor for `delta`. Should be in (0,1) interval.
	 * Default: 0.9
	 */
	decayDelta: number;
	/**
	 * Decay factor for `step`. Should be in (0,1) interval.
	 * Default: 0.9
	 */
	decayStep: number;
	/**
	 * Decay factor for `aliveProp`.
	 * Default: 0.95
	 */
	decayAlive: number;
	/**
	 * PRNG to use for probability checks. Default: SYSTEM
	 */
	rnd: IRandom;
	/**
	 * Alive flag. Default: true
	 */
	alive: boolean;
	/**
	 * Current recorded path
	 */
	curr: Vec[];
	/**
	 * Accumulator of all recorded paths
	 */
	paths: Vec[][];
	/**
	 * Branch stack
	 */
	stack: LSystemMovementContext[];
}

export class LSystemMovementTurtle {
	context!: LSystemMovementContext;
	finals!: Record<string, () => void>;
	initialPosition!: Vec;
	
	constructor(data: LSystemComponentData) {
		this.resetContext(data);
	}
	
	resetContext(data: LSystemComponentData) {
		this.initialPosition = [...data.initialPosition];
		this.context = {
			pos: [...data.initialPosition],
			theta: -Math.PI / 2,//ensureRadians(data.initialDirection, data.initialDirectionUnit),
			delta: Math.PI / 10,//ensureRadians(data.rotation, data.rotationUnit),
			step: data.stepDistance,
			jitter: 0.5,
			decayDelta: 0.98,
			decayStep: 0.85,
			decayAlive: 0.975,
			aliveProb: 0.999,
			rnd: new XsAdd(0x1337c0de),
			alive: true,
			curr: [[...data.initialPosition]],
			paths: [],
			stack: [],
		};
		this.#resetFinals();
		this.resetPositions();
	}
	
	resetPositions() {
		this.context.pos = [...this.initialPosition];
		this.context.curr = [[...this.context.pos]];
		this.context.paths = [];
		this.context.stack = [];
	}
	
	#resetFinals() {
		this.finals = {
			// move forward
			f: () => {
				const ctx = this.context;
				if (ctx.alive) {
					const change = cossin(ctx.theta, ctx.step);
					ctx.pos = [ctx.pos[0] + change[0], ctx.pos[1] + change[1]];
					// console.log(ctx.pos, change);
					ctx.curr.push(ctx.pos);
					// console.log("adding", ctx.curr.length);
				}
			},
			g: () => {
				const ctx = this.context;
				if (ctx.alive) {
					ctx.curr.length > 1 && ctx.paths.push(ctx.curr);
					const change = cossin(ctx.theta, ctx.step);
					ctx.pos = [ctx.pos[0] + change[0], ctx.pos[1] + change[1]];
					ctx.curr = [ctx.pos];
				}
			},
			// rotate ccw
			"+": () => {
				const ctx = this.context;
				ctx.alive && (ctx.theta += ctx.delta);
			},
			// rotate cw
			"-": () => {
				const ctx = this.context;
				ctx.alive && (ctx.theta -= ctx.delta);
			},
			// shrink rotation angle
			">": () => {
				const ctx = this.context;
				ctx.alive && (ctx.delta *= ctx.decayDelta);
			},
			// grow rotation angle
			"<": () => {
				const ctx = this.context;
				ctx.alive && (ctx.delta /= ctx.decayDelta);
			},
			// decay step distance
			"!": () => {
				const ctx = this.context;
				ctx.alive && (ctx.step *= ctx.decayStep);
			},
			// grow step distance
			"^": () => {
				const ctx = this.context;
				ctx.alive && (ctx.step /= ctx.decayStep);
			},
			"/": () => {
				const ctx = this.context;
				ctx.alive && (ctx.theta += ctx.rnd.norm(ctx.delta * ctx.jitter));
			},
			// kill branch (stochastic)
			k: () => {
				const ctx = this.context;
				ctx.alive && (ctx.alive = ctx.rnd.probability(ctx.aliveProb));
			},
			// decay alive probability
			p: () => {
				const ctx = this.context;
				ctx.alive && (ctx.aliveProb *= ctx.decayAlive);
			},
			// grow alive probability
			P: () => {
				const ctx = this.context;
				ctx.alive && (ctx.aliveProb /= ctx.decayAlive);
			},
			// start branch / store context on stack
			"[": () => {
				const ctx = this.context;
				if (ctx.alive && ctx.curr.length > 1) {
					ctx.paths.push(ctx.curr);
				}
				ctx.curr = [ctx.pos];
				const copy = { ...ctx };
				//@ts-ignore
				delete copy.paths;
				//@ts-ignore
				delete copy.stack;
				ctx.stack.push(copy);
				ctx.curr = [ctx.pos];
			},
			// end branch / pop context from stack
			"]": () => {
				const ctx = this.context;
				if (ctx.alive && ctx.curr.length > 1) {
					ctx.paths.push(ctx.curr);
				}
				const prev = ctx.stack.pop();
				if (!prev) {
					throw new Error("stack empty");
				}
				Object.assign(ctx, prev);
			},
		};
	}
}

const cossin = (theta: number, n = 1): [number, number] => [
	Math.cos(theta) * n,
	Math.sin(theta) * n,
];

function ensureRadians(value: number, valueUnit: AngleUnit) {
	return valueUnit === AngleUnit.degrees ? (Math.PI / 180) * value : value;
}
