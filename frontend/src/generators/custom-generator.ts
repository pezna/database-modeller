import { CustomExport, RootState, RunCodeContainer, SchemaExportItem, TableExportItem } from "@/data";

export function createExportRunContainers(customExport: CustomExport, state: RootState): RunCodeContainer[] {
	return [
		...addTableExportRunContainers(Object.values(customExport.tableExports), state),
		...addSchemaExportRunContainers(Object.values(customExport.fullSchemaExports)),
	];
}

function addTableExportRunContainers(exports: TableExportItem[], state: RootState): RunCodeContainer[] {
	const ret: RunCodeContainer[] = [];
	for (const exportData of exports) {
		
		for (const table of Object.values(state.schema.tables)) {
			if (exportData.excludedTableIds.includes(table.id)) {
				continue;
			}
			if (exportData.includedTableIds.length > 0 && !exportData.includedTableIds.includes(table.id)) {
				continue;
			}
			
			ret.push({
				id: exportData.id,
				exportOptions: {
					customExportId: exportData.id,
					tableId: table.id,
					outputToFile: true,
				},
				nameOptions: null,
				structOutputOptions: null,
			});
		}
	}
	return ret;
}

function addSchemaExportRunContainers(exports: SchemaExportItem[]): RunCodeContainer[] {
	const ret: RunCodeContainer[] = [];
	for (const exportData of exports) {
		ret.push({
			id: exportData.id,
			nameOptions: null,
			exportOptions: null,
			structOutputOptions: null,
		});
	}
	return ret;
}
