import { ColumnData, ColumnTypeData, IndexData, NamespaceData, PrimaryKeyData, RelationshipData, TableData, UniqueData } from "@/data";
import { getColumn, getTable } from "@/selectors";
import { ColumnDiff, IndexDiff, NamespaceDiff, PrimaryKeyDiff, RelationshipDiff, TableDiff, UniqueDiff } from "@/utils";
import { SqlData, SqlGenerator } from "./sql-generator-base";
import { generateConflictClauseSql, generateForeignKeyChangeSql, generateForeignKeyDeferrability, getIndexDirectionSql, quoteName } from "./sql-utils";

export class SqliteGenerator extends SqlGenerator {
	generateSql() {
		const ret = super.generateSql();
		if (ret.sql) {
			return {
				sql: `PRAGMA foreign_keys = OFF;\n${ret.sql}\nPRAGMA foreign_keys = ON;`,
			};
		}
		
		return ret;
	}
	
	generateNamespaceDifference(diff: NamespaceDiff, oldNamespace: NamespaceData | undefined, newNamespace: NamespaceData | undefined): SqlData {
		if (diff.isDeleted) {
			return this.deleteNamespace(oldNamespace!);
		}
		else if (diff.isNew) {
			return this.createNamespace(newNamespace!);
		}
		
		return {
			text: '',
			isCompleteSqlStatement: true,
		};
	}
	
	createNamespace(namespace: NamespaceData): SqlData {
		return {
			text: '',
			isCompleteSqlStatement: true,
		};
	}
	
	deleteNamespace(namespace: NamespaceData): SqlData {
		return {
			text: '',
			isCompleteSqlStatement: true,
		};
	}
	
	generateTableDifference(diff: TableDiff, oldTable: TableData | undefined, newTable: TableData | undefined): SqlData {
		const state = this.newState;
		
		if (diff.isDeleted) {
			return this.deleteTable(oldTable!);
		}
		else if (diff.isNew) {
			return this.createTable(newTable!);
		}
		
		oldTable = oldTable!;
		newTable = newTable!;
		
		const numPropertyChanges = diff.propertyChanges.length;
		const isNameChanged = diff.propertyChanges.includes('name');
		const isRelationshipChanged = diff.propertyChanges.includes('relationshipIds');
		// in sqlite, a relationship will always result in a new table being created, and that will be handled above in generateSql()
		const hasOtherChanges = numPropertyChanges > 1 || (numPropertyChanges === 1 && !isNameChanged && !isRelationshipChanged);
		
		if (hasOtherChanges) {
			return this.recreateTable(oldTable, newTable);
		}
		else if (isNameChanged) {
			return {
				text: `ALTER TABLE ${quoteName(oldTable.name, state)} RENAME TO ${quoteName(newTable.name, state)};`,
				isCompleteSqlStatement: true,
			};
		}
		
		return {
			text: '',
			isCompleteSqlStatement: true,
			createdNewTable: false,
		};
	}
	
	generateColumnDifference(diff: ColumnDiff, oldColumn: ColumnData | undefined, newColumn: ColumnData | undefined): SqlData {
		const state = this.newState;
		
		if (diff.isDeleted) {
			return this.deleteColumn(oldColumn!);
		}
		else if (diff.isNew) {
			return this.createColumn(newColumn!);
		}
		else if (diff.propertyChanges.length === 0) {
			return {
				text: '',
			};
		}
		
		oldColumn = oldColumn!;
		newColumn = newColumn!;
		
		const isNameChanged = diff.propertyChanges.includes('name');
		const oldColumnType = this.oldSchemaState.columnTypes[oldColumn.typeId];
		const newColumnType = state.schema.columnTypes[newColumn.typeId];
		const typeTextChanged = sqliteColumnLanguage(oldColumnType) !== sqliteColumnLanguage(newColumnType);
		const hasOtherChanges = diff.propertyChanges.length > 1 || (diff.propertyChanges.length === 1 && !isNameChanged) || typeTextChanged;
		
		if (isNameChanged && !hasOtherChanges) {
			const table = getTable(state, newColumn.tableId);
			
			return {
				text: `ALTER TABLE ${quoteName(table.name, state)} RENAME COLUMN ${quoteName(oldColumn.name, state)} TO ${quoteName(newColumn.name, state)};`,
				isCompleteSqlStatement: true,
			};
		}
		
		return this.createColumn(newColumn);
	}
	
	createColumn(column: ColumnData): SqlData {
		const state = this.newState;
		const columnTypeText = sqliteColumnLanguage(state.schema.columnTypes[column.typeId]);
		let text = `${quoteName(column.name, state)} ${columnTypeText}`;
		
		if (column.options.notNull) {
			text += ' NOT NULL';
		}
		
		if (column.options.defaultValue) {
			text += ` DEFAULT ${column.options.defaultValue}`;
		}
		
		return {
			text,
			requiresNewTable: true,
			createdNewColumn: true,
		};
	}
	
	deleteColumn(column: ColumnData): SqlData {
		return {
			text: '',
			requiresNewTable: true,
		};
	}
	
	generateRelationshipDifference(diff: RelationshipDiff, oldRelationship: RelationshipData | undefined, newRelationship: RelationshipData | undefined): SqlData {
		if (diff.isDeleted) {
			return this.deleteRelationship(oldRelationship!);
		}
		else if (diff.isNew) {
			return this.createRelationship(newRelationship!);
		}
		
		if (diff.propertyChanges.includes('isNotExported')) {
			if (newRelationship!.isNotExported) {
				return this.deleteRelationship(oldRelationship!);
			}
			else {
				return this.createRelationship(newRelationship!);
			}
		}
		
		return {
			text: '',
			requiresNewTable: true,
		};
	}
	
	createRelationship(relationship: RelationshipData): SqlData {
		const state = this.newState;
		const parentTableName = state.schema.tables[relationship.parentTableId].name;
		const childColumnNames = relationship.childColumnIds.map(id => state.schema.columns[id].name);
		const parentColumnNames = relationship.parentColumnIds.map(id => state.schema.columns[id].name);
		
		const constraintChildColumns = childColumnNames.map(n => quoteName(n, state)).join(', ');
		const constraintParentColumns = parentColumnNames.map(n => quoteName(n, state)).join(', ');
		
		let text = `CONSTRAINT ${quoteName(relationship.name, state)} FOREIGN KEY (${constraintChildColumns}) REFERENCES ${quoteName(parentTableName, state)} (${constraintParentColumns})`;
		
		if (relationship.onUpdate) {
			text += ` ON UPDATE ${generateForeignKeyChangeSql(relationship.onUpdate)}`;
		}
		
		if (relationship.onDelete) {
			text += ` ON DELETE ${generateForeignKeyChangeSql(relationship.onDelete)}`;
		}
		
		if (relationship.deferrability) {
			text += ` ${generateForeignKeyDeferrability(relationship.deferrability)}`;
		}
		
		return {
			text,
			requiresNewTable: true,
		};
	}
	
	deleteRelationship(relationship: RelationshipData): SqlData {
		return {
			text: '',
			requiresNewTable: true,
		};
	}
	
	generateIndexDifference(diff: IndexDiff, oldIndex: IndexData | undefined, newIndex: IndexData | undefined): SqlData {
		if (diff.isDeleted) {
			return this.deleteIndex(oldIndex!);
		}
		else if (diff.isNew) {
			return this.createIndex(newIndex!);
		}
		
		return {
			text: '',
			requiresNewTable: true,
		};
	}
	
	createIndex(index: IndexData): SqlData {
		const state = this.newState;
		const table = getTable(state, index.tableId);
		const columnNames = index.columnIds.map(id => getColumn(state, id).name);
		const columnDefTexts = [];
		for (let i = 0; i < columnNames.length; ++i) {
			columnDefTexts.push(`${quoteName(columnNames[i], state)} ${getIndexDirectionSql(index.directions[i])}`);
		}
		
		return {
			text: `CREATE INDEX ${quoteName(index.name, state)} ON ${quoteName(table.name, state)} (${columnDefTexts.join(', ')});`,
			isCompleteSqlStatement: true,
		};
	}
	
	deleteIndex(index: IndexData): SqlData {
		const state = this.newState;
		
		return {
			text: `DROP INDEX ${quoteName(index.name, state)};`,
			isCompleteSqlStatement: true,
		};
	}
	
	generateUniqueDifference(diff: UniqueDiff, oldUnique: UniqueData | undefined, newUnique: UniqueData | undefined): SqlData {
		if (diff.isDeleted) {
			return this.deleteUnique(oldUnique!);
		}
		else if (diff.isNew) {
			return this.createUnique(newUnique!);
		}
		
		return {
			text: '',
			requiresNewTable: true,
		};
	}
	
	createUnique(unique: UniqueData): SqlData {
		const state = this.newState;
		const table = getTable(state, unique.tableId);
		const columnNames = unique.columnIds.map(id => getColumn(state, id).name);
		const columnDefTexts = [];
		for (let i = 0; i < columnNames.length; ++i) {
			columnDefTexts.push(`${quoteName(columnNames[i], state)} ${getIndexDirectionSql(unique.directions[i])}`);
		}
		
		if (unique.isIndex) {
			return {
				text: `CREATE UNIQUE INDEX ${quoteName(unique.name, state)} ON ${quoteName(table.name, state)} (${columnDefTexts.join(', ')});`,
				isCompleteSqlStatement: true,
			};
		}
		
		return {
			text: `CONSTRAINT ${quoteName(unique.name, state)} UNIQUE (${columnDefTexts.join(', ')})`,
			requiresNewTable: true,
		};
	}
	
	deleteUnique(unique: UniqueData): SqlData {
		const state = this.newState;
		
		if (unique.isIndex) {
			return {
				text: `DROP INDEX ${quoteName(unique.name, state)};`,
				isCompleteSqlStatement: true,
			};
		}
		
		return {
			text: '',
			requiresNewTable: true,
		};
	}
	
	generatePrimaryKeyDifference(diff: PrimaryKeyDiff, oldPrimaryKey: PrimaryKeyData | undefined, newPrimaryKey: PrimaryKeyData | undefined): SqlData {
		if (diff.isDeleted) {
			return this.deletePrimaryKey(oldPrimaryKey!);
		}
		else if (diff.isNew) {
			return this.createPrimaryKey(newPrimaryKey!);
		}
		
		return {
			text: '',
			requiresNewTable: true,
		};
	}
	
	createPrimaryKey(primaryKey: PrimaryKeyData): SqlData {
		const state = this.newState;
		const columnNames = primaryKey.columnIds.map(id => getColumn(state, id).name);
		const columnDefTexts = [];
		for (let i = 0; i < columnNames.length; ++i) {
			columnDefTexts.push(`${quoteName(columnNames[i], state)} ${getIndexDirectionSql(primaryKey.directions[i])}`);
		}
		
		const conflictText = primaryKey.onConflict ? ' ON CONFLICT ' + generateConflictClauseSql(primaryKey.onConflict) : '';
		
		return {
			text: `CONSTRAINT ${quoteName(primaryKey.name, state)} PRIMARY KEY (${columnDefTexts.join(', ')})${conflictText}`,
			requiresNewTable: true,
		};
	}
	
	deletePrimaryKey(primaryKey: PrimaryKeyData): SqlData {
		return {
			text: '',
			requiresNewTable: true,
		};
	}
}

function sqliteColumnLanguage(type: ColumnTypeData): string {
	const lang = type.languages.find(l => l.name === "sqlite");
	if (!lang) {
		throw new Error("No column language named 'sqlite'");
	}
	return lang.value;
}
