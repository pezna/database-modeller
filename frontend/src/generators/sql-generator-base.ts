import { ColumnData, IndexData, NamespaceData, PrimaryKeyData, RelationshipData, RootState, SimpleSchemaState, SqlGeneratorResultData, TableData, UniqueData } from "@/data";
import { getColumn } from "@/selectors";
import { ColumnDiff, IndexDiff, NamespaceDiff, PrimaryKeyDiff, RelationshipDiff, SchemaDiff, TableDiff, UniqueDiff, createSchemaDiff, findTableNamespace } from "@/utils";
import { createTableIdOrderForSql, quoteName, quoteObjectName } from "./sql-utils";

export abstract class SqlGenerator {
	oldSchemaState: SimpleSchemaState;
	newState: RootState;
	schemaDiff: SchemaDiff;
	recreatedTableIds: Set<string>;
	
	constructor(oldSchemaState: SimpleSchemaState, newState: RootState) {
		this.oldSchemaState = oldSchemaState;
		this.newState = newState;
		this.schemaDiff = createSchemaDiff(oldSchemaState, newState.schema);
		this.recreatedTableIds = new Set<string>();
	}
	
	generateSql(): SqlGeneratorResultData {
		const currentTableIdOrder = createTableIdOrderForSql(this.newState.schema);
		const allTableIds = Object.keys(this.schemaDiff.tables).sort((a, b) => currentTableIdOrder.indexOf(a) - currentTableIdOrder.indexOf(b));
		const changes: SqlData[] = [];
		
		const namespaceKeys = new Set([...Object.keys(this.oldSchemaState.namespaces), ...Object.keys(this.newState.schema.namespaces)]);
		const namespacesData = this.generateItemDifference(
			this.generateNamespaceDifference.bind(this),
			this.oldSchemaState.namespaces,
			this.newState.schema.namespaces,
			this.schemaDiff.namespaces,
			namespaceKeys
		);
		// Create new namespaces at the very top here.
		// Namespace deletions will be added after the for-loop
		namespacesData.filter(s => s.isCompleteSqlStatement && s.createdNewNamespace).forEach(s => changes.push(s));
		
		for (const tableId of allTableIds) {
			let oldTable: TableData | undefined = this.oldSchemaState.tables[tableId];
			let newTable: TableData | undefined = this.newState.schema.tables[tableId];
			
			if (oldTable?.isMixin && newTable && !newTable.isMixin) {
				// behave as if a new table has been created
				oldTable = undefined;
				setAllTableDiff(this.newState.schema, newTable.id, this.schemaDiff, true, false);
			}
			else if (oldTable && !oldTable.isMixin && newTable?.isMixin) {
				// behave as if table has been deleted
				newTable = undefined;
				setAllTableDiff(this.oldSchemaState, oldTable.id, this.schemaDiff, false, true);
			}
			else if (newTable?.isMixin || oldTable?.isMixin) {
				// do not export mixins
				continue;
			}
			
			const columnIds = new Set([
				...(oldTable?.columnIds ?? []),
				...(newTable?.columnIds ?? []),
			]);
			const relationshipIds = [
				...(oldTable?.relationshipIds ?? []),
				...(newTable?.relationshipIds ?? []),
			];
			const childRelationshipIds = new Set(
				relationshipIds.filter(rId =>
					(this.oldSchemaState.relationships[rId]?.childTableId === tableId) ||
					(this.newState.schema.relationships[rId]?.childTableId === tableId))
			);
			
			// check if any of the parent tables have been recreated, which means this needs to be recreated as well
			// let shouldRecreate = false;
			// for (const rId of childRelationshipIds) {
			// 	const oldRel = this.oldSchemaState.relationships[rId];
			// 	const newRel = this.newState.schema.relationships[rId];
			
			// 	shouldRecreate = (oldRel && this.recreatedTableIds.has(oldRel.parentTableId)) || (newRel && this.recreatedTableIds.has(newRel.parentTableId));
			// }
			
			// if (shouldRecreate) {
			// 	tableChanges.push(this.recreateTable(oldTable, newTable));
			// 	continue;
			// }
			
			const oldTableIndexIds = oldTable
				? Object.values(this.oldSchemaState.indexes).filter(x => x.tableId === tableId).map(x => x.id)
				: [];
			const newTableIndexIds = oldTable
				? Object.values(this.newState.schema.indexes).filter(x => x.tableId === tableId).map(x => x.id)
				: [];
			const indexIds = new Set([...oldTableIndexIds, ...newTableIndexIds]);
			
			const oldTableUniqueIds = oldTable
				? Object.values(this.oldSchemaState.uniques).filter(x => x.tableId === tableId).map(x => x.id)
				: [];
			const newTableUniqueIds = oldTable
				? Object.values(this.newState.schema.uniques).filter(x => x.tableId === tableId).map(x => x.id)
				: [];
			const uniqueIds = new Set([...oldTableUniqueIds, ...newTableUniqueIds]);
			
			const oldTablePrimaryKeyIds = oldTable
				? Object.values(this.oldSchemaState.primaryKeys).filter(x => x.tableId === tableId).map(x => x.id)
				: [];
			const newTablePrimaryKeyIds = oldTable
				? Object.values(this.newState.schema.primaryKeys).filter(x => x.tableId === tableId).map(x => x.id)
				: [];
			const primarykeyIds = new Set([...oldTablePrimaryKeyIds, ...newTablePrimaryKeyIds]);
			
			const columnsSqlData = this.generateItemDifference(
				this.generateColumnDifference.bind(this),
				this.oldSchemaState.columns,
				this.newState.schema.columns,
				this.schemaDiff.columns,
				columnIds
			);
			
			const relationshipsSqlData = this.generateItemDifference(
				this.generateRelationshipDifference.bind(this),
				this.oldSchemaState.relationships,
				this.newState.schema.relationships,
				this.schemaDiff.relationships,
				childRelationshipIds
			);
			
			const indexesSqlData = this.generateItemDifference(
				this.generateIndexDifference.bind(this),
				this.oldSchemaState.indexes,
				this.newState.schema.indexes,
				this.schemaDiff.indexes,
				indexIds
			);
			
			const uniquesSqlData = this.generateItemDifference(
				this.generateUniqueDifference.bind(this),
				this.oldSchemaState.uniques,
				this.newState.schema.uniques,
				this.schemaDiff.uniques,
				uniqueIds
			);
			
			const primaryKeysSqlData = this.generateItemDifference(
				this.generatePrimaryKeyDifference.bind(this),
				this.oldSchemaState.primaryKeys,
				this.newState.schema.primaryKeys,
				this.schemaDiff.primaryKeys,
				primarykeyIds
			);
			
			const requiresNewTable = columnsSqlData.findIndex((x) => x.requiresNewTable) > -1
				|| relationshipsSqlData.findIndex((x) => x.requiresNewTable) > -1
				|| indexesSqlData.findIndex((x) => x.requiresNewTable) > -1
				|| uniquesSqlData.findIndex((x) => x.requiresNewTable) > -1
				|| primaryKeysSqlData.findIndex((x) => x.requiresNewTable) > -1;
			
			if (oldTable && newTable && requiresNewTable) {
				changes.push(this.recreateTable(oldTable, newTable));
			}
			else {
				const tableSqlData = this.generateTableDifference(this.schemaDiff.tables[tableId], oldTable, newTable);
				changes.push(tableSqlData);
				
				if (!tableSqlData.createdNewTable && newTable !== undefined) {
					columnsSqlData.filter(s => s.isCompleteSqlStatement && s.createdNewColumn).forEach(s => changes.push(s));
					primaryKeysSqlData.filter(s => s.isCompleteSqlStatement).forEach(s => changes.push(s));
					relationshipsSqlData.filter(s => s.isCompleteSqlStatement).forEach(s => changes.push(s));
					indexesSqlData.filter(s => s.isCompleteSqlStatement).forEach(s => changes.push(s));
					uniquesSqlData.filter(s => s.isCompleteSqlStatement).forEach(s => changes.push(s));
					columnsSqlData.filter(s => s.isCompleteSqlStatement && !s.createdNewColumn).forEach(s => changes.push(s));
				}
			}
		}
		// Add namespace deletions after everything else
		namespacesData.filter(s => s.isCompleteSqlStatement && !s.createdNewNamespace).forEach(s => changes.push(s));
		
		const sql = changes.map(s => s.text).join("\n\n").trim().replaceAll(/\n{3,}/g, "\n\n") + "\n";
		
		return {
			sql,
		};
	}
	
	generateItemDifference<D extends ColumnDiff | RelationshipDiff | IndexDiff | UniqueDiff | PrimaryKeyDiff | NamespaceDiff, I>(
		func: (diff: D, oldItem: I | undefined, newItem: I | undefined) => SqlData,
		oldContainer: {[id: string]: I},
		newContainer: {[id: string]: I},
		diffContainer: {[id: string]: D},
		ids: Set<string>
	): SqlData[]
	{
		const ret: SqlData[] = [];
		
		for (const id of ids) {
			const oldItem = oldContainer[id];
			const newItem = newContainer[id];
			const diff = diffContainer[id];
			
			if (!diff.isNew && !diff.isDeleted && !diff.propertyChanges.length) {
				// no changes, so skip
				continue;
			}
			
			ret.push(func(diff, oldItem, newItem));
		}
		
		return ret;
	}
	
	recreateTable(oldTable: TableData, newTable: TableData): SqlData {
		const state = this.newState;
		const sqlData: SqlData[] = [];
		
		const table = {...newTable};
		table.name += `_new_${Math.trunc(Date.now() / (60_000))}`; // new table name to avoid collisions
		const newNamespace = findTableNamespace(table, state.schema)?.name ?? '';
		const oldNamespace = findTableNamespace(oldTable, this.oldSchemaState)?.name ?? '';
		
		sqlData.push(this.createTable(table));
		sqlData.push({
			text: `--Put your migration insertion script here.\n\nDROP TABLE ${quoteObjectName(oldNamespace, oldTable.name, state)};`,
			isCompleteSqlStatement: true,
		});
		sqlData.push({
			text: `ALTER TABLE ${quoteObjectName(newNamespace, table.name, state)} RENAME TO ${quoteName(newTable.name, state)};`,
			isCompleteSqlStatement: true,
		});
		
		this.recreatedTableIds.add(newTable.id);
		
		return {
			text: sqlData.map(s => s.text).join('\n\n'),
			isCompleteSqlStatement: true,
			createdNewTable: true,
		};
	}
	
	createTable(table: TableData): SqlData {
		const state = this.newState;
		
		const columnsData = table.columnIds
			.map(columnId => getColumn(state, columnId))
			.map(col => this.createColumn(col, !(col.tableId in this.oldSchemaState.tables)));
		
		const indexesData = Object.values(state.schema.indexes)
			.filter(ix => ix.tableId === table.id)
			.map(ix => this.createIndex(ix));
		const innerIndexesData = indexesData.filter(sd => !sd.isCompleteSqlStatement);
		const outerIndexesData = indexesData.filter(sd => sd.isCompleteSqlStatement);
		
		const uniquesData = Object.values(state.schema.uniques)
			.filter(uq => uq.tableId === table.id)
			.map(uq => this.createUnique(uq, !(uq.tableId in this.oldSchemaState.tables)));
		const innerUniquesData = uniquesData.filter(sd => !sd.isCompleteSqlStatement);
		const outerUniquesData = uniquesData.filter(sd => sd.isCompleteSqlStatement);
		
		const primaryKeysData = Object.values(state.schema.primaryKeys)
			.filter(pk => pk.tableId === table.id)
			.map(pk => this.createPrimaryKey(pk, !(pk.tableId in this.oldSchemaState.tables)));
		const innerPrimaryKeysData = primaryKeysData.filter(sd => !sd.isCompleteSqlStatement);
		const outerPrimaryKeysData = primaryKeysData.filter(sd => sd.isCompleteSqlStatement);
		
		const relationshipsData = Object.values(state.schema.relationships)
			.filter(rel => rel.childTableId === table.id && !rel.isNotExported)
			.map(rel => this.createRelationship(rel, !(rel.childTableId in this.oldSchemaState.tables)));
		
		const innerText: string[] = [];
		
		columnsData.forEach(sd => innerText.push(sd.text));
		innerPrimaryKeysData.forEach(sd => innerText.push(sd.text));
		innerUniquesData.forEach(sd => innerText.push(sd.text));
		innerIndexesData.forEach(sd => innerText.push(sd.text));
		relationshipsData.forEach(sd => innerText.push(sd.text));
		
		const outerText: string[] = [];
		
		outerPrimaryKeysData.forEach(sd => outerText.push(sd.text));
		outerUniquesData.forEach(sd => outerText.push(sd.text));
		outerIndexesData.forEach(sd => outerText.push(sd.text));
		
		const namespace = findTableNamespace(table, state.schema)?.name ?? '';
		const text = `CREATE TABLE ${quoteObjectName(namespace, table.name, state)} (\n${innerText.join(',\n')}\n);\n${outerText.join('\n')}`;
		
		return {
			text,
			isCompleteSqlStatement: true,
			createdNewTable: true,
		};
	}
	
	deleteTable(table: TableData): SqlData {
		const state = this.newState;
		const namespace = findTableNamespace(table, this.oldSchemaState)?.name ?? '';
		
		return {
			text: `DROP TABLE ${quoteObjectName(namespace, table.name, state)};`,
			isCompleteSqlStatement: true,
		};
	}
	
	abstract generateNamespaceDifference(diff: NamespaceDiff, oldNamespace: NamespaceData | undefined, newNamespace: NamespaceData | undefined): SqlData;
	abstract createNamespace(namespace: NamespaceData): SqlData;
	abstract deleteNamespace(namespace: NamespaceData): SqlData;
	abstract generateTableDifference(diff: TableDiff, oldTable: TableData | undefined, newTable: TableData | undefined): SqlData;
	abstract generateColumnDifference(diff: ColumnDiff, oldColumn: ColumnData | undefined, newColumn: ColumnData | undefined): SqlData;
	abstract generateRelationshipDifference(diff: RelationshipDiff, oldRelationship: RelationshipData | undefined, newRelationship: RelationshipData | undefined): SqlData;
	abstract generateIndexDifference(diff: IndexDiff, oldIndex: IndexData | undefined, newIndex: IndexData | undefined): SqlData;
	abstract generateUniqueDifference(diff: UniqueDiff, oldUnique: UniqueData | undefined, newUnique: UniqueData | undefined): SqlData;
	abstract generatePrimaryKeyDifference(diff: PrimaryKeyDiff, oldPrimaryKey: PrimaryKeyData | undefined, newPrimaryKey: PrimaryKeyData | undefined): SqlData;
	abstract createColumn(column: ColumnData, forNewTable?: boolean): SqlData;
	abstract deleteColumn(column: ColumnData): SqlData;
	abstract createRelationship(relationship: RelationshipData, forNewTable?: boolean): SqlData;
	abstract deleteRelationship(relationship: RelationshipData): SqlData;
	abstract createIndex(index: IndexData): SqlData;
	abstract deleteIndex(index: IndexData): SqlData;
	abstract createUnique(unique: UniqueData, forNewTable?: boolean): SqlData;
	abstract deleteUnique(unique: UniqueData): SqlData;
	abstract createPrimaryKey(primaryKey: PrimaryKeyData, forNewTable?: boolean): SqlData;
	abstract deletePrimaryKey(primaryKey: PrimaryKeyData): SqlData;
	
	isTableNew(tableId: string) {
		return this.schemaDiff.tables[tableId].isNew;
	}
}

export interface SqlData {
	text: string;
	isCompleteSqlStatement?: boolean;
	requiresNewTable?: boolean;
	createdNewTable?: boolean;
	createdNewColumn?: boolean;
	createdNewNamespace?: boolean;
}

function setAllTableDiff(schemaState: SimpleSchemaState, tableId: string, schemaDiff: SchemaDiff, isNew: boolean, isDeleted: boolean) {
	const tableDiff = schemaDiff.tables[tableId];
	tableDiff.isNew = isNew;
	tableDiff.isDeleted = isDeleted;
	
	for (const columnDiff of Object.values(schemaDiff.columns)) {
		const column = schemaState.columns[columnDiff.id];
		if (column?.tableId !== tableId) {
			continue;
		}
		
		columnDiff.isNew = isNew;
		columnDiff.isDeleted = isDeleted;
	}
	
	for (const relationshipDiff of Object.values(schemaDiff.relationships)) {
		const relationship = schemaState.relationships[relationshipDiff.id];
		if (relationship?.childTableId !== tableId && relationship?.parentTableId !== tableId) {
			continue;
		}
		
		relationshipDiff.isNew = isNew;
		relationshipDiff.isDeleted = isDeleted;
	}
	
	for (const primaryKeyDiff of Object.values(schemaDiff.primaryKeys)) {
		const primaryKey = schemaState.primaryKeys[primaryKeyDiff.id];
		if (primaryKey?.tableId !== tableId) {
			continue;
		}
		
		primaryKeyDiff.isNew = isNew;
		primaryKeyDiff.isDeleted = isDeleted;
	}
	
	for (const indexDiff of Object.values(schemaDiff.indexes)) {
		const index = schemaState.indexes[indexDiff.id];
		if (index?.tableId !== tableId) {
			continue;
		}
		
		indexDiff.isNew = isNew;
		indexDiff.isDeleted = isDeleted;
	}
	
	for (const uniqueDiff of Object.values(schemaDiff.uniques)) {
		const unique = schemaState.uniques[uniqueDiff.id];
		if (unique?.tableId !== tableId) {
			continue;
		}
		
		uniqueDiff.isNew = isNew;
		uniqueDiff.isDeleted = isDeleted;
	}
}
