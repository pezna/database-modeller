import { getColumns, getTables } from "@/selectors";
import { useSelector } from "@/store";
import { useEffect, useState } from "react";

type SchemaDefinition = {
	[table: string]: string[];
}

export function useCodeMirrorSqlSchema() {
	const tables = useSelector(getTables);
	const columns = useSelector(getColumns);
	const [schema, setSchema] = useState<SchemaDefinition>({});
	
	useEffect(() => {
		const merged: SchemaDefinition = {};
		Object.values(tables).forEach(t => {
			merged[t.name] = t.columnIds.map(cId => columns[cId].name);
		})
		setSchema(merged);
	}, [tables, columns]);
	
	return schema;
}
