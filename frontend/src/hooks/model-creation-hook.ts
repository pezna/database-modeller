import { createColumn, createComponent, createConnector, createIndex, createMachine, createNamespace, createNote, createNotification, createPrimaryKey, createRelationship, createTable, createUnique, selectColumn, selectComponent, selectConnector, selectMachine, selectNamespace, selectNote, selectRelationship, selectTable } from "@/actions";
import { AppMode, ComponentData, ConnectorData, MessageAlertType, defaultComponentSize, newBucketData, newChannelConnectorData, newColumnData, newIndexData, newMachineData, newNamespaceData, newNoteData, newPrimaryKey, newProducerData, newRelationshipData, newSignalConnectorData, newTableData, newUniqueData } from "@/data";
import { newLSystemData } from "@/data/machine/lsystem-data";
import { diagramService } from "@/services";
import { Dispatch, useDispatch } from "@/store";
import { useMemo } from "react";

type FunctionParamsAndReturnType<T> = T extends (...args: infer A) => infer R ? { args: A, ret: R } : never;
type Tail<Arr> = Arr extends [any, ...infer Rest] ? Rest : never;
type FunctionWithoutFirstParam<F> = (...args: Tail<FunctionParamsAndReturnType<F>["args"]>) => FunctionParamsAndReturnType<F>["ret"];

interface CreateFunctions {
	createTable: FunctionWithoutFirstParam<typeof handleCreateTable>,
	createColumn: FunctionWithoutFirstParam<typeof handleCreateColumn>,
	createRelationship: FunctionWithoutFirstParam<typeof handleCreateRelationship>,
	createIndex: FunctionWithoutFirstParam<typeof handleCreateIndex>,
	createUnique: FunctionWithoutFirstParam<typeof handleCreateUnique>,
	createPrimaryKey: FunctionWithoutFirstParam<typeof handleCreatePrimaryKey>,
	createNamespace: FunctionWithoutFirstParam<typeof handleCreateNamespace>,
	createNote: FunctionWithoutFirstParam<typeof handleCreateNote>,
	createMachine: FunctionWithoutFirstParam<typeof handleCreateMachine>,
	createProducer: FunctionWithoutFirstParam<typeof handleCreateProducer>,
	createBucket: FunctionWithoutFirstParam<typeof handleCreateBucket>,
	createChannel: FunctionWithoutFirstParam<typeof handleCreateChannel>,
	createSignal: FunctionWithoutFirstParam<typeof handleCreateSignal>,
	createLSystem: FunctionWithoutFirstParam<typeof handleCreateLSystem>,
}

export function useModelCreation() {
	const dispatch = useDispatch();
	
	const createObject = useMemo<CreateFunctions>(() => ({
		createTable: (...args) => handleCreateTable(dispatch, ...args),
		createColumn: (...args) => handleCreateColumn(dispatch, ...args),
		createRelationship: (...args) => handleCreateRelationship(dispatch, ...args),
		createIndex: (...args) => handleCreateIndex(dispatch, ...args),
		createUnique: (...args) => handleCreateUnique(dispatch, ...args),
		createPrimaryKey: (...args) => handleCreatePrimaryKey(dispatch, ...args),
		createNamespace: (...args) => handleCreateNamespace(dispatch, ...args),
		createNote: (...args) => handleCreateNote(dispatch, ...args),
		createMachine: (...args) => handleCreateMachine(dispatch, ...args),
		createProducer: (...args) => handleCreateProducer(dispatch, ...args),
		createBucket: (...args) => handleCreateBucket(dispatch, ...args),
		createChannel: (...args) => handleCreateChannel(dispatch, ...args),
		createSignal: (...args) => handleCreateSignal(dispatch, ...args),
		createLSystem: (...args) => handleCreateLSystem(dispatch, ...args),
	}), [dispatch]);
	
	return createObject;
}

function handleCreateTable(dispatch: Dispatch, namespaceId: string, x: number, y: number, errorKey?: string) {
	const tb = newTableData(namespaceId);
	const res = dispatch(createTable(tb, errorKey));
	if (!res.errors) {
		diagramService.sendItemDimensions(tb.id, namespaceId, { x, y, width: 0, height: 0});
		dispatch(selectTable(tb.id));
	}
	
	return res;
}

function handleCreateColumn(dispatch: Dispatch, tableId: string, errorKey?: string) {
	const col = newColumnData(tableId);
	const res = dispatch(createColumn(col, errorKey));
	if (!res.errors) {
		// the column will send it's dimensions to diagramService when it's rendered
		dispatch(selectColumn(col.id));
	}
	
	return res;
}

function handleCreateRelationship(dispatch: Dispatch, childTableId: string, childColumnIds: string[], parentTableId: string, parentColumnIds: string[], errorKey?: string) {
	const rel = newRelationshipData(childTableId, childColumnIds, parentTableId, parentColumnIds);
	const res = dispatch(createRelationship(rel, errorKey));
	if (res.errors) {
		dispatch(createNotification(MessageAlertType.error, "Could not create relationship", res.errors[0].message));
	}
	else {
		dispatch(selectRelationship(rel.id));
	}
	
	return res;
}

function handleCreateIndex(dispatch: Dispatch, tableId: string, columnId?: string, errorKey?: string) {
	const ix = newIndexData(tableId, columnId);
	return dispatch(createIndex(ix, errorKey));
}

function handleCreateUnique(dispatch: Dispatch, tableId: string, columnId?: string, errorKey?: string) {
	const uq = newUniqueData(tableId, columnId);
	return dispatch(createUnique(uq, errorKey));
}

function handleCreatePrimaryKey(dispatch: Dispatch, tableId: string, columnId?: string, errorKey?: string) {
	const pk = newPrimaryKey(tableId, columnId);
	return dispatch(createPrimaryKey(pk, errorKey));
}

function handleCreateNamespace(dispatch: Dispatch, name: string | null, errorKey?: string) {
	const ns = newNamespaceData(name);
	const res = dispatch(createNamespace(ns, errorKey));
	if (!res.errors) {
		diagramService.addBoard(ns.id);
		dispatch(selectNamespace(ns.id));
	}
	
	return res;
}

function handleCreateNote(dispatch: Dispatch, appMode: AppMode, sectionId: string, x: number, y: number) {
	const note = newNoteData(appMode, sectionId);
	const res = dispatch(createNote(note));
	diagramService.sendItemDimensions(note.id, sectionId, { x, y, width: 0, height: 0});
	dispatch(selectNote(note.id));
	return res;
}

function handleCreateMachine(dispatch: Dispatch, errorKey?: string) {
	const m = newMachineData();
	const res = dispatch(createMachine(m, errorKey));
	if (!res.errors) {
		diagramService.addBoard(m.id);
		dispatch(selectMachine(m.id));
	}
	
	return res;
}

function handleCreateComponent(dispatch: Dispatch, component: ComponentData, x: number, y: number, errorKey?: string) {
	const res = dispatch(createComponent(component, errorKey));
	if (!res.errors) {
		diagramService.sendItemDimensions(component.id, component.machineId, { x, y, ...defaultComponentSize });
		dispatch(selectComponent(component.id));
	}
	
	return res;
}

function handleCreateProducer(dispatch: Dispatch, machineId: string, x: number, y: number, errorKey?: string) {
	return handleCreateComponent(dispatch, newProducerData(machineId), x, y, errorKey);
}

function handleCreateBucket(dispatch: Dispatch, machineId: string, x: number, y: number, errorKey?: string) {
	return handleCreateComponent(dispatch, newBucketData(machineId), x, y, errorKey);
}

function handleCreateLSystem(dispatch: Dispatch, machineId: string, x: number, y: number, errorKey?: string) {
	return handleCreateComponent(dispatch, newLSystemData(machineId), x, y, errorKey);
}

function handleCreateConnector(dispatch: Dispatch, connector: ConnectorData, errorKey?: string) {
	const res = dispatch(createConnector(connector, errorKey));
	if (!res.errors) {
		dispatch(selectConnector(connector.id));
	}
	
	return res;
}

function handleCreateChannel(dispatch: Dispatch, machineId: string, sourceId: string, destId: string, errorKey?: string) {
	const conn = newChannelConnectorData(machineId, sourceId, destId);
	return handleCreateConnector(dispatch, conn, errorKey);
}

function handleCreateSignal(dispatch: Dispatch, machineId: string, sourceId: string, destId: string, errorKey?: string) {
	const conn = newSignalConnectorData(machineId, sourceId, destId);
	return handleCreateConnector(dispatch, conn, errorKey);
}
