import { alterUiStateProperty, deselectBoard, selectColumn, selectComponent, selectConnector, selectMachine, selectNamespace, selectRelationship, selectTable } from "@/actions";
import { ColumnSidebarUiState, ManageSchemaTab, ModelObjectType, OptionsSidebarSection, PropertySidebarSection, RootState, TableSidebarUiState } from "@/data";
import { getColumn, getColumnType, getComponent, getConnector, getIndex, getMachine, getNamespace, getPrimaryKey, getRelationship, getTable, getUnique } from "@/selectors";
import { store, useDispatch } from "@/store";
import { useMemo } from "react";
import { Dispatch } from "redux";

interface NavigationOptions {
	keepOtherGroupsOpen?: boolean;
	columnMixinParentId?: string;
}

export function useModelNavigation() {
	const dispatch = useDispatch();
	
	const select = useMemo(() => {
		const obj = {
			deselect: (everything?: boolean) => dispatch(deselectBoard(everything)),
			selectTable: (id: string) => handleTable(dispatch, store.getState(), id),
			selectColumn: (id: string) => handleColumn(dispatch, store.getState(), id),
			selectColumnType: (id: string) => handleColumnType(dispatch, store.getState(), id),
			selectRelationship: (id: string) => handleRelationship(dispatch, store.getState(), id),
			selectIndex: (id: string, options?: NavigationOptions) => handleIndex(dispatch, store.getState(), id, options),
			selectUnique: (id: string, options?: NavigationOptions) => handleUnique(dispatch, store.getState(), id, options),
			selectPrimaryKey: (id: string, options?: NavigationOptions) => handlePrimaryKey(dispatch, store.getState(), id, options),
			selectNamespace: (id: string) => handleNamespace(dispatch, store.getState(), id),
			selectMachine: (id: string) => handleMachine(dispatch, store.getState(), id),
			selectComponent: (id: string) => handleComponent(dispatch, store.getState(), id),
			selectConnector: (id: string) => handleConnector(dispatch, store.getState(), id),
			select: (type: ModelObjectType, id: string, options?: NavigationOptions) => {
				switch (type) {
					case ModelObjectType.table:
						obj.selectTable(id);
						break;
					case ModelObjectType.column:
						obj.selectColumn(id);
						break;
					case ModelObjectType.columnType:
						obj.selectColumnType(id);
						break;
					case ModelObjectType.relationship:
						obj.selectRelationship(id);
						break;
					case ModelObjectType.index:
						obj.selectIndex(id, options);
						break;
					case ModelObjectType.unique:
						obj.selectUnique(id, options);
						break;
					case ModelObjectType.primaryKey:
						obj.selectPrimaryKey(id, options);
						break;
					case ModelObjectType.namespace:
						obj.selectNamespace(id);
						break;
					case ModelObjectType.machine:
						obj.selectMachine(id);
						break;
					case ModelObjectType.component:
						obj.selectComponent(id);
						break;
					case ModelObjectType.connector:
						obj.selectConnector(id);
						break;
					default:
						throw new Error(`${type} navigation is not implemented`);
				}
			},
		};
		
		return obj;
	}, [dispatch]);
	
	return select;
}

function handleTable(dispatch: Dispatch<any>, state: RootState, tableId: string) {
	const table = getTable(state, tableId);
	if (table) {
		dispatch(selectTable(tableId));
	}
}

function handleColumn(dispatch: Dispatch<any>, state: RootState, columnId: string) {
	const col = getColumn(state, columnId);
	if (col) {
		dispatch(selectColumn(columnId));
	}
}

function handleColumnType(dispatch: Dispatch<any>, state: RootState, columnTypeId: string) {
	const type = getColumnType(state, columnTypeId);
	if (type) {
		dispatch(alterUiStateProperty(['generalAppUi', 'optionsSidebar', 'visibleSection'], OptionsSidebarSection.manage));
		dispatch(alterUiStateProperty(['manageSchemaSidebar', 'activeTab'], ManageSchemaTab.columnTypes));
		dispatch(alterUiStateProperty(['manageSchemaSidebar', 'openColumnTypeIds'], [columnTypeId]));
	}
}

function handleRelationship(dispatch: Dispatch<any>, state: RootState, relationshipId: string) {
	const relationship = getRelationship(state, relationshipId);
	if (relationship) {
		dispatch(selectRelationship(relationshipId));
	}
}

function handleIndex(dispatch: Dispatch<any>, state: RootState, indexId: string, options?: NavigationOptions) {
	const index = getIndex(state, indexId);
	if (index) {
		dispatch(alterUiStateProperty(['generalAppUi', 'propertySidebar', 'visibleSection'], PropertySidebarSection.details));
		
		if (options && options.columnMixinParentId) {
			dispatch(selectColumn(options.columnMixinParentId));
			if (!options.keepOtherGroupsOpen) {
				closeOtherColumnGroups(dispatch);
			}
			dispatch(alterUiStateProperty(['columnSidebar', 'isIndexGroupExpanded'], true));
		}
		else {
			const tableId = index.tableId;
			dispatch(selectTable(tableId));
			if (!(options && options.keepOtherGroupsOpen)) {
				closeOtherTableGroups(dispatch);
			}
			dispatch(alterUiStateProperty(['tableSidebar', 'isIndexGroupExpanded'], true));
		}
	}
}

function handleUnique(dispatch: Dispatch<any>, state: RootState, uniqueId: string, options?: NavigationOptions) {
	const unique = getUnique(state, uniqueId);
	if (unique) {
		dispatch(alterUiStateProperty(['generalAppUi', 'propertySidebar', 'visibleSection'], PropertySidebarSection.details));
		
		if (options && options.columnMixinParentId) {
			dispatch(selectColumn(options.columnMixinParentId));
			if (!options.keepOtherGroupsOpen) {
				closeOtherColumnGroups(dispatch);
			}
			dispatch(alterUiStateProperty(['columnSidebar', 'isUniqueGroupExpanded'], true));
		}
		else {
			const tableId = unique.tableId;
			dispatch(selectTable(tableId));
			if (!(options && options.keepOtherGroupsOpen)) {
				closeOtherTableGroups(dispatch);
			}
			dispatch(alterUiStateProperty(['tableSidebar', 'isUniqueGroupExpanded'], true));
		}
	}
}

function handlePrimaryKey(dispatch: Dispatch<any>, state: RootState, primaryKeyId: string, options?: NavigationOptions) {
	const primaryKey = getPrimaryKey(state, primaryKeyId);
	if (primaryKey) {
		dispatch(alterUiStateProperty(['generalAppUi', 'propertySidebar', 'visibleSection'], PropertySidebarSection.details));
		
		const tableId = primaryKey.tableId;
		dispatch(selectTable(tableId));
		if (!(options && options.keepOtherGroupsOpen)) {
			closeOtherTableGroups(dispatch);
		}
		dispatch(alterUiStateProperty(['tableSidebar', 'isPrimaryKeyExpanded'], true));
	}
}

function handleNamespace(dispatch: Dispatch<any>, state: RootState, namespaceId: string) {
	const namespace = getNamespace(state, namespaceId);
	if (namespace) {
		dispatch(selectNamespace(namespaceId));
	}
}

function handleMachine(dispatch: Dispatch<any>, state: RootState, machineId: string) {
	const machine = getMachine(state, machineId);
	if (machine) {
		dispatch(selectMachine(machineId));
	}
}

function handleComponent(dispatch: Dispatch<any>, state: RootState, componentId: string) {
	const component = getComponent(state, componentId);
	if (component) {
		dispatch(selectComponent(componentId));
	}
}

function handleConnector(dispatch: Dispatch<any>, state: RootState, connectorId: string) {
	const connector = getConnector(state, connectorId);
	if (connector) {
		dispatch(selectConnector(connectorId));
	}
}

/**
 * Used to automatically close accordion components when only one group should be selected.
 * It's added here to ensure that this file is updated when there is a change to the accordion groups.
 */
type TableSidebarGroupKeys = Exclude<keyof TableSidebarUiState, "scrollAmount">
const tableSidebarGroupKeys: {[K in TableSidebarGroupKeys]: null} = {
	isColumnGroupExpanded: null,
	isPrimaryKeyExpanded: null,
	isForeignKeyGroupExpanded: null,
	isIndexGroupExpanded: null,
	isUniqueGroupExpanded: null,
	isMixinGroupExpanded: null,
	isMixinGroupUsagesExpanded: null,
};

type ColumnSidebarGroupKeys = Exclude<keyof ColumnSidebarUiState, "scrollAmount">
const columnSidebarGroupKeys: {[K in ColumnSidebarGroupKeys]: null} = {
	isForeignKeyGroupExpanded: null,
	isIndexGroupExpanded: null,
	isUniqueGroupExpanded: null,
};

function closeOtherTableGroups(dispatch: Dispatch<any>) {
	Object.keys(tableSidebarGroupKeys).forEach(key => {
		dispatch(alterUiStateProperty(["tableSidebar", key as any], false))
	});
}

function closeOtherColumnGroups(dispatch: Dispatch<any>) {
	Object.keys(columnSidebarGroupKeys).forEach(key => {
		dispatch(alterUiStateProperty(["columnSidebar", key as any], false));
	});
}
