import { combineReducers } from 'redux';
import boardReducer from './board-reducer';
import codeReducer from './code-reducer';
import errorReducer from './error-reducer';
import exportReducer from './export-reducer';
import generalReducer from './general-reducer';
import machineReducer from './machine-reducer';
import schemaReducer from './schema-reducer';
import searchReducer from './search-reducer';
import settingReducer from './setting-reducer';
import uiReducer from './ui-reducer';

const reducers = combineReducers({
	board: boardReducer,
	schema: schemaReducer,
	errors: errorReducer,
	settings: settingReducer,
	ui: uiReducer,
	search: searchReducer,
	code: codeReducer,
	export: exportReducer,
	general: generalReducer,
	machine: machineReducer,
});

export default reducers;
