import { ActionData, ActionType, AlterSettingsPropertyActionData, SetSettingsActionData, SettingsState, getDefaultSettingsState } from "@/data";

export const initialSettingsState: SettingsState = getDefaultSettingsState();

export default function settingReducer(settingState = initialSettingsState, action: ActionData): SettingsState {
	switch (action.type) {
		case ActionType.alterSettingsProperty:
			return handleAlterSettingsProperty(settingState, action as AlterSettingsPropertyActionData);
		case ActionType.setSettings:
			return handleSetSettings(action as SetSettingsActionData);
		default:
			return settingState;
	}
}

function handleAlterSettingsProperty(settingState: SettingsState, action: AlterSettingsPropertyActionData): SettingsState {
	const { path, value } = action.payload;
	
	const settings = {...settingState};
	let container: any = settings;
	for (let index = 0; index < path.length - 1; index++) {
		const key = path[index] as any;
		container[key] = {
			...container[key],
		};
		container = container[key];
	}
	
	container[path[path.length - 1] as any] = value;
	
	return settings;
}

function handleSetSettings(action: SetSettingsActionData): SettingsState {
	const { settings } = action.payload;
	
	return {
		...settings,
	};
}
