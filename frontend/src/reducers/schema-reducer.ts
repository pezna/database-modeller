import {
	ActionData, ActionType, AlterColumnOptionPropertyActionData, AlterColumnPropertyActionData, AlterIndexPropertyActionData,
	AlterNamespacePropertyActionData, AlterPrimaryKeyPropertyActionData, AlterRelationshipPropertyActionData,
	AlterSchemaSettingsPropertyActionData, AlterTablePropertyActionData, AlterUniquePropertyActionData, ColumnOptionType,
	CreateColumnActionData, CreateIndexActionData, CreateNamespaceActionData, CreatePrimaryKeyActionData, CreateRelationshipActionData,
	CreateTableActionData, CreateUniqueActionData, DeleteColumnActionData, DeleteColumnTypeActionData, DeleteIndexActionData,
	DeleteNamespaceActionData, DeletePrimaryKeyActionData, DeleteRelationshipActionData, DeleteTableActionData, DeleteUniqueActionData,
	ReplaceColumnTypeUsageActionData,
	SchemaState, SetColumnTypeActionData, SetSchemaSettingsActionData, SetSchemaStateActionData, TableData, defaultSchemaState,
} from "@/data";
import { IdPrefix, generateId } from "@/utils";
import { cloneDeep } from "lodash";
import { shallowEqual } from "react-redux";

export const initialSchemaState: SchemaState = defaultSchemaState();

export default function schemaReducer(schemaState = initialSchemaState, action: ActionData): SchemaState {
	switch (action.type) {
		case ActionType.setSchemaState:
			return handleSetSchemaState(schemaState, action as SetSchemaStateActionData);
		case ActionType.createTable:
			return handleCreateTable(schemaState, action as CreateTableActionData);
		case ActionType.deleteTable:
			return handleDeleteTable(schemaState, action as DeleteTableActionData);
		case ActionType.alterTableProperty:
			return handleAlterTableProperty(schemaState, action as AlterTablePropertyActionData);
		case ActionType.createColumn:
			return handleCreateColumn(schemaState, action as CreateColumnActionData);
		case ActionType.deleteColumn:
			return handleDeleteColumn(schemaState, action as DeleteColumnActionData);
		case ActionType.alterColumnProperty:
			return handleAlterColumnProperty(schemaState, action as AlterColumnPropertyActionData);
		case ActionType.alterColumnOptionProperty:
			return handleAlterColumnOptionProperty(schemaState, action as AlterColumnOptionPropertyActionData<ColumnOptionType>);
		case ActionType.setColumnType:
			return handleSetColumnType(schemaState, action as SetColumnTypeActionData);
		case ActionType.deleteColumnType:
			return handleDeleteColumnType(schemaState, action as DeleteColumnTypeActionData);
		case ActionType.replaceColumnTypeUsage:
			return handleReplaceColumnTypeUsage(schemaState, action as ReplaceColumnTypeUsageActionData);
		case ActionType.createRelationship:
			return handleCreateRelationship(schemaState, action as CreateRelationshipActionData);
		case ActionType.alterRelationshipProperty:
			return handleAlterRelationshipProperty(schemaState, action as AlterRelationshipPropertyActionData);
		case ActionType.deleteRelationship:
			return handleDeleteRelationship(schemaState, action as DeleteRelationshipActionData);
		case ActionType.createIndex:
			return handleCreateIndex(schemaState, action as CreateIndexActionData);
		case ActionType.alterIndexProperty:
			return handleAlterIndexProperty(schemaState, action as AlterIndexPropertyActionData);
		case ActionType.deleteIndex:
			return handleDeleteIndex(schemaState, action as DeleteIndexActionData);
		case ActionType.createUnique:
			return handleCreateUnique(schemaState, action as CreateUniqueActionData);
		case ActionType.alterUniqueProperty:
			return handleAlterUniqueProperty(schemaState, action as AlterUniquePropertyActionData);
		case ActionType.deleteUnique:
			return handleDeleteUnique(schemaState, action as DeleteUniqueActionData);
		case ActionType.createPrimaryKey:
			return handleCreatePrimaryKey(schemaState, action as CreatePrimaryKeyActionData);
		case ActionType.alterPrimaryKeyProperty:
			return handleAlterPrimaryKeyProperty(schemaState, action as AlterPrimaryKeyPropertyActionData);
		case ActionType.deletePrimaryKey:
			return handleDeletePrimaryKey(schemaState, action as DeletePrimaryKeyActionData);
		case ActionType.setSchemaSettings:
			return handleSetSchemaSettings(schemaState, action as SetSchemaSettingsActionData);
		case ActionType.alterSchemaSettingsProperty:
			return handleAlterSchemaSettingsProperty(schemaState, action as AlterSchemaSettingsPropertyActionData);
		case ActionType.createNamespace:
			return handleCreateNamespace(schemaState, action as CreateNamespaceActionData);
		case ActionType.alterNamespaceProperty:
			return handleAlterNamespaceProperty(schemaState, action as AlterNamespacePropertyActionData);
		case ActionType.deleteNamespace:
			return handleDeleteNamespace(schemaState, action as DeleteNamespaceActionData);
		default:
			return schemaState;
	}
}

function handleSetSchemaState(schemaState: SchemaState, action: SetSchemaStateActionData): SchemaState {
	const { schema } = action.payload;
	
	return {
		...schemaState,
		tables: schema.tables,
		columns: schema.columns,
		columnTypes: schema.columnTypes,
		relationships: schema.relationships,
		indexes: schema.indexes,
		uniques: schema.uniques,
		primaryKeys: schema.primaryKeys,
		namespaces: schema.namespaces,
	};
}

function handleCreateTable(schemaState: SchemaState, action: CreateTableActionData): SchemaState {
	const { table } = action.payload;
	table.id = table.id || generateId(IdPrefix.table);
	
	return {
		...schemaState,
		tables: {
			...schemaState.tables,
			[table.id]: table,
		},
	};
}

function handleDeleteTable(schemaState: SchemaState, action: DeleteTableActionData): SchemaState {
	const { id } = action.payload;
	const tables = {...schemaState.tables}; // copy existing tables
	delete tables[id]; // delete table from new object
	
	return {
		...schemaState,
		tables,
	};
}

function handleAlterTableProperty(schemaState: SchemaState, action: AlterTablePropertyActionData): SchemaState {
	const { tableId, propertyName, value } = action.payload;
	
	return {
		...schemaState,
		tables: {
			...schemaState.tables,
			[tableId]: {
				...schemaState.tables[tableId],
				[propertyName]: value,
			},
		},
	};
}

function handleCreateColumn(schemaState: SchemaState, action: CreateColumnActionData): SchemaState {
	const { column } = action.payload;
	const table = schemaState.tables[column.tableId];
	column.id = column.id || generateId(IdPrefix.column);
	let columnTypes = schemaState.columnTypes;
	const type = columnTypes[column.typeId];
	if (type) {
		columnTypes = {
			...schemaState.columnTypes,
			[type.id]: {
				...type,
				columnIds: [column.id, ...type.columnIds],
			},
		};
	}
	
	return {
		...schemaState,
		tables: {
			...schemaState.tables,
			[table.id]: {
				...table,
				columnIds: [
					...table.columnIds,
					column.id,
				],
			},
		},
		columns: {
			...schemaState.columns,
			[column.id]: column,
		},
		columnTypes,
	};
}

function handleDeleteColumn(schemaState: SchemaState, action: DeleteColumnActionData): SchemaState {
	const { columnId } = action.payload;
	const column = schemaState.columns[columnId];
	const table = schemaState.tables[column.tableId];
	let columnTypes = schemaState.columnTypes;
	const type = columnTypes[column.typeId];
	if (type) {
		const typeColumnIds = [...type.columnIds];
		typeColumnIds.splice(typeColumnIds.indexOf(columnId), 1);
		
		columnTypes = {
			...schemaState.columnTypes,
			[type.id]: {
				...type,
				columnIds: typeColumnIds,
			},
		};
	}
	
	const columns = {...schemaState.columns}; // copy existing columns
	const tableColumnIds = [...table.columnIds]; // copy existing column order
	tableColumnIds.splice(tableColumnIds.indexOf(columnId), 1); // delete column from order
	
	delete columns[columnId]; // delete column from new object
	
	return {
		...schemaState,
		tables: {
			...schemaState.tables,
			[table.id]: {
				...table,
				columnIds: tableColumnIds,
			},
		},
		columns,
		columnTypes,
	};
}

function handleAlterColumnProperty(schemaState: SchemaState, action: AlterColumnPropertyActionData): SchemaState {
	const { columnId, propertyName, value } = action.payload;
	
	const column = schemaState.columns[columnId];
	let columnTypes = schemaState.columnTypes;
	
	if (propertyName === "typeId") {
		const oldType = columnTypes[column.typeId];
		if (oldType) {
			const typeColumnIds = [...oldType.columnIds];
			typeColumnIds.splice(typeColumnIds.indexOf(columnId), 1);
			
			columnTypes = {
				...schemaState.columnTypes,
				[oldType.id]: {
					...oldType,
					columnIds: typeColumnIds,
				},
			};
		}
		const newType = columnTypes[value];
		if (newType) {
			columnTypes = {
				...schemaState.columnTypes,
				[newType.id]: {
					...newType,
					columnIds: [columnId, ...newType.columnIds],
				},
			};
		}
	}
	
	return {
		...schemaState,
		columns: {
			...schemaState.columns,
			[columnId]: {
				...schemaState.columns[columnId],
				[propertyName]: value,
			},
		},
		columnTypes,
	};
}

function handleAlterColumnOptionProperty<T extends ColumnOptionType>(schemaState: SchemaState, action: AlterColumnOptionPropertyActionData<T>): SchemaState {
	const { columnId, propertyName, value } = action.payload;
	const column = schemaState.columns[columnId];
	const existingOptions = column.options;
	
	return {
		...schemaState,
		columns: {
			...schemaState.columns,
			[columnId]: {
				...column,
				options: {
					...existingOptions,
					[propertyName]: value,
				},
			},
		},
	};
}

function handleSetColumnType(schemaState: SchemaState, action: SetColumnTypeActionData): SchemaState {
	const { columnType } = action.payload;
	
	return {
		...schemaState,
		columnTypes: {
			...schemaState.columnTypes,
			[columnType.id]: columnType,
		},
	};
}

function handleDeleteColumnType(schemaState: SchemaState, action: DeleteColumnTypeActionData): SchemaState {
	const { columnTypeId } = action.payload;
	const columnTypes = {...schemaState.columnTypes};
	delete columnTypes[columnTypeId];
	
	return {
		...schemaState,
		columnTypes,
	};
}

function handleReplaceColumnTypeUsage(schemaState: SchemaState, action: ReplaceColumnTypeUsageActionData): SchemaState {
	const { columnTypeId, replacementColumnTypeId } = action.payload;
	const columns = { ...schemaState.columns };
	
	const oldColumnType = schemaState.columnTypes[columnTypeId];
	const newColumnType = schemaState.columnTypes[replacementColumnTypeId];
	
	oldColumnType.columnIds.forEach(colId => {
		columns[colId] = {
			...columns[colId],
			typeId: replacementColumnTypeId,
		};
	});
	
	
	return {
		...schemaState,
		columns,
		columnTypes: {
			...schemaState.columnTypes,
			[oldColumnType.id]: {
				...oldColumnType,
				columnIds: [],
			},
			[newColumnType.id]: {
				...newColumnType,
				columnIds: [...newColumnType.columnIds, ...oldColumnType.columnIds],
			},
		},
	};
}

function handleCreateRelationship(schemaState: SchemaState, action: CreateRelationshipActionData): SchemaState {
	const { relationship } = action.payload;
	relationship.id = relationship.id || generateId(IdPrefix.relationship);
	const childTable = schemaState.tables[relationship.childTableId];
	const parentTable = schemaState.tables[relationship.parentTableId];
	
	const newState: SchemaState = {
		...schemaState,
		tables: {
			...schemaState.tables,
		},
		relationships: {
			...schemaState.relationships,
			[relationship.id]: relationship,
		},
	};
	
	newState.tables[relationship.childTableId] = {
		...childTable,
		relationshipIds: [...childTable.relationshipIds, relationship.id],
	};
	
	if (relationship.childTableId !== relationship.parentTableId) {
		newState.tables[relationship.parentTableId] = {
			...parentTable,
			relationshipIds: [...parentTable.relationshipIds, relationship.id],
		};
	}
	
	return newState;
}

function handleAlterRelationshipProperty(schemaState: SchemaState, action: AlterRelationshipPropertyActionData): SchemaState {
	const { id, propertyName, value } = action.payload;
	
	// deep copy so that the inner objects are new and will trigger an update
	const newState: SchemaState = {
		...schemaState,
		relationships: {
			...schemaState.relationships,
		},
	};
	
	const oldRelationship = schemaState.relationships[id];
	const relationship = {...oldRelationship};
	(relationship[propertyName] as any) = value;
	
	if (oldRelationship.childTableId !== relationship.childTableId && oldRelationship.childTableId !== relationship.parentTableId)
	{
		// former child table should no longer have this relationship
		const oldTable = newState.tables[oldRelationship.childTableId];
		const relationshipIds = [...oldTable.relationshipIds];
		relationshipIds.splice(relationshipIds.indexOf(oldTable.id), 1);
		
		newState.tables = {
			...newState.tables,
			[oldTable.id]: {
				...oldTable,
				relationshipIds,
			},
		};
	}
	
	if (oldRelationship.parentTableId !== relationship.parentTableId &&
		oldRelationship.parentTableId !== relationship.childTableId)
	{
		// former parent table should no longer have this relationship
		const oldTable = newState.tables[oldRelationship.parentTableId];
		const relationshipIds = [...oldTable.relationshipIds];
		relationshipIds.splice(relationshipIds.indexOf(oldTable.id), 1);
		
		newState.tables = {
			...newState.tables,
			[oldTable.id]: {
				...oldTable,
				relationshipIds,
			},
		};
	}
	
	const childTable = newState.tables[relationship.childTableId];
	if (!childTable.relationshipIds.includes(id)) {
		newState.tables = {
			...newState.tables,
			[childTable.id]: {
				...childTable,
				relationshipIds: [...childTable.relationshipIds, id],
			},
		};
	}
	
	const parentTable = newState.tables[relationship.parentTableId];
	if (!parentTable.relationshipIds.includes(id)) {
		newState.tables = {
			...newState.tables,
			[parentTable.id]: {
				...parentTable,
				relationshipIds: [...parentTable.relationshipIds, id],
			},
		};
	}
	
	newState.relationships[id] = relationship;
	
	return newState;
}

function handleDeleteRelationship(schemaState: SchemaState, action: DeleteRelationshipActionData): SchemaState {
	const { relationshipId } = action.payload;
	const relationship = schemaState.relationships[relationshipId];
	const childTable = schemaState.tables[relationship.childTableId];
	const parentTable = schemaState.tables[relationship.parentTableId];
	
	const childRelationships = [...childTable.relationshipIds];
	const parentRelationships = [...parentTable.relationshipIds];
	childRelationships.splice(childRelationships.indexOf(relationshipId), 1);
	parentRelationships.splice(parentRelationships.indexOf(relationshipId), 1);
	
	const relationships = {...schemaState.relationships};
	delete relationships[relationshipId];
	
	return {
		...schemaState,
		tables: {
			...schemaState.tables,
			[childTable.id]: {
				...childTable,
				relationshipIds: childRelationships,
			},
			[parentTable.id]: {
				...parentTable,
				relationshipIds: parentRelationships,
			},
		},
		relationships,
	};
}

function handleCreateIndex(schemaState: SchemaState, action: CreateIndexActionData): SchemaState {
	const { index } = action.payload;
	index.id = index.id || generateId(IdPrefix.index);
	const columns = index.columnIds.map(id => schemaState.columns[id]);
	
	const newState: SchemaState = {
		...schemaState,
		columns: {
			...schemaState.columns,
		},
		indexes: {
			...schemaState.indexes,
			[index.id]: index,
		},
	};
	
	for (const column of columns) {
		const existingIndexes = column.options[ColumnOptionType.index]?.indexIds || [];
		
		newState.columns[column.id] = {
			...column,
			options: {
				...column.options,
				[ColumnOptionType.index]: {
					indexIds: [...existingIndexes, index.id],
				},
			},
		};
	}
	
	return newState;
}

function handleAlterIndexProperty(schemaState: SchemaState, action: AlterIndexPropertyActionData): SchemaState {
	const { id, propertyName, value } = action.payload;
	
	const newState: SchemaState = {
		...schemaState,
		indexes: {
			...schemaState.indexes,
		},
		columns: {
			...schemaState.columns,
		},
	};
	
	const oldIndex = schemaState.indexes[id];
	const newIndex = {...oldIndex};
	(newIndex[propertyName] as any) = value;
	
	if (!shallowEqual(oldIndex.columnIds, newIndex.columnIds)) {
		// remove index id from old columns
		const oldColumns = oldIndex.columnIds.map(id => newState.columns[id]);
		for (let c of oldColumns) {
			if (newIndex.columnIds.includes(c.id)) {
				continue;
			}
			
			const options = cloneDeep(c.options);
			const columnIndexData = options[ColumnOptionType.index]!;
			
			if (columnIndexData.indexIds.length === 1) {
				delete options[ColumnOptionType.index];
			}
			else {
				columnIndexData.indexIds.splice(columnIndexData.indexIds.indexOf(id), 1);
			}
			
			newState.columns[c.id] = {
				...c,
				options,
			};
		}
		
		
		// add index id to new columns
		const newColumns = newIndex.columnIds.map(id => newState.columns[id]);
		for (const c of newColumns) {
			const options = cloneDeep(c.options);
			const columnIndexData = options[ColumnOptionType.index] ?? {indexIds: []};
			if (!columnIndexData.indexIds.includes(id)) {
				columnIndexData.indexIds.push(id);
			}
			options[ColumnOptionType.index] = columnIndexData;
			
			newState.columns[c.id] = {
				...c,
				options,
			};
		}
	}
	
	newState.indexes[id] = newIndex;
	
	return newState;
}

function handleDeleteIndex(schemaState: SchemaState, action: DeleteIndexActionData): SchemaState {
	const { id } = action.payload;
	
	const index = schemaState.indexes[id];
	const indexes = {...schemaState.indexes};
	delete indexes[id];
	
	const newState: SchemaState = {
		...schemaState,
		columns: {
			...schemaState.columns,
		},
		indexes,
	};
	
	const columns = index.columnIds.map(id => newState.columns[id]);
	
	for (let c of columns) {
		const options = cloneDeep(c.options);
		const columnIndexData = options[ColumnOptionType.index]!;
		
		if (columnIndexData.indexIds.length === 1) {
			delete options[ColumnOptionType.index];
		}
		else {
			columnIndexData.indexIds.splice(columnIndexData.indexIds.indexOf(id), 1);
		}
		
		newState.columns[c.id] = {
			...c,
			options,
		};
	}
	
	return newState;
}

function handleCreateUnique(schemaState: SchemaState, action: CreateUniqueActionData): SchemaState {
	const { unique } = action.payload;
	unique.id = unique.id || generateId(IdPrefix.unique);
	const columns = unique.columnIds.map(id => schemaState.columns[id]);
	
	const newState: SchemaState = {
		...schemaState,
		columns: {
			...schemaState.columns,
		},
		uniques: {
			...schemaState.uniques,
			[unique.id]: unique,
		},
	};
	
	for (const column of columns) {
		const existingUniques = column.options[ColumnOptionType.unique]?.uniqueIds || [];
		
		newState.columns[column.id] = {
			...column,
			options: {
				...column.options,
				[ColumnOptionType.unique]: {
					uniqueIds: [...existingUniques, unique.id],
				},
			},
		};
	}
	
	return newState;
}

function handleAlterUniqueProperty(schemaState: SchemaState, action: AlterUniquePropertyActionData): SchemaState {
	const { id, propertyName, value } = action.payload;
	
	const newState: SchemaState = {
		...schemaState,
		uniques: {
			...schemaState.uniques,
		},
		columns: {
			...schemaState.columns,
		},
	};
	
	const oldUnique = schemaState.uniques[id];
	const newUnique = {...oldUnique};
	(newUnique[propertyName] as any) = value;
	
	if (!shallowEqual(oldUnique.columnIds, newUnique.columnIds)) {
		// remove unique id from old columns
		const oldColumns = oldUnique.columnIds.map(id => newState.columns[id]);
		for (const c of oldColumns) {
			const options = cloneDeep(c.options);
			const columnUniqueData = options[ColumnOptionType.unique]!;
			
			if (columnUniqueData.uniqueIds.length === 1) {
				delete options[ColumnOptionType.unique];
			}
			else {
				columnUniqueData.uniqueIds.splice(columnUniqueData.uniqueIds.indexOf(id), 1);
			}
			
			newState.columns[c.id] = {
				...c,
				options,
			};
		}
		
		// add unique id to new columns
		const newColumns = newUnique.columnIds.map(id => newState.columns[id]);
		for (const c of newColumns) {
			const options = cloneDeep(c.options);
			const columnUniqueData = options[ColumnOptionType.unique] ?? {uniqueIds: []};
			if (!columnUniqueData.uniqueIds.includes(id)) {
				columnUniqueData.uniqueIds.push(id);
			}
			options[ColumnOptionType.unique] = columnUniqueData;
			
			newState.columns[c.id] = {
				...c,
				options,
			};
		}
	}
	
	newState.uniques[id] = newUnique;
	
	return newState;
}

function handleDeleteUnique(schemaState: SchemaState, action: DeleteUniqueActionData): SchemaState {
	const { id } = action.payload;
	
	const unique = schemaState.uniques[id];
	const uniques = {...schemaState.uniques};
	delete uniques[id];
	
	const newState: SchemaState = {
		...schemaState,
		columns: {
			...schemaState.columns,
		},
		uniques,
	};
	
	const columns = unique.columnIds.map(id => newState.columns[id]);
	
	for (let c of columns) {
		const options = cloneDeep(c.options);
		const columnUniqueData = options[ColumnOptionType.unique]!;
		
		if (columnUniqueData.uniqueIds.length === 1) {
			delete options[ColumnOptionType.unique];
		}
		else {
			columnUniqueData.uniqueIds.splice(columnUniqueData.uniqueIds.indexOf(id), 1);
		}
		
		newState.columns[c.id] = {
			...c,
			options,
		};
	}
	
	return newState;
}

function handleCreatePrimaryKey(schemaState: SchemaState, action: CreatePrimaryKeyActionData): SchemaState {
	const { primaryKey } = action.payload;
	primaryKey.id = primaryKey.id || generateId(IdPrefix.primaryKey);
	const table = schemaState.tables[primaryKey.tableId];
	if (table.primaryKeyId) {
		// a mixin could create a new primary key for a table that already has one
		delete schemaState.primaryKeys[table.primaryKeyId];
	}
	
	const newState: SchemaState = {
		...schemaState,
		tables: {
			...schemaState.tables,
			[primaryKey.tableId]: {
				...schemaState.tables[primaryKey.tableId],
				primaryKeyId: primaryKey.id,
			},
		},
		primaryKeys: {
			...schemaState.primaryKeys,
			[primaryKey.id]: primaryKey,
		},
	};
	
	return newState;
}

function handleAlterPrimaryKeyProperty(schemaState: SchemaState, action: AlterPrimaryKeyPropertyActionData): SchemaState {
	const { id, propertyName, value } = action.payload;
	const pk = schemaState.primaryKeys[id];
	
	return {
		...schemaState,
		primaryKeys: {
			...schemaState.primaryKeys,
			[id]: {
				...pk,
				[propertyName]: value,
			},
		},
	};
}

function handleDeletePrimaryKey(schemaState: SchemaState, action: DeletePrimaryKeyActionData): SchemaState {
	const { id } = action.payload;
	
	const primaryKey = schemaState.primaryKeys[id];
	const primaryKeys = {...schemaState.primaryKeys};
	delete primaryKeys[id];
	const table: TableData = {
		...schemaState.tables[primaryKey.tableId],
		primaryKeyId: null,
	};
	
	const newState: SchemaState = {
		...schemaState,
		tables: {
			...schemaState.tables,
			[table.id]: table,
		},
		primaryKeys,
	};
	
	return newState;
}

function handleSetSchemaSettings(schemaState: SchemaState, action: SetSchemaSettingsActionData): SchemaState {
	const { settings } = action.payload;
	
	return {
		...schemaState,
		settings,
	};
}

function handleAlterSchemaSettingsProperty(schemaState: SchemaState, action: AlterSchemaSettingsPropertyActionData): SchemaState {
	const { path, value } = action.payload;
	
	const settings = {...schemaState.settings};
	let container: any = settings;
	for (let index = 0; index < path.length - 1; index++) {
		const key = path[index] as any;
		container[key] = {
			...container[key],
		};
		container = container[key];
	}
	
	container[path[path.length - 1] as any] = value;
	
	return {
		...schemaState,
		settings,
	};
}

function handleCreateNamespace(schemaState: SchemaState, action: CreateNamespaceActionData): SchemaState {
	const { namespace } = action.payload;
	namespace.id = namespace.id || generateId(IdPrefix.namespace);
	
	return {
		...schemaState,
		namespaces: {
			...schemaState.namespaces,
			[namespace.id]: namespace,
		},
	};
}

function handleAlterNamespaceProperty(schemaState: SchemaState, action: AlterNamespacePropertyActionData): SchemaState {
	const { id, propertyName, value } = action.payload;
	
	return {
		...schemaState,
		namespaces: {
			...schemaState.namespaces,
			[id]: {
				...schemaState.namespaces[id],
				[propertyName]: value,
			},
		},
	};
}

function handleDeleteNamespace(schemaState: SchemaState, action: DeleteNamespaceActionData): SchemaState {
	const { id } = action.payload;
	const namespaces = {...schemaState.namespaces};
	delete namespaces[id];
	
	return {
		...schemaState,
		namespaces,
	};
}
