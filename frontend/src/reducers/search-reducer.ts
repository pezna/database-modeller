import { ActionData, ActionType, SearchState, SetSearchParametersActionData, SetSearchResultsActionData } from "@/data";

const initialSearchState: SearchState = {
	parameters: {
		searchText: "",
		isRegex: false,
		isCaseSensitive: false,
		currentNamespaceOnly: true,
	},
	results: {
		tables: [],
		columns: [],
		relationships: [],
		indexes: [],
		uniques: [],
		primaryKeys: [],
		machines: [],
		components: [],
		connectors: [],
	},
};

export default function searchReducer(searchState = initialSearchState, action: ActionData): SearchState {
	switch (action.type) {
		case ActionType.setSearchParameters:
			return handleSetSearchParameters(searchState, action as SetSearchParametersActionData);
		case ActionType.setSearchResults:
			return handleSetSearchResults(searchState, action as SetSearchResultsActionData);
		default:
			return searchState;
	}
}

function handleSetSearchParameters(searchState: SearchState, action: SetSearchParametersActionData): SearchState {
	const { parameters } = action.payload;
	
	return {
		...searchState,
		parameters,
	};
}

function handleSetSearchResults(searchState: SearchState, action: SetSearchResultsActionData): SearchState {
	const { results } = action.payload;
	
	return {
		...searchState,
		results,
	};
}
