import { ActionData, ActionType, AlterNotePropertyActionData, BoardState, CreateNoteActionData, DeleteNoteActionData, SetBoardStateActionData, SetNotesActionData } from "@/data";

const initialBoardState: BoardState = {
	notes: {},
};

export default function boardReducer(boardState = initialBoardState, action: ActionData): BoardState {
	switch (action.type) {
		case ActionType.setBoardState:
			return handleSetBoardState(boardState, action as SetBoardStateActionData);
		case ActionType.createNote:
			return handleCreateNote(boardState, action as CreateNoteActionData);
		case ActionType.alterNoteProperty:
			return handleAlterNoteProperty(boardState, action as AlterNotePropertyActionData);
		case ActionType.deleteNote:
			return handleDeleteNote(boardState, action as DeleteNoteActionData);
		case ActionType.setNotes:
			return handleSetNotes(boardState, action as SetNotesActionData);
		default:
			return boardState;
	}
}

function handleSetBoardState(_boardState: BoardState, action: SetBoardStateActionData): BoardState {
	return action.payload.data;
}

function handleCreateNote(boardState: BoardState, action: CreateNoteActionData): BoardState {
	const { note } = action.payload;
	
	return {
		...boardState,
		notes: {
			...boardState.notes,
			[note.id]: note,
		},
	};
}

function handleAlterNoteProperty(boardState: BoardState, action: AlterNotePropertyActionData): BoardState {
	const { id, propertyName, value } = action.payload;
	
	return {
		...boardState,
		notes: {
			...boardState.notes,
			[id]: {
				...boardState.notes[id],
				[propertyName]: value,
			},
		},
	};
}

function handleDeleteNote(boardState: BoardState, action: DeleteNoteActionData): BoardState {
	const { id } = action.payload;
	const notes = {...boardState.notes};
	delete notes[id];
	
	return {
		...boardState,
		notes,
	};
}

function handleSetNotes(boardState: BoardState, action: SetNotesActionData): BoardState {
	const { notes } = action.payload;
	
	return {
		...boardState,
		notes,
	};
}
