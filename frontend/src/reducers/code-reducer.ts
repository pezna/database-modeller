import { ActionData, ActionType, CodeState, DeleteSqlDocumentActionData, DeleteSqlSchemaUsesActionData, DeleteStructOutputActionData, SetCodeStateActionData, SetSqlDocumentActionData, SetSqlSchemaUsesActionData, SetStructOutputActionData } from "@/data";

const initialCodeState: CodeState = {
	sqlDocuments: {},
	structOutputs: {},
};

export default function codeReducer(codeState = initialCodeState, action: ActionData): CodeState {
	switch (action.type) {
		case ActionType.setCodeState:
			return handleSetCodeState(codeState, action as SetCodeStateActionData);
		case ActionType.setSqlDocument:
			return handleSetSqlDocument(codeState, action as SetSqlDocumentActionData);
		case ActionType.deleteSqlDocument:
			return handleDeleteSqlDocument(codeState, action as DeleteSqlDocumentActionData);
		case ActionType.setSqlSchemaUses:
			return handleSetSqlSchemaUses(codeState, action as SetSqlSchemaUsesActionData);
		case ActionType.deleteSqlSchemaUses:
			return handleDeleteSqlSchemaUses(codeState, action as DeleteSqlSchemaUsesActionData);
		case ActionType.setStructOutput:
			return handleSetStructOutput(codeState, action as SetStructOutputActionData);
		case ActionType.deleteStructOutput:
			return handleDeleteStructOutput(codeState, action as DeleteStructOutputActionData);
		default:
			return codeState;
	}
}

function handleSetCodeState(codeState: CodeState, action: SetCodeStateActionData): CodeState {
	const { data } = action.payload;
	
	return {
		...data,
	};
}

function handleSetSqlDocument(codeState: CodeState, action: SetSqlDocumentActionData): CodeState {
	const { data } = action.payload;
	
	return {
		...codeState,
		sqlDocuments: {
			...codeState.sqlDocuments,
			[data.id]: data,
		},
	};
}

function handleDeleteSqlDocument(codeState: CodeState, action: DeleteSqlDocumentActionData): CodeState {
	const { id } = action.payload;
	
	const sqlDocuments = {...codeState.sqlDocuments};
	delete sqlDocuments[id];
	
	return {
		...codeState,
		sqlDocuments,
	};
}

function handleSetSqlSchemaUses(codeState: CodeState, action: SetSqlSchemaUsesActionData): CodeState {
	const { id, uses } = action.payload;
	const sqlDoc = codeState.sqlDocuments[id];
	
	return {
		...codeState,
		sqlDocuments: {
			...codeState.sqlDocuments,
			[id]: {
				...sqlDoc,
				schemaUsages: uses,
			},
		},
	};
}

function handleDeleteSqlSchemaUses(codeState: CodeState, action: DeleteSqlSchemaUsesActionData): CodeState {
	const { id } = action.payload;
	const sqlDoc = codeState.sqlDocuments[id];
	
	return {
		...codeState,
		sqlDocuments: {
			...codeState.sqlDocuments,
			[id]: {
				...sqlDoc,
				schemaUsages: [],
			},
		},
	};
}

function handleSetStructOutput(codeState: CodeState, action: SetStructOutputActionData): CodeState {
	const { data } = action.payload;
	
	return {
		...codeState,
		structOutputs: {
			...codeState.structOutputs,
			[data.id]: data,
		},
	};
}

function handleDeleteStructOutput(codeState: CodeState, action: DeleteStructOutputActionData): CodeState {
	const { id } = action.payload;
	
	const structOutputs = {...codeState.structOutputs};
	delete structOutputs[id];
	
	return {
		...codeState,
		structOutputs,
	};
}
