import { ActionData, ActionType, AlterComponentPropertyActionData, AlterConnectorPropertyActionData, AlterMachinePropertyActionData, ComponentData, ConnectorData, CreateComponentActionData, CreateConnectorActionData, CreateMachineActionData, DeleteComponentActionData, DeleteConnectorActionData, DeleteMachineActionData, MachineState, defaultMachineState } from "@/data";

export const initialMachineState: MachineState = defaultMachineState();

export default function machineReducer(machineState = initialMachineState, action: ActionData): MachineState {
	switch (action.type) {
		case ActionType.createMachine:
			return handleCreateMachine(machineState, action as CreateMachineActionData);
		case ActionType.alterMachineProperty:
			return handleAlterMachineProperty(machineState, action as AlterMachinePropertyActionData);
		case ActionType.deleteMachine:
			return handleDeleteMachine(machineState, action as DeleteMachineActionData);
		case ActionType.createComponent:
			return handleCreateComponent(machineState, action as CreateComponentActionData);
		case ActionType.deleteComponent:
			return handleDeleteComponent(machineState, action as DeleteComponentActionData);
		case ActionType.alterComponentProperty:
			return handleAlterComponentProperty(machineState, action as AlterComponentPropertyActionData<any, any>);
		case ActionType.createConnector:
			return handleCreateConnector(machineState, action as CreateConnectorActionData);
		case ActionType.deleteConnector:
			return handleDeleteConnector(machineState, action as DeleteConnectorActionData);
		case ActionType.alterConnectorProperty:
			return handleAlterConnectorProperty(machineState, action as AlterConnectorPropertyActionData<any, any>);
		default:
			return machineState;
	}
}

function handleCreateMachine(machineState = initialMachineState, action: CreateMachineActionData): MachineState {
	const { machine } = action.payload;
	
	return {
		...machineState,
		machines: {
			...machineState.machines,
			[machine.id]: {
				...machine,
			},
		},
	};
}

function handleAlterMachineProperty(machineState = initialMachineState, action: AlterMachinePropertyActionData): MachineState {
	const { machineId, propertyName, value } = action.payload;
	
	const machine = machineState.machines[machineId];
	
	return {
		...machineState,
		machines: {
			...machineState.machines,
			[machineId]: {
				...machine,
				[propertyName]: value,
			},
		},
	};
}

function handleDeleteMachine(machineState = initialMachineState, action: DeleteMachineActionData): MachineState {
	const { id } = action.payload;
	
	const machines = {...machineState.machines};
	delete machines[id];
	
	return {
		...machineState,
		machines,
	};
}

function handleCreateComponent(machineState = initialMachineState, action: CreateComponentActionData): MachineState {
	const { component } = action.payload;
	
	const machine = machineState.machines[component.machineId];
	
	return {
		...machineState,
		machines: {
			...machineState.machines,
			[machine.id]: {
				...machine,
				componentIds: [component.id, ...machine.componentIds],
			},
		},
		components: {
			...machineState.components,
			[component.id]: component,
		},
	};
}

function handleAlterComponentProperty<T extends ComponentData, K extends keyof T>(machineState = initialMachineState, action: AlterComponentPropertyActionData<T, K>): MachineState {
	const { componentId, propertyName, value } = action.payload;
	
	const component = machineState.components[componentId];
	
	return {
		...machineState,
		components: {
			...machineState.components,
			[componentId]: {
				...component,
				[propertyName]: value,
			},
		},
	};
}

function handleDeleteComponent(machineState = initialMachineState, action: DeleteComponentActionData): MachineState {
	const { id } = action.payload;
	
	const component = machineState.components[id];
	const components = {...machineState.components};
	delete components[id];
	
	const machine = machineState.machines[component.machineId];
	const componentIds = [...machine.componentIds];
	const index = componentIds.indexOf(id);
	if (index > -1) {
		componentIds.splice(index, 1);
	}
	
	return {
		...machineState,
		machines: {
			...machineState.machines,
			[machine.id]: {
				...machine,
				componentIds,
			},
		},
		components,
	};
}

function handleCreateConnector(machineState = initialMachineState, action: CreateConnectorActionData): MachineState {
	const { connector } = action.payload;
	
	const machine = machineState.machines[connector.machineId];
	const component1 = machineState.components[connector.sourceComponentId];
	const component2 = machineState.components[connector.destComponentId];
	
	return {
		...machineState,
		machines: {
			...machineState.machines,
			[machine.id]: {
				...machine,
				connectorIds: [connector.id, ...machine.connectorIds],
			},
		},
		connectors: {
			...machineState.connectors,
			[connector.id]: connector,
		},
		components: {
			...machineState.components,
			[component1.id]: {
				...component1,
				connectorIds: [connector.id, ...component1.connectorIds],
			},
			[component2.id]: {
				...component2,
				connectorIds: [connector.id, ...component2.connectorIds],
			},
		},
	};
}

function handleAlterConnectorProperty<T extends ConnectorData, K extends keyof T>(machineState = initialMachineState, action: AlterConnectorPropertyActionData<T, K>): MachineState {
	const { connectorId, propertyName, value } = action.payload;
	
	const connector = machineState.connectors[connectorId];
	
	return {
		...machineState,
		connectors: {
			...machineState.connectors,
			[connectorId]: {
				...connector,
				[propertyName]: value,
			},
		},
	};
}

function handleDeleteConnector(machineState = initialMachineState, action: DeleteConnectorActionData): MachineState {
	const { id } = action.payload;
	
	const connector = machineState.connectors[id];
	const component1 = machineState.components[connector.sourceComponentId];
	const comp1ConnectorIds = [...component1.connectorIds];
	let index = comp1ConnectorIds.indexOf(id);
	if (index > -1) {
		comp1ConnectorIds.splice(index, 1);
	}
	
	const component2 = machineState.components[connector.destComponentId];
	const comp2ConnectorIds = [...component2.connectorIds];
	index = comp2ConnectorIds.indexOf(id);
	if (index > -1) {
		comp2ConnectorIds.splice(index, 1);
	}
	
	const connectors = {...machineState.connectors};
	delete connectors[id];
	
	const machine = machineState.machines[connector.machineId];
	const connectorIds = [...machine.connectorIds];
	index = connectorIds.indexOf(id);
	if (index > -1) {
		connectorIds.splice(index, 1);
	}
	
	return {
		...machineState,
		machines: {
			...machineState.machines,
			[machine.id]: {
				...machine,
				connectorIds,
			},
		},
		connectors,
		components: {
			...machineState.components,
			[component1.id]: {
				...component1,
				connectorIds: comp1ConnectorIds,
			},
			[component2.id]: {
				...component2,
				connectorIds: comp2ConnectorIds,
			},
		},
	};
}
