import { ActionData, ActionType, CreateErrorActionData, DeleteErrorActionData, ErrorState, defaultErrorState } from "@/data";

const initialErrorState: ErrorState = defaultErrorState();

export default function errorReducer(errorState = initialErrorState, action: ActionData): ErrorState {
	switch (action.type) {
		case ActionType.createError:
			return handleCreateError(errorState, action as CreateErrorActionData);
		case ActionType.deleteError:
			return handleDeleteError(errorState, action as DeleteErrorActionData);
		default:
			return errorState;
	}
}

function handleCreateError(errorState: ErrorState, action: CreateErrorActionData): ErrorState {
	const { errorKey, errorGroup, data } = action.payload;
	
	const array = errorState[errorGroup][errorKey] ?? [];
	
	return {
		...errorState,
		[errorGroup]: {
			...errorState[errorGroup],
			[errorKey]: [...array, data],
		},
	};
}

function handleDeleteError(errorState: ErrorState, action: DeleteErrorActionData): ErrorState {
	const { errorGroup, errorKey } = action.payload;
	
	const newState: ErrorState = {
		...errorState,
		[errorGroup]: {
			...errorState[errorGroup],
		}
	};
	
	delete newState[errorGroup][errorKey];
	
	return newState;
}
