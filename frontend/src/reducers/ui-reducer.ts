import { ActionData, ActionType, AlterUiStatePropertyActionData, DeselectBoardActionData, ModelObjectType, SelectModelObjectActionData, SetUiStateActionData, UiState, defaultUiState } from "@/data";

export const initialUiState: UiState = defaultUiState();

export default function uiReducer(uiState = initialUiState, action: ActionData): UiState {
	switch (action.type) {
		case ActionType.deselectBoard:
			return handleDeselectBoard(uiState, action as DeselectBoardActionData);
		case ActionType.selectModelObject:
			return handleSelectModelObject(uiState, action as SelectModelObjectActionData);
		case ActionType.setUiState:
			return handleSetUiState(uiState, action as SetUiStateActionData);
		case ActionType.alterUiStateProperty:
			return handleAlterUiStateProperty(uiState, action as AlterUiStatePropertyActionData);
		default:
			return uiState;
	}
}

function handleSetUiState(_uiState: UiState, action: SetUiStateActionData): UiState {
	const { state } = action.payload;
	
	return state;
}

function handleAlterUiStateProperty(uiState: UiState, action: AlterUiStatePropertyActionData): UiState {
	const { path, value } = action.payload;
	
	const state = {...uiState};
	let container: any = state;
	for (let index = 0; index < path.length - 1; index++) {
		const key = path[index] as any;
		container[key] = {
			...container[key],
		};
		container = container[key];
	}
	
	container[path.at(-1) as any] = value;
	
	return state;
}

function handleDeselectBoard(uiState: UiState, action: DeselectBoardActionData): UiState {
	// don't deselect machine or namespace
	const { everything } = action.payload;
	return {
		...uiState,
		boardSelection: {
			selectedTableId: null,
			selectedColumnId: null,
			selectedRelationshipId: null,
			selectedNoteId: null,
			selectedComponentId: null,
			selectedConnectorId: null,
			selectedMachineId: everything ? null : uiState.boardSelection.selectedMachineId,
			selectedNamespaceId: everything ? null : uiState.boardSelection.selectedNamespaceId,
		},
	};
}

function handleSelectModelObject(uiState: UiState, action: SelectModelObjectActionData): UiState {
	const { modelType, id, otherIds } = action.payload;
	let key: keyof UiState['boardSelection'];
	switch (modelType) {
		case ModelObjectType.column:
			key = "selectedColumnId";
			break;
		case ModelObjectType.component:
			key = "selectedComponentId";
			break;
		case ModelObjectType.connector:
			key = "selectedConnectorId";
			break;
		case ModelObjectType.machine:
			key = "selectedMachineId";
			break;
		case ModelObjectType.namespace:
			key = "selectedNamespaceId";
			break;
		case ModelObjectType.note:
			key = "selectedNoteId";
			break;
		case ModelObjectType.relationship:
			key = "selectedRelationshipId";
			break;
		case ModelObjectType.table:
			key = "selectedTableId";
			break;
	}
	
	return {
		...uiState,
		boardSelection: {
			selectedTableId: null,
			selectedColumnId: null,
			selectedRelationshipId: null,
			selectedNoteId: null,
			selectedNamespaceId: otherIds.namespaceId ?? uiState.boardSelection.selectedNamespaceId,
			selectedMachineId: otherIds.machineId ?? uiState.boardSelection.selectedMachineId,
			selectedComponentId: null,
			selectedConnectorId: null,
			[key]: id,
		}
	};
}
