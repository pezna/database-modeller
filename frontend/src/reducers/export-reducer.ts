import { ActionData, ActionType, AddExportHistoryActionData, DeleteCustomExportActionData, DeleteExportHistoryActionData, ExportState, SetCustomExportActionData, SetExportStateActionData } from "@/data";

const initialExportState: ExportState = {
	sqlExportHistory: {},
	customExports: {},
};

export default function exportReducer(exportState = initialExportState, action: ActionData): ExportState {
	switch (action.type) {
		case ActionType.setExportState:
			return handleSetExportState(exportState, action as SetExportStateActionData);
		case ActionType.addExportHistory:
			return handleAddExportHistory(exportState, action as AddExportHistoryActionData);
		case ActionType.deleteExportHistory:
			return handleDeleteExportHistory(exportState, action as DeleteExportHistoryActionData);
		case ActionType.setCustomExport:
			return handleSetCustomExport(exportState, action as SetCustomExportActionData);
		case ActionType.deleteCustomExport:
			return handleDeleteCustomExport(exportState, action as DeleteCustomExportActionData);
		default:
			return exportState;
	}
}

function handleSetExportState(exportState: ExportState, action: SetExportStateActionData): ExportState {
	const { data } = action.payload;
	
	return {
		...data,
	};
}

function handleAddExportHistory(exportState: ExportState, action: AddExportHistoryActionData): ExportState {
	const { data } = action.payload;
	
	return {
		...exportState,
		sqlExportHistory: {
			...exportState.sqlExportHistory,
			[data.id]: data,
		},
	};
}

function handleDeleteExportHistory(exportState: ExportState, action: DeleteExportHistoryActionData): ExportState {
	const { time } = action.payload;
	const sqlExportHistory = {...exportState.sqlExportHistory};
	delete sqlExportHistory[time];
	
	return {
		...exportState,
		sqlExportHistory,
	};
}

function handleSetCustomExport(exportState: ExportState, action: SetCustomExportActionData): ExportState {
	const { data } = action.payload;
	
	return {
		...exportState,
		customExports: {
			...exportState.customExports,
			[data.id]: data,
		},
	};
}

function handleDeleteCustomExport(exportState: ExportState, action: DeleteCustomExportActionData): ExportState {
	const { id } = action.payload;
	
	const newState = {
		...exportState,
		customExports: {
			...exportState.customExports,
		},
	};
	
	delete newState.customExports[id];
	
	return newState;
}
