import { ActionData, ActionType, AppMode, ClearNotificationsActionData, CreateNotificationActionData, DeleteNotificationActionData, GeneralState, SetAppModeActionData, SetGeneralStateActionData, SetIsUnsavedActionData, SetProjectIdActionData } from "@/data";

export const initialGeneralState: GeneralState = {
	projectId: null,
	appMode: AppMode.database,
	modelFilePath: null,
	notifications: {},
	isUnsaved: true,
};

export default function generalReducer(generalState = initialGeneralState, action: ActionData): GeneralState {
	switch (action.type) {
		case ActionType.setProjectId:
			return handleSetProjectId(generalState, action as SetProjectIdActionData);
		case ActionType.setGeneralState:
			return handleSetGeneralState(generalState, action as SetGeneralStateActionData);
		case ActionType.createNotification:
			return handleCreateNotification(generalState, action as CreateNotificationActionData);
		case ActionType.deleteNotification:
			return handleDeleteNotification(generalState, action as DeleteNotificationActionData);
		case ActionType.clearNotifications:
			return handleClearNotifications(generalState, action as ClearNotificationsActionData);
		case ActionType.setAppMode:
			return handleSetAppMode(generalState, action as SetAppModeActionData);
		case ActionType.setIsUnsaved:
			return handleSetIsUnsaved(generalState, action as SetIsUnsavedActionData);
		default:
			return generalState;
	}
}

function handleSetIsUnsaved(generalState: GeneralState, action: SetIsUnsavedActionData): GeneralState {
	const { isUnsaved } = action.payload;
	
	return {
		...generalState,
		isUnsaved,
	};
}

function handleSetProjectId(generalState: GeneralState, action: SetProjectIdActionData): GeneralState {
	return {
		...generalState,
		projectId: action.payload.id,
	};
}

function handleSetGeneralState(_generalState: GeneralState, action: SetGeneralStateActionData): GeneralState {
	return action.payload.data;
}

function handleCreateNotification(generalState: GeneralState, action: CreateNotificationActionData): GeneralState {
	const { data } = action.payload;
	
	return {
		...generalState,
		notifications: {
			...generalState.notifications,
			[data.id]: data,
		},
	};
}

function handleDeleteNotification(generalState: GeneralState, action: DeleteNotificationActionData): GeneralState {
	const { id } = action.payload;
	
	const notifications = {
		...generalState.notifications,
	};
	delete notifications[id];
	
	return {
		...generalState,
		notifications,
	};
}

function handleClearNotifications(generalState: GeneralState, _action: ClearNotificationsActionData): GeneralState {
	return {
		...generalState,
		notifications: {},
	};
}

function handleSetAppMode(generalState: GeneralState, action: SetAppModeActionData): GeneralState {
	const { appMode } = action.payload;
	
	return {
		...generalState,
		appMode,
	};
}
