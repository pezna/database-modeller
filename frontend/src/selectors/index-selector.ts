import { ColumnOptionType, IndexData, RootState } from "@/data";
import { createSelector } from "@reduxjs/toolkit";
import { getColumnOptions } from "./column-selector";

export function getIndexes(state: RootState) {
	return state.schema.indexes;
}

export function getIndexesAsArray(state: RootState) {
	return Object.values(state.schema.indexes);
}

export function getIndex(state: RootState, indexId: string): IndexData {
	return state.schema.indexes[indexId];
}

export const getColumnIndexes = createSelector(
	[
		getIndexes,
		(state: RootState, columnId: string) => getColumnOptions(state, columnId)[ColumnOptionType.index],
	],
	(indexes, optionIndexData) => {
		if (!optionIndexData) {
			return [];
		}
		
		return optionIndexData.indexIds.map(key => indexes[key]);
	}
);

export const getTableIndexes = createSelector(
	[
		getIndexes,
		(_: RootState, tableId: string) => tableId,
	],
	(indexes, tableId) => Object.values(indexes).filter(index => index.tableId === tableId),
);

export const getMixinIndexClones = createSelector(
	[
		getIndexes,
		(_: RootState, mixinIndexId: string) => mixinIndexId,
	],
	(indexes, mixinIndexId) => {
		return Object.values(indexes).filter(index => index.belongsToMixinSchemaItem === mixinIndexId);
	},
);
