import { RootState } from "@/data/state";
import { createSelector } from "@reduxjs/toolkit";

export function getPrimaryKeys(state: RootState) {
	return state.schema.primaryKeys;
}

export function getPrimaryKey(state: RootState, primaryKeyId: string) {
	return state.schema.primaryKeys[primaryKeyId];
}

export const getMixinPrimaryKeyClones = createSelector([
		getPrimaryKeys,
		(_: RootState, mixinPrimaryKeyId: string) => mixinPrimaryKeyId,
	],
	(primaryKeys, mixinPrimaryKeyId) => {
		return Object.values(primaryKeys).filter(p => p.belongsToMixinSchemaItem === mixinPrimaryKeyId);
	}
);
