import { RootState } from "@/data/state";

export function getSchemaSettings(state: RootState) {
	return state.schema.settings;
}

export function getDefaultMixinTableIds(state: RootState) {
	return state.schema.settings.schema.mixinTables.defaultMixinTableIds;
}

export function getPrimaryKeyNameGeneratorCode(state: RootState) {
	return state.schema.settings.schema.generatorCode.primaryKeyNameGeneratorCode;
}

export function getIndexNameGeneratorCode(state: RootState) {
	return state.schema.settings.schema.generatorCode.indexNameGeneratorCode;
}

export function getUniqueNameGeneratorCode(state: RootState) {
	return state.schema.settings.schema.generatorCode.uniqueNameGeneratorCode;
}

export function getRelationshipNameGeneratorCode(state: RootState) {
	return state.schema.settings.schema.generatorCode.relationshipNameGeneratorCode;
}

export function getUseSeparateSaveFiles(state: RootState) {
	return state.schema.settings.files.saveFiles.useSeparateSaveFiles;
}

export function getSqlExportSchemaSettings(state: RootState) {
	return state.schema.settings.export.sql;
}