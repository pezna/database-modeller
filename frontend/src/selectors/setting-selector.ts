import { RootState } from "@/data/state";

export function getSettings(state: RootState) {
	return state.settings;
}

export function getBoardSettings(state: RootState) {
	return state.settings.databaseBoard;
}

export function getShowKeyColumns(state: RootState) {
	return state.settings.databaseBoard.table.showKeys;
}

export function getColumnSettings(state: RootState) {
	return state.settings.databaseBoard.column;
}

export function getRelationshipSettings(state: RootState) {
	return state.settings.databaseBoard.relationships;
}

export function getShowRelationshipLines(state: RootState) {
	return state.settings.databaseBoard.relationships.showLines;
}

export function getMixinSettings(state: RootState) {
	return state.settings.databaseBoard.mixins;
}

export function getMachineBoardSettings(state: RootState) {
	return state.settings.machineBoard;
}
