import { MachineData, RootState } from "@/data";
import { getSelectedMachineId } from "@/selectors/ui-selector";
import { nameSort } from "@/utils";
import { createSelector } from "@reduxjs/toolkit";

export function getMachines(state: RootState) {
	return state.machine.machines;
}

export const getMachinesAsArray = createSelector(
	getMachines,
	(machines) => Object.values(machines).sort(nameSort),
);

export function getMachine(state: RootState, id: string): MachineData {
	return state.machine.machines[id];
}

export function getCurrentMachine(state: RootState): MachineData {
	const id = getSelectedMachineId(state);
	if (!id) {
		throw new Error('No machine selected');
	}
	return getMachine(state, id);
}
