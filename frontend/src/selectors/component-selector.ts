import { ComponentData, RootState } from "@/data";
import { getMachine } from "@/selectors/machine-selector";
import { createSelector } from "@reduxjs/toolkit";

export function getComponents(state: RootState) {
	return state.machine.components;
}

export function getComponent<T extends ComponentData>(state: RootState, id: string): T {
	return state.machine.components[id] as T;
}

export const getComponentsForMachine = createSelector([
		getComponents,
		(state: RootState, machineId: string) => getMachine(state, machineId),
	],
	(components, machine) => machine.componentIds.map(id => components[id]),
);
