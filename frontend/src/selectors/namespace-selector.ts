import { RootState } from "@/data/state";
import { createSelector } from "@reduxjs/toolkit";
import { nameSort } from "../utils";

export function getNamespaces(state: RootState) {
	return state.schema.namespaces;
}

export const getSortedNamespaces = createSelector(
	getNamespaces,
	(namespaces) => Object.values(namespaces).sort(nameSort),
);

export function getNamespace(state: RootState, id: string) {
	return state.schema.namespaces[id];
}
