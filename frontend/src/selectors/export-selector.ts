import { RootState, SqlExportType } from "@/data";
import { nameSort } from "@/utils";
import { createSelector } from "@reduxjs/toolkit";

export function getExportHistory(state: RootState) {
	return state.export.sqlExportHistory;
}

export const getSortedExportHistory = createSelector([
	getExportHistory,
	(_: RootState, type?: SqlExportType) => type,
], (history, type) => {
	return Object.values(history).filter(x => type ? x.type === type : true).sort((a, b) => b.time - a.time);
})

export function getCustomExports(state: RootState) {
	return state.export.customExports;
}

export const getSortedCustomExports = createSelector(getCustomExports, (exports) => Object.values(exports).sort(nameSort));
