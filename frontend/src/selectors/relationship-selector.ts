import { RelationshipData, RootState } from "@/data";
import { getTables } from "@/selectors/table-selector";
import { createSelector } from "@reduxjs/toolkit";

export function getRelationships(state: RootState) {
	return state.schema.relationships;
}

export const getRelationshipsForNamespace = createSelector([
		getRelationships,
		getTables,
		(_: RootState, namespaceId: string) => namespaceId,
	],
	(relationships, tables, namespaceId) => Object.fromEntries(
		Object
		.entries(relationships)
		.filter(entry => tables[entry[1].childTableId].namespaceId === namespaceId)
	)
);

export function getRelationship(state: RootState, relationshipId: string): RelationshipData {
	return state.schema.relationships[relationshipId];
}

export const getTableRelationships = createSelector([
		getRelationships,
		getTables,
		(_: RootState, tableId: string) => tableId,
	],
	(relationships, tables, tableId) => tables[tableId].relationshipIds.map((key) => relationships[key]),
);

export const getMixinRelationshipClones = createSelector([
		getRelationships,
		(_: RootState, mixinRelationshipId: string) => mixinRelationshipId,
	],
	(relationships, mixinRelationshipId) => {
		return Object.values(relationships).filter(p => p.belongsToMixinSchemaItem === mixinRelationshipId);
	}
);
