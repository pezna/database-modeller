import { RootState } from "@/data/state";

export function getSearchResults(state: RootState) {
	return state.search.results;
}

export function getSearchParameters(state: RootState) {
	return state.search.parameters;
}
