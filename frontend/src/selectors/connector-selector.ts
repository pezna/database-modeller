import { ConnectorData, RootState } from "@/data";
import { getMachine } from "@/selectors/machine-selector";
import { createSelector } from "@reduxjs/toolkit";

export function getConnectors(state: RootState) {
	return state.machine.connectors;
}

export function getConnector<T extends ConnectorData>(state: RootState, id: string): T {
	return state.machine.connectors[id] as T;
}

export const getConnectorsForMachine = createSelector([
		getConnectors,
		(state: RootState, machineId: string) => getMachine(state, machineId),
	],
	(connectors, machine) => machine.connectorIds.map(id => connectors[id]),
);
