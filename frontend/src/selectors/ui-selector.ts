import { RootState } from "@/data/state";
import { getColumn } from "@/selectors/column-selector";

export function getTableSidebarUi(state: RootState) {
	return state.ui.tableSidebar;
}

export function getColumnSidebarUi(state: RootState) {
	return state.ui.columnSidebar;
}

export function getRelationshipSidebarUi(state: RootState) {
	return state.ui.relationshipSidebar;
}

export function getSettingsSidebarUi(state: RootState) {
	return state.ui.settingsSidebar;
}

export function getManageSchemaSidebarUi(state: RootState) {
	return state.ui.manageSchemaSidebar;
}

export function getConsoleSidebarUi(state: RootState) {
	return state.ui.consoleSidebar;
}

export function getExportSidebarUi(state: RootState) {
	return state.ui.exportSidebar;
}

export function getCodeSidebarUi(state: RootState) {
	return state.ui.codeSidebar;
}

export function getNamespaceSidebarUi(state: RootState) {
	return state.ui.namespaceSidebar;
}

export function getGeneralAppUi(state: RootState) {
	return state.ui.generalAppUi;
}

export function getDatabaseModeUi(state: RootState) {
	return state.ui.databaseModeUi;
}

export function getMachineModeUi(state: RootState) {
	return state.ui.machineModeUi;
}

export function getMachineSidebarUi(state: RootState) {
	return state.ui.machineSidebar;
}

export function getComponentSidebarUi(state: RootState) {
	return state.ui.componentSidebar;
}

export function getConnectorSidebarUi(state: RootState) {
	return state.ui.connectorSidebar;
}

export function getOptionsSidebarUi(state: RootState) {
	return state.ui.generalAppUi.optionsSidebar;
};

export function getPropertySidebarUi(state: RootState) {
	return state.ui.generalAppUi.propertySidebar;
};

/** Selections */

export function getSelectedTableId(state: RootState) {
	return state.ui.boardSelection.selectedTableId;
}

export function getIsTableSelected(state: RootState, tableId: string) {
	const { selectedTableId, selectedColumnId } = state.ui.boardSelection;
	if (!selectedTableId && selectedColumnId) {
		return tableId === getColumn(state, selectedColumnId).tableId;
	}
	return selectedTableId === tableId;
}

export function getSelectedColumnId(state: RootState) {
	return state.ui.boardSelection.selectedColumnId;
}

export function getIsColumnSelected(state: RootState, columnId: string) {
	return state.ui.boardSelection.selectedColumnId === columnId;
}

export function getSelectedRelationshipId(state: RootState) {
	return state.ui.boardSelection.selectedRelationshipId;
}

export function getIsRelationshipSelected(state: RootState, relationshipId: string) {
	return state.ui.boardSelection.selectedRelationshipId === relationshipId;
}

export function getIsNoteSelected(state: RootState, noteId: string) {
	return state.ui.boardSelection.selectedNoteId === noteId;
}

export function getIsAnyItemSelected(state: RootState) {
	return state.ui.boardSelection.selectedTableId !== null
		|| state.ui.boardSelection.selectedColumnId !== null
		|| state.ui.boardSelection.selectedRelationshipId !== null
		|| state.ui.boardSelection.selectedNoteId !== null
		|| state.ui.boardSelection.selectedComponentId !== null
		|| state.ui.boardSelection.selectedConnectorId !== null;
}

export function getSelectedNamespaceId(state: RootState) {
	return state.ui.boardSelection.selectedNamespaceId;
}

export function getSelectedComponentId(state: RootState) {
	return state.ui.boardSelection.selectedComponentId;
}

export function getIsComponentSelected(state: RootState, machineComponentId: string) {
	return state.ui.boardSelection.selectedComponentId === machineComponentId;
}

export function getSelectedMachineId(state: RootState) {
	return state.ui.boardSelection.selectedMachineId;
}

export function getSelectedConnectorId(state: RootState) {
	return state.ui.boardSelection.selectedConnectorId;
}

export function getIsConnectorSelected(state: RootState, connectorId: string) {
	return state.ui.boardSelection.selectedConnectorId === connectorId;
}
