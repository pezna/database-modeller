import { ColumnData, ColumnOptionValueContainer, ColumnTypeUsageData, RootState } from "@/data";
import { getNamespaces } from "@/selectors/namespace-selector";
import { nameSort } from "@/utils";
import { createSelector } from "@reduxjs/toolkit";
import { getTables } from "./table-selector";

export function getColumns(state: RootState) {
	return state.schema.columns;
}

export function getColumn(state: RootState, columnId: string): ColumnData {
	return state.schema.columns[columnId];
}

export function getColumnOptions(state: RootState, columnId: string): ColumnOptionValueContainer {
	return getColumn(state, columnId).options;
}

export const getTableColumns = createSelector([
		getTables,
		getColumns,
		(_, tableId: string) => tableId,
	],
	(tables, columns, tableId) => tables[tableId].columnIds.map(columnId => columns[columnId])
);

export const getMixinColumnClones = createSelector([
		getColumns,
		(_: RootState, mixinColumnId: string) => mixinColumnId,
		(_: RootState, _mixinColumnId: string, tableId?: string) => tableId,
	],
	(columns, mixinColumnId, tableId) => {
		const cols = Object.values(columns).filter(c => c.belongsToMixinSchemaItem === mixinColumnId!);
		return tableId ? cols.filter(c => c.tableId === tableId) : cols;
	}
);

export function getColumnTypes(state: RootState) {
	return state.schema.columnTypes;
}

export function getColumnType(state: RootState, typeId: string) {
	return state.schema.columnTypes[typeId];
}

export const getSortedColumnTypes = createSelector(getColumnTypes, types => Object.values(types).sort(nameSort));

export const getColumnTypeUses = createSelector([
		getNamespaces,
		getTables,
		getColumns,
		(state: RootState, columnTypeId: string) => getColumnType(state, columnTypeId),
	],
	(namespaces, tables, columns, columnType) => {
		return columnType.columnIds
			.map(columnId => {
				const col = columns[columnId];
				const t = tables[col.tableId];
				const ns = namespaces[t.namespaceId];
				const use: ColumnTypeUsageData = {
					columnTypeId: columnType.id,
					namespaceId: ns.id,
					namespaceName: ns.name,
					tableId: t.id,
					tableName: t.name,
					columnId: col.id,
					columnName: col.name,
				};
				return use;
			})
			.sort((a, b) => {
				let cmp = a.namespaceName.localeCompare(b.namespaceName);
				if (cmp === 0) {
					cmp = a.tableName.localeCompare(b.tableName);
					if (cmp === 0) {
						cmp = a.columnName.localeCompare(b.columnName);
					}
				}
				
				return cmp;
			});
	}
);

export const getAllColumnTypeValueKeys = createSelector([
		getColumnTypes,
	],
	(columnTypes) => {
		const allKeys = Object.values(columnTypes).map(ct => Object.keys(ct.languages))
		const keySet = new Set<string>();
		for (let keys of allKeys) {
			for (let k of keys) {
				keySet.add(k);
			}
		}
		
		return [...keySet].sort();
	}
);