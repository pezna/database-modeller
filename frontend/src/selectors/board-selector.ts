import { AppMode, RootState } from "@/data";
import { createSelector } from "@reduxjs/toolkit";

export function getNotes(state: RootState) {
	return state.board.notes;
}

export function getNote(state: RootState, id: string) {
	return state.board.notes[id];
}

export const getDatabaseNotes = createSelector(
	getNotes,
	(_: RootState, namespaceId: string) => namespaceId,
	(notes, namespaceId) => Object.values(notes).filter(n => n.appMode === AppMode.database && n.sectionId === namespaceId),
);

export const getMachineNotes = createSelector(
	getNotes,
	(_: RootState, machineId: string) => machineId,
	(notes, machineId) => Object.values(notes).filter(n => n.appMode === AppMode.machine && n.sectionId === machineId),
);
