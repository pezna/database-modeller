import { ColumnOptionType, RootState, UniqueData } from "@/data";
import { getColumnOptions } from "@/selectors/column-selector";
import { createSelector } from "@reduxjs/toolkit";

export function getUniques(state: RootState) {
	return state.schema.uniques;
}

export function getUnique(state: RootState, uniqueId: string): UniqueData {
	return state.schema.uniques[uniqueId];
}

export const getColumnUniques = createSelector([
		getUniques,
		(state: RootState, columnId: string) => getColumnOptions(state, columnId)[ColumnOptionType.unique],
	],
	(uniques, uniqueOption) => {
		if (!uniqueOption) {
			return [];
		}
		
		return uniqueOption.uniqueIds.map(key => uniques[key]);
	}
);

export const getTableUniques = createSelector([
		getUniques,
		(_, tableId: string) => tableId,
	],
	(uniques, tableId) => Object.values(uniques).filter(uq => uq.tableId === tableId)
);

export const getMixinUniqueClones = createSelector([
		getUniques,
		(_: RootState, mixinUniqueId: string) => mixinUniqueId,
	],
	(uniques, mixinUniqueId) => {
		return Object.values(uniques).filter(u => u.belongsToMixinSchemaItem === mixinUniqueId);
	},
);
