import { GenericErrorData, ModelItemErrorData, RootState } from "@/data";
import { CodeError } from "@/data/backend";

export function getErrors(state: RootState) {
	return state.errors;
}

export function getModelError(state: RootState, key: string): ModelItemErrorData[] | undefined {
	return state.errors.modelErrors[key];
}

export function getJavascriptError(state: RootState, key: string): CodeError[] | undefined {
	return state.errors.javascriptErrors[key];
}

export function getStructOutputError(state: RootState, key: string): GenericErrorData[] | undefined {
	return state.errors.structOutputErrors[key];
}
