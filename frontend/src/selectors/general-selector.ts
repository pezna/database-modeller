import { RootState } from "@/data";

export function getProjectId(state: RootState) {
	return state.general.projectId;
}

export function getModelFilePath(state: RootState) {
	return state.general.modelFilePath;
}

export function getNotifications(state: RootState) {
	return state.general.notifications;
}

export function getAppMode(state: RootState) {
	return state.general.appMode;
}

export function getIsUnsaved(state: RootState) {
	return state.general.isUnsaved;
}
