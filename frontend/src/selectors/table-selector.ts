import { RootState } from "@/data";
import { createSelector } from "@reduxjs/toolkit";
import { nameSort } from "../utils";

export function getTables(state: RootState) {
	return state.schema.tables;
}

export const getTablesForNamespace = createSelector([
		getTables,
		(_: RootState, namespaceId: string) => namespaceId,
	],
	(tables, namespaceId) => Object.fromEntries(Object.entries(tables).filter(entry => entry[1].namespaceId === namespaceId))
);

export const getSortedTables = createSelector(getTables, (tables) => Object.values(tables).sort(nameSort));

export const getNonMixinTables = createSelector(getSortedTables, (tables) => tables.filter(t => !t.isMixin));

export const getMixinTables = createSelector(getSortedTables, (tables) => tables.filter(t => t.isMixin));

export function getTable(state: RootState, tableId: string) {
	return state.schema.tables[tableId];
}

export const getMixinTableClones = createSelector([
		getTables,
		(_: RootState, mixinTableId: string) => mixinTableId,
	],
	(tables, mixinTableId) => Object.values(tables).filter(t => t.mixinTableIds.includes(mixinTableId))
);
