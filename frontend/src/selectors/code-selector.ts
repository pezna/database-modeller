import { RootState } from "@/data";
import { createSelector } from "@reduxjs/toolkit";

export function getSqlDocuments(state: RootState) {
	return state.code.sqlDocuments;
}

export function getSqlDocument(state: RootState, id: string) {
	return state.code.sqlDocuments[id];
}

export const getTableSqlUses = createSelector([
		getSqlDocuments,
		(_: RootState, tableId: string) => tableId,
	],
	(docs, tableId) => (
		Object.values(docs)
		.flatMap(x => x.schemaUsages)
		.filter(x => x.type === 'table' && x.tableId === tableId)
	),
);

export const getColumnSqlUses = createSelector([
		getSqlDocuments,
		(_: RootState, columnId: string) => columnId,
	],
	(docs, columnId) => (
		Object.values(docs)
		.flatMap(x => x.schemaUsages)
		.filter(x => x.type === 'column' && x.columnId === columnId)
	),
);

export function getStructOutputs(state: RootState) {
	return state.code.structOutputs;
}

export function getStructOutput(state: RootState, id: string) {
	return state.code.structOutputs[id];
}
