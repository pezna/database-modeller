import { errorNameAlreadyExists, errorNoColumns, errorSchemaItemNotExists, refreshGeneratedNames } from "@/actions";
import { ActionData, ActionType, AlterIndexPropertyActionData, CreateIndexActionData, DeleteIndexActionData, FunctionActionData, IndexData, ModelObjectType, RootState } from "@/data";
import { getMixinColumnClones, getMixinIndexClones, getMixinTableClones } from "@/selectors";
import { IdPrefix, generateId, isActionObject, isNextFunction } from "@/utils";
import { Dispatch, Middleware } from "redux";

export const indexMiddleware: Middleware<{}, RootState, Dispatch<any>> =
	({ getState, dispatch }) =>
	(next) =>
	(action) =>
	{
		if (!isNextFunction(next)) {
			return;
		}
		
		if (!isActionObject(action)) {
			return next(action);
		}
		
		const deferredActions: (ActionData | FunctionActionData)[] = [];
		
		if (action.type === ActionType.createIndex) {
			const index = (action as CreateIndexActionData).payload.index;
			deferredActions.push(refreshGeneratedNames('indexes', index.id));
		}
		else if (action.type === ActionType.alterIndexProperty) {
			const alteration = (action as AlterIndexPropertyActionData).payload;
			const { indexes, tables, columns } = getState().schema;
			const index = indexes[alteration.id];
			
			if (alteration.propertyName === 'name') {
				const newName = alteration.value as IndexData['name'];
				const indexExists = Object.values(indexes).filter(x => x.name === newName && x.id !== alteration.id).length > 0;
				
				if (indexExists) {
					const errorAction = dispatch(errorNameAlreadyExists(newName, ModelObjectType.index, alteration.id, action.errorKey));
					(action.errors = action.errors || []).push(errorAction.payload.data);
				}
			}
			
			if (alteration.propertyName === 'tableId') {
				const newTableId = alteration.value as IndexData['tableId'];
				if (!(newTableId in tables)) {
					const errorAction = dispatch(errorSchemaItemNotExists(ModelObjectType.index, alteration.id, ModelObjectType.table, newTableId, action.errorKey));
					(action.errors = action.errors || []).push(errorAction.payload.data);
				}
				else {
					deferredActions.push(refreshGeneratedNames('indexes', index.id));
				}
			}
			
			if (alteration.propertyName === 'columnIds') {
				const columnIds = alteration.value as IndexData['columnIds'];
				if (columnIds.length === 0) {
					const errorAction = dispatch(errorNoColumns(ModelObjectType.index, alteration.id, action.errorKey));
					(action.errors = action.errors || []).push(errorAction.payload.data);
					// no return; the error will be set, but it should be allowed to have an empty column list
				}
				else {
					for (const id of columnIds) {
						if (!(id in columns)) {
							const errorAction = dispatch(errorSchemaItemNotExists(ModelObjectType.index, alteration.id, ModelObjectType.column, id, action.errorKey));
							(action.errors = action.errors || []).push(errorAction.payload.data);
						}
					}
					
					deferredActions.push(refreshGeneratedNames('indexes', index.id));
				}
			}
		}
		else if (action.type === ActionType.deleteIndex) {
			const { id } = (action as DeleteIndexActionData).payload;
			
			const { schema: { indexes } } = getState();
			
			if (!(id in indexes)) {
				const errorAction = dispatch(errorSchemaItemNotExists(ModelObjectType.index, id, ModelObjectType.index, id, action.errorKey));
				(action.errors = action.errors || []).push(errorAction.payload.data);
				return action;
			}
		}
		
		const nextAction = next(action);
		
		deferredActions.forEach(a => dispatch(a));
		
		if (!nextAction?.errors || nextAction.errors.length === 0) {
			// only try to apply to mixin children if there are no errors here
			applyToMixinChildren(getState(), dispatch, action);
		}
		
		return nextAction;
	}

function applyToMixinChildren(state: RootState, dispatch: Dispatch<ActionData>, action: ActionData) {
	if (action.type === ActionType.createIndex) {
		const payload = (action as CreateIndexActionData).payload;
		const index = payload.index;
		const mixinCloneTables = getMixinTableClones(state, index.tableId);
		mixinCloneTables.forEach(t => {
			const mixinCloneColumns = index.columnIds.map(id => getMixinColumnClones(state, id, t.id)).flatMap(x => x);
			const newPayload: CreateIndexActionData['payload'] = {
				...payload,
				index: {
					...index,
					id: generateId(IdPrefix.index),
					tableId: t.id,
					columnIds: mixinCloneColumns.map(c => c.id),
					belongsToMixinSchemaItem: index.id,
					belongsToMixinTable: index.tableId,
				},
			};
			
			dispatch({
				...action,
				payload: newPayload,
			});
		});
	}
	else if (action.type === ActionType.deleteIndex) {
		const payload = (action as DeleteIndexActionData).payload;
		const mixinCloneIndexes = getMixinIndexClones(state, payload.id);
		mixinCloneIndexes.forEach(ix => {
			const newPayload: DeleteIndexActionData['payload'] = {
				...payload,
				id: ix.id,
			};
			
			dispatch({
				...action,
				payload: newPayload,
			});
		});
	}
	else if (action.type === ActionType.alterIndexProperty) {
		const payload = (action as AlterIndexPropertyActionData).payload;
		const mixinCloneIndexes = getMixinIndexClones(state, payload.id);
		mixinCloneIndexes.forEach(ix => {
			const newPayload: AlterIndexPropertyActionData['payload'] = {
				...payload,
				id: ix.id,
			};
			
			if (newPayload.propertyName === 'columnIds') {
				const newColumnIds: string[] = payload.value;
				const mixinCloneColumns = newColumnIds.map(id => getMixinColumnClones(state, id, ix.tableId)).flatMap(x => x);
				newPayload.value = mixinCloneColumns.map(c => c.id);
			}
			
			dispatch({
				...action,
				payload: newPayload,
			});
		});
	}
}
