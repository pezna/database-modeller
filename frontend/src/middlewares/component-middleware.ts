import { createError, deselectBoard } from "@/actions";
import { ActionData, ActionType, AlterComponentPropertyActionData, ComponentType, CreateComponentActionData, DeleteComponentActionData, LSystemComponentData, ModelItemErrorData, ModelObjectType, RootState } from "@/data";
import { getComponent, getIsComponentSelected, getSelectedMachineId } from "@/selectors";
import { diagramService } from "@/services";
import { isActionObject, isNextFunction } from "@/utils";
import { Dispatch, Middleware } from "redux";

export const componentMiddleware: Middleware<{}, RootState, Dispatch<ActionData>> =
	({ getState, dispatch }) =>
	(next) =>
	(action) =>
	{
		if (!isNextFunction(next)) {
			return;
		}
		
		if (!isActionObject(action)) {
			return next(action);
		}
		
		const deferredActions: ActionData[] = [];
		
		if (action.type === ActionType.createComponent) {
			const { component } = (action as CreateComponentActionData).payload;
			
			if (!component.machineId) {
				const machineId = getSelectedMachineId(getState());
				component.machineId = machineId!;
			}
		}
		else if (action.type === ActionType.alterComponentProperty) {
			const act = (action as AlterComponentPropertyActionData<any>);
			const comp = getComponent(getState(), act.payload.componentId);
			if (comp.type === ComponentType.lsystem) {
				handleAlterLSystem(act as AlterComponentPropertyActionData<LSystemComponentData>, dispatch);
			}
		}
		else if (action.type === ActionType.deleteComponent) {
			const { id } = (action as DeleteComponentActionData).payload;
			
			if (getIsComponentSelected(getState(), id)) {
				dispatch(deselectBoard());
			}
			
			diagramService.deleteItem(id);
		}
		
		const nextAction = next(action);
		
		deferredActions.forEach(a => dispatch(a));
		
		return nextAction;
	};

function handleAlterLSystem(action: AlterComponentPropertyActionData<LSystemComponentData>, dispatch: Dispatch<ActionData>) {
	const { componentId, propertyName, value } = action.payload;
	const potentialError: ModelItemErrorData = {
		errorKey: action.errorKey!,
		schemaType: ModelObjectType.component,
		schemaId: componentId,
		message: "",
	};
	
	if (propertyName === "maxIterations") {
		const num = Number(value);
		if (Number.isNaN(num) || Math.floor(num) !== num) {
			potentialError.message = "Must be an integer";
			dispatch(createError(action.errorKey!, "modelErrors", potentialError));
		}
	}
	else if (propertyName === "initialDirection" || propertyName === "stepDistance" || propertyName === "rotation") {
		if (Number.isNaN(Number(value))) {
			potentialError.message = "Must be numeric";
			dispatch(createError(action.errorKey!, "modelErrors", potentialError));
		}
	}
}