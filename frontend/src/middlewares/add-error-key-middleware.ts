import { ActionData, RootState } from "@/data";
import { isActionObject, isNextFunction } from "@/utils";
import { Dispatch, Middleware } from "redux";

export const addErrorKeyMiddleware: Middleware<{}, RootState, Dispatch<ActionData>> =
	({ getState, dispatch }) =>
	(next) =>
	(action) =>
	{
		if (!isNextFunction(next)) {
			return;
		}
		
		if (!isActionObject(action)) {
			return next(action);
		}
		
		if (!action.errorKey) {
			action.errorKey = `err_${Date.now()}_${Math.random().toString().substring(2)}`;
		}
		
		return next(action);
	};
