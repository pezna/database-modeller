import { alterColumnOptionProperty, alterSchemaSettingsProperty, alterTableProperty, createColumn, createIndex, createPrimaryKey, createRelationship, createUnique, deleteColumn, deleteIndex, deletePrimaryKey, deleteRelationship, deleteUnique, deselectBoard, errorNameAlreadyExists, errorSchemaItemNotExists, refreshGeneratedNames, removeManageSchemaOpenAccordion } from "@/actions";
import { ActionData, ActionType, AlterTablePropertyActionData, ColumnData, ColumnOptionType, CreateTableActionData, DeleteTableActionData, FunctionActionData, IndexData, ModelObjectType, PrimaryKeyData, RelationshipData, RootState, TableData, UniqueData } from "@/data";
import { getColumn, getDefaultMixinTableIds, getIndex, getIsTableSelected, getPrimaryKey, getRelationship, getTable, getUnique } from "@/selectors";
import { diagramService } from "@/services";
import { IdPrefix, generateId, isActionObject, isNextFunction } from "@/utils";
import { Dispatch, Middleware } from "redux";

export const tableMiddleware: Middleware<{}, RootState, Dispatch<any>> =
	({ getState, dispatch }) =>
	(next) =>
	(action) =>
	{
		if (!isNextFunction(next)) {
			return;
		}
		
		if (!isActionObject(action)) {
			return next(action);
		}
		
		const deferredActions: (ActionData | FunctionActionData)[] = [];
		
		if (action.type === ActionType.createTable) {
			const { table } = (action as CreateTableActionData).payload;
			const state = getState();
			const defaultMixinTableIds = getDefaultMixinTableIds(state);
			
			if (defaultMixinTableIds.length > 0 && !table.isMixin) {
				const newMixinTableIds = [...table.mixinTableIds];
				newMixinTableIds.splice(table.mixinTableIds.length, 0, ...defaultMixinTableIds);
				deferredActions.push(alterTableProperty(table.id, 'mixinTableIds', newMixinTableIds));
			}
		}
		else if (action.type === ActionType.alterTableProperty) {
			const alteration = (action as AlterTablePropertyActionData).payload;
			const state = getState();
			const { tables } = state.schema;
			const { tableId } = alteration;
			
			if (alteration.propertyName === 'name') {
				const newName = alteration.value as TableData['name'];
				const tableExists = Object.values(tables).filter(x => x.name === newName && x.id !== tableId).length > 0;
				
				if (tableExists) {
					const errorAction = dispatch(errorNameAlreadyExists(newName, ModelObjectType.table, tableId, action.errorKey));
					(action.errors = action.errors || []).push(errorAction.payload.data);
				}
				
				deferredActions.push(refreshGeneratedNames('tables', tableId));
			}
			else if (alteration.propertyName === 'isMixin') {
				const isMixin = alteration.value as TableData['isMixin'];
				if (!isMixin) {
					// table will no longer be mixin, so remove any subclasses and remove it from the default mixin table if it is a default
					const defaultMixinTableIds = getDefaultMixinTableIds(state);
					removeMixinFromDefaultMixins(dispatch, defaultMixinTableIds, tableId);
					removeMixinTableFromOtherTables(dispatch, tables, tableId, action.errorKey);
				}
			}
			else if (alteration.propertyName === 'mixinTableIds') {
				const mixinTableIds = alteration.value as TableData['mixinTableIds'];
				const table = tables[tableId];
				for (const existingMixinId of table.mixinTableIds) {
					if (!mixinTableIds.includes(existingMixinId)) {
						// mixin removed, remove all schema items for it
						deleteMixinItems(existingMixinId, table, state, dispatch, action.errorKey);
					}
				}
				
				for (const mixinId of mixinTableIds) {
					if (!table.mixinTableIds.includes(mixinId)) {
						// mixin added, add all schema items for it
						createMixinItems(mixinId, table, state, dispatch, action.errorKey);
					}
				}
			}
		}
		else if (action.type === ActionType.deleteTable) {
			const { id } = (action as DeleteTableActionData).payload;
			const state = getState();
			const defaultMixinTableIds = getDefaultMixinTableIds(state);
			const { schema: { tables }, board } = state;
			
			if (!(id in tables)) {
				const errorAction = dispatch(errorSchemaItemNotExists(ModelObjectType.table, id, ModelObjectType.table, id, action.errorKey));
				(action.errors = action.errors || []).push(errorAction.payload.data);
				return action;
			}
			
			const table = tables[id];
			
			if (table.isMixin) {
				removeMixinFromDefaultMixins(dispatch, defaultMixinTableIds, table.id);
				removeMixinTableFromOtherTables(dispatch, tables, table.id, action.errorKey);
			}
			
			for (let relationshipId of table.relationshipIds) {
				const ret = dispatch(deleteRelationship(relationshipId));
				if (ret.errors) {
					action.errors = (action.errors || []).concat(ret.errors);
					return action;
				}
			}
			
			if (table.primaryKeyId) {
				const ret = dispatch(deletePrimaryKey(table.primaryKeyId));
				if (ret.errors) {
					action.errors = (action.errors || []).concat(ret.errors);
					return action;
				}
			}
			
			for (let columnId of table.columnIds) {
				const ret = dispatch(deleteColumn(columnId));
				if (ret.errors) {
					action.errors = (action.errors || []).concat(ret.errors);
					return action;
				}
			}
			
			if (getIsTableSelected(state, id)) {
				const ret = dispatch(deselectBoard());
				if (ret.errors) {
					action.errors = (action.errors || []).concat(ret.errors);
					return action;
				}
			}
			
			dispatch(removeManageSchemaOpenAccordion(id, "openTableIds"));
			
			diagramService.deleteItem(id);
		}
		
		const nextAction = next(action);
		
		deferredActions.forEach(a => dispatch(a));
		
		return nextAction;
	};

function deleteMixinItems(mixinTableId: string, table: TableData, state: RootState, dispatch: Dispatch<ActionData>, errorKey?: string) {
	table.relationshipIds.map(id => getRelationship(state, id))
		.forEach((x) => {
			if (x.belongsToMixinTable !== mixinTableId) {
				return;
			}
			
			dispatch(deleteRelationship(x.id, errorKey));
		});
	table.columnIds.map(id => getColumn(state, id))
		.forEach((c) => {
			if (c.belongsToMixinTable !== mixinTableId) {
				return;
			}
			
			dispatch(deleteColumn(c.id, errorKey));
			
			if (c.options.index) {
				c.options.index.indexIds
					.map(id => getIndex(state, id))
					.filter(ix => ix.belongsToMixinTable === mixinTableId)
					.forEach(ix => {
						dispatch(deleteIndex(ix.id, errorKey));
					});
			}
			
			if (c.options.unique) {
				c.options.unique.uniqueIds
					.map(id => getUnique(state, id))
					.filter(uq => uq.belongsToMixinTable === mixinTableId)
					.forEach(uq => {
						dispatch(deleteUnique(uq.id, errorKey));
					});
			}
		});
	if (table.primaryKeyId) {
		const pk = getPrimaryKey(state, table.primaryKeyId);
		if (pk.belongsToMixinTable === mixinTableId) {
			dispatch(deletePrimaryKey(table.primaryKeyId, errorKey));
		}
	}
}

function createMixinItems(mixinTableId: string, table: TableData, state: RootState, dispatch: Dispatch<ActionData>, errorKey?: string) {
	const mixinTable = getTable(state, mixinTableId);
	const mixinColumns = mixinTable.columnIds.map(id => getColumn(state, id));
	const colIdMap: Record<string, string> = {};
	mixinColumns.forEach((col, i) => {
		colIdMap[col.id] = generateId(IdPrefix.column);
	});
	const newRelationshipIds: string[] = [];
	
	// add columns and their indexes and uniques
	mixinColumns.forEach((mixCol) => {
		const col: ColumnData = {
			...mixCol,
			id: colIdMap[mixCol.id],
			tableId: table.id,
			belongsToMixinTable: mixinTableId,
			belongsToMixinSchemaItem: mixCol.id,
		};
		dispatch(createColumn(col, errorKey));
		
		if (mixCol.options.index) {
			// delete the mixCol index from the new column so that we don't end up with multiple of the same index
			dispatch(alterColumnOptionProperty(col.id, ColumnOptionType.index, undefined, errorKey));
			
			mixCol.options.index.indexIds
				.map(id => getIndex(state, id))
				.forEach(ix => {
					const index: IndexData = {
						...ix,
						id: generateId(IdPrefix.index),
						tableId: table.id,
						columnIds: [...ix.columnIds.map(id => colIdMap[id])],
						belongsToMixinTable: mixinTableId,
						belongsToMixinSchemaItem: ix.id,
					};
					dispatch(createIndex(index, errorKey));
				});
		}
		
		if (mixCol.options.unique) {
			// delete the mixCol unique from the new column so that we don't end up with multiple of the same unique
			dispatch(alterColumnOptionProperty(col.id, ColumnOptionType.unique, undefined, errorKey));
			
			mixCol.options.unique.uniqueIds
				.map(id => getUnique(state, id))
				.forEach(uq => {
					const unique: UniqueData = {
						...uq,
						id: generateId(IdPrefix.unique),
						tableId: table.id,
						columnIds: [...uq.columnIds.map(id => colIdMap[id])],
						belongsToMixinTable: mixinTableId,
						belongsToMixinSchemaItem: uq.id,
					};
					dispatch(createUnique(unique, errorKey));
				});
		}
	});
	
	mixinTable.relationshipIds.map(id => getRelationship(state, id))
		.forEach(r => {
			const rel: RelationshipData = {
				...r,
				id: generateId(IdPrefix.relationship),
				childTableId: table.id,
				childColumnIds: [...r.childColumnIds.map(id => colIdMap[id])],
				belongsToMixinTable: mixinTableId,
				belongsToMixinSchemaItem: r.id,
			};
			dispatch(createRelationship(rel, errorKey));
			newRelationshipIds.push(rel.id);
		});
	if (mixinTable.primaryKeyId) {
		const pk = getPrimaryKey(state, mixinTable.primaryKeyId);
		const newPk: PrimaryKeyData = {
			...pk,
			id: generateId(IdPrefix.primaryKey),
			tableId: table.id,
			columnIds: [...pk.columnIds.map(id => colIdMap[id])],
			belongsToMixinTable: mixinTableId,
			belongsToMixinSchemaItem: pk.id,
		};
		dispatch(createPrimaryKey(newPk, errorKey));
	}
}

function removeMixinTableFromOtherTables(dispatch: Dispatch<ActionData>, tables: {[id: string]: TableData}, mixinTableId: string, actionErrorKey?: string) {
	for (const otherTable of Object.values(tables)) {
		if (otherTable.id === mixinTableId) {
			continue;
		}
		
		const mixinIndex = otherTable.mixinTableIds.indexOf(mixinTableId);
		if (mixinIndex > -1) {
			const newMixinIds = [...otherTable.mixinTableIds];
			newMixinIds.splice(mixinIndex, 1);
			dispatch(alterTableProperty(otherTable.id, 'mixinTableIds', newMixinIds, actionErrorKey));
		}
	}
}

function removeMixinFromDefaultMixins(dispatch: Dispatch<ActionData>, defaultMixinTableIds: string[], mixinTableId: string) {
	const defaultIndex = defaultMixinTableIds.indexOf(mixinTableId);
	if (defaultIndex > -1) {
		const newDefaultMixinTableIds = [...defaultMixinTableIds];
		newDefaultMixinTableIds.splice(defaultIndex, 1);
		
		dispatch(alterSchemaSettingsProperty(["schema", "mixinTables", "defaultMixinTableIds"], newDefaultMixinTableIds));
	}
}
