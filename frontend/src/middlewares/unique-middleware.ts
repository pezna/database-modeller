import { errorNameAlreadyExists, errorNoColumns, errorSchemaItemNotExists, refreshGeneratedNames } from "@/actions";
import { ActionData, ActionType, AlterUniquePropertyActionData, CreateUniqueActionData, DeleteUniqueActionData, FunctionActionData, ModelObjectType, RootState, UniqueData } from "@/data";
import { getMixinColumnClones, getMixinTableClones, getMixinUniqueClones } from "@/selectors";
import { IdPrefix, generateId, isActionObject, isNextFunction } from "@/utils";
import { Dispatch, Middleware } from "redux";

export const uniqueMiddleware: Middleware<{}, RootState, Dispatch<any>> =
	({ getState, dispatch }) =>
	(next) =>
	(action) =>
	{
		if (!isNextFunction(next)) {
			return;
		}
		
		if (!isActionObject(action)) {
			return next(action);
		}
		
		const deferredActions: (ActionData | FunctionActionData)[] = [];
		
		if (action.type === ActionType.createUnique) {
			const unique = (action as CreateUniqueActionData).payload.unique;
			deferredActions.push(refreshGeneratedNames('uniques', unique.id));
		}
		else if (action.type === ActionType.alterUniqueProperty) {
			const alteration = (action as AlterUniquePropertyActionData).payload;
			const { uniques, tables, columns } = getState().schema;
			const unique = uniques[alteration.id];
			
			// make sure new name isn't already in use
			if (alteration.propertyName === 'name') {
				const newName = alteration.value as UniqueData['name'];
				const uniqueExists = Object.values(uniques).filter(x => x.name === newName && x.id !== alteration.id).length > 0;
				
				if (uniqueExists) {
					const errorAction = dispatch(errorNameAlreadyExists(newName, ModelObjectType.unique, alteration.id, action.errorKey));
					(action.errors = action.errors || []).push(errorAction.payload.data);
				}
			}
			
			// make sure table exists
			if (alteration.propertyName === 'tableId') {
				const newTableId = alteration.value as UniqueData['tableId'];
				if (!(newTableId in tables)) {
					const errorAction = dispatch(errorSchemaItemNotExists(ModelObjectType.unique, alteration.id, ModelObjectType.table, newTableId, action.errorKey));
					(action.errors = action.errors || []).push(errorAction.payload.data);
				}
				else {
					// generate and set the unique name
					deferredActions.push(refreshGeneratedNames('uniques', unique.id));
				}
			}
			
			// make sure columns exist
			if (alteration.propertyName === 'columnIds') {
				const columnIds = alteration.value as UniqueData['columnIds'];
				if (columnIds.length === 0) {
					const errorAction = dispatch(errorNoColumns(ModelObjectType.unique, alteration.id, action.errorKey));
					(action.errors = action.errors || []).push(errorAction.payload.data);
					// no return; the error will be set, but it should be allowed to have an empty column list
				}
				else {
					for (let id of columnIds) {
						if (!(id in columns)) {
							const errorAction = dispatch(errorSchemaItemNotExists(ModelObjectType.unique, alteration.id, ModelObjectType.column, id, action.errorKey));
							(action.errors = action.errors || []).push(errorAction.payload.data);
						}
					}
					
					deferredActions.push(refreshGeneratedNames('uniques', unique.id));
				}
			}
		}
		else if (action.type === ActionType.deleteUnique) {
			const { id } = (action as DeleteUniqueActionData).payload;
			
			const { schema: { uniques } } = getState();
			
			if (!(id in uniques)) {
				const errorAction = dispatch(errorSchemaItemNotExists(ModelObjectType.unique, id, ModelObjectType.unique, id, action.errorKey));
				(action.errors = action.errors || []).push(errorAction.payload.data);
				return action;
			}
		}
		
		const nextAction = next(action);
		
		deferredActions.forEach(a => dispatch(a));
		
		if (!nextAction?.errors || nextAction.errors.length === 0) {
			// only try to apply to mixin children if there are no errors here
			applyToMixinChildren(getState(), dispatch, action);
		}
		
		return nextAction;
	}

function applyToMixinChildren(state: RootState, dispatch: Dispatch<ActionData>, action: ActionData) {
	if (action.type === ActionType.createUnique) {
		const payload = (action as CreateUniqueActionData).payload;
		const unique = payload.unique;
		const mixinCloneTables = getMixinTableClones(state, unique.tableId);
		mixinCloneTables.forEach(t => {
			const mixinCloneColumns = unique.columnIds.map(id => getMixinColumnClones(state, id, t.id)).flatMap(x => x);
			const newPayload: CreateUniqueActionData['payload'] = {
				...payload,
				unique: {
					...unique,
					id: generateId(IdPrefix.unique),
					tableId: t.id,
					columnIds: mixinCloneColumns.map(c => c.id),
					belongsToMixinSchemaItem: unique.id,
					belongsToMixinTable: unique.tableId,
				},
			};
			
			dispatch({
				...action,
				payload: newPayload,
			});
		});
	}
	else if (action.type === ActionType.deleteUnique) {
		const payload = (action as DeleteUniqueActionData).payload;
		const mixinCloneUniques = getMixinUniqueClones(state, payload.id);
		mixinCloneUniques.forEach(uq => {
			const newPayload: DeleteUniqueActionData['payload'] = {
				...payload,
				id: uq.id,
			};
			
			dispatch({
				...action,
				payload: newPayload,
			});
		});
	}
	else if (action.type === ActionType.alterUniqueProperty) {
		const payload = (action as AlterUniquePropertyActionData).payload;
		const mixinCloneUniques = getMixinUniqueClones(state, payload.id);
		mixinCloneUniques.forEach(uq => {
			const newPayload: AlterUniquePropertyActionData['payload'] = {
				...payload,
				id: uq.id,
			};
			
			if (newPayload.propertyName === 'columnIds') {
				const newColumnIds: string[] = payload.value;
				const mixinCloneColumns = newColumnIds.map(id => getMixinColumnClones(state, id, uq.tableId)).flatMap(x => x);
				newPayload.value = mixinCloneColumns.map(c => c.id);
			}
			
			dispatch({
				...action,
				payload: newPayload,
			});
		});
	}
}