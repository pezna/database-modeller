import { setIsUnsaved } from "@/actions";
import { ActionData, actionsThatTriggerUnsaved, ActionType, RootState, SettingsState, UiState } from "@/data";
import { getIsUnsaved, getModelFilePath, getProjectId } from "@/selectors";
import { fileService } from "@/services";
import { isActionObject, isNextFunction } from "@/utils";
import { Dispatch, Middleware } from "redux";
import { debounce } from "throttle-debounce";

const debounceSaveSettings = debounce(500, async (settings: SettingsState) => {
	await fileService.saveAppSettings(settings);
});
const debounceSaveUiState = debounce(500, async (ui: UiState) => {
	await fileService.updateUiState(ui);
});

export const backendMiddleware: Middleware<{}, RootState, Dispatch<ActionData>> =
	({ getState, dispatch }) =>
	(next) =>
	(action) =>
	{
		if (!isNextFunction(next)) {
			return;
		}
		
		if (!isActionObject(action)) {
			return next(action);
		}
		
		// before continuing the action, check is it should trigger unsaved
		if (actionsThatTriggerUnsaved[action.type]) {
			const isUnsaved = getIsUnsaved(getState());
			if (!isUnsaved) {
				dispatch(setIsUnsaved(true));
			}
		}
		
		// complete the action first
		const nextAction = next(action);
		
		// to make sure that this state is the newest one
		const state = getState();
		
		if (action.type === ActionType.alterSettingsProperty) {
			debounceSaveSettings(state.settings);
		}
		else if (
			action.type === ActionType.alterUiStateProperty ||
			action.type === ActionType.setGeneralState
		) {
			const projectId = getProjectId(state);
			const modelFilePath = getModelFilePath(state);
			const isUnsaved = getIsUnsaved(state);
			// Only save ui state when the model has a saved file.
			// This will prevent ui state files from being made each time the app is opened.
			if (!isUnsaved && modelFilePath && projectId) {
				debounceSaveUiState(state.ui);
			}
		}
		
		return nextAction;
	};
