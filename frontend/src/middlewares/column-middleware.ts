import { deleteIndex, deletePrimaryKey, deleteRelationship, deleteUnique, errorNameAlreadyExists, errorSchemaItemNotExists, refreshGeneratedNames, selectTable } from "@/actions";
import { ActionData, ActionType, AlterColumnOptionPropertyActionData, AlterColumnPropertyActionData, ColumnData, ColumnOptionType, ColumnOptionValueContainer, CreateColumnActionData, DeleteColumnActionData, FunctionActionData, ModelObjectType, RootState } from "@/data";
import { getIsColumnSelected, getMixinColumnClones, getMixinTableClones, getTableColumns } from "@/selectors";
import { diagramService } from "@/services";
import { IdPrefix, generateId, isActionObject, isNextFunction } from "@/utils";
import { Dispatch, Middleware } from "redux";

export const columnMiddleware: Middleware<{}, RootState, Dispatch<any>> =
	({ getState, dispatch }) =>
	(next) =>
	(action) =>
	{
		if (!isNextFunction(next)) {
			return;
		}
		
		if (!isActionObject(action)) {
			return next(action);
		}
		
		const deferredActions: (ActionData | FunctionActionData)[] = [];
		
		if (action.type === ActionType.alterColumnProperty) {
			const alteration = (action as AlterColumnPropertyActionData).payload;
			const { tables, columns } = getState().schema;
			const column = columns[alteration.columnId];
			const table = tables[column.tableId];
			
			if (alteration.propertyName === 'name') {
				const newName = alteration.value as ColumnData['name'];
				const tableColumns = getTableColumns(getState(), table.id);
				const columnExists = tableColumns.filter((c) => c.name === newName && c.id !== alteration.columnId).length > 0;
				
				if (columnExists) {
					const errorAction = dispatch(errorNameAlreadyExists(newName, ModelObjectType.column, alteration.columnId, action.errorKey));
					(action.errors = action.errors || []).push(errorAction.payload.data);
				}
				
				deferredActions.push(refreshGeneratedNames('columns', column.id));
			}
		}
		else if (action.type === ActionType.deleteColumn) {
			const { columnId } = (action as DeleteColumnActionData).payload;
			const { schema: { tables, relationships, columns, primaryKeys }, board } = getState();
			
			if (!(columnId in columns)) {
				const errorAction = dispatch(errorSchemaItemNotExists(ModelObjectType.column, columnId, ModelObjectType.column, columnId, action.errorKey));
				(action.errors = action.errors || []).push(errorAction.payload.data);
				return action;
			}
			
			const column = columns[columnId];
			const table = tables[column.tableId];
			
			// delete all relationships that use this column
			for (const relationshipId of table.relationshipIds) {
				const relationship = relationships[relationshipId];
				if (relationship.childColumnIds.includes(columnId) || relationship.parentColumnIds.includes(columnId)) {
					const ret = dispatch(deleteRelationship(relationshipId));
					if (ret.errors) {
						action.errors = (action.errors || []).concat(ret.errors);
						return ret;
					}
				}
			}
			
			// delete any primary key that uses this column
			if (table.primaryKeyId) {
				const primaryKey = primaryKeys[table.primaryKeyId];
				if (primaryKey.columnIds.includes(columnId)) {
					const ret = dispatch(deletePrimaryKey(table.primaryKeyId));
					if (ret.errors) {
						action.errors = (action.errors || []).concat(ret.errors);
						return ret;
					}
				}
			}
			
			// delete all indexes and uniques that use this column
			for (const [k, value] of Object.entries(column.options)) {
				const key = k as keyof ColumnOptionValueContainer;
				if (key === ColumnOptionType.index) {
					const option = value as ColumnOptionValueContainer['index'];
					if (option && option.indexIds) {
						for (const indexId of option.indexIds) {
							const ret = dispatch(deleteIndex(indexId));
							if (ret.errors) {
								action.errors = (action.errors || []).concat(ret.errors);
								return ret;
							}
						}
					}
				}
				else if (key === ColumnOptionType.unique) {
					const option = value as ColumnOptionValueContainer['unique'];
					if (option && option.uniqueIds) {
						for (const uniqueId of option.uniqueIds) {
							const ret = dispatch(deleteUnique(uniqueId));
							if (ret.errors) {
								action.errors = (action.errors || []).concat(ret.errors);
								return ret;
							}
						}
					}
				}
			}
			
			if (getIsColumnSelected(getState(), columnId)) {
				const ret = dispatch(selectTable(table.id));
				if (ret.errors) {
					action.errors = (action.errors || []).concat(ret.errors);
					return action;
				}
			}
			
			//deferredActions.push(runSqlSchemaUseFinderForColumn(columnId) as any);
			
			diagramService.deleteItem(columnId);
		}
		
		const nextAction = next(action);
		
		deferredActions.forEach(a => dispatch(a));
		
		if (!nextAction?.errors || nextAction.errors.length === 0) {
			// only try to apply to mixin children if there are no errors here
			applyToMixinChildren(getState(), dispatch, action);
		}
		
		return nextAction;
	};

function applyToMixinChildren(state: RootState, dispatch: Dispatch<ActionData>, action: ActionData) {
	if (action.type === ActionType.createColumn) {
		const payload = (action as CreateColumnActionData).payload;
		const column = payload.column;
		const mixinCloneTables = getMixinTableClones(state, column.tableId);
		mixinCloneTables.forEach(t => {
			const newPayload: CreateColumnActionData['payload'] = {
				...payload,
				column: {
					...column,
					id: generateId(IdPrefix.column),
					tableId: t.id,
					belongsToMixinSchemaItem: column.id,
					belongsToMixinTable: column.tableId,
				},
			};
			
			dispatch({
				...action,
				payload: newPayload,
			});
		});
	}
	else if (action.type === ActionType.deleteColumn) {
		const payload = (action as DeleteColumnActionData).payload;
		const mixinCloneColumns = getMixinColumnClones(state, payload.columnId);
		mixinCloneColumns.forEach(c => {
			const newPayload: DeleteColumnActionData['payload'] = {
				...payload,
				columnId: c.id,
			};
			
			dispatch({
				...action,
				payload: newPayload,
			});
		});
	}
	else if (action.type === ActionType.alterColumnProperty) {
		const payload = (action as AlterColumnPropertyActionData).payload;
		const mixinCloneColumns = getMixinColumnClones(state, payload.columnId);
		mixinCloneColumns.forEach(c => {
			const newPayload: AlterColumnPropertyActionData['payload'] = {
				...payload,
				columnId: c.id,
			};
			
			dispatch({
				...action,
				payload: newPayload,
			});
		});
	}
	else if (action.type === ActionType.alterColumnOptionProperty) {
		const payload = (action as AlterColumnOptionPropertyActionData<ColumnOptionType>).payload;
		const mixinCloneColumns = getMixinColumnClones(state, payload.columnId);
		mixinCloneColumns.forEach(c => {
			const newPayload: AlterColumnOptionPropertyActionData<ColumnOptionType>['payload'] = {
				...payload,
				columnId: c.id,
			};
			
			dispatch({
				...action,
				payload: newPayload,
			});
		});
	}
}
