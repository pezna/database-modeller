import { ActionData, ActionType, AppMode, ModelObjectType, RootState, SelectModelObjectActionData } from "@/data";
import { getColumn, getComponent, getConnector, getNote, getRelationship, getTable } from "@/selectors";
import { isActionObject, isNextFunction } from "@/utils";
import { Dispatch, Middleware } from "redux";

export const uiMiddleware: Middleware<{}, RootState, Dispatch<ActionData>> =
	({ getState }) =>
	(next) =>
	(action) =>
	{
		if (!isNextFunction(next)) {
			return;
		}
		
		if (!isActionObject(action)) {
			return next(action);
		}
		
		if (action.type === ActionType.selectModelObject) {
			const selectAction = (action as SelectModelObjectActionData);
			const { modelType, id, otherIds } = selectAction.payload;
			const state = getState();
			
			switch (modelType) {
				case ModelObjectType.component:
					if (!otherIds.machineId) {
						const component = getComponent(state, id!);
						otherIds.machineId = component.machineId;
					}
					break;
				case ModelObjectType.connector:
					if (!otherIds.machineId) {
						const connector = getConnector(state, id!);
						otherIds.machineId = connector.machineId;
					}
					break;
				case ModelObjectType.column:
					if (!otherIds.namespaceId) {
						const column = getColumn(state, id!);
						const table = getTable(state, column.tableId);
						otherIds.namespaceId = table.namespaceId;
					}
					break;
				case ModelObjectType.relationship:
					if (!otherIds.namespaceId) {
						const rel = getRelationship(state, id!);
						const table = getTable(state, rel.childTableId);
						otherIds.namespaceId = table.namespaceId;
					}
					break;
				case ModelObjectType.table:
					if (!otherIds.namespaceId) {
						const table = getTable(state, id!);
						otherIds.namespaceId = table.namespaceId;
					}
					break;
				case ModelObjectType.note:
					const note = getNote(state, id!);
					if (note.appMode === AppMode.database) {
						if (!otherIds.namespaceId) {
							otherIds.namespaceId = note.sectionId;
						}
					}
					else if (note.appMode === AppMode.machine) {
						if (!otherIds.machineId) {
							otherIds.machineId = note.sectionId;
						}
					}
					break;
			}
		}
		
		return next(action);
	};
