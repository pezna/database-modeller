import { ActionData, ActionType, CodeContainerType, CustomExport, DeleteCustomExportActionData, RootState, SetCustomExportActionData, SetExportStateActionData } from "@/data";
import { codeService, updateBackendCode } from "@/services";
import { isActionObject, isNextFunction } from "@/utils";
import { Dispatch, Middleware } from "redux";

export const exportMiddleware: Middleware<{}, RootState, Dispatch<ActionData>> =
	({ getState, dispatch }) =>
	(next) =>
	(action) =>
	{
		if (!isNextFunction(next)) {
			return;
		}
		
		if (!isActionObject(action)) {
			return next(action);
		}
		
		const deferredActions: ActionData[] = [];
		
		if (action.type === ActionType.deleteCustomExport) {
			const { id } = (action as DeleteCustomExportActionData).payload;
			
			codeService.deleteExportCodeContainers(id);
		}
		else if (action.type === ActionType.setCustomExport) {
			const { data } = (action as SetCustomExportActionData).payload;
			
			updateExportBackendCode(dispatch, data);
		}
		else if (action.type === ActionType.setExportState) {
			const { data } = (action as SetExportStateActionData).payload;
			
			for (const exp of Object.values(data.customExports)) {
				updateExportBackendCode(dispatch, exp);
			}
		}
		
		const nextAction = next(action);
		
		deferredActions.forEach(a => dispatch(a));
		
		return nextAction;
	};

async function updateExportBackendCode(dispatch: Dispatch, exportData: CustomExport) {
	const codeIds: string[] = [];
	const promises = [];
		
	for (const exp of exportData.tableExports) {
		codeIds.push(exp.id);
		
		promises.push(updateBackendCode(dispatch, false, {
			id: exp.id,
			code: exp.code,
			type: CodeContainerType.perTableGenerator,
		}));
	}
	
	for (const exp of exportData.fullSchemaExports) {
		codeIds.push(exp.id);
		
		promises.push(updateBackendCode(dispatch, false, {
			id: exp.id,
			code: exp.code,
			type: CodeContainerType.perSchemaGenerator,
		}));
	}
	
	await Promise.all(promises);
	
	codeService.setExportCodeContainerIds({
		exportId: exportData.id,
		codeContainerIds: codeIds,
	});
}