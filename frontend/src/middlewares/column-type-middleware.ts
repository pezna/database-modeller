import { removeManageSchemaOpenAccordion } from "@/actions";
import { ActionData, ActionType, DeleteColumnTypeActionData, RootState } from "@/data";
import { isActionObject, isNextFunction } from "@/utils";
import { Dispatch, Middleware } from "redux";

export const columnTypeMiddleware: Middleware<{}, RootState, Dispatch<any>> =
	({ getState, dispatch }) =>
	(next) =>
	(action) =>
	{
		if (!isNextFunction(next)) {
			return;
		}
		
		if (!isActionObject(action)) {
			return next(action);
		}
		
		const deferredActions: ActionData[] = [];
		
		if (action.type === ActionType.deleteColumnType) {
			const { columnTypeId } = (action as DeleteColumnTypeActionData).payload;
			
			dispatch(removeManageSchemaOpenAccordion(columnTypeId, "openColumnTypeIds"));
		}
		
		const nextAction = next(action);
		
		deferredActions.forEach(a => dispatch(a));
		
		return nextAction;
	};
