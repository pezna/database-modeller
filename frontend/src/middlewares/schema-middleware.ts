import { ActionData, ActionType, AlterSchemaSettingsPropertyActionData, CodeContainerType, RootState, SchemaSettingsData, SetSchemaSettingsActionData } from "@/data";
import { updateBackendCode } from "@/services";
import { ALLOWED_SCHEMA_NAME_CODE_ID, INDEX_CODE_ID, PRIMARY_KEY_CODE_ID, RELATIONSHIP_CODE_ID, UNIQUE_CODE_ID, isActionObject, isNextFunction } from "@/utils";
import { Dispatch, Middleware } from "redux";
import { ThunkDispatch } from "redux-thunk";
import { debounce } from "throttle-debounce";

export const schemaMiddleware: Middleware<{}, RootState, Dispatch<ActionData>> =
	({ getState, dispatch }) =>
	(next) =>
	(action) =>
	{
		if (!isNextFunction(next)) {
			return;
		}
		
		if (!isActionObject(action)) {
			return next(action);
		}
		
		const deferredActions: ActionData[] = [];
		
		if (action.type === ActionType.setSchemaSettings) {
			const payload = (action as SetSchemaSettingsActionData).payload;
			updateBackendSettings(dispatch, payload.settings);
		}
		else if (action.type === ActionType.alterSchemaSettingsProperty) {
			const payload = (action as AlterSchemaSettingsPropertyActionData).payload;
			const path = payload.path.join(".");
			
			if (ALLOWED_SCHEMA_NAME_CODE_ID.has(path)) {
				if (path === RELATIONSHIP_CODE_ID) {
					updateBackendCode(dispatch, true, {
						id: path,
						code: payload.value,
						type: CodeContainerType.relationshipName,
					});
				}
				else {
					updateBackendCode(dispatch, true, {
						id: path,
						code: payload.value,
						type: CodeContainerType.schemaItemName,
					});
				}
			}
		}
		
		const nextAction = next(action);
		deferredActions.forEach(a => dispatch(a));
		return nextAction;
	};

const updateBackendSettings = debounce(100, async (dispatch: ThunkDispatch<RootState, any, ActionData>, settings: SchemaSettingsData) => {
	await Promise.all([
		updateBackendCode(dispatch, true, {
			id: INDEX_CODE_ID,
			code: settings.schema.generatorCode.indexNameGeneratorCode,
			type: CodeContainerType.schemaItemName,
		}),
		updateBackendCode(dispatch, true, {
			id: PRIMARY_KEY_CODE_ID,
			code: settings.schema.generatorCode.primaryKeyNameGeneratorCode,
			type: CodeContainerType.schemaItemName,
		}),
		updateBackendCode(dispatch, true, {
			id: RELATIONSHIP_CODE_ID,
			code: settings.schema.generatorCode.relationshipNameGeneratorCode,
			type: CodeContainerType.relationshipName,
		}),
		updateBackendCode(dispatch, true, {
			id: UNIQUE_CODE_ID,
			code: settings.schema.generatorCode.uniqueNameGeneratorCode,
			type: CodeContainerType.schemaItemName,
		}),
	]);
});
