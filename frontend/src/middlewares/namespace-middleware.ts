import { deleteTable, removeNamespaceTab } from "@/actions";
import { ActionData, ActionType, DeleteNamespaceActionData, RootState } from "@/data";
import { getTables } from "@/selectors";
import { isActionObject, isNextFunction } from "@/utils";
import { Dispatch, Middleware } from "redux";

export const namespaceMiddleware: Middleware<{}, RootState, Dispatch<any>> =
	({ getState, dispatch }) =>
	(next) =>
	(action) =>
	{
		if (!isNextFunction(next)) {
			return;
		}
		
		if (!isActionObject(action)) {
			return next(action);
		}
		
		const deferredActions: ActionData[] = [];
		
		if (action.type === ActionType.deleteNamespace) {
			const { id } = (action as DeleteNamespaceActionData).payload;
			const tables = getTables(getState());
			
			for (const table of Object.values(tables)) {
				if (table.namespaceId === id) {
					dispatch(deleteTable(table.id, action.errorKey));
				}
			}
			
			dispatch(removeNamespaceTab(id));
		}
		
		const nextAction = next(action);
		
		deferredActions.forEach(a => dispatch(a));
		
		return nextAction;
	};
