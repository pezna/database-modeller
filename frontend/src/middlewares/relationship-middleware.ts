import { createError, deselectBoard, errorNameAlreadyExists, errorNoColumns, errorSchemaItemNotExists, refreshGeneratedNames } from "@/actions";
import { ActionData, ActionType, AlterRelationshipPropertyActionData, CreateRelationshipActionData, DeleteRelationshipActionData, FunctionActionData, ModelObjectType, RelationshipData, RootState } from "@/data";
import { getIsRelationshipSelected, getMixinColumnClones, getMixinRelationshipClones, getMixinTableClones } from "@/selectors";
import { IdPrefix, generateId, isActionObject, isNextFunction } from "@/utils";
import { Dispatch, Middleware } from "redux";

export const relationshipMiddleware: Middleware<{}, RootState, Dispatch<any>> =
	({ getState, dispatch }) =>
	(next) =>
	(action) =>
	{
		if (!isNextFunction(next)) {
			return;
		}
		
		if (!isActionObject(action)) {
			return next(action);
		}
		
		const deferredActions: (ActionData | FunctionActionData)[] = [];
		
		if (action.type === ActionType.createRelationship) {
			const relationship = (action as CreateRelationshipActionData).payload.relationship;
			const parentTable = getState().schema.tables[relationship.parentTableId];
			if (parentTable && parentTable.isMixin) {
				// The relationship does not exist yet, so don't set a schema ID
				const error = createError(action.errorKey!, "modelErrors", {
					schemaId: "",
					schemaType: ModelObjectType.relationship,
					message: "Cannot use a mixin table as the parent of a foreign key.",
					errorKey: action.errorKey!,
				});
				dispatch(error);
				(action.errors = action.errors || []).push(error.payload.data);
				return action;
			}
			
			deferredActions.push(refreshGeneratedNames('relationships', relationship.id));
		}
		else if (action.type === ActionType.alterRelationshipProperty) {
			const alteration = (action as AlterRelationshipPropertyActionData).payload;
			const { relationships, tables, columns } = getState().schema;
			const relationship = relationships[alteration.id];
			
			if (alteration.propertyName === 'name') {
				const newName = alteration.value as RelationshipData['name'];
				const relationshipExists = Object.values(relationships).filter(x => x.name === newName && x.id !== alteration.id).length > 0;
				
				if (relationshipExists) {
					const errorAction = dispatch(errorNameAlreadyExists(newName, ModelObjectType.relationship, alteration.id, action.errorKey));
					(action.errors = action.errors || []).push(errorAction.payload.data);
				}
			}
			else if (alteration.propertyName === 'childTableId') {
				const newChildTableId = alteration.value as RelationshipData['childTableId'];
				if (!(newChildTableId in tables)) {
					const errorAction = dispatch(errorSchemaItemNotExists(ModelObjectType.relationship, alteration.id, ModelObjectType.table, newChildTableId, action.errorKey));
					(action.errors = action.errors || []).push(errorAction.payload.data);
				}
				else {
					// generate new relationship name
					deferredActions.push(refreshGeneratedNames('relationships', relationship.id));
				}
			}
			else if (alteration.propertyName === 'parentTableId') {
				const newParentTableId = alteration.value as RelationshipData['parentTableId'];
				if (!(newParentTableId in tables)) {
					const errorAction = dispatch(errorSchemaItemNotExists(ModelObjectType.relationship, alteration.id, ModelObjectType.table, newParentTableId, action.errorKey));
					(action.errors = action.errors || []).push(errorAction.payload.data);
				}
				else {
					// generate new relationship name
					deferredActions.push(refreshGeneratedNames('relationships', relationship.id));
				}
			}
			else if (alteration.propertyName === 'childColumnIds') {
				const newChildColumnIds = alteration.value as RelationshipData['childColumnIds'];
				if (newChildColumnIds.length === 0) {
					const errorAction = dispatch(errorNoColumns(ModelObjectType.relationship, alteration.id, action.errorKey));
					(action.errors = action.errors || []).push(errorAction.payload.data);
					// no return; the error will be set, but it should be allowed to have an empty column list
				}
				else {
					for (let id of newChildColumnIds) {
						if (!(id in columns)) {
							const errorAction = dispatch(errorSchemaItemNotExists(ModelObjectType.relationship, alteration.id, ModelObjectType.column, id, action.errorKey));
							(action.errors = action.errors || []).push(errorAction.payload.data);
						}
					}
					deferredActions.push(refreshGeneratedNames('relationships', relationship.id));
				}
			}
			else if (alteration.propertyName === 'parentColumnIds') {
				const newParentColumnIds = alteration.value as RelationshipData['parentColumnIds'];
				if (newParentColumnIds.length === 0) {
					const errorAction = dispatch(errorNoColumns(ModelObjectType.relationship, alteration.id, action.errorKey));
					(action.errors = action.errors || []).push(errorAction.payload.data);
					// no return; the error will be set, but it should be allowed to have an empty column list
				}
				else {
					for (let id of newParentColumnIds) {
						if (!(id in columns)) {
							const errorAction = dispatch(errorSchemaItemNotExists(ModelObjectType.relationship, alteration.id, ModelObjectType.column, id, action.errorKey));
							(action.errors = action.errors || []).push(errorAction.payload.data);
						}
					}
					deferredActions.push(refreshGeneratedNames('relationships', relationship.id));
				}
			}
		}
		else if (action.type === ActionType.deleteRelationship) {
			const { relationshipId } = (action as DeleteRelationshipActionData).payload;
			
			const { schema: { relationships } } = getState();
			
			if (!(relationshipId in relationships)) {
				const errorAction = dispatch(errorSchemaItemNotExists(ModelObjectType.relationship, relationshipId, ModelObjectType.relationship, relationshipId, action.errorKey));
				(action.errors = action.errors || []).push(errorAction.payload.data);
				return action;
			}
			
			if (getIsRelationshipSelected(getState(), relationshipId)) {
				const ret = dispatch(deselectBoard());
				if (ret.errors) {
					action.errors = (action.errors || []).concat(ret.errors);
					return action;
				}
			}
		}
		
		const nextAction = next(action);
		
		deferredActions.forEach(a => dispatch(a));
		
		if (!nextAction?.errors || nextAction.errors.length === 0) {
			// only try to apply to mixin children if there are no errors here
			applyToMixinChildren(getState(), dispatch, action);
		}
		
		return nextAction;
	}

function applyToMixinChildren(state: RootState, dispatch: Dispatch<ActionData>, action: ActionData) {
	if (action.type === ActionType.createRelationship) {
		const payload = (action as CreateRelationshipActionData).payload;
		const relationship = payload.relationship;
		const mixinCloneTables = getMixinTableClones(state, relationship.childTableId);
		mixinCloneTables.forEach(t => {
			const mixinCloneColumns = relationship.childColumnIds.map(id => getMixinColumnClones(state, id, t.id)).flatMap(x => x);
			const newPayload: CreateRelationshipActionData['payload'] = {
				...payload,
				relationship: {
					...relationship,
					id: generateId(IdPrefix.relationship),
					childTableId: t.id,
					childColumnIds: mixinCloneColumns.map(c => c.id),
					belongsToMixinSchemaItem: relationship.id,
					belongsToMixinTable: relationship.childTableId,
				},
			};
			
			dispatch({
				...action,
				payload: newPayload,
			});
		});
	}
	else if (action.type === ActionType.deleteRelationship) {
		const payload = (action as DeleteRelationshipActionData).payload;
		const mixinCloneRelationships = getMixinRelationshipClones(state, payload.relationshipId);
		mixinCloneRelationships.forEach(rel => {
			const newPayload: DeleteRelationshipActionData['payload'] = {
				...payload,
				relationshipId: rel.id,
			};
			
			dispatch({
				...action,
				payload: newPayload,
			});
		});
	}
	else if (action.type === ActionType.alterRelationshipProperty) {
		const payload = (action as AlterRelationshipPropertyActionData).payload;
		const mixinCloneRelationships = getMixinRelationshipClones(state, payload.id);
		mixinCloneRelationships.forEach(rel => {
			const newPayload: AlterRelationshipPropertyActionData['payload'] = {
				...payload,
				id: rel.id,
			};
			
			if (newPayload.propertyName === 'childColumnIds') {
				const newColumnIds: string[] = payload.value;
				const mixinCloneColumns = newColumnIds.map(id => getMixinColumnClones(state, id, rel.childTableId)).flatMap(x => x);
				newPayload.value = mixinCloneColumns.map(c => c.id);
			}
			
			dispatch({
				...action,
				payload: newPayload,
			});
		});
	}
}
