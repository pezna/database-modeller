import { removeMachineTab, removeManageSchemaOpenAccordion } from "@/actions";
import { ActionData, ActionType, DeleteMachineActionData, RootState } from "@/data";
import { isActionObject, isNextFunction } from "@/utils";
import { Dispatch, Middleware } from "redux";

export const machineMiddleware: Middleware<{}, RootState, Dispatch<any>> =
	({ getState, dispatch }) =>
	(next) =>
	(action) =>
	{
		if (!isNextFunction(next)) {
			return;
		}
		
		if (!isActionObject(action)) {
			return next(action);
		}
		
		const deferredActions: ActionData[] = [];
		
		if (action.type === ActionType.deleteMachine) {
			const { id } = (action as DeleteMachineActionData).payload;
			
			dispatch(removeMachineTab(id));
			dispatch(removeManageSchemaOpenAccordion(id, "openMachineIds"));
		}
		
		const nextAction = next(action);
		
		deferredActions.forEach(a => dispatch(a));
		
		return nextAction;
	};
