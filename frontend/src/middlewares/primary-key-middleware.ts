import { deletePrimaryKey, errorNameAlreadyExists, errorNoColumns, errorSchemaItemNotExists, refreshGeneratedNames } from "@/actions";
import { ActionData, ActionType, AlterPrimaryKeyPropertyActionData, CreatePrimaryKeyActionData, DeletePrimaryKeyActionData, FunctionActionData, ModelObjectType, PrimaryKeyData, RootState } from "@/data";
import { getMixinColumnClones, getMixinPrimaryKeyClones, getMixinTableClones } from "@/selectors";
import { IdPrefix, generateId, isActionObject, isNextFunction } from "@/utils";
import { Dispatch, Middleware } from "redux";

export const primaryKeyMiddleware: Middleware<{}, RootState, Dispatch<any>> =
	({ getState, dispatch }) =>
	(next) =>
	(action) =>
	{
		if (!isNextFunction(next)) {
			return;
		}
		
		if (!isActionObject(action)) {
			return next(action);
		}
		
		const deferredActions: (ActionData | FunctionActionData)[] = [];
		
		if (action.type === ActionType.createPrimaryKey) {
			const primaryKey = (action as CreatePrimaryKeyActionData).payload.primaryKey;
			deferredActions.push(refreshGeneratedNames('primaryKeys', primaryKey.id));
		}
		else if (action.type === ActionType.alterPrimaryKeyProperty) {
			const alteration = (action as AlterPrimaryKeyPropertyActionData).payload;
			const { primaryKeys, tables, columns } = getState().schema;
			const primaryKey = primaryKeys[alteration.id];
			
			// make sure new name isn't already in use
			if (alteration.propertyName === 'name') {
				const newName = alteration.value as PrimaryKeyData['name'];
				const primaryKeyExists = Object.values(primaryKeys).filter(x => x.name === newName && x.id !== primaryKey.id).length > 0;
				
				if (primaryKeyExists) {
					const errorAction = dispatch(errorNameAlreadyExists(newName, ModelObjectType.primaryKey, alteration.id, action.errorKey));
					(action.errors = action.errors || []).push(errorAction.payload.data);
				}
			}
			
			// make sure table exists
			if (alteration.propertyName === 'tableId') {
				const newTableId = alteration.value as PrimaryKeyData['tableId'];
				if (!(newTableId in tables)) {
					const errorAction = dispatch(errorSchemaItemNotExists(ModelObjectType.primaryKey, alteration.id, ModelObjectType.table, newTableId, action.errorKey));
					(action.errors = action.errors || []).push(errorAction.payload.data);
				}
				else {
					// generate and set the primary key name
					deferredActions.push(refreshGeneratedNames('primaryKeys', primaryKey.id));
				}
			}
			
			// make sure columns exist
			if (alteration.propertyName === 'columnIds') {
				const columnIds = alteration.value as PrimaryKeyData['columnIds'];
				if (columnIds.length === 0) {
					const errorAction = dispatch(errorNoColumns(ModelObjectType.primaryKey, alteration.id, action.errorKey));
					(action.errors = action.errors || []).push(errorAction.payload.data);
					// no return; the error will be set, but it should be allowed to have an empty column list
				}
				else {
					for (let id of columnIds) {
						if (!(id in columns)) {
							const errorAction = dispatch(errorSchemaItemNotExists(ModelObjectType.primaryKey, alteration.id, ModelObjectType.column, id, action.errorKey));
							(action.errors = action.errors || []).push(errorAction.payload.data);
						}
					}
					
					deferredActions.push(refreshGeneratedNames('primaryKeys', primaryKey.id));
				}
			}
		}
		else if (action.type === ActionType.deletePrimaryKey) {
			const { id } = (action as DeletePrimaryKeyActionData).payload;
			const { schema: { primaryKeys } } = getState();
			
			if (!(id in primaryKeys)) {
				const errorAction = dispatch(errorSchemaItemNotExists(ModelObjectType.primaryKey, id, ModelObjectType.primaryKey, id, action.errorKey));
				(action.errors = action.errors || []).push(errorAction.payload.data);
				return action;
			}
		}
		
		const nextAction = next(action);
		
		deferredActions.forEach(a => dispatch(a));
		
		if (!nextAction?.errors || nextAction.errors.length === 0) {
			// only try to apply to mixin children if there are no errors here
			applyToMixinChildren(getState(), dispatch, action);
		}
		
		return nextAction;
	}

function applyToMixinChildren(state: RootState, dispatch: Dispatch<ActionData>, action: ActionData) {
	if (action.type === ActionType.createPrimaryKey) {
		const payload = (action as CreatePrimaryKeyActionData).payload;
		const primaryKey = payload.primaryKey;
		const mixinCloneTables = getMixinTableClones(state, primaryKey.tableId);
		mixinCloneTables.forEach(t => {
			if (t.primaryKeyId) {
				dispatch(deletePrimaryKey(t.primaryKeyId!));
			}
			
			const mixinCloneColumns = primaryKey.columnIds.map(id => getMixinColumnClones(state, id, t.id)).flatMap(x => x);
			const newPayload: CreatePrimaryKeyActionData['payload'] = {
				...payload,
				primaryKey: {
					...primaryKey,
					id: generateId(IdPrefix.primaryKey),
					tableId: t.id,
					columnIds: mixinCloneColumns.map(c => c.id),
					belongsToMixinSchemaItem: primaryKey.id,
					belongsToMixinTable: primaryKey.tableId,
				},
			};
			
			dispatch({
				...action,
				payload: newPayload,
			});
		});
	}
	else if (action.type === ActionType.deletePrimaryKey) {
		const payload = (action as DeletePrimaryKeyActionData).payload;
		const mixinClonePrimaryKeys = getMixinPrimaryKeyClones(state, payload.id);
		mixinClonePrimaryKeys.forEach(pk => {
			const newPayload: DeletePrimaryKeyActionData['payload'] = {
				...payload,
				id: pk.id,
			};
			
			dispatch({
				...action,
				payload: newPayload,
			});
		});
	}
	else if (action.type === ActionType.alterPrimaryKeyProperty) {
		const payload = (action as AlterPrimaryKeyPropertyActionData).payload;
		const mixinClonePrimaryKeys = getMixinPrimaryKeyClones(state, payload.id);
		mixinClonePrimaryKeys.forEach(pk => {
			const newPayload: AlterPrimaryKeyPropertyActionData['payload'] = {
				...payload,
				id: pk.id,
			};
			
			if (newPayload.propertyName === 'columnIds') {
				const newColumnIds: string[] = payload.value;
				const mixinCloneColumns = newColumnIds.map(id => getMixinColumnClones(state, id, pk.tableId)).flatMap(x => x);
				newPayload.value = mixinCloneColumns.map(c => c.id);
			}
			
			dispatch({
				...action,
				payload: newPayload,
			});
		});
	}
}
