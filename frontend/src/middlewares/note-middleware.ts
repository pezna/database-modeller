import { deselectBoard } from "@/actions";
import { ActionData, ActionType, DeleteNoteActionData, RootState } from "@/data";
import { getIsNoteSelected } from "@/selectors";
import { diagramService } from "@/services";
import { isActionObject, isNextFunction } from "@/utils";
import { Dispatch, Middleware } from "redux";

export const noteMiddleware: Middleware<{}, RootState, Dispatch<ActionData>> =
	({ getState, dispatch }) =>
	(next) =>
	(action) =>
	{
		if (!isNextFunction(next)) {
			return;
		}
		
		if (!isActionObject(action)) {
			return next(action);
		}
		
		const deferredActions: ActionData[] = [];
		
		if (action.type === ActionType.deleteNote) {
			const { id } = (action as DeleteNoteActionData).payload;
			
			if (getIsNoteSelected(getState(), id)) {
				const ret = dispatch(deselectBoard());
				if (ret.errors) {
					action.errors = (action.errors || []).concat(ret.errors);
					return action;
				}
			}
			
			diagramService.deleteItem(id);
		}
		
		const nextAction = next(action);
		
		deferredActions.forEach(a => dispatch(a));
		
		return nextAction;
	};
