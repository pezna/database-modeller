import { deleteSqlSchemaUses, setSqlSchemaUses } from "@/actions";
import { ActionData, ActionType, DeleteSqlDocumentActionData, RootState, SetSqlDocumentActionData } from "@/data";
import { isActionObject, isNextFunction, ParsedPath } from "@/utils";
import { Dispatch, Middleware } from "redux";

export const codeMiddleware: Middleware<{}, RootState, Dispatch<ActionData>> =
	({ getState, dispatch }) =>
	(next) =>
	(action) =>
	{
		if (!isNextFunction(next)) {
			return;
		}
		
		if (!isActionObject(action)) {
			return next(action);
		}
		
		const deferredActions: ActionData[] = [];
		
		if (action.type === ActionType.setSqlDocument) {
			const { data } = (action as SetSqlDocumentActionData).payload;
			const { code, general: { modelFilePath } } = getState();
			
			if (!(data.id in code.sqlDocuments)) {
				dispatch(setSqlSchemaUses(data.id, []));
			}
			
			if (!data.folderPath && modelFilePath) {
				data.folderPath = new ParsedPath(modelFilePath).directory;
			}
		}
		else if (action.type === ActionType.deleteSqlDocument) {
			const { id } = (action as DeleteSqlDocumentActionData).payload;
			
			deferredActions.push(deleteSqlSchemaUses(id));
		}
		
		const nextAction = next(action);
		
		deferredActions.forEach(a => dispatch(a));
		
		return nextAction;
	};
