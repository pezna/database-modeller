import { InputComponentProps, TableCascadeInput } from "@/components/widgets";


export function DefaultMixinTables({ value, onChange }: InputComponentProps) {
	return (
		<TableCascadeInput
			tableType="mixins"
			value={value}
			onChange={onChange}
			multiPicker
		/>
	);
}
