import { alterSchemaSettingsProperty, alterUiStateProperty } from '@/actions';
import { ScrollableContainer, getSettingDetailRow } from '@/components/widgets';
import { SettingsContextData, SettingsProvider } from '@/contexts/SettingsContext';
import { SchemaSettingsData, SchemaSettingsDataPath, SettingsSidebarUiState, schemaSettingsDetails } from '@/data';
import { getAppMode, getSchemaSettings, getSettingsSidebarUi } from '@/selectors';
import { useDispatch, useSelector } from "@/store";
import { useCallback, useLayoutEffect, useRef } from 'react';
import { DefaultMixinTables } from './DefaultMixinTables';

export function SchemaSettings() {
	const dispatch = useDispatch();
	const settings = useSelector(getSchemaSettings);
	const scrollAmount = useSelector(state => getSettingsSidebarUi(state).schemaSettingsScrollAmount);
	const refContainer = useRef<HTMLDivElement>(null);
	const appMode = useSelector(getAppMode);
	
	useLayoutEffect(() => {
			const container = refContainer.current;
			if (container && container.scrollTop !== scrollAmount) {
				setTimeout(() => {
					container.scrollTop = scrollAmount;
				}, 0);
			}
	}, [scrollAmount]);
	
	const handleScroll = (e: React.UIEvent<HTMLDivElement>) => {
		if (e.currentTarget.scrollTop !== scrollAmount) {
			dispatch(alterUiStateProperty(['settingsSidebar', 'schemaSettingsScrollAmount'], e.currentTarget.scrollTop));
		}
	};
	
	const getExpandedGroupKey = useCallback<(key: keyof SchemaSettingsData) => keyof SettingsSidebarUiState>((key) => `${key}SchemaSettingsExpanded`, []);
	
	const setValue = useCallback((keyPath: string[], value: any) => {
		dispatch(alterSchemaSettingsProperty(keyPath as any, value));
	}, [dispatch]);
	
	const setExpandedGroup = useCallback((key: string, isExpanded: boolean) => {
		dispatch(alterUiStateProperty(['settingsSidebar', key as any], isExpanded));
	}, [dispatch]);
	
	const getInputComponent = useCallback<SettingsContextData['getInputComponent']>((path) => {
		const pathKey = path.join(".") as SchemaSettingsDataPath;
		const componentConstructor = componentMap[pathKey];
		return componentConstructor ?? (() => null);
	}, []);
	
	const contextData: SettingsContextData = {
		state: settings,
		getExpandedGroupKey,
		setValue,
		setExpandedGroup,
		getInputComponent,
	};
	
	return (
		<ScrollableContainer ref={refContainer} onScroll={handleScroll}>
			<SettingsProvider contextData={contextData}>
			{
				Object.entries(schemaSettingsDetails).map(([key, detail]) => {
					const kp = [key];
					const R = getSettingDetailRow(kp, detail, appMode);
					return (
						<R
							key={key}
							keyPath={kp}
							settingDetail={detail}
							isParentEnabled={true}
							appMode={appMode}
						/>
					);
				})
			}
			</SettingsProvider>
		</ScrollableContainer>
	);
}

const componentMap: {[K in SchemaSettingsDataPath]?: ReturnType<SettingsContextData['getInputComponent']>} = {
	"schema.mixinTables.defaultMixinTableIds": DefaultMixinTables,
};
