import { alterUiStateProperty } from "@/actions";
import { CodeContainer } from "@/components/options-sidebar/CodeContainer";
import { ErrorContainer } from "@/components/options-sidebar/ErrorContainer";
import { ExportContainer } from "@/components/options-sidebar/ExportContainer";
import { ManageSchemaContainer } from "@/components/options-sidebar/ManageSchemaContainer";
import { SettingsContainer } from "@/components/options-sidebar/SettingsContainer";
import { Sidebar, SidebarSectionItem } from "@/components/widgets";
import { AppMode, OptionsSidebarSection } from "@/data";
import { getAppMode, getOptionsSidebarUi } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import { useCallback, useMemo } from "react";

export function OptionsSidebar() {
	const dispatch = useDispatch();
	const appMode = useSelector(getAppMode);
	const {visibleSection, sidebarWidth} = useSelector(getOptionsSidebarUi);
	const sections = useMemo<Record<string, SidebarSectionItem>>(() => ({
		[OptionsSidebarSection.manage]: {
			icon: "fa fa-database",
			buttonTitle: "Manage Schema",
			component: () => <ManageSchemaContainer />,
		},
		[OptionsSidebarSection.errors]: {
			icon: "fas fa-file-circle-exclamation",
			buttonTitle: "Errors",
			component: () => <ErrorContainer />,
		},
		[OptionsSidebarSection.exports]: {
			icon: "fas fa-file-export",
			buttonTitle: "Exports",
			component: () => <ExportContainer />,
			isHidden: () => appMode !== AppMode.database,
		},
		[OptionsSidebarSection.code]: {
			icon: "fas fa-file-code",
			buttonTitle: "Code",
			component: () => <CodeContainer />,
			invalidAppModes: [AppMode.machine],
		},
		[OptionsSidebarSection.settings]: {
			icon: "fas fa-cog",
			buttonTitle: "Settings",
			component: () => <SettingsContainer />,
		},
	}), [appMode]);
	
	const handleSetVisibleSection = useCallback((section: string | null) => {
		dispatch(alterUiStateProperty(['generalAppUi', 'optionsSidebar', 'visibleSection'], section as any));
	}, []);
	
	const handleSetSidebarWidth = useCallback((width: number) => {
		dispatch(alterUiStateProperty(['generalAppUi', 'optionsSidebar', 'sidebarWidth'], width));
	}, []);
	
	return (
		<Sidebar
			key={appMode}
			className="options-sidebar"
			placement="left"
			visibleSection={visibleSection}
			setVisibleSection={handleSetVisibleSection}
			sections={sections}
			sidebarWidth={sidebarWidth}
			setSidebarWidth={handleSetSidebarWidth}
		/>
	);
};
