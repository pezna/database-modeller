import { alterSettingsProperty, alterUiStateProperty } from "@/actions";
import { ScrollableContainer, getSettingDetailRow } from "@/components/widgets";
import { SettingsContextData, SettingsProvider } from "@/contexts/SettingsContext";
import { SettingsSidebarUiState, appSettingsDetails } from "@/data";
import { getAppMode, getSettings, getSettingsSidebarUi } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import { useCallback, useLayoutEffect, useRef } from "react";

export function AppSettings() {
	const dispatch = useDispatch();
	const settings = useSelector(getSettings);
	const scrollAmount = useSelector(state => getSettingsSidebarUi(state).appSettingsScrollAmount);
	const appMode = useSelector(getAppMode);
	const refContainer = useRef<HTMLDivElement>(null);
	
	useLayoutEffect(() => {
		const container = refContainer.current;
		if (container && container.scrollTop !== scrollAmount) {
			container.scrollTop = scrollAmount;
		}
	}, [scrollAmount]);
	
	const handleScroll = (e: React.UIEvent<HTMLDivElement>) => {
		if (e.currentTarget.scrollTop !== scrollAmount) {
			dispatch(alterUiStateProperty(['settingsSidebar', 'appSettingsScrollAmount'], e.currentTarget.scrollTop));
		}
	};
	
	const updateCallback = useCallback((keyPath: string[], value: any) => {
		dispatch(alterSettingsProperty(keyPath as any, value));
	}, [dispatch]);
	
	const setExpandedGroupCallback = useCallback((key: string, isExpanded: boolean) => {
		dispatch(alterUiStateProperty(['settingsSidebar', key as any], isExpanded));
	}, [dispatch]);
	
	const contextData: SettingsContextData = {
		state: settings,
		getExpandedGroupKey: (key) => `${key}SettingsExpanded` as keyof SettingsSidebarUiState,
		setValue: updateCallback,
		setExpandedGroup: setExpandedGroupCallback,
		getInputComponent: () => () => null,
	};
	
	return (
		<ScrollableContainer ref={refContainer} onScroll={handleScroll}>
			<SettingsProvider contextData={contextData}>
			{
				Object.entries(appSettingsDetails).map(([key, detail]) => {
					const kp = [key];
					const R = getSettingDetailRow(kp, detail, appMode);
					return (
						<R
							key={key}
							keyPath={kp}
							settingDetail={detail}
							isParentEnabled={true}
							appMode={appMode}
						/>
					);
				})
			}
			</SettingsProvider>
		</ScrollableContainer>
	);
}
