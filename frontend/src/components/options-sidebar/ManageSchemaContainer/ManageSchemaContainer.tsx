import { alterUiStateProperty } from "@/actions";
import { ColumnTypesContainer } from "@/components/options-sidebar/ColumnTypesContainer";
import { ModelSearchContainer } from "@/components/options-sidebar/ModelSearchContainer";
import { TabPanel } from "@/components/widgets";
import { AppMode, ManageSchemaTab } from "@/data";
import { getAppMode, getManageSchemaSidebarUi } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import { useEffect } from "react";

export function ManageSchemaContainer() {
	const dispatch = useDispatch();
	const appMode = useSelector(getAppMode);
	const activeTab = useSelector(state => getManageSchemaSidebarUi(state).activeTab);
	
	// For the sake of UX, make sure a tab is selected when modes are changed
	useEffect(() => {
		if (appMode === AppMode.database) {
			if (activeTab === ManageSchemaTab.machines) {
				setActiveTab(ManageSchemaTab.tables);
			}
		}
		else {
			if (activeTab !== ManageSchemaTab.machines) {
				setActiveTab(ManageSchemaTab.machines);
			}
		}
	}, [appMode, activeTab]);
	
	const setActiveTab = (tab: ManageSchemaTab | string) => {
		dispatch(alterUiStateProperty(["manageSchemaSidebar", "activeTab"], tab));
	};
	
	const renderContents = () => {
		if (appMode === AppMode.database) {
			return [
				<TabPanel.Item key="a" tabKey={ManageSchemaTab.tables} title="Tables">
					<ModelSearchContainer appMode={appMode} />
				</TabPanel.Item>
				,
				<TabPanel.Item key="b" tabKey={ManageSchemaTab.columnTypes} title="Column Types">
					<ColumnTypesContainer />
				</TabPanel.Item>
			];
		}
		else {
			return [
				<TabPanel.Item key="c" tabKey={ManageSchemaTab.machines} title="Machines">
					<ModelSearchContainer appMode={appMode} />
				</TabPanel.Item>
			];
		}
	};
	
	return (
		<TabPanel active={activeTab} onTabSelect={setActiveTab}>
			{renderContents()}
		</TabPanel>
	);
}
