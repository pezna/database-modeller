import { alterUiStateProperty, exportToCustom, setCustomExport } from "@/actions";
import { DeleteConfirm, FlexRowWithAction, HelpIcon, SlideReveal } from "@/components/widgets";
import { CustomExport, MODEL_FILE_FOLDER_TEMPLATE, SchemaExportItem, TableExportItem, newSchemaExportItem, newTableExportItem } from "@/data";
import { getSortedTables } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import { IdPrefix, generateId } from "@/utils";
import { cloneDeep } from "lodash";
import { useState } from "react";
import { Button, Form, Input, Message } from "rsuite";
import { EditSchemaItem } from "./EditSchemaItem";
import { EditTableItem } from "./EditTableItem";


interface EditExportTemplateProps {
	data: CustomExport;
	onSave: (data: CustomExport) => void;
	onDelete: (data: CustomExport) => void;
}

export function EditExportTemplate({ data, onSave, onDelete }: EditExportTemplateProps) {
	const dispatch = useDispatch();
	const sortedTables = useSelector(getSortedTables);
	const [showDirTip, setShowDirTip] = useState(false);
	
	const handleNameInput = (e: React.FormEvent<HTMLInputElement>) => {
		const name = e.currentTarget.value;
		onSave({
			...data,
			name,
		});
	}
	
	const handleBaseExportDirectoryInput = (e: React.FormEvent<HTMLInputElement>) => {
		const baseExportDirectory = e.currentTarget.value;
		onSave({
			...data,
			baseExportDirectory,
		});
	}
	
	const changeTableExport = (index: number, value: TableExportItem) => {
		const tableExports = [...data.tableExports];
		tableExports[index] = value;
		onSave({
			...data,
			tableExports,
		});
	}
	
	const changeSchemaExport = (index: number, value: SchemaExportItem) => {
		const fullSchemaExports = [...data.fullSchemaExports];
		fullSchemaExports[index] = value;
		onSave({
			...data,
			fullSchemaExports,
		});
	}
	
	const addTableExport = () => {
		const item = newTableExportItem(data.id);
		const tableExports = [...data.tableExports, item];
		onSave({
			...data,
			tableExports,
		});
	}
	
	const addSchemaExport = () => {
		const item = newSchemaExportItem(data.id);
		const fullSchemaExports = [...data.fullSchemaExports, item];
		onSave({
			...data,
			fullSchemaExports,
		});
	}
	
	const deleteTableExport = (index: number) => {
		const tableExports = [...data.tableExports];
		tableExports.splice(index, 1);
		
		onSave({
			...data,
			tableExports,
		});
	}
	
	const deleteSchemaExport = (index: number) => {
		const fullSchemaExports = [...data.fullSchemaExports];
		fullSchemaExports.splice(index, 1);
		
		onSave({
			...data,
			fullSchemaExports,
		});
	}
	
	const runExport = () => {
		dispatch(exportToCustom(data, true));
	}
	
	const duplicateExport = () => {
		const newData = cloneDeep(data);
		newData.id = generateId(IdPrefix.export);
		newData.name += " Copy";
		newData.tableExports.forEach(x => {
			x.id = `${newData.id}${Math.random().toString(32).substring(2)}`;
		});
		newData.fullSchemaExports.forEach(x => {
			x.id = `${newData.id}${Math.random().toString(32).substring(2)}`;
		});
		dispatch(setCustomExport(newData));
		dispatch(alterUiStateProperty(["exportSidebar", "templatesOpenExportId"], newData.id));
	}
	
	const deleteExport = () => {
		onDelete(data);
	}
	
	return (
		<Form className="template-form" fluid>
			<FlexRowWithAction className="export-list-actions">
				<Button appearance="primary" color="green" title="Run export" onClick={runExport}>
					<span className="fa fa-play"></span>
				</Button>
				<Button appearance="primary" title="Duplicate" onClick={duplicateExport}>
					<span className="fa fa-copy"></span>
				</Button>
				<DeleteConfirm itemName="export" yes={deleteExport} placement="bottomStart" />
			</FlexRowWithAction>
			<Form.Group controlId="ex-t-name">
				<Form.ControlLabel>Name</Form.ControlLabel>
				<Input value={data.name} onInput={handleNameInput} />
			</Form.Group>
			<Form.Group controlId="ex-t-bed">
				<Form.ControlLabel>
					<FlexRowWithAction>
						<div>Base Export Directory</div>
						<HelpIcon onClick={() => setShowDirTip(!showDirTip)} />
					</FlexRowWithAction>
				</Form.ControlLabel>
				<ExportDirectoryHelpText show={showDirTip} />
				<Input value={data.baseExportDirectory} onInput={handleBaseExportDirectoryInput} />
			</Form.Group>
			
			<h4>Table Exports</h4>
			<Button onClick={addTableExport}>Add Table Export</Button>
			{
				data.tableExports.map((x, index) => (
					<EditTableItem
						key={x.id}
						item={x}
						tables={sortedTables}
						onChange={(val) => changeTableExport(index, val)}
						onDelete={() => deleteTableExport(index)}
					/>
				))
			}
			<h4>Full Schema Exports</h4>
			<Button onClick={addSchemaExport}>Add Full Schema Export</Button>
			{
				data.fullSchemaExports.map((x, index) => (
					<EditSchemaItem
						key={x.id}
						item={x}
						onChange={(val) => changeSchemaExport(index, val)}
						onDelete={() => deleteSchemaExport(index)}
					/>
				))
			}
		</Form>
	);
}

interface ExportDirectoryHelpTextProps {
	show: boolean;
}

function ExportDirectoryHelpText({ show }: ExportDirectoryHelpTextProps) {
	const renderHidden = () => {
		return (
			<Message
				type="info"
				header="Options"
			>
				<div>
					<fieldset>
						<legend>Template Replacement</legend>
						<p>
							<code>{MODEL_FILE_FOLDER_TEMPLATE}</code> &mdash; the folder that the current model file is saved.
						</p>
					</fieldset>
				</div>
			</Message>
		);
	};
	
	return (
		<SlideReveal
			showHidden={show}
			alwaysVisibleComponent={() => null}
			hiddenComponent={renderHidden}
		/>
	);
}
