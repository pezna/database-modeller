import { DeleteConfirm, SlideReveal } from "@/components/widgets";
import { CodeEntry } from "@/components/widgets/CodeEntry";
import { FlexRowWithAction } from "@/components/widgets/FlexRowWithAction";
import { HelpIcon } from "@/components/widgets/icons";
import { SchemaExportItem } from "@/data";
import { useState } from "react";
import { Form, Input, Message } from "rsuite";


type EditSchemaItemProps = {
	item: SchemaExportItem;
	onChange: (value: SchemaExportItem) => void;
	onDelete: () => void;
}

export function EditSchemaItem({ item, onChange, onDelete }: EditSchemaItemProps) {
	const [showCodeTip, setShowCodeTip] = useState(false);
	
	const toggleShowTip = () => setShowCodeTip(!showCodeTip);
	
	const updateItemProperty = <K extends keyof SchemaExportItem>(propertyName: K, value: SchemaExportItem[K]) => {
		onChange({
			...item,
			[propertyName]: value,
		});
	};
	
	const handleNameChange = (val: string) => updateItemProperty('name', val);
	const handleEditorChange = (value: string) => updateItemProperty('code', value)
	
	return (
		<div className="export-item">
			<Form.Group controlId={`esch-name-${item.id}`}>
				<Form.ControlLabel>Name</Form.ControlLabel>
				<Input value={item.name} onChange={handleNameChange} />
			</Form.Group>
			<Form.Group>
				<FlexRowWithAction>
					<div>
						<label>Code</label>
					</div>
					<HelpIcon onClick={toggleShowTip} />
				</FlexRowWithAction>
				<CodeHelpText show={showCodeTip} />
				<CodeEntry
					value={item.code}
					onChange={handleEditorChange}
					language="javascript"
					errorKey={item.id}
				/>
			</Form.Group>
			<Form.Group>
				<DeleteConfirm itemName="full schema export" placement="topEnd" yes={onDelete} />
			</Form.Group>
		</div>
	);
}

interface CodeHelpTextProps {
	show: boolean;
}

function CodeHelpText({ show }: CodeHelpTextProps) {
	const renderHidden = () => {
		return (
			<Message
				type="info"
				header="Code Information"
			>
				<div>
					<fieldset>
						<legend>Functions to fill</legend>
						<p>
							<code>getFileName()</code> &mdash; the filename of the resulting file that will be generated. This will be appended to the Base Export Directory specified above.
						</p>
						<p>
							<code>generate()</code> &mdash; the file contents that will be output.
						</p>
					</fieldset>
					<fieldset>
						<legend>Available variables</legend>
						<p>
							<code>tables</code> &mdash; the javascript object container for all tables. Key-Value is ID =&gt; table object
						</p>
						<p>
							<code>primaryKeys</code> &mdash; the javascript object container for all primary keys. Key-Value is ID =&gt; primary key object
						</p>
						<p>
							<code>columns</code> &mdash; the javascript object container for all columns. Key-Value is ID =&gt; column object
						</p>
						<p>
							<code>columnTypes</code> &mdash; the javascript object container for all column types. Key-Value is ID =&gt; column type object
						</p>
						<p>
							<code>relationships</code> &mdash; the javascript object container for all relationships. Key-Value is ID =&gt; relationship object
						</p>
						<p>
							<code>indexes</code> &mdash; the javascript object container for all indexes. Key-Value is ID =&gt; index object
						</p>
						<p>
							<code>uniques</code> &mdash; the javascript object container for all unique constraints. Key-Value is ID =&gt; unique object
						</p>
					</fieldset>
					<fieldset>
						<legend>Available utility functions</legend>
						<p>
							<code>convertCase.upperCase(text)</code> &mdash; changes "yolo swag" to "YOLO SWAG"
						</p>
						<p>
							<code>convertCase.lowerCase(text)</code> &mdash; changes "YOLO SWAG" to "yolo swag"
						</p>
						<p>
							<code>convertCase.titleCase(text)</code> &mdash; changes "yolo swag" to "Yolo Swag"
						</p>
						<p>
							<code>convertCase.toggleCase(text)</code> &mdash; changes "yolo swag" to "yOLO sWAG"
						</p>
						<p>
							<code>convertCase.camelCase(text)</code> &mdash; changes "yolo swag" to "yoloSwag"
						</p>
						<p>
							<code>convertCase.pascalCase(text)</code> &mdash; changes "yolo swag" to "YoloSwag"
						</p>
						<p>
							<code>convertCase.snakeCase(text)</code> &mdash; changes "yolo swag" to "yolo_swag"
						</p>
						<p>
							<code>convertCase.upperSnakeCase(text)</code> &mdash; changes "yolo swag" to "YOLO_SWAG"
						</p>
						<p>
							<code>convertCase.kebabCase(text)</code> &mdash; changes "yolo swag" to "yolo-swag"
						</p>
						<p>
							<code>convertCase.upperKebabCase(text)</code> &mdash; changes "yolo swag" to "YOLO-SWAG"
						</p>
						<p>
							<code>convertCase.cobolCase(text)</code> &mdash; changes "yolo swag" to "YOLO-SWAG"
						</p>
						<p>
							<code>convertCase.trainCase(text)</code> &mdash; changes "yolo swag" to "Yolo-Swag"
						</p>
						<p>
							<code>convertCase.flatCase(text)</code> &mdash; changes "yolo swag" to "yoloswag"
						</p>
						<p>
							<code>convertCase.upperFlatCase(text)</code> &mdash; changes "yolo swag" to "YOLOSWAG"
						</p>
						<p>
							<code>convertCase.alternatingCase(text)</code> &mdash; changes "yolo swag" to "yOlO sWaG"
						</p>
					</fieldset>
				</div>
			</Message>
		);
	};
	
	return (
		<SlideReveal
			showHidden={show}
			alwaysVisibleComponent={() => null}
			hiddenComponent={renderHidden}
		/>
	);
}