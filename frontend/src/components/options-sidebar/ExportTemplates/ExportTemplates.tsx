import { alterUiStateProperty, deleteCustomExport, setCustomExport } from "@/actions";
import { ScrollableContainer } from "@/components/widgets";
import { CustomExport, newCustomExport } from "@/data";
import { getCustomExports, getExportSidebarUi, getSortedCustomExports } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import styled from "@emotion/styled";
import { useLayoutEffect, useRef } from "react";
import { Button, SelectPicker } from "rsuite";
import { EditExportTemplate } from "./EditExportTemplate";

export function ExportTemplates() {
	const dispatch = useDispatch();
	const sidebarSettings = useSelector(getExportSidebarUi);
	const customExports = useSelector(getCustomExports);
	const sortedCustomExports = useSelector(getSortedCustomExports);
	const editingExportData = customExports[sidebarSettings.templatesOpenExportId];
	const refContainer = useRef<HTMLDivElement>(null);
	
	useLayoutEffect(() => {
		const container = refContainer.current;
		if (container && container.scrollTop !== sidebarSettings.templatesScrollAmount) {
			container.scrollTop = sidebarSettings.templatesScrollAmount;
		}
	}, [sidebarSettings]);
	
	const handleScroll = (e: React.UIEvent<HTMLDivElement>) => {
		if (e.currentTarget.scrollTop !== sidebarSettings.templatesScrollAmount) {
			dispatch(alterUiStateProperty(["exportSidebar", "templatesScrollAmount"], e.currentTarget.scrollTop));
		}
	};
	
	const changeSelectedExport = (id: string | null) => {
		if (!id) {
			return;
		}
		
		dispatch(alterUiStateProperty(["exportSidebar", "templatesOpenExportId"], id));
	};
	
	const addNewExport = () => {
		const exp = newCustomExport();
		dispatch(setCustomExport(exp));
		changeSelectedExport(exp.id);
	};
	
	const saveEdit = (data: CustomExport) => {
		dispatch(setCustomExport(data));
	};
	
	const deleteExport = (data: CustomExport) => {
		changeSelectedExport("");
		dispatch(deleteCustomExport(data.id));
	};
	
	return (
		<ExportTemplatesWrapper ref={refContainer} onScroll={handleScroll}>
			<div className="export-list-actions">
				<Button appearance="subtle" onClick={addNewExport} title="New custom export">
					<span className="fa fa-file-circle-plus"></span>
				</Button>
				<SelectPicker
					data={sortedCustomExports}
					labelKey="name"
					valueKey="id"
					value={editingExportData?.id ?? ""}
					onChange={changeSelectedExport}
					cleanable={false}
					preventOverflow
				/>
			</div>
			{
				editingExportData
				? <EditExportTemplate data={editingExportData} onSave={saveEdit} onDelete={deleteExport} />
				: null
			}
		</ExportTemplatesWrapper>
	);
}

const ExportTemplatesWrapper = styled(ScrollableContainer)`
	.export-list-actions {
		display: flex;
		flex-direction: row;
		gap: 1em;
		padding: 10px 0;
		border-bottom: 1px solid #ccc;
		
		.rs-picker-select {
			flex-grow: 1;
		}
	}
	
	.template-form {
		display: flex;
		flex-direction: column;
		gap: 10px;
		
		.export-item {
			border: 1px solid #aaa;
			margin: 10px;
			padding: 20px;
			
			.test-settings {
				display: grid;
				grid-template-columns: 1fr 1fr;
				border-top: 1px solid white;
				padding-top: 12px;
			}
			
			.rs-message {
				margin: 10px 0;
				
				h6 {
					margin-top: 10px;
					text-transform: uppercase;
				}
			}
		}
	}
`;