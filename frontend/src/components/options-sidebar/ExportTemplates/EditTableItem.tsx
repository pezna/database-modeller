import { CodeEntry, DeleteConfirm, FlexRowWithAction, HelpIcon, SlideReveal } from "@/components/widgets";
import { TableData, TableExportItem } from "@/data";
import { useState } from "react";
import { Form, Input, Message, TagPicker } from "rsuite";

type EditTableItemProps = {
	item: TableExportItem;
	tables: TableData[];
	onChange: (value: TableExportItem) => void;
	onDelete: () => void;
}

export function EditTableItem({ item, tables, onChange, onDelete }: EditTableItemProps) {
	const [showCodeTip, setShowCodeTip] = useState(false);
	
	const toggleShowTip = () => setShowCodeTip(!showCodeTip);
	
	const updateItemProperty = <K extends keyof TableExportItem>(propertyName: K, value: TableExportItem[K]) => {
		onChange({
			...item,
			[propertyName]: value,
		});
	};
	const handleNameChange = (val: string) => updateItemProperty('name', val);
	const handleEditorChange = (value: string) => updateItemProperty('code', value);
	const handleIncludedTablesChange = (val: string[]) => updateItemProperty('includedTableIds', val);
	const handleExcludedTablesChange = (val: string[]) => updateItemProperty('excludedTableIds', val);
	
	return (
		<div className="export-item">
			<Form.Group controlId={`etbl-name-${item.id}`}>
				<Form.ControlLabel>Name</Form.ControlLabel>
				<Input value={item.name} onChange={handleNameChange} />
			</Form.Group>
			<Form.Group>
				<FlexRowWithAction>
					<div>
						<label>Code</label>
					</div>
					<HelpIcon onClick={toggleShowTip} />
				</FlexRowWithAction>
				<CodeHelpText show={showCodeTip} />
				<CodeEntry
					value={item.code}
					onChange={handleEditorChange}
					language="javascript"
					errorKey={item.id}
				/>
			</Form.Group>
			<Form.Group controlId={`etbl-1-${item.id}`}>
				<Form.ControlLabel>Included Tables</Form.ControlLabel>
				<TagPicker
					id={`etbl-1-${item.id}`}
					data={tables}
					labelKey="name"
					valueKey="id"
					value={item.includedTableIds}
					onChange={handleIncludedTablesChange}
					block
				/>
			</Form.Group>
			<Form.Group controlId={`etbl-2-${item.id}`}>
				<Form.ControlLabel>Excluded Tables</Form.ControlLabel>
				<TagPicker
					id={`etbl-2-${item.id}`}
					data={tables}
					labelKey="name"
					valueKey="id"
					value={item.excludedTableIds}
					onChange={handleExcludedTablesChange}
					block
				/>
			</Form.Group>
			<Form.Group>
				<DeleteConfirm itemName="table export" placement="topEnd" yes={onDelete} />
			</Form.Group>
		</div>
	);
}

interface CodeHelpTextProps {
	show: boolean;
}

function CodeHelpText({ show }: CodeHelpTextProps) {
	const renderHidden = () => {
		return (
			<Message
				type="info"
				header="Code Information"
			>
				<div>
					<fieldset>
						<legend>Functions to fill</legend>
						<p>
							<code>getFileName()</code> &mdash; the filename of the resulting file that will be generated. This will be appended to the Base Export Directory specified above.
						</p>
						<p>
							<code>generate()</code> &mdash; the file contents that will be output.
						</p>
					</fieldset>
					<fieldset>
						<legend>Available variables</legend>
						<p>
							<code>table</code> &mdash; the javascript object that represents a table
						</p>
						<p>
							<code>primaryKey</code> &mdash; the javascript object that represents a table's primary key (may be null)
						</p>
						<p>
							<code>columns</code> &mdash; the array of javascript objects that represent a column on the table
						</p>
						<p>
							<code>columnTypes</code> &mdash; the array of javascript objects that represent a column's type
						</p>
						<p>
							<code>relationships</code> &mdash; the array of javascript objects that represent a foreign key on the table
						</p>
						<p>
							<code>indexes</code> &mdash; the array of javascript objects that represent an index on the table
						</p>
						<p>
							<code>uniques</code> &mdash; the array of javascript objects that represent a unique constraint on the table
						</p>
					</fieldset>
					<fieldset>
						<legend>Available utility functions</legend>
						<p>
							<code>convertCase.upperCase(text)</code> &mdash; changes "yolo swag" to "YOLO SWAG"
						</p>
						<p>
							<code>convertCase.lowerCase(text)</code> &mdash; changes "YOLO SWAG" to "yolo swag"
						</p>
						<p>
							<code>convertCase.titleCase(text)</code> &mdash; changes "yolo swag" to "Yolo Swag"
						</p>
						<p>
							<code>convertCase.toggleCase(text)</code> &mdash; changes "yolo swag" to "yOLO sWAG"
						</p>
						<p>
							<code>convertCase.camelCase(text)</code> &mdash; changes "yolo swag" to "yoloSwag"
						</p>
						<p>
							<code>convertCase.pascalCase(text)</code> &mdash; changes "yolo swag" to "YoloSwag"
						</p>
						<p>
							<code>convertCase.snakeCase(text)</code> &mdash; changes "yolo swag" to "yolo_swag"
						</p>
						<p>
							<code>convertCase.upperSnakeCase(text)</code> &mdash; changes "yolo swag" to "YOLO_SWAG"
						</p>
						<p>
							<code>convertCase.kebabCase(text)</code> &mdash; changes "yolo swag" to "yolo-swag"
						</p>
						<p>
							<code>convertCase.upperKebabCase(text)</code> &mdash; changes "yolo swag" to "YOLO-SWAG"
						</p>
						<p>
							<code>convertCase.cobolCase(text)</code> &mdash; changes "yolo swag" to "YOLO-SWAG"
						</p>
						<p>
							<code>convertCase.trainCase(text)</code> &mdash; changes "yolo swag" to "Yolo-Swag"
						</p>
						<p>
							<code>convertCase.flatCase(text)</code> &mdash; changes "yolo swag" to "yoloswag"
						</p>
						<p>
							<code>convertCase.upperFlatCase(text)</code> &mdash; changes "yolo swag" to "YOLOSWAG"
						</p>
						<p>
							<code>convertCase.alternatingCase(text)</code> &mdash; changes "yolo swag" to "yOlO sWaG"
						</p>
					</fieldset>
				</div>
			</Message>
		);
	}
	return (
		<SlideReveal
			showHidden={show}
			alwaysVisibleComponent={() => null}
			hiddenComponent={renderHidden}
		/>
	);
}