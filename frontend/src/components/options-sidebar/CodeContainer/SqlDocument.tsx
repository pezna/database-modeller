import { deleteSqlDocument, selectColumn, selectTable, setSqlDocument } from "@/actions";
import { AlignFlexItems, CodeEntry, DeleteConfirm, FormGroupWrapper } from "@/components/widgets";
import { SchemaUseItem, SqlDocumentData, SqlDocumentSchemaUsage } from "@/data";
import { useCodeMirrorSqlSchema } from "@/hooks";
import { getModelFilePath } from "@/selectors";
import { fileService } from "@/services";
import { useDispatch, useSelector } from "@/store";
import { EditorSelection } from "@codemirror/state";
import styled from "@emotion/styled";
import { useState } from "react";
import { Button, Form, Input, Stack } from "rsuite";

type AggregatedTableUses = {
	tableUses: Extract<SqlDocumentSchemaUsage, {type: 'table'}>[];
	columns: AggregatedColumnUses[];
}

type AggregatedColumnUses = {
	columnUses: Extract<SqlDocumentSchemaUsage, {type: 'column'}>[];
}

type AggregatedBindParamUses = {
	bindParamUses: Extract<SqlDocumentSchemaUsage, {type: 'bindParameter'}>[];
}

interface EditSqlDocumentProps {
	document: SqlDocumentData;
	onAfterDelete: (id: string) => void;
}

export function SqlDocument({ document, onAfterDelete }: EditSqlDocumentProps) {
	const dispatch = useDispatch();
	const modellerPath = useSelector(getModelFilePath);
	const { aggTableUses } = aggregateTableUses(document.schemaUsages);
	const bindParamUses = aggregateBindParamUses(document.schemaUsages);
	const identifierUses = document.schemaUsages.filter(x => x.type === "identifier");
	const [codeSelection, setCodeSelection] = useState<EditorSelection>();
	const [codeKey, setCodeKey] = useState(""); // necessary so that code highlights will be redrawn by react when the key changes
	const cmSqlSchema = useCodeMirrorSqlSchema();
	
	const updateItemProperty = <K extends keyof SqlDocumentData>(propertyName: K, value: SqlDocumentData[K]) => {
		dispatch(setSqlDocument({
			...document,
			[propertyName]: value,
		}));
	};
	
	const handleNameChange = (val: string) => updateItemProperty("name", val);
	const handleEditorChange = (value: string) => updateItemProperty("code", value);
	const handleFolderChange = async () => {
		const path = await fileService.openFolder(document.folderPath || modellerPath || undefined);
		if (path) {
			updateItemProperty("folderPath", path);
		}
	};
	const handleDelete = () => {
		dispatch(deleteSqlDocument(document.id));
		onAfterDelete(document.id);
	};
	
	const highlightUse = (use: SchemaUseItem) => {
		const selection = EditorSelection.create(
			[EditorSelection.range(use.locationStartIndex, use.locationStopIndex)]
		);
		setCodeSelection(selection);
		setCodeKey(`${use.locationStartIndex}:${use.locationStopIndex}`);
	};
	
	const renderBindParamUse = (use: SchemaUseItem) => {
		return (
			<div key={use.locationStartIndex} className="use-item bind-use">
				<button className="line" onClick={() => highlightUse(use)}>Line {use.lineNumber}:{use.columnNumber}</button>
			</div>
		);
	};
	
	const renderTableUse = (use: SchemaUseItem) => {
		return (
			<div key={use.locationStartIndex} className="use-item table-use">
				<button className="line" onClick={() => highlightUse(use)}>Line {use.lineNumber}:{use.columnNumber}</button>
			</div>
		);
	};
	
	const renderColumnUse = (use: SchemaUseItem) => {
		return (
			<div key={use.locationStartIndex} className="use-item column-use">
				<button className="line" onClick={() => highlightUse(use)}>Line {use.lineNumber}:{use.columnNumber}</button>
			</div>
		);
	};
	
	const renderIdentifierUse = (usage: Extract<SqlDocumentSchemaUsage, {type: 'identifier'}>) => {
		const use = usage.usage;
		return (
			<fieldset key={use.identifier}>
				<legend>
					Unknown Identifier "{use.identifier}"
				</legend>
				Potentially {usage.potentialSchemaType}?
				<div className="use-item bind-use">
					<button className="line" onClick={() => highlightUse(use)}>Line {use.lineNumber}:{use.columnNumber}</button>
				</div>
			</fieldset>
		);
	};
	
	const renderAggTableUse = (use: AggregatedTableUses) => {
		const tableUse = use.tableUses[0];
		const select = () => dispatch(selectTable(tableUse.tableId));
		
		return (
			<fieldset key={tableUse.tableId}>
				<legend>
					Table "{tableUse.name}"
				</legend>
				<button className="select-item" onClick={select}>Select table</button>
				{use.tableUses.map(x => renderTableUse(x.usage))}
				{use.columns.map(renderAggColumnUse)}
			</fieldset>
		);
	};
	
	const renderAggColumnUse = (use: AggregatedColumnUses) => {
		const columnUse = use.columnUses[0];
		const select = () => dispatch(selectColumn(columnUse.columnId));
		
		return (
			<fieldset key={columnUse.columnId}>
				<legend>
					Column "{columnUse.name}"
				</legend>
				<button className="select-item" onClick={select}>Select column</button>
				{use.columnUses.map(x => renderColumnUse(x.usage))}
			</fieldset>
		);
	};
	
	const renderAggBindParamUse = (use: AggregatedBindParamUses) => {
		const bindParam = use.bindParamUses[0];
		
		return (
			<fieldset key={bindParam.usage.identifier}>
				<legend>
					Bind Parameter "{bindParam.usage.identifier}"
				</legend>
				{use.bindParamUses.map(x => renderBindParamUse(x.usage))}
			</fieldset>
		);
	};
	
	return (
		<Wrapper>
			<Form fluid>
				<Form.Group controlId="sql-name">
					<Form.ControlLabel>Name</Form.ControlLabel>
					<Input value={document.name} onChange={handleNameChange} />
				</Form.Group>
				<Form.Group controlId="sql-folder">
					<Form.ControlLabel>Folder Path</Form.ControlLabel>
					<Stack spacing={8}>
						<Button onClick={handleFolderChange}>
							<span className="fa fa-folder-open"></span>
						</Button>
						<Input value={document.folderPath} plaintext={true} />
					</Stack>
				</Form.Group>
				<Form.Group>
					<Form.ControlLabel>SQL Code</Form.ControlLabel>
					<CodeEntry
						key={codeKey}
						value={document.code}
						onChange={handleEditorChange}
						codeMirrorProps={{selection: codeSelection}}
						language="sql"
						sqlSchema={cmSqlSchema}
					/>
				</Form.Group>
				<FormGroupWrapper>
					<AlignFlexItems align="right">
						<DeleteConfirm
							itemName="sql document"
							placement="bottomStart"
							yes={handleDelete}
						/>
					</AlignFlexItems>
				</FormGroupWrapper>
				<Form.Group>
					<div className="schema-use-container">
						{aggTableUses.map(renderAggTableUse)}
						{bindParamUses.map(renderAggBindParamUse)}
						{identifierUses.map(renderIdentifierUse)}
					</div>
				</Form.Group>
			</Form>
		</Wrapper>
	);
}

const Wrapper = styled.div`
	.schema-use-container {
		margin-top: 20px;
		
		.select-item {
			padding: 2px;
			border: none;
			outline: none;
			background: none;
			float: right;
			
			&:hover {
				color: #4290f0;
			}
		}
		
		.line {
			padding: 1px;
			border: none;
			outline: none;
			background: none;
			
			&:hover {
				color: #4290f0;
			}
		}
	}
`;

function aggregateTableUses(allUses: SqlDocumentSchemaUsage[]) {
	const aggTableUses: {[id: string]: AggregatedTableUses} = {};
	const aggColumnUses: {[id: string]: AggregatedColumnUses} = {};
	
	for (const use of allUses) {
		if (use.type === "table") {
			let agg = aggTableUses[use.tableId];
			if (!agg) {
				agg = {
					tableUses: [],
					columns: [],
				};
				aggTableUses[use.tableId] = agg;
			}
			
			agg.tableUses.push(use);
		}
		else if (use.type === "column") {
			let aggTable = aggTableUses[use.tableId];
			if (!aggTable) {
				aggTable = {
					tableUses: [],
					columns: [],
				};
				aggTableUses[use.tableId] = aggTable;
			}
			
			let aggColumn = aggColumnUses[use.columnId];
			if (!aggColumn) {
				aggColumn = {
					columnUses: [],
				};
				aggColumnUses[use.columnId] = aggColumn;
				aggTable.columns.push(aggColumn);
			}
			
			aggColumn.columnUses.push(use);
		}
	}
	
	return { aggTableUses: Object.values(aggTableUses), aggColumnUses: Object.values(aggColumnUses) };
}

function aggregateBindParamUses(allUses: SqlDocumentSchemaUsage[]) {
	const aggBindParamUses: {[id: string]: AggregatedBindParamUses} = {};
	
	for (const use of allUses) {
		if (use.type === "bindParameter") {
			let agg = aggBindParamUses[use.usage.identifier!];
			if (!agg) {
				agg = {
					bindParamUses: [],
				};
				aggBindParamUses[use.usage.identifier!] = agg;
			}
			
			agg.bindParamUses.push(use);
		}
	}
	
	return Object.values(aggBindParamUses);
}
