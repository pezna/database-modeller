import { Accordion } from "@/components/widgets";
import { CaseChoice, OutputLanguageOptions, caseChoicesSelectData, csharpObjectKeywordsSelectData, javaObjectKeywordsSelectData, typescriptObjectKeywordsSelectData } from "@/data";
import { Checkbox, Form, InputPicker } from "rsuite";

interface LanguageOutputOptionsProps {
	options: OutputLanguageOptions;
	onChange: (options: OutputLanguageOptions) => void;
}

export function LanguageOutputOptions({ options, onChange }: LanguageOutputOptionsProps) {
	const renderOption = () => {
		if (options.type === 'cpp') {
			return <CppOptions options={options} onChange={onChange} />
		}
		else if (options.type === 'csharp') {
			return <CsharpOptions options={options} onChange={onChange} />
		}
		else if (options.type === 'java') {
			return <JavaOptions options={options} onChange={onChange} />
		}
		else if (options.type === 'javascript') {
			return <JavascriptOptions options={options} onChange={onChange} />
		}
		else if (options.type === 'typescript') {
			return <TypescriptOptions options={options} onChange={onChange} />
		}
		else if (options.type === 'rust') {
			return <RustOptions options={options} onChange={onChange} />
		}
		else if (options.type === 'python') {
			return <PythonOptions options={options} onChange={onChange} />
		}
		
		return null;
	};
	
	return (
		<Accordion heading="Options">
			{renderOption()}
		</Accordion>
	);
}

interface CppOptionsProps {
	options: Extract<OutputLanguageOptions, {type: 'cpp'}>;
	onChange: (options: Extract<OutputLanguageOptions, {type: 'cpp'}>) => void;
}

function CppOptions({ options, onChange }: CppOptionsProps) {
	const handleChangeProperty = (property: keyof typeof options, value: any) => {
		onChange({
			...options,
			[property]: value,
		});
	};
	
	return (
		<>
			<TextOption
				label="Class Name Prefix"
				value={options.classNamePrefix}
				onChange={(val) => handleChangeProperty("classNamePrefix", val)}
			/>
			<TextOption
				label="Class Name Suffix"
				value={options.classNameSuffix}
				onChange={(val) => handleChangeProperty("classNameSuffix", val)}
			/>
			<CaseOption
				label="Class Name Case"
				value={options.classNameCase}
				onChange={(val) => handleChangeProperty("classNameCase", val)}
			/>
			<CaseOption
				label="Variable Case"
				value={options.variableCase}
				onChange={(val) => handleChangeProperty("variableCase", val)}
			/>
			<CaseOption
				label="File Name Case"
				value={options.fileNameCase}
				onChange={(val) => handleChangeProperty("fileNameCase", val)}
			/>
			<TextOption
				label="File Name Suffix"
				value={options.fileNameSuffix}
				onChange={(val) => handleChangeProperty("fileNameSuffix", val)}
			/>
			<TextOption
				label="File Extension"
				value={options.fileExtension}
				onChange={(val) => handleChangeProperty("fileExtension", val)}
			/>
			<BooleanOption
				label="Make Struct"
				value={options.makeStruct}
				onChange={(val) => handleChangeProperty("makeStruct", val)}
			/>
			<BooleanOption
				label="Add Constructor"
				value={options.addConstructor}
				onChange={(val) => handleChangeProperty("addConstructor", val)}
			/>
		</>
	);
}

interface CsharpOptionsProps {
	options: Extract<OutputLanguageOptions, {type: 'csharp'}>;
	onChange: (options: Extract<OutputLanguageOptions, {type: 'csharp'}>) => void;
}

function CsharpOptions({ options, onChange }: CsharpOptionsProps) {
	const handleChangeProperty = (property: keyof typeof options, value: any) => {
		if (property === "objectKeyword") {
			if (value.includes("record") || value.includes("interface")) {
				options = {
					...options,
					addConstructor: false,
				};
			}
		}
		
		onChange({
			...options,
			[property]: value,
			useRecordShorthand: (
				["makeFieldsVirtual", "variablesNotProperties", "propertyInitNotSet"].includes(property)
				? false
				: (property === "useRecordShorthand" ? value : options.useRecordShorthand)
			),
		});
	};
	const showRecordShorthandOption = options.objectKeyword.includes("record") && !options.makeFieldsVirtual && !options.variablesNotProperties && !options.propertyInitNotSet;
	const showAddConstructor = !options.objectKeyword.includes("record") && !options.objectKeyword.includes("interface");
	
	return (
		<>
			<TextOption
				label="Class Name Prefix"
				value={options.classNamePrefix}
				onChange={(val) => handleChangeProperty("classNamePrefix", val)}
			/>
			<TextOption
				label="Class Name Suffix"
				value={options.classNameSuffix}
				onChange={(val) => handleChangeProperty("classNameSuffix", val)}
			/>
			<BooleanOption
				label="Use Member Variables Not Properties"
				value={options.variablesNotProperties}
				onChange={(val) => handleChangeProperty("variablesNotProperties", val)}
			/>
			{
				!options.variablesNotProperties
				? (
					<BooleanOption
						label="Use { init; } not { set; } for Properties"
						value={options.propertyInitNotSet}
						onChange={(val) => handleChangeProperty("propertyInitNotSet", val)}
					/>
				)
				: null
			}
			<BooleanOption
				label="Make Fields Virtual"
				value={options.makeFieldsVirtual}
				onChange={(val) => handleChangeProperty("makeFieldsVirtual", val)}
			/>
			<Form.Group controlId={`csharp-kw-option`}>
				<Form.ControlLabel>Object Keyword</Form.ControlLabel>
				<Form.Control
					accepter={InputPicker}
					name=""
					data={csharpObjectKeywordsSelectData}
					labelKey="label"
					valueKey="value"
					value={options.objectKeyword}
					onChange={(val) => handleChangeProperty("objectKeyword", val)}
					cleanable={false}
					preventOverflow
					block
				/>
			</Form.Group>
			{
				showRecordShorthandOption
				? (
					<BooleanOption
						label="Use Record Shorthand"
						value={options.useRecordShorthand}
						onChange={(val) => handleChangeProperty("useRecordShorthand", val)}
					/>
				)
				: null
			}
			{
				showAddConstructor
				? (
					<BooleanOption
						label="Add Constructor"
						value={options.addConstructor}
						onChange={(val) => handleChangeProperty("addConstructor", val)}
					/>
				)
				: null
			}
		</>
	);
}

interface JavaOptionsProps {
	options: Extract<OutputLanguageOptions, {type: 'java'}>;
	onChange: (options: Extract<OutputLanguageOptions, {type: 'java'}>) => void;
}

function JavaOptions({ options, onChange }: JavaOptionsProps) {
	const handleChangeProperty = (property: keyof typeof options, value: any) => {
		if (property === "objectKeyword") {
			if (value.includes("record")) {
				options = {
					...options,
					addConstructor: false,
				};
			}
		}
		onChange({
			...options,
			[property]: value,
		});
	};
	
	return (
		<>
			<TextOption
				label="Class Name Prefix"
				value={options.classNamePrefix}
				onChange={(val) => handleChangeProperty("classNamePrefix", val)}
			/>
			<TextOption
				label="Class Name Suffix"
				value={options.classNameSuffix}
				onChange={(val) => handleChangeProperty("classNameSuffix", val)}
			/>
			<Form.Group controlId="java-kw-option">
				<Form.ControlLabel>Object Keyword</Form.ControlLabel>
				<Form.Control
					accepter={InputPicker}
					name=""
					data={javaObjectKeywordsSelectData}
					labelKey="label"
					valueKey="value"
					value={options.objectKeyword}
					onChange={(val) => handleChangeProperty("objectKeyword", val)}
					cleanable={false}
					preventOverflow
					block
				/>
			</Form.Group>
			<BooleanOption
				label="Make Static"
				value={options.makeStatic}
				onChange={(val) => handleChangeProperty("makeStatic", val)}
			/>
			{
				options.objectKeyword !== "record"
				? (
					<BooleanOption
						label="Add Constructor"
						value={options.addConstructor}
						onChange={(val) => handleChangeProperty("addConstructor", val)}
					/>
				)
				: null
			}
		</>
	);
}

interface JavascriptOptionsProps {
	options: Extract<OutputLanguageOptions, {type: 'javascript'}>;
	onChange: (options: Extract<OutputLanguageOptions, {type: 'javascript'}>) => void;
}

function JavascriptOptions({ options, onChange }: JavascriptOptionsProps) {
	const handleChangeProperty = (property: keyof typeof options, value: any) => {
		onChange({
			...options,
			[property]: value,
		});
	};
	
	return (
		<>
			<TextOption
				label="Class Name Prefix"
				value={options.classNamePrefix}
				onChange={(val) => handleChangeProperty("classNamePrefix", val)}
			/>
			<TextOption
				label="Class Name Suffix"
				value={options.classNameSuffix}
				onChange={(val) => handleChangeProperty("classNameSuffix", val)}
			/>
			<CaseOption
				label="Variable Case"
				value={options.variableCase}
				onChange={(val) => handleChangeProperty("variableCase", val)}
			/>
			<CaseOption
				label="File Name Case"
				value={options.fileNameCase}
				onChange={(val) => handleChangeProperty("fileNameCase", val)}
			/>
			<TextOption
				label="File Name Suffix"
				value={options.fileNameSuffix}
				onChange={(val) => handleChangeProperty("fileNameSuffix", val)}
			/>
			<BooleanOption
				label="Add Constructor"
				value={options.addConstructor}
				onChange={(val) => handleChangeProperty("addConstructor", val)}
			/>
		</>
	);
}

interface TypescriptOptionsProps {
	options: Extract<OutputLanguageOptions, {type: 'typescript'}>;
	onChange: (options: Extract<OutputLanguageOptions, {type: 'typescript'}>) => void;
}

function TypescriptOptions({ options, onChange }: TypescriptOptionsProps) {
	const handleChangeProperty = (property: keyof typeof options, value: any) => {
		onChange({
			...options,
			[property]: value,
		});
	};
	
	return (
		<>
			<TextOption
				label="Class Name Prefix"
				value={options.classNamePrefix}
				onChange={(val) => handleChangeProperty("classNamePrefix", val)}
			/>
			<TextOption
				label="Class Name Suffix"
				value={options.classNameSuffix}
				onChange={(val) => handleChangeProperty("classNameSuffix", val)}
			/>
			<CaseOption
				label="Variable Case"
				value={options.variableCase}
				onChange={(val) => handleChangeProperty("variableCase", val)}
			/>
			<CaseOption
				label="File Name Case"
				value={options.fileNameCase}
				onChange={(val) => handleChangeProperty("fileNameCase", val)}
			/>
			<TextOption
				label="File Name Suffix"
				value={options.fileNameSuffix}
				onChange={(val) => handleChangeProperty("fileNameSuffix", val)}
			/>
			<Form.Group controlId="typescript-kw-option">
				<Form.ControlLabel>Object Keyword</Form.ControlLabel>
				<Form.Control
					accepter={InputPicker}
					name=""
					data={typescriptObjectKeywordsSelectData}
					labelKey="label"
					valueKey="value"
					value={options.objectKeyword}
					onChange={(val) => handleChangeProperty("objectKeyword", val)}
					cleanable={false}
					preventOverflow
					block
				/>
			</Form.Group>
		</>
	);
}

interface RustOptionsProps {
	options: Extract<OutputLanguageOptions, {type: 'rust'}>;
	onChange: (options: Extract<OutputLanguageOptions, {type: 'rust'}>) => void;
}

function RustOptions({ options, onChange }: RustOptionsProps) {
	const handleChangeProperty = (property: keyof typeof options, value: any) => {
		onChange({
			...options,
			[property]: value,
		});
	};
	
	return (
		<>
			<TextOption
				label="Struct Name Prefix"
				value={options.structNamePrefix}
				onChange={(val) => handleChangeProperty("structNamePrefix", val)}
			/>
			<TextOption
				label="Struct Name Suffix"
				value={options.structNameSuffix}
				onChange={(val) => handleChangeProperty("structNameSuffix", val)}
			/>
			<TextOption
				label="File Name Suffix"
				value={options.fileNameSuffix}
				onChange={(val) => handleChangeProperty("fileNameSuffix", val)}
			/>
			<BooleanOption
				label="Add Constructor"
				value={options.addConstructor}
				onChange={(val) => handleChangeProperty("addConstructor", val)}
			/>
		</>
	);
}

interface PythonOptionsProps {
	options: Extract<OutputLanguageOptions, {type: 'python'}>;
	onChange: (options: Extract<OutputLanguageOptions, {type: 'python'}>) => void;
}

function PythonOptions({ options, onChange }: PythonOptionsProps) {
	const handleChangeProperty = (property: keyof typeof options, value: any) => {
		onChange({
			...options,
			[property]: value,
		});
	};
	
	return (
		<>
			<TextOption
				label="Class Name Prefix"
				value={options.classNamePrefix}
				onChange={(val) => handleChangeProperty("classNamePrefix", val)}
			/>
			<TextOption
				label="Class Name Suffix"
				value={options.classNameSuffix}
				onChange={(val) => handleChangeProperty("classNameSuffix", val)}
			/>
			<TextOption
				label="File Name Suffix"
				value={options.fileNameSuffix}
				onChange={(val) => handleChangeProperty("fileNameSuffix", val)}
			/>
			<BooleanOption
				label="Make Dataclass"
				value={options.makeDataclass}
				onChange={(val) => handleChangeProperty("makeDataclass", val)}
			/>
		</>
	);
}

interface TextOptionProps {
	label: string;
	value: string;
	onChange: (value: string) => void;
}

function TextOption({ label, value, onChange }: TextOptionProps) {
	return (
		<Form.Group controlId={`text-option-${label}`}>
			<Form.ControlLabel>{label}</Form.ControlLabel>
			<Form.Control name="" value={value} onChange={onChange} />
		</Form.Group>
	);
}

interface CaseOptionProps {
	label: string;
	value: CaseChoice | null;
	onChange: (value: CaseChoice | null) => void;
}

function CaseOption({ label, value, onChange }: CaseOptionProps) {
	const handleChange = (newVal: CaseChoice | '') => {
		if (!newVal) {
			return onChange(null);
		}
		
		onChange(newVal);
	}
	
	return (
		<Form.Group controlId={`case-option-${label}`}>
			<Form.ControlLabel>{label}</Form.ControlLabel>
			<Form.Control
				accepter={InputPicker}
				name=""
				data={caseChoicesSelectData}
				labelKey="label"
				valueKey="value"
				value={value}
				onChange={handleChange}
				cleanable={false}
				preventOverflow
				block
			/>
		</Form.Group>
	);
}

interface BooleanOptionProps {
	label: string;
	value: boolean;
	onChange: (value: boolean) => void;
}

function BooleanOption({ label, value, onChange }: BooleanOptionProps) {
	return (
		<Form.Group controlId={`bool-option-${label}`}>
			<Form.ControlLabel>{label}</Form.ControlLabel>
			<Form.Control
				accepter={Checkbox}
				name=""
				checked={value}
				onChange={(_, chk) => onChange(chk as boolean)}
			/>
		</Form.Group>
	);
}