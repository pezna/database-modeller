import { alterUiStateProperty, setSqlDocument, setStructOutput } from "@/actions";
import { ScrollableContainer, SplitLayout, SplitPanel } from "@/components/widgets";
import { languageTypeSelectData, newSqlDocumentData, newStructOutputData, OutputLanguageOptions } from "@/data";
import { getCodeSidebarUi, getSqlDocuments, getStructOutputs } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import styled from "@emotion/styled";
import { useLayoutEffect, useRef } from "react";
import { Button, Dropdown } from "rsuite";
import { CodeDocument } from "./CodeDocument";
import { CodeDocumentList } from "./CodeDocumentList";

export function CodeContainer() {
	const dispatch = useDispatch();
	const documents = useSelector(getSqlDocuments);
	const structOutputs = useSelector(getStructOutputs);
	const sidebarUi = useSelector(getCodeSidebarUi);
	const refContainer = useRef<HTMLDivElement>(null);
	
	useLayoutEffect(() => {
		const container = refContainer.current;
		if (container && container.scrollTop !== sidebarUi.scrollAmount) {
			container.scrollTop = sidebarUi.scrollAmount;
		}
	}, [sidebarUi.scrollAmount]);
	
	const handleScroll = (e: React.UIEvent<HTMLDivElement>) => {
		if (e.currentTarget.scrollTop !== sidebarUi.scrollAmount) {
			dispatch(alterUiStateProperty(["codeSidebar", "scrollAmount"], e.currentTarget.scrollTop));
		}
	};
	
	const newDocument = () => {
		const data = newSqlDocumentData();
		dispatch(setSqlDocument(data));
		selectDocument(data.id);
	};
	
	const newStructOutput = (lang: OutputLanguageOptions['type']) => {
		const data = newStructOutputData(lang);
		dispatch(setStructOutput(data));
		selectDocument(data.id);
	};
	
	const selectDocument = (newActiveId: string) => {
		dispatch(alterUiStateProperty(["codeSidebar", "activeDocumentId"], newActiveId));
	};
	
	const handleAfterDelete = (id: string) => {
		if (sidebarUi.activeDocumentId === id) {
			dispatch(alterUiStateProperty(["codeSidebar", "activeDocumentId"], ""));
		}
	};
	
	const handleDividerMoved = (position: number) => {
		dispatch(alterUiStateProperty(["codeSidebar", "splitPanelDividerPosition"], position));
	};
	
	const handleExpandedItems = (ids: string[]) => {
		dispatch(alterUiStateProperty(["codeSidebar", "expandedTreeIds"], ids));
	};
	
	const renderDocumentList = () => {
		return (
			<CodeDocumentList
				selectedId={sidebarUi.activeDocumentId}
				expandedIds={sidebarUi.expandedTreeIds}
				onSelectDocument={selectDocument}
				onExpanded={handleExpandedItems}
			/>
		);
	};
	
	const renderEditDocument = () => {
		if (!sidebarUi.activeDocumentId) {
			return null;
		}
		
		return (
			<CodeDocument
				document={documents[sidebarUi.activeDocumentId]}
				structOutput={structOutputs[sidebarUi.activeDocumentId]}
				onAfterDelete={handleAfterDelete}
			/>
		);
	};
	
	return (
		<CodeContainerWrapper ref={refContainer} onScroll={handleScroll}>
			<div className="code-container-header">
				<Button onClick={newDocument}>New SQL</Button>
				<Dropdown title="New Data Output">
					{
						languageTypeSelectData.map(x => (
							<Dropdown.Item
								key={x.value}
								onSelect={() => newStructOutput(x.value as any)}
							>
								{x.label}
							</Dropdown.Item>
						))
					}
				</Dropdown>
			</div>
			<SplitPanel
				first={renderDocumentList()}
				second={renderEditDocument()}
				layout={SplitLayout.horizontal}
				dividerPosition={sidebarUi.splitPanelDividerPosition}
				onDividerMoved={handleDividerMoved}
			/>
		</CodeContainerWrapper>
	);
}

const CodeContainerWrapper = styled(ScrollableContainer)`
	min-height: 100%;
	display: flex;
	flex-direction: column;
	
	.code-container-header {
		display: flex;
		flex-direction: row;
		border-bottom: 1px solid #ccc;
		padding-bottom: 1em;
		gap: 0.5rem;
	}
	
	.split-panel {
		flex-grow: 1;
	}
`;