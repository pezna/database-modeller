import { SqlDocumentData, StructOutputData } from "@/data";
import styled from "@emotion/styled";
import { SqlDocument } from "./SqlDocument";
import { StructOutput } from "./StructOutput";

interface CodeDocumentProps {
	document?: SqlDocumentData;
	structOutput?: StructOutputData;
	onAfterDelete: (id: string) => void;
}

export function CodeDocument({ document, structOutput, onAfterDelete }: CodeDocumentProps) {
	return (
		<Wrapper>
			{
				document
				? (
					<SqlDocument
						key={document.id}
						document={document}
						onAfterDelete={onAfterDelete}
					/>
				)
				: null
			}
			{
				structOutput
				? (
					<StructOutput
						key={structOutput.id}
						structOutput={structOutput}
						onAfterDelete={onAfterDelete}
					/>
				)
				: null
			}
		</Wrapper>
	);
}

const Wrapper = styled.div`
	position: sticky;
	top: 0;
	padding: 0.5rem;
`;
