import { getSqlDocuments, getStructOutputs } from "@/selectors";
import { useSelector } from "@/store";
import styled from "@emotion/styled";
import { useMemo } from "react";
import { Tree } from "rsuite";
import { ItemDataType } from "rsuite/esm/internals/types";

interface CodeDocumentListProps {
	selectedId: string;
	expandedIds: string[];
	onSelectDocument: (id: string) => void;
	onExpanded: (ids: string[]) => void;
}

export function CodeDocumentList({ selectedId, expandedIds, onSelectDocument, onExpanded }: CodeDocumentListProps) {
	const documents = useSelector(getSqlDocuments);
	const structOutputs = useSelector(getStructOutputs);
	
	const data = useMemo(() => {
		const sql: ItemDataType = {
			name: "SQL",
			id: "sql",
			children: Object.values(documents),
		};
		const code: ItemDataType = {
			name: "Code Output",
			id: "code",
			children: Object.values(structOutputs),
		};
		
		return [sql, code];
	}, [documents, structOutputs]);
	
	const handleSelect = (item: ItemDataType) => {
		if (item.children) {
			const index = expandedIds.indexOf(item.value as string);
			if (index > -1) {
				const newExpandedIds = [...expandedIds];
				newExpandedIds.splice(index, 1);
				onExpanded(newExpandedIds);
			}
			else {
				onExpanded([...expandedIds, item.value as string]);
			}
			// don't select items with children
			return;
		}
		onSelectDocument(item.value as string);
	};
	
	const handleExpanded = (items: any[]) => {
		if (!items) {
			return;
		}
		onExpanded(items.filter(x => typeof x === "string"));
	};
	
	return (
		<Wrapper>
			<Tree
				data={data}
				value={selectedId}
				labelKey="name"
				valueKey="id"
				expandItemValues={expandedIds}
				onSelect={handleSelect}
				onExpand={handleExpanded}
			/>
		</Wrapper>
	);
}

const Wrapper = styled.div`
	height: 100%;
	
	.rs-tree {
		max-height: 100%;
		height: 100% !important;
	}
`;
