import { deleteError, deleteStructOutput, runStructOutput, setStructOutput } from "@/actions";
import { AlignFlexItems, CodeEntry, DeleteConfirm, ErrorMessage, FormGroupWrapper } from "@/components/widgets";
import { ErrorKeyPrefix, OutputLanguageOptions, StructOutputData, languageTypeNameMap } from "@/data";
import { getSortedTables, getStructOutputError } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import styled from "@emotion/styled";
import { Button, Checkbox, Form, Input, TagPicker } from "rsuite";
import { LanguageOutputOptions } from "./LanguageOutputOptions";

interface StructOutputProps {
	structOutput: StructOutputData;
	onAfterDelete: (id: string) => void;
}

export function StructOutput({ structOutput, onAfterDelete }: StructOutputProps) {
	const dispatch = useDispatch();
	const tables = useSelector(getSortedTables);
	
	const updateItemProperty = <K extends keyof StructOutputData>(propertyName: K, value: StructOutputData[K]) => {
		dispatch(setStructOutput({
			...structOutput,
			[propertyName]: value,
		}));
	};
	
	const handleNameChange = (val: string) => updateItemProperty("name", val);
	const handleFolderChange = (val: string) => updateItemProperty("folderPath", val);
	const handleEditorChange = (value: string) => updateItemProperty("code", value);
	const handleIncludedTablesChange = (val: string[]) => updateItemProperty("includedTableIds", val);
	const handleExcludedTablesChange = (val: string[]) => updateItemProperty("excludedTableIds", val);
	const handleSingleFileChange = (_: any, checked: any) => updateItemProperty("singleFileOutput", checked);
	const handleDelete = () => {
		dispatch(deleteStructOutput(structOutput.id));
		onAfterDelete(structOutput.id);
	};
	const handleOptionsChange = (options: OutputLanguageOptions) => {
		updateItemProperty("options", options);
	};
	
	const handleRunOutput = () => {
		dispatch(runStructOutput(structOutput.id));
	};
	
	return (
		<Form fluid>
			<Header>
				<Title>{languageTypeNameMap[structOutput.options.type]}</Title>
				<Button onClick={handleRunOutput} color="green" appearance="primary" title="Run">
					<span className="fa fa-play"></span>
				</Button>
			</Header>
			<Form.Group controlId="so-name">
				<Form.ControlLabel>Name</Form.ControlLabel>
				<Input value={structOutput.name} onChange={handleNameChange} />
			</Form.Group>
			<Form.Group controlId="so-folder">
				<Form.ControlLabel>Output Folder Path</Form.ControlLabel>
				<Input value={structOutput.folderPath} onChange={handleFolderChange} />
			</Form.Group>
			<OutputCode
				id={structOutput.id}
				value={structOutput.code}
				onChange={handleEditorChange}
			/>
			<Form.Group controlId="so-inc-tbls">
				<Form.ControlLabel>
					Included Tables
					<Form.HelpText tooltip>If empty, all tables will be included. If not empty, only the selected tables will be included.</Form.HelpText>
				</Form.ControlLabel>
				<TagPicker
					id="so-inc-tbls"
					data={tables}
					labelKey="name"
					valueKey="id"
					value={structOutput.includedTableIds}
					onChange={handleIncludedTablesChange}
					block
				/>
			</Form.Group>
			<Form.Group controlId="so-exc-tbls">
				<Form.ControlLabel>
					Excluded Tables
					<Form.HelpText tooltip>If empty, no table will be excluded. If not empty, only the selected tables will be excluded.</Form.HelpText>
				</Form.ControlLabel>
				<TagPicker
					id="so-exc-tbls"
					data={tables}
					labelKey="name"
					valueKey="id"
					value={structOutput.excludedTableIds}
					onChange={handleExcludedTablesChange}
					block
				/>
			</Form.Group>
			<Form.Group controlId="so-single-file">
				<Form.ControlLabel>
					Single File Unified Output
					<Form.HelpText tooltip>If checked, only one file will be created and contain all data structure outputs for all tables.</Form.HelpText>
				</Form.ControlLabel>
				<Form.Control
					accepter={Checkbox}
					name=""
					checked={structOutput.singleFileOutput}
					onChange={handleSingleFileChange}
				/>
			</Form.Group>
			<LanguageOutputOptions
				options={structOutput.options}
				onChange={handleOptionsChange}
			/>
			<FormGroupWrapper>
				<AlignFlexItems align="right">
					<DeleteConfirm
						itemName="data output"
						placement="topEnd"
						yes={handleDelete}
					/>
				</AlignFlexItems>
			</FormGroupWrapper>
		</Form>
	);
}

const Header = styled.div`
	display: flex;
	padding-bottom: 0.5rem;
	margin-bottom: 1rem;
	border-bottom: 1px solid white;
`;

const Title = styled.h5`
	flex-grow: 1;
`;

interface OutputCodeProps {
	id: string;
	value: string;
	onChange: (value: string) => void;
}

function OutputCode({ id, value, onChange }: OutputCodeProps) {
	const dispatch = useDispatch();
	const errorKey = ErrorKeyPrefix.structOutputCode + id;
	const errorMessage = useSelector(state => getStructOutputError(state, errorKey));
	
	const changeInput = (newValue: string) => {
		if (errorMessage) {
			dispatch(deleteError(errorKey, "structOutputErrors"));
		}
		
		onChange(newValue);
	};
	
	return (
		<Form.Group>
			<Form.ControlLabel>
				Code Template
				<Form.HelpText tooltip></Form.HelpText>
			</Form.ControlLabel>
			<CodeEntry
				value={value}
				onChange={changeInput}
			/>
			<ErrorMessage errors={errorMessage} />
		</Form.Group>
	);
}