import { alterUiStateProperty } from "@/actions";
import { AppSettings } from "@/components/options-sidebar/AppSettings";
import { SchemaSettings } from "@/components/options-sidebar/SchemaSettings";
import { TabPanel } from "@/components/widgets";
import { SettingsTab } from "@/data";
import { getSettingsSidebarUi } from "@/selectors";
import { useDispatch, useSelector } from "@/store";

export function SettingsContainer() {
	const dispatch = useDispatch();
	const activeTab = useSelector(state => getSettingsSidebarUi(state).activeTab);
	
	const setActiveTab = (active: SettingsTab | string) => {
		dispatch(alterUiStateProperty(["settingsSidebar", "activeTab"], active));
	};
	
	return (
		<TabPanel active={activeTab} onTabSelect={setActiveTab}>
			<TabPanel.Item tabKey={SettingsTab.appSettings} title="Global App Settings">
				<AppSettings />
			</TabPanel.Item>
			<TabPanel.Item tabKey={SettingsTab.modelSettings} title="Model Settings">
				<SchemaSettings />
			</TabPanel.Item>
		</TabPanel>
	);
}
