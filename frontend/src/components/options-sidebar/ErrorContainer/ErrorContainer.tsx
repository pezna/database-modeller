import { alterUiStateProperty, deleteSchemaError } from "@/actions";
import { ScrollableContainer } from "@/components/widgets";
import { AppMode, ModelItemErrorData, ModelObjectType } from "@/data";
import { useModelNavigation } from "@/hooks";
import { getAppMode, getConsoleSidebarUi, getErrors } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import styled from "@emotion/styled";
import { useLayoutEffect, useRef } from "react";
import { Message } from "rsuite";

export function ErrorContainer() {
	const dispatch = useDispatch();
	const appMode = useSelector(getAppMode);
	const errors = useSelector(getErrors);
	const scrollAmount = useSelector(state => getConsoleSidebarUi(state).scrollAmount);
	const schemaNavigation = useModelNavigation();
	const refContainer = useRef<HTMLDivElement>(null);
	
	useLayoutEffect(() => {
		const container = refContainer.current;
		if (container && container.scrollTop !== scrollAmount) {
			container.scrollTop = scrollAmount;
		}
	}, [scrollAmount]);
	
	const handleScroll = (e: React.UIEvent<HTMLDivElement>) => {
		if (e.currentTarget.scrollTop !== scrollAmount) {
			dispatch(alterUiStateProperty(['consoleSidebar', 'scrollAmount'], e.currentTarget.scrollTop));
		}
	};
	
	const deleteError = (schemaType: ModelObjectType, errorKey: string) => {
		dispatch(deleteSchemaError(schemaType, errorKey));
	};
	
	const selectItem = (err: ModelItemErrorData) => {
		schemaNavigation.select(err.schemaType, err.schemaId);
	};
	
	const renderError = (err: ModelItemErrorData) => {
		return (
			<Message
				key={err.errorKey}
				type="error"
				header="Error"
				showIcon={true}
				closable
				onClose={() => deleteError(err.schemaType, err.errorKey!)}
			>
				<span className="title">Error Key:</span><span>{err.errorKey}</span>
				<span className="title">Schema Type:</span><span>{err.schemaType}</span>
				<span className="title">Schema Id:</span><span><button className="link" onClick={() => selectItem(err)}>{err.schemaId}</button></span>
				<span className="title">Message:</span><span>{err.message}</span>
				<hr />
			</Message>
		);
	};
	
	const renderSchemaErrors = () => {
		if (appMode !== AppMode.database) {
			return null;
		}
		
		return (
			<>
			<h4>Errors</h4>
			{Object.values(errors.modelErrors).flatMap(errs => errs.map(renderError))}
			</>
		);
	};
	
	return (
		<ErrorContainerWrapper ref={refContainer} onScroll={handleScroll}>
			{renderSchemaErrors()}
		</ErrorContainerWrapper>
	);
}

const ErrorContainerWrapper = styled(ScrollableContainer)`
	.rs-message-body {
		display: grid;
		grid-template-columns: 1fr 3fr;
		grid-template-rows: auto;
		
		.title {
			font-weight: bold;
		}
	}
`;