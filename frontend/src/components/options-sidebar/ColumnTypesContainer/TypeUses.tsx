import { Accordion, Link } from "@/components/widgets";
import { ColumnTypeUsageData } from "@/data";
import { useModelNavigation } from "@/hooks";
import styled from "@emotion/styled";

interface TypeUsesProps {
	usages: ColumnTypeUsageData[];
}

export function TypeUses({ usages }: TypeUsesProps) {
	const { selectNamespace, selectTable, selectColumn } = useModelNavigation();
	
	return (
		<Accordion
			heading={`Usages (${usages.length})`}
			useAlternateBodyBackground={true}
		>
			<Container>
			{
				usages.map((obj, i) => (
					<div key={i}>
						<Link onClick={() => selectNamespace(obj.namespaceId)}>{obj.namespaceName}</Link>
						&nbsp;&gt;&nbsp;
						<Link onClick={() => selectTable(obj.tableId)}>{obj.tableName}</Link>
						&nbsp;&gt;&nbsp;
						<Link onClick={() => selectColumn(obj.columnId)}>{obj.columnName}</Link>
					</div>
				))
			}
			</Container>
		</Accordion>
	);
}

const Container = styled.div`
	overflow: auto;
	max-height: 400px;
`;