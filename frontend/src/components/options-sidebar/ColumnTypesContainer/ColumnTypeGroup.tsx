import { deleteColumnType, setColumnType } from "@/actions";
import { Accordion, AlignFlexItems, DeleteConfirm, FormGroupWrapper } from "@/components/widgets";
import { ColumnTypeData } from "@/data";
import { getColumnTypeUses } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import { useState } from "react";
import { Button } from "rsuite";
import { ColumnTypeForm } from "./ColumnTypeForm";
import { ReplaceTypeUses } from "./ReplaceTypeUses";
import { TypeUses } from "./TypeUses";

interface ColumnTypeGroupProps {
	columnType: ColumnTypeData;
	expanded: boolean;
	expandCallback: (id: string, expanded: boolean) => void;
}

export function ColumnTypeGroup({ columnType, expanded, expandCallback }: ColumnTypeGroupProps) {
	const dispatch = useDispatch();
	const columnUses = useSelector(state => getColumnTypeUses(state, columnType.id));
	const [showReplace, setShowReplace] = useState(false);
	
	const toggleShowReplace = () => setShowReplace(!showReplace);
	
	const handleExpanded = (exp: boolean) => expandCallback(columnType.id, exp);
	
	const changeProperty = <T extends keyof ColumnTypeData>(key: T, value: ColumnTypeData[T]) => {
		const newData = {
			...columnType,
			[key]: value,
		};
		dispatch(setColumnType(newData));
	};
	
	const deleteType = () => {
		dispatch(deleteColumnType(columnType.id));
	};
	
	const renderReplaceUse = () => {
		if (!showReplace) {
			return null;
		}
		
		return (
			<ReplaceTypeUses columnType={columnType} />
		);
	}
	
	return (
		<Accordion heading={columnType.name} expanded={expanded} expandCallback={handleExpanded}>
			<FormGroupWrapper>
				<ColumnTypeForm
					columnType={columnType}
					isCreating={false}
					onChangeProperty={changeProperty}
				/>
				<TypeUses usages={columnUses} />
				<AlignFlexItems align="right">
					<Button
						appearance="subtle"
						onClick={toggleShowReplace}
						title="Replace type uses"
					>
						<span className="fa fa-shuffle"></span>
					</Button>
					<DeleteConfirm
						itemName="type"
						placement="topEnd"
						yes={deleteType}
						disabled={columnUses.length > 0}
						disabledMessage="Replace the type's usages before deleting it."
					/>
				</AlignFlexItems>
				{renderReplaceUse()}
			</FormGroupWrapper>
		</Accordion>
	);
}