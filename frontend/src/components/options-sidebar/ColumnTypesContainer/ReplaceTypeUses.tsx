import { replaceColumnTypeUsage } from "@/actions";
import { FlexRowWithAction } from "@/components/widgets";
import { ColumnTypeData } from "@/data";
import { getSortedColumnTypes } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import { useState } from "react";
import { Button, Form, SelectPicker } from "rsuite";

interface ReplaceTypeUsesProps {
	columnType: ColumnTypeData;
}

export function ReplaceTypeUses({ columnType: type }: ReplaceTypeUsesProps) {
	const dispatch = useDispatch();
	const columnTypes = useSelector(getSortedColumnTypes).filter(ct => ct.id !== type.id);
	const [selectedType, setSelectedType] = useState("");
	
	const changeInput = (value: any) => {
		const found = columnTypes.find(x => x.id === value);
		if (!found) {
			return;
		}
		
		setSelectedType(found.id);
	};
	
	const confirmReplaceType = () => {
		if (selectedType) {
			dispatch(replaceColumnTypeUsage(type.id, selectedType));
		}
	};
	
	return (
		<Form.Group controlId="replace-2">
			<Form.ControlLabel>Replace type usages with:</Form.ControlLabel>
			<FlexRowWithAction>
				<Form.Control
					name="rw"
					accepter={SelectPicker}
					data={columnTypes}
					labelKey="name"
					valueKey="id"
					searchable={false}
					onChange={changeInput}
					preventOverflow
				/>
				<Button
					appearance="subtle"
					color="green"
					onClick={confirmReplaceType}
				>
					Replace
				</Button>
			</FlexRowWithAction>
		</Form.Group>
	);
}
