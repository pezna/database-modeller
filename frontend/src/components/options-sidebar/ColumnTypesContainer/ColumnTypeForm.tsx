import { TypeValue } from "@/components/options-sidebar/ColumnTypesContainer/TypeValue";
import { ColumnLanguageData } from "@/components/options-sidebar/ColumnTypesContainer/shared-types";
import { FormGroupWrapper } from "@/components/widgets";
import { ColumnTypeData } from "@/data";
import { useState } from "react";
import { Button, Form } from "rsuite";

interface ColumnTypeFormProps {
	columnType: ColumnTypeData;
	isCreating: boolean;
	onChangeProperty: <T extends keyof ColumnTypeData>(key: T, value: ColumnTypeData[T]) => void;
}

export function ColumnTypeForm({ columnType, isCreating, onChangeProperty }: ColumnTypeFormProps) {
	const [newLanguageIndex, setNewLanguageIndex] = useState(-1);
	
	const handleChangeLanguage = (index: number, data: ColumnLanguageData) => {
		const langs = [...columnType.languages];
		langs[index] = data;
		onChangeProperty("languages", langs);
	};
	
	const handleAddNewLanguage = () => {
		const newIndex = columnType.languages.length;
		setNewLanguageIndex(newIndex);
		
		const langs = [...columnType.languages];
		langs.push({
			name: "",
			value: "",
		});
		
		onChangeProperty("languages", langs);
	};
	
	const handleDelete = (index: number) => {
		const langs = [...columnType.languages];
		langs.splice(index, 1);
		onChangeProperty("languages", langs);
	};
	
	return (
		<FormGroupWrapper>
			<Form.Group controlId={`type-${columnType.id}-name`}>
				<Form.ControlLabel>Name</Form.ControlLabel>
				<Form.Control
					name="name"
					type="text"
					value={columnType.name}
					onChange={(val) => onChangeProperty("name", val)}
				/>
			</Form.Group>
			<Form.Group>
				<fieldset>
					<legend>Language Values</legend>
					<Button
						color="green"
						appearance="subtle"
						startIcon={<span className="fa-solid fa-plus"></span>}
						onClick={handleAddNewLanguage}
					>
						Add Value
					</Button>
					{
						columnType.languages.map((data, index) => (
							<TypeValue
								key={index}
								data={data}
								startEditing={isCreating || newLanguageIndex === index}
								onChange={(data) => handleChangeLanguage(index, data)}
								onDelete={() => handleDelete(index)}
							/>
						))
					}
				</fieldset>
			</Form.Group>
		</FormGroupWrapper>
	);
}