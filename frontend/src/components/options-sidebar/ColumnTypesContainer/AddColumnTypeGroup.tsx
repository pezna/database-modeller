import { setColumnType } from "@/actions";
import { FormGroupWrapper, SlideReveal } from "@/components/widgets";
import { ColumnTypeData, newColumnTypeData } from "@/data";
import { useModelNavigation } from "@/hooks";
import { useDispatch } from "@/store";
import { useState } from "react";
import { Button } from "rsuite";
import { ColumnTypeForm } from "./ColumnTypeForm";

export function AddColumnTypeGroup() {
	const dispatch = useDispatch();
	const [data, setData] = useState<ColumnTypeData>(newColumnTypeData);
	const [showForm, setShowForm] = useState(false);
	const { selectColumnType } = useModelNavigation();
	
	const toggleShowForm = () => setShowForm(!showForm);
	
	const changeProperty = <T extends keyof ColumnTypeData>(key: T, value: ColumnTypeData[T]) => {
		const newData = {
			...data,
			[key]: value,
		};
		setData(newData);
	};
	
	const createType = () => {
		dispatch(setColumnType(data));
		selectColumnType(data.id);
		resetData();
	};
	
	const resetData = () => {
		setData(newColumnTypeData());
	};
	
	const renderPopover = () => {
		return (
			<FormGroupWrapper>
				<ColumnTypeForm
					columnType={data}
					isCreating={true}
					onChangeProperty={changeProperty}
				/>
				<Button
					appearance="primary"
					color="green"
					onClick={createType}
				>
					Create Type
				</Button>
			</FormGroupWrapper>
		);
	};
	
	const renderAddButton = () => {
		if (showForm) {
			return (
				<Button
					key="2"
					appearance="subtle"
					color="red"
					startIcon={<span className="fa-solid fa-minus"></span>}
					onClick={toggleShowForm}
				>
					Cancel Add Type
				</Button>
			);
		}
		
		return (
			<Button
				key="1"
				appearance="subtle"
				color="green"
				startIcon={<span className="fa-solid fa-plus"></span>}
				onClick={toggleShowForm}
			>
				Add Type
			</Button>
		);
	};
	
	return (
		<SlideReveal
			alwaysVisibleComponent={renderAddButton}
			hiddenComponent={renderPopover}
			showHidden={showForm}
		/>
	)
}
