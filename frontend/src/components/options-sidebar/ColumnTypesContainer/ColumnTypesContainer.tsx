import { alterUiStateProperty } from "@/actions";
import { ScrollableContainer } from "@/components/widgets";
import { getManageSchemaSidebarUi, getSortedColumnTypes } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import { useLayoutEffect, useMemo, useRef } from "react";
import { Form } from "rsuite";
import { AddColumnTypeGroup } from "./AddColumnTypeGroup";
import { ColumnTypeGroup } from "./ColumnTypeGroup";

export function ColumnTypesContainer() {
	const dispatch = useDispatch();
	const columnTypes = useSelector(getSortedColumnTypes);
	const settings = useSelector(getManageSchemaSidebarUi);
	const openColumnTypeIds = useMemo(() => new Set(settings.openColumnTypeIds), [settings.openColumnTypeIds]);
	const refContainer = useRef<HTMLDivElement>(null);
	
	useLayoutEffect(() => {
		const container = refContainer.current;
		if (container && container.scrollTop !== settings.typesScrollAmount) {
			container.scrollTop = settings.typesScrollAmount;
		}
	}, [settings.typesScrollAmount]);
	
	const handleScroll = (e: React.UIEvent<HTMLDivElement>) => {
		if (e.currentTarget.scrollTop !== settings.typesScrollAmount) {
			dispatch(alterUiStateProperty(["manageSchemaSidebar", "typesScrollAmount"], e.currentTarget.scrollTop));
		}
	};
	
	const setGroupExpanded = (id: string, expanded: boolean) => {
		if (expanded) {
			openColumnTypeIds.add(id);
		}
		else {
			openColumnTypeIds.delete(id);
		}
		
		dispatch(alterUiStateProperty(["manageSchemaSidebar", "openColumnTypeIds"], [...openColumnTypeIds]));
	};
	
	return (
		<ScrollableContainer ref={refContainer} onScroll={handleScroll}>
			<Form fluid>
				<AddColumnTypeGroup />
				{
					columnTypes.map(ct => (
						<ColumnTypeGroup
							key={ct.id}
							columnType={ct}
							expanded={openColumnTypeIds.has(ct.id)}
							expandCallback={setGroupExpanded}
						/>
					))
				}
			</Form>
		</ScrollableContainer>
	);
}
