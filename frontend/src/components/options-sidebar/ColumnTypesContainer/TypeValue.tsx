import { ColumnLanguageData } from "@/components/options-sidebar/ColumnTypesContainer/shared-types";
import styled from "@emotion/styled";
import { useState } from "react";
import { Button, Dropdown, Input } from "rsuite";

interface TypeValueProps {
	data: ColumnLanguageData;
	startEditing?: boolean;
	onChange: (data: ColumnLanguageData) => void;
	onDelete: () => void;
}

export function TypeValue({ data, startEditing, onChange, onDelete }: TypeValueProps) {
	const [isEditing, setIsEditing] = useState(startEditing);
	
	const toggleEditing = () => setIsEditing(!isEditing);
	const changeLanguageValue = (value: string) => onChange({ ...data, value, });
	const changeLanguageName = (name: string) => onChange({ ...data, name, });
	
	const renderActions = () => {
		return (
			<>
				<div>
					<Button
						key={`${isEditing}`}
						color={isEditing ? "green" : undefined}
						appearance="subtle"
						onClick={toggleEditing}
					>
						<span className={`fa fa-${isEditing ? "check" : "pencil"}`}></span>
					</Button>
				</div>
				<div>
					<Dropdown
						title={<span className="fa fa-ellipsis-vertical"></span>}
						noCaret
						placement="bottomEnd"
					>
						<Dropdown.Item onSelect={onDelete}>Delete {data.name}</Dropdown.Item>
					</Dropdown>
				</div>
			</>
		);
	};
	
	return (
		<Wrapper>
			<Col>
				<Input placeholder="Name" value={data.name} onChange={changeLanguageName} plaintext={!isEditing} />
			</Col>
			<Col>
				<Input placeholder="Value" value={data.value} onChange={changeLanguageValue} plaintext={!isEditing} />
			</Col>
			{renderActions()}
		</Wrapper>
	);
}

const Col = styled.div`
	overflow: hidden;
	text-overflow: ellipsis;
`;

const Wrapper = styled.div`
	display: grid;
	grid-template-columns: 1fr 2fr 38px 38px;
	gap: 0.25rem;
	align-items: center;
`;