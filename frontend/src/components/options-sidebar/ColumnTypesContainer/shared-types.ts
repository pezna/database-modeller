export type ColumnLanguageData = {
	name: string;
	value: string;
}
