import { alterUiStateProperty } from "@/actions";
import { Accordion, Link } from "@/components/widgets";
import { ColumnData, IndexData, PrimaryKeyData, RelationshipData, TableData, UniqueData } from "@/data";
import { useModelNavigation } from "@/hooks";
import { getColumns, getIndexes, getManageSchemaSidebarUi, getNamespaces, getPrimaryKeys, getRelationships, getSearchResults, getSelectedNamespaceId, getTables, getUniques } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import styled from "@emotion/styled";
import { useEffect, useLayoutEffect, useMemo, useRef } from "react";
import { Tag } from "rsuite";

interface TableResultsProps {
	hasSearch: boolean;
	currentNamespaceOnly: boolean;
	forceSearch: () => void;
}

export function TableResults({ hasSearch, currentNamespaceOnly, forceSearch }: TableResultsProps) {
	const dispatch = useDispatch();
	const results = useSelector(getSearchResults);
	const currentNamespaceId = useSelector(getSelectedNamespaceId);
	const namespaces = useSelector(getNamespaces);
	const tables = useSelector(getTables);
	const columns = useSelector(getColumns);
	const relationships = useSelector(getRelationships);
	const primaryKeys = useSelector(getPrimaryKeys);
	const indexes = useSelector(getIndexes);
	const uniques = useSelector(getUniques);
	const settings = useSelector(getManageSchemaSidebarUi);
	const openTableIds = useMemo(() => new Set(settings.openTableIds), [settings.openTableIds]);
	const {
		selectTable,
		selectColumn,
		selectRelationship,
		selectPrimaryKey,
		selectIndex,
		selectUnique,
	} = useModelNavigation();
	const refContainer = useRef<HTMLDivElement>(null);
	
	useLayoutEffect(() => {
		const container = refContainer.current;
		if (container && container.scrollTop !== settings.searchScrollAmount) {
			container.scrollTop = settings.searchScrollAmount;
		}
	}, [settings.searchScrollAmount]);
	
	useEffect(() => {
		forceSearch();
	}, [forceSearch, tables, columns, relationships, primaryKeys, indexes, uniques, namespaces]);
	
	const expandGroup = (tableId: string, expanded: boolean) => {
		if (expanded) {
			openTableIds.add(tableId);
		}
		else {
			openTableIds.delete(tableId);
		}
		
		dispatch(alterUiStateProperty(["manageSchemaSidebar", "openTableIds"], [...openTableIds]));
	};
	
	const renderTree = () => {
		if (hasSearch) {
			const includeTableIds = new Set<string>([
				...results.tables.map(x => x.id),
				...results.columns.map(x => x.tableId),
				...results.relationships.map(x => x.tableId),
				...results.primaryKeys.map(x => x.tableId),
				...results.indexes.map(x => x.tableId),
				...results.uniques.map(x => x.tableId),
			]);
			
			const tableIds = [...includeTableIds];
			
			return tableIds.map(tId => {
				const t = tables[tId];
				const cl = results.columns.filter(x => x.tableId === t.id).map(x => columns[x.id]);
				const rl = results.relationships.filter(x => x.tableId === t.id).map(x => relationships[x.id]);
				const pk = results.primaryKeys.filter(x => x.tableId === t.id).map(x => primaryKeys[x.id])[0];
				const ix = results.indexes.filter(x => x.tableId === t.id).map(x => indexes[x.id]);
				const uq = results.uniques.filter(x => x.tableId === t.id).map(x => uniques[x.id]);
				
				return renderTable(tables[t.id], cl, pk, rl, ix, uq);
			});
		}
		else {
			return Object.values(tables)
			.filter(t => t.namespaceId === currentNamespaceId || !currentNamespaceOnly)
			.map(t => {
				const cl = t.columnIds.map(cId => columns[cId]);
				const rl = t.relationshipIds.map(rId => relationships[rId]).filter(r => r.childTableId === t.id);
				const pk = t.primaryKeyId ? primaryKeys[t.primaryKeyId] : null;
				const ix = Object.values(indexes).filter(x => x.tableId === t.id);
				const uq = Object.values(uniques).filter(x => x.tableId === t.id);
				
				return renderTable(t, cl, pk, rl, ix, uq);
			});
		}
	};
	
	const renderTableHeader = (table: TableData) => {
		const ns = namespaces[table.namespaceId];
		return (
			<Header>
				<span>{table.name}</span>
				<Tag style={{color: ns.foregroundColor, background: ns.backgroundColor}}>
					{ns.name}
				</Tag>
			</Header>
		);
	};
	
	const renderTable = (table: TableData, columns: ColumnData[], primaryKey: PrimaryKeyData | null, relationships: RelationshipData[], indexes: IndexData[], uniques: UniqueData[]) => {
		return (
			<Accordion
				key={table.id}
				heading={renderTableHeader(table)}
				expanded={openTableIds.has(table.id)}
				expandCallback={(e) => expandGroup(table.id, e)}
			>
				<SelectTableButton onClick={() => selectTable(table.id)}>
					<span className="fa-regular fa-hand-pointer"></span>&nbsp;
					Select table
				</SelectTableButton>
				{
					columns.length === 0
					? null
					: renderTableGroup("Columns", columns.map(renderColumn))
				}
				{
					!primaryKey
					? null
					: renderTableGroup("Primary Key", renderPrimaryKey(primaryKey))
				}
				{
					relationships.length === 0
					? null
					: renderTableGroup("Relationships", relationships.map(renderRelationship))
				}
				{
					indexes.length === 0
					? null
					: renderTableGroup("Indexes", indexes.map(renderIndex))
				}
				{
					uniques.length === 0
					? null
					: renderTableGroup("Uniques", uniques.map(renderUnique))
				}
			</Accordion>
		);
	};
	
	const renderTableGroup = (heading: string, elements: React.ReactNode) => {
		return (
			<fieldset>
				<legend>{heading}</legend>
				{elements}
			</fieldset>
		);
	};
	
	const renderColumn = (column: ColumnData) => {
		return (
			<EntryRow key={column.id}>
				<Link onClick={() => selectColumn(column.id)}>
					{column.name}
				</Link>
			</EntryRow>
		);
	};
	
	const renderRelationship = (relationship: RelationshipData) => {
		return (
			<EntryRow key={relationship.id}>
				<Link onClick={() => selectRelationship(relationship.id)}>
					{relationship.name}
				</Link>
			</EntryRow>
		);
	};
	
	const renderPrimaryKey = (pk: PrimaryKeyData) => {
		return (
			<EntryRow key={pk.id}>
				<Link onClick={() => selectPrimaryKey(pk.id)}>
					{pk.name}
				</Link>
			</EntryRow>
		);
	};
	
	const renderIndex = (index: IndexData) => {
		return (
			<EntryRow key={index.id}>
				<Link onClick={() => selectIndex(index.id)}>
					{index.name}
				</Link>
			</EntryRow>
		);
	};
	
	const renderUnique = (unique: UniqueData) => {
		return (
			<EntryRow key={unique.id}>
				<Link onClick={() => selectUnique(unique.id)}>
					{unique.name}
				</Link>
			</EntryRow>
		);
	};
	
	return renderTree();
}

const EntryRow = styled.div`
	display: flex;
	flex-direction: row;
	
	> ${Link} {
		flex-grow: 1;
		overflow: hidden;
		text-overflow: ellipsis;
	}
`;

const SelectTableButton = styled.button`
	background: transparent;
	&:hover {
		color: white;
	}
`;

const Header = styled.div`
	display: flex;
	justify-content: space-between;
`;