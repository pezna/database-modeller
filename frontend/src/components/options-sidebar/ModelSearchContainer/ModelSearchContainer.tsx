import { alterUiStateProperty, performSearch, setSearchParameters } from "@/actions";
import { MachineResults } from "@/components/options-sidebar/ModelSearchContainer/MachineResults";
import { TableResults } from "@/components/options-sidebar/ModelSearchContainer/TableResults";
import { ScrollableContainer } from "@/components/widgets";
import { AppMode } from "@/data";
import { getManageSchemaSidebarUi, getSearchParameters } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import { useCallback, useEffect, useLayoutEffect, useRef } from "react";
import { Checkbox, Input, InputGroup } from "rsuite";
import { debounce } from "throttle-debounce";

interface SearchContainerProps {
	appMode: AppMode;
}

export function ModelSearchContainer({ appMode }: SearchContainerProps) {
	const dispatch = useDispatch();
	const searchParameters = useSelector(getSearchParameters);
	const settings = useSelector(getManageSchemaSidebarUi);
	const refContainer = useRef<HTMLDivElement>(null);
	const hasSearch = Boolean(searchParameters.searchText);
	
	useLayoutEffect(() => {
		const container = refContainer.current;
		if (container && container.scrollTop !== settings.searchScrollAmount) {
			container.scrollTop = settings.searchScrollAmount;
		}
	}, [settings.searchScrollAmount]);
	
	const handleScroll = (e: React.UIEvent<HTMLDivElement>) => {
		if (e.currentTarget.scrollTop !== settings.searchScrollAmount) {
			dispatch(alterUiStateProperty(["manageSchemaSidebar", "searchScrollAmount"], e.currentTarget.scrollTop));
		}
	};
	
	const forceSearch = useCallback(() => {
		doSearch(dispatch);
	}, [dispatch, searchParameters]);
	
	useEffect(() => {
		forceSearch();
	}, [forceSearch]);
	
	const handleSearchText = (e: React.FormEvent<HTMLInputElement>) => {
		dispatch(setSearchParameters({
			...searchParameters,
			searchText: e.currentTarget.value,
		}));
	};
	const handleRegexChange = (_: any, checked: boolean) => {
		dispatch(setSearchParameters({
			...searchParameters,
			isRegex: checked,
		}));
	};
	const handleCaseSensitiveChange = (_: any, checked: boolean) => {
		dispatch(setSearchParameters({
			...searchParameters,
			isCaseSensitive: checked,
		}));
	};
	const handleCurrentNamespaceChange = (_: any, checked: boolean) => {
		dispatch(setSearchParameters({
			...searchParameters,
			currentNamespaceOnly: checked,
		}));
	};
	
	const renderTree = () => {
		if (appMode === AppMode.machine) {
			return (
				<MachineResults
					hasSearch={hasSearch}
					forceSearch={forceSearch}
				/>
			);
		}
		
		return (
			<TableResults
				hasSearch={hasSearch}
				forceSearch={forceSearch}
				currentNamespaceOnly={searchParameters.currentNamespaceOnly}
			/>
		);
	};
	
	return (
		<ScrollableContainer ref={refContainer} onScroll={handleScroll}>
			<div>
				<InputGroup>
					<InputGroup.Addon>
						Search
					</InputGroup.Addon>
					<Input type="text" id="search-text" onInput={handleSearchText} value={searchParameters.searchText} />
					<InputGroup.Button onClick={forceSearch}>
						<span className="fa-solid fa-magnifying-glass"></span>
					</InputGroup.Button>
				</InputGroup>
			</div>
			<div>
				<Checkbox
					checked={searchParameters.isRegex}
					onChange={handleRegexChange}
					inline={true}
				>
					Regex
				</Checkbox>
				<Checkbox
					checked={searchParameters.isCaseSensitive}
					onChange={handleCaseSensitiveChange}
					inline={true}
				>
					Case-sensitive
				</Checkbox>
			</div>
			<div>
			{
				appMode === AppMode.database
				? (
					<Checkbox
						checked={searchParameters.currentNamespaceOnly}
						onChange={handleCurrentNamespaceChange}
						inline={true}
					>
						Current namespace only
					</Checkbox>
				)
				: null
			}
			</div>
			<div>
				{renderTree()}
			</div>
		</ScrollableContainer>
	);
}

const doSearch = debounce(100, (dispatch: (x: any) => any) => {
	dispatch(performSearch());
});
