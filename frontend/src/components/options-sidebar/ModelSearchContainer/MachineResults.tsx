import { alterUiStateProperty } from "@/actions";
import { Accordion, Link, SelectIcon } from "@/components/widgets";
import { ComponentData, ConnectorData, MachineData } from "@/data";
import { useModelNavigation } from "@/hooks";
import { getComponents, getConnectors, getMachines, getManageSchemaSidebarUi, getSearchResults } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import styled from "@emotion/styled";
import { useEffect, useLayoutEffect, useMemo, useRef } from "react";

interface MachineResultsProps {
	hasSearch: boolean;
	forceSearch: () => void;
}

export function MachineResults({ hasSearch, forceSearch }: MachineResultsProps) {
	const dispatch = useDispatch();
	const results = useSelector(getSearchResults);
	const machines = useSelector(getMachines);
	const components = useSelector(getComponents);
	const connectors = useSelector(getConnectors);
	const settings = useSelector(getManageSchemaSidebarUi);
	const openMachineIds = useMemo(() => new Set(settings.openMachineIds), [settings.openMachineIds]);
	const { selectMachine, selectComponent, selectConnector } = useModelNavigation();
	const refContainer = useRef<HTMLDivElement>(null);
	
	useLayoutEffect(() => {
		const container = refContainer.current;
		if (container && container.scrollTop !== settings.searchScrollAmount) {
			container.scrollTop = settings.searchScrollAmount;
		}
	}, [settings.searchScrollAmount]);
	
	useEffect(() => {
		forceSearch();
	}, [forceSearch, machines]);
	
	const expandGroup = (machineId: string, expanded: boolean) => {
		if (expanded) {
			openMachineIds.add(machineId);
		}
		else {
			openMachineIds.delete(machineId);
		}
		
		dispatch(alterUiStateProperty(["manageSchemaSidebar", "openMachineIds"], [...openMachineIds]));
	};
	
	const renderTree = () => {
		if (hasSearch) {
			const includeMachineIds = new Set<string>([
				...results.machines.map(x => x.id),
				...results.components.map(x => x.machineId),
				...results.connectors.map(x => x.machineId),
			]);
			
			const machineIds = [...includeMachineIds];
			
			return machineIds.map(mId => {
				const m = machines[mId];
				const comps = results.components.filter(x => x.machineId === m.id).map(x => components[x.id]);
				const conns = results.connectors.filter(x => x.machineId === m.id).map(x => connectors[x.id]);
				
				return renderMachine(machines[m.id], comps, conns);
			});
		}
		else {
			return Object.values(machines).map(m => {
				const comps = m.componentIds.map(cId => components[cId]);
				const conns = m.connectorIds.map(cId => connectors[cId]);
				
				return renderMachine(m, comps, conns);
			});
		}
	};
	
	const renderMachine = (machine: MachineData, components: ComponentData[], connectors: ConnectorData[]) => {
		return (
			<Accordion
				key={machine.id}
				heading={machine.name}
				expanded={openMachineIds.has(machine.id)}
				expandCallback={(e) => expandGroup(machine.id, e)}
			>
				<SelectIcon onClick={() => selectMachine(machine.id)} label="Select machine" />
				{
					components.length === 0
					? null
					: renderMachineGroup("Components", components.map(renderComponent))
				}
				{
					connectors.length === 0
					? null
					: renderMachineGroup("Connectors", connectors.map(renderConnector))
				}
			</Accordion>
		);
	};
	
	const renderMachineGroup = (heading: string, elements: React.ReactNode) => {
		return (
			<fieldset>
				<legend>{heading}</legend>
				{elements}
			</fieldset>
		);
	};
	
	const renderComponent = (component: ComponentData) => {
		return (
			<EntryRow key={component.id}>
				<Link onClick={() => selectComponent(component.id)}>
					{component.name}
				</Link>
			</EntryRow>
		);
	};
	
	const renderConnector = (connector: ConnectorData) => {
		return (
			<EntryRow key={connector.id}>
				<Link onClick={() => selectConnector(connector.id)}>
					{connector.name}
				</Link>
			</EntryRow>
		);
	};
	
	return (
		<>
			{renderTree()}
		</>
	);
}

const EntryRow = styled.div`
	display: flex;
	flex-direction: row;
	
	> ${Link} {
		flex-grow: 1;
		overflow: hidden;
		text-overflow: ellipsis;
	}
`;
