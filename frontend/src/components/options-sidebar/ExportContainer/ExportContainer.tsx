import { alterUiStateProperty } from "@/actions";
import { ExportHistory } from "@/components/options-sidebar/ExportHistory";
import { ExportTemplates } from "@/components/options-sidebar/ExportTemplates";
import { TabPanel } from "@/components/widgets";
import { ExportTab } from "@/data";
import { getExportSidebarUi } from "@/selectors";
import { useDispatch, useSelector } from "@/store";

export function ExportContainer() {
	const dispatch = useDispatch();
	const activeTab = useSelector(state => getExportSidebarUi(state).activeTab);
	
	const setActiveTab = (active: ExportTab | string) => {
		dispatch(alterUiStateProperty(["exportSidebar", "activeTab"], active));
	}
	
	return (
		<TabPanel active={activeTab} onTabSelect={setActiveTab}>
			<TabPanel.Item tabKey={ExportTab.exportHistory} title="Export History">
				<ExportHistory />
			</TabPanel.Item>
			<TabPanel.Item tabKey={ExportTab.customExportTemplates} title="Custom Export Templates">
				<ExportTemplates />
			</TabPanel.Item>
		</TabPanel>
	);
}
