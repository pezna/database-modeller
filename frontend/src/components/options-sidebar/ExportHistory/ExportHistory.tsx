import { alterUiStateProperty } from "@/actions";
import { ExportEntry } from "@/components/options-sidebar/ExportHistory/ExportEntry";
import { Accordion, ScrollableContainer } from "@/components/widgets";
import { SqlExportHistoryItem } from "@/data";
import { getExportHistory, getExportSidebarUi, getSortedExportHistory } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import styled from "@emotion/styled";
import { useLayoutEffect, useRef } from "react";

const dateTimeFormat = new Intl.DateTimeFormat(undefined, {
	dateStyle: 'medium',
	timeStyle: 'medium',
});

export function ExportHistory() {
	const dispatch = useDispatch();
	const sidebarSettings = useSelector(getExportSidebarUi);
	const exportHistory = useSelector(getExportHistory);
	const sortedExportHistory = useSelector(getSortedExportHistory);
	const refContainer = useRef<HTMLDivElement>(null);
	
	useLayoutEffect(() => {
		const container = refContainer.current;
		if (container && container.scrollTop !== sidebarSettings.historyScrollAmount) {
			container.scrollTop = sidebarSettings.historyScrollAmount;
		}
	}, [sidebarSettings]);
	
	const handleScroll = (e: React.UIEvent<HTMLDivElement>) => {
		if (e.currentTarget.scrollTop !== sidebarSettings.historyScrollAmount) {
			dispatch(alterUiStateProperty(['exportSidebar', 'historyScrollAmount'], e.currentTarget.scrollTop));
		}
	};
	
	const handleToggleExpand = (e: boolean, id: string) => {
		const openIds = new Set(sidebarSettings.historyOpenExportIds);
		
		if (e) {
			openIds.add(id);
		}
		else {
			openIds.delete(id);
		}
		
		dispatch(alterUiStateProperty(['exportSidebar', 'historyOpenExportIds'], [...openIds]));
	};
	
	const renderExportHeading = (data: SqlExportHistoryItem) => {
		const date = new Date(data.time);
		const dateText = dateTimeFormat.format(date);
		
		return (
			<div className="export-heading">
				<span className="date">{dateText}</span>
				<span className="type">{data.type}</span>
			</div>
		);
	};
	
	const renderExportItem = (data: SqlExportHistoryItem) => {
		const prev = exportHistory[data.parentId ?? ''] ?? {
			...data,
			id: '',
			namespaces: {},
			tables: {},
			columns: {},
			primaryKeys: {},
			relationships: {},
			indexes: {},
			uniques: {},
		};
		
		return (
			<Accordion
				key={data.id}
				heading={renderExportHeading(data)}
				expanded={sidebarSettings.historyOpenExportIds.includes(data.id)}
				expandCallback={(e) => handleToggleExpand(e, data.id)}
			>
				<ExportEntry
					entry={data}
					previousEntry={prev}
				/>
			</Accordion>
		);
	};
	
	return (
		<ExportHistoryWrapper ref={refContainer} onScroll={handleScroll}>
			{sortedExportHistory.map(renderExportItem)}
		</ExportHistoryWrapper>
	);
}

const ExportHistoryWrapper = styled(ScrollableContainer)`
	.export-heading {
		display: flex;
		flex-direction: row;
		padding-right: 5px;
		
		.date {
			flex-grow: 1;
		}
		
		.type {
			color: cyan;
		}
	}
	
	.export-entry {
		.table-info {
			color: yellow;
		}
		
		.info-item {
			margin-left: 20px;
		}
	}
`;