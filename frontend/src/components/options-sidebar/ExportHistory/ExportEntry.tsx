import { Link } from "@/components/widgets";
import { SchemaItemKey, SqlExportHistoryItem, getModelObjectType, getSchemaItemName } from "@/data";
import { useModelNavigation } from "@/hooks";
import { Diff, PerTableSchemaDiff, SchemaDiff, createPerTableSchemaDiff, createSchemaDiff, mergeSchema } from "@/utils";
import { ReactNode, useEffect, useMemo, useState } from "react";

type ExportEntryProps = {
	entry: SqlExportHistoryItem;
	previousEntry: SqlExportHistoryItem;
}

export function ExportEntry({ entry, previousEntry }: ExportEntryProps) {
	const {select, selectTable} = useModelNavigation();
	const [tableSchemaDiffs, setTableSchemaDiffs] = useState<PerTableSchemaDiff>();
	const [combinedSchema, schemaDiff] = useMemo(() => {
		return [
			mergeSchema(previousEntry, entry),
			createSchemaDiff(previousEntry, entry),
		];
	}, [entry, previousEntry]);
	
	useEffect(() => {
		if (tableSchemaDiffs === undefined) {
			setTableSchemaDiffs(createPerTableSchemaDiff(schemaDiff, combinedSchema));
		}
	}, [schemaDiff, combinedSchema, tableSchemaDiffs]);
	
	const renderTableDiff = (tableId: string, tableSchemaDiff: SchemaDiff) => {
		if (combinedSchema.tables[tableId].isMixin) {
			return null;
		}
		
		const tableInfo = renderItemDiff(tableSchemaDiff.tables[tableId], 'tables', 'table-info', 'info-item');
		const columnInfo = Object.values(tableSchemaDiff.columns)
			.map(x => renderItemDiff(x, 'columns', 'info-item'))
			.flatMap(x => x);
		const primaryKeyInfo = Object.values(tableSchemaDiff.primaryKeys)
			.map(x => renderItemDiff(x, 'primaryKeys', 'info-item'))
			.flatMap(x => x);
		const relationshipInfo = Object.values(tableSchemaDiff.relationships)
			.map(x => renderItemDiff(x, 'relationships', 'info-item'))
			.flatMap(x => x);
		const indexInfo = Object.values(tableSchemaDiff.indexes)
			.map(x => renderItemDiff(x, 'indexes', 'info-item'))
			.flatMap(x => x);
		const uniqueInfo = Object.values(tableSchemaDiff.uniques)
			.map(x => renderItemDiff(x, 'uniques', 'info-item'))
			.flatMap(x => x);
		const namespaceInfo = Object.values(tableSchemaDiff.namespaces)
			.map(x => renderItemDiff(x, 'namespaces', 'info-item'))
			.flatMap(x => x);
		
		const ret = [
			...tableInfo,
			...columnInfo,
			...primaryKeyInfo,
			...relationshipInfo,
			...indexInfo,
			...uniqueInfo,
			...namespaceInfo,
		];
		
		if (tableInfo.length === 0 && ret.length > 0) {
			const tableName = previousEntry.tables[tableId]!.name;
			ret.splice(0, 0, renderAlterTable(tableId, tableName, 'table-info'));
		}
		
		return ret;
	};
	
	const renderItemDiff = <T extends Diff<any>>(diff: T, type: SchemaItemKey, className1: string, className2?: string) => {
		if (diff.propertyChanges.length === 0) {
			return [];
		}
		
		const schemaItemName = getSchemaItemName(type);
		const objectType = getModelObjectType(type);
		const handleSelectLink = () => select(objectType, diff.id);
		
		const oldName = previousEntry[type][diff.id]?.name;
		const newName = entry[type][diff.id]?.name;
		
		const elements: ReactNode[] = [];
		
		if (diff.isNew) {
			elements.push(
				<div key={`1-${diff.id}`} className={className1}>
					Created {schemaItemName} <Link onClick={handleSelectLink}>{newName}</Link>
				</div>
			);
		}
		else if (diff.isDeleted) {
			elements.push(
				<div key={`2-${diff.id}`} className={className1}>
					Deleted {schemaItemName} {oldName}
				</div>
			);
		}
		else {
			const changedName = !diff.isNew && oldName !== newName;
			if ((changedName && diff.propertyChanges.length > 1) || diff.propertyChanges.length > 0) {
				elements.push(
					<div key={`3-${diff.id}`} className={className1}>
						Altered {schemaItemName} <Link onClick={handleSelectLink}>{oldName}</Link>
					</div>
				);
			}
			
			if (changedName) {
				elements.push(
					<div key={`4-${diff.id}`} className={className2 || className1}>
						Renamed {schemaItemName} {oldName} to <Link onClick={handleSelectLink}>{newName}</Link>
					</div>
				);
			}
		}
		
		return elements;
	};
	
	const renderAlterTable = (id: string, name: string, className: string) => {
		return (
			<div key={`5-${id}`} className={className}>
				Altered table <Link onClick={() => selectTable(id)}>{name}</Link>
			</div>
		);
	}
	
	return (
		<div className="export-entry">
			<div>ID: {entry.id}</div>
			<div>Parent ID: {entry.parentId}</div>
			{
				tableSchemaDiffs
				? Object.entries(tableSchemaDiffs).map(([tableId, schemaDiff]) => renderTableDiff(tableId, schemaDiff))
				: null
			}
		</div>
	);
}
