import { DimensionsData } from "@/data";

export type ColumnDimensionsCallback = (tableId: string, columnId: string, dimensions: DimensionsData) => void;

export type TableDimensionsCallback = (tableId: string, dimensions: DimensionsData) => void;
