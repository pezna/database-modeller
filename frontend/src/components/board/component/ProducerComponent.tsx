import { ColorData, ComponentData, DimensionsData } from "@/data";

interface ProducerComponentProps {
	component: ComponentData;
	dimensions: DimensionsData;
	defaultColor: ColorData;
}

export function ProducerComponent({ dimensions, defaultColor }: ProducerComponentProps) {
	const centerX = dimensions.width / 2;
	const centerY = dimensions.height / 2;
	const radius = dimensions.width / 2 - 2;
	const foregroundColor = defaultColor.foregroundColor;
	const backgroundColor = defaultColor.backgroundColor;
	
	return (
		<svg>
			<circle
				cx={centerX}
				cy={centerY}
				r={radius}
				stroke={foregroundColor}
				strokeWidth="2"
				fill={backgroundColor}
			/>
		</svg>
	);
}
