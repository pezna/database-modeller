import { ColorData, ComponentData, DimensionsData, LSystemComponentData } from "@/data";
import { lsystemManager } from "@/rxjs";
import { diagramService } from "@/services";
import styled from "@emotion/styled";
import { useEffect, useState } from "react";
import { Button, Checkbox, Popover, Radio, RadioGroup, Whisper } from "rsuite";

interface SvgViewOptions {
	centerX0: boolean;
	centerY0: boolean;
	viewBoxMultiplier: number;
	diagramSize: number;
	showZeroAxis: boolean;
}

interface LSystemComponentProps {
	component: ComponentData;
	dimensions: DimensionsData;
	defaultColor: ColorData;
}

export function LSystemComponent({ component, dimensions, defaultColor }: LSystemComponentProps) {
	const lsystem = component as LSystemComponentData;
	const [currentStep, setCurrentStep] = useState(0);
	const [paths, setPaths] = useState<string[]>([]);
	const [svgViewOptions, setSvgViewOptions] = useState<SvgViewOptions>({
		centerX0: false,
		centerY0: false,
		viewBoxMultiplier: 1,
		diagramSize: dimensions.width,
		showZeroAxis: true,
	});
	
	const reactive = lsystemManager.getLSystemReactive(component.id);
	
	useEffect(() => {
		const s1 = reactive.observeCurrentStep({
			next: setCurrentStep,
		});
		const s2 = reactive.observeAxiom({
			next: () => {
				reactive.movementTurtle.resetPositions();
				// const context = reactive.runGeneratorFinals();
				// setPaths(renderPaths(context.paths));
			},
		});
		
		return () => {
			s1.unsubscribe();
			s2.unsubscribe();
		};
	}, [component.id]);
	
	const handleSetViewOptions = (opts: SvgViewOptions) => {
		setSvgViewOptions(opts);
		if (dimensions.width !== opts.diagramSize) {
			const dim = {
				...dimensions,
				width: opts.diagramSize,
				height: opts.diagramSize,
			};
			diagramService.sendItemDimensions(component.id, component.machineId, dim);
		}
	};
	
	const calcViewbox = () => {
		let w = svgViewOptions.diagramSize;
		let h = svgViewOptions.diagramSize;
		let x = 0;
		let y = 0;
		
		if (svgViewOptions.centerX0) {
			x -= w/2;
		}
		
		if (svgViewOptions.centerY0) {
			y -= h/2;
		}
		
		x *= svgViewOptions.viewBoxMultiplier;
		y *= svgViewOptions.viewBoxMultiplier;
		w *= svgViewOptions.viewBoxMultiplier;
		h *= svgViewOptions.viewBoxMultiplier;
		
		return x + " " + y + " " + w + " " + h;
	};
	
	const renderAxis = () => {
		if (!svgViewOptions.showZeroAxis) {
			return null;
		}
		
		return (
			<>
				<line x1="-3000" x2="3000" y1="0" y2="0" stroke="black" strokeDasharray={5} />
				<line x1="0" x2="0" y1="-3000" y2="3000" stroke="black" strokeDasharray={5} />
			</>
		);
	};
	
	return (
		<>
			<svg viewBox={calcViewbox()} style={{background: defaultColor.backgroundColor}}>
				{renderAxis()}
				{
					paths.map((p, i) => (
						<path key={i} d={p} fill="none" stroke={defaultColor.foregroundColor} />
					))
				}
				<text x="10" y="20">{currentStep} / {lsystem.maxIterations}</text>
			</svg>
			<Container>
				<Whisper
					preventOverflow
					trigger="click"
					placement="auto"
					speaker={(
						<Popover>
							<PopupOptions
								options={svgViewOptions}
								onChange={handleSetViewOptions}
							/>
						</Popover>
					)}
				>
					<Button size="xs" appearance="subtle">
						<span className="fa-solid fa-gear"></span>
					</Button>
				</Whisper>
			</Container>
		</>
	);
}

const Container = styled.div`
	z-index: 100;
`;

interface PopupOptionsProps {
	options: SvgViewOptions;
	onChange: (val: SvgViewOptions) => void;
}

function PopupOptions({ options, onChange }: PopupOptionsProps) {
	const handleChange = <K extends keyof SvgViewOptions>(key: K, val: SvgViewOptions[K]) => {
		console.log(key, val);
		onChange({
			...options,
			[key]: val,
		});
	};
	
	const style = {
		marginLeft: "12px",
		marginTop: "12px",
		marginBottom: "12px",
	};
	
	return (
		<div>
			<div>
				<Checkbox
					checked={options.showZeroAxis}
					onChange={(_, chk) => handleChange("showZeroAxis", chk as boolean)}
				>
					Show Axis Lines
				</Checkbox>
			</div>
			<div style={style}>
				<label>Diagram Size</label>
				<RadioGroup inline value={options.diagramSize} onChange={(val) => handleChange("diagramSize", val as number)}>
					<Radio value={50}>50x50</Radio>
					<Radio value={200}>200x200</Radio>
					<Radio value={400}>400x400</Radio>
					<Radio value={600}>600x600</Radio>
					<Radio value={800}>800x800</Radio>
					<Radio value={1000}>1000x1000</Radio>
				</RadioGroup>
			</div>
			<div style={style}>
				<label>Viewbox Zoom Out</label>
				<RadioGroup inline value={options.viewBoxMultiplier} onChange={(val) => handleChange("viewBoxMultiplier", val as number)}>
					<Radio value={0.5}>0.5x</Radio>
					<Radio value={1}>1x</Radio>
					<Radio value={2}>2x</Radio>
					<Radio value={3}>3x</Radio>
				</RadioGroup>
			</div>
			<div>
				<Checkbox
					checked={options.centerX0}
					onChange={(_, chk) => handleChange("centerX0", chk as boolean)}
				>
					Center X=0 on diagram
				</Checkbox>
				<Checkbox
					checked={options.centerY0}
					onChange={(_, chk) => handleChange("centerY0", chk as boolean)}
				>
					Center Y=0 on diagram
				</Checkbox>
			</div>
		</div>
	);
}

function renderPaths(paths: [number, number][][]): string[] {
	const res = [];
	for (const section of paths) {
		let path = `M ${section[0][0].toFixed(2)},${section[0][1].toFixed(2)}`;
		for (let i = 1; i < section.length; ++i) {
			path += ` L ${section[i][0].toFixed(2)},${section[i][1].toFixed(2)}`;
		}
		
		res.push(path);
	}
	return res;
}
