import { selectComponent } from "@/actions";
import { useRightClickContext } from "@/contexts";
import { ComponentData, DimensionsData, ModelObjectType } from "@/data";
import { getIsComponentSelected, getMachineBoardSettings } from "@/selectors";
import { diagramService } from "@/services";
import { useDispatch, useSelector } from "@/store";
import styled from "@emotion/styled";
import { useCallback, useEffect, useRef, useState } from "react";
import Draggable, { DraggableData, DraggableEvent } from "react-draggable";
import { boardDraggableBounds } from "../shared-board-utils";
import { getMachineComponentRenderer } from "./machine-component";

interface MachineComponentProps {
	component: ComponentData;
	onSelectCallback?: (component: ComponentData) => void;
}

export function MachineComponent({ component, onSelectCallback }: MachineComponentProps) {
	const dispatch = useDispatch();
	const { showMenu } = useRightClickContext();
	const isSelected = useSelector(state => getIsComponentSelected(state, component.id));
	const boardSettings = useSelector(getMachineBoardSettings);
	const [_, setForceRefresh] = useState(false);
	const elementRef = useRef<HTMLDivElement>({} as any);
	const dimensions = diagramService.getItemDimensions(component.id)!;
	
	const diagramCallback = useCallback(() => {
		setForceRefresh((v) => !v);
	}, []);
	
	useEffect(() => {
		diagramService.addItemListener(component.id, "listener" + component.id, diagramCallback);
		
		return () => {
			diagramService.removeItemListener(component.id, "listener" + component.id);
		};
	}, [component.id]);
	
	const selectItem = () => {
		dispatch(selectComponent(component.id));
		onSelectCallback?.(component);
	};
	
	const handleDragCallback = (e: DraggableEvent, data: DraggableData) => {
		diagramService.sendItemDimensions(component.id, component.machineId, {
			...dimensions,
			x: data.x,
			y: data.y,
		});
	};
	
	const handleRightClick = (e: React.MouseEvent) => {
		e.preventDefault();
		e.stopPropagation();
		showMenu({ x: e.pageX, y: e.pageY, boardX: 0, boardY: 0 }, ModelObjectType.component, component.id);
	};
	
	const R = getMachineComponentRenderer(component.type);
	
	return (
		<Draggable
			bounds={boardDraggableBounds}
			defaultPosition={dimensions}
			onMouseDown={selectItem}
			onStart={handleDragCallback}
			onDrag={handleDragCallback}
			onStop={handleDragCallback}
			nodeRef={elementRef}
		>
			<ComponentWrapper
				ref={elementRef}
				className="machine-component"
				dimensions={dimensions}
				isSelected={isSelected}
				onContextMenu={handleRightClick}
			>
				<R
					component={component}
					dimensions={dimensions}
					defaultColor={boardSettings.components}
				/>
				<Label componentWidth={dimensions.width} foreground={boardSettings.components.foregroundColor}>
					{component.name}
				</Label>
				<div className="selected-border"></div>
			</ComponentWrapper>
		</Draggable>
	);
}

interface ComponentWrapperProps {
	dimensions: DimensionsData;
	isSelected: boolean;
}

const ComponentWrapper = styled.div<ComponentWrapperProps>`
	position: absolute;
	display: flex;
	flex-direction: column;
	align-items: center;
	width: ${props => props.dimensions.width}px;
	min-height: ${props => props.dimensions.height}px;
	z-index: ${props => props.isSelected ? 1000 : 'initial'};
	background: none;
	cursor: pointer;
	
	> svg {
		width: ${props => props.dimensions.width}px;
		height: ${props => props.dimensions.height}px;
		
		path, circle, rect {
			/* filter: ${props => props.isSelected ? "drop-shadow(0px 0px 8px rgba(0, 0, 0, 0.75))" : "none"}; */
		}
	}
	
	.selected-border {
		position: absolute;
		inset: ${props => props.isSelected ? "-10px" : "0"};
		box-shadow: ${props => props.isSelected ? "0px 4px 8px rgba(50, 50, 50, 0.75)" : "none"};
	}
`;

interface LabelProps {
	componentWidth: number;
	foreground: string;
}

const Label = styled.div<LabelProps>`
	word-wrap: break-word;
	max-width: ${props => props.componentWidth * 3}px;
	color: ${props => props.foreground ?? "inherit"};
	text-align: center;
`;
