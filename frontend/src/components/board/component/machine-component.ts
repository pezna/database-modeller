import { ComponentType } from "@/data";
import { BucketComponent } from "./BucketComponent";
import { LSystemComponent } from "./LSystemComponent";
import { ProducerComponent } from "./ProducerComponent";

export function getMachineComponentRenderer(componentType: ComponentType) {
	switch (componentType) {
		case ComponentType.producer:
			return ProducerComponent;
		case ComponentType.bucket:
			return BucketComponent;
		case ComponentType.lsystem:
			return LSystemComponent;
	}
	
	throw new Error(`No component to render machine component type: ${componentType}`);
}
