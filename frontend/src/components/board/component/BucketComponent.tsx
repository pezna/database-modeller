import { ColorData, ComponentData, DimensionsData } from "@/data";

interface BucketComponentProps {
	component: ComponentData;
	dimensions: DimensionsData;
	defaultColor: ColorData;
}

export function BucketComponent({ dimensions, defaultColor }: BucketComponentProps) {
	const foregroundColor = defaultColor.foregroundColor;
	const backgroundColor = defaultColor.backgroundColor;
	
	return (
		<svg>
			<path
				d={`M 1,1
					L 1,10
					L 10,${dimensions.height - 1}
					L ${dimensions.width - 10},${dimensions.height - 1}
					L ${dimensions.width - 1},10
					L ${dimensions.width - 1},1
					L 1,1
					M 1,10
					L ${dimensions.width - 1},10`}
				stroke={foregroundColor}
				strokeWidth="2"
				fill={backgroundColor}
			/>
		</svg>
	);
}
