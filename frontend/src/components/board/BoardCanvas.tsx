import { deselectBoard } from "@/actions";
import { useBoardItemCreation } from "@/contexts";
import { BoardTool, isToolConnector } from "@/data";
import { getIsAnyItemSelected, getSelectedComponentId, getSelectedTableId } from "@/selectors";
import { diagramService } from "@/services";
import { useDispatch, useSelector } from "@/store";
import styled from "@emotion/styled";
import { useCallback, useLayoutEffect, useRef } from "react";
import { determineScrollAmount } from "./shared-board-utils";

interface BoardCanvasProps {
	boardId: string;
	connectionSvgRef: React.RefObject<SVGSVGElement | null>;
	performToolAction: (tool: BoardTool, x: number, y: number) => void;
	onMouseMove?: (e: React.MouseEvent) => void;
	onRightClick?: (e: React.MouseEvent, x: number, y: number) => void;
}

export function BoardCanvas({
	boardId,
	connectionSvgRef,
	performToolAction,
	onMouseMove,
	onRightClick,
	children,
}: React.PropsWithChildren<BoardCanvasProps>) {
	const dispatch = useDispatch();
	const { tool } = useBoardItemCreation();
	const selectedTableId = useSelector(getSelectedTableId);
	const selectedComponentId = useSelector(getSelectedComponentId);
	const isAnySelected = useSelector(getIsAnyItemSelected);
	const boardRef = useRef<HTMLDivElement>(null);
	
	const diagramCallback = useCallback(() => {
		const boardDimensions = diagramService.getBoardDimensions(boardId);
		if (!boardDimensions) {
			return;
		}
		
		const svg = connectionSvgRef.current;
		if (!svg) {
			return;
		}
		
		svg.style.minWidth = `${boardDimensions.width}px`;
		svg.style.minHeight = `${boardDimensions.height}px`;
	}, [boardId]);
	
	useLayoutEffect(() => {
		diagramService.addUniversalListener(boardId, diagramCallback);
		diagramCallback();
		
		return () => {
			diagramService.removeUniversalListener(boardId);
		};
	}, [diagramCallback, boardId]);
	
	// When a table or component is programmatically selected (via navigation hook)
	// This will scroll the board to the show the item
	useLayoutEffect(() => {
		if (!boardRef.current) {
			return;
		}
		
		const itemDims = diagramService.getItemDimensions(selectedTableId ?? selectedComponentId ?? "");
		if (!itemDims) {
			return;
		}
		
		const scrollAmount = determineScrollAmount({
			x: boardRef.current.scrollLeft,
			y: boardRef.current.scrollTop,
			width: boardRef.current.clientWidth,
			height: boardRef.current.clientHeight,
		}, itemDims);
		
		boardRef.current.scrollLeft += scrollAmount.deltaX;
		boardRef.current.scrollTop += scrollAmount.deltaY;
		
	}, [selectedTableId, selectedComponentId]);
	
	const handleClickBoard = (e: React.MouseEvent<HTMLElement>) => {
		const boardCanvas: HTMLElement = getElementWithBoardParent(e.currentTarget);
		const target = e.target as Element;
		if (!target.classList.contains("board-canvas") && target.nodeName !== "svg") {
			return;
		}
		const board = boardCanvas.parentElement!;
		const x = Math.max((e.clientX - boardCanvas.offsetLeft - board.offsetLeft) + boardCanvas.scrollLeft, 0);
		const y = Math.max((e.clientY - boardCanvas.offsetTop - board.offsetTop) + boardCanvas.scrollTop, 0);
		
		if (e.button === 0) {
			leftClick(e, x, y);
		}
		else if (e.button === 2) {
			onRightClick?.(e, x, y);
		}
	};
	
	const leftClick = (e: React.MouseEvent<HTMLElement>, x: number, y: number) => {
		if (isAnySelected) {
			//deselect because the board was clicked
			dispatch(deselectBoard());
		}
		
		if (tool) {
			performToolAction(tool, x, y);
		}
	};
	
	return (
		<CanvasWrapper
			ref={boardRef}
			className="board-canvas"
			onClick={handleClickBoard}
			onMouseMove={onMouseMove}
			isToolSelected={Boolean(tool)}
			highlightComponent={isToolConnector(tool)}
			onContextMenu={handleClickBoard}
		>
			{children}
		</CanvasWrapper>
	);
};

function getElementWithBoardParent(e: HTMLElement): HTMLElement {
	if (!e.parentElement) {
		throw new Error("HTML Element does not have a parent while trying to find '.board'. This should never happen");
	}
	
	if (e.parentElement.classList.contains("board")) {
		return e;
	}
	
	return getElementWithBoardParent(e.parentElement);
}

interface WrapperProps {
	isToolSelected: boolean;
	highlightComponent: boolean;
}

const CanvasWrapper = styled.div<WrapperProps>`
	position: relative;
	flex-grow: 1;
	overflow: auto;
	user-select: none;
	-moz-user-select: none;
	-webkit-user-select: none;
	cursor: ${props => props.isToolSelected ? "crosshair" : "default"};
	
	.connections-svg {
		position: absolute;
		/* width: 100%;
		height: 100%; */
	}
	
	.machine-component:hover {
		svg {
			${
				props => props.highlightComponent
				? "filter: drop-shadow(0 0 10px #ffff00) brightness(1.5);"
				: ""
			}
		}
	}
`;
