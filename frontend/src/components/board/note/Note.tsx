import { alterNoteProperty, selectNote } from "@/actions";
import { useRightClickContext } from "@/contexts";
import { DimensionsData, ModelObjectType, NoteData } from "@/data";
import { getIsNoteSelected } from "@/selectors";
import { diagramService } from "@/services";
import { useDispatch, useSelector } from "@/store";
import styled from "@emotion/styled";
import React, { useRef } from "react";
import Draggable, { DraggableData, DraggableEvent } from "react-draggable";
import { Input } from "rsuite";
import { boardDraggableBounds } from "../shared-board-utils";

interface NoteProps {
	note: NoteData;
}

export function Note({ note }: NoteProps) {
	const dispatch = useDispatch();
	const { showMenu } = useRightClickContext();
	const isSelected = useSelector(state => getIsNoteSelected(state, note.id));
	const dimensions = diagramService.getItemDimensions(note.id)!;
	const tempDimensions = useRef({
		...dimensions,
	});
	const noteRef = useRef<HTMLDivElement>({} as any);
	
	const sendDimensions = () => {
		diagramService.sendItemDimensions(
			note.id,
			note.sectionId,
			tempDimensions.current,
		);
	};
	
	const selectItem = () => dispatch(selectNote(note.id));
	
	const handleDragCallback = (e: DraggableEvent, data: DraggableData) => {
		tempDimensions.current.x = data.x;
		tempDimensions.current.y = data.y;
		sendDimensions();
	};
	
	const mouseUp = (e: React.MouseEvent) => {
		tempDimensions.current.width = noteRef.current!.offsetWidth;
		tempDimensions.current.height = noteRef.current!.offsetHeight;
		// save note dimensions in case the textarea size is changed
		sendDimensions();
	};
	
	const mouseDown = (e: React.MouseEvent) => {
		e.stopPropagation();
	};
	
	const changeText = (val: string) => {
		dispatch(alterNoteProperty(note.id, "text", val));
	};
	
	const handleRightClick = (e: React.MouseEvent) => {
		e.preventDefault();
		e.stopPropagation();
		showMenu({ x: e.pageX, y: e.pageY, boardX: 0, boardY: 0 }, ModelObjectType.note, note.id);
	};
	
	return (
		<Draggable
			bounds={boardDraggableBounds}
			defaultPosition={dimensions}
			onMouseDown={selectItem}
			onStart={handleDragCallback}
			onDrag={handleDragCallback}
			onStop={handleDragCallback}
			nodeRef={noteRef}
		>
			<Wrapper
				ref={noteRef}
				isSelected={isSelected}
				dimensions={dimensions}
				onContextMenu={handleRightClick}
			>
				<Input
					as="textarea"
					onChange={changeText}
					onMouseDown={mouseDown}
					onMouseUp={mouseUp}
					value={note.text}
				/>
			</Wrapper>
		</Draggable>
	);
}

interface WrapperProps {
	dimensions: DimensionsData;
	isSelected: boolean;
}

const Wrapper = styled.div<WrapperProps>`
	position: absolute;
	background: white;
	padding: 8px;
	border: 1px solid #444;
	border-radius: 12px;
	min-height: fit-content;
	min-width: fit-content;
	box-shadow: ${props => props.isSelected ? "0px 10px 28px 0px rgba(0, 0, 0, 0.75)" : "none"};
	z-index: ${props => props.isSelected ? "1000" : "initial"};
	
	textarea {
		resize: both !important;
		width: ${props => props.dimensions.width - 18}px;
		height: ${props => props.dimensions.height - 18}px;
	}
`;
