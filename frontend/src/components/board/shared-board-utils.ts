import { DimensionsData } from "@/data";
import { DraggableBounds } from "react-draggable";

export function determineScrollAmount(boardRect: DimensionsData, itemRect: DimensionsData) {
	if (checkRectsOverlap(boardRect, itemRect)) {
		return {
			deltaX: 0,
			deltaY: 0,
		};
	}
	
	const boardRight = boardRect.x + boardRect.width;
	const boardBottom = boardRect.y + boardRect.height;
	const itemRight = itemRect.x + itemRect.width;
	const itemBottom = itemRect.y + itemRect.height;
	let deltaX = 0;
	let deltaY = 0;
	
	if (itemRight >= boardRight) {
		deltaX = (itemRect.x - boardRight) + itemRect.width;
	}
	else if (itemRect.x < boardRect.x) {
		deltaX = itemRect.x - boardRect.x;
	}
	
	if (itemBottom >= boardBottom) {
		deltaY = (itemRect.y - boardBottom) + itemRect.height;
	}
	else if (itemRect.y < boardRect.y) {
		deltaY = itemRect.y - boardRect.y;
	}
	
	return {
		deltaX,
		deltaY,
	};
}

function checkRectsOverlap(rect1: DimensionsData, rect2: DimensionsData): boolean {
	if (rect1.x + rect1.width < rect2.x) {
		return false;
	}

	if (rect2.x + rect2.width < rect1.x) {
		return false;
	}

	if (rect1.y + rect1.height < rect2.y) {
		return false;
	}

	if (rect2.y + rect2.height < rect1.y) {
		return false;
	}

	return true;
}

export const boardDraggableBounds: DraggableBounds = {
	left: 0,
	top: 0,
	right: Number.MAX_SAFE_INTEGER,
	bottom: Number.MAX_SAFE_INTEGER,
};
