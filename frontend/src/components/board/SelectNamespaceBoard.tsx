import { useModelCreation } from "@/hooks";
import styled from "@emotion/styled";
import { Button } from "rsuite";

export function SelectNamespaceBoard() {
	const { createNamespace } = useModelCreation();
	return (
		<Wrapper>
			<Button onClick={() => createNamespace(null)}>
				New Namespace
			</Button>
		</Wrapper>
	);
}

const Wrapper = styled.div`
	display: flex;
	flex-direction: column;
	gap: 1em;
	align-items: center;
	justify-content: center;
	height: 100%;
	
	h6 {
		text-align: center;
		color: var(--rs-bg-card);
	}
	
	.rs-list {
		max-height: 300px;
	}
`;
