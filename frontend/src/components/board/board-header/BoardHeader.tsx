import { AppMode } from "@/data";
import styled from "@emotion/styled";
import { MachineBoardTabs } from "./MachineBoardTabs";
import { NamespaceBoardTabs } from "./NamespaceBoardTabs";
import { ToolList } from "./ToolList";

interface BoardHeaderProps {
	appMode: AppMode;
	openTabIds: string[];
	currentTabId: string;
}

export function BoardHeader({ appMode, openTabIds, currentTabId }: BoardHeaderProps) {
	const renderAppModeHeader = () => {
		if (appMode === AppMode.database) {
			return (
				<NamespaceBoardTabs
					openNamespaceTabs={openTabIds}
					currentNamespaceId={currentTabId}
				/>
			);
		}
		else if (appMode === AppMode.machine) {
			return (
				<MachineBoardTabs
					openMachineTabs={openTabIds}
					currentMachineId={currentTabId}
				/>
			);
		}
	};
	
	return (
		<Wrapper className="board-header">
			{renderAppModeHeader()}
			<ToolList appMode={appMode} />
		</Wrapper>
	);
}

const Wrapper = styled.div`
	display: grid;
	grid-template-columns: 1fr;
	grid-template-rows: repeat(2, min-content);
	grid-auto-flow: row;
	border-bottom: 1px solid ${props => props.theme.sidebar.buttonBarBackgroundColor};
	background: ${props => props.theme.board.headerBackgroundColor};
	
	.tool-list {
		margin: 0.5em 0;
		margin-left: 0.5em;
	}
	
	.rs-nav {
		background: ${props => props.theme.sidebar.buttonBarBackgroundColor};
	}
`;

