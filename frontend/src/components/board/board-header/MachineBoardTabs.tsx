import { alterUiStateProperty, selectMachine } from "@/actions";
import { getMachines } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import ResponsiveNav from "@rsuite/responsive-nav";

interface MachineBoardTabsProps {
	openMachineTabs: string[];
	currentMachineId: string;
}

export function MachineBoardTabs({ openMachineTabs, currentMachineId }: MachineBoardTabsProps) {
	const dispatch = useDispatch();
	const machines = useSelector(getMachines);
	
	const handleSelectMachine = (eventKey: string | number | undefined) => {
		if (typeof eventKey !== "string") {
			return;
		}
		
		dispatch(selectMachine(eventKey));
	};
	
	const handleRemoveMachine = (eventKey: string | number) => {
		if (typeof eventKey !== "string") {
			return;
		}
		
		const openTabIds = [...openMachineTabs];
		const index = openTabIds.indexOf(eventKey);
		openTabIds.splice(index, 1);
		dispatch(alterUiStateProperty(["machineModeUi", "openMachineTabs"], openTabIds));
		
		if (eventKey === currentMachineId) {
			const nextId = openTabIds[Math.max(0, index - 1)] ?? null;
			dispatch(selectMachine(nextId));
		}
	};
	
	return (
		<ResponsiveNav
			appearance="tabs"
			activeKey={currentMachineId}
			onSelect={handleSelectMachine}
			onItemRemove={handleRemoveMachine}
			removable
		>
			{
				openMachineTabs.map(id => (
					<ResponsiveNav.Item key={id} eventKey={id}>
						{machines[id].name}
					</ResponsiveNav.Item>
				))
			}
		</ResponsiveNav>
	);
}
