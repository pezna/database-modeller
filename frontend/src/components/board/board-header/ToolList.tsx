import { useBoardItemCreation } from "@/contexts";
import { AppMode, BoardTool, boardToolAppMode, boardToolDisplayName } from "@/data";
import styled from "@emotion/styled";
import { useEffect } from "react";
import { Button, Stack, Tooltip, Whisper } from "rsuite";

interface ToolListProps {
	appMode: AppMode;
	onToolSelect?: (tool: BoardTool | null) => void;
}

export function ToolList({ appMode, onToolSelect }: ToolListProps) {
	const { tool, setTool } = useBoardItemCreation();
	
	useEffect(() => {
		return () => setTool(null);
	}, [appMode]);
	
	const handleClickTool = (e: React.MouseEvent<HTMLElement>, newTool: BoardTool) => {
		e.stopPropagation();
		const nt = newTool !== tool ? newTool : null;
		setTool(nt);
		if (onToolSelect) {
			onToolSelect(nt);
		}
	};
	
	return (
		<Wrapper className="tool-list">
			<Stack spacing={8}>
			{
				Object.values(BoardTool)
				.filter(t => boardToolAppMode[t] === appMode)
				.map(t => (
					<Stack.Item key={t}>
						<Whisper
							speaker={
								<Tooltip>{boardToolDisplayName[t]}</Tooltip>
							}
							placement="bottomStart"
						>
							<Button
								className={tool === t ? "selected" : ""}
								onClick={(e) => handleClickTool(e, t)}
								appearance="default"
							>
								{getToolIcon(t)}
							</Button>
						</Whisper>
					</Stack.Item>
				))
			}
			</Stack>
		</Wrapper>
	);
}

interface WrapperProps {
	positionY?: string;
}

const Wrapper = styled.div<WrapperProps>`
	max-width: min-content;
	
	button.selected {
		background: green;
	}
`;

const getToolIcon = (tool: BoardTool) => {
	switch (tool) {
		case BoardTool.createDatabaseNote:
		case BoardTool.createMachineNote:
			return <span className="fa-regular fa-note-sticky"></span>
		case BoardTool.createTable:
			return <span className="fa-solid fa-table"></span>
		case BoardTool.createRelationship:
			return <span className="fa-solid fa-arrows-left-right-to-line"></span>
		case BoardTool.createProducer:
			return <span className="fa-solid fa-cloud-arrow-down"></span>
		case BoardTool.createBucket:
			return <span className="fa-solid fa-bucket"></span>
		case BoardTool.createChannel:
			return <span className="fa fa-arrow-right"></span>
		case BoardTool.createSignal:
			return <span className="fa fa-signal"></span>
		case BoardTool.createLSystem:
			return <span className="fa-solid fa-arrow-down-up-across-line"></span>
	}
};
