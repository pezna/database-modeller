import { alterUiStateProperty, selectNamespace } from "@/actions";
import { getNamespaces } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import ResponsiveNav from "@rsuite/responsive-nav";

interface NamespaceBoardTabsProps {
	openNamespaceTabs: string[];
	currentNamespaceId: string;
}

export function NamespaceBoardTabs({ openNamespaceTabs, currentNamespaceId }: NamespaceBoardTabsProps) {
	const dispatch = useDispatch();
	const namespaces = useSelector(getNamespaces);
	
	const handleSelectNamespace = (id: string | number | undefined) => {
		if (typeof id !== "string") {
			return;
		}
		
		dispatch(selectNamespace(id));
	}
	
	const handleRemoveNamespace = (eventKey: string | number) => {
		if (typeof eventKey !== "string") {
			return;
		}
		
		const openTabIds = [...openNamespaceTabs];
		const index = openTabIds.indexOf(eventKey);
		openTabIds.splice(index, 1);
		dispatch(alterUiStateProperty(["databaseModeUi", "openNamespaceTabs"], openTabIds));
		
		if (eventKey === currentNamespaceId) {
			const nextId = openTabIds[Math.max(0, index - 1)] ?? null;
			dispatch(selectNamespace(nextId));
		}
	};
	
	return (
		<ResponsiveNav
			appearance="tabs"
			activeKey={currentNamespaceId}
			onSelect={handleSelectNamespace}
			onItemRemove={handleRemoveNamespace}
			removable
		>
			{
				openNamespaceTabs.map(id => (
					<ResponsiveNav.Item key={id} eventKey={id}>
						{namespaces[id].name}
					</ResponsiveNav.Item>
				))
			}
		</ResponsiveNav>
	);
}
