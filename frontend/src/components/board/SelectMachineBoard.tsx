import { NewMachineSection } from "@/components/widgets";
import styled from "@emotion/styled";

export function SelectMachineBoard() {
	return (
		<Wrapper>
			<NewMachineSection />
		</Wrapper>
	);
}

const Wrapper = styled.div`
	display: flex;
	flex-direction: column;
	gap: 1em;
	align-items: center;
	justify-content: center;
	height: 100%;
	
	h6 {
		text-align: center;
		color: var(--rs-bg-card);
	}
	
	.rs-list {
		max-height: 300px;
	}
`;
