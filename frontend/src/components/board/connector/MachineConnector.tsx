import { selectConnector } from "@/actions";
import { useRightClickContext } from "@/contexts";
import { ConnectorData, ConnectorType, DimensionsData, ModelObjectType, SettingsState } from "@/data";
import { getIsConnectorSelected, getMachineBoardSettings } from "@/selectors";
import { diagramService } from "@/services";
import { useDispatch, useSelector } from "@/store";
import styled from "@emotion/styled";
import { useCallback, useEffect, useState } from "react";

interface MachineConnectorProps {
	connector: ConnectorData;
	onSelectCallback?: (connector: ConnectorData) => void;
}

export function MachineConnector({ connector, onSelectCallback }: MachineConnectorProps) {
	const dispatch = useDispatch();
	const { showMenu } = useRightClickContext();
	const [linePathData, setLinePathData] = useState("");
	const isSelected = useSelector(state => getIsConnectorSelected(state, connector.id));
	const connectorSettings = useSelector(state => getMachineBoardSettings(state).connectors);
	const diagramListenerKey = `${connector.type}:${connector.id}`;
	const diagramDrawOrderKey = [connector.sourceComponentId, connector.destComponentId].sort().join(":");
	
	const handleClick = () => {
		dispatch(selectConnector(connector.id));
		onSelectCallback?.(connector);
	};
	
	const diagramCallback = useCallback(() => {
		const sourceDimensions = diagramService.getItemDimensions(connector.sourceComponentId);
		const destDimensions = diagramService.getItemDimensions(connector.destComponentId);
		
		// When a component is deleted, the dimensions will be deleted from the diagram service
		// before this component is no longer rendered. Meaning that this callback will be called
		// with one dimension being undefined, so make sure both are present
		if (sourceDimensions && destDimensions) {
			const allConnectorIdsForComps = diagramService.getConnectorDrawOrder(diagramDrawOrderKey);
			if (!allConnectorIdsForComps) {
				return;
			}
			
			let order = allConnectorIdsForComps.indexOf(connector.id);
			if (order < 0) {
				order = 0;
			}
			
			setLinePathData(generatePath(destDimensions, sourceDimensions, diagramDrawOrderKey.startsWith(connector.sourceComponentId), order));
		}
		
	}, [setLinePathData, connector]);
	
	useEffect(() => {
		diagramService.addItemListener(connector.sourceComponentId, diagramListenerKey, diagramCallback);
		diagramService.addItemListener(connector.destComponentId, diagramListenerKey, diagramCallback);
		
		return () => {
			diagramService.removeItemListener(connector.sourceComponentId, diagramListenerKey);
			diagramService.removeItemListener(connector.destComponentId, diagramListenerKey);
		};
	}, [connector, diagramCallback]);
	
	useEffect(() => {
		diagramService.addConnector(diagramDrawOrderKey, connector.id);
		
		return () => {
			diagramService.removeConnector(diagramDrawOrderKey, connector.id);
		};
	}, [connector.id]);
	
	const handleRightClick = (e: React.MouseEvent) => {
		e.preventDefault();
		e.stopPropagation();
		showMenu({ x: e.pageX, y: e.pageY, boardX: 0, boardY: 0 }, ModelObjectType.connector, connector.id);
	};
	
	return (
		<>
			<ConnectorPath
				settings={connectorSettings}
				isSelected={isSelected}
				d={linePathData}
				onClick={handleClick}
				strokeDasharray={connector.type === ConnectorType.signal ? "10" : undefined}
				markerStart={isSelected ? "url(#marker-arrow-selected)" : "url(#marker-arrow)"}
				markerMid={isSelected ? "url(#marker-arrow-selected)" : "url(#marker-arrow)"}
				markerEnd={isSelected ? "url(#marker-arrow-selected)" : "url(#marker-arrow)"}
			/>
			<ConnectorOverlay
				settings={connectorSettings}
				isSelected={isSelected}
				className="machine-connector"
				d={linePathData}
				onClick={handleClick}
				onContextMenu={handleRightClick}
			/>
		</>
	);
}

interface ConnectorPathProps {
	settings: SettingsState["machineBoard"]["connectors"];
	isSelected: boolean;
}

const ConnectorPath = styled.path<ConnectorPathProps>`
	stroke: ${props => props.isSelected ? "#000" : props.settings.lineColor};
	stroke-width: ${props => props.settings.lineWidth};
	fill: none;
`;

const ConnectorOverlay = styled.path<ConnectorPathProps>`
	stroke: #ffffff01;
	stroke-width: ${props => props.settings.lineWidth + 6};
	fill: none;
	cursor: pointer;
`;

function radius(dim: DimensionsData): number {
	return Math.sqrt(dim.width * dim.width + dim.height * dim.height) / 2;
}

function generatePath(destDimensions: DimensionsData, sourceDimensions: DimensionsData, sourceFirst: boolean, ladderPosition: number): string {
	const sourceHalfX = sourceDimensions.x + sourceDimensions.width / 2;
	const sourceHalfY = sourceDimensions.y + sourceDimensions.height / 2;
	const destHalfX = destDimensions.x + destDimensions.width / 2;
	const destHalfY = destDimensions.y + destDimensions.height / 2;
	
	const len = Math.sqrt(Math.pow(destHalfX - sourceHalfX, 2) + Math.pow(destHalfY - sourceHalfY, 2));
	
	const sourceRadius = radius(sourceDimensions);
	const destRadius = radius(destDimensions);
	const visibleLen = len - destRadius - sourceRadius;
	
	const ladderEven = ladderPosition % 2 === 0;
	const dy = sourceFirst ? (sourceHalfY - destHalfY) : (destHalfY - sourceHalfY);
	const dx = sourceFirst ? (sourceHalfX - destHalfX) : (destHalfX - sourceHalfX);
	const normal = ladderEven ? [-dy, dx] : [dy, -dx];
	
	ladderPosition = ladderPosition / 2;
	if (!ladderEven) {
		ladderPosition = Math.trunc(ladderPosition + 1);
	}
	
	const midPointAdd = [
		normal[0] * (ladderPosition * 0.1) / (len / 300),
		normal[1] * (ladderPosition * 0.1) / (len / 300),
	];
	
	let pathData = `M ${sourceHalfX} ${sourceHalfY} `;
	
	if (visibleLen > 300) {
		const deltaX = (destHalfX - sourceHalfX) / 8;
		const deltaY = (destHalfY - sourceHalfY) / 8;
		let midPointX = sourceHalfX + deltaX;
		let midPointY = sourceHalfY + deltaY;
		pathData += `L ${midPointX + midPointAdd[0]} ${midPointY + midPointAdd[1]} `;
		
		midPointX = destHalfX - deltaX;
		midPointY = destHalfY - deltaY;
		pathData += `L ${midPointX + midPointAdd[0]} ${midPointY + midPointAdd[1]} `;
	}
	else {
		const midPointX = sourceHalfX + (destHalfX - sourceHalfX) / 2;
		const midPointY = sourceHalfY + (destHalfY - sourceHalfY) / 2;
		pathData += `L ${midPointX + midPointAdd[0]} ${midPointY + midPointAdd[1]} `;
	}
	
	pathData += `L ${destHalfX} ${destHalfY}`;
	
	return pathData;
}
