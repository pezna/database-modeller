import { selectColumn } from "@/actions";
import { useRightClickContext } from "@/contexts";
import { ColumnData, ColumnOptionType, createDimensionsObject, ModelObjectType, TextFormattingOption } from "@/data";
import { getColumnSettings, getColumnType, getIsColumnSelected, getMixinSettings, getShowKeyColumns } from "@/selectors";
import { diagramService } from "@/services";
import { useDispatch, useSelector } from "@/store";
import styled from "@emotion/styled";
import React, { useLayoutEffect, useRef } from "react";

interface ColumnProps {
	column: ColumnData;
	namespaceId: string;
	isPrimaryKey: boolean;
	isForeignKey: boolean;
	onSelect?: (column: ColumnData) => void;
};

export function Column({ column, namespaceId, isPrimaryKey, isForeignKey, onSelect }: ColumnProps) {
	const dispatch = useDispatch();
	const { showMenu } = useRightClickContext();
	const type = useSelector(state => getColumnType(state, column.typeId));
	const isSelected = useSelector(state => getIsColumnSelected(state, column.id));
	const shouldShowKeys = useSelector(getShowKeyColumns);
	const mixinSettings = useSelector(getMixinSettings);
	const settings = useSelector(getColumnSettings);
	const isUnique = Boolean(column.options[ColumnOptionType.unique]);
	const isIndex = Boolean(column.options[ColumnOptionType.index]);
	const isNotNull = Boolean(column.options[ColumnOptionType.notNull]);
	const useMixinBackgroundColor = Boolean(column.belongsToMixinSchemaItem && mixinSettings.applyColorToColumns);
	const columnRef = useRef<HTMLTableRowElement>(null);
	
	useLayoutEffect(() => {
		const col = columnRef.current;
		if (!col) {
			return;
		}
		
		const newDimensions = createDimensionsObject(
			col.offsetLeft + (col.offsetParent as HTMLElement).offsetLeft,
			col.offsetTop + (col.offsetParent as HTMLElement).offsetTop,
			col.offsetWidth,
			col.offsetHeight,
		);
		
		diagramService.sendItemDimensions(column.id, namespaceId, newDimensions);
		
	}, [column, namespaceId, dispatch]);
	
	const mouseDown = (e: React.MouseEvent) => {
		e.stopPropagation();
		
		dispatch(selectColumn(column.id));
		onSelect?.(column);
	};
	
	const renderName = () => {
		let extraClass = "";
		
		if (isPrimaryKey && settings.stylePrimaryKeys !== TextFormattingOption.none) {
			extraClass += `${settings.stylePrimaryKeys} `;
		}
		if (isForeignKey && settings.styleForeignKeys !== TextFormattingOption.none) {
			extraClass += `${settings.styleForeignKeys} `;
		}
		
		return <td className={`column-name ${extraClass}`}>{column.name}</td>;
	};
	
	const renderType = () => {
		if (!settings.showType) {
			return null;
		}
		
		return <td className="column-type">{type?.name}</td>;
	};
	
	const renderColumnIndicator = () => {
		if (!(settings.indicatePrimaryKey
			|| settings.indicateForeignKey
			|| settings.indicateUnique
			|| settings.indicateIndex
			|| settings.indicateNotNull))
		{
			return null;
		}
		
		const indicators = [];
		const tooltips = [];
		
		if (settings.indicatePrimaryKey && isPrimaryKey) {
			indicators.push("PK");
			tooltips.push("Primary Key");
		}
		
		if (settings.indicateForeignKey && isForeignKey) {
			indicators.push("FK");
			tooltips.push("Foreign Key");
		}
		
		if (settings.indicateUnique && isUnique) {
			indicators.push("UQ");
			tooltips.push("Unique");
		}
		
		if (settings.indicateIndex && isIndex) {
			indicators.push("IX");
			tooltips.push("Index");
		}
		
		if (settings.indicateNotNull && isNotNull) {
			indicators.push("NN");
			tooltips.push("Not Null");
		}
		
		return (
			<td>
				<span title={tooltips.join(", ")}>
					{indicators.join(" ")}
				</span>
			</td>
		);
	};
	
	const handleRightClick = (e: React.MouseEvent) => {
		e.preventDefault();
		e.stopPropagation();
		
		showMenu({ x: e.pageX, y: e.pageY, boardX: 0, boardY: 0 }, ModelObjectType.column, column.id);
	};
	
	if ((isPrimaryKey || isForeignKey) && !shouldShowKeys) {
		return null;
	}
	const mixinBackgroundColor = useMixinBackgroundColor ? mixinSettings.mixinBackgroundColor : undefined;
	
	return (
		<Row ref={columnRef}
			className={isSelected ? "selected" : ""}
			mixinBackgroundColor={mixinBackgroundColor}
			onMouseDown={mouseDown}
			onContextMenu={handleRightClick}
		>
			{renderName()}
			{renderType()}
			{renderColumnIndicator()}
		</Row>
	);
}

const Row = styled.tr<{mixinBackgroundColor?: string}>`
	background: ${props => props.mixinBackgroundColor ?? "none" };
	
	td {
		font-size: 0.9em;
		padding: 4px 8px;
		white-space: nowrap;
		
		&.bold {
			font-weight: bold;
		}
		
		&.italic {
			font-style: italic;
		}
		
		&.underline {
			text-decoration: underline;
		}
	}
	
	&:hover {
		background: #00000022;
	}

	&.selected {
		background: #00000044;
	}
`;