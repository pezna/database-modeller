import { selectTable } from "@/actions";
import { useRightClickContext } from "@/contexts";
import { ColorData, ColumnData, ModelObjectType, TableData, createDimensionsObject } from "@/data";
import { getBoardSettings, getIsTableSelected, getNamespace, getPrimaryKey, getTableColumns, getTableRelationships } from "@/selectors";
import { diagramService } from "@/services";
import { useDispatch, useSelector } from "@/store";
import styled from "@emotion/styled";
import React, { useLayoutEffect, useRef } from "react";
import Draggable, { DraggableData, DraggableEvent } from "react-draggable";
import { boardDraggableBounds } from "../shared-board-utils";
import { Column } from "./Column";

interface TableProps {
	table: TableData;
	onColumnSelect?: (column: ColumnData) => void;
}

export function Table({ table, onColumnSelect }: TableProps) {
	const dispatch = useDispatch();
	const { showMenu } = useRightClickContext();
	const isSelected = useSelector(state => getIsTableSelected(state, table.id));
	const boardSettings = useSelector(getBoardSettings);
	const columns = useSelector(state => getTableColumns(state, table.id));
	const relationshipColumnIds = useSelector(
		state => getTableRelationships(state, table.id)
			.filter(rel => rel.childTableId === table.id)
			.flatMap(r => r.childColumnIds)
	);
	const primaryKeyColumnIds = useSelector(
		state => table.primaryKeyId ? getPrimaryKey(state, table.primaryKeyId).columnIds : []
	);
	const namespace = useSelector(state => table.namespaceId ? getNamespace(state, table.namespaceId) : null);
	const tableRef = useRef<HTMLDivElement>({} as any);
	const dimensions = diagramService.getItemDimensions(table.id);
	const tempDimensions = useRef({
		x: dimensions?.x ?? 0,
		y: dimensions?.y ?? 0,
	});
	
	const sendDimensions = () => {
		const newDimensions = createDimensionsObject(
			tempDimensions.current.x,
			tempDimensions.current.y,
			tableRef.current!.offsetWidth,
			tableRef.current!.offsetHeight,
		);
		
		diagramService.sendItemDimensions(table.id, table.namespaceId, newDimensions);
	};
	
	useLayoutEffect(() => {
		sendDimensions();
		// some of these dependencies are needed in case the settings change or column name changes (resulting in a different column width)
	}, [table, columns, dimensions, boardSettings, dispatch, sendDimensions]);
	
	const selectItem = () => dispatch(selectTable(table.id));
	
	const handleDragCallback = (e: DraggableEvent, data: DraggableData) => {
		tempDimensions.current.x = data.x;
		tempDimensions.current.y = data.y;
		sendDimensions();
	};
	
	const handleRightClick = (e: React.MouseEvent) => {
		e.preventDefault();
		e.stopPropagation();
		showMenu({ x: e.pageX, y: e.pageY, boardX: 0, boardY: 0 }, ModelObjectType.table, table.id);
	};
	
	const renderColumns = () => {
		return (
			<table>
				<tbody>
					{
						columns.map((column, i) => (
							<Column
								key={`${i}-${column.id}`}
								column={column}
								namespaceId={table.namespaceId}
								isPrimaryKey={primaryKeyColumnIds.includes(column.id)}
								isForeignKey={relationshipColumnIds.includes(column.id)}
								onSelect={onColumnSelect}
							/>
						))
					}
				</tbody>
			</table>
		);
	}
	
	const overrideColors = namespace
		? createTheme(namespace, boardSettings.table)
		: createTheme(boardSettings.table, boardSettings.table);
	if (table.isMixin) {
		overrideColors.backgroundColor = boardSettings.mixins.mixinBackgroundColor;
	}
	
	return (
		<Draggable
			bounds={boardDraggableBounds}
			defaultPosition={dimensions}
			onMouseDown={selectItem}
			onStart={handleDragCallback}
			onDrag={handleDragCallback}
			onStop={handleDragCallback}
			nodeRef={tableRef}
		>
			<StyledTable
				ref={tableRef}
				className={`table ${isSelected ? "selected" : ""}`}
				onContextMenu={handleRightClick}
				shouldBringToFront={isSelected}
				overrideColors={overrideColors}
			>
				<TableHeading>
					{table.name}
				</TableHeading>
				{renderColumns()}
			</StyledTable>
		</Draggable>
	);
}

const TableHeading = styled.div`
	padding: 5px;
	font-weight: bold;
	text-align: center;
	border-bottom: 1px solid #111;
`;

type StyledTableProps = {
	shouldBringToFront: boolean;
	overrideColors: ColorData;
}

const StyledTable = styled.div<StyledTableProps>`
	position: absolute;
	border-radius: 5px;
	border: 1px solid ${props => props.overrideColors.borderColor ?? props.theme.table.borderColor};
	background: ${props => props.overrideColors.backgroundColor ?? props.theme.table.backgroundColor};
	color: ${props => props.overrideColors.foregroundColor ?? props.theme.table.foregroundColor};
	min-width: 100px;
	min-height: 50px;
	z-index: ${props => props.shouldBringToFront ? 1000 : "initial"};

	${TableHeading} {
		border-bottom: 1px solid ${props => props.theme.table.borderColor};
	}
	
	table {
		width: 100%;
		border-collapse: collapse;
		margin-bottom: 5px;
	}
	
	&.selected {
		box-shadow: 0px 10px 28px 0px rgba(0, 0, 0, 0.75);
	}
	
	&:hover {
		cursor: pointer;
	}
`;

function createTheme(color: ColorData, alternative: ColorData): ColorData {
	return {
		borderColor: color.borderColor || alternative.borderColor,
		backgroundColor: color.backgroundColor || alternative.backgroundColor,
		foregroundColor: color.foregroundColor || alternative.foregroundColor,
	};
}

// export const Table = React.memo(_Table);
