export type TipDirection = 'left' | 'right' | 'up' | 'down';

export const LEFT = 'left';
export const RIGHT = 'right';
export const UP = 'up';
export const DOWN = 'down';
export const tipHeight = 15;
export const manyProngSpace = 8;

export function findIntersection(p1X: number, p1Y: number, p2X: number, p2Y: number, p3X: number, p3Y: number, p4X: number, p4Y: number) {
	// differences
	const deltaX1 = p2X - p1X;
	const deltaY1 = p2Y - p1Y;
	const deltaX2 = p4X - p3X;
	const deltaY2 = p4Y - p3Y;
	
	const xCol = (((deltaX1 * deltaX2) * (p3Y - p1Y)) - 
		(p3X * deltaY2 * deltaX1) + 
		(p1X * deltaY1 * deltaX2)) / 
		((deltaY1 * deltaX2) - (deltaY2 * deltaX1));
	
	let yCol = 0;
	if (deltaX1 < 0.001) {
		// L1 is a vertical line
		yCol = ((xCol * deltaY2) +
				(p3Y * deltaX2) -
				(p3X * deltaY2)) / deltaX2;
	}
	else {
		yCol = ((xCol * deltaY1) +
				(p1Y * deltaX1) -
				(p1X * deltaY1)) / deltaX1;
	}
	
	const line1MinX = p1X < p2X ? p1X : p2X;
	const line1MaxX = p1X >= p2X ? p1X : p2X;
	const line1MinY = p1Y < p2Y ? p1Y : p2Y;
	const line1MaxY = p1Y >= p2Y ? p1Y : p2Y;
	
	if ((xCol < line1MinX || xCol > line1MaxX) || (yCol < line1MinY || yCol > line1MaxY))
	{
		// intersection point must be on the first line
		return [];
	}
	
	return [xCol, yCol];
}
