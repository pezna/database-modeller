import { useRightClickContext } from "@/contexts";
import { DimensionsData, ModelObjectType, RelationshipConnectLocation, RelationshipData } from "@/data";
import { useModelNavigation } from "@/hooks";
import { getBoardSettings, getIsRelationshipSelected, getNamespace, getShowKeyColumns, getTable } from "@/selectors";
import { diagramService } from "@/services";
import { useSelector } from "@/store";
import styled from "@emotion/styled";
import { useCallback, useEffect, useState } from "react";
import { DOWN, LEFT, RIGHT, TipDirection, UP, findIntersection, manyProngSpace, tipHeight } from "./shared";

interface RelationshipProps {
	relationship: RelationshipData;
};

export function Relationship({ relationship }: RelationshipProps) {
	const { selectTable, selectRelationship } = useModelNavigation();
	const { showMenu } = useRightClickContext();
	const [pathData, setPathData] = useState<GeneratePathData>();
	const childTableNamespaceId = useSelector(state => getTable(state, relationship.childTableId)?.namespaceId);
	const parentTable = useSelector(state => getTable(state, relationship.parentTableId));
	const parentNamespace = useSelector(state => getNamespace(state, parentTable.namespaceId));
	const isSelected = useSelector(state => getIsRelationshipSelected(state, relationship.id));
	const showKeys = useSelector(getShowKeyColumns);
	const boardSettings = useSelector(getBoardSettings);
	const {lineConnectLocation, showOtherNamespaceTableName} = boardSettings.relationships;
	const isSameTable = relationship.childTableId === relationship.parentTableId;
	const shouldConnectToColumn = lineConnectLocation === RelationshipConnectLocation.column && showKeys;
	const shouldConnectToTable = lineConnectLocation === RelationshipConnectLocation.table || !shouldConnectToColumn;
	const isSameNamespace = childTableNamespaceId === parentNamespace.id;
	
	const diagramCallback = useCallback(() => {
		const parentTableDimensions = diagramService.getItemDimensions(relationship.parentTableId);
		const childTableDimensions = diagramService.getItemDimensions(relationship.childTableId);
		const parentColumnsDimensions = relationship.parentColumnIds.map(id => diagramService.getItemDimensions(id));
		const childColumnsDimensions = relationship.childColumnIds.map(id => diagramService.getItemDimensions(id));
		
		if (
			parentTableDimensions &&
			childTableDimensions &&
			allDimensionsExist(parentColumnsDimensions) &&
			allDimensionsExist(childColumnsDimensions)
		) {
			setPathData(generatePath(
				parentTableDimensions, parentColumnsDimensions,
				childTableDimensions, childColumnsDimensions,
				shouldConnectToColumn, shouldConnectToTable,
				isSameTable, isSameNamespace,
			));
		}
	}, [setPathData, relationship, lineConnectLocation, showKeys]);
	
	useEffect(() => {
		diagramService.addItemListener(relationship.parentTableId, relationship.id, diagramCallback);
		diagramService.addItemListener(relationship.childTableId, relationship.id, diagramCallback);
		
		return () => {
			diagramService.removeItemListener(relationship.parentTableId, relationship.id);
			diagramService.removeItemListener(relationship.childTableId, relationship.id);
		};
	}, [relationship, diagramCallback]);
	
	const handleClickLine = () => {
		if (!isSelected) {
			selectRelationship(relationship.id);
		}
	};
	
	const handleClickText = () => {
		selectTable(parentTable.id);
	};
	
	const handleRightClick = (e: React.MouseEvent) => {
		e.preventDefault();
		e.stopPropagation();
		showMenu(
			{ x: e.pageX, y: e.pageY, boardX: 0, boardY: 0 },
			ModelObjectType.relationship,
			relationship.id,
		);
	};
	
	const lineColor = relationship.isNotExported ? boardSettings.relationships.notExportedLineColor : boardSettings.relationships.lineColor;
	const lineWidth = boardSettings.relationships.lineWidth;
	
	const renderParentTableName = () => {
		if (isSameNamespace) {
			return null;
		}
		
		const cx = (pathData?.midpointForNames[0] ?? 0);
		const cy = (pathData?.midpointForNames[1] ?? 0);
		
		return (
			<>
				<ConnectorCircle
					cx={cx + 4}
					cy={cy}
					r={4}
					lineWidth={lineWidth}
					lineColor={lineColor}
					isSelected={isSelected}
					onClick={handleClickLine}
					onContextMenu={handleRightClick}
				/>
				{
					showOtherNamespaceTableName && (
						<TableNameText
							x={cx + 10}
							y={cy + 4}
							onClick={handleClickText}
						>
							{parentNamespace.name}.{parentTable.name}
						</TableNameText>
					)
				}
			</>
		);
	};
	
	return (
		<>
			<ConnectorPath
				d={pathData?.path ?? ""}
				fill="transparent"
				isSelected={isSelected}
				lineWidth={lineWidth}
				lineColor={lineColor}
			/>
			<ConnectorOverlay
				isSelected={isSelected}
				lineWidth={lineWidth}
				lineColor={lineColor}
				d={pathData?.path ?? ""}
				onClick={handleClickLine}
				onContextMenu={handleRightClick}
			/>
			{renderParentTableName()}
		</>
	);
}

const TableNameText = styled.text`
	cursor: pointer;
`;

interface ConnectorPathProps {
	isSelected: boolean;
	lineColor: string;
	lineWidth: number;
}

const ConnectorPath = styled.path<ConnectorPathProps>`
	stroke: ${props => props.isSelected ? "#222" : props.lineColor};
	stroke-width: ${props => props.lineWidth};
	fill: none;
`;

const ConnectorOverlay = styled.path<ConnectorPathProps>`
	stroke: #ffffff01;
	stroke-width: ${props => props.lineWidth + 6};
	fill: none;
	cursor: pointer;
`;

const ConnectorCircle = styled.circle<ConnectorPathProps>`
	stroke: ${props => props.isSelected ? "#222" : props.lineColor};
	stroke-width: ${props => props.lineWidth};
	fill: none;
	cursor: pointer;
`;

type GeneratePathData = {
	path: string;
	midpointForNames: [number, number];
}

function generatePath(
	parentTableDimensions: DimensionsData, parentColumnsDimensions: DimensionsData[],
	childTableDimensions: DimensionsData, childColumnsDimensions: DimensionsData[],
	shouldConnectToColumn: boolean, shouldConnectToTable: boolean, isSameTable: boolean,
	isSameNamespace: boolean,
): GeneratePathData
{
	if (!childColumnsDimensions || !childColumnsDimensions[0] || !childColumnsDimensions[0].hasOwnProperty('x') ||
		!parentColumnsDimensions || !parentColumnsDimensions[0] || !parentColumnsDimensions[0].hasOwnProperty('x'))
	{
		return {
			path: "",
			midpointForNames: [0, 0],
		};
	}
	
	const parentLeftX = parentTableDimensions.x;
	const parentHalfX = parentTableDimensions.x + parentTableDimensions.width / 2;
	const parentRightX = parentTableDimensions.x + parentTableDimensions.width;
	const parentTopY = parentTableDimensions.y;
	const parentHalfY = parentTableDimensions.y + parentTableDimensions.height / 2;
	const parentBottomY = parentTableDimensions.y + parentTableDimensions.height;
	const childLeftX = childTableDimensions.x;
	const childHalfX = childTableDimensions.x + childTableDimensions.width / 2;
	const childRightX = childTableDimensions.x + childTableDimensions.width;
	const childTopY = childTableDimensions.y;
	const childHalfY = childTableDimensions.y + childTableDimensions.height / 2;
	const childBottomY = childTableDimensions.y + childTableDimensions.height;
	
	let lineStartPoint = null;
	let lineEndPoint = null;
	const pathData = [];
	const midpointForNames: [number, number] = [0, 0];
	
	if (shouldConnectToColumn || !isSameNamespace) {
		const parentConnectorOnLeftSide = isSameNamespace && (isSameTable || (parentLeftX < childLeftX && childLeftX < parentRightX) || childRightX < parentLeftX);
		const childConnectorOnLeftSide = isSameNamespace && (isSameTable || (parentConnectorOnLeftSide && childRightX > parentHalfX) || childLeftX > parentRightX);
		const childTips = [];
		const parentTips = [];
		
		for (let columnDimensions of childColumnsDimensions) {
			const columnLeftX = childLeftX;
			const columnTopY = columnDimensions.y + childTopY;
			const columnRightX = Math.max(childRightX, childLeftX + columnDimensions.width);
			
			const tip = childConnectorOnLeftSide
				? [columnLeftX - tipHeight, columnTopY + columnDimensions.height / 2 + 1]
				: [columnRightX + tipHeight, columnTopY + columnDimensions.height / 2 + 1];
			
			childTips.push(tip);
			pathData.push(drawManyConnector(
				tip,
				tipHeight,
				Math.min(columnDimensions.height / 2 - 2, manyProngSpace),
				childConnectorOnLeftSide ? LEFT : RIGHT
			));
		}
		
		if (childTips.length > 1) {
			let x = childTips[0][0];
			let y = childTips[0][1];
			for (let i = 0; i < childTips.length - 1; i++) {
				const curr = childTips[i];
				const next = childTips[i + 1];
				pathData.push(`M ${curr[0]} ${curr[1]} L ${next[0]} ${next[1]}`);
				
				x += next[0];
				y += next[1];
			}
			
			lineStartPoint = [x / childTips.length, y / childTips.length];
		}
		else {
			lineStartPoint = childTips[0];
		}
		
		if (isSameNamespace) {
			for (let columnDimensions of parentColumnsDimensions) {
				const columnLeftX = parentLeftX;
				const columnTopY = columnDimensions.y + parentTopY;
				const columnRightX = Math.max(parentRightX, parentLeftX + columnDimensions.width);
				
				const tip = parentConnectorOnLeftSide
					? [columnLeftX - tipHeight, columnTopY + columnDimensions.height / 2]
					: [columnRightX + tipHeight, columnTopY + columnDimensions.height / 2];
				
				parentTips.push(tip);
				pathData.push(drawOneConnector(
					tip,
					tipHeight,
					parentConnectorOnLeftSide ? LEFT : RIGHT
				));
			}
			
			if (parentTips.length > 1) {
				let x = parentTips[0][0];
				let y = parentTips[0][1];
				for (let i = 0; i < parentTips.length - 1; i++) {
					const curr = parentTips[i];
					const next = parentTips[i + 1];
					pathData.push(`M ${curr[0]} ${curr[1]} L ${next[0]} ${next[1]}`);
					
					x += next[0];
					y += next[1];
				}
				
				lineEndPoint = [x / parentTips.length, y / parentTips.length];
			}
			else {
				lineEndPoint = parentTips[0];
			}
			
			pathData.push(`M ${lineStartPoint[0]} ${lineStartPoint[1]} L ${lineEndPoint[0]} ${lineEndPoint[1]}`);
		}
		else {
			midpointForNames[0] = lineStartPoint[0];
			midpointForNames[1] = lineStartPoint[1];
		}
	}
	else if (shouldConnectToTable) {
		// prevent a division by 0 error by doing it this way
		let angle = childTopY > parentBottomY ? 270 : 90;
		if (childHalfX !== parentHalfX) {
			const slope = (parentHalfY - childHalfY) / (parentHalfX - childHalfX);
			const rad = Math.atan(slope);
			angle = (rad * 180 / Math.PI);
			
			if (childHalfX <= parentHalfX && angle < 0) {
				angle += 360;
			}
			else if (childHalfX > parentHalfX) {
				angle += 180;
			}
		}
		
		let childLineToCheck: number[] = [];
		let parentLineToCheck: number[] = [];
		let childSecondaryLineToCheck: number[] = [];
		let parentSecondaryLineToCheck: number[] = [];
		let childIntersectPoint = null;
		let parentIntersectPoint = null;
		let childTipDirection: TipDirection = LEFT;
		let childSecondaryTipDirection: TipDirection = LEFT;
		let parentTipDirection: TipDirection = LEFT;
		let parentSecondaryTipDirection: TipDirection = LEFT;
		
		if (angle <= 45 || angle > 315) {
			childLineToCheck = [childRightX, childTopY, childRightX, childBottomY]; // RIGHT
			parentLineToCheck = [parentLeftX, parentTopY, parentLeftX, parentBottomY]; // LEFT
			
			const secondaryCheck = angle > 315;
			childSecondaryLineToCheck = secondaryCheck
				? [childLeftX, childTopY, childRightX, childTopY]
				: [childLeftX, childBottomY, childRightX, childBottomY]; // UP : DOWN
			parentSecondaryLineToCheck = secondaryCheck
				? [parentLeftX, parentBottomY, parentRightX, parentBottomY]
				: [parentLeftX, parentTopY, parentRightX, parentTopY]; // DOWN : UP
			
			childTipDirection = RIGHT;
			childSecondaryTipDirection = secondaryCheck ? UP : DOWN;
			parentTipDirection = LEFT;
			parentSecondaryTipDirection = secondaryCheck ? DOWN : UP;
		}
		else if (angle > 45 && angle <= 135) {
			childLineToCheck = [childLeftX, childBottomY, childRightX, childBottomY]; // DOWN
			parentLineToCheck = [parentLeftX, parentTopY, parentRightX, parentTopY]; // UP
			
			const secondaryCheck = angle <= 90;
			childSecondaryLineToCheck = secondaryCheck
				? [childLeftX, childTopY, childLeftX, childBottomY]
				: [childRightX, childTopY, childRightX, childBottomY]; // LEFT : RIGHT
			parentSecondaryLineToCheck = secondaryCheck
				? [parentRightX, parentTopY, parentRightX, parentBottomY]
				: [parentLeftX, parentTopY, parentLeftX, parentBottomY]; // RIGHT : LEFT
			
			childTipDirection = DOWN;
			childSecondaryTipDirection = secondaryCheck ? LEFT : RIGHT;
			parentTipDirection = UP;
			parentSecondaryTipDirection = secondaryCheck ? RIGHT : LEFT;
		}
		else if (angle > 135 && angle <= 225) {
			childLineToCheck = [childLeftX, childTopY, childLeftX, childBottomY]; // LEFT
			parentLineToCheck = [parentRightX, parentTopY, parentRightX, parentBottomY]; // RIGHT
			
			const secondaryCheck = angle <= 180;
			childSecondaryLineToCheck = secondaryCheck
				? [childLeftX, childBottomY, childRightX, childBottomY]
				: [childLeftX, childTopY, childRightX, childTopY]; // DOWN : UP
			parentSecondaryLineToCheck = secondaryCheck
				? [parentLeftX, parentTopY, parentRightX, parentTopY]
				: [parentLeftX, parentBottomY, parentRightX, parentBottomY]; // UP : DOWN
			
			childTipDirection = LEFT;
			childSecondaryTipDirection = secondaryCheck ? DOWN : UP;
			parentTipDirection = RIGHT;
			parentSecondaryTipDirection = secondaryCheck ? UP : DOWN;
		}
		else if (angle > 225 && angle <= 315) {
			childLineToCheck = [childLeftX, childTopY, childRightX, childTopY]; // UP
			parentLineToCheck = [parentLeftX, parentBottomY, parentRightX, parentBottomY]; // DOWN
			
			const secondaryCheck = angle <= 270;
			childSecondaryLineToCheck = secondaryCheck
				? [childRightX, childTopY, childRightX, childBottomY]
				: [childLeftX, childTopY, childLeftX, childBottomY]; // RIGHT : LEFT
			parentSecondaryLineToCheck = secondaryCheck
				? [parentLeftX, parentTopY, parentLeftX, parentBottomY]
				: [parentRightX, parentTopY, parentRightX, parentBottomY]; // LEFT : RIGHT
			
			childTipDirection = UP;
			childSecondaryTipDirection = secondaryCheck ? RIGHT : LEFT;
			parentTipDirection = DOWN;
			parentSecondaryTipDirection = secondaryCheck ? LEFT : RIGHT;
		}
		
		let selectedChildTipDirection = childTipDirection;
		let selectedParentTipDirection = parentTipDirection;
		
		childIntersectPoint = findIntersection(
			childLineToCheck[0], childLineToCheck[1], childLineToCheck[2], childLineToCheck[3],
			childHalfX, childHalfY, parentHalfX, parentHalfY
		);
		if (!childIntersectPoint || isNaN(childIntersectPoint[0]) || isNaN(childIntersectPoint[1])) {
			childIntersectPoint = findIntersection(
				childSecondaryLineToCheck[0], childSecondaryLineToCheck[1], childSecondaryLineToCheck[2], childSecondaryLineToCheck[3],
				childHalfX, childHalfY, parentHalfX, parentHalfY
			);
			selectedChildTipDirection = childSecondaryTipDirection;
		}
		
		parentIntersectPoint = findIntersection(
			parentLineToCheck[0], parentLineToCheck[1], parentLineToCheck[2], parentLineToCheck[3],
			childHalfX, childHalfY, parentHalfX, parentHalfY
		);
		if (!parentIntersectPoint || isNaN(parentIntersectPoint[0]) || isNaN(parentIntersectPoint[1])) {
			parentIntersectPoint = findIntersection(
				parentSecondaryLineToCheck[0], parentSecondaryLineToCheck[1], parentSecondaryLineToCheck[2], parentSecondaryLineToCheck[3],
				childHalfX, childHalfY, parentHalfX, parentHalfY
			);
			selectedParentTipDirection = parentSecondaryTipDirection;
		}
		
		const prongInset = 5;
		switch (selectedChildTipDirection) {
			case LEFT:
				childIntersectPoint[0] -= tipHeight;	
				childIntersectPoint[1] = Math.min(
					childBottomY - manyProngSpace - prongInset,
					Math.max(childTopY + manyProngSpace + prongInset, childIntersectPoint[1])
				);
				break;
			case RIGHT:
				childIntersectPoint[0] += tipHeight;
				childIntersectPoint[1] = Math.min(
					childBottomY - manyProngSpace - prongInset,
					Math.max(childTopY + manyProngSpace + prongInset, childIntersectPoint[1])
				);
				break;
			case UP:
				childIntersectPoint[0] = Math.min(
					childRightX - manyProngSpace - prongInset,
					Math.max(childLeftX + manyProngSpace + prongInset, childIntersectPoint[0])
				);
				childIntersectPoint[1] -= tipHeight;
				break;
			case DOWN:
				childIntersectPoint[0] = Math.min(
					childRightX - manyProngSpace - prongInset,
					Math.max(childLeftX + manyProngSpace + prongInset, childIntersectPoint[0])
				);
				childIntersectPoint[1] += tipHeight;
				break;
			default:
				break;
		}
		
		switch (selectedParentTipDirection) {
			case LEFT:
				parentIntersectPoint[0] -= tipHeight;
				parentIntersectPoint[1] = Math.min(
					parentBottomY - manyProngSpace - prongInset,
					Math.max(parentTopY + manyProngSpace + prongInset, parentIntersectPoint[1])
				);
				break;
			case RIGHT:
				parentIntersectPoint[0] += tipHeight;
				parentIntersectPoint[1] = Math.min(
					parentBottomY - manyProngSpace - prongInset,
					Math.max(parentTopY + manyProngSpace + prongInset, parentIntersectPoint[1])
				);
				break;
			case UP:
				parentIntersectPoint[0] = Math.min(
					parentRightX - manyProngSpace - prongInset,
					Math.max(parentLeftX + manyProngSpace + prongInset, parentIntersectPoint[0])
				);
				parentIntersectPoint[1] -= tipHeight;
				break;
			case DOWN:
				parentIntersectPoint[0] = Math.min(
					parentRightX - manyProngSpace - prongInset,
					Math.max(parentLeftX + manyProngSpace + prongInset, parentIntersectPoint[0])
				);
				parentIntersectPoint[1] += tipHeight;
				break;
			default:
				break;
		}
		
		pathData.push(drawManyConnector(
			childIntersectPoint,
			tipHeight,
			manyProngSpace,
			selectedChildTipDirection
		));
		pathData.push(drawOneConnector(
			parentIntersectPoint,
			tipHeight,
			selectedParentTipDirection
		));
		pathData.push(`M ${childIntersectPoint[0]} ${childIntersectPoint[1]} L ${parentIntersectPoint[0]} ${parentIntersectPoint[1]}`);
	}
	
	return {
		path: pathData.join(' '),
		midpointForNames,
	};
}

function drawManyConnector(tip: number[], tipHeight: number, spaceBetweenPoints: number, tipDirection: TipDirection) {
	const depth = 5;
	let points: number[][] = [];
	
	if (tipDirection === UP) {
		points = [
			[tip[0], tip[1]],
			[tip[0] - spaceBetweenPoints, tip[1] + tipHeight],
			[tip[0] - spaceBetweenPoints, tip[1] + tipHeight + depth],
			[tip[0] + spaceBetweenPoints, tip[1] + tipHeight + depth],
			[tip[0] + spaceBetweenPoints, tip[1] + tipHeight],
			[tip[0], tip[1]],
			[tip[0], tip[1] + tipHeight + depth]
		];
	}
	else if (tipDirection === DOWN) {
		points = [
			[tip[0], tip[1]],
			[tip[0] - spaceBetweenPoints, tip[1] - tipHeight],
			[tip[0] - spaceBetweenPoints, tip[1] - tipHeight - depth],
			[tip[0] + spaceBetweenPoints, tip[1] - tipHeight - depth],
			[tip[0] + spaceBetweenPoints, tip[1] - tipHeight],
			[tip[0], tip[1]],
			[tip[0], tip[1] - tipHeight - depth]
		];
	}
	else if (tipDirection === LEFT) {
		points = [
			[tip[0], tip[1]],
			[tip[0] + tipHeight, tip[1] - spaceBetweenPoints],
			[tip[0] + tipHeight + depth, tip[1] - spaceBetweenPoints],
			[tip[0] + tipHeight + depth, tip[1] + spaceBetweenPoints],
			[tip[0] + tipHeight, tip[1] + spaceBetweenPoints],
			[tip[0], tip[1]],
			[tip[0] + tipHeight + depth, tip[1]]
		];
	}
	else if (tipDirection === RIGHT) {
		points = [
			[tip[0], tip[1]],
			[tip[0] - tipHeight, tip[1] - spaceBetweenPoints],
			[tip[0] - tipHeight - depth, tip[1] - spaceBetweenPoints],
			[tip[0] - tipHeight - depth, tip[1] + spaceBetweenPoints],
			[tip[0] - tipHeight, tip[1] + spaceBetweenPoints],
			[tip[0], tip[1]],
			[tip[0] - tipHeight - depth, tip[1]]
		];
	}
	
	const pathString = [`M ${points[0][0]} ${points[0][1]} `];
	points.forEach((p, index) => {
		if (index === 0) {
			return;
		}
		pathString.push(` ${p[0]} ${p[1]} `);
	});
	
	return pathString.join('L');
}

function drawOneConnector(tip: number[], tipHeight: number, tipDirection: TipDirection) {
	let point: number[] = [];
	
	if (tipDirection === UP) {
		point = [tip[0], tip[1] + tipHeight];
	}
	else if (tipDirection === DOWN) {
		point = [tip[0], tip[1] - tipHeight];
	}
	else if (tipDirection === LEFT) {
		point = [tip[0] + tipHeight, tip[1]];
	}
	else if (tipDirection === RIGHT) {
		point = [tip[0] - tipHeight, tip[1]];
	}
	
	return `M ${tip[0]} ${tip[1]} L ${point[0]} ${point[1]}`;
}

function allDimensionsExist(arr: Array<DimensionsData | undefined>): arr is DimensionsData[] {
	return arr.every(x => x)
}
