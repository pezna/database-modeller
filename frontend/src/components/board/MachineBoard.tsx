import { BoardCanvas } from "@/components/board/BoardCanvas";
import { MachineComponent } from "@/components/board/component/MachineComponent";
import { MachineConnector } from "@/components/board/connector/MachineConnector";
import { SelectedBoardItem, useBoardItemCreation, useRightClickContext } from "@/contexts";
import { AppMode, BoardTool, ComponentData, isToolConnector } from "@/data";
import { useModelCreation } from "@/hooks";
import { getComponentsForMachine, getConnectorsForMachine, getMachine, getMachineBoardSettings, getMachineModeUi, getMachineNotes, getSelectedMachineId } from "@/selectors";
import { useSelector } from "@/store";
import styled from "@emotion/styled";
import { useCallback, useLayoutEffect, useRef } from "react";
import { BoardHeader } from "./board-header/BoardHeader";
import { Note } from "./note/Note";

const appMode = AppMode.machine;

export function MachineBoard() {
	const { showMenu } = useRightClickContext();
	const { tool, setTool, firstSelectedItem, setFirstSelectedItem, setMouseLocation } = useBoardItemCreation();
	const { createBucket, createProducer, createNote, createChannel, createSignal, createLSystem } = useModelCreation();
	const connectorSettings = useSelector(state => getMachineBoardSettings(state).connectors);
	const openMachineTabs = useSelector(state => getMachineModeUi(state).openMachineTabs);
	const activeMachineId = useSelector(getSelectedMachineId)!;
	const machine = useSelector(state => getMachine(state, activeMachineId));
	const components = useSelector(state => getComponentsForMachine(state, machine.id))
	const connectors = useSelector(state => getConnectorsForMachine(state, machine.id))
	const notes = useSelector(state => getMachineNotes(state, machine.id));
	const svgRef = useRef<SVGSVGElement>(null);
	
	const performToolAction = (tool: BoardTool, x: number, y: number) => {
		if (tool === BoardTool.createProducer) {
			createProducer(machine.id, x, y);
		}
		else if (tool === BoardTool.createBucket) {
			createBucket(machine.id, x, y);
		}
		else if (tool === BoardTool.createMachineNote) {
			createNote(appMode, machine.id, x, y);
		}
		else if (tool === BoardTool.createLSystem) {
			createLSystem(machine.id, x, y);
		}
		
		setTool(null);
	};
	
	const handleConnectorCreationStep = useCallback((item: SelectedBoardItem) => {
		if (isToolConnector(tool)) {
			if (!firstSelectedItem) {
				setFirstSelectedItem(item);
			}
			else {
				if (firstSelectedItem.id === item.id) {
					return;
				}
				
				if (tool === BoardTool.createChannel) {
					createChannel(activeMachineId, firstSelectedItem.id, item.id);
				}
				else if (tool === BoardTool.createSignal) {
					createSignal(activeMachineId, firstSelectedItem.id, item.id);
				}
				
				setTool(null);
			}
		}
	}, [tool, firstSelectedItem]);
	
	const componentSelectCallback = useCallback((component: ComponentData) => {
		if (!tool) {
			return;
		}
		
		const item: SelectedBoardItem = {
			id: component.id,
			isComponent: true,
			isColumn: false,
		};
		handleConnectorCreationStep(item);
	}, [tool, handleConnectorCreationStep]);
	
	const handleMouseMove = (e: React.MouseEvent) => {
		if (isToolConnector(tool)) {
			setMouseLocation({ x: e.clientX, y: e.clientY });
		}
	};
	
	const handleRightClick = (e: React.MouseEvent, boardX: number, boardY: number) => {
		e.preventDefault();
		e.stopPropagation();
		showMenu({ x: e.pageX, y: e.pageY, boardX, boardY });
	};
	
	return (
		<>
			<BoardHeader
				appMode={appMode}
				currentTabId={machine.id}
				openTabIds={openMachineTabs}
			/>
			<BoardCanvas
				boardId={activeMachineId}
				connectionSvgRef={svgRef}
				performToolAction={performToolAction}
				onMouseMove={handleMouseMove}
				onRightClick={handleRightClick}
			>
				<svg className="connections-svg" ref={svgRef}>
					<defs>
						<marker
							id="marker-arrow"
							viewBox="0 0 10 10"
							refX="5"
							refY="5"
							markerWidth="6"
							markerHeight="6"
							orient="auto"
						>
							<path d="M 0 0 L 10 5 L 0 10 L 3 5 z" stroke={connectorSettings.lineColor} fill={connectorSettings.lineColor} />
						</marker>
						<marker
							id="marker-arrow-selected"
							viewBox="0 0 10 10"
							refX="5"
							refY="5"
							markerWidth="6"
							markerHeight="6"
							orient="auto"
						>
							<path d="M 0 0 L 10 5 L 0 10 L 3 5 z" stroke="#000" fill="#000" />
						</marker>
					</defs>
					{
						connectors.map(c => (
							<MachineConnector
								key={c.id}
								connector={c}
							/>
						))
					}
				</svg>
				{
					components.map(x => (
						<MachineComponent
							key={x.id}
							component={x}
							onSelectCallback={componentSelectCallback}
						/>
					))
				}
				{
					notes.map(n => (
						<Note
							key={n.id}
							note={n}
						/>
					))
				}
				<MouseTooltip />
			</BoardCanvas>
		</>
	);
}

function MouseTooltip() {
	const { tool, mouseLocation, firstSelectedItem } = useBoardItemCreation();
	const ref = useRef<HTMLDivElement>(null);
	
	useLayoutEffect(() => {
		if (ref.current) {
			ref.current.style.top = `${mouseLocation.y + 10}px`;
			ref.current.style.left = `${mouseLocation.x - 10}px`;
		}
	}, [mouseLocation]);
	
	if (!isToolConnector(tool)) {
		return null;
	}
	
	const renderMessage = () => {
		if (!firstSelectedItem) {
			return "Select first component";
		}
		else {
			return "Select second component";
		}
	};
	
	return (
		<MouseTooltipWrapper ref={ref}>
			{renderMessage()}
		</MouseTooltipWrapper>
	);
}

const MouseTooltipWrapper = styled.div`
	position: fixed;
	z-index: 10000;
	width: min-content;
	height: min-content;
	color: black;
	background: #ccc;
	padding: 5px;
`;
