import { RightClickMenu } from "@/components/widgets";
import { BoardItemCreation, RightClickProvider } from "@/contexts";
import { AppMode } from "@/data";
import { getAppMode, getSelectedMachineId, getSelectedNamespaceId } from "@/selectors";
import { useSelector } from "@/store";
import styled from "@emotion/styled";
import { DatabaseBoard } from "./DatabaseBoard";
import { MachineBoard } from "./MachineBoard";
import { SelectMachineBoard } from "./SelectMachineBoard";
import { SelectNamespaceBoard } from "./SelectNamespaceBoard";

export function Board() {
	const appMode = useSelector(getAppMode);
	const selectedMachineId = useSelector(getSelectedMachineId);
	const selectedNamespaceId = useSelector(getSelectedNamespaceId);
	
	const renderModeBoard = () => {
		switch (appMode) {
			case AppMode.database:
				if (!selectedNamespaceId) {
					return <SelectNamespaceBoard />;
				}
				return <DatabaseBoard />;
			case AppMode.machine:
				if (!selectedMachineId) {
					return <SelectMachineBoard />;
				}
				return <MachineBoard />;
		}
	};
	
	return (
		<BoardItemCreation>
			<RightClickProvider>
				<BoardWrapper className="board">
					{renderModeBoard()}
				</BoardWrapper>
				<RightClickMenu />
			</RightClickProvider>
		</BoardItemCreation>
	);
};

const BoardWrapper = styled.div`
	position: relative;
	flex-grow: 1;
	display: flex;
	flex-direction: column;
	overflow: auto;
	user-select: none;
	-moz-user-select: none;
	-webkit-user-select: none;
	background: ${props => props.theme.board.backgroundColor};
`;
