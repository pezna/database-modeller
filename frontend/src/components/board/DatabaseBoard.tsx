import { BoardCanvas } from "@/components/board/BoardCanvas";
import { BoardHeader } from "@/components/board/board-header/BoardHeader";
import { SelectedBoardItem, useBoardItemCreation, useRightClickContext } from "@/contexts";
import { AppMode, BoardTool, ColumnData } from "@/data";
import { useModelCreation } from "@/hooks";
import { getDatabaseModeUi, getDatabaseNotes, getRelationshipsForNamespace, getSelectedNamespaceId, getShowRelationshipLines, getTablesForNamespace } from "@/selectors";
import { useSelector } from "@/store";
import styled from "@emotion/styled";
import { useCallback, useLayoutEffect, useRef } from "react";
import { Note } from "./note/Note";
import { Relationship } from "./relationship/Relationship";
import { Table } from "./table/Table";

const appMode = AppMode.database;

export function DatabaseBoard() {
	const { showMenu } = useRightClickContext();
	const { tool, setTool, firstSelectedItem, setFirstSelectedItem, setMouseLocation } = useBoardItemCreation();
	const { createTable, createNote, createRelationship } = useModelCreation();
	const openNamespaceTabs = useSelector(state => getDatabaseModeUi(state).openNamespaceTabs);
	const activeNamespaceId = useSelector(getSelectedNamespaceId)!;
	const tables = useSelector(state => getTablesForNamespace(state, activeNamespaceId));
	const relationships = useSelector(state => getRelationshipsForNamespace(state, activeNamespaceId));
	const notes = useSelector(state => getDatabaseNotes(state, activeNamespaceId));
	const showRelationshipLines = useSelector(getShowRelationshipLines);
	const svgRef = useRef<SVGSVGElement>(null);
	
	const performToolAction = (tool: BoardTool, x: number, y: number) => {
		if (tool === BoardTool.createTable) {
			createTable(activeNamespaceId, x, y);
		}
		else if (tool === BoardTool.createDatabaseNote) {
			createNote(appMode, activeNamespaceId, x, y);
		}
		
		setTool(null);
	};
	
	const handleConnectorCreationStep = useCallback((item: SelectedBoardItem) => {
		if (tool !== BoardTool.createRelationship) {
			return;
		}
		
		if (!firstSelectedItem) {
			setFirstSelectedItem(item);
		}
		else {
			if (firstSelectedItem.id === item.id) {
				return;
			}
			
			if (tool === BoardTool.createRelationship) {
				createRelationship(firstSelectedItem.tableId!, [firstSelectedItem.id], item.tableId!, [item.id]);
			}
			
			setTool(null);
		}
	}, [tool, firstSelectedItem]);
	
	const columnSelectCallback = useCallback((column: ColumnData) => {
		if (!tool) {
			return;
		}
		
		const item: SelectedBoardItem = {
			id: column.id,
			isComponent: false,
			isColumn: true,
			tableId: column.tableId,
		};
		handleConnectorCreationStep(item);
	}, [tool, handleConnectorCreationStep]);
	
	const handleMouseMove = (e: React.MouseEvent) => {
		if (tool === BoardTool.createRelationship) {
			setMouseLocation({ x: e.clientX, y: e.clientY });
		}
	};
	
	const handleRightClick = (e: React.MouseEvent, boardX: number, boardY: number) => {
		e.preventDefault();
		e.stopPropagation();
		showMenu({ x: e.pageX, y: e.pageY, boardX, boardY });
	};
	
	return (
		<>
			<BoardHeader
				appMode={appMode}
				openTabIds={openNamespaceTabs}
				currentTabId={activeNamespaceId}
			/>
			<BoardCanvas
				boardId={activeNamespaceId}
				connectionSvgRef={svgRef}
				performToolAction={performToolAction}
				onMouseMove={handleMouseMove}
				onRightClick={handleRightClick}
			>
				<svg className="connections-svg" ref={svgRef}>
					{
						showRelationshipLines
						? Object.values(relationships)
							.map((relationship) => (
								<Relationship
									key={relationship.id}
									relationship={relationship}
								/>
							))
						: null
					}
				</svg>
				{
					Object.values(tables)
					.map((table) => (
						<Table
							key={`${table.name}${table.id}`}
							table={table}
							onColumnSelect={columnSelectCallback}
						/>
					))
				}
				{
					notes.map(n => (
						<Note
							key={n.id}
							note={n}
						/>
					))
				}
				<MouseTooltip />
			</BoardCanvas>
		</>
	);
};


function MouseTooltip() {
	const { tool, mouseLocation, firstSelectedItem } = useBoardItemCreation();
	const ref = useRef<HTMLDivElement>(null);
	
	useLayoutEffect(() => {
		if (ref.current) {
			ref.current.style.top = `${mouseLocation.y + 10}px`;
			ref.current.style.left = `${mouseLocation.x - 10}px`;
		}
	}, [mouseLocation]);
	
	if (tool !== BoardTool.createRelationship) {
		return null;
	}
	
	const renderMessage = () => {
		if (!firstSelectedItem) {
			return "Select first column";
		}
		else {
			return "Select second column";
		}
	};
	
	return (
		<MouseTooltipWrapper ref={ref}>
			{renderMessage()}
		</MouseTooltipWrapper>
	);
}

const MouseTooltipWrapper = styled.div`
	position: fixed;
	z-index: 10000;
	width: min-content;
	height: min-content;
	color: black;
	background: #ccc;
	padding: 5px;
`;
