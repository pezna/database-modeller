import { initializeApp } from "@/actions";
import { Board } from "@/components/board/Board";
import { Header } from "@/components/header/Header";
import { ChooseMachineModal, ExportSqlModal, NewProjectModal } from "@/components/modals";
import { OptionsSidebar } from "@/components/options-sidebar/OptionsSidebar";
import { PropertySidebar } from "@/components/property-sidebar/PropertySidebar";
import { useDispatch } from "@/store";
import styled from "@emotion/styled";
import { useLayoutEffect } from "react";
import "./App.scss";
import { PopupNotificationContainer } from "./notification/PopupNotificationContainer";

export function App() {
	const dispatch = useDispatch();
	
	useLayoutEffect(() => {
		dispatch(initializeApp());
		return () => {};
	}, [dispatch]);
	
	const handleRightClick = (e: React.MouseEvent) => {
		if (process.env.NODE_ENV === "development") {
			return;
		}
		
		e.stopPropagation();
		e.preventDefault();
	};
	
	return (
		<Wrapper className="app" onContextMenu={handleRightClick}>
			<Header />
			<MainContainer>
				<OptionsSidebar />
				<Board />
				<PropertySidebar />
			</MainContainer>
			<PopupNotificationContainer />
			<NewProjectModal />
			<ExportSqlModal />
			<ChooseMachineModal />
		</Wrapper>
	);
}

const Wrapper = styled.div`
	background: ${props => props.theme.board.backgroundColor};
`;

const MainContainer = styled.div`
	flex-grow: 1;
	max-width: 100vw;
	display: flex;
	flex-direction: row;
	align-content: space-around;

	.options-sidebar {
		flex-shrink: 0;
	}

	.property-sidebar {
		flex-shrink: 0;
	}
`;