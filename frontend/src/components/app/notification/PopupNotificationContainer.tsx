import { clearNotifications, deleteNotification } from "@/actions";
import { NotificationData } from "@/data";
import { getNotifications } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import { Button, Message } from "rsuite";
import "./PopupNotificationContainer.scss";

export function PopupNotificationContainer() {
	const dispatch = useDispatch();
	const notifications = useSelector(getNotifications);
	
	const clearAll = () => dispatch(clearNotifications());
	
	const renderNotification = (data: NotificationData) => {
		const del = () => dispatch(deleteNotification(data.id));
		
		return (
			<Message
				key={data.id}
				showIcon
				closable
				duration={0}
				type={data.alertType}
				header={data.title}
				onClose={del}
			>
				{data.message}
			</Message>
		);
	};
	
	if (Object.keys(notifications).length === 0) {
		return null;
	}
	
	return (
		<div className="popup-notification-container">
			{Object.values(notifications).map(renderNotification)}
			<Button onClick={clearAll} size="sm">
				Clear All
			</Button>
		</div>
	);
}
