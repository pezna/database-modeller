import { newModelState } from "@/actions";
import { FlexRowWithAction } from "@/components/widgets/FlexRowWithAction";
import { HelpIcon } from "@/components/widgets/icons";
import { useModalContext } from "@/contexts";
import { useDispatch } from "@/store";
import { useState } from "react";
import { Button, Form, Message, Modal, Toggle } from "rsuite";

export function NewProjectModal() {
	const dispatch = useDispatch();
	const { showNewProject, setShowNewProject } = useModalContext();
	const [copySchemaSettings, setCopySchemaSettings] = useState(true);
	const [showSchemaSettingsTooltip, setShowSchemaSettingsTooltip] = useState(false);
	
	const handleCreate = () => {
		dispatch(newModelState(copySchemaSettings));
		handleClose();
	};
	
	const handleClose = () => setShowNewProject(false);
	
	const toggleTooltip = () => setShowSchemaSettingsTooltip(!showSchemaSettingsTooltip);
	
	const renderSchemaSettingsTooltip = () => {
		if (!showSchemaSettingsTooltip) {
			return null;
		}
		
		return (
			<div className="mb-1">
				<Message type="info">
					Copy the schema settings for the current project into the new project. <br />
					Schema settings that require specific tables, namespaces, etc. are not copied. <br />
					See schema settings here: Settings sidebar &#187; Schema Settings
				</Message>
			</div>
		);
	};
	
	if (!showNewProject) {
		return null;
	}
	
	return (
		<Modal open={showNewProject} onClose={handleClose}>
			<Modal.Header>
				<Modal.Title>New Project</Modal.Title>
			</Modal.Header>
			<Modal.Body>
				<Form.Group>
					<FlexRowWithAction>
						<div>
							<label>Copy schema settings to new project?</label>
						</div>
						<HelpIcon onClick={toggleTooltip} />
					</FlexRowWithAction>
					{renderSchemaSettingsTooltip()}
					<Toggle
						checked={copySchemaSettings}
						onChange={setCopySchemaSettings}
						checkedChildren="Yes"
						unCheckedChildren="No"
					/>
				</Form.Group>
			</Modal.Body>
			<Modal.Footer>
				<Button appearance="primary" onClick={handleCreate}>Create</Button>
			</Modal.Footer>
		</Modal>
	);
}
