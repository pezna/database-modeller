import { alterSchemaSettingsProperty, exportToSql } from "@/actions";
import { FlexRowWithAction, HelpIcon } from "@/components/widgets";
import { useModalContext } from "@/contexts/ModalContext";
import { DATE_TIME_FILE_NAME_TEMPLATE, SqlExportOptions, sqlExportSelectData, SqlExportType, TYPE_FILE_NAME_TEMPLATE, UNIX_TIME_FILE_NAME_TEMPLATE } from "@/data";
import { getSqlExportSchemaSettings } from "@/selectors";
import { fileService } from "@/services";
import { useDispatch, useSelector } from "@/store";
import styled from "@emotion/styled";
import { useState } from "react";
import { Button, Checkbox, Form, Input, InputGroup, InputPicker, Message, Modal } from "rsuite";

export function ExportSqlModal() {
	const dispatch = useDispatch();
	const {
		type,
		fileNameTemplate,
		separateDirectionFolders,
		forwardFolderPath,
		backwardFolderPath,
	} = useSelector(getSqlExportSchemaSettings);
	const [showFileNameHint, setShowFileNameHint] = useState(false);
	const { showSqlExport, setShowSqlExport } = useModalContext();
	
	const closeModal = () => setShowSqlExport(false);
	const changeType = (type: SqlExportType) => dispatch(alterSchemaSettingsProperty(["export", "sql", "type"], type));
	
	const runExport = () => {
		const options: SqlExportOptions = {
			type,
			fileNameTemplate,
			separateDirectionFolders,
			forwardFolderPath,
			backwardFolderPath,
		};
		dispatch(exportToSql(options, false));
	};
	
	const handleSelectForwardFolder = async () => {
		const folder = await fileService.openFolder(forwardFolderPath);
		if (folder) {
			changeForwardFolder(folder);
		}
	};
	
	const changeForwardFolder = (folder: string) => {
		dispatch(alterSchemaSettingsProperty(["export", "sql", "forwardFolderPath"], folder));
	};
	
	const handleSelectBackwardFolder = async () => {
		const folder = await fileService.openFolder(backwardFolderPath);
		if (folder) {
			changeBackwardFolder(folder);
		}
	};
	
	const changeBackwardFolder = (folder: string) => {
		dispatch(alterSchemaSettingsProperty(["export", "sql", "backwardFolderPath"], folder));
	};
	
	const handleSeparateDirectionFolders = (checked: boolean) => {
		dispatch(alterSchemaSettingsProperty(["export", "sql", "separateDirectionFolders"], checked));
	};
	
	const changeFileNameTemplate = (template: string) => {
		dispatch(alterSchemaSettingsProperty(["export", "sql", "fileNameTemplate"], template));
	};
	
	const renderFileNameHint = () => {
		if (!showFileNameHint) {
			return null;
		}
		
		return (
			<HintWrapper type="info">
				{TYPE_FILE_NAME_TEMPLATE} &mdash; the database type chosen above (ex: sqlite)<br />
				{DATE_TIME_FILE_NAME_TEMPLATE} &mdash; numeric date and time (ex: 2024_01_30_08_55_10)<br />
				{UNIX_TIME_FILE_NAME_TEMPLATE} &mdash; unix epoch timestamp (ex: 1674690098)<br />
			</HintWrapper>
		);
	};
	
	const renderFolderInputs = () => {
		return (
			<>
			<Form.Group>
				<Form.ControlLabel>{separateDirectionFolders && "Forward Migration "} Output Folder</Form.ControlLabel>
				<InputGroup>
					<Input value={forwardFolderPath} onChange={(val) => changeForwardFolder(val)} />
					<InputGroup.Button onClick={handleSelectForwardFolder}>
						<span className="fas fa-folder-open"></span>
					</InputGroup.Button>
				</InputGroup>
			</Form.Group>
			{separateDirectionFolders && (
				<Form.Group>
					<Form.ControlLabel>Backward Migration Output Folder</Form.ControlLabel>
					<InputGroup>
						<Input value={backwardFolderPath} onChange={(val) => changeBackwardFolder(val)} />
						<InputGroup.Button onClick={handleSelectBackwardFolder}>
							<span className="fas fa-folder-open"></span>
						</InputGroup.Button>
					</InputGroup>
				</Form.Group>
			)}
			</>
		);
	};
	
	return (
		<Modal backdrop="static" open={showSqlExport} onClose={closeModal}>
			<Modal.Header>
				<Modal.Title>Export to SQL</Modal.Title>
			</Modal.Header>
			<Modal.Body>
				<Form fluid>
					<Form.Group controlId="export-db-type">
						<Form.ControlLabel>Database Type</Form.ControlLabel>
						<Form.Control
							name="export-db-type"
							accepter={InputPicker}
							data={sqlExportSelectData}
							value={type}
							cleanable={false}
							onChange={changeType}
							block
						/>
					</Form.Group>
					<Form.Group controlId="export-db-fname">
						<FlexRowWithAction>
							<Form.ControlLabel>File name template</Form.ControlLabel>
							<HelpIcon onClick={() => setShowFileNameHint(!showFileNameHint)} />
						</FlexRowWithAction>
						{renderFileNameHint()}
						<Form.Control
							name="export-db-fname"
							value={fileNameTemplate}
							onChange={changeFileNameTemplate}
						/>
					</Form.Group>
					<Form.Group controlId="export-separate-folders">
						<Form.ControlLabel>
							Separate Forward and Backward Migration Folders
						</Form.ControlLabel>
						<Checkbox
							id="export-separate-folders"
							checked={separateDirectionFolders}
							onChange={(_, checked) => handleSeparateDirectionFolders(checked)}
						/>
					</Form.Group>
					{renderFolderInputs()}
					<Form.Group>
						<Button appearance="primary" color="blue" onClick={runExport}>Export</Button>
					</Form.Group>
				</Form>
			</Modal.Body>
		</Modal>
	);
}

const HintWrapper = styled(Message)`
	margin: 1em 0;
`;