import { ListMachinesSection, NewMachineSection } from "@/components/widgets";
import { useModalContext } from "@/contexts/ModalContext";
import { getMachinesAsArray } from "@/selectors";
import { useSelector } from "@/store";
import styled from "@emotion/styled";
import { Modal } from "rsuite";

export function ChooseMachineModal() {
	const machines = useSelector(getMachinesAsArray);
	const { showMachineSelection, setShowMachineSelection } = useModalContext();
	
	const closeModal = () => setShowMachineSelection(false);
	
	return (
		<Modal backdrop="static" open={showMachineSelection} onClose={closeModal}>
			<Modal.Header>
				<Modal.Title>
					Create { machines.length > 0 ? " or Select " : "" } Machine
				</Modal.Title>
			</Modal.Header>
			<Modal.Body>
				<BodyWrapper>
					<NewMachineSection />
					<ListMachinesSection />
				</BodyWrapper>
			</Modal.Body>
		</Modal>
	);
}

const BodyWrapper = styled.div`
	display: flex;
	flex-direction: column;
	gap: 1em;
`;
