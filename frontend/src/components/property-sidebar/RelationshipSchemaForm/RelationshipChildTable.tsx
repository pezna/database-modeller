import { getRelationship, getTable } from "@/selectors";
import { useSelector } from "@/store";
import { Form, Input } from "rsuite";


interface RelationshipChildTableProps {
	relationshipId: string;
}

export function RelationshipChildTable({ relationshipId }: RelationshipChildTableProps) {
	const value = useSelector(state => getRelationship(state, relationshipId).childTableId);
	const childTable = useSelector(state => getTable(state, value));
	return (
		<Form.Group controlId="rel-child-table">
			<Form.ControlLabel>
				Child Table
				<Form.HelpText tooltip>The table that will contain the foreign key column. This cannot be changed.</Form.HelpText>
			</Form.ControlLabel>
			<Input value={childTable.name} plaintext={true} />
		</Form.Group>
	);
}
