import { alterRelationshipProperty, deleteSchemaError } from "@/actions";
import { ErrorKeyPrefix, ForeignKeyDeferrability, ModelObjectType, foreignKeyDeferrabilitySelectData } from "@/data";
import { getModelError, getRelationship } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import { Form, InputPicker } from "rsuite";

interface RelationshipDeferrabilityProps {
	relationshipId: string;
	disallowEdit: boolean;
}

const propertyName = "deferrability";

export function RelationshipDeferrability({ relationshipId, disallowEdit }: RelationshipDeferrabilityProps) {
	const dispatch = useDispatch();
	const value = useSelector(state => getRelationship(state, relationshipId).deferrability);
	const errorKey = ErrorKeyPrefix.relationshipDeferrability + relationshipId;
	const errorMessage = useSelector(state => getModelError(state, errorKey));
	
	const changeInput = (val: any) => {
		const newValue = val ? val as ForeignKeyDeferrability : undefined;
		
		if (errorMessage) {
			dispatch(deleteSchemaError(ModelObjectType.relationship, errorKey));
		}
		
		dispatch(alterRelationshipProperty(relationshipId, propertyName, newValue, errorKey));
	};
	
	return (
		<Form.Group controlId="rel-deferrable">
			<Form.ControlLabel>Deferrable</Form.ControlLabel>
			<Form.Control
				name="def"
				accepter={InputPicker}
				data={foreignKeyDeferrabilitySelectData}
				labelKey="label"
				valueKey="value"
				value={value}
				cleanable={false}
				onChange={changeInput}
				placement="bottomEnd"
				plaintext={disallowEdit}
				block
				preventOverflow
			/>
		</Form.Group>
	);
}
