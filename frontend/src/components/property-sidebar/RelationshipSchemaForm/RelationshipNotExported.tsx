import { alterRelationshipProperty, deleteSchemaError } from "@/actions";
import { ErrorMessage } from "@/components/widgets";
import { ErrorKeyPrefix, ModelObjectType } from "@/data";
import { getModelError, getRelationship } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import { Checkbox, Form } from "rsuite";


interface RelationshipNotExportedProps {
	relationshipId: string;
	disallowEdit: boolean;
}

const propertyName = "isNotExported";

export function RelationshipNotExported({ relationshipId, disallowEdit }: RelationshipNotExportedProps) {
	const dispatch = useDispatch();
	const value = useSelector(state => getRelationship(state, relationshipId).isNotExported);
	const errorKey = ErrorKeyPrefix.relationshipNotExported + relationshipId;
	const errorMessage = useSelector(state => getModelError(state, errorKey));
	
	const changeInput = (val: boolean) => {
		if (errorMessage) {
			dispatch(deleteSchemaError(ModelObjectType.relationship, errorKey));
		}
		
		dispatch(alterRelationshipProperty(relationshipId, propertyName, val, errorKey));
	};
	
	return (
		<Form.Group controlId={errorKey}>
			<Form.ControlLabel>No SQL Export</Form.ControlLabel>
			<Checkbox
				id={errorKey}
				checked={value}
				onChange={(_, checked) => changeInput(checked)}
				disabled={disallowEdit}
			/>
			<ErrorMessage errors={errorMessage} />
		</Form.Group>
	);
}
