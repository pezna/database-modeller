import { alterRelationshipProperty, deleteSchemaError } from "@/actions";
import { MultiAddSelection } from "@/components/widgets";
import { ErrorKeyPrefix, ModelObjectType } from "@/data";
import { getModelError, getRelationship, getTableColumns } from "@/selectors";
import { useDispatch, useSelector } from "@/store";


interface RelationshipChildColumnsProps {
	relationshipId: string;
	disallowEdit: boolean;
}

const propertyName = "childColumnIds";

export function RelationshipChildColumns({ relationshipId, disallowEdit }: RelationshipChildColumnsProps) {
	const dispatch = useDispatch();
	const relationship = useSelector(state => getRelationship(state, relationshipId));
	const tableColumns = useSelector(state => getTableColumns(state, relationship.childTableId));
	const value = relationship.childColumnIds;
	const errorKey = ErrorKeyPrefix.relationshipChildColumns + relationshipId;
	const errorMessage = useSelector(state => getModelError(state, errorKey));
	
	const addColumn = (columnId: string | null) => {
		if (!columnId) {
			return;
		}
		
		changeColumn(value.length, columnId);
	};
	
	const changeColumn = (index: number, columnId: string | null) => {
		if (!columnId) {
			return;
		}
		
		const columns = [...value];
		columns[index] = columnId;
		
		if (errorMessage) {
			dispatch(deleteSchemaError(ModelObjectType.relationship, errorKey));
		}
		
		dispatch(alterRelationshipProperty(relationshipId, propertyName, columns, errorKey));
	};
	
	const deleteColumn = (index: number) => {
		const columns = [...value];
		columns.splice(index, 1);
		
		dispatch(alterRelationshipProperty(relationshipId, propertyName, columns, errorKey));
	};
	
	return (
		<MultiAddSelection
			label="Child Columns"
			valueIds={value}
			options={tableColumns}
			optionLabelKey="name"
			optionValueKey="id"
			disallowEdit={disallowEdit}
			pickerPlaceholder="Select column"
			addLabel="Add column"
			cancelAddLabel="Cancel adding column"
			errorKey={errorKey}
			flashWhenEmpty
			disableValueIds={relationship.parentColumnIds}
			onChangeValue={changeColumn}
			onDelete={deleteColumn}
			onAddValue={addColumn}
		/>
	);
}
