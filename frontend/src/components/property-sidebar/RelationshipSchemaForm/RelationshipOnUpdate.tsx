import { alterRelationshipProperty, deleteSchemaError } from "@/actions";
import { ErrorKeyPrefix, ForeignKeyChangeAction, ModelObjectType, foreignKeyChangeActionSelectData } from "@/data";
import { getModelError, getRelationship } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import { Form, InputPicker } from "rsuite";


interface RelationshipOnUpdateProps {
	relationshipId: string;
	disallowEdit: boolean;
}

const propertyName = "onUpdate";

export function RelationshipOnUpdate({ relationshipId, disallowEdit }: RelationshipOnUpdateProps) {
	const dispatch = useDispatch();
	const value = useSelector(state => getRelationship(state, relationshipId).onUpdate);
	const errorKey = ErrorKeyPrefix.relationshipOnUpdate + relationshipId;
	const errorMessage = useSelector(state => getModelError(state, errorKey));
	
	const changeInput = (val: any) => {
		const newValue = val ? val as ForeignKeyChangeAction : undefined;
		
		if (errorMessage) {
			dispatch(deleteSchemaError(ModelObjectType.relationship, errorKey));
		}
		
		dispatch(alterRelationshipProperty(relationshipId, propertyName, newValue, errorKey));
	};
	
	return (
		<Form.Group controlId="rel-on-update">
			<Form.ControlLabel>On Update</Form.ControlLabel>
			<Form.Control
				name="ou"
				accepter={InputPicker}
				data={foreignKeyChangeActionSelectData}
				labelKey="label"
				valueKey="value"
				value={value}
				searchable={false}
				cleanable={false}
				onChange={changeInput}
				placement="bottomEnd"
				plaintext={disallowEdit}
				block
				preventOverflow
			/>
		</Form.Group>
	);
}
