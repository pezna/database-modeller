import { alterUiStateProperty, deleteRelationship } from "@/actions";
import { AlignFlexItems, DeleteConfirm, MixinMessage, PropertyForm } from "@/components/widgets";
import { ModelObjectType } from "@/data";
import { getRelationship, getRelationshipSidebarUi } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import { useLayoutEffect, useRef } from "react";
import { Form } from "rsuite";
import { RelationshipChildColumns } from "./RelationshipChildColumns";
import { RelationshipChildTable } from "./RelationshipChildTable";
import { RelationshipDeferrability } from "./RelationshipDeferrability";
import { RelationshipName } from "./RelationshipName";
import { RelationshipNotExported } from "./RelationshipNotExported";
import { RelationshipOnDelete } from "./RelationshipOnDelete";
import { RelationshipOnUpdate } from "./RelationshipOnUpdate";
import { RelationshipParentColumns } from "./RelationshipParentColumns";
import { RelationshipParentTable } from "./RelationshipParentTable";


interface RelationshipSchemaFormProps {
	relationshipId: string;
}

export function RelationshipSchemaForm({ relationshipId }: RelationshipSchemaFormProps) {
	const dispatch = useDispatch();
	const scrollAmount = useSelector(state => getRelationshipSidebarUi(state).scrollAmount);
	const [belongsToMixinSchemaItem, isNotExported] = useSelector(state => {
		const rel = getRelationship(state, relationshipId);
		return [rel.belongsToMixinSchemaItem, rel.isNotExported];
	});
	const isMixinClone = Boolean(belongsToMixinSchemaItem);
	const refContainer = useRef<HTMLDivElement>(null);
	
	useLayoutEffect(() => {
		const container = refContainer.current;
		if (container && container.scrollTop !== scrollAmount) {
			container.scrollTop = scrollAmount;
		}
	}, [scrollAmount]);
	
	const handleScroll = (e: React.UIEvent<HTMLDivElement>) => {
		if (e.currentTarget.scrollTop !== scrollAmount) {
			dispatch(alterUiStateProperty(["relationshipSidebar", "scrollAmount"], e.currentTarget.scrollTop));
		}
	};
	
	const confirmDeleteRelationship = () => {
		if (!relationshipId) {
			return;
		}
		
		dispatch(deleteRelationship(relationshipId));
	};
	
	const renderDelete = () => {
		if (isMixinClone) {
			return null;
		}
		
		return (
			<DeleteConfirm itemName="relationship" yes={confirmDeleteRelationship} placement="topEnd" />
		);
	};
	
	const renderExportRequiredComponents = () => {
		if (isNotExported) {
			return null;
		}
		
		return (
			<>
				<RelationshipOnUpdate relationshipId={relationshipId} disallowEdit={isMixinClone} />
				<RelationshipOnDelete relationshipId={relationshipId} disallowEdit={isMixinClone} />
				<RelationshipDeferrability relationshipId={relationshipId} disallowEdit={isMixinClone} />
			</>
		);
	};
	
	return (
		<PropertyForm.Container ref={refContainer} onScroll={handleScroll}>
			<PropertyForm.Header>
				<h4>Foreign Key Relationship</h4>
			</PropertyForm.Header>
			<PropertyForm.Contents>
				<Form fluid>
					<RelationshipName relationshipId={relationshipId} disallowEdit={true} />
					<RelationshipChildTable relationshipId={relationshipId} />
					<RelationshipChildColumns relationshipId={relationshipId} disallowEdit={isMixinClone} />
					<RelationshipParentTable relationshipId={relationshipId} disallowEdit={isMixinClone} />
					<RelationshipParentColumns relationshipId={relationshipId} disallowEdit={isMixinClone} />
					<RelationshipNotExported relationshipId={relationshipId} disallowEdit={isMixinClone} />
					{renderExportRequiredComponents()}
				</Form>
			</PropertyForm.Contents>
			<PropertyForm.Footer>
				<AlignFlexItems align="right">
					<MixinMessage
						visible={isMixinClone}
						schemaType={ModelObjectType.relationship}
						mixinParentId={belongsToMixinSchemaItem}
					/>
					{renderDelete()}
				</AlignFlexItems>
			</PropertyForm.Footer>
		</PropertyForm.Container>
	);
}
