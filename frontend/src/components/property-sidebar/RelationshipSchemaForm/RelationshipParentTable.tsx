import { alterRelationshipProperty, deleteSchemaError } from "@/actions";
import { TableCascadeInput } from "@/components/widgets";
import { ActionData, ErrorKeyPrefix, ModelObjectType } from "@/data";
import { getModelError, getRelationship } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import { Form } from "rsuite";

interface RelationshipParentTableProps {
	relationshipId: string;
	disallowEdit: boolean;
}

const propertyName = "parentTableId";

export function RelationshipParentTable({ relationshipId, disallowEdit }: RelationshipParentTableProps) {
	const dispatch = useDispatch();
	const value = useSelector(state => getRelationship(state, relationshipId).parentTableId);
	const errorKey = ErrorKeyPrefix.relationshipParentTable + relationshipId;
	const errorMessage = useSelector(state => getModelError(state, errorKey));
	
	const changeInput = (newValue: string | null) => {
		if (!newValue) {
			return;
		}
		
		if (errorMessage) {
			dispatch(deleteSchemaError(ModelObjectType.relationship, errorKey));
		}
		
		if (handleResponse(dispatch(alterRelationshipProperty(relationshipId, propertyName, newValue, errorKey)))) {
			// if successfully changed table, set columns to empty
			const parentColumnErrorKey = ErrorKeyPrefix.relationshipParentColumns + relationshipId;
			dispatch(alterRelationshipProperty(relationshipId, "parentColumnIds", [], parentColumnErrorKey));
		}
	}
	
	const handleResponse = (res: ActionData) => {
		return !res.errors;
	}
	
	return (
		<Form.Group controlId="rel-parent-table">
			<Form.ControlLabel>
				Parent Table
				<Form.HelpText tooltip>The table referenced by the foreign key in the child table. No mixins.</Form.HelpText>
			</Form.ControlLabel>
			<TableCascadeInput
				tableType="nonMixins"
				value={value}
				onChange={changeInput}
				disallowEdit={disallowEdit}
			/>
		</Form.Group>
	);
}
