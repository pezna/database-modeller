import { alterUiStateProperty } from "@/actions";
import { Accordion, AlignFlexItems, ErrorMessage, Link, SlideReveal, TableCascadeInput, TextButtonWithIcon } from "@/components/widgets";
import { RelationshipData } from "@/data";
import { useModelCreation, useModelNavigation } from "@/hooks";
import { getColumn, getColumnSidebarUi, getModelError, getTable, getTableRelationships, getTableSidebarUi } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import styled from "@emotion/styled";
import { useState } from "react";
import { Button, Form } from "rsuite";

const createRelationshipErrorPrefix = "createRelationship";

interface RelationshipGroupFormProps {
	tableId: string;
	columnId?: string;
}

export function RelationshipGroupForm({ tableId, columnId }: RelationshipGroupFormProps) {
	const dispatch = useDispatch();
	const uiKey = columnId ? "columnSidebar" : "tableSidebar";
	const isForeignKeyGroupExpanded = useSelector(
		state => (
				columnId
				? getColumnSidebarUi(state)
				: getTableSidebarUi(state)
			).isForeignKeyGroupExpanded
	);
	const relationships = useSelector(state => getTableRelationships(state, tableId));
	const ownedRelationships = relationships.filter(
		r => r.childTableId === tableId &&
			(columnId ? r.childColumnIds.includes(columnId) : true)
	);
	const notOwnedRelationshipes = relationships.filter(
		r => !ownedRelationships.includes(r) &&
			(columnId ? r.parentColumnIds.includes(columnId) : true)
	);
	const addRelationshipErrorKey = createRelationshipErrorPrefix + tableId;
	const errors = useSelector(state => getModelError(state, addRelationshipErrorKey));
	const { createRelationship } = useModelCreation();
	
	const handleExpanded = (ex: boolean) => {
		dispatch(alterUiStateProperty([uiKey, "isForeignKeyGroupExpanded"], ex));
	};
	
	const addRelationship = (otherTableId: string) => {
		createRelationship(tableId, [], otherTableId, [], addRelationshipErrorKey);
	};
	
	const renderHeading = () => {
		return (
			<div>
				Relationships
				<span> {ownedRelationships.length ? `(${ownedRelationships.length})` : ""}</span>
			</div>
		);
	};
	
	const renderOwned = () => {
		if (!ownedRelationships.length) {
			return null;
		}
		
		return (
			<>
				<h6>Owned</h6>
				{
					ownedRelationships
					.map(rel => (
						<TableForeignKey
							key={rel.id}
							relationship={rel}
						/>
					))
				}
			</>
		)
	};
	
	const renderNotOwned = () => {
		if (!notOwnedRelationshipes.length) {
			return null;
		}
		
		return (
			<>
				<h6>Not Owned</h6>
				{
					notOwnedRelationshipes
					.map(rel => (
						<TableForeignKey
							key={rel.id}
							relationship={rel}
						/>
					))
				}
			</>
		)
	};
	
	const renderAddRelationship = () => {
		return (
			<CreateForm onComplete={addRelationship} />
		)
	};
	
	return (
		<Accordion
			heading={renderHeading()}
			expanded={isForeignKeyGroupExpanded}
			expandCallback={handleExpanded}
		>
			{renderOwned()}
			{renderNotOwned()}
			{renderAddRelationship()}
			<ErrorMessage errors={errors} />
		</Accordion>
	);
}

interface TableForeignKeyProps {
	relationship: RelationshipData;
}

function TableForeignKey({ relationship }: TableForeignKeyProps) {
	const childTableName = useSelector(state => getTable(state, relationship.childTableId).name);
	const parentTableName = useSelector(state => getTable(state, relationship.parentTableId).name);
	const parentColumns = useSelector(
		state => relationship.parentColumnIds.map(id => getColumn(state, id))
	);
	const childColumns = useSelector(
		state => relationship.childColumnIds.map(id => getColumn(state, id))
	);
	const { selectTable, selectColumn, selectRelationship } = useModelNavigation();
	
	const selectParentTable = () => selectTable(relationship.parentTableId);
	const selectChildTable = () => selectTable(relationship.childTableId);
	const selectChildColumn = (id: string) => selectColumn(id);
	
	const renderPlainColumns = () => {
		return (
			<Group>
				<div>Table:</div>
				<Link onClick={selectChildTable}>{childTableName}</Link>
				<div>Columns:</div>
				{
					childColumns.map(c => (
						<Link
							key={c.id}
							onClick={() => selectChildColumn(c.id)}
						>
							{c.name}
						</Link>
					))
				}
			</Group>
		);
	};
	
	const renderPlainParentColumns = () => {
		return (
			<Group>
				<div>Other Table:</div>
				<Link onClick={selectParentTable}>{parentTableName}</Link>
				<div>Other Columns:</div>
				{
					parentColumns.map(c => (
						<Link
							key={c.id}
							onClick={() => selectChildColumn(c.id)}
						>
							{c.name}
						</Link>
					))
				}
			</Group>
		);
	};
	
	const handleSelectRelationship = () => selectRelationship(relationship.id);
	
	return (
		<Wrapper>
			<legend>{relationship.name}</legend>
			<ForeignKeyContainer>
				{renderPlainColumns()}
				<Group>
					<div>&nbsp;</div>
					<span className="fa fa-arrow-right"></span>
					<div>&nbsp;</div>
				</Group>
				{renderPlainParentColumns()}
			</ForeignKeyContainer>
			<AlignFlexItems align="right">
				<Button appearance="subtle" onClick={handleSelectRelationship}>
					<span className="fa fa-pencil-alt"></span>
				</Button>
			</AlignFlexItems>
		</Wrapper>
	);
}

const Group = styled.div`
	display: flex;
	flex-direction: column;
	justify-content: space-between;
	align-content: center;
	overflow: hidden;
	text-overflow: ellipsis;
	white-space: nowrap;
	gap: 2px;
`;

const ForeignKeyContainer = styled.div`
	display: flex;
	flex-direction: row;
	justify-content: space-between;
	gap: 1em;
`;

const Wrapper = styled.fieldset`
	margin-bottom: 1em;
	
	legend {
		max-width: 100%;
		overflow: hidden;
		text-overflow: ellipsis;
	}
`;

interface CreateFormProps {
	onComplete: (otherTableId: string) => void;
}

function CreateForm({ onComplete }: CreateFormProps) {
	const [showForm, setShowForm] = useState(false);
	
	const renderAlwaysVisible = () => {
		if (showForm) {
			return (
				<TextButtonWithIcon
					key="1"
					appearance="subtle"
					color="red"
					onClick={() => setShowForm(false)}
				>
					<span className="fas fa-minus"></span>
					Cancel Add Foreign Key
				</TextButtonWithIcon>
			);
		}
		return (
			<TextButtonWithIcon
				key="2"
				appearance="subtle"
				color="green"
				onClick={() => setShowForm(true)}
			>
				<span className="fa-solid fa-plus"></span>
				Add Foreign Key
			</TextButtonWithIcon>
		);
	};
	
	const renderHidden = () => {
		return (
			<Form.Group controlId="new-parent-table-picker">
				<Form.ControlLabel>
					Parent Table
					<Form.HelpText tooltip>The table referenced by the foreign key in the child table. Cannot be a mixin.</Form.HelpText>
				</Form.ControlLabel>
				<TableCascadeInput
					tableType="nonMixins"
					value=""
					onChange={(val) => onComplete(val)}
				/>
			</Form.Group>
		);
	};
	
	return (
		<SlideReveal
			showHidden={showForm}
			alwaysVisibleComponent={renderAlwaysVisible}
			hiddenComponent={renderHidden}
		/>
	);
}
