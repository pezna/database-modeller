import { alterRelationshipProperty, deleteSchemaError } from "@/actions";
import { ErrorMessage } from "@/components/widgets";
import { ErrorKeyPrefix, ModelObjectType } from "@/data";
import { getModelError, getRelationship } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import { Form } from "rsuite";


interface RelationshipNameProps {
	relationshipId: string;
	disallowEdit: boolean;
}

const propertyName = "name";

export function RelationshipName({ relationshipId, disallowEdit }: RelationshipNameProps) {
	const dispatch = useDispatch();
	const value = useSelector(state => getRelationship(state, relationshipId).name);
	const errorKey = ErrorKeyPrefix.relationshipName + relationshipId;
	const errorMessage = useSelector(state => getModelError(state, errorKey));
	
	const changeInput = (newValue: string) => {
		if (errorMessage) {
			dispatch(deleteSchemaError(ModelObjectType.relationship, errorKey));
		}
		
		dispatch(alterRelationshipProperty(relationshipId, propertyName, newValue, errorKey));
	}
	
	return (
		<Form.Group controlId="edit-relationship-name">
			<Form.ControlLabel>Name</Form.ControlLabel>
			<Form.Control
				name="name"
				type="text"
				value={value}
				onChange={changeInput}
				plaintext={disallowEdit}
			/>
			<ErrorMessage errors={errorMessage} />
		</Form.Group>
	);
}
