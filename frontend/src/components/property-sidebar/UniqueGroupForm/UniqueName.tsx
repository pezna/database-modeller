import { alterUniqueProperty, deleteSchemaError } from "@/actions";
import { ErrorMessage } from "@/components/widgets";
import { ErrorKeyPrefix, ModelObjectType } from "@/data";
import { getModelError, getUnique } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import { Form } from "rsuite";


interface UniqueNameProps {
	uniqueId: string;
	disallowEdit: boolean;
}

const propertyName = "name";

export function UniqueName({ uniqueId, disallowEdit }: UniqueNameProps) {
	const dispatch = useDispatch();
	const value = useSelector(state => getUnique(state, uniqueId).name);
	const errorKey = ErrorKeyPrefix.uniqueName + uniqueId;
	const errorMessage = useSelector(state => getModelError(state, errorKey));
	
	const changeInput = (newValue: string) => {
		if (errorMessage) {
			dispatch(deleteSchemaError(ModelObjectType.unique, errorKey));
		}
		
		dispatch(alterUniqueProperty(uniqueId, propertyName, newValue, errorKey));
	}
	
	return (
		<Form.Group controlId="edit-unique-name">
			<Form.ControlLabel>Name</Form.ControlLabel>
			<Form.Control
				name="name"
				type="text"
				value={value}
				onChange={changeInput}
				plaintext={disallowEdit}
			/>
			<ErrorMessage errors={errorMessage} />
		</Form.Group>
	);
}
