import { alterUniqueProperty, deleteSchemaError } from "@/actions";
import { ErrorMessage } from "@/components/widgets";
import { ErrorKeyPrefix, ModelObjectType } from "@/data";
import { getModelError, getUnique } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import { Checkbox, Form } from "rsuite";


interface UniqueIndexProps {
	uniqueId: string;
	disallowEdit: boolean;
}

const propertyName = "isIndex";

export function UniqueIndex({ uniqueId, disallowEdit }: UniqueIndexProps) {
	const dispatch = useDispatch();
	const value = useSelector(state => getUnique(state, uniqueId).isIndex);
	const errorKey = ErrorKeyPrefix.uniqueIsIndex + uniqueId;
	const errorMessage = useSelector(state => getModelError(state, errorKey));
	
	const changeInput = (newChecked: boolean) => {
		if (errorMessage) {
			dispatch(deleteSchemaError(ModelObjectType.unique, errorKey));
		}
		
		dispatch(alterUniqueProperty(uniqueId, propertyName, newChecked, errorKey));
	};
	
	return (
		<Form.Group controlId="edit-unique-index">
			<Form.ControlLabel>Is Index</Form.ControlLabel>
			<Checkbox
				id="edit-unique-index"
				checked={value}
				onChange={(_, checked) => changeInput(checked)}
				disabled={disallowEdit}
			/>
			<ErrorMessage errors={errorMessage} />
		</Form.Group>
	);
}
