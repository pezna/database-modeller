import { alterUniqueProperty, deleteSchemaError } from "@/actions";
import { ErrorMessage } from "@/components/widgets";
import { ConflictClause, ErrorKeyPrefix, ModelObjectType, conflictClauseSelectData } from "@/data";
import { getModelError, getUnique } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import { Form, SelectPicker } from "rsuite";


interface UniqueOnConflictProps {
	uniqueId: string;
	disallowEdit: boolean;
}

const propertyName = "onConflict";

export function UniqueOnConflict({ uniqueId, disallowEdit }: UniqueOnConflictProps) {
	const dispatch = useDispatch();
	const value = useSelector(state => getUnique(state, uniqueId).onConflict);
	const errorKey = ErrorKeyPrefix.uniqueOnConflict + uniqueId;
	const errorMessage = useSelector(state => getModelError(state, errorKey));
	
	const changeInput = (val: any) => {
		const newValue = val ? val as ConflictClause : undefined;
		
		if (errorMessage) {
			dispatch(deleteSchemaError(ModelObjectType.unique, errorKey));
		}
		
		dispatch(alterUniqueProperty(uniqueId, propertyName, newValue, errorKey));
	}
	
	return (
		<Form.Group controlId="edit-unique-conflict">
			<Form.ControlLabel>On Conflict</Form.ControlLabel>
			<Form.Control
				name="on-conflict"
				accepter={SelectPicker}
				value={value}
				data={conflictClauseSelectData}
				labelKey="label"
				valueKey="value"
				onChange={changeInput}
				searchable={false}
				cleanable={false}
				plaintext={disallowEdit}
				placement="bottomEnd"
				block
				preventOverflow
			/>
			<ErrorMessage errors={errorMessage} />
		</Form.Group>
	);
}
