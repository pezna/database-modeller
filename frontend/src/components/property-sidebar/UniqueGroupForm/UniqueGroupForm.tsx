import { alterUiStateProperty } from "@/actions";
import { Accordion, TextButtonWithIcon } from "@/components/widgets";
import { useModelCreation } from "@/hooks";
import { getColumnSidebarUi, getTableSidebarUi } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import { UniqueItemForm } from "./UniqueItemForm";


interface UniqueGroupFormProps {
	uniqueIds: string[];
	tableId: string;
	columnId?: string;
}

export function UniqueGroupForm({ uniqueIds, tableId, columnId }: UniqueGroupFormProps) {
	const dispatch = useDispatch();
	const uiKey = columnId ? "columnSidebar" : "tableSidebar";
	const isUniqueGroupExpanded = useSelector(
		state => (
			columnId
			? getColumnSidebarUi(state)
			: getTableSidebarUi(state)
		).isUniqueGroupExpanded
	);
	const { createUnique } = useModelCreation();
	
	const handleExpanded = (ex: boolean) => {
		dispatch(alterUiStateProperty([uiKey, "isUniqueGroupExpanded"], ex));
	};
	
	const addUnique = () => {
		createUnique(tableId, columnId);
	};
	
	const renderAccordionBody = () => {
		return (
			<div>
				{
					uniqueIds.map(id => (
						<UniqueItemForm
							key={id}
							uniqueId={id}
							columnId={columnId}
						/>
					))
				}
				<TextButtonWithIcon appearance="subtle" color="green" onClick={addUnique}>
					<span className="fa-solid fa-plus"></span>
					Add Unique
				</TextButtonWithIcon>
			</div>
		);
	}
	
	const renderAccordionHeading = () => {
		return (
			<div>
				Uniques
				<span> {uniqueIds.length ? `(${uniqueIds.length})` : ''}</span>
			</div>
		);
	}
	
	return (
		<Accordion heading={renderAccordionHeading()} expanded={isUniqueGroupExpanded} expandCallback={handleExpanded}>
			{renderAccordionBody()}
		</Accordion>
	);
}
