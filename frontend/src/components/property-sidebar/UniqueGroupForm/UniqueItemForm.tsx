import { deleteUnique } from "@/actions";
import { AlignFlexItems, DeleteConfirm, FormGroupWrapper, MixinMessage } from "@/components/widgets";
import { ModelObjectType } from "@/data";
import { getUnique } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import { Divider } from "rsuite";
import { UniqueColumns } from "./UniqueColumns";
import { UniqueIndex } from "./UniqueIndex";
import { UniqueName } from "./UniqueName";
import { UniqueOnConflict } from "./UniqueOnConflict";

type UniqueItemFormProps = {
	uniqueId: string;
	columnId?: string;
}

export function UniqueItemForm({ uniqueId, columnId }: UniqueItemFormProps) {
	const dispatch = useDispatch();
	const belongsToMixinSchemaItem = useSelector(state => getUnique(state, uniqueId).belongsToMixinSchemaItem);
	const isMixinClone = Boolean(belongsToMixinSchemaItem);
	
	const handleDeleteUnique = () => {
		dispatch(deleteUnique(uniqueId));
	};
	
	const renderDelete = () => {
		if (isMixinClone) {
			return null;
		}
		
		return (
			<DeleteConfirm itemName="unique" yes={handleDeleteUnique} />
		);
	};
	
	return (
		<FormGroupWrapper>
			<UniqueName uniqueId={uniqueId} disallowEdit={true} />
			<UniqueColumns uniqueId={uniqueId} disallowEdit={isMixinClone} />
			<UniqueIndex uniqueId={uniqueId} disallowEdit={isMixinClone} />
			<UniqueOnConflict uniqueId={uniqueId} disallowEdit={isMixinClone} />
			<AlignFlexItems align="right">
				<MixinMessage
					visible={isMixinClone}
					schemaType={ModelObjectType.unique}
					mixinParentId={belongsToMixinSchemaItem}
					mixinColumnId={columnId}
				/>
				{renderDelete()}
			</AlignFlexItems>
			<Divider />
		</FormGroupWrapper>
	);
}
