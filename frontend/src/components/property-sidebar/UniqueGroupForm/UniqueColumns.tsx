import { alterUniqueProperty } from "@/actions";
import { MultiAddSelection } from "@/components/widgets";
import { ColumnIndexDirection, ErrorKeyPrefix, columnIndexDirectionSelectData } from "@/data";
import { getTableColumns, getUnique } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import { SelectPicker } from "rsuite";


interface UniqueColumnsProps {
	uniqueId: string;
	disallowEdit: boolean;
}

const propertyName = "columnIds";
const directionsPropertyName = "directions";

export function UniqueColumns({ uniqueId, disallowEdit }: UniqueColumnsProps) {
	const dispatch = useDispatch();
	const tableColumns = useSelector(state => getTableColumns(state, getUnique(state, uniqueId).tableId));
	
	const value = useSelector(state => getUnique(state, uniqueId).columnIds);
	const directions = useSelector(state => getUnique(state, uniqueId).directions);
	
	const errorKey = ErrorKeyPrefix.uniqueColumns + uniqueId;
	
	const addColumn = (columnId: string | null) => {
		if (!columnId) {
			return;
		}
		
		changeColumn(value.length, columnId);
		changeDirection(directions.length, ColumnIndexDirection.ascending);
	};
	
	const changeColumn = (columnIndex: number, columnId: string | null) => {
		if (!columnId) {
			return;
		}
		
		const columns = [...value];
		columns[columnIndex] = columnId;
		
		dispatch(alterUniqueProperty(uniqueId, propertyName, columns, errorKey));
	};
	
	const changeDirection = (index: number, direction: ColumnIndexDirection) => {
		const newDirections = [...directions];
		newDirections[index] = direction;
		
		dispatch(alterUniqueProperty(uniqueId, directionsPropertyName, newDirections, errorKey));
	};
	
	const deleteColumn = (index: number) => {
		const columns = [...value];
		columns.splice(index, 1);
		const newDirections = [...directions];
		newDirections.splice(index, 1);
		
		dispatch(alterUniqueProperty(uniqueId, propertyName, columns, errorKey));
		dispatch(alterUniqueProperty(uniqueId, directionsPropertyName, newDirections, errorKey));
	};
	
	const renderDirection = (index: number) => {
		const directionValue = directions[index];
		
		return (
			<SelectPicker
				data={columnIndexDirectionSelectData}
				value={directionValue}
				placeholder="Direction"
				onChange={(val) => changeDirection(index, val as ColumnIndexDirection)}
				labelKey="label"
				valueKey="value"
				cleanable={false}
				searchable={false}
				placement="bottomEnd"
				disabled={disallowEdit}
				block
				preventOverflow
			/>
		)
	};
	
	return (
		<MultiAddSelection
			label="Columns"
			valueIds={value}
			options={tableColumns}
			optionLabelKey="name"
			optionValueKey="id"
			disallowEdit={disallowEdit}
			pickerPlaceholder="Select column"
			addLabel="Add column"
			cancelAddLabel="Cancel adding column"
			errorKey={errorKey}
			flashWhenEmpty
			additionalElementPerRow={renderDirection}
			onChangeValue={changeColumn}
			onDelete={deleteColumn}
			onAddValue={addColumn}
		/>
	);
}
