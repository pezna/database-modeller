import { alterUiStateProperty, deleteTable } from "@/actions";
import { MixinGroupForm } from "@/components/property-sidebar/MixinGroupForm";
import { AlignFlexItems, DeleteConfirm, PropertyForm } from "@/components/widgets";
import { getTable, getTableIndexes, getTableSidebarUi, getTableUniques } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import { useLayoutEffect, useRef } from "react";
import { Form } from "rsuite";
import { IndexGroupForm } from "../IndexGroupForm";
import { PrimaryKeyGroupForm } from "../PrimaryKeyGroupForm";
import { RelationshipGroupForm } from "../RelationshipSchemaForm";
import { UniqueGroupForm } from "../UniqueGroupForm/UniqueGroupForm";
import { TableColumns } from "./TableColumns";
import { TableName } from "./TableName";
import { TableNamespace } from "./TableNamespace";


interface TableSchemaFormProps {
	tableId: string;
}

export function TableSchemaForm({ tableId }: TableSchemaFormProps) {
	const dispatch = useDispatch();
	const table = useSelector(state => getTable(state, tableId));
	const indexIds = useSelector(state => getTableIndexes(state, tableId).map(index => index.id));
	const uniqueIds = useSelector(state => getTableUniques(state, tableId).map(unique => unique.id));
	const scrollAmount = useSelector(state => getTableSidebarUi(state).scrollAmount);
	const refContainer = useRef<HTMLDivElement>(null);
	
	useLayoutEffect(() => {
		const container = refContainer.current;
		if (container && container.scrollTop !== scrollAmount) {
			container.scrollTop = scrollAmount;
		}
	}, [scrollAmount]);
	
	const confirmDeleteTable = () => {
		dispatch(deleteTable(table.id));
	}
	
	const handleScroll = (e: React.UIEvent<HTMLDivElement>) => {
		if (e.currentTarget.scrollTop !== scrollAmount) {
			dispatch(alterUiStateProperty(["tableSidebar", "scrollAmount"], e.currentTarget.scrollTop));
		}
	}
	
	return (
		<PropertyForm.Container ref={refContainer} onScroll={handleScroll}>
			<PropertyForm.Header>
				<h4>Table Properties</h4>
			</PropertyForm.Header>
			<PropertyForm.Contents>
				<Form fluid>
					<TableName tableId={tableId} />
					<TableNamespace tableId={tableId} />
					<TableColumns tableId={tableId} />
					<PrimaryKeyGroupForm primaryKeyId={table.primaryKeyId} tableId={tableId} />
					<RelationshipGroupForm tableId={tableId} />
					<IndexGroupForm indexIds={indexIds} tableId={tableId} />
					<UniqueGroupForm uniqueIds={uniqueIds} tableId={tableId} />
					<MixinGroupForm tableId={tableId} />
				</Form>
			</PropertyForm.Contents>
			<PropertyForm.Footer>
				<AlignFlexItems align="right">
					<DeleteConfirm itemName="table" yes={confirmDeleteTable} placement="topEnd" />
				</AlignFlexItems>
			</PropertyForm.Footer>
		</PropertyForm.Container>
	);
}
