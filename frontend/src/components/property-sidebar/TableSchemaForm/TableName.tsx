import { alterTableProperty, deleteSchemaError } from "@/actions";
import { ErrorMessage } from "@/components/widgets";
import { ErrorKeyPrefix, ModelObjectType } from "@/data";
import { getModelError, getTable } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import { Form } from "rsuite";

interface TableNameProps {
	tableId: string;
}

const propertyName = "name";

export function TableName({ tableId }: TableNameProps) {
	const dispatch = useDispatch();
	const value = useSelector(state => getTable(state, tableId).name);
	const errorKey = ErrorKeyPrefix.tableName + tableId;
	const errorMessage = useSelector(state => getModelError(state, errorKey));
	
	const onInputChange = (newValue: string) => {
		if (errorMessage) {
			dispatch(deleteSchemaError(ModelObjectType.table, errorKey));
		}
		
		dispatch(alterTableProperty(tableId, propertyName, newValue, errorKey));
	}
	
	return (
		<Form.Group controlId="edit-table-name">
			<Form.ControlLabel>Name</Form.ControlLabel>
			<Form.Control
				name="name"
				type="text"
				value={value}
				onChange={onInputChange}
			/>
			<ErrorMessage errors={errorMessage} />
		</Form.Group>
	);
}
