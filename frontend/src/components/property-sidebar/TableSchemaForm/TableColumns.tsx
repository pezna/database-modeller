import { alterUiStateProperty } from "@/actions";
import { Accordion, Link, TextButtonWithIcon } from "@/components/widgets";
import { ColumnData } from "@/data";
import { useModelCreation, useModelNavigation } from "@/hooks";
import { getColumnTypes, getTableColumns, getTableSidebarUi } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import { nameSort } from "@/utils";
import styled from "@emotion/styled";
import { Popover, Whisper } from "rsuite";

interface TableColumnsProps {
	tableId: string;
}

export function TableColumns({ tableId }: TableColumnsProps) {
	const dispatch = useDispatch();
	const columns = useSelector(state => getTableColumns(state, tableId));
	const isColumnGroupExpanded = useSelector(state => getTableSidebarUi(state).isColumnGroupExpanded);
	const columnTypes = useSelector(getColumnTypes);
	const { createColumn } = useModelCreation();
	const { selectColumn, selectColumnType } = useModelNavigation();
	
	const handleExpanded = (ex: boolean) => {
		dispatch(alterUiStateProperty(["tableSidebar", "isColumnGroupExpanded"], ex));
	};
	
	const addColumn = () => {
		createColumn(tableId);
	};
	
	const renderRow = (column: ColumnData) => {
		const type = columnTypes[column.typeId];
		const name = type?.name ?? "<no type>";
		const values = type
			? Object.values(type.languages)
				.sort(nameSort)
				.map(x => x.value)
				.join(", ")
			: "<no type>";
		
		return (
			<ColumnRow key={column.id}>
				<Cell>
					<Link onClick={() => selectColumn(column.id)}>
						{column.name}
					</Link>
				</Cell>
				<Cell>
					<Whisper
						placement="topStart"
						trigger="hover"
						speaker={<Popover>{values}</Popover>}
					>
						{
							type
							? (
								<Link onClick={() => selectColumnType(column.typeId)}>
									{name}
								</Link>
							)
							: <span>{name}</span>
						}
						
					</Whisper>
				</Cell>
			</ColumnRow>
		);
	};
	
	return (
		<Accordion heading="Columns" expanded={isColumnGroupExpanded} expandCallback={handleExpanded}>
			<div>
				{columns.map(renderRow)}
			</div>
			<TextButtonWithIcon appearance="subtle" color="green" onClick={addColumn}>
				<span className="fa-solid fa-plus"></span>
				Add Column
			</TextButtonWithIcon>
		</Accordion>
	);
}

const ColumnRow = styled.div`
	display: grid;
	grid-template-columns: 1fr 1fr;
	border-top: 1px solid rgba(255, 255, 255, 0.25);
	padding-left: 1em;
	
	&:last-child {
		border-bottom: 1px solid rgba(255, 255, 255, 0.25);
		margin-bottom: 1em;
	}
`;

const Cell = styled.div`
	white-space: nowrap;
	overflow: hidden;
	text-overflow: ellipsis;
	align-self: center;
`;
