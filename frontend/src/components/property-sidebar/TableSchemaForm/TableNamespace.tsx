import { alterTableProperty, deleteSchemaError } from "@/actions";
import { ErrorMessage } from "@/components/widgets";
import { ErrorKeyPrefix, ModelObjectType } from "@/data";
import { getModelError, getSortedNamespaces, getTable } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import { Form, SelectPicker } from "rsuite";

interface TableNamespaceProps {
	tableId: string;
}

const propertyName = "namespaceId";

export function TableNamespace({ tableId }: TableNamespaceProps) {
	const dispatch = useDispatch();
	const namespaces = useSelector(getSortedNamespaces);
	const value = useSelector(state => getTable(state, tableId).namespaceId);
	const errorKey = ErrorKeyPrefix.tableNamespace + tableId;
	const errorMessage = useSelector(state => getModelError(state, errorKey));
	
	const changeValue = (id: any) => {
		if (typeof id !== "string") {
			return;
		}
		
		if (errorMessage) {
			dispatch(deleteSchemaError(ModelObjectType.table, errorKey));
		}
		
		dispatch(alterTableProperty(tableId, propertyName, id, errorKey));
	};
	
	return (
		<Form.Group controlId="edit-table-namespace">
			<Form.ControlLabel>Namespace</Form.ControlLabel>
			<Form.Control
				name="namespace"
				accepter={SelectPicker}
				value={value}
				data={namespaces}
				labelKey="name"
				valueKey="id"
				onChange={changeValue}
				cleanable={false}
				searchable={false}
				block
				preventOverflow
			/>
			<ErrorMessage errors={errorMessage} />
		</Form.Group>
	);
}
