import { alterUiStateProperty, deleteNamespace } from "@/actions";
import { DeleteConfirm, PropertyForm } from "@/components/widgets";
import { useModelNavigation } from "@/hooks";
import { getDatabaseModeUi, getSortedNamespaces } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import styled from "@emotion/styled";
import { useLayoutEffect, useRef } from "react";
import { List, Stack } from "rsuite";

export function NamespaceListSidebar() {
	const dispatch = useDispatch();
	const scrollAmount = useSelector(state => getDatabaseModeUi(state).namespaceListScrollAmount);
	const namespaces = useSelector(getSortedNamespaces);
	const { selectNamespace } = useModelNavigation();
	const refContainer = useRef<HTMLDivElement>(null);
	
	useLayoutEffect(() => {
		const container = refContainer.current;
		if (container && container.scrollTop !== scrollAmount) {
			container.scrollTop = scrollAmount;
		}
	}, [scrollAmount]);
	
	const handleScroll = (e: React.UIEvent<HTMLDivElement>) => {
		if (e.currentTarget.scrollTop !== scrollAmount) {
			dispatch(alterUiStateProperty(["databaseModeUi", "namespaceListScrollAmount"], e.currentTarget.scrollTop));
		}
	};
	
	const handleDelete = (id: string) => dispatch(deleteNamespace(id));
	
	const renderNamespaceList = () => {
		if (!namespaces.length) {
			return null;
		}
		
		return (
			<List bordered hover>
				{
					namespaces.map(m => (
						<List.Item key={m.id}>
							<Stack spacing={10}>
								<Stack.Item grow={1}>
									<NamespaceNameItem onClick={() => selectNamespace(m.id)}>
										{m.name}
									</NamespaceNameItem>
								</Stack.Item>
								<Stack.Item shrink={0}>
									<DeleteConfirm itemName="namespace" yes={() => handleDelete(m.id)} />
								</Stack.Item>
							</Stack>
						</List.Item>
					))
				}
			</List>
		);
	};
	
	return (
		<PropertyForm.Container ref={refContainer} onScroll={handleScroll}>
			<PropertyForm.Header>
				<h4>Namespaces</h4>
			</PropertyForm.Header>
			<PropertyForm.Contents>
				<Wrapper>
					{renderNamespaceList()}
				</Wrapper>
			</PropertyForm.Contents>
		</PropertyForm.Container>
	);
}

const Wrapper = styled.div`
	margin: 0 1em;
`;

const NamespaceNameItem = styled.div`
	cursor: pointer;
	
	&:hover {
		color: var(--rs-primary-500);
	}
`;
