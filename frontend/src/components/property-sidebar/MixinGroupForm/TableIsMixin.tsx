import { alterTableProperty, deleteSchemaError } from "@/actions";
import { ErrorMessage } from "@/components/widgets";
import { ErrorKeyPrefix, ModelObjectType } from "@/data";
import { getModelError, getTableRelationships } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import { Checkbox, Form } from "rsuite";


interface TableIsMixinProps {
	tableId: string;
	isMixin: boolean;
	tableHasMixins: boolean;
}

const propertyName = "isMixin";

export function TableIsMixin({ tableId, isMixin, tableHasMixins }: TableIsMixinProps) {
	const dispatch = useDispatch();
	const isParentTable = useSelector(state => getTableRelationships(state, tableId).findIndex(r => r.parentTableId === tableId) > -1);
	const errorKey = ErrorKeyPrefix.tableIsMixin + tableId;
	const errorMessage = useSelector(state => getModelError(state, errorKey));
	
	const changeInput = (newValue: boolean) => {
		if (errorMessage) {
			dispatch(deleteSchemaError(ModelObjectType.table, errorKey));
		}
		
		dispatch(alterTableProperty(tableId, propertyName, newValue, errorKey));
	};
	
	const renderInput = () => {
		if (tableHasMixins) {
			return <span>Cannot be set as a mixin because this table is using a mixin</span>;
		}
		else if (isParentTable) {
			return <span>Cannot be set as a mixin because this table is the parent in a foreign key relationship.</span>;
		}
		
		return (
			<Checkbox
				id="edit-table-is-mixin"
				checked={isMixin}
				onChange={(_, checked) => changeInput(checked)}
			/>
		);
	};
	
	return (
		<Form.Group controlId="edit-table-is-mixin">
			<Form.ControlLabel>Is Mixin</Form.ControlLabel>
			{renderInput()}
			<ErrorMessage errors={errorMessage} />
		</Form.Group>
	);
}
