import { alterUiStateProperty } from "@/actions";
import { Accordion, Link } from "@/components/widgets";
import { useModelNavigation } from "@/hooks";
import { getMixinTableClones, getTable, getTableSidebarUi } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import { TableIsMixin } from "./TableIsMixin";
import { TableMixins } from "./TableMixins";

interface MixinGroupFormProps {
	tableId: string;
}

export function MixinGroupForm({ tableId }: MixinGroupFormProps) {
	const dispatch = useDispatch();
	const table = useSelector(state => getTable(state, tableId));
	const isMixinGroupExpanded = useSelector(state => getTableSidebarUi(state).isMixinGroupExpanded);
	const isMixinGroupUsagesExpanded = useSelector(state => getTableSidebarUi(state).isMixinGroupUsagesExpanded);
	const mixinClones = useSelector(state => getMixinTableClones(state, tableId));
	const { selectTable } = useModelNavigation();
	
	const handleExpanded = (ex: boolean) => {
		dispatch(alterUiStateProperty(["tableSidebar", "isMixinGroupExpanded"], ex));
	};
	
	const handleExpandedUsages = (ex: boolean) => {
		dispatch(alterUiStateProperty(["tableSidebar", "isMixinGroupUsagesExpanded"], ex));
	};
	
	const renderUsages = () => {
		if (!table.isMixin) {
			return null;
		}
		
		return (
			<Accordion
				heading={`Usages (${mixinClones.length})`}
				useAlternateBodyBackground={true}
				expanded={isMixinGroupUsagesExpanded}
				expandCallback={handleExpandedUsages}
			>
				{
					mixinClones.map(t => (
						<div key={t.id}>
							<Link onClick={() => selectTable(t.id)}>{t.name}</Link>
						</div>
					))
				}
			</Accordion>
		);
	};
	
	return (
		<Accordion
			heading="Mixins"
			expanded={isMixinGroupExpanded}
			expandCallback={handleExpanded}
		>
			<TableIsMixin
				tableId={tableId}
				isMixin={table.isMixin}
				tableHasMixins={table.mixinTableIds.length > 0}
			/>
			<TableMixins
				tableId={tableId}
				isMixin={table.isMixin}
				mixinTableIds={table.mixinTableIds}
			/>
			{renderUsages()}
		</Accordion>
	);
}