import { alterTableProperty, deleteSchemaError } from "@/actions";
import { ErrorMessage, TableCascadeInput } from "@/components/widgets";
import { ErrorKeyPrefix, ModelObjectType } from "@/data";
import { getModelError } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import { Form } from "rsuite";

interface TableMixinsProps {
	tableId: string;
	isMixin: boolean;
	mixinTableIds: string[];
}

const propertyName = "mixinTableIds";

export function TableMixins({ tableId, isMixin, mixinTableIds }: TableMixinsProps) {
	const dispatch = useDispatch();
	const errorKey = ErrorKeyPrefix.tableMixinTableIds + tableId;
	const errorMessage = useSelector(state => getModelError(state, errorKey));
	
	const handleChangeValue = (newValueIds: any[]) => {
		if (errorMessage) {
			dispatch(deleteSchemaError(ModelObjectType.table, errorKey));
		}
		
		dispatch(alterTableProperty(tableId, propertyName, newValueIds, errorKey));
	};
	
	return (
		<Form.Group>
			<Form.ControlLabel>Mixin Tables</Form.ControlLabel>
			{
				isMixin
				? <span>A mixin table cannot contain another mixin</span>
				: (
					<TableCascadeInput
						tableType="mixins"
						value={mixinTableIds}
						onChange={handleChangeValue}
						multiPicker
					/>
				)
			}
			<ErrorMessage errors={errorMessage} />
		</Form.Group>
	);
}
