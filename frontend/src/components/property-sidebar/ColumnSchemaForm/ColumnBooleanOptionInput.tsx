import { alterColumnOptionProperty, deleteSchemaError } from "@/actions";
import { ErrorMessage } from "@/components/widgets";
import { ModelObjectType, columnOptionTypeDisplayName } from "@/data";
import { getColumnOptions, getModelError } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import { Checkbox, Form } from "rsuite";
import { BaseOptionComponentProps } from "./shared-types";


export function ColumnOptionBooleanInput({ columnId, optionType, errorKeyPrefix, disallowEdit }: BaseOptionComponentProps) {
	const dispatch = useDispatch();
	const optionValue = useSelector(state => Boolean(getColumnOptions(state, columnId)[optionType]));
	const errorKey = errorKeyPrefix + columnId;
	const errorMessage = useSelector(state => getModelError(state, errorKey));
	
	const changeInput = (newValue: boolean) => {
		if (errorMessage) {
			dispatch(deleteSchemaError(ModelObjectType.column, errorKey));
		}
		
		dispatch(alterColumnOptionProperty(columnId, optionType, newValue, errorKey));
	};
	
	return (
		<Form.Group controlId={optionType}>
			<Form.ControlLabel>{columnOptionTypeDisplayName[optionType]}</Form.ControlLabel>
			<Checkbox
				id={optionType}
				checked={optionValue}
				onChange={(_, checked) => changeInput(checked)}
				disabled={disallowEdit}
			/>
			<ErrorMessage errors={errorMessage} />
		</Form.Group>
	);
}
