import { alterColumnProperty, deleteSchemaError, setColumnType } from "@/actions";
import { ErrorMessage } from "@/components/widgets";
import { ErrorKeyPrefix, ModelObjectType } from "@/data";
import { useModelNavigation } from "@/hooks";
import { getColumn, getColumnType, getModelError, getSortedColumnTypes } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import { IdPrefix, generateId } from "@/utils";
import { Form, InputPicker } from "rsuite";


export interface ColumnTypeProps {
	columnId: string;
	disallowEdit: boolean;
}

const propertyName = "typeId";

export function ColumnType({ columnId, disallowEdit }: ColumnTypeProps) {
	const dispatch = useDispatch();
	const value = useSelector(state => getColumnType(state, getColumn(state, columnId).typeId));
	const errorKey = ErrorKeyPrefix.columnType + columnId;
	const errorMessage = useSelector(state => getModelError(state, errorKey));
	const columnTypes = useSelector(getSortedColumnTypes);
	const { selectColumnType } = useModelNavigation();
	
	const changeValue = (newValueId: string) => {
		if (newValueId) {
			let val = columnTypes.find(n => n.id === newValueId);
			if (!val) {
				const text = newValueId;
				val = {
					id: generateId(IdPrefix.columnType),
					name: text,
					languages: [],
					columnIds: [],
				};
				newValueId = val.id;
				
				dispatch(setColumnType(val));
				selectColumnType(val.id);
			}
		}
		
		if (!newValueId) {
			return;
		}
		
		if (errorMessage) {
			dispatch(deleteSchemaError(ModelObjectType.column, errorKey));
		}
		
		dispatch(alterColumnProperty(columnId, propertyName, newValueId, errorKey));
	};
	
	return (
		<Form.Group controlId="edit-column-type">
			<Form.ControlLabel>Type</Form.ControlLabel>
			<Form.Control
				name="column-types"
				accepter={InputPicker}
				value={value?.id}
				data={columnTypes}
				labelKey="name"
				valueKey="id"
				onChange={changeValue}
				creatable
				cleanable={false}
				placeholder="Search or Add"
				block
				plaintext={disallowEdit}
				preventOverflow
			/>
			<ErrorMessage errors={errorMessage} />
		</Form.Group>
	);
}
