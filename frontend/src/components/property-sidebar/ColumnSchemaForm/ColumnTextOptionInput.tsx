import { alterColumnOptionProperty, deleteSchemaError } from "@/actions";
import { ErrorMessage } from "@/components/widgets";
import { ModelObjectType, columnOptionTypeDisplayName } from "@/data";
import { getColumnOptions, getModelError } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import { Form } from "rsuite";
import { BaseOptionComponentProps } from "./shared-types";


export function ColumnOptionTextInput({ columnId, optionType, errorKeyPrefix, disallowEdit }: BaseOptionComponentProps) {
	const dispatch = useDispatch();
	const optionValue = useSelector(state => getColumnOptions(state, columnId)[optionType] as string);
	const errorKey = errorKeyPrefix + columnId;
	const errorMessage = useSelector(state => getModelError(state, errorKey));
	
	const changeInput = (newValue: string) => {
		if (errorMessage) {
			dispatch(deleteSchemaError(ModelObjectType.column, errorKey));
		}
		
		dispatch(alterColumnOptionProperty(columnId, optionType, newValue, errorKey));
	};
	
	return (
		<Form.Group controlId={optionType}>
			<Form.ControlLabel>{columnOptionTypeDisplayName[optionType]}</Form.ControlLabel>
			<Form.Control
				name={optionType}
				type="text"
				value={optionValue ?? ""}
				onChange={changeInput}
				plaintext={disallowEdit}
			/>
			<ErrorMessage errors={errorMessage} />
		</Form.Group>
	);
}
