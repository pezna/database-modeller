import { ColumnOptionType, ErrorKeyPrefix } from "@/data";

export type BaseOptionComponentProps = {
	columnId: string;
	optionType: ColumnOptionType;
	errorKeyPrefix: ErrorKeyPrefix;
	disallowEdit: boolean;
}
