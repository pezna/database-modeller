import { alterColumnProperty, deleteSchemaError } from "@/actions";
import { ErrorMessage } from "@/components/widgets";
import { ErrorKeyPrefix, ModelObjectType } from "@/data";
import { getColumn, getModelError } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import { Form } from "rsuite";


export interface ColumnNameProps {
	columnId: string;
	disallowEdit: boolean;
}

const propertyName = "name";

export function ColumnName({ columnId, disallowEdit }: ColumnNameProps) {
	const dispatch = useDispatch();
	const value = useSelector(state => getColumn(state, columnId).name);
	const errorKey = ErrorKeyPrefix.columnName + columnId;
	const errorMessage = useSelector(state => getModelError(state, errorKey));
	
	const changeInput = (newValue: string) => {
		if (errorMessage) {
			dispatch(deleteSchemaError(ModelObjectType.column, errorKey));
		}
		
		dispatch(alterColumnProperty(columnId, propertyName, newValue, errorKey));
	};
	
	return (
		<Form.Group controlId="edit-column-name">
			<Form.ControlLabel>Name</Form.ControlLabel>
			<Form.Control
				name="name"
				type="text"
				value={value}
				onChange={changeInput}
				plaintext={disallowEdit}
			/>
			<ErrorMessage errors={errorMessage} />
		</Form.Group>
	);
}
