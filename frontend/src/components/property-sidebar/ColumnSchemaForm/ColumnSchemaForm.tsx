import { alterUiStateProperty, deleteColumn, selectTable } from "@/actions";
import { RelationshipGroupForm } from "@/components/property-sidebar/RelationshipSchemaForm";
import { AlignFlexItems, DeleteConfirm, MixinMessage, PropertyForm } from "@/components/widgets";
import { ColumnOptionType, ErrorKeyPrefix, ModelObjectType } from "@/data";
import { getColumn, getColumnSidebarUi } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import { useLayoutEffect, useRef } from "react";
import { Form } from "rsuite";
import { ColumnOptionBooleanInput } from "./ColumnBooleanOptionInput";
import { ColumnName } from "./ColumnName";
import { ColumnOptionTextInput } from "./ColumnTextOptionInput";
import { ColumnType } from "./ColumnType";
import { IndexInput } from "./IndexInput";
import { UniqueInput } from "./UniqueInput";


interface ColumnSchemaFormProps {
	columnId: string;
}

export function ColumnSchemaForm({ columnId }: ColumnSchemaFormProps) {
	const dispatch = useDispatch();
	const column = useSelector(state => getColumn(state, columnId));
	const scrollAmount = useSelector(state => getColumnSidebarUi(state).scrollAmount);
	const refContainer = useRef<HTMLDivElement>(null);
	const isMixinClone = Boolean(column.belongsToMixinSchemaItem);
	
	useLayoutEffect(() => {
		const container = refContainer.current;
		if (container && container.scrollTop !== scrollAmount) {
			container.scrollTop = scrollAmount;
		}
	}, [scrollAmount]);
	
	const confirmDeleteColumn = () => {
		dispatch(deleteColumn(columnId));
	};
	
	const backToTable = () => {
		dispatch(selectTable(column.tableId));
	};
	
	const handleScroll = (e: React.UIEvent<HTMLDivElement>) => {
		if (e.currentTarget.scrollTop !== scrollAmount) {
			dispatch(alterUiStateProperty(["columnSidebar", "scrollAmount"], e.currentTarget.scrollTop));
		}
	};
	
	const renderDelete = () => {
		if (isMixinClone) {
			return null;
		}
		
		return (
			<DeleteConfirm itemName="column" yes={confirmDeleteColumn} placement="topEnd" />
		);
	};
	
	return (
		<PropertyForm.Container ref={refContainer} onScroll={handleScroll}>
			<PropertyForm.Header>
				<button className="icon-button" onClick={backToTable} title="Go to table">
					<span className="fas fa-angle-double-left"></span>
				</button>
				<h4>Column Properties</h4>
			</PropertyForm.Header>
			<PropertyForm.Contents>
				<Form fluid>
					<ColumnName columnId={column.id} disallowEdit={isMixinClone} />
					<ColumnType columnId={column.id} disallowEdit={isMixinClone} />
					<ColumnOptionTextInput
						columnId={columnId}
						optionType={ColumnOptionType.defaultValue}
						errorKeyPrefix={ErrorKeyPrefix.columnDefaultValue}
						disallowEdit={isMixinClone}
					/>
					<ColumnOptionBooleanInput
						columnId={columnId}
						optionType={ColumnOptionType.notNull}
						errorKeyPrefix={ErrorKeyPrefix.columnNotNull}
						disallowEdit={isMixinClone}
					/>
					<RelationshipGroupForm
						tableId={column.tableId}
						columnId={column.id}
					/>
					<IndexInput
						tableId={column.tableId}
						columnId={columnId}
						options={column.options}
					/>
					<UniqueInput
						columnId={columnId}
					/>
				</Form>
			</PropertyForm.Contents>
			<PropertyForm.Footer>
				<AlignFlexItems align="right">
					<MixinMessage
						visible={isMixinClone}
						schemaType={ModelObjectType.column}
						mixinParentId={column.belongsToMixinSchemaItem}
					/>
					{renderDelete()}
				</AlignFlexItems>
			</PropertyForm.Footer>
		</PropertyForm.Container>
	);
}
