import { UniqueGroupForm } from "@/components/property-sidebar/UniqueGroupForm";
import { getColumn, getColumnOptions } from "@/selectors";
import { useSelector } from "@/store";

interface UniqueInputProps {
	columnId: string;
}

export function UniqueInput({ columnId }: UniqueInputProps) {
	const tableId = useSelector(state => getColumn(state, columnId).tableId);
	const value = useSelector(state => getColumnOptions(state, columnId).unique);
	
	return <UniqueGroupForm uniqueIds={value?.uniqueIds || []} tableId={tableId} columnId={columnId} />;
}
