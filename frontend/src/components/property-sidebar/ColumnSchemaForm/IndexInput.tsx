import { IndexGroupForm } from "@/components/property-sidebar/IndexGroupForm";
import { ColumnData } from "@/data";

interface IndexInputProps {
	tableId: string;
	columnId: string;
	options: ColumnData["options"];
}

export function IndexInput({ tableId, columnId, options }: IndexInputProps) {
	const value = options.index;
	
	return (
		<IndexGroupForm
			indexIds={value?.indexIds || []}
			tableId={tableId}
			columnId={columnId}
		/>
	);
}
