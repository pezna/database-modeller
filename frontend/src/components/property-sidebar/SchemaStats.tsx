import { getColumns, getIndexes, getMixinTables, getPrimaryKeys, getRelationships, getTables, getUniques } from "@/selectors";
import { useSelector } from "@/store";
import { css } from "@emotion/react";


export function SchemaStats() {
	const numTables = useSelector(state => Object.keys(getTables(state)).length);
	const numColumns = useSelector(state => Object.keys(getColumns(state)).length);
	const numPrimaryKeys = useSelector(state => Object.keys(getPrimaryKeys(state)).length);
	const numRelationships = useSelector(state => Object.keys(getRelationships(state)).length);
	const indexes = Object.values(useSelector(getIndexes));
	const numIndexes = indexes.length;
	const indexesTableIds = new Set(indexes.map(x => x.tableId));
	const uniques = Object.values(useSelector(getUniques));
	const numUniques = uniques.length;
	const uniquesTableIds = new Set(uniques.map(x => x.tableId));
	
	const mixinTables = useSelector(getMixinTables);
	const numMixinTables = mixinTables.length;
	const numMixinColumns = mixinTables.map(x => x.columnIds).reduce((prev, curr) => prev + curr.length, 0);
	const numMixinPrimaryKeys = mixinTables.filter(x => x.primaryKeyId).length;
	const numMixinRelationships = new Set(mixinTables.map(x => x.relationshipIds).flat()).size;
	const numMixinIndexes = mixinTables.filter(t => indexesTableIds.has(t.id)).length;
	const numMixinUniques = mixinTables.filter(t => uniquesTableIds.has(t.id)).length;
	
	return (
		<div className="schema-form-container ">
			<div className="schema-form-contents">
				<div css={contentStyle}>
					<label>Tables (non-mixin):</label>
					<span>{numTables - numMixinTables}</span>
					<label>Mixin Tables:</label>
					<span>{numMixinTables}</span>
					<label>Columns (non-mixin):</label>
					<span>{numColumns - numMixinColumns}</span>
					<label>Mixin Columns:</label>
					<span>{numMixinColumns}</span>
					<label>Primary Keys (non-mixin):</label>
					<span>{numPrimaryKeys - numMixinPrimaryKeys}</span>
					<label>Mixin Primary Keys:</label>
					<span>{numMixinPrimaryKeys}</span>
					<label>Relationships (non-mixin):</label>
					<span>{numRelationships - numMixinRelationships}</span>
					<label>Mixin Relationships:</label>
					<span>{numMixinRelationships}</span>
					<label>Indexes (non-mixin):</label>
					<span>{numIndexes - numMixinIndexes}</span>
					<label>Mixin Indexes:</label>
					<span>{numMixinIndexes}</span>
					<label>Uniques (non-mixin):</label>
					<span>{numUniques - numMixinUniques}</span>
					<label>Mixin Uniques:</label>
					<span>{numMixinUniques}</span>
				</div>
			</div>
		</div>
	);
}

const contentStyle = css({
	display: "grid",
	gridTemplateColumns: "2fr 1fr",
	alignItems: "flex-start",
	padding: "10px",
	rowGap: "10px",
});
