import { alterPrimaryKeyProperty, deleteSchemaError } from "@/actions";
import { MultiAddSelection } from "@/components/widgets";
import { ColumnIndexDirection, ErrorKeyPrefix, ModelObjectType, columnIndexDirectionSelectData } from "@/data";
import { getModelError, getPrimaryKey, getTableColumns } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import { SelectPicker } from "rsuite";

interface PrimaryKeyColumnsProps {
	primaryKeyId: string;
	disallowEdit: boolean;
}

const propertyName = "columnIds";
const directionsPropertyName = "directions";

export function PrimaryKeyColumns({ primaryKeyId, disallowEdit }: PrimaryKeyColumnsProps) {
	const dispatch = useDispatch();
	const tableColumns = useSelector(state => getTableColumns(state, getPrimaryKey(state, primaryKeyId).tableId));
	
	const value = useSelector(state => getPrimaryKey(state, primaryKeyId).columnIds);
	const directions = useSelector(state => getPrimaryKey(state, primaryKeyId).directions);
	
	const errorKey = ErrorKeyPrefix.primaryKeyColumns + primaryKeyId;
	const errorMessage = useSelector(state => getModelError(state, errorKey));
	
	const addColumn = (columnId: string) => {
		changeColumn(value.length, columnId);
		changeDirection(directions.length, ColumnIndexDirection.ascending);
	}
	
	const changeColumn = (index: number, columnId: string | null) => {
		if (!columnId) {
			return;
		}
		
		const columns = [...value];
		columns[index] = columnId;
		
		if (errorMessage) {
			dispatch(deleteSchemaError(ModelObjectType.primaryKey, errorKey));
		}
		
		dispatch(alterPrimaryKeyProperty(primaryKeyId, propertyName, columns, errorKey));
	}
	
	const changeDirection = (index: number, direction: ColumnIndexDirection) => {
		const newDirections = [...directions];
		newDirections[index] = direction;
		
		dispatch(alterPrimaryKeyProperty(primaryKeyId, directionsPropertyName, newDirections, errorKey));
	}
	
	const deleteColumn = (index: number) => {
		const columns = [...value];
		columns.splice(index, 1);
		const newDirections = [...directions];
		newDirections.splice(index, 1);
		
		dispatch(alterPrimaryKeyProperty(primaryKeyId, propertyName, columns, errorKey));
		dispatch(alterPrimaryKeyProperty(primaryKeyId, directionsPropertyName, newDirections, errorKey));
	}
	
	const renderDirection = (index: number) => {
		const directionValue = directions[index];
		
		return (
			<SelectPicker
				data={columnIndexDirectionSelectData}
				value={directionValue}
				placeholder="Direction"
				onChange={(val) => changeDirection(index, val as ColumnIndexDirection)}
				labelKey="label"
				valueKey="value"
				searchable={false}
				cleanable={false}
				placement="bottomEnd"
				block
				preventOverflow
			/>
		);
	};
	
	return (
		<MultiAddSelection
			label="Columns"
			valueIds={value}
			options={tableColumns}
			optionLabelKey="name"
			optionValueKey="id"
			disallowEdit={disallowEdit}
			pickerPlaceholder="Select column"
			addLabel="Add column"
			cancelAddLabel="Cancel adding column"
			errorKey={errorKey}
			flashWhenEmpty
			additionalElementPerRow={renderDirection}
			onChangeValue={changeColumn}
			onDelete={deleteColumn}
			onAddValue={addColumn}
		/>
	);
}
