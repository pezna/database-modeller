import { alterPrimaryKeyProperty, deleteSchemaError } from "@/actions";
import { ErrorMessage } from "@/components/widgets";
import { ConflictClause, ErrorKeyPrefix, ModelObjectType, conflictClauseSelectData } from "@/data";
import { getModelError, getPrimaryKey } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import { Form, SelectPicker } from "rsuite";


interface PrimaryKeyOnConflictProps {
	primaryKeyId: string;
	disallowEdit: boolean;
}

const propertyName = "onConflict";

export function PrimaryKeyOnConflict({ primaryKeyId, disallowEdit }: PrimaryKeyOnConflictProps) {
	const dispatch = useDispatch();
	const value = useSelector(state => getPrimaryKey(state, primaryKeyId).onConflict);
	const errorKey = ErrorKeyPrefix.primaryKeyOnConflict + primaryKeyId;
	const errorMessage = useSelector(state => getModelError(state, errorKey));
	
	const changeInput = (val: any) => {
		const newValue = val ? val as ConflictClause : undefined;
		
		if (errorMessage) {
			dispatch(deleteSchemaError(ModelObjectType.primaryKey, errorKey));
		}
		
		dispatch(alterPrimaryKeyProperty(primaryKeyId, propertyName, newValue, errorKey));
	};
	
	return (
		<Form.Group controlId="edit-pk-conflict">
			<Form.ControlLabel>On Conflict</Form.ControlLabel>
			<Form.Control
				name="on-conflict"
				accepter={SelectPicker}
				value={value}
				data={conflictClauseSelectData}
				labelKey="label"
				valueKey="value"
				onChange={changeInput}
				searchable={false}
				cleanable={false}
				plaintext={disallowEdit}
				placement="bottomEnd"
				block
				preventOverflow
			/>
			<ErrorMessage errors={errorMessage} />
		</Form.Group>
	);
}
