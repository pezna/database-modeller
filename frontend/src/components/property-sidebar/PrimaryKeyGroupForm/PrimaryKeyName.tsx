import { alterPrimaryKeyProperty, deleteSchemaError } from "@/actions";
import { ErrorMessage } from "@/components/widgets";
import { ErrorKeyPrefix, ModelObjectType } from "@/data";
import { getModelError, getPrimaryKey } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import { Form } from "rsuite";


interface PrimaryKeyNameProps {
	primaryKeyId: string;
	disallowEdit: boolean;
}

const propertyName = 'name';

export function PrimaryKeyName({ primaryKeyId, disallowEdit }: PrimaryKeyNameProps) {
	const dispatch = useDispatch();
	const value = useSelector(state => getPrimaryKey(state, primaryKeyId).name);
	const errorKey = ErrorKeyPrefix.primaryKeyName + primaryKeyId;
	const errorMessage = useSelector(state => getModelError(state, errorKey));
	
	const changeInput = (newValue: string) => {
		if (errorMessage) {
			// if there is a previous error, remove it since the user is editing
			dispatch(deleteSchemaError(ModelObjectType.primaryKey, errorKey));
		}
		
		dispatch(alterPrimaryKeyProperty(primaryKeyId, propertyName, newValue, errorKey));
	}
	
	return (
		<Form.Group controlId="edit-pk-name">
			<Form.ControlLabel>Name</Form.ControlLabel>
			<Form.Control
				name="name"
				type="text"
				value={value}
				onChange={changeInput}
				plaintext={disallowEdit}
			/>
			<ErrorMessage errors={errorMessage} />
		</Form.Group>
	);
}
