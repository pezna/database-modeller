import { alterUiStateProperty, deletePrimaryKey } from "@/actions";
import { Accordion, AlignFlexItems, DeleteConfirm, FormGroupWrapper, MixinMessage, TextButtonWithIcon } from "@/components/widgets";
import { ModelObjectType } from "@/data";
import { useModelCreation } from "@/hooks";
import { getPrimaryKey, getTableSidebarUi } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import { PrimaryKeyColumns } from "./PrimaryKeyColumns";
import { PrimaryKeyName } from "./PrimaryKeyName";
import { PrimaryKeyOnConflict } from "./PrimaryKeyOnConflict";


interface PrimaryKeyGroupFormProps {
	primaryKeyId: string | null;
	tableId: string;
}

export function PrimaryKeyGroupForm({ primaryKeyId, tableId }: PrimaryKeyGroupFormProps) {
	const dispatch = useDispatch();
	const belongsToMixinSchemaItem = useSelector(state => primaryKeyId ? getPrimaryKey(state, primaryKeyId).belongsToMixinSchemaItem : undefined);
	const isMixinClone = Boolean(belongsToMixinSchemaItem);
	const isPrimaryKeyExpanded = useSelector(state => getTableSidebarUi(state).isPrimaryKeyExpanded);
	const { createPrimaryKey } = useModelCreation();
	
	const handleExpanded = (ex: boolean) => {
		dispatch(alterUiStateProperty(["tableSidebar", "isPrimaryKeyExpanded"], ex));
	};
	
	const handleDeletePrimaryKey = () => {
		if (!primaryKeyId) {
			return;
		}
		
		dispatch(deletePrimaryKey(primaryKeyId));
	};
	
	const addPrimaryKey = () => {
		createPrimaryKey(tableId);
	};
	
	const renderDelete = () => {
		if (isMixinClone) {
			return null;
		}
		
		return (
			<DeleteConfirm itemName="primary key" yes={handleDeletePrimaryKey} />
		);
	};
	
	const renderAccordionBody = () => {
		if (!primaryKeyId) {
			return (
				<div>
					<TextButtonWithIcon appearance="subtle" color="green" onClick={addPrimaryKey}>
						<span className="fa-solid fa-plus"></span>
						Add Primary Key
					</TextButtonWithIcon>
				</div>
			);
		}
		
		return (
			<FormGroupWrapper>
				<PrimaryKeyName primaryKeyId={primaryKeyId} disallowEdit={true} />
				<PrimaryKeyColumns primaryKeyId={primaryKeyId} disallowEdit={isMixinClone} />
				<PrimaryKeyOnConflict primaryKeyId={primaryKeyId} disallowEdit={isMixinClone} />
				<AlignFlexItems align="right">
					<MixinMessage
						visible={isMixinClone}
						schemaType={ModelObjectType.primaryKey}
						mixinParentId={belongsToMixinSchemaItem}
					/>
					{renderDelete()}
				</AlignFlexItems>
			</FormGroupWrapper>
		);
	};
	
	return (
		<Accordion
			heading="Primary Key"
			expanded={isPrimaryKeyExpanded}
			expandCallback={handleExpanded}
		>
			{renderAccordionBody()}
		</Accordion>
	);
}
