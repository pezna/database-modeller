import { alterUiStateProperty } from "@/actions";
import { ComponentForm } from "@/components/property-sidebar/ComponentForm";
import { ConnectorForm } from "@/components/property-sidebar/ConnectorForm";
import { MachineForm } from "@/components/property-sidebar/MachineForm";
import { Sidebar, SidebarSectionItem } from "@/components/widgets";
import { AppMode, PropertySidebarSection } from "@/data";
import { getAppMode, getPropertySidebarUi, getSelectedColumnId, getSelectedComponentId, getSelectedConnectorId, getSelectedMachineId, getSelectedNamespaceId, getSelectedRelationshipId, getSelectedTableId } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import { useCallback, useMemo } from "react";
import { ColumnSchemaForm } from "./ColumnSchemaForm";
import { MachineListSidebar } from "./MachineListSidebar";
import { NamespaceListSidebar } from "./NamespaceListSidebar";
import { NamespaceSchemaForm } from "./NamespaceSchemaForm";
import { RelationshipSchemaForm } from "./RelationshipSchemaForm";
import { TableSchemaForm } from "./TableSchemaForm";

export function PropertySidebar() {
	const dispatch = useDispatch();
	const appMode = useSelector(getAppMode);
	const {visibleSection, sidebarWidth} = useSelector(getPropertySidebarUi);
	const selectedTableId = useSelector(getSelectedTableId);
	const selectedColumnId = useSelector(getSelectedColumnId);
	const selectedRelationshipId = useSelector(getSelectedRelationshipId);
	const selectedNamespaceId = useSelector(getSelectedNamespaceId);
	const selectedMachineId = useSelector(getSelectedMachineId);
	const selectedComponentId = useSelector(getSelectedComponentId);
	const selectedConnectorId = useSelector(getSelectedConnectorId);
	
	const renderPropertyForm = useCallback(() => {
		if (appMode === AppMode.machine) {
			if (selectedComponentId) {
				return <ComponentForm key={selectedComponentId} componentId={selectedComponentId} />;
			}
			else if (selectedConnectorId) {
				return <ConnectorForm key={selectedConnectorId} connectorId={selectedConnectorId} />;
			}
			else if (selectedMachineId) {
				return <MachineForm key={selectedMachineId} machineId={selectedMachineId} />;
			}
			
			return null;
		}
		
		if (selectedTableId) {
			return <TableSchemaForm key={selectedTableId} tableId={selectedTableId} />;
		}
		else if (selectedColumnId) {
			return <ColumnSchemaForm key={selectedColumnId} columnId={selectedColumnId} />;
		}
		else if (selectedRelationshipId) {
			return <RelationshipSchemaForm key={selectedRelationshipId} relationshipId={selectedRelationshipId} />;
		}
		else if (selectedNamespaceId) {
			return <NamespaceSchemaForm key={selectedNamespaceId} namespaceId={selectedNamespaceId} />;
		}
		
		return null;
	}, [
		appMode,
		selectedTableId,
		selectedColumnId,
		selectedRelationshipId,
		selectedNamespaceId,
		selectedMachineId,
		selectedComponentId,
		selectedConnectorId,
	]);
	
	const renderMainSection = useCallback(() => {
		if (appMode === AppMode.machine) {
			return <MachineListSidebar />;
		}
		
		return <NamespaceListSidebar />;
	}, [appMode]);
	
	const sections = useMemo<Record<string, SidebarSectionItem>>(() => ({
		[PropertySidebarSection.main]: {
			icon: "fa fa-book",
			buttonTitle: "Main",
			component: renderMainSection,
		},
		[PropertySidebarSection.details]: {
			icon: "fa fa-list",
			buttonTitle: "Details",
			component: renderPropertyForm,
			isHidden: () => !(selectedNamespaceId || selectedMachineId),
		},
	}), [renderPropertyForm]);
	
	const handleSetVisibleSection = useCallback((section: string | null) => {
		dispatch(alterUiStateProperty(['generalAppUi', 'propertySidebar', 'visibleSection'], section as any));
	}, []);
	
	const handleSetSidebarWidth = useCallback((width: number) => {
		dispatch(alterUiStateProperty(['generalAppUi', 'propertySidebar', 'sidebarWidth'], width));
	}, []);
	
	return (
		<Sidebar
			key={appMode}
			className="property-sidebar"
			placement="right"
			visibleSection={visibleSection}
			setVisibleSection={handleSetVisibleSection}
			sections={sections}
			sidebarWidth={sidebarWidth}
			setSidebarWidth={handleSetSidebarWidth}
		/>
	);
}
