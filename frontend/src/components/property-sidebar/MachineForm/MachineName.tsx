import { alterMachineProperty } from "@/actions";
import { getMachine } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import { Form } from "rsuite";

interface MachineNameProps {
	machineId: string;
}

const propertyName = "name";

export function MachineName({ machineId }: MachineNameProps) {
	const dispatch = useDispatch();
	const value = useSelector(state => getMachine(state, machineId).name);
	
	const handleInputChange = (newValue: string) => {
		dispatch(alterMachineProperty(machineId, propertyName, newValue));
	};
	
	return (
		<Form.Group controlId="machine-name">
			<Form.ControlLabel>Name</Form.ControlLabel>
			<Form.Control
				name="name"
				type="text"
				value={value}
				onChange={handleInputChange}
			/>
		</Form.Group>
	);
}
