import { alterUiStateProperty, deleteMachine } from "@/actions";
import { MachineName } from "@/components/property-sidebar/MachineForm/MachineName";
import { AlignFlexItems, DeleteConfirm } from "@/components/widgets";
import { getMachineSidebarUi } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import { useLayoutEffect, useRef } from "react";
import { Form } from "rsuite";


interface MachineFormProps {
	machineId: string;
}

export function MachineForm({ machineId }: MachineFormProps) {
	const dispatch = useDispatch();
	const scrollAmount = useSelector(state => getMachineSidebarUi(state).scrollAmount);
	const refContainer = useRef<HTMLDivElement>(null);
	
	useLayoutEffect(() => {
		const container = refContainer.current;
		if (container && container.scrollTop !== scrollAmount) {
			container.scrollTop = scrollAmount;
		}
	}, [scrollAmount]);
	
	const confirmDeleteMachine = () => {
		dispatch(deleteMachine(machineId));
	};
	
	const handleScroll = (e: React.UIEvent<HTMLDivElement>) => {
		if (e.currentTarget.scrollTop !== scrollAmount) {
			dispatch(alterUiStateProperty(["machineSidebar", "scrollAmount"], e.currentTarget.scrollTop));
		}
	};
	
	return (
		<div className="schema-form-container" ref={refContainer} onScroll={handleScroll}>
			<div className="schema-form-header">
				<h4>Machine Properties</h4>
			</div>
			<div className="schema-form-contents">
				<Form fluid>
					<MachineName machineId={machineId} />
				</Form>
			</div>
			<div className="schema-form-footer">
				<AlignFlexItems align="right">
					<DeleteConfirm itemName="machine" yes={confirmDeleteMachine} placement="topEnd" />
				</AlignFlexItems>
			</div>
		</div>
	);
}
