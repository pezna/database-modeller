import { alterNamespaceProperty, deleteSchemaError } from "@/actions";
import { ErrorMessage } from "@/components/widgets";
import { ErrorKeyPrefix, ModelObjectType, NamespaceData } from "@/data";
import { getModelError } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import { Form } from "rsuite";


export interface NamespaceNameProps {
	namespaceId: string;
	value: NamespaceData["name"];
}

const propertyName = "name";

export function NamespaceName({ namespaceId, value }: NamespaceNameProps) {
	const dispatch = useDispatch();
	const errorKey = ErrorKeyPrefix.namespaceName + namespaceId;
	const errorMessage = useSelector(state => getModelError(state, errorKey));
	
	const onInputChange = (newValue: string) => {
		if (errorMessage) {
			dispatch(deleteSchemaError(ModelObjectType.namespace, errorKey));
		}
		
		dispatch(alterNamespaceProperty(namespaceId, propertyName, newValue, errorKey));
	}
	
	return (
		<Form.Group controlId="edit-ns-name">
			<Form.ControlLabel>Name</Form.ControlLabel>
			<Form.Control
				name="name"
				type="text"
				value={value}
				onChange={onInputChange}
			/>
			<ErrorMessage errors={errorMessage} />
		</Form.Group>
	);
}
