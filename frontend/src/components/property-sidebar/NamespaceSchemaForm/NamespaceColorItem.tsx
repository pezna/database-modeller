import { alterNamespaceProperty, deleteSchemaError } from "@/actions";
import { ErrorMessage } from "@/components/widgets";
import { ColorData, ErrorKeyPrefix, ModelObjectType } from "@/data";
import { getModelError, getNamespace } from "@/selectors";
import { Dispatch, useDispatch, useSelector } from "@/store";
import styled from "@emotion/styled";
import { Form, IconButton } from "rsuite";
import { throttle } from "throttle-debounce";

export interface NamespaceColorItemProps {
	namespaceId: string;
	label: string;
	propertyName: keyof ColorData;
}

const dispatchChangeProperty = throttle(100, (dispatch: Dispatch, namespaceId: string, propertyName: NamespaceColorItemProps["propertyName"], newValue: string | undefined, errorKey: string) => {
	dispatch(alterNamespaceProperty(namespaceId, propertyName, newValue, errorKey));
});

export function NamespaceColorItem({ namespaceId, label, propertyName }: NamespaceColorItemProps) {
	const dispatch = useDispatch();
	const value = useSelector(state => getNamespace(state, namespaceId)[propertyName]);
	const errorKey = `${ErrorKeyPrefix.namespaceColor}${propertyName}${namespaceId}`;
	const errorMessage = useSelector(state => getModelError(state, errorKey));
	
	const changeInput = (newValue?: string) => {
		if (errorMessage) {
			dispatch(deleteSchemaError(ModelObjectType.namespace, errorKey));
		}
		
		dispatchChangeProperty(dispatch, namespaceId, propertyName, newValue, errorKey);
	}
	
	const renderInput = () => {
		if (value) {
			return (
				<InputWrapper>
					<Form.Control
						name={propertyName}
						type="color"
						value={value}
						onChange={changeInput}
					/>
					<IconButton
						icon={<span className="fa-solid fa-xmark"></span>}
						onClick={() => changeInput(undefined)}
						color="red"
						appearance="subtle"
					/>
				</InputWrapper>
			);
		}
		
		return (
			<IconButton
				icon={<span className="fa-solid fa-plus"></span>}
				onClick={() => changeInput("#000")}
				appearance="subtle"
			/>
		);
	}
	
	return (
		<Form.Group controlId={`edit-ns-${propertyName}-color`}>
			<Form.ControlLabel>{label}</Form.ControlLabel>
			{renderInput()}
			<ErrorMessage errors={errorMessage} />
		</Form.Group>
	);
}

const InputWrapper = styled.div`
	display: flex;
	gap: 1em;
	
	.rs-form-control-wrapper {
		flex-grow: 1;
	}
`;