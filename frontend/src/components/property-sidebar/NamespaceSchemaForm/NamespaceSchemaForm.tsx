import { alterUiStateProperty, deleteNamespace } from "@/actions";
import { AlignFlexItems, DeleteConfirm, PropertyForm } from "@/components/widgets";
import { getNamespace, getNamespaceSidebarUi } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import { useLayoutEffect, useRef } from "react";
import { Form } from "rsuite";
import { NamespaceColorItem } from "./NamespaceColorItem";
import { NamespaceName } from "./NamespaceName";


interface NamespaceSchemaFormProps {
	namespaceId: string;
}

export function NamespaceSchemaForm({ namespaceId }: NamespaceSchemaFormProps) {
	const dispatch = useDispatch();
	const namespace = useSelector(state => getNamespace(state, namespaceId));
	const scrollAmount = useSelector(state => getNamespaceSidebarUi(state).scrollAmount);
	const refContainer = useRef<HTMLDivElement>(null);
	
	useLayoutEffect(() => {
		const container = refContainer.current;
		if (container && container.scrollTop !== scrollAmount) {
			container.scrollTop = scrollAmount;
		}
	}, [scrollAmount]);
	
	const confirmDeleteNamespace = () => {
		dispatch(deleteNamespace(namespace.id));
	}
	
	const handleScroll = (e: React.UIEvent<HTMLDivElement>) => {
		if (e.currentTarget.scrollTop !== scrollAmount) {
			dispatch(alterUiStateProperty(["namespaceSidebar", "scrollAmount"], e.currentTarget.scrollTop));
		}
	}
	
	return (
		<PropertyForm.Container ref={refContainer} onScroll={handleScroll}>
			<PropertyForm.Header>
				<h4>Namespace Properties</h4>
			</PropertyForm.Header>
			<PropertyForm.Contents>
				<Form fluid>
					<NamespaceName namespaceId={namespaceId} value={namespace.name} />
					<NamespaceColorItem namespaceId={namespaceId} label="Table Background" propertyName="backgroundColor" />
					<NamespaceColorItem namespaceId={namespaceId} label="Table Foreground" propertyName="foregroundColor" />
					<NamespaceColorItem namespaceId={namespaceId} label="Table Border" propertyName="borderColor" />
				</Form>
			</PropertyForm.Contents>
			<PropertyForm.Footer>
				<AlignFlexItems align="right">
					<DeleteConfirm itemName="namespace" yes={confirmDeleteNamespace} placement="topEnd" />
				</AlignFlexItems>
			</PropertyForm.Footer>
		</PropertyForm.Container>
	);
}
