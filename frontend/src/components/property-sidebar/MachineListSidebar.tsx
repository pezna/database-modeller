import { alterUiStateProperty } from "@/actions";
import { ListMachinesSection } from "@/components/widgets";
import { getMachineModeUi } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import styled from "@emotion/styled";
import { useLayoutEffect, useRef } from "react";

export function MachineListSidebar() {
	const dispatch = useDispatch();
	const scrollAmount = useSelector(state => getMachineModeUi(state).machineListScrollAmount);
	const refContainer = useRef<HTMLDivElement>(null);
	
	useLayoutEffect(() => {
		const container = refContainer.current;
		if (container && container.scrollTop !== scrollAmount) {
			container.scrollTop = scrollAmount;
		}
	}, [scrollAmount]);
	
	const handleScroll = (e: React.UIEvent<HTMLDivElement>) => {
		if (e.currentTarget.scrollTop !== scrollAmount) {
			dispatch(alterUiStateProperty(["machineModeUi", "machineListScrollAmount"], e.currentTarget.scrollTop));
		}
	}
	
	return (
		<div className="schema-form-container" ref={refContainer} onScroll={handleScroll}>
			<div className="schema-form-header">
				<h4>Machines</h4>
			</div>
			<div className="schema-form-contents">
				<Wrapper>
					<ListMachinesSection />
				</Wrapper>
			</div>
		</div>
	);
}

const Wrapper = styled.div`
	margin: 0 1em;
`;