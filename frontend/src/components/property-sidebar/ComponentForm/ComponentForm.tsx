import { alterUiStateProperty, deleteComponent } from "@/actions";
import { BucketForm } from "@/components/property-sidebar/ComponentForm/BucketForm";
import { ComponentName } from "@/components/property-sidebar/ComponentForm/ComponentName";
import { LSystemForm } from "@/components/property-sidebar/ComponentForm/LSystemForm";
import { ProducerForm } from "@/components/property-sidebar/ComponentForm/ProducerForm";
import { AlignFlexItems, DeleteConfirm } from "@/components/widgets";
import { ComponentType, componentTypeDisplayName } from "@/data";
import { getComponent, getComponentSidebarUi } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import { useLayoutEffect, useRef } from "react";
import { Form } from "rsuite";

interface ComponentFormProps {
	componentId: string;
}

const typeToScrollKey: {[K in ComponentType]: `${K}ScrollAmount`} = {
	[ComponentType.producer]: "producerScrollAmount",
	[ComponentType.bucket]: "bucketScrollAmount",
	[ComponentType.destroyer]: "destroyerScrollAmount",
	[ComponentType.converter]: "converterScrollAmount",
	[ComponentType.lsystem]: "lsystemScrollAmount",
};

export function ComponentForm({ componentId }: ComponentFormProps) {
	const dispatch = useDispatch();
	const componentType = useSelector(state => getComponent(state, componentId).type);
	const typeName = componentTypeDisplayName[componentType];
	const scrollAmountKey = typeToScrollKey[componentType];
	const scrollAmount = useSelector(state => getComponentSidebarUi(state)[scrollAmountKey] as number);
	const refContainer = useRef<HTMLDivElement>(null);
	
	useLayoutEffect(() => {
		const container = refContainer.current;
		if (container && container.scrollTop !== scrollAmount) {
			container.scrollTop = scrollAmount;
		}
	}, [scrollAmount]);
	
	const confirmDelete = () => {
		dispatch(deleteComponent(componentId));
	};
	
	const handleScroll = (e: React.UIEvent<HTMLDivElement>) => {
		if (e.currentTarget.scrollTop !== scrollAmount) {
			dispatch(alterUiStateProperty(["componentSidebar", scrollAmountKey], e.currentTarget.scrollTop));
		}
	};
	
	const renderComponentForm = () => {
		switch (componentType) {
			case ComponentType.producer:
				return <ProducerForm componentId={componentId} />;
			case ComponentType.bucket:
				return <BucketForm componentId={componentId} />;
			case ComponentType.lsystem:
				return <LSystemForm componentId={componentId} />;
		}
	};
	
	return (
		<div className="schema-form-container" ref={refContainer} onScroll={handleScroll}>
			<div className="schema-form-header">
				<h4>{typeName} Properties</h4>
			</div>
			<div className="schema-form-contents">
				<Form fluid>
					<ComponentName componentId={componentId} />
					{renderComponentForm()}
				</Form>
			</div>
			<div className="schema-form-footer">
				<AlignFlexItems align="right">
					<DeleteConfirm itemName={typeName.toLowerCase()} yes={confirmDelete} placement="topEnd" />
				</AlignFlexItems>
			</div>
		</div>
	);
}
