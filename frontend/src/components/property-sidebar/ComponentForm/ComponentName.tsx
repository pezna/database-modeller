import { alterComponentProperty } from "@/actions";
import { getComponent } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import { Form } from "rsuite";

interface ComponentNameProps {
	componentId: string;
}

const propertyName = "name";

export function ComponentName({ componentId }: ComponentNameProps) {
	const dispatch = useDispatch();
	const value = useSelector(state => getComponent(state, componentId).name);
	
	const handleInputChange = (newValue: string) => {
		dispatch(alterComponentProperty(componentId, propertyName, newValue));
	};
	
	return (
		<Form.Group controlId="component-name">
			<Form.ControlLabel>Name</Form.ControlLabel>
			<Form.Control
				name="name"
				type="text"
				value={value}
				onChange={handleInputChange}
			/>
		</Form.Group>
	);
}
