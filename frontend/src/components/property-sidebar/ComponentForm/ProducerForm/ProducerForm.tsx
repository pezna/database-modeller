import { ProducerAmount } from "./ProducerAmount";
import { ProducerInterval } from "./ProducerInterval";

interface ProducerFormProps {
	componentId: string;
}

export function ProducerForm({ componentId }: ProducerFormProps) {
	return (
		<>
			<ProducerInterval componentId={componentId} />
			<ProducerAmount componentId={componentId} />
		</>
	);
}
