import { alterComponentProperty, deleteError } from "@/actions";
import { ErrorMessage } from "@/components/widgets";
import { ErrorKeyPrefix, ProducerComponentData, intervalFrequencySelectData } from "@/data";
import { getComponent, getModelError } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import { Form, InputNumber, SelectPicker, Stack } from "rsuite";

interface ProducerIntervalProps {
	componentId: string;
}

const frequencyPropertyName = "intervalFrequency"
const gapPropertyName = "intervalGap"

export function ProducerInterval({ componentId }: ProducerIntervalProps) {
	const dispatch = useDispatch();
	const frequencyValue = useSelector(state => getComponent<ProducerComponentData>(state, componentId).intervalFrequency);
	const gapValue = useSelector(state => getComponent<ProducerComponentData>(state, componentId).intervalGap);
	const errorKey = ErrorKeyPrefix.producerInterval + componentId;
	const errorMessage = useSelector(state => getModelError(state, errorKey));
	
	const changeGapValue = (newValue: string | number) => {
		newValue = Number(newValue);
		
		if (errorMessage) {
			dispatch(deleteError(errorKey, "modelErrors"));
		}
		
		dispatch(alterComponentProperty<ProducerComponentData, typeof gapPropertyName>(componentId, gapPropertyName, newValue, errorKey));
	};
	
	const changeFrequencyValue = (newValue: any) => {
		if (!newValue) {
			return;
		}
		
		if (errorMessage) {
			dispatch(deleteError(errorKey, "modelErrors"));
		}
		
		dispatch(alterComponentProperty<ProducerComponentData, typeof frequencyPropertyName>(componentId, frequencyPropertyName, newValue, errorKey));
	};
	
	return (
		<Form.Group controlId={errorKey}>
			<Form.ControlLabel>Interval</Form.ControlLabel>
			<Stack>
				<Stack.Item grow={1}>
					<Form.Control
						name={errorKey}
						accepter={InputNumber}
						value={gapValue}
						onChange={changeGapValue}
					/>
				</Stack.Item>
				<Stack.Item grow={1}>
					<SelectPicker
						value={frequencyValue}
						data={intervalFrequencySelectData}
						labelKey="label"
						valueKey="value"
						onChange={changeFrequencyValue}
						searchable={false}
						cleanable={false}
						block
						preventOverflow
					/>
				</Stack.Item>
			</Stack>
			<ErrorMessage errors={errorMessage} />
		</Form.Group>
	);
}