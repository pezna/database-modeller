import { alterComponentProperty, deleteError } from "@/actions";
import { ErrorMessage } from "@/components/widgets";
import { ErrorKeyPrefix, ProducerComponentData } from "@/data";
import { getComponent, getModelError } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import { Form, InputNumber } from "rsuite";

interface ProducerAmountProps {
	componentId: string;
}

const propertyName = "amountProduced";

export function ProducerAmount({ componentId }: ProducerAmountProps) {
	const dispatch = useDispatch();
	const value = useSelector(state => getComponent<ProducerComponentData>(state, componentId).amountProduced);
	const errorKey = ErrorKeyPrefix.producerAmount + componentId;
	const errorMessage = useSelector(state => getModelError(state, errorKey));
	
	const changeValue = (newValue: string | number) => {
		newValue = Number(newValue);
		
		if (errorMessage) {
			dispatch(deleteError(errorKey, "modelErrors"));
		}
		
		dispatch(alterComponentProperty<ProducerComponentData, typeof propertyName>(componentId, propertyName, newValue, errorKey));
	};
	
	return (
		<Form.Group controlId={errorKey}>
			<Form.ControlLabel>Amount Produced</Form.ControlLabel>
			<Form.Control
				name={errorKey}
				accepter={InputNumber}
				value={value}
				onChange={changeValue}
			/>
			<ErrorMessage errors={errorMessage} />
		</Form.Group>
	);
}
