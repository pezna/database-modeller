import { alterComponentProperty, deleteError } from "@/actions";
import { ErrorMessage } from "@/components/widgets";
import { BucketComponentData, ErrorKeyPrefix } from "@/data";
import { getComponent, getModelError } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import { Form } from "rsuite";

interface BucketMaxAmountProps {
	componentId: string;
}

const propertyName = "maxAmount";

export function BucketMaxAmount({ componentId }: BucketMaxAmountProps) {
	const dispatch = useDispatch();
	const value = useSelector(state => getComponent<BucketComponentData>(state, componentId).maxAmount);
	const errorKey = ErrorKeyPrefix.bucketMaxAmount + componentId;
	const errorMessage = useSelector(state => getModelError(state, errorKey));
	
	const changeValue = (val: string) => {
		let newValue = Number(val);
		if (!val.trim() || Number.isNaN(newValue)) {
			newValue = -1;
		}
		
		if (errorMessage) {
			dispatch(deleteError(errorKey, "modelErrors"));
		}
		
		dispatch(alterComponentProperty<BucketComponentData, typeof propertyName>(componentId, propertyName, newValue, errorKey));
	};
	
	return (
		<Form.Group controlId={errorKey}>
			<Form.ControlLabel>Max Amount</Form.ControlLabel>
			<Form.Control
				name={errorKey}
				value={value < 0 ? "" : value}
				onChange={changeValue}
				placeholder="unlimited"
			/>
			<ErrorMessage errors={errorMessage} />
		</Form.Group>
	);
}
