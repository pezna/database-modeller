import { BucketMaxAmount } from "./BucketMaxAmount";

interface BucketFormProps {
	componentId: string;
}

export function BucketForm({ componentId }: BucketFormProps) {
	return (
		<>
			<BucketMaxAmount componentId={componentId} />
		</>
	);
}
