import { LSystemComponentData } from "@/data/machine/lsystem-data";
import { lsystemManager } from "@/rxjs";
import { getComponent } from "@/selectors";
import { useSelector } from "@/store";
import { useEffect, useState } from "react";
import { Button, Form, Stack } from "rsuite";

interface LSystemTestRunProps {
	componentId: string;
}

export function LSystemTestRun({ componentId }: LSystemTestRunProps) {
	const iterations = useSelector(state => getComponent<LSystemComponentData>(state, componentId).maxIterations);
	const [currentStep, setCurrentStep] = useState(0);
	
	const reactive = lsystemManager.getLSystemReactive(componentId);
	
	useEffect(() => {
		const sub = reactive.observeCurrentStep({
			next: setCurrentStep,
		});
		
		return () => {
			sub.unsubscribe();
		};
	}, [componentId]);
	
	const stepBackward = () => {
		reactive.decrementStep();
	};
	
	const stepForward = () => {
		reactive.incrementStep();
	};
	
	return (
		<Form.Group>
			<Stack spacing={10}>
				<Stack.Item>
					<Button onClick={stepBackward}>
						<span className="fa-solid fa-backward-step"></span>
					</Button>
				</Stack.Item>
				<Stack.Item>
					{currentStep}/{iterations}
				</Stack.Item>
				<Stack.Item>
					<Button onClick={stepForward}>
						<span className="fa-solid fa-forward-step"></span>
					</Button>
				</Stack.Item>
			</Stack>
		</Form.Group>
	);
}
