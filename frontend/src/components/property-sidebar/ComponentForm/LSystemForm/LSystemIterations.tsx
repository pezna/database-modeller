import { alterComponentProperty, deleteError } from "@/actions";
import { ErrorMessage } from "@/components/widgets";
import { ErrorKeyPrefix, LSystemComponentData } from "@/data";
import { getComponent, getModelError } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import { useState } from "react";
import { Form } from "rsuite";

interface LSystemIterationsProps {
	componentId: string;
}

const propertyName = "maxIterations";

export function LSystemIterations({ componentId }: LSystemIterationsProps) {
	const dispatch = useDispatch();
	const value = useSelector(state => getComponent<LSystemComponentData>(state, componentId)[propertyName]);
	const [inputValue, setInputValue] = useState(String(value));
	const errorKey = ErrorKeyPrefix.lsystemIterations + componentId;
	const errorMessage = useSelector(state => getModelError(state, errorKey));
	
	const changeValue = (val: string) => {
		setInputValue(val);
		const newValue = Number(val);
		
		if (errorMessage) {
			dispatch(deleteError(errorKey, "modelErrors"));
		}
		
		dispatch(alterComponentProperty<LSystemComponentData, typeof propertyName>(componentId, propertyName, newValue, errorKey));
	};
	
	return (
		<Form.Group controlId={errorKey}>
			<Form.ControlLabel>Max Iterations</Form.ControlLabel>
			<Form.Control
				name={errorKey}
				value={inputValue}
				onChange={changeValue}
			/>
			<ErrorMessage errors={errorMessage} />
		</Form.Group>
	);
}
