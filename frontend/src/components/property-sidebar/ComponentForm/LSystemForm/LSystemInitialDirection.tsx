import { alterComponentProperty, deleteError } from "@/actions";
import { ErrorMessage } from "@/components/widgets";
import { AngleUnit, ErrorKeyPrefix, LSystemComponentData, angleUnitSelectData } from "@/data";
import { getComponent, getModelError } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import { useState } from "react";
import { Form, SelectPicker, Stack } from "rsuite";

interface LSystemInitialDirectionProps {
	componentId: string;
}

const propertyName = "initialDirection";
const unitPropertyName = "initialDirectionUnit";

export function LSystemInitialDirection({ componentId }: LSystemInitialDirectionProps) {
	const dispatch = useDispatch();
	const value = useSelector(state => getComponent<LSystemComponentData>(state, componentId)[propertyName]);
	const unitValue = useSelector(state => getComponent<LSystemComponentData>(state, componentId)[unitPropertyName]);
	const [inputValue, setInputValue] = useState(String(value));
	const errorKey = ErrorKeyPrefix.lsystemInitialDirection + componentId;
	const errorMessage = useSelector(state => getModelError(state, errorKey));
	
	const changeValue = (val: string) => {
		setInputValue(val);
		const newValue = Number(val);
		
		if (errorMessage) {
			dispatch(deleteError(errorKey, "modelErrors"));
		}
		
		dispatch(alterComponentProperty<LSystemComponentData, typeof propertyName>(componentId, propertyName, newValue, errorKey));
	};
	
	const changeUnitValue = (newValue: AngleUnit | null) => {
		if (!newValue) {
			return;
		}
		
		dispatch(alterComponentProperty<LSystemComponentData, typeof unitPropertyName>(componentId, unitPropertyName, newValue, errorKey));
	};
	
	return (
		<Form.Group controlId={errorKey}>
			<Form.ControlLabel>Initial Direction</Form.ControlLabel>
			<Stack>
				<Stack.Item grow={1}>
					<Form.Control
						name={errorKey}
						value={inputValue}
						onChange={changeValue}
					/>
				</Stack.Item>
				<Stack.Item grow={1}>
					<SelectPicker
						value={unitValue}
						data={angleUnitSelectData}
						labelKey="label"
						valueKey="value"
						onChange={changeUnitValue}
						searchable={false}
						cleanable={false}
						block
						preventOverflow
					/>
				</Stack.Item>
			</Stack>
			<ErrorMessage errors={errorMessage} />
		</Form.Group>
	);
}
