import { LSystemInitialDirection } from "./LSystemInitialDirection";
import { LSystemIterations } from "./LSystemIterations";
import { LSystemRotation } from "./LSystemRotation";
import { LSystemStepDistance } from "./LSystemStepDistance";
import { LSystemTestRun } from "./LSystemTestRun";

interface LSystemFormProps {
	componentId: string;
}

export function LSystemForm({ componentId }: LSystemFormProps) {
	return (
		<>
			<LSystemIterations componentId={componentId} />
			<LSystemStepDistance componentId={componentId} />
			<LSystemInitialDirection componentId={componentId} />
			<LSystemRotation componentId={componentId} />
			<LSystemTestRun componentId={componentId} />
		</>
	);
}
