import { alterComponentProperty, deleteError } from "@/actions";
import { ErrorMessage } from "@/components/widgets";
import { ErrorKeyPrefix, LSystemComponentData } from "@/data";
import { getComponent, getModelError } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import { useState } from "react";
import { Form } from "rsuite";

interface LSystemStepDistanceProps {
	componentId: string;
}

const propertyName = "stepDistance";

export function LSystemStepDistance({ componentId }: LSystemStepDistanceProps) {
	const dispatch = useDispatch();
	const value = useSelector(state => getComponent<LSystemComponentData>(state, componentId)[propertyName]);
	const [inputValue, setInputValue] = useState(String(value));
	const errorKey = ErrorKeyPrefix.lsystemStepDistance + componentId;
	const errorMessage = useSelector(state => getModelError(state, errorKey));
	
	const changeValue = (val: string) => {
		setInputValue(val);
		const newValue = Number(val);
		
		if (errorMessage) {
			dispatch(deleteError(errorKey, "modelErrors"));
		}
		
		dispatch(alterComponentProperty<LSystemComponentData, typeof propertyName>(componentId, propertyName, newValue, errorKey));
	};
	
	return (
		<Form.Group controlId={errorKey}>
			<Form.ControlLabel>Step Distance</Form.ControlLabel>
			<Form.Control
				name={errorKey}
				value={inputValue}
				onChange={changeValue}
			/>
			<ErrorMessage errors={errorMessage} />
		</Form.Group>
	);
}
