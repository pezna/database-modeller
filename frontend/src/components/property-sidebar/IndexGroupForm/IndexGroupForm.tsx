import { alterUiStateProperty } from "@/actions";
import { Accordion, TextButtonWithIcon } from "@/components/widgets";
import { useModelCreation } from "@/hooks";
import { getColumnSidebarUi, getTableSidebarUi } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import { IndexItemForm } from "./IndexItemForm";

interface IndexGroupFormProps {
	indexIds: string[];
	tableId: string;
	columnId?: string;
}

export function IndexGroupForm({ indexIds, tableId, columnId }: IndexGroupFormProps) {
	const dispatch = useDispatch();
	const uiKey = columnId ? "columnSidebar" : "tableSidebar";
	const isIndexGroupExpanded = useSelector(
		state => (columnId ? getColumnSidebarUi(state) : getTableSidebarUi(state)).isIndexGroupExpanded
	);
	const { createIndex } = useModelCreation();
	
	const handleExpanded = (ex: boolean) => {
		dispatch(alterUiStateProperty([uiKey, "isIndexGroupExpanded"], ex));
	};
	
	const addIndex = () => {
		createIndex(tableId, columnId);
	};
	
	const renderAccordionBody = () => {
		return (
			<div>
				{
					indexIds.map(id => (
						<IndexItemForm
							key={id}
							indexId={id}
							columnId={columnId}
						/>
					))
				}
				<TextButtonWithIcon appearance="subtle" color="green" onClick={addIndex}>
					<span className="fa-solid fa-plus"></span>
					Add Index
				</TextButtonWithIcon>
			</div>
		);
	};
	
	const renderAccordionHeading = () => {
		return (
			<div>
				Indexes
				<span> {indexIds.length ? `(${indexIds.length})` : ""}</span>
			</div>
		);
	};
	
	return (
		<Accordion heading={renderAccordionHeading()} expanded={isIndexGroupExpanded} expandCallback={handleExpanded}>
			{renderAccordionBody()}
		</Accordion>
	);
}
