import { alterIndexProperty } from "@/actions";
import { MultiAddSelection } from "@/components/widgets";
import { ColumnIndexDirection, ErrorKeyPrefix, columnIndexDirectionSelectData } from "@/data";
import { getIndex, getTableColumns } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import { SelectPicker } from "rsuite";

export interface IndexColumnsProps {
	indexId: string;
	disallowEdit: boolean;
}

const propertyName = "columnIds";
const directionsPropertyName = "directions";

export function IndexColumns({ indexId, disallowEdit }: IndexColumnsProps) {
	const dispatch = useDispatch();
	const tableColumns = useSelector(state => getTableColumns(state, getIndex(state, indexId).tableId));
	const value = useSelector(state => getIndex(state, indexId).columnIds);
	const directions = useSelector(state => getIndex(state, indexId).directions);
	
	const errorKey = ErrorKeyPrefix.indexColumns + indexId;
	
	const addColumn = (columnId: string | null) => {
		if (!columnId) {
			return;
		}
		
		changeColumn(value.length, columnId);
		changeDirection(directions.length, ColumnIndexDirection.ascending);
	};
	
	const changeColumn = (index: number, columnId: string | null) => {
		if (!columnId) {
			return;
		}
		
		const columns = [...value];
		columns[index] = columnId;
		
		dispatch(alterIndexProperty(indexId, propertyName, columns, errorKey));
	};
	
	const changeDirection = (index: number, direction: ColumnIndexDirection) => {
		const newDirections = [...directions];
		newDirections[index] = direction;
		dispatch(alterIndexProperty(indexId, directionsPropertyName, newDirections, errorKey));
	};
	
	const deleteColumn = (index: number) => {
		const columns = [...value];
		columns.splice(index, 1);
		const newDirections = [...directions];
		newDirections.splice(index, 1);
		
		dispatch(alterIndexProperty(indexId, propertyName, columns, errorKey));
		dispatch(alterIndexProperty(indexId, directionsPropertyName, newDirections, errorKey));
	};
	
	const renderDirection = (index: number) => {
		const directionValue = directions[index];
		
		return (
			<SelectPicker
				data={columnIndexDirectionSelectData}
				value={directionValue}
				placeholder="Direction"
				onChange={(val) => changeDirection(index, val as ColumnIndexDirection)}
				labelKey="label"
				valueKey="value"
				searchable={false}
				cleanable={false}
				placement="bottomEnd"
				plaintext={disallowEdit}
				block
				preventOverflow
			/>
		)
	};
	
	return (
		<MultiAddSelection
			label="Columns"
			valueIds={value}
			options={tableColumns}
			optionLabelKey="name"
			optionValueKey="id"
			disallowEdit={disallowEdit}
			pickerPlaceholder="Select column"
			addLabel="Add column"
			cancelAddLabel="Cancel adding column"
			errorKey={errorKey}
			flashWhenEmpty
			additionalElementPerRow={renderDirection}
			onChangeValue={changeColumn}
			onDelete={deleteColumn}
			onAddValue={addColumn}
		/>
	);
}
