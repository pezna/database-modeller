import { alterIndexProperty, deleteSchemaError } from "@/actions";
import { ErrorMessage } from "@/components/widgets";
import { ErrorKeyPrefix, ModelObjectType } from "@/data";
import { getIndex, getModelError } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import { Form } from "rsuite";


export interface IndexNameProps {
	indexId: string;
	disallowEdit: boolean;
}

const propertyName = "name";

export function IndexName({ indexId, disallowEdit }: IndexNameProps) {
	const dispatch = useDispatch();
	const value = useSelector(state => getIndex(state, indexId).name);
	const errorKey = ErrorKeyPrefix.indexName + indexId;
	const errorMessage = useSelector(state => getModelError(state, errorKey));
	
	const changeInput = (newValue: string) => {
		if (errorMessage) {
			dispatch(deleteSchemaError(ModelObjectType.index, errorKey));
		}
		
		dispatch(alterIndexProperty(indexId, propertyName, newValue, errorKey));
	}
	
	return (
		<Form.Group controlId="edit-index-name">
			<Form.ControlLabel>Name</Form.ControlLabel>
			<Form.Control
				name="name"
				type="text"
				value={value}
				onChange={changeInput}
				plaintext={disallowEdit}
			/>
			<ErrorMessage errors={errorMessage} />
		</Form.Group>
	);
}
