import { deleteIndex } from "@/actions";
import { AlignFlexItems, DeleteConfirm, FormGroupWrapper, MixinMessage } from "@/components/widgets";
import { ModelObjectType } from "@/data";
import { getIndex } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import { Divider } from "rsuite";
import { IndexColumns } from "./IndexColumns";
import { IndexName } from "./IndexName";


interface IndexItemFormProps {
	indexId: string;
	columnId?: string;
}

export function IndexItemForm({ indexId, columnId }: IndexItemFormProps) {
	const dispatch = useDispatch();
	const belongsToMixinSchemaItem = useSelector(state => getIndex(state, indexId).belongsToMixinSchemaItem);
	const isMixinClone = Boolean(belongsToMixinSchemaItem);
	
	const handleDeleteIndex = () => {
		dispatch(deleteIndex(indexId));
	};
	
	const renderDelete = () => {
		if (isMixinClone) {
			return null;
		}
		
		return (
			<DeleteConfirm itemName="index" yes={handleDeleteIndex} />
		);
	};
	
    return (
        <FormGroupWrapper>
			<IndexName indexId={indexId} disallowEdit={true} />
			<IndexColumns indexId={indexId} disallowEdit={isMixinClone} />
			<AlignFlexItems align="right">
				<MixinMessage
					visible={isMixinClone}
					schemaType={ModelObjectType.index}
					mixinParentId={belongsToMixinSchemaItem}
					mixinColumnId={columnId}
				/>
				{renderDelete()}
			</AlignFlexItems>
			<Divider />
		</FormGroupWrapper>
    );
}
