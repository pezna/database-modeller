import { alterUiStateProperty, deleteConnector } from "@/actions";
import { AlignFlexItems, DeleteConfirm } from "@/components/widgets";
import { ConnectorSidebarUiState, ConnectorType, connectorTypeDisplayName } from "@/data";
import { getConnector, getConnectorSidebarUi } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import { useRef } from "react";
import { Form } from "rsuite";
import { ChannelConnectorForm } from "./ChannelConnectorForm";
import { ConnectorName } from "./ConnectorName";

interface ConnectorFormProps {
	connectorId: string;
}

const typeToScrollKey: {[K in ConnectorType]: keyof ConnectorSidebarUiState} = {
	[ConnectorType.channel]: "channelScrollAmount",
	[ConnectorType.signal]: "signalScrollAmount",
};

export function ConnectorForm({ connectorId }: ConnectorFormProps) {
	const dispatch = useDispatch();
	const connectorType = useSelector(state => getConnector(state, connectorId).type);
	const typeName = connectorTypeDisplayName[connectorType];
	const scrollAmountKey = typeToScrollKey[connectorType];
	const scrollAmount = useSelector(state => getConnectorSidebarUi(state)[scrollAmountKey] as number);
	const refContainer = useRef<HTMLDivElement>(null);
	
	const confirmDelete = () => {
		dispatch(deleteConnector(connectorId));
	};
	
	const handleScroll = (e: React.UIEvent<HTMLDivElement>) => {
		if (e.currentTarget.scrollTop !== scrollAmount) {
			dispatch(alterUiStateProperty(["connectorSidebar", scrollAmountKey], e.currentTarget.scrollTop));
		}
	};
	
	const renderConnectorForm = () => {
		switch (connectorType) {
			case ConnectorType.channel:
				return <ChannelConnectorForm connectorId={connectorId} />;
		}
	};
	
	return (
		<div className="schema-form-container" ref={refContainer} onScroll={handleScroll}>
			<div className="schema-form-header">
				<h4>{typeName} Connector</h4>
			</div>
			<div className="schema-form-contents">
				<Form fluid>
					<ConnectorName connectorId={connectorId} />
					{renderConnectorForm()}
				</Form>
			</div>
			<div className="schema-form-footer">
				<AlignFlexItems align="right">
					<DeleteConfirm itemName={typeName.toLowerCase()} yes={confirmDelete} placement="topEnd" />
				</AlignFlexItems>
			</div>
		</div>
	);
}