import { alterConnectorProperty } from "@/actions";
import { getConnector } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import { Form } from "rsuite";

interface ConnectorNameProps {
	connectorId: string;
}

const propertyName = "name";

export function ConnectorName({ connectorId }: ConnectorNameProps) {
	const dispatch = useDispatch();
	const value = useSelector(state => getConnector(state, connectorId).name);
	
	const handleInputChange = (newValue: string) => {
		dispatch(alterConnectorProperty(connectorId, propertyName, newValue));
	};
	
	return (
		<Form.Group controlId="connector-name">
			<Form.ControlLabel>Name</Form.ControlLabel>
			<Form.Control
				name="name"
				type="text"
				value={value}
				onChange={handleInputChange}
			/>
		</Form.Group>
	);
}
