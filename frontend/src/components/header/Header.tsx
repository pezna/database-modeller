import { createColumn, createNamespace, createPrimaryKey, createRelationship, createTable, selectNamespace, setColumnType, setCurrentAppDomain } from "@/actions";
import { DisplayModelInfo } from "@/components/header/DisplayModelInfo";
import { FileMenu } from "@/components/header/FileMenu";
import { RunMenu } from "@/components/header/RunMenu";
import { NewMachineSection } from "@/components/widgets";
import { ActionData, AppMode, ColumnIndexDirection, ColumnTypeData, FunctionActionData, appModeDisplayName, createDimensionsObject, newNamespaceData } from "@/data";
import { getAppMode } from "@/selectors";
import { diagramService } from "@/services";
import { Dispatch, useDispatch, useSelector } from "@/store";
import { IdPrefix, generateId } from "@/utils";
import styled from "@emotion/styled";
import { Button, SelectPicker } from "rsuite";

export function Header() {
	const dispatch = useDispatch();
	const appMode = useSelector(getAppMode);
	
	const changeAppMode = (mode: AppMode | null) => {
		if (mode) {
			dispatch(setCurrentAppDomain(mode));
		}
	};
	
	const handleCreateTestTables = () => createTestTables(dispatch);
	
	const renderTools = () => {
		return (
			<ButtonContainer>
				{
					appMode === AppMode.database
					? <Button appearance="default" onClick={handleCreateTestTables}>Add Test Tables</Button>
					: null
				}
				{
					appMode === AppMode.machine
					? <NewMachineSection />
					: null
				}
			</ButtonContainer>
		);
	}
	
	const renderDomain = () => {
		const selectData = Object.values(AppMode).map((type) => ({
			value: type,
			label: appModeDisplayName[type],
		}));
		
		return (
			<ButtonContainer>
				<SelectPicker
					data={selectData}
					labelKey="label"
					valueKey="value"
					value={appMode}
					onChange={changeAppMode}
					searchable={false}
					cleanable={false}
					preventOverflow
				/>
			</ButtonContainer>
		);
	}
	
	return (
		<Wrapper>
			<div>
				{renderDomain()}
			</div>
			<MenuWrapper>
				<FileMenu />
				<RunMenu />
			</MenuWrapper>
			<div className="last">
				{renderTools()}
			</div>
			<DisplayModelInfo />
		</Wrapper>
	);
}

const Wrapper = styled.header`
	display: flex;
	flex-direction: row;
	gap: 1rem;
	padding: 0.7em;
	background: ${props => props.theme.header.backgroundColor};
	color: ${props => props.theme.header.foregroundColor};
	
	.last {
		flex-grow: 1;
	}
`;

const MenuWrapper = styled.div`
	display: flex;
	flex-direction: row;
	gap: 0.5rem;
`;

const ButtonContainer = styled.div`
	position: relative;
	display: inline-flex;
	flex-direction: row;
	flex-wrap: wrap;
	gap: 0.7em;
	
	> button {
		&.selected {
			background: #468B78;
			color: white;
		}
	}
`;

const createTestTables = (dispatch: Dispatch) => {
	const actions: Array<ActionData | FunctionActionData> = [];
	const namespace = newNamespaceData(null);
	actions.push(createNamespace(namespace));
	diagramService.addBoard(namespace.id);
	actions.push(selectNamespace(namespace.id));
	const rand = Math.trunc(Math.random() * 10000);
	const table1Id = generateId(IdPrefix.table) + rand;
	const table2Id = generateId(IdPrefix.table) + rand;
	const table3Id = generateId(IdPrefix.table) + rand;
	const table4Id = generateId(IdPrefix.table) + rand;
	actions.push(createTable({
			id: table4Id,
			name: 'base',
			columnIds: [],
			relationshipIds: [],
			primaryKeyId: null,
			mixinTableIds: [],
			isMixin: true,
			namespaceId: namespace.id,
		},
	));
	diagramService.sendItemDimensions(table4Id, namespace.id, createDimensionsObject(400, 100, 300, 200));
	
	actions.push(createTable({
			id: table1Id,
			name: 'test_one',
			columnIds: [],
			relationshipIds: [],
			primaryKeyId: null,
			mixinTableIds: [table4Id],
			isMixin: false,
			namespaceId: namespace.id,
		},
	));
	diagramService.sendItemDimensions(table1Id, namespace.id, createDimensionsObject(200, 100, 300, 200));
	
	actions.push(createTable({
			id: table2Id,
			name: 'test_two',
			columnIds: [],
			relationshipIds: [],
			primaryKeyId: null,
			mixinTableIds: [],
			isMixin: false,
			namespaceId: namespace.id,
		},
	));
	diagramService.sendItemDimensions(table2Id, namespace.id, createDimensionsObject(200, 250, 300, 200));
	
	actions.push(createTable({
			id: table3Id,
			name: 'test_three',
			columnIds: [],
			relationshipIds: [],
			primaryKeyId: null,
			mixinTableIds: [],
			isMixin: false,
			namespaceId: namespace.id,
		},
	));
	diagramService.sendItemDimensions(table3Id, namespace.id, createDimensionsObject(400, 250, 300, 200));
	
	const t1c1Id = generateId(IdPrefix.column) + rand;
	const t2c1Id = generateId(IdPrefix.column) + rand;
	const t2c2Id = generateId(IdPrefix.column) + rand;
	const t3c1Id = generateId(IdPrefix.column) + rand;
	const t3c2Id = generateId(IdPrefix.column) + rand;
	const t4c1Id = generateId(IdPrefix.column) + rand;
	
	const typeId = generateId(IdPrefix.columnType);
	const columnType: ColumnTypeData = {
		id: typeId,
		name: "int",
		languages:[
			{
				name: "sqlite",
				value: "INTEGER",
			}
		],
		columnIds: [],
	};
	actions.push(setColumnType(columnType));
	
	actions.push(createColumn({
		id: t1c1Id,
		tableId: table1Id,
		name: 'id',
		typeId: columnType.id,
		options: {},
	}));
	actions.push(createColumn({
		id: t2c1Id,
		tableId: table2Id,
		name: 'id',
		typeId: columnType.id,
		options: {},
	}));
	actions.push(createColumn({
		id: t2c2Id,
		tableId: table2Id,
		name: 'one_id',
		typeId: columnType.id,
		options: {},
	}));
	actions.push(createColumn({
		id: t3c1Id,
		tableId: table3Id,
		name: 'id',
		typeId: columnType.id,
		options: {},
	}));
	actions.push(createColumn({
		id: t3c2Id,
		tableId: table3Id,
		name: 'two_id',
		typeId: columnType.id,
		options: {},
	}));
	actions.push(createColumn({
		id: t4c1Id,
		tableId: table4Id,
		name: 'yolo',
		typeId: columnType.id,
		options: {},
	}));
	
	const pk1Id = generateId(IdPrefix.primaryKey);
	const pk2Id = generateId(IdPrefix.primaryKey);
	const pk3Id = generateId(IdPrefix.primaryKey);
	
	actions.push(createPrimaryKey({
		id: pk1Id,
		name: 'PK_One',
		tableId: table1Id,
		columnIds: [t1c1Id],
		directions: [ColumnIndexDirection.ascending],
	}));
	actions.push(createPrimaryKey({
		id: pk2Id,
		name: 'PK_Two',
		tableId: table2Id,
		columnIds: [t2c1Id],
		directions: [ColumnIndexDirection.ascending],
	}));
	actions.push(createPrimaryKey({
		id: pk3Id,
		name: 'PK_Three',
		tableId: table3Id,
		columnIds: [t3c1Id],
		directions: [ColumnIndexDirection.ascending],
	}));
	
	actions.push(createRelationship({
		id: generateId(IdPrefix.relationship),
		name: 'FK_One',
		childTableId: table2Id,
		childColumnIds: [t2c2Id],
		parentTableId: table1Id,
		parentColumnIds: [t1c1Id]
	}));
	actions.push(createRelationship({
		id: generateId(IdPrefix.relationship),
		name: 'FK_Two',
		childTableId: table3Id,
		childColumnIds: [t3c2Id],
		parentTableId: table2Id,
		parentColumnIds: [t2c1Id]
	}));
	
	for (const action of actions) {
		if (!handleResponse(dispatch(action))) {
			break;
		}
	}
};

const handleResponse = (res: ActionData) => {
	if (res.errors) {
		alert(res.errors.map(e => e.message).reduce((a, b) => `${a}\n${b}`));
		return false;
	}
	
	return true;
};
