import { loadModelState, saveModelState } from "@/actions";
import { FlexRowWithAction } from "@/components/widgets";
import { useModalContext } from "@/contexts";
import { useDispatch } from "@/store";
import { useKeyPressEvent } from "react-use";
import { Dropdown } from "rsuite";

export function FileMenu() {
	const dispatch = useDispatch();
	const { setShowNewProject } = useModalContext();
	const handleLoadProject = () => dispatch(loadModelState(null));
	const handleSaveProject = () => dispatch(saveModelState(false));
	const handleSaveProjectNewFile = () => dispatch(saveModelState(true));
	const handleNewProject = () => setShowNewProject(true);
	
	useKeyPressEvent((event) => (event.ctrlKey || event.metaKey) && event.key === "n", handleNewProject);
	useKeyPressEvent((event) => (event.ctrlKey || event.metaKey) && event.key === "s", handleSaveProject);
	useKeyPressEvent((event) => (event.ctrlKey || event.metaKey) && event.key === "o", handleLoadProject);
	
	return (
		<Dropdown title="File">
			<Dropdown.Item onSelect={handleNewProject}>
				<FlexRowWithAction>
					<div>New Project</div>
					<span className="small-text">(Ctrl + N)</span>
				</FlexRowWithAction>
			</Dropdown.Item>
			<Dropdown.Item onSelect={handleLoadProject}>
				<FlexRowWithAction>
					<div>Open Project</div>
					<span className="small-text">(Ctrl + O)</span>
				</FlexRowWithAction>
			</Dropdown.Item>
			<Dropdown.Item onSelect={handleSaveProject}>
				<FlexRowWithAction>
					<div>Save Project</div>
					<span className="small-text">(Ctrl + S)</span>
				</FlexRowWithAction>
			</Dropdown.Item>
			<Dropdown.Item onSelect={handleSaveProjectNewFile}>
				Save New Project File
			</Dropdown.Item>
		</Dropdown>
	);
}
