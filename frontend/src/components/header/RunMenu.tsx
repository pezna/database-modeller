import { useModalContext } from "@/contexts";
import { AppMode } from "@/data";
import { getAppMode } from "@/selectors";
import { useSelector } from "@/store";
import { Dropdown } from "rsuite";

export function RunMenu() {
	const appMode = useSelector(getAppMode);
	const { setShowSqlExport } = useModalContext();
	
	return (
		<Dropdown title="Run">
			{
				appMode === AppMode.database
				? (
					<Dropdown.Item onSelect={() => setShowSqlExport(true)}>
						Export SQL
					</Dropdown.Item>
				)
				: null
			}
			<Dropdown.Item>
				Code Output
			</Dropdown.Item>
		</Dropdown>
	);
}
