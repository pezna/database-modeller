import { getIsUnsaved, getModelFilePath } from "@/selectors";
import { useSelector } from "@/store";
import { ParsedPath } from "@/utils";
import { useMemo } from "react";
import { Tooltip, Whisper } from "rsuite";


export function DisplayModelInfo() {
	const filePath = useSelector(getModelFilePath);
	const isUnsaved = useSelector(getIsUnsaved);
	const parsedPath = useMemo(() => new ParsedPath(filePath ?? ""), [filePath]);
	const env = process.env.NODE_ENV;
	
	return (
		<div>
			<Whisper placement="bottomEnd" speaker={<Tooltip>{filePath}</Tooltip>}>
				<span>
					{parsedPath.name}
					{isUnsaved && ' *'}
				</span>
			</Whisper>
			{
				env !== "production"
				? (<span>&nbsp;&mdash;&nbsp;{env}</span>)
				: null
			}
		</div>
	);
}
