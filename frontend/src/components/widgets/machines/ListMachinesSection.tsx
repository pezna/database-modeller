import { deleteMachine, selectMachine } from "@/actions";
import { DeleteConfirm } from "@/components/widgets";
import { getMachinesAsArray } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import styled from "@emotion/styled";
import { List, Stack } from "rsuite";

export function ListMachinesSection() {
	const machines = useSelector(getMachinesAsArray);
	const dispatch = useDispatch();
	
	const handleSelectMachine = (id: string) => {
		dispatch(selectMachine(id));
	};
	
	const handleDelete = (id: string) => dispatch(deleteMachine(id));
	
	if (!machines.length) {
		return null;
	}
	
	return (
		<List bordered hover>
			{
				machines.map(m => (
					<List.Item key={m.id}>
						<Stack spacing={10}>
							<Stack.Item grow={1}>
								<MachineNameItem onClick={() => handleSelectMachine(m.id)}>
									{m.name}
								</MachineNameItem>
							</Stack.Item>
							<Stack.Item shrink={0}>
								<DeleteConfirm itemName="machine" yes={() => handleDelete(m.id)} />
							</Stack.Item>
						</Stack>
					</List.Item>
				))
			}
		</List>
	)
}

const MachineNameItem = styled.div`
	cursor: pointer;
	
	&:hover {
		color: var(--rs-primary-500);
	}
`;
