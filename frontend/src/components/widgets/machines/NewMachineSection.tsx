import { useModelCreation } from "@/hooks";
import { Button } from "rsuite";

export function NewMachineSection() {
	const { createMachine } = useModelCreation();
	
	const handleCreateMachine = () => {
		createMachine();
	};
	
	return (
		<Button
			appearance="primary"
			color="green"
			startIcon={<span className="fa fa-circle-nodes"></span>}
			onClick={handleCreateMachine}
		>
			New Machine
		</Button>
	)
}
