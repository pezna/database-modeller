import styled from "@emotion/styled";
import { PropsWithChildren, ReactNode, useEffect, useState } from "react";

interface AccordionProps {
	heading: ReactNode;
	expanded?: boolean;
	expandCallback?: (val: boolean) => void;
	useAlternateBodyBackground?: boolean;
}

export function Accordion({ heading, children, expanded, expandCallback, useAlternateBodyBackground }: PropsWithChildren<AccordionProps>) {
	const [isExpanded, setIsExpanded] = useState(Boolean(expanded));
	useAlternateBodyBackground = Boolean(useAlternateBodyBackground);
	
	useEffect(() => {
		setIsExpanded(Boolean(expanded));
	}, [expanded]);
	
	const toggleExpand = () => {
		const val = !isExpanded;
		setIsExpanded(val);
		
		if (expandCallback) {
			expandCallback(val);
		}
	};
	
	return (
		<Wrapper className={isExpanded ? "expanded" : ""} useAlternateBodyBackground={useAlternateBodyBackground}>
			<Heading onClick={toggleExpand}>
				<Arrow className="fa fa-angle-right"></Arrow>
				<Title>{heading}</Title>
			</Heading>
			<Body>
				{isExpanded ? children : null}
			</Body>
		</Wrapper>
	);
}

const Arrow = styled.span`
	font-size: 1.1em;
	transition-property: transform;
	transition-duration: 0.25s;
	margin: 10px;
	flex-shrink: 0;
`;

const Title = styled.div`
	overflow-wrap: break-word;
	min-width: 0;
	flex-grow: 1;
	font-size: 1.2em;
`;

const Heading = styled.div`
	display: flex;
	flex-direction: row;
	align-items: center;
	cursor: default;
	
	&:hover {
		cursor: pointer;
		color: #ccd;
	}
	
	h4, h5, h6 {
		display: inline;
	}
`;

const Body = styled.div`
	margin: 0 10px;
	padding: 10px;
	display: none;
`;

interface WrapperProps {
	useAlternateBodyBackground: boolean;
}

const Wrapper = styled.div<WrapperProps>`
	&.expanded {
		> ${Body} {
			display: block;
			background: ${
				props => props.useAlternateBodyBackground
				? props.theme.accordion.bodyBackgroundColor
				: props.theme.accordion.alternateBodyBackgroundColor
			};
		}
		
		> ${Heading} ${Arrow} {
			transform: rotateZ(90deg);
		}
	}
`;
