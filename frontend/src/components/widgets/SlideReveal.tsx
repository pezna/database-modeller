import styled from "@emotion/styled";
import { ReactNode } from "react";

interface SlideRevealProps {
	hiddenComponent: () => ReactNode;
	alwaysVisibleComponent: () => ReactNode;
	showHidden: boolean;
}

export function SlideReveal({ showHidden, hiddenComponent, alwaysVisibleComponent }: SlideRevealProps) {
	return (
		<Wrapper revealed={showHidden}>
			<div>
				{alwaysVisibleComponent()}
			</div>
			<HiddemItem>
				<div className="expander-content">
					{hiddenComponent()}
				</div>
			</HiddemItem>
		</Wrapper>
	);
}

const HiddemItem = styled.div`
	min-height: 0;
`;

interface WrapperProps {
	revealed: boolean;
}

const Wrapper = styled.div<WrapperProps>`
	display: grid;
	grid-template-rows: min-content ${props => props.revealed ? "1fr" : "0fr"};
	overflow: hidden;
	transition: grid-template-rows 0.25s;
	
	${HiddemItem} {
		transition: visibility 0.25s;
		visibility: ${props => props.revealed ? "visible" : "hidden"};
		margin-top: ${props => props.revealed ? "0.5rem" : "0"};
	}
`;
