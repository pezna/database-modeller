import styled from "@emotion/styled";
import React, { useLayoutEffect, useRef } from "react";

export const enum SplitLayout {
	horizontal = "horizontal",
	vertical = "vertical",
}

interface SplitPanelProps {
	first?: React.ReactNode;
	second?: React.ReactNode;
	layout: SplitLayout;
	dividerPosition?: number;
	onDividerMoved?: (pos: number) => void;
}

export function SplitPanel({ first, second, layout, dividerPosition, onDividerMoved }: SplitPanelProps) {
	const movingInfoRef = useRef({
		clientX: 0,
		clientY: 0,
		isMovingDivider: false,
		initialFirstSize: 0,
	});
	const firstRef = useRef<HTMLDivElement>(null);
	const splitPaneRef = useRef<HTMLDivElement>(null);
	const dividerRef = useRef<HTMLDivElement>(null);
	
	useLayoutEffect(() => {
		if (dividerPosition === undefined) {
			return;
		}
		
		if (layout === SplitLayout.horizontal) {
			firstRef.current!.style.width = `${dividerPosition}px`;
		}
		else {
			firstRef.current!.style.height = `${dividerPosition}px`;
		}
		
	}, [dividerPosition, layout]);
	
	const mouseDown = (e: React.MouseEvent<HTMLDivElement>) => {
		const target = e.target as Element;
		if (!target.classList.contains("divider")) {
			return;
		}
		
		movingInfoRef.current.isMovingDivider = true;
		movingInfoRef.current.initialFirstSize = (layout === SplitLayout.horizontal) ? firstRef.current!.clientWidth : firstRef.current!.clientHeight;
		
		if (movingInfoRef.current.isMovingDivider) {
			movingInfoRef.current.clientX = e.clientX;
			movingInfoRef.current.clientY = e.clientY;
			splitPaneRef.current!.style.userSelect = "none";
			e.stopPropagation();
		}
	}
	
	const mouseMove = (e: React.MouseEvent<HTMLDivElement>) => {
		if (movingInfoRef.current.isMovingDivider) {
			if (layout === SplitLayout.vertical) {
				const delta = e.clientY - movingInfoRef.current.clientY;
				const size = movingInfoRef.current.initialFirstSize + delta;
				firstRef.current!.style.height = `${size}px`;
			}
			else {
				const delta = e.clientX - movingInfoRef.current.clientX;
				const size = movingInfoRef.current.initialFirstSize + delta;
				firstRef.current!.style.width = `${size}px`;
			}
		}
		
		if (movingInfoRef.current.isMovingDivider) {
			e.stopPropagation();
		}
	}
	
	const mouseUp = (e: React.MouseEvent<HTMLDivElement>) => {
		if (movingInfoRef.current.isMovingDivider) {
			movingInfoRef.current.isMovingDivider = false;
			splitPaneRef.current!.style.userSelect = "";
			
			if (layout === SplitLayout.vertical) {
				const match = /^\d+/g.exec(firstRef.current!.style.height);
				if (match) {
					onDividerMoved?.(Number(match[0]));
				}
			}
			else {
				const match = /^\d+/g.exec(firstRef.current!.style.width);
				if (match) {
					onDividerMoved?.(Number(match[0]));
				}
			}
		}
	}
	
	return (
		<Wrapper
			ref={splitPaneRef}
			className={`split-panel ${layout}`}
			onMouseDown={mouseDown}
			onMouseMove={mouseMove}
			onMouseUp={mouseUp}
		>
			<div ref={firstRef} className="split-content">{first}</div>
			<div ref={dividerRef} className="divider"></div>
			<div className="split-content">{second}</div>
		</Wrapper>
	);
}

const Wrapper = styled.div`
	position: relative;
	display: flex;
	height: 100%;
	
	.divider {
		flex-shrink: 0;
	}
	
	&.horizontal {
		flex-direction: row;
		
		.divider {
			background: #ccc;
			width: 5px;
			cursor: ew-resize;
		}
	}
	
	&.vertical {
		flex-direction: column;
		
		.divider {
			background: #ccc;
			height: 5px;
			cursor: ns-resize;
		}
	}
	
	.split-content {
		min-height: 100%;
		
		&:last-child {
			flex-grow: 1;
		}
		
		&:first-child {
			flex-shrink: 0;
		}
	}
`;