import { getMixinTables, getNamespaces, getNonMixinTables, getSelectedNamespaceId, getSortedTables } from "@/selectors";
import { useSelector } from "@/store";
import { nameSort } from "@/utils";
import styled from "@emotion/styled";
import { ReactNode, useMemo } from "react";
import { Cascader, MultiCascader } from "rsuite";
import { ItemDataType } from "rsuite/esm/MultiCascadeTree";

interface TableCascadeInputProps {
	tableType: "all" | "mixins" | "nonMixins";
	value: any;
	onChange: (value: any) => void;
	disallowEdit?: boolean;
	multiPicker?: boolean;
}

export function TableCascadeInput({ tableType, value, onChange, disallowEdit, multiPicker }: TableCascadeInputProps) {
	if (multiPicker && !Array.isArray(value)) {
		throw new Error(`TableCascadeInput value must be an array when multiPicker is true: ${value}`);
	}
	
	const tables = useSelector(state => {
		switch (tableType) {
			case "all":
				return getSortedTables(state);
			case "mixins":
				return getMixinTables(state);
			case "nonMixins":
				return getNonMixinTables(state);
		}
	});
	const namespaces = useSelector(getNamespaces);
	const selectedNamespaceId = useSelector(getSelectedNamespaceId);
	const [options, tableIdToNamespaceId] = useMemo(() => {
		const tableToNamespace: Record<string, string> = {};
		const data: Record<string, ItemDataType<string> & {name: string}> = {};
		for (const tbl of tables) {
			tableToNamespace[tbl.id] = tbl.namespaceId;
			let nsDataType = data[tbl.namespaceId];
			if (!nsDataType) {
				nsDataType = {
					...namespaces[tbl.namespaceId],
					children: [],
				};
				data[tbl.namespaceId] = nsDataType;
			}
			
			nsDataType.children?.push(tbl);
		}
		
		return [
			Object.values(data).sort(nameSort),
			tableToNamespace,
		];
	}, [tables, namespaces]);
	const [namespaceIds, setNamespaceIds] = useMemo(() => {
		const ids = options.map(x => x["id"]);
		const setIds = new Set(ids);
		return [ids, setIds];
	}, [options]);
	const highlightNamespaceIds = useMemo(() => {
		if (Array.isArray(value)) {
			return new Set(value.map(id => tableIdToNamespaceId[id]).filter(x => x));
		}
		return new Set([tableIdToNamespaceId[value]]);
	}, [value]);
	const showNamespaceColumn = options.length > 1 || selectedNamespaceId !== namespaceIds[0];
	
	const renderColumn = (layer: number, childNodes: ReactNode) => {
		if (!options.length) {
			return childNodes;
		}
		
		return (
			<div>
				<ColumnTitle>
					{showNamespaceColumn && layer === 0 ? "Namespace" : "Table"}
				</ColumnTitle>
				{childNodes}
			</div>
		);
	};
	
	const getNamespaceColor = (id: string) => {
		return highlightNamespaceIds.has(id) ? "var(--rs-blue-300)" : "inherit"
	};
	
	if (multiPicker) {
		return (
			<MultiCascader
				data={showNamespaceColumn ? options : options[0].children!}
				labelKey="name"
				valueKey="id"
				value={value}
				onChange={onChange as any}
				cleanable={false}
				placement="bottomEnd"
				placeholder="Select mixin"
				columnWidth={200}
				renderColumn={(childNodes, { layer }) => renderColumn(layer!, childNodes)}
				renderTreeNode={(node, item) => (
					<div style={{color: getNamespaceColor(item.id)}}>{node}</div>
				)}
				uncheckableItemValues={namespaceIds}
				plaintext={disallowEdit}
				cascade
				block
				preventOverflow
			/>
		);
	}
	
	return (
		<Cascader
			data={showNamespaceColumn ? options : options[0].children!}
			labelKey="name"
			valueKey="id"
			value={value}
			onChange={onChange as any}
			cleanable={false}
			placement="bottomEnd"
			placeholder="Select mixin"
			columnWidth={200}
			renderColumn={(childNodes, { layer }) => renderColumn(layer!, childNodes)}
			renderValue={(_, activePaths) => activePaths.at(-1)?.["name"]}
			renderTreeNode={(node, item) => (
				<div style={{color: getNamespaceColor(item.id)}}>{node}</div>
			)}
			plaintext={disallowEdit}
			block
			preventOverflow
		/>
	);
}

const ColumnTitle = styled.h6`
	padding: 4px 10px;
	color: var(--rs-blue-100);
	border-bottom: 1px solid var(--rs-blue-100);
`;
