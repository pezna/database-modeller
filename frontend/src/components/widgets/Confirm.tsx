import styled from "@emotion/styled";
import { Button } from "rsuite";

interface ConfirmProps {
	message: string;
	yes: () => void;
	no?: () => void;
}

export function Confirm({ message, yes, no }: ConfirmProps) {
	return (
		<Wrapper>
			{message}
			<div className="horizontal-buttons">
				<Button color="green" appearance="primary" onClick={yes}>Yes</Button>
				{
					no
					? <Button appearance="ghost" onClick={no}>No</Button>
					: null
				}
			</div>
		</Wrapper>
	);
}

const Wrapper = styled.div`
	padding: 10px;
	font-size: 1.1em;
`;
