import { SettingDetail } from "@/data";
import { Message } from "rsuite";

interface SettingDescriptionProps {
	settingDetail: SettingDetail<any, any>;
}

export function SettingDescription({ settingDetail }: SettingDescriptionProps) {
	return (
		<div>
			{
				settingDetail.description
				? <p dangerouslySetInnerHTML={{__html: settingDetail.description}}></p>
				: null
			}
			{
				settingDetail.disabledHint
				? (
					<Message
						showIcon
						type="warning"
					>
						{settingDetail.disabledHint}
					</Message>
				)
				: null
			}
		</div>
	);
}
