import { useSettingsContext } from "@/contexts/SettingsContext";
import styled from "@emotion/styled";
import { SettingDetailRowProps } from "../types";
import { SettingDescription } from "./SettingDescription";
import { getSettingDetailRow } from "./setting-detail-child";


export function SettingDetailGroup({ keyPath, settingDetail, isParentEnabled, appMode }: SettingDetailRowProps) {
	const context = useSettingsContext();
	const isEnabled = (isParentEnabled ?? true) && (settingDetail.isEnabled?.(context.state) ?? true);
	
	if (!(settingDetail.isVisible?.(context.state) ?? true)) {
		return null;
	}
	
	const renderChildren = () => {
		if (!settingDetail.children) {
			return null;
		}
		
		return Object.entries(settingDetail.children).map(([childKey, childDetail]) => {
			const kp = [...keyPath, childKey];
			const R = getSettingDetailRow(kp, childDetail, appMode);
			return (
				<R
					key={childKey}
					keyPath={kp}
					settingDetail={childDetail}
					isParentEnabled={isEnabled}
					appMode={appMode}
				/>
			);
		});
	};
	
	const renderTitle = () => {
		if (typeof settingDetail.title === "string") {
			return settingDetail.title;
		}
		
		return settingDetail.title(context.state);
	};
	
	// group
	return (
		<Wrapper>
			<legend>{renderTitle()}</legend>
			<SettingDescription settingDetail={settingDetail} />
			{renderChildren()}
		</Wrapper>
	);
}

const Wrapper = styled.fieldset`
	
	&:not(:first-of-type) {
		margin-top: 10px;
		margin-bottom: 10px;
	}
`;