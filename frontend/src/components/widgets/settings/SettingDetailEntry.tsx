import { FolderSelect } from "@/components/widgets/FolderSelect";
import { SettingsContextData, useSettingsContext } from "@/contexts/SettingsContext";
import { ValueInputType } from "@/data";
import { getByPath } from "@/utils";
import styled from "@emotion/styled";
import { useCallback, useState } from "react";
import { Checkbox, Input, InputNumber, Message, SelectPicker } from "rsuite";
import { debounce } from "throttle-debounce";
import { CodeEntry } from "../CodeEntry";
import { FlexRowWithAction } from "../FlexRowWithAction";
import { HelpIcon } from "../icons";
import { SettingDetailRowProps } from "../types";
import { SettingDescription } from "./SettingDescription";


const changeProperty = debounce(100, (context: SettingsContextData, keyPath: string[], value: any) => {
	context.setValue(keyPath, value);
});


export function SettingDetailEntry({ keyPath, settingDetail, isParentEnabled }: SettingDetailRowProps) {
	const context = useSettingsContext();
	const settingValue: any = getByPath(context.state, keyPath as any);
	const [showCodeTip, setShowCodeTip] = useState(false);
	const isEnabled = (isParentEnabled ?? true) && (settingDetail.isEnabled?.(context.state) ?? true);
	const inputId = keyPath.join(".");
	
	const handleChange = useCallback((newValue: any) => {
		changeProperty(context, keyPath, newValue);
	}, [context, keyPath]);
	
	if (!(settingDetail.isVisible?.(context.state) ?? true)) {
		return null;
	}
	
	const toggleShowTip = () => setShowCodeTip(!showCodeTip);
	
	const renderCodeTooltip = () => {
		if (!showCodeTip) {
			return null;
		}
		
		return (
			<Message type="info" className="mb-1">
				<SettingDescription settingDetail={settingDetail} />
			</Message>
		);
	};
	
	const renderHelpIcon = () => {
		if (!settingDetail.description && !settingDetail.disabledHint) {
			return null;
		}
		
		return (
			<HelpIcon onClick={toggleShowTip} />
		);
	};
	
	const renderInput = () => {
		if (settingDetail.inputType === ValueInputType.checkbox) {
			return (
				<Checkbox
					id={inputId}
					disabled={!isEnabled}
					checked={settingValue}
					onChange={(e, chk) => handleChange(chk)}
				/>
			);
		}
		else if (settingDetail.inputType === ValueInputType.text) {
			return (
				<Input
					id={inputId}
					disabled={!isEnabled}
					value={settingValue}
					onChange={handleChange}
				/>
			);
		}
		else if (settingDetail.inputType === ValueInputType.select) {
			const data = Object.entries(settingDetail.allowedValues!).map(([key, label]) => ({
				label,
				value: key,
			}));
			
			return (
				<SelectPicker
					id={inputId}
					data={data}
					labelKey="label"
					valueKey="value"
					disabled={!isEnabled}
					value={settingValue}
					onChange={handleChange}
					searchable={false}
					cleanable={false}
					preventOverflow
					block
				/>
			);
		}
		else if (settingDetail.inputType === ValueInputType.color) {
			return (
				<Input
					id={inputId}
					type="color"
					disabled={!isEnabled}
					value={settingValue}
					onChange={handleChange}
				/>
			);
		}
		else if (settingDetail.inputType === ValueInputType.number) {
			return (
				<InputNumber
					id={inputId}
					disabled={!isEnabled}
					value={settingValue}
					onChange={(val) => handleChange(Number(val))}
				/>
			);
		}
		else if (settingDetail.inputType === ValueInputType.javascript) {
			return (
				<CodeEntry
					labelId={inputId}
					value={settingValue}
					onChange={handleChange}
					language="javascript"
					errorKey={inputId}
				/>
			);
		}
		else if (settingDetail.inputType === ValueInputType.folder) {
			return (
				<FolderSelect
					labelId={inputId}
					value={settingValue}
					onChange={handleChange}
				/>
			);
		}
		
		const IC = context.getInputComponent(keyPath);
		
		return (
			<IC
				labelId={inputId}
				value={settingValue}
				onChange={handleChange}
			/>
		);
	};
	
	const renderTitle = () => {
		if (typeof settingDetail.title === "string") {
			return settingDetail.title;
		}
		
		return settingDetail.title(context.state);
	};
	
	const opacityStyle = isEnabled ? { opacity: "inherit" } : { opacity: "50%" };
	
	return (
		<EntryGroup>
			<FlexRowWithAction style={opacityStyle}>
				<div>
					<label htmlFor={inputId}>{renderTitle()}</label>
				</div>
				{renderHelpIcon()}
			</FlexRowWithAction>
			{renderCodeTooltip()}
			<div style={opacityStyle}>
				{renderInput()}
			</div>
		</EntryGroup>
	)
}

const EntryGroup = styled.div`
padding-bottom: 1em;
margin-bottom: 1em;
border-bottom: 1px solid #444;

&:last-child {
	padding-bottom: 0;
	margin-bottom: 0;
	border-bottom: none;
}
`