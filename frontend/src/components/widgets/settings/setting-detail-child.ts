import { AppMode } from "@/data";
import { SettingDetailRowProps } from "../types";
import { SettingDetailEntry } from "./SettingDetailEntry";
import { SettingDetailGroup } from "./SettingDetailGroup";
import { SettingDetailMasterGroup } from "./SettingDetailMasterGroup";


export function getSettingDetailRow(keyPath: SettingDetailRowProps['keyPath'], settingDetail: SettingDetailRowProps['settingDetail'], appMode: AppMode) {
	// return early if the domain is not for the current domain
	if (settingDetail.appModeEditable && !settingDetail.appModeEditable.includes(appMode)) {
		return () => null;
	}
	
	if (keyPath.length === 1) {
		// master group
		return SettingDetailMasterGroup;
	}
	else if (keyPath.length > 1 && settingDetail.children) {
		// group
		return SettingDetailGroup;
	}
	else if (!settingDetail.children) {
		// entry
		return SettingDetailEntry;
	}
	
	throw new Error("SettingDetailRow must have a key path and children or just key path");
}
