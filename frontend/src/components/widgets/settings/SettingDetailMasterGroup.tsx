import { alterUiStateProperty } from "@/actions";
import { useSettingsContext } from "@/contexts/SettingsContext";
import { getSettingsSidebarUi } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import { Accordion } from "../Accordion";
import { SettingDetailRowProps } from "../types";
import { SettingDescription } from "./SettingDescription";
import { getSettingDetailRow } from "./setting-detail-child";


export function SettingDetailMasterGroup({ keyPath, settingDetail, isParentEnabled, appMode }: SettingDetailRowProps) {
	const dispatch = useDispatch();
	const context = useSettingsContext();
	const expandKey = context.getExpandedGroupKey(keyPath[0] as any);
	const isExpanded = useSelector(state => getSettingsSidebarUi(state)[expandKey] as boolean);
	const isEnabled = (isParentEnabled ?? true) && (settingDetail.isEnabled?.(context.state) ?? true);
	
	if (!(settingDetail.isVisible?.(context.state) ?? true)) {
		return null;
	}
	
	const handleExpanded = (ex: boolean) => {
		dispatch(alterUiStateProperty(['settingsSidebar', expandKey], ex));
	};
	
	const renderChildren = () => {
		if (!settingDetail.children) {
			return null;
		}
		
		return Object.entries(settingDetail.children).map(([childKey, childDetail]) => {
			const kp = [...keyPath, childKey];
			const R = getSettingDetailRow(kp, childDetail, appMode);
			return (
				<R
					key={childKey}
					keyPath={kp}
					settingDetail={childDetail}
					isParentEnabled={isEnabled}
					appMode={appMode}
				/>
			);
		});
	};
	
	const renderHeading = () => {
		if (typeof settingDetail.title === "string") {
			return settingDetail.title;
		}
		
		return settingDetail.title(context.state);
	};
	
	return (
		<Accordion
			heading={renderHeading()}
			expandCallback={handleExpanded}
			expanded={isExpanded}
		>
			<SettingDescription settingDetail={settingDetail} />
			{renderChildren()}
		</Accordion>
	);
}
