import { getJavascriptError } from "@/selectors";
import { useSelector } from "@/store";
import { javascript } from "@codemirror/lang-javascript";
import { sql } from "@codemirror/lang-sql";
import CodeMirror, { ReactCodeMirrorProps } from "@uiw/react-codemirror";
import { useEffect, useState } from "react";
import { Message } from "rsuite";
import { debounce } from "throttle-debounce";
import { InputComponentProps } from "./types";

export type CodeMirrorSqlSchema = {
	[table: string]: string[];
}

interface CodeEntryProps extends InputComponentProps {
	codeMirrorProps?: ReactCodeMirrorProps;
	language?: "javascript" | "sql";
	sqlSchema?: CodeMirrorSqlSchema;
	errorKey?: string;
}

const throttleOnChange = debounce(1000, (save: InputComponentProps["onChange"], value: string) => {
	save(value);
});

export function CodeEntry({ value, onChange, codeMirrorProps, language, sqlSchema, errorKey }: CodeEntryProps) {
	const [editingValue, setEditingValue] = useState(value);
	const [cmProps, setCmProps] = useState<ReactCodeMirrorProps>(codeMirrorProps ?? {});
	
	useEffect(() => {
		switch (language) {
			case "sql":
				const defaults = { upperCaseKeywords: true };
				setCmProps({
					...(codeMirrorProps ?? {}), // use codeMirrorProps instead of cmProps to avoid infinite renders
					extensions: [
						sql(sqlSchema ? { schema: sqlSchema, ...defaults } : defaults),
					],
				});
				break;
			case "javascript":
				setCmProps({
					...(codeMirrorProps ?? {}),
					extensions: [
						javascript({}),
					],
				});
				break;
		}
	}, [language, sqlSchema, codeMirrorProps, setCmProps]);
	
	const handleEditorChange = (value: string) => {
		setEditingValue(value);
		throttleOnChange(onChange, value);
	};
	
	return (
		<>
			<CodeMirror
				{...cmProps}
				value={editingValue}
				onChange={handleEditorChange}
				theme="dark"
			/>
			<JavascriptError errorKey={errorKey} />
		</>
	);
}

interface JavascriptErrorProps {
	errorKey?: string;
}

function JavascriptError({ errorKey }: JavascriptErrorProps) {
	if (!errorKey) {
		return null;
	}
	
	const errorMessages = useSelector(state => getJavascriptError(state, errorKey));
	return (
		errorMessages
		? (
			errorMessages.map((error, i) => (
				<Message key={i} showIcon type="error">
					{error.message}
					{
						error.line !== 0 || error.startColumn !== 0
						? <div>Line {error.line}:{error.startColumn}</div>
						: null
					}
				</Message>
			))
		)
		: null
	)
}
