import styled from "@emotion/styled";
import { Button } from "rsuite";

export const TextButtonWithIcon = styled(Button)`
> svg {
	&:first-of-type {
		margin-right: 12px;
	}
}
`;
