import styled from "@emotion/styled";

export const FormGroupWrapper = styled.div`
	display: flex;
	flex-direction: column;
	gap: 1em;
	
	.rs-form-group {
		margin: 0 !important;
	}
`;
