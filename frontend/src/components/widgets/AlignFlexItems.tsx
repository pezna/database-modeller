import styled from "@emotion/styled";

interface AlignFlexItemsProps {
	align: "right" | "left";
}

export const AlignFlexItems = styled.div<AlignFlexItemsProps>`
	display: flex;
	justify-content: ${props => props.align === "right" ? "flex-end" : "flex-start"};
	width: 100%;
	gap: 0.25rem;
`;