import styled from "@emotion/styled";

export const ScrollableContainer = styled.div`
	position: absolute;
	overflow-y: auto;
	width: 100%;
	height: 100%;
	max-height: 100%;
`;
