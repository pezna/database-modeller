import { Confirm } from "@/components/widgets";
import styled from "@emotion/styled";
import { Button, Popover, Tooltip, Whisper } from "rsuite";
import { TypeAttributes } from "rsuite/esm/internals/types";

interface DeleteConfirmProps {
	itemName: string;
	placement?: TypeAttributes.Placement;
	yes: () => void;
	className?: string;
	disabled?: boolean;
	disabledMessage?: string;
	deleteLanguage?: "delete" | "remove";
	icon?: "trash" | "times";
}

export function DeleteConfirm({
	itemName,
	placement,
	yes,
	className,
	disabled,
	disabledMessage,
	deleteLanguage,
	icon,
}: DeleteConfirmProps) {
	if (disabled) {
		return (
			<Wrapper className={className}>
				<Whisper
					trigger="hover"
					placement={placement ?? "bottomEnd"}
					speaker={<Tooltip>{disabledMessage}</Tooltip>}
				>
					<Button appearance="subtle" color="red">
						<span className="fa fa-trash"></span>
					</Button>
				</Whisper>
			</Wrapper>
		);
	}
	
	const renderPopover = () => {
		return (
			<Popover>
				<Confirm
					message={`Are you sure you want to ${deleteLanguage ?? "delete"} ${itemName}?`}
					yes={yes}
				/>
			</Popover>
		);
	};
	
	return (
		<Wrapper className={className}>
			<Whisper
				trigger="click"
				placement={placement ?? "bottomEnd"}
				speaker={renderPopover()}
			>
				<Button appearance="subtle" color="red">
					<span className={`fa fa-${icon ?? "trash"}`}></span>
				</Button>
			</Whisper>
		</Wrapper>
	);
}

const Wrapper = styled.div`
	display: flex;
	justify-content: flex-end;
`;