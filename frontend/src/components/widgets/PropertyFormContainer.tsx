import styled from "@emotion/styled";
import { ScrollableContainer } from "./ScrollableContainer";

const Header = styled.div`
	position: relative;
	padding-bottom: 2em;
	top: 0;
	flex-grow: 0;
	
	h4 {
		display: inline-block;
		margin-left: 10px;
	}
`;

const Contents = styled.div`
	flex-grow: 1;
	
	.rs-divider {
		margin: 0 0 10px 0;
		background: white !important;
	}
	
	.rs-plaintext {
		background: ${props => props.theme.sidebar.plaintextBackground};
		padding: 0.5em;
		border-radius: 4px;
		overflow: hidden;
	}
`;

const Footer = styled.div`
	position: relative;
	bottom: 0;
	flex-grow: 0;
	border-top: 1px solid #ccc;
	padding: 1em;
	margin-top: 1.5em;
`;

const Container = styled(ScrollableContainer)`
	flex-grow: 1;
	display: flex;
	flex-direction: column;
`;

export const PropertyForm = {
	Container,
	Header,
	Contents,
	Footer,
};
