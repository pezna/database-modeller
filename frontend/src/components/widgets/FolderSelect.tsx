import { InputComponentProps } from "@/components/widgets/types";
import { fileService } from "@/services";
import { Input, InputGroup } from "rsuite";

interface FolderSelectProps extends InputComponentProps {}

export function FolderSelect({labelId, value, onChange}: FolderSelectProps) {
	const handleSelectFolder = async () => {
		const folder = await fileService.openFolder(value);
		if (folder) {
			onChange(folder);
		}
	};
	
	return (
		<InputGroup>
			<Input id={labelId} value={value} onChange={onChange} />
			<InputGroup.Button onClick={handleSelectFolder}>
				<span className="fas fa-folder-open"></span>
			</InputGroup.Button>
		</InputGroup>
	)
}