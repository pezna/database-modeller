import styled from '@emotion/styled';
import { ReactElement } from 'react';
import { Nav } from 'rsuite';

interface TabsProps<T extends string> {
	active: T;
	onTabSelect: (active: T) => void;
	children: ReactElement<ItemProps<T>>[] | ReactElement<ItemProps<T>>;
}

export function TabPanel<T extends string>({ active, onTabSelect, children, ...rest }: TabsProps<T> & React.DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement>) {
	const hasArrayChildren = Array.isArray(children);
	const activeIndex = hasArrayChildren
		? children.findIndex(x => x.props.tabKey === active)
		: 0;
	
	const renderContent = () => {
		if (activeIndex < 0) {
			return null;
		}
		
		return hasArrayChildren ? children[activeIndex] : children;
	};
	
	const renderTabs = () => {
		if (hasArrayChildren) {
			return children.map((c, i) => (
				<Nav.Item key={i} eventKey={c.props.tabKey}>{c.props.title}</Nav.Item>
			))
		}
		
		return <Nav.Item eventKey={children.props.tabKey}>{children.props.title}</Nav.Item>
	};
	
	return (
		<Wrapper {...rest}>
			<Nav appearance="tabs" activeKey={active} onSelect={onTabSelect}>
				{renderTabs()}
			</Nav>
			<Content>
				{renderContent()}
			</Content>
		</Wrapper>
	);
}

interface ItemProps<T> {
	tabKey: T;
	title: string;
	children: ReactElement;
}

function Item<T>({ children }: ItemProps<T>): ReactElement {
	return children;
}
TabPanel.Item = Item;

const Wrapper = styled.div`
	display: flex;
	flex-direction: column;
	.rs-nav-item-active {
		// prevent the active tab from floating past the edge of the sidebar
		z-index: 0 !important;
	}
`;

const Content = styled.div`
	flex-grow: 1;
	position: relative;
`;
