import { RightClickMousePos } from "@/contexts";
import { AppMode } from "@/data";
import { useModelCreation } from "@/hooks";
import { getSelectedMachineId } from "@/selectors";
import { useSelector } from "@/store";
import { Dropdown } from "rsuite";

interface RightClickMachineMenuProps {
	pos: RightClickMousePos;
	closeMenu: () => void;
}

export function RightClickMachineMenu({ pos, closeMenu }: RightClickMachineMenuProps) {
	const { createBucket, createProducer, createNote } = useModelCreation();
	const machineId = useSelector(getSelectedMachineId)!;
	
	const handleCreateProducer = () => {
		createProducer(machineId, pos.boardX, pos.boardY);
		closeMenu();
	};
	
	const handleCreateBucket = () => {
		createBucket(machineId, pos.boardX, pos.boardY);
		closeMenu();
	};
	
	const handleCreateNote = () => {
		createNote(AppMode.machine, machineId, pos.boardX, pos.boardY);
		closeMenu();
	};
	
	return (
		<Dropdown.Menu>
			<Dropdown.Item onSelect={handleCreateProducer}>
				New Producer
			</Dropdown.Item>
			<Dropdown.Item onSelect={handleCreateBucket}>
				New Bucket
			</Dropdown.Item>
			<Dropdown.Item onSelect={handleCreateNote}>
				New Note
			</Dropdown.Item>
		</Dropdown.Menu>
	);
}