import { useRightClickContext } from "@/contexts";
import { AppMode, ModelObjectType } from "@/data";
import { getAppMode } from "@/selectors";
import { useSelector } from "@/store";
import styled from "@emotion/styled";
import { useLayoutEffect, useRef } from "react";
import { useClickAway } from "react-use";
import { RightClickColumnMenu } from "./RightClickColumnMenu";
import { RightClickComponentMenu } from "./RightClickComponentMenu";
import { RightClickConnectorMenu } from "./RightClickConnectorMenu";
import { RightClickDatabaseMenu } from "./RightClickDatabaseMenu";
import { RightClickMachineMenu } from "./RightClickMachineMenu";
import { RightClickNoteMenu } from "./RightClickNoteMenu";
import { RightClickTableMenu } from "./RightClickTableMenu";

export function RightClickMenu() {
	const { isMenuActive, itemId, itemType, mousePos, showMenu, hideMenu } = useRightClickContext();
	const appMode = useSelector(getAppMode);
	const ref = useRef<HTMLDivElement>(null);
	
	useClickAway(ref, () => {
		hideMenu();
	}, ["mouseup"]);
	
	useLayoutEffect(() => {
		if (!ref.current) {
			return;
		}
		
		// make sure the menu isn't overflowing
		const elementWidth = ref.current.clientWidth;
		const elementHeight = ref.current.clientHeight;
		const windowWidth = window.innerWidth;
		const windowHeight = window.innerHeight;
		let { x, y } = mousePos;
		const bottom = y + elementHeight;
		const right = x + elementWidth;
		
		if (bottom > windowHeight) {
			y = windowHeight - elementHeight;
		}
		
		if (right > windowWidth) {
			x = windowWidth - elementWidth;
		}
		
		if (x !== mousePos.x || y !== mousePos.y) {
			showMenu({ ...mousePos, x, y }, itemType, itemId);
		}
	}, [itemType, itemId]);
	
	if (!isMenuActive) {
		return null;
	}
	
	const renderMenu = () => {
		switch (itemType) {
			case ModelObjectType.table:
				return <RightClickTableMenu itemId={itemId!} closeMenu={hideMenu} />;
			case ModelObjectType.column:
				return <RightClickColumnMenu itemId={itemId!} closeMenu={hideMenu} />;
			case ModelObjectType.note:
				return <RightClickNoteMenu itemId={itemId!} closeMenu={hideMenu} />;
			case ModelObjectType.component:
				return <RightClickComponentMenu itemId={itemId!} closeMenu={hideMenu} />;
			case ModelObjectType.connector:
				return <RightClickConnectorMenu itemId={itemId!} closeMenu={hideMenu} />;
		}
		
		if (!itemType) {
			if (appMode === AppMode.database) {
				return <RightClickDatabaseMenu pos={mousePos} closeMenu={hideMenu} />;
			}
			else if (appMode === AppMode.machine) {
				return <RightClickMachineMenu pos={mousePos} closeMenu={hideMenu} />;
			}
		}
		
		return null;
	};
	
	return (
		<Wrapper ref={ref} style={{ left: `${mousePos.x}px`, top: `${mousePos.y}px` }}>
			{renderMenu()}
		</Wrapper>
	);
}

const Wrapper = styled.div`
	position: fixed;
	z-index: 100000;
`;