import { deleteComponent } from "@/actions";
import { useBoardItemCreation, useRightClickContext } from "@/contexts";
import { BoardTool, componentTypeDisplayName } from "@/data";
import { getComponent } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import { Dropdown } from "rsuite";
import { DeleteMenuItem } from "./DeleteMenuItem";

interface RightClickComponentMenuProps {
	itemId: string;
	closeMenu: () => void;
}

export function RightClickComponentMenu({ itemId, closeMenu }: RightClickComponentMenuProps) {
	const dispatch = useDispatch();
	const { mousePos } = useRightClickContext();
	const { setTool, setFirstSelectedItem, setMouseLocation } = useBoardItemCreation();
	const componentType = useSelector(state => getComponent(state, itemId).type);
	
	const handleCreateChannel = () => {
		setTool(BoardTool.createChannel);
		setFirstSelectedItem({
			id: itemId,
			isComponent: true,
			isColumn: false,
		});
		setMouseLocation({ x: mousePos.x, y: mousePos.y });
		closeMenu();
	};
	
	const handleCreateSignal = () => {
		setTool(BoardTool.createSignal);
		setFirstSelectedItem({
			id: itemId,
			isComponent: true,
			isColumn: false,
		});
		setMouseLocation({ x: mousePos.x, y: mousePos.y });
		closeMenu();
	};
	
	const handleDelete = () => {
		dispatch(deleteComponent(itemId));
		closeMenu();
	};
	
	return (
		<Dropdown.Menu>
			<Dropdown.Item onSelect={handleCreateChannel}>
				New Channel
			</Dropdown.Item>
			<Dropdown.Item onSelect={handleCreateSignal}>
				New Signal
			</Dropdown.Item>
			<Dropdown.Separator />
			<DeleteMenuItem itemName={componentTypeDisplayName[componentType]} confirmDelete={handleDelete} />
		</Dropdown.Menu>
	);
}