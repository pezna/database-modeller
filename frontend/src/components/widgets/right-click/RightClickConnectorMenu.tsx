import { deleteConnector } from "@/actions";
import { connectorTypeDisplayName } from "@/data";
import { getConnector } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import { Dropdown } from "rsuite";
import { DeleteMenuItem } from "./DeleteMenuItem";

interface RightClickConnectorMenuProps {
	itemId: string;
	closeMenu: () => void;
}

export function RightClickConnectorMenu({ itemId, closeMenu }: RightClickConnectorMenuProps) {
	const dispatch = useDispatch();
	const connectorType = useSelector(state => getConnector(state, itemId).type);
	
	const handleDelete = () => {
		dispatch(deleteConnector(itemId));
		closeMenu();
	};
	
	return (
		<Dropdown.Menu>
			<DeleteMenuItem itemName={connectorTypeDisplayName[connectorType]} confirmDelete={handleDelete} />
		</Dropdown.Menu>
	);
}