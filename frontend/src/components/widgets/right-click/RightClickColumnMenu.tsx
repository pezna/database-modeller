import { alterTableProperty, deleteColumn } from "@/actions";
import { useBoardItemCreation, useRightClickContext } from "@/contexts";
import { BoardTool } from "@/data";
import { useModelCreation } from "@/hooks";
import { getColumn, getTable } from "@/selectors";
import { useDispatch, useSelector } from "@/store";
import { Dropdown } from "rsuite";
import { DeleteMenuItem } from "./DeleteMenuItem";

interface RightClickColumnMenuProps {
	itemId: string;
	closeMenu: () => void;
}

export function RightClickColumnMenu({ itemId, closeMenu }: RightClickColumnMenuProps) {
	const dispatch = useDispatch();
	const { mousePos } = useRightClickContext();
	const { createIndex, createUnique } = useModelCreation();
	const { setTool, setFirstSelectedItem, setMouseLocation } = useBoardItemCreation();
	const tableId = useSelector(state => getColumn(state, itemId).tableId);
	const table = useSelector(state => getTable(state, tableId));
	
	const handleDelete = () => {
		dispatch(deleteColumn(itemId));
		closeMenu();
	};
	
	const handleCreateIndex = () => {
		createIndex(tableId, itemId);
		closeMenu();
	};
	
	const handleCreateUnique = () => {
		createUnique(tableId, itemId);
		closeMenu();
	};
	
	const handleCreateForeignKey = () => {
		setTool(BoardTool.createRelationship);
		setFirstSelectedItem({
			id: itemId,
			isComponent: false,
			isColumn: true,
			tableId,
		});
		setMouseLocation({ x: mousePos.x, y: mousePos.y });
		closeMenu();
	};
	
	const handleMoveUp = () => {
		const index = table.columnIds.indexOf(itemId);
		if (index <= 0) {
			return;
		}
		const prevIndex = index - 1;
		const columnIds = [...table.columnIds];
		// swap
		const prevColumnId = columnIds[prevIndex];
		columnIds[prevIndex] = itemId;
		columnIds[index] = prevColumnId;
		
		dispatch(alterTableProperty(tableId, "columnIds", columnIds));
	};
	
	const handleMoveDown = () => {
		const index = table.columnIds.indexOf(itemId);
		if (index === -1 || index === table.columnIds.length - 1) {
			return;
		}
		const nextIndex = index + 1;
		const columnIds = [...table.columnIds];
		// swap
		const nextColumnId = columnIds[nextIndex];
		columnIds[nextIndex] = itemId;
		columnIds[index] = nextColumnId;
		
		dispatch(alterTableProperty(tableId, "columnIds", columnIds));
	};
	
	return (
		<Dropdown.Menu>
			<Dropdown.Item onSelect={handleCreateIndex}>
				New Index
			</Dropdown.Item>
			<Dropdown.Item onSelect={handleCreateUnique}>
				New Unique
			</Dropdown.Item>
			<Dropdown.Item onSelect={handleCreateForeignKey}>
				New Relationship
			</Dropdown.Item>
			<Dropdown.Separator />
			<Dropdown.Item onSelect={handleMoveUp}>
				Move Up
			</Dropdown.Item>
			<Dropdown.Item onSelect={handleMoveDown}>
				Move Down
			</Dropdown.Item>
			<Dropdown.Separator />
			<DeleteMenuItem itemName="Column" confirmDelete={handleDelete} />
		</Dropdown.Menu>
	);
}