import { deleteNote } from "@/actions";
import { useDispatch } from "@/store";
import { Dropdown } from "rsuite";
import { DeleteMenuItem } from "./DeleteMenuItem";

interface RightClickNoteMenuProps {
	itemId: string;
	closeMenu: () => void;
}

export function RightClickNoteMenu({ itemId, closeMenu }: RightClickNoteMenuProps) {
	const dispatch = useDispatch();
	
	const handleDelete = () => {
		dispatch(deleteNote(itemId));
		closeMenu();
	};
	
	return (
		<Dropdown.Menu>
			<DeleteMenuItem itemName="Note" confirmDelete={handleDelete} />
		</Dropdown.Menu>
	);
}