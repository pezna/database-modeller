import { RightClickMousePos } from "@/contexts";
import { AppMode } from "@/data";
import { useModelCreation } from "@/hooks";
import { getSelectedNamespaceId } from "@/selectors";
import { useSelector } from "@/store";
import { Dropdown } from "rsuite";

interface RightClickDatabaseMenuProps {
	pos: RightClickMousePos;
	closeMenu: () => void;
}

export function RightClickDatabaseMenu({ pos, closeMenu }: RightClickDatabaseMenuProps) {
	const { createTable, createNote } = useModelCreation();
	const namespaceId = useSelector(getSelectedNamespaceId);
	
	const handleCreateTable = () => {
		if (!namespaceId) {
			return;
		}
		createTable(namespaceId, pos.boardX, pos.boardY);
		closeMenu();
	};
	
	const handleCreateNote = () => {
		if (!namespaceId) {
			return;
		}
		createNote(AppMode.database, namespaceId, pos.boardX, pos.boardY);
		closeMenu();
	};
	
	return (
		<Dropdown.Menu>
			<Dropdown.Item onSelect={handleCreateTable} disabled={!namespaceId}>
				New Table
			</Dropdown.Item>
			<Dropdown.Item onSelect={handleCreateNote} disabled={!namespaceId}>
				New Note
			</Dropdown.Item>
		</Dropdown.Menu>
	);
}