import { Dropdown, Stack } from "rsuite";

interface DeleteItemProps {
	itemName: string;
	confirmDelete: () => void;
}

export function DeleteMenuItem({ itemName, confirmDelete }: DeleteItemProps) {
	return (
		<Dropdown.Menu
			trigger="click"
			title={
				<Stack spacing={10}>
					<Stack.Item grow={0}>
						<span className="fa fa-trash"></span>
					</Stack.Item>
					<Stack.Item grow={1}>
						Delete {itemName}
					</Stack.Item>
				</Stack>
			}
		>
			<Dropdown.Item onSelect={confirmDelete}>
				Confirm deletion?
			</Dropdown.Item>
		</Dropdown.Menu>
	);
}
