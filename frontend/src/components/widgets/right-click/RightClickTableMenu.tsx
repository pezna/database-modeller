import { deleteTable } from "@/actions";
import { useModelCreation } from "@/hooks";
import { useDispatch } from "@/store";
import { Dropdown } from "rsuite";
import { DeleteMenuItem } from "./DeleteMenuItem";

interface RightClickTableMenuProps {
	itemId: string;
	closeMenu: () => void;
}

export function RightClickTableMenu({ itemId, closeMenu }: RightClickTableMenuProps) {
	const dispatch = useDispatch();
	const { createColumn } = useModelCreation();
	
	const handleDelete = () => {
		dispatch(deleteTable(itemId));
		closeMenu();
	};
	
	const handleCreateColumn = () => {
		createColumn(itemId);
		closeMenu();
	};
	
	return (
		<Dropdown.Menu>
			<Dropdown.Item onSelect={handleCreateColumn}>
				New Column
			</Dropdown.Item>
			<Dropdown.Separator />
			<DeleteMenuItem itemName="Table" confirmDelete={handleDelete} />
		</Dropdown.Menu>
	);
}