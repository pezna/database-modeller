import styled from "@emotion/styled";

interface ResizeBorderProps {
	placement: "left" | "right";
	onMouseDown: (e: React.MouseEvent<HTMLDivElement>) => void;
}

export function ResizeBorder({ placement, onMouseDown }: ResizeBorderProps) {
	return (
		<Wrapper placement={placement} onMouseDown={onMouseDown} />
	);
}

const Wrapper = styled.div<ResizeBorderProps>`
	position: absolute;
	cursor: ew-resize;
	height: 100%;
	width: ${props => props.theme.sidebar.resizeBorderSize};
	background: #fff;
	top: 0;
	${
		props => props.placement === "left"
		? "left: 0;"
		: "right: 0;"
	}
`;