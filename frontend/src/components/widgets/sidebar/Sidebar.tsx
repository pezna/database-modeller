import { getAppMode } from "@/selectors";
import { useSelector } from "@/store";
import styled from "@emotion/styled";
import { MutableRefObject, useCallback, useEffect, useLayoutEffect, useRef } from "react";
import { ResizeBorder } from "./ResizeBorder";
import { SideButtonBar } from "./SideButtonBar";
import { SidebarSectionItem } from "./types";

interface SidebarProps {
	placement: "left" | "right";
	sections: Record<string, SidebarSectionItem>;
	className: string;
	visibleSection: string | null;
	setVisibleSection: (index: string | null) => void;
	sidebarWidth: number;
	setSidebarWidth: (width: number) => void;
}

function isProperRefType(ref: any): ref is MutableRefObject<HTMLDivElement> {
	return typeof ref === "object" && Object.hasOwn(ref, "current");
}

export function Sidebar({
	placement,
	sections,
	className,
	visibleSection,
	setVisibleSection,
	sidebarWidth,
	setSidebarWidth,
}: SidebarProps) {
	const appMode = useSelector(getAppMode);
	const movingRef = useRef({
		initialWidth: 0,
		clientX: 0,
		clientY: 0,
		isMoving: false,
	});
	const firstRunRef = useRef(true);
	const sidebarRef = useRef<HTMLDivElement>(null);
	const isExpanded = visibleSection !== null;
	const isLeft = placement === 'left';
	
	useLayoutEffect(() => {
		// when this component is loaded for the first time,
		// this will make sure that the sidebar is set to the given width
		if (!visibleSection || !isProperRefType(sidebarRef) || !firstRunRef.current) {
			return;
		}
		
		firstRunRef.current = false;
		
		const sidebar = sidebarRef.current;
		const sidebarStyle = sidebar.style;
		if (sidebarWidth && sidebar.clientWidth !== sidebarWidth) {
			sidebarStyle.width = `${sidebarWidth}px`;
		}
	}, [visibleSection, sidebarWidth]);
	
	useLayoutEffect(() => {
		const handleMouseMove = (e: MouseEvent) => {
			if (!isProperRefType(sidebarRef)) {
				return;
			}
			
			const data = movingRef.current;
			if (data.isMoving) {
				const deltaX = e.clientX - data.clientX;
				const width = data.initialWidth + (isLeft ? deltaX : -deltaX);
				
				sidebarRef.current.style.width = `${width}px`;
				e.stopPropagation();
			}
		};
		
		const handleMouseUp = (e: MouseEvent) => {
			if (!isProperRefType(sidebarRef)) {
				return;
			}
			
			const data = movingRef.current;
			const sidebarStyle = sidebarRef.current.style;
			if (data.isMoving && sidebarStyle.width.endsWith('px')) {
				data.isMoving = false;
				sidebarStyle.userSelect = '';
				setSidebarWidth(sidebarRef.current.clientWidth);
				e.stopPropagation();
			}
		};
		
		document.addEventListener('mousemove', handleMouseMove);
		document.addEventListener('mouseup', handleMouseUp);
		
		return () => {
			document.removeEventListener('mousemove', handleMouseMove);
			document.removeEventListener('mouseup', handleMouseUp);
		};
	}, [setSidebarWidth]);
	
	const handleMouseDown = (e: React.MouseEvent<HTMLDivElement>) => {
		const target = e.target as Element;
		const targetParent = target.parentNode as Element;
		if (!isProperRefType(sidebarRef)) {
			return;
		}
		
		const data = movingRef.current;
		data.isMoving = true;
		data.initialWidth = targetParent.clientWidth;
		
		if (data.isMoving) {
			data.clientX = e.clientX;
			data.clientY = e.clientY;
			sidebarRef.current.style.userSelect = 'none';
			e.stopPropagation();
		}
	};
	
	const toggleVisibility = useCallback((section: string) => {
		if (!isProperRefType(sidebarRef)) {
			return;
		}
		
		const visible = visibleSection !== section;
		const anyAlreadyOpen = visibleSection !== null;
		
		setVisibleSection(visibleSection === section ? null : section);
		
		const style = sidebarRef.current.style;
		
		if (!visible) {
			style.width = "auto";
		}
		else if (!anyAlreadyOpen) {
			style.width = `${sidebarWidth}px`;
		}
	}, [visibleSection, sidebarWidth]);
	
	useEffect(() => {
		if (!visibleSection) {
			return;
		}
		
		// if the current section is not valid for the current app mode, toggle it off
		const item = sections[visibleSection];
		const hidden = item.isHidden?.() ?? false;
		if (item && hidden) {
			toggleVisibility(visibleSection);
		}
	}, [appMode, visibleSection, sections, toggleVisibility]);
	
	const renderResizeBar = () => {
		if (!isExpanded) {
			return null;
		}
		
		return (
			<ResizeBorder
				placement={isLeft ? "right" : "left"}
				onMouseDown={handleMouseDown}
			/>
		);
	};
	
	const renderButtonBar = () => {
		return (
			<SideButtonBar>
				{
					Object.entries(sections)
					.filter(([_, item]) => !item.isHidden?.())
					.map(([key, item]) => (
						<button
							key={key}
							title={item.buttonTitle}
							className={visibleSection === key ? "selected" : ""}
							onClick={() => toggleVisibility(key)}
						>
							<span className={item.icon}></span>
						</button>
					))
				}
			</SideButtonBar>
		);
	};
	
	const renderSection = () => {
		if (!visibleSection) {
			return null;
		}
		
		return (
			<SidebarContent>
				{sections[visibleSection].component()}
			</SidebarContent>
		);
	};
	
	const renderContents = () => {
		return isLeft ? (
			<>
				{renderButtonBar()}
				{renderSection()}
				{renderResizeBar()}
			</>
		) : (
			<>
				{renderResizeBar()}
				{renderButtonBar()}
				{renderSection()}
			</>
		);
	};
	
	return (
		<SidebarWrapper
			ref={sidebarRef}
			className={className + (isExpanded ? " expanded" : "")}
			placement={placement}
		>
			{renderContents()}
		</SidebarWrapper>
	);
};

const SidebarContent = styled.div`
	position: relative;
	display: flex;
	flex-grow: 1;
	
	& > * {
		flex-grow: 1;
	}
`;

interface SidebarWrapperProps {
	placement: SidebarProps["placement"];
}

const SidebarWrapper = styled.div<SidebarWrapperProps>`
	position: relative;
	display: flex;
	flex-direction: row;
	max-height: 100%;
	height: 100%;
	align-items: stretch;
	background-color: ${props => props.theme.sidebar.expandedBackgroundColor};
	color: ${props => props.theme.sidebar.foregroundColor};
	
	* {
		min-width: 0;
	}
	
	&.expanded {
		min-width: ${props => props.theme.sidebar.minWidth};
		
		${SidebarContent} {
			margin-right: ${props => props.placement === "left" ? props.theme.sidebar.resizeBorderSize : "0"};
		}
	}
`;
