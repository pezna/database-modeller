import { ReactNode } from "react";

export interface SidebarSectionItem {
	icon: string;
	buttonTitle: string;
	component: () => ReactNode;
	isHidden?: () => boolean;
}