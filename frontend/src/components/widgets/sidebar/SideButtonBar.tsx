import styled from "@emotion/styled";

export const SideButtonBar = styled.div`
	display: flex;
	flex-direction: column;
	background: ${props => props.theme.sidebar.buttonBarBackgroundColor};
	
	> button {
		border: none;
		background: none;
		cursor: pointer;
		color: #999;
		font-size: 2em;
		padding: 10px;
		
		&:hover {
			color: white;
		}
		
		&.selected {
			background: ${props => props.theme.sidebar.expandedBackgroundColor};
			color: white;
		}
		
		&:active {
			padding: 10px;
		}
	}
`;