import styled from "@emotion/styled";
import { IconButtonProps } from "rsuite";

interface SelectIconProps {
	onClick?: IconButtonProps["onClick"];
	label?: string;
}

export function SelectIcon({ onClick, label }: SelectIconProps) {
	return (
		<Wrapper onClick={onClick}>
			<span className="fa-regular fa-hand-pointer"></span>
			{label ?? ""}
		</Wrapper>
	);
}

const Wrapper = styled.button`
	display: flex;
	gap: 0.25em;
	align-items: center;
	background-color: transparent;
	color: var(--rs-gray-100);
	cursor: pointer;
	
	&:hover {
		background-color: transparent;
		color: var(--rs-gray-50);
	}
`;