import styled from "@emotion/styled";
import { IconButtonProps } from "rsuite";

interface HelpIconProps {
	onClick: IconButtonProps["onClick"];
}

export function HelpIcon({ onClick }: HelpIconProps) {
	return (
		<Wrapper onClick={onClick}>
			<span className="fa-regular fa-circle-question"></span>
		</Wrapper>
	);
}

const Wrapper = styled.button`
	background-color: transparent;
	color: #888;
	
	&:hover {
		background-color: transparent;
		color: #666;
	}
`;
