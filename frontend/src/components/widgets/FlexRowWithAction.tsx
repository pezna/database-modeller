import styled from "@emotion/styled";

export const FlexRowWithAction = styled.div`
	display: flex;
	margin-bottom: 4px;
	align-items: center;

	> div:first-of-type {
		flex-grow: 1;
		align-self: center;
	}
`;