import { ModelObjectType } from "@/data";
import { useModelNavigation } from "@/hooks";
import { getColumn } from "@/selectors";
import { useSelector } from "@/store";
import { Button } from "rsuite";

interface MixinMessageProps {
	visible: boolean;
	schemaType: ModelObjectType;
	mixinParentId?: string;
	mixinColumnId?: string;
}

export function MixinMessage({ visible, schemaType, mixinParentId, mixinColumnId }: MixinMessageProps) {
	const { select } = useModelNavigation();
	const columnMixinParentId = useSelector(state => 
		mixinColumnId
		? getColumn(state, mixinColumnId).belongsToMixinSchemaItem
		: undefined
	);
	
	if (!visible || !mixinParentId) {
		return null;
	}
	
	const navigateToOriginal = () => {
		columnMixinParentId
		? select(schemaType, mixinParentId, {columnMixinParentId})
		: select(schemaType, mixinParentId);
	};
	
	return (
		<div>
			This is a mixin clone; anything set from the original must be altered from the original.
			<Button appearance="subtle" onClick={navigateToOriginal}>
				<span className="fa fa-arrow-right-to-bracket"></span>
			</Button>
		</div>
	);
}