import { DeleteConfirm } from "@/components/widgets/DeleteConfirm";
import { ErrorMessage } from "@/components/widgets/ErrorMessage";
import { SlideReveal } from "@/components/widgets/SlideReveal";
import { TextButtonWithIcon } from "@/components/widgets/TextButtonWithIcon";
import { getModelError } from "@/selectors";
import { useSelector } from "@/store";
import styled from "@emotion/styled";
import { ReactNode, useMemo, useState } from "react";
import { Form, InputPicker } from "rsuite";
import { InputItemDataType } from "rsuite/esm/InputPicker";
import { TypeAttributes } from "rsuite/esm/internals/types";

interface MultiAddSelectionProps {
	label: ReactNode;
	valueIds: string[];
	options: InputItemDataType<string>[];
	optionLabelKey: string;
	optionValueKey: string;
	disallowEdit: boolean;
	pickerPlaceholder: string;
	addLabel: string;
	cancelAddLabel: string;
	errorKey: string;
	flashWhenEmpty?: boolean;
	allowDeleteLastItem?: boolean;
	deleteConfirmPlacement?: TypeAttributes.Placement;
	disableValueIds?: string[];
	additionalElementPerRow?: (index: number) => ReactNode;
	onChangeValue: (index: number, value: string | null) => void;
	onDelete: (index: number) => void;
	onAddValue: (value: string) => void;
}

export function MultiAddSelection({
	label,
	valueIds,
	options,
	optionLabelKey,
	optionValueKey,
	disallowEdit,
	pickerPlaceholder,
	addLabel,
	cancelAddLabel,
	errorKey,
	flashWhenEmpty,
	allowDeleteLastItem,
	deleteConfirmPlacement,
	disableValueIds,
	additionalElementPerRow,
	onChangeValue,
	onDelete,
	onAddValue,
}: MultiAddSelectionProps) {
	const errorMessage = useSelector(state => getModelError(state, errorKey));
	const allowDelete = (allowDeleteLastItem || valueIds.length > 1) && !disallowEdit;
	const disabledOptionValues = useMemo(() => {
		if (disableValueIds) {
			return [...valueIds, ...disableValueIds];
		}
		
		return valueIds;
	}, [disableValueIds, valueIds]);
	
	return (
		<Form.Group className={flashWhenEmpty && !valueIds.length ? "flash-border" : ""}>
			<Form.ControlLabel>{label}</Form.ControlLabel>
			{
				valueIds.map((id, index) => (
					<SelectItemRow
						key={`${id}-${index}`}
						index={index}
						value={id}
						options={options}
						optionLabelKey={optionLabelKey}
						optionValueKey={optionValueKey}
						disabledOptionValues={disabledOptionValues}
						disallowEdit={disallowEdit}
						pickerPlaceholder={pickerPlaceholder}
						allowDelete={allowDelete}
						deleteConfirmPlacement={deleteConfirmPlacement}
						additionalElementPerRow={additionalElementPerRow}
						onChangeValue={onChangeValue}
						onDelete={onDelete}
					/>
				))
			}
			<AddRow
				disallowEdit={disallowEdit}
				options={options}
				optionLabelKey={optionLabelKey}
				optionValueKey={optionValueKey}
				disabledOptionValues={disabledOptionValues}
				pickerPlaceholder={pickerPlaceholder}
				addLabel={addLabel}
				cancelAddLabel={cancelAddLabel}
				onSelect={onAddValue}
			/>
			<ErrorMessage errors={errorMessage} />
		</Form.Group>
	);
}

interface SelectItemRowProps {
	index: number;
	value: string;
	options: InputItemDataType<string>[];
	optionLabelKey: string;
	optionValueKey: string;
	disabledOptionValues: string[];
	disallowEdit: boolean;
	pickerPlaceholder: string;
	allowDelete: boolean;
	deleteConfirmPlacement?: TypeAttributes.Placement;
	additionalElementPerRow?: (index: number) => ReactNode;
	onChangeValue: (index: number, value: string | null) => void;
	onDelete: (index: number) => void;
}

function SelectItemRow({
	index,
	value,
	options,
	optionLabelKey,
	optionValueKey,
	disabledOptionValues,
	disallowEdit,
	pickerPlaceholder,
	allowDelete,
	deleteConfirmPlacement,
	additionalElementPerRow,
	onChangeValue,
	onDelete,
}: SelectItemRowProps) {
	const handleChange = (val: any) => {
		onChangeValue(index, val);
	};
	
	const selectedLabel = options.find(x => x[optionValueKey] === value)?.[optionLabelKey];
	
	return (
		<Row addCol={Boolean(additionalElementPerRow)}>
			<InputPicker
				data={options}
				labelKey={optionLabelKey}
				valueKey={optionValueKey}
				value={value}
				onChange={handleChange}
				disabledItemValues={disabledOptionValues}
				cleanable={false}
				placement="bottomEnd"
				plaintext={disallowEdit}
				placeholder={pickerPlaceholder}
				block
				preventOverflow
			/>
			{additionalElementPerRow && additionalElementPerRow(index)}
			{allowDelete && (
				<DeleteConfirm
					itemName={selectedLabel}
					yes={() => onDelete(index)}
					className="delete-button"
					deleteLanguage="remove"
					icon="times"
					placement={deleteConfirmPlacement ?? "topEnd"}
				/>
			)}
		</Row>
	);
}

const Row = styled.div<{addCol: boolean}>`
	display: grid;
	grid-template-columns: ${props => props.addCol ? "2fr 1fr" : "1fr"};
	grid-auto-columns: 32px;
	gap: 0.5em;
	margin-bottom: 1em;
	
	.delete-button {
		grid-column: ${props => props.addCol ? "3" : "2"};
	}
`;

interface AddRowProps {
	disallowEdit: boolean;
	options: InputItemDataType<string>[];
	optionLabelKey: string;
	optionValueKey: string;
	disabledOptionValues: string[];
	pickerPlaceholder: string;
	addLabel: string;
	cancelAddLabel: string;
	onSelect: (value: string) => void;
}

function AddRow({
	disallowEdit,
	options,
	optionLabelKey,
	optionValueKey,
	disabledOptionValues,
	pickerPlaceholder,
	addLabel,
	cancelAddLabel,
	onSelect,
}: AddRowProps) {
	const [isAdding, setIsAdding] = useState(false);
	
	if (disallowEdit) {
		return null;
	}
	
	const handleChange = (columnId: string | null) => {
		if (!columnId) {
			return;
		}
		
		setIsAdding(false);
		onSelect(columnId);
	};
	
	const renderNewPicker = () => {
		if (!isAdding) {
			return null;
		}
		
		return (
			<InputPicker
				data={options}
				labelKey={optionLabelKey}
				valueKey={optionValueKey}
				disabledItemValues={disabledOptionValues}
				placeholder={pickerPlaceholder}
				cleanable={false}
				onChange={handleChange}
				placement="bottomEnd"
				block
				preventOverflow
			/>
		);
	};
	
	const renderAddButton = () => {
		if (isAdding) {
			return (
				<TextButtonWithIcon key="1" color="red" appearance="subtle" onClick={() => setIsAdding(false)}>
					<span className="fas fa-minus"></span>
					{cancelAddLabel}
				</TextButtonWithIcon>
			);
		}
		
		return (
			<TextButtonWithIcon key="2" color="green" appearance="subtle" onClick={() => setIsAdding(true)}>
				<span className="fas fa-plus"></span>
				{addLabel}
			</TextButtonWithIcon>
		);
	}
	
	return (
		<SlideReveal
			hiddenComponent={renderNewPicker}
			alwaysVisibleComponent={renderAddButton}
			showHidden={isAdding}
		/>
	);
}