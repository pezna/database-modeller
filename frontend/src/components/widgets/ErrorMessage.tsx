import { ErrorData } from "@/data";
import styled from "@emotion/styled";

interface ErrorMessageProps {
	errors?: ErrorData[];
}

export function ErrorMessage({ errors }: ErrorMessageProps) {
	if (!errors) {
		return null;
	}
	
	return (
		<Wrapper>
			{errors.map((e, i) => (
				<div key={i}>{e.message}</div>
			))}
		</Wrapper>
	);
}

const Wrapper = styled.div`
	color: #ff9282;
`;
