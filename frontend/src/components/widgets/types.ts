import { AppMode, SettingDetail } from "@/data";

export interface SettingDetailRowProps {
	/**
	 * Ex: ["database"], ["database", "mysql"], etc
	 */
	keyPath: string[];
	/**
	 * The detail to be rendered
	 */
	settingDetail: SettingDetail<any, any>;
	isParentEnabled: boolean;
	appMode: AppMode;
}

export interface InputComponentProps {
	labelId?: string;
	value: any;
	onChange: (newValue: any) => void;
}
