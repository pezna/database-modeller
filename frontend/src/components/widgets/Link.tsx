import styled from "@emotion/styled";

export const Link = styled.span`
	color: var(--rs-primary-300);
	
	&:hover {
		cursor: pointer;
		text-decoration: underline;
	}
`;