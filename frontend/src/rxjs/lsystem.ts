import { ComponentType, LSystemComponentData } from "@/data";
import { LSystemGenerator } from "@/generators/lsystem-generator";
import { store } from "@/store";
import { Unsubscribe } from "redux";
import { BehaviorSubject, PartialObserver, Subscription } from "rxjs";

export class LSystemReactive {
	private currentStep: BehaviorSubject<number>;
	private currentAxiom: BehaviorSubject<string>;
	private generator: LSystemGenerator;
	
	constructor(public data: LSystemComponentData) {
		this.generator = new LSystemGenerator(data);
		this.currentStep = new BehaviorSubject(0);
		this.currentAxiom = new BehaviorSubject(this.generator.lsystem.axiom);
		this.currentStep.subscribe({
			next: (step) => {
				console.time("generating step " + step);
				const axiom = this.generator.getStep(step);
				console.timeEnd("generating step " + step);
				console.log(step + " length: " + axiom.length);
				this.currentAxiom.next(axiom);
			},
		});
	}
	
	delete() {
		this.currentStep.unsubscribe();
		this.currentStep.complete();
		this.currentAxiom.unsubscribe();
		this.currentAxiom.complete();
	}
	
	observeCurrentStep(observer: PartialObserver<number>): Subscription {
		return this.currentStep.subscribe(observer);
	}
	
	observeAxiom(observer: PartialObserver<string>): Subscription {
		return this.currentAxiom.subscribe(observer);
	}
	
	incrementStep() {
		const currVal = this.currentStep.value;
		const nextValue = currVal < this.generator.maxIterations ? currVal + 1 : currVal;
		this.#changeStep(nextValue);
	}
	
	decrementStep() {
		const currVal = this.currentStep.value;
		const nextValue = currVal > 0 ? currVal - 1 : currVal;
		this.#changeStep(nextValue);
	}
	
	#changeStep(step: number) {
		this.currentStep.next(step);
	}
	
	runGeneratorFinals() {
		return this.generator.runFinals();
	}
	
	updateData(data: LSystemComponentData) {
		if (data !== this.data) {
			this.data = data;
			this.generator.updateData(data);
			// must resend the current value to update diagram
			this.currentStep.next(this.currentStep.value);
		}
		else {
			console.log("skip because equals");
		}
	}
	
	get movementTurtle() {
		return this.generator.movement;
	}
}

class LSystemManager {
	systems: Record<string, LSystemReactive>;
	unsubscribeStore?: Unsubscribe;
	
	constructor() {
		this.systems = {};
		this.#subscribeStore();
	}
	
	setLSystem(data: LSystemComponentData) {
		this.systems[data.id]?.delete();
		this.systems[data.id] = new LSystemReactive(data);
	}
	
	getLSystemReactive(id: string) {
		return this.systems[id];
	}
	
	delete(id: string) {
		console.log("deleting lsystem", id);
		this.systems[id]?.delete();
		delete this.systems[id];
	}
	
	#subscribeStore() {
		if (this.unsubscribeStore) {
			return;
		}
		
		console.log("subscribing");
		this.unsubscribeStore = store.subscribe(() => {
			const state = store.getState();
			const updatedIds = new Set<string>();
			for (const comp of Object.values(state.machine.components)) {
				if (comp.type !== ComponentType.lsystem) {
					continue;
				}
				updatedIds.add(comp.id);
				
				const rc = this.systems[comp.id];
				if (!rc) {
					console.log("adding lsystem", comp.id);
					this.setLSystem(comp as LSystemComponentData);
					continue;
				}
				
				console.log("updating lsystem", comp.id);
				rc.updateData(comp as LSystemComponentData);
			}
			
			// delete lsystems that no longer exist
			Object.keys(this.systems).filter(id => !updatedIds.has(id)).map(this.delete);
		});
	}
};

export const lsystemManager = new LSystemManager();
