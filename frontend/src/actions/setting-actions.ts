import { ActionType, AlterSettingsPropertyActionData, FunctionActionData, SetSettingsActionData, SettingsState } from "@/data";
import { getModelFilePath } from "@/selectors";
import { fileService } from "@/services";
import { IfKey, NextKey } from "@/utils";
import { loadModelState } from "./file-actions";

export function alterSettingsProperty<
	K1 extends NextKey<SettingsState>, T1 extends IfKey<SettingsState, K1>,
	K2 extends NextKey<T1, K1>, T2 extends IfKey<T1, K2>,
	K3 extends NextKey<T2, K2>, T3 extends IfKey<T2, K3>,
>(path: [K1?, K2?, K3?], value: T3): AlterSettingsPropertyActionData {
	return {
		type: ActionType.alterSettingsProperty,
		payload: {
			path: path as string[],
			value,
		},
	};
}

export function setSettings(settings: SettingsState): SetSettingsActionData {
	return {
		type: ActionType.setSettings,
		payload: {
			settings,
		},
	};
}

/**
 * Load application settings. Should only ever be called once (at start up)
 */
export function loadAppSettings(): FunctionActionData {
	return async (dispatch, getState) => {
		const settings = await fileService.loadAppSettings();
		dispatch(setSettings(settings));
		
		const currentFilePath = getModelFilePath(getState());
		const { mostRecentModelFiles } = settings.general.files;
		const { reopenLastModelFile } = settings.general.startUp;
		if (reopenLastModelFile && mostRecentModelFiles.length && !currentFilePath) {
			const filePath = mostRecentModelFiles.at(-1);
			dispatch(loadModelState(filePath ?? null));
		}
	};
}
