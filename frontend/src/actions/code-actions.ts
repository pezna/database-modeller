import { ActionType, CodeState, DeleteSqlDocumentActionData, DeleteSqlSchemaUsesActionData, DeleteStructOutputActionData, FunctionActionData, SetCodeStateActionData, SetSqlDocumentActionData, SetSqlSchemaUsesActionData, SetStructOutputActionData, SqlDocumentData, SqlDocumentSchemaUsage, StructOutputData } from "@/data";
import { StructOutputGenerator } from "@/generators";
import { getStructOutput } from "@/selectors";

export function setCodeState(data: CodeState): SetCodeStateActionData {
	return {
		type: ActionType.setCodeState,
		payload: {
			data,
		},
	};
}

export function setSqlDocument(data: SqlDocumentData): SetSqlDocumentActionData {
	return {
		type: ActionType.setSqlDocument,
		payload: {
			data,
		},
	};
}

export function deleteSqlDocument(id: string): DeleteSqlDocumentActionData {
	return {
		type: ActionType.deleteSqlDocument,
		payload: {
			id,
		},
	};
}

export function setSqlSchemaUses(id: string, uses: SqlDocumentSchemaUsage[]): SetSqlSchemaUsesActionData {
	return {
		type: ActionType.setSqlSchemaUses,
		payload: {
			id,
			uses,
		},
	};
}

export function deleteSqlSchemaUses(id: string): DeleteSqlSchemaUsesActionData {
	return {
		type: ActionType.deleteSqlSchemaUses,
		payload: {
			id,
		},
	};
}

export function setStructOutput(data: StructOutputData): SetStructOutputActionData {
	return {
		type: ActionType.setStructOutput,
		payload: {
			data,
		},
	};
}

export function deleteStructOutput(id: string): DeleteStructOutputActionData {
	return {
		type: ActionType.deleteStructOutput,
		payload: {
			id,
		},
	};
}

export function runStructOutput(id: string): FunctionActionData {
	return async function(dispatch, getState) {
		const state = getState();
		const structOutput = getStructOutput(state, id);
		const gen = new StructOutputGenerator(structOutput, state);
		await gen.run(dispatch);
	}
}
