import { ActionType, AlterUiStatePropertyActionData, DeselectBoardActionData, FunctionActionData, ManageSchemaSidebarUiState, ModelObjectType, SelectModelObjectActionData, SetUiStateActionData, UiState } from "@/data";
import { getDatabaseModeUi, getMachineModeUi, getManageSchemaSidebarUi, getSelectedMachineId, getSelectedNamespaceId } from "@/selectors";
import { ArrayPropertyNames, IfKey, NextKey } from "@/utils";

export function setUiState(state: UiState): SetUiStateActionData {
	return {
		type: ActionType.setUiState,
		payload: {
			state,
		},
	};
}

export function alterUiStateProperty<
	K1 extends NextKey<UiState>, T1 extends IfKey<UiState, K1>,
	K2 extends NextKey<T1, K1>, T2 extends IfKey<T1, K2>,
	K3 extends NextKey<T2, K2>, T3 extends IfKey<T2, K3>,
>(path: [K1?, K2?, K3?], value: T3): AlterUiStatePropertyActionData {
	return {
		type: ActionType.alterUiStateProperty,
		payload: {
			path: path as any,
			value,
		},
	};
}

export function removeMachineTab(machineId: string): FunctionActionData {
	return async (dispatch, getState) => {
		const state = getState();
		const machineUi = getMachineModeUi(state);
		const activeMachineId = getSelectedMachineId(state);
		const openTabIds = machineUi.openMachineTabs;
		const index = openTabIds.indexOf(machineId);
		if (index > -1) {
			const newTabIds = [...openTabIds];
			newTabIds.splice(index, 1);
			dispatch(alterUiStateProperty(["machineModeUi", "openMachineTabs"], newTabIds));
			if (activeMachineId === machineId) {
				const nextTab = newTabIds[Math.max(0, index - 1)] ?? null;
				dispatch(selectMachine(nextTab));
			}
		}
	}
}

export function removeNamespaceTab(namespaceId: string): FunctionActionData {
	return async (dispatch, getState) => {
		const state = getState();
		const namespaceUi = getDatabaseModeUi(state);
		const activeNamespaceId = getSelectedNamespaceId(state);
		const openTabIds = namespaceUi.openNamespaceTabs;
		const index = openTabIds.indexOf(namespaceId);
		if (index > -1) {
			const newTabIds = [...openTabIds];
			newTabIds.splice(index, 1);
			dispatch(alterUiStateProperty(["databaseModeUi", "openNamespaceTabs"], newTabIds));
			if (activeNamespaceId === namespaceId) {
				const nextTab = newTabIds[Math.max(0, index - 1)] ?? null;
				dispatch(selectNamespace(nextTab));
			}
		}
	}
}

export function removeManageSchemaOpenAccordion(id: string, property: ArrayPropertyNames<ManageSchemaSidebarUiState>): FunctionActionData {
	return async (dispatch, getState) => {
		const state = getState();
		const openAccordionIds = getManageSchemaSidebarUi(state)[property];
		const openIndex = openAccordionIds.indexOf(id);
		if (openIndex > -1) {
			const newIds = [...openAccordionIds];
			newIds.splice(openIndex, 1);
			dispatch(alterUiStateProperty(["manageSchemaSidebar", property], newIds));
		}
	}
}

export function clearProjectSpecificIdsFromUiState(): FunctionActionData {
	return async (dispatch) => {
		dispatch(alterUiStateProperty(["machineModeUi", "openMachineTabs"], []));
		dispatch(alterUiStateProperty(["databaseModeUi", "openNamespaceTabs"], []));
		dispatch(alterUiStateProperty(["manageSchemaSidebar", "openMachineIds"], []));
		dispatch(alterUiStateProperty(["manageSchemaSidebar", "openTableIds"], []));
		dispatch(alterUiStateProperty(["manageSchemaSidebar", "openColumnTypeIds"], []));
		dispatch(alterUiStateProperty(["exportSidebar", "historyOpenExportIds"], []));
		dispatch(alterUiStateProperty(["codeSidebar", "expandedTreeIds"], []));
	}
}

export function deselectBoard(everything?: boolean): DeselectBoardActionData {
	return {
		type: ActionType.deselectBoard,
		payload: {
			everything,
		},
	};
}

export function selectNamespace(namespaceId: string | null): FunctionActionData {
	return async (dispatch, getState) => {
		if (namespaceId) {
			const state = getState();
			const namespaceUi = getDatabaseModeUi(state);
			if (!namespaceUi.openNamespaceTabs.includes(namespaceId)) {
				const openTabs = [
					...namespaceUi.openNamespaceTabs,
					namespaceId,
				];
				dispatch(alterUiStateProperty(["databaseModeUi", "openNamespaceTabs"], openTabs));
			}
		}
		
		const action: SelectModelObjectActionData = {
			type: ActionType.selectModelObject,
			payload: {
				modelType: ModelObjectType.namespace,
				id: namespaceId,
				otherIds: {},
			},
		};
		
		dispatch(action);
	};
}

export function selectMachine(machineId: string | null): FunctionActionData {
	return async (dispatch, getState) => {
		if (machineId) {
			const state = getState();
			const machineUi = getMachineModeUi(state);
			if (!machineUi.openMachineTabs.includes(machineId)) {
				const openTabs = [
					...machineUi.openMachineTabs,
					machineId,
				];
				dispatch(alterUiStateProperty(["machineModeUi", "openMachineTabs"], openTabs));
			}
		}
		
		const action: SelectModelObjectActionData = {
			type: ActionType.selectModelObject,
			payload: {
				modelType: ModelObjectType.machine,
				id: machineId,
				otherIds: {},
			},
		};
		
		dispatch(action);
	};
}

export function selectComponent(componentId: string): SelectModelObjectActionData {
	return {
		type: ActionType.selectModelObject,
		payload: {
			modelType: ModelObjectType.component,
			id: componentId,
			otherIds: {},
		},
	};
}

export function selectConnector(connectorId: string): SelectModelObjectActionData {
	return {
		type: ActionType.selectModelObject,
		payload: {
			modelType: ModelObjectType.connector,
			id: connectorId,
			otherIds: {},
		},
	};
}

export function selectTable(tableId: string): SelectModelObjectActionData {
	return {
		type: ActionType.selectModelObject,
		payload: {
			modelType: ModelObjectType.table,
			id: tableId,
			otherIds: {},
		},
	};
}

export function selectColumn(columnId: string): SelectModelObjectActionData {
	return {
		type: ActionType.selectModelObject,
		payload: {
			modelType: ModelObjectType.column,
			id: columnId,
			otherIds: {},
		},
	};
}

export function selectRelationship(relationshipId: string): SelectModelObjectActionData {
	return {
		type: ActionType.selectModelObject,
		payload: {
			modelType: ModelObjectType.relationship,
			id: relationshipId,
			otherIds: {},
		},
	};
}

export function selectNote(noteId: string): SelectModelObjectActionData {
	return {
		type: ActionType.selectModelObject,
		payload: {
			modelType: ModelObjectType.note,
			id: noteId,
			otherIds: {},
		},
	};
}
