import {
	ActionType, AlterColumnOptionPropertyActionData, AlterColumnPropertyActionData, AlterIndexPropertyActionData, AlterNamespacePropertyActionData,
	AlterPrimaryKeyPropertyActionData, AlterRelationshipPropertyActionData, AlterSchemaSettingsPropertyActionData, AlterTablePropertyActionData,
	AlterUniquePropertyActionData, ColumnData, ColumnDataProperty, ColumnOptionType, ColumnOptionValueContainer, ColumnTypeData,
	CreateColumnActionData, CreateIndexActionData, CreateNamespaceActionData, CreatePrimaryKeyActionData, CreateRelationshipActionData,
	CreateTableActionData, CreateUniqueActionData, DeleteColumnActionData, DeleteColumnTypeActionData, DeleteIndexActionData,
	DeleteNamespaceActionData, DeletePrimaryKeyActionData, DeleteRelationshipActionData, DeleteTableActionData, DeleteUniqueActionData,
	FunctionActionData, IndexData, IndexDataProperty, NamespaceData, NamespaceDataProperty, PrimaryKeyData, PrimaryKeyDataProperty,
	RelationshipData, RelationshipDataProperty, ReplaceColumnTypeUsageActionData, SchemaItemKey, SchemaSettingsData,
	SetColumnTypeActionData,
	SetSchemaSettingsActionData, SetSchemaStateActionData, SimpleSchemaState, TableData, TableDataProperty, UniqueData, UniqueDataProperty
} from "@/data";
import { codeService } from "@/services";
import { INDEX_CODE_ID, IfKey, NextKey, PRIMARY_KEY_CODE_ID, RELATIONSHIP_CODE_ID, UNIQUE_CODE_ID } from "@/utils";
import { createJavascriptError } from "./error-actions";

export function setSchemaState(schema: SimpleSchemaState): SetSchemaStateActionData {
	return {
		type: ActionType.setSchemaState,
		payload: {
			schema,
		},
	};
}

export function createTable(table: TableData, errorKey?: string): CreateTableActionData {
	return {
		type: ActionType.createTable,
		payload: {
			table,
		},
		errorKey,
	};
}

export function deleteTable(id: string, errorKey?: string): DeleteTableActionData {
	return {
		type: ActionType.deleteTable,
		payload: {
			id,
		},
		errorKey,
	};
}

export function alterTableProperty<T extends TableDataProperty>(tableId: string, propertyName: T, value: TableData[T], errorKey?: string): AlterTablePropertyActionData {
	return {
		type: ActionType.alterTableProperty,
		payload: {
			tableId,
			propertyName,
			value,
		},
		errorKey,
	};
}

export function createColumn(column: ColumnData, errorKey?: string) : CreateColumnActionData {
	return {
		type: ActionType.createColumn,
		payload: {
			column,
		},
		errorKey,
	};
}

export function deleteColumn(columnId: string, errorKey?: string): DeleteColumnActionData {
	return {
		type: ActionType.deleteColumn,
		payload: {
			columnId,
		},
		errorKey,
	};
}

export function alterColumnProperty<T extends ColumnDataProperty>(columnId: string, propertyName: T, value: ColumnData[T], errorKey?: string): AlterColumnPropertyActionData {
	return {
		type: ActionType.alterColumnProperty,
		payload: {
			columnId,
			propertyName,
			value,
		},
		errorKey,
	};
}

export function alterColumnOptionProperty<T extends ColumnOptionType>(columnId: string, propertyName: T, value: ColumnOptionValueContainer[T], errorKey?: string): AlterColumnOptionPropertyActionData<T> {
	return {
		type: ActionType.alterColumnOptionProperty,
		payload: {
			columnId,
			propertyName,
			value,
		},
		errorKey,
	};
}

export function setColumnType(columnType: ColumnTypeData, errorKey?: string) : SetColumnTypeActionData
{
	return {
		type: ActionType.setColumnType,
		payload: {
			columnType,
		},
		errorKey,
	};
}

export function deleteColumnType(columnTypeId: string, errorKey?: string): DeleteColumnTypeActionData {
	return {
		type: ActionType.deleteColumnType,
		payload: {
			columnTypeId,
		},
		errorKey,
	};
}

export function replaceColumnTypeUsage(columnTypeId: string, replacementColumnTypeId: string, errorKey?: string): ReplaceColumnTypeUsageActionData {
	return {
		type: ActionType.replaceColumnTypeUsage,
		payload: {
			columnTypeId,
			replacementColumnTypeId,
		},
		errorKey,
	};
}

export function createRelationship(relationship: RelationshipData, errorKey?: string) : CreateRelationshipActionData {
	return {
		type: ActionType.createRelationship,
		payload: {
			relationship,
		},
		errorKey,
	};
}

export function alterRelationshipProperty<T extends RelationshipDataProperty>(id: string, propertyName: T, value: RelationshipData[T], errorKey?: string): AlterRelationshipPropertyActionData {
	return {
		type: ActionType.alterRelationshipProperty,
		payload: {
			id,
			propertyName,
			value,
		},
		errorKey,
	};
}

export function deleteRelationship(relationshipId: string, errorKey?: string): DeleteRelationshipActionData {
	return {
		type: ActionType.deleteRelationship,
		payload: {
			relationshipId,
		},
		errorKey,
	};
}

export function createIndex(index: IndexData, errorKey?: string) : CreateIndexActionData {
	return {
		type: ActionType.createIndex,
		payload: {
			index,
		},
		errorKey,
	};
}

export function alterIndexProperty<T extends IndexDataProperty>(id: string, propertyName: T, value: IndexData[T], errorKey?: string): AlterIndexPropertyActionData {
	return {
		type: ActionType.alterIndexProperty,
		payload: {
			id,
			propertyName,
			value,
		},
		errorKey,
	};
}

export function deleteIndex(id: string, errorKey?: string): DeleteIndexActionData {
	return {
		type: ActionType.deleteIndex,
		payload: {
			id,
		},
		errorKey,
	};
}

export function createUnique(unique: UniqueData, errorKey?: string) : CreateUniqueActionData {
	return {
		type: ActionType.createUnique,
		payload: {
			unique,
		},
		errorKey,
	};
}

export function alterUniqueProperty<T extends UniqueDataProperty>(id: string, propertyName: T, value: UniqueData[T], errorKey?: string): AlterUniquePropertyActionData {
	return {
		type: ActionType.alterUniqueProperty,
		payload: {
			id,
			propertyName,
			value,
		},
		errorKey,
	};
}

export function deleteUnique(id: string, errorKey?: string): DeleteUniqueActionData {
	return {
		type: ActionType.deleteUnique,
		payload: {
			id,
		},
		errorKey,
	};
}

export function createPrimaryKey(primaryKey: PrimaryKeyData, errorKey?: string) : CreatePrimaryKeyActionData {
	return {
		type: ActionType.createPrimaryKey,
		payload: {
			primaryKey,
		},
		errorKey,
	};
}

export function alterPrimaryKeyProperty<T extends PrimaryKeyDataProperty>(id: string, propertyName: T, value: PrimaryKeyData[T], errorKey?: string): AlterPrimaryKeyPropertyActionData {
	return {
		type: ActionType.alterPrimaryKeyProperty,
		payload: {
			id,
			propertyName,
			value,
		},
		errorKey,
	};
}

export function deletePrimaryKey(id: string, errorKey?: string): DeletePrimaryKeyActionData {
	return {
		type: ActionType.deletePrimaryKey,
		payload: {
			id,
		},
		errorKey,
	};
}

export function setSchemaSettings(settings: SchemaSettingsData, errorKey?: string): SetSchemaSettingsActionData {
	return {
		type: ActionType.setSchemaSettings,
		payload: {
			settings,
		},
		errorKey,
	};
}

export function alterSchemaSettingsProperty<
	K1 extends NextKey<SchemaSettingsData>, T1 extends IfKey<SchemaSettingsData, K1>,
	K2 extends NextKey<T1, K1>, T2 extends IfKey<T1, K2>,
	K3 extends NextKey<T2, K2>, T3 extends IfKey<T2, K3>,
>(path: [K1?, K2?, K3?], value: T3, errorKey?: string): AlterSchemaSettingsPropertyActionData {
	if (path.length === 0 || path.includes(undefined)) {
		throw new Error(`Path must contain at least one element and must not contain undefined: ${JSON.stringify(path)}`);
	}
	
	return {
		type: ActionType.alterSchemaSettingsProperty,
		payload: {
			path: path as string[],
			value,
		},
		errorKey,
	};
}

export function refreshGeneratedNames(schemaType: Exclude<SchemaItemKey, 'namespaces' | 'columnTypes'>, schemaItemId: string): FunctionActionData {
	return async (dispatch, getState) => {
		const schemaState = getState().schema;
		// TODO jsonService.updateSchemaJson(schemaState);
		
		const items = {
			primaryKeys: [] as PrimaryKeyData[],
			indexes: [] as IndexData[],
			uniques: [] as UniqueData[],
			relationships: [] as RelationshipData[],
		};
		
		if (schemaType === 'columns') {
			const schemaId = schemaItemId!;
			items.primaryKeys = Object.values(schemaState.primaryKeys).filter(x => x.columnIds.includes(schemaId));
			items.indexes = Object.values(schemaState.indexes).filter(x => x.columnIds.includes(schemaId));
			items.uniques = Object.values(schemaState.uniques).filter(x => x.columnIds.includes(schemaId));
			items.relationships = Object.values(schemaState.relationships).filter(x => x.childColumnIds.includes(schemaId) || x.parentColumnIds.includes(schemaId));
		}
		else if (schemaType === 'tables') {
			const schemaId = schemaItemId!;
			items.primaryKeys = Object.values(schemaState.primaryKeys).filter(x => x.tableId === schemaId);
			items.indexes = Object.values(schemaState.indexes).filter(x => x.tableId === schemaId);
			items.uniques = Object.values(schemaState.uniques).filter(x => x.tableId === schemaId);
			items.relationships = Object.values(schemaState.relationships).filter(x => x.childTableId === schemaId || x.parentTableId === schemaId);
		}
		else {
			const val = schemaState[schemaType][schemaItemId] as any;
			items[schemaType].push(val);
		}
		
		const basicSchemaMap = async (x: PrimaryKeyData | IndexData | UniqueData, type: typeof schemaType, codeId: string) => {
			const res = await codeService.runCodeContainer({
				id: codeId,
				nameOptions: {
					firstTableId: x.tableId,
					firstColumnIds: x.columnIds,
					secondTableId: null,
					secondColumnIds: null,
				},
				exportOptions: null,
				structOutputOptions: null,
			});
			
			if (res.ok) {
				switch (type) {
					case "indexes":
						dispatch(alterIndexProperty(x.id, "name", res.ok.result.value));
						break;
					case "primaryKeys":
						dispatch(alterPrimaryKeyProperty(x.id, "name", res.ok.result.value));
						break;
					case "uniques":
						dispatch(alterUniqueProperty(x.id, "name", res.ok.result.value));
						break;
				}
			}
			else if (res.err && res.err.type === 'runFailure') {
				dispatch(createJavascriptError(res.err.error, codeId));
			}
			else {
				console.error('No code found for basicSchemaMap', codeId);
			}
		};
		
		const relationshipSchemaMap = async (rel: RelationshipData, codeId: string) => {
			const res = await codeService.runCodeContainer({
				id: codeId,
				nameOptions: {
					firstTableId: rel.childTableId,
					firstColumnIds: rel.childColumnIds,
					secondTableId: rel.parentTableId,
					secondColumnIds: rel.parentColumnIds,
				},
				exportOptions: null,
				structOutputOptions: null,
			});
			
			if (res.ok) {
				dispatch(alterRelationshipProperty(rel.id, "name", res.ok.result.value));
			}
			else if (res.err && res.err.type === 'runFailure') {
				dispatch(createJavascriptError(res.err.error, codeId));
			}
			else {
				console.error('No code found for basicSchemaMap', codeId);
			}
		};
		
		items.primaryKeys.forEach(pk => basicSchemaMap(pk, "primaryKeys", PRIMARY_KEY_CODE_ID));
		items.indexes.forEach(ix => basicSchemaMap(ix, "indexes", INDEX_CODE_ID));
		items.uniques.forEach(uq => basicSchemaMap(uq, "uniques", UNIQUE_CODE_ID));
		items.relationships.forEach(r => relationshipSchemaMap(r, RELATIONSHIP_CODE_ID));
	};
}

export function createNamespace(namespace: NamespaceData, errorKey?: string): CreateNamespaceActionData {
	return {
		type: ActionType.createNamespace,
		payload: {
			namespace,
		},
		errorKey,
	};
}

export function alterNamespaceProperty<T extends NamespaceDataProperty>(id: string, propertyName: T, value: NamespaceData[T], errorKey?: string): AlterNamespacePropertyActionData {
	return {
		type: ActionType.alterNamespaceProperty,
		payload: {
			id,
			propertyName,
			value,
		},
		errorKey,
	};
}

export function deleteNamespace(id: string, errorKey?: string): DeleteNamespaceActionData {
	return {
		type: ActionType.deleteNamespace,
		payload: {
			id,
		},
		errorKey,
	};
}
