import { ActionType, AddExportHistoryActionData, CodeServiceError, CustomExport, DeleteCustomExportActionData, DeleteExportHistoryActionData, ExportState, FunctionActionData, MessageAlertType, ServiceResponseStatus, SetCustomExportActionData, SetExportStateActionData, SqlExportHistoryItem, SqlExportOptions, convertTemplateString, createSimpleSchemaState, newSqlExportHistory } from "@/data";
import { createExportRunContainers } from "@/generators";
import { initialSchemaState } from "@/reducers/schema-reducer";
import { getModelFilePath, getSortedExportHistory } from "@/selectors";
import { codeService, exportService } from "@/services";
import { ParsedPath } from "@/utils";
import { partition } from "lodash";
import { createNotification } from "./general-actions";


export function setExportState(data: ExportState): SetExportStateActionData {
	return {
		type: ActionType.setExportState,
		payload: {
			data,
		},
	};
}

export function addExportHistory(data: SqlExportHistoryItem): AddExportHistoryActionData {
	return {
		type: ActionType.addExportHistory,
		payload: {
			data,
		},
	};
}

export function deleteExportHistory(time: number): DeleteExportHistoryActionData {
	return {
		type: ActionType.deleteExportHistory,
		payload: {
			time,
		},
	};
}

export function exportToSql(options: SqlExportOptions, shouldCreateAll: boolean): FunctionActionData {
	return async (dispatch, getState) => {
		const state = getState();
		const mostRecentExport: SqlExportHistoryItem | undefined = getSortedExportHistory(state, options.type)[0];
		const oldSchemaState = createSimpleSchemaState((mostRecentExport && !shouldCreateAll) ? mostRecentExport : initialSchemaState);
		const result = await exportService.generateSqlMigration(options, oldSchemaState, state);
		
		// only check failed, not aborted
		if (result.status === ServiceResponseStatus.fail) {
			dispatch(createNotification(
				MessageAlertType.error,
				"Could not export SQL",
				result.statusMessage ?? "",
			));
			return;
		}
		
		const historyItem = newSqlExportHistory(options.type, result.time, mostRecentExport ? mostRecentExport.id : "", state);
		dispatch(addExportHistory(historyItem));
	};
}

export function exportToCustom(exportData: CustomExport, writeFiles: boolean): FunctionActionData {
	return async (dispatch, getState) => {
		const state = getState();
		
		const runContainers = createExportRunContainers(exportData, state);
		const results = await Promise.all(runContainers.map(codeService.runCodeContainer));
		const [successes, failures] = partition(results, (x) => x.ok !== undefined);
		
		dispatchErrors(failures as any);
		
		if (!failures.length) {
			// TODO
			const modelFilePath = getModelFilePath(state) ?? "";
			const baseDir = convertTemplateString(exportData.baseExportDirectory ?? "", {
				modelFileFolder: new ParsedPath(modelFilePath).directory,
			});
			/* 
			const writeResponses = await Promise.all(successes.map(res =>
				// fileService.writeFile(baseDir + pathSeparator() + res.result!.fileName, res.result!.value)
			));
			if (!writeResponses) {
				return;
			}
			const failedResponses = writeResponses.filter(x => x.status === ServiceResponseStatus.fail);
			
			dispatchErrors(failedResponses);
			
			if (!failedResponses.length) {
				dispatch(createNotification(
					MessageAlertType.success,
					"Successfully exported to " + baseDir,
					""
				));
			}
			 */
		}
		
		function dispatchErrors(failures: CodeServiceError[]) {
			failures.forEach(result => {
				dispatch(createNotification(
					MessageAlertType.error,
					"Could not perform export",
					`\n${(result as any).error?.message ?? ""}`
				));
			});
		}
	}
}

export function setCustomExport(data: CustomExport): SetCustomExportActionData {
	return {
		type: ActionType.setCustomExport,
		payload: {
			data,
		},
	};
}

export function deleteCustomExport(id: string): DeleteCustomExportActionData {
	return {
		type: ActionType.deleteCustomExport,
		payload: {
			id,
		},
	};
}
