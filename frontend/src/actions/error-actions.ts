import { ActionType, CreateErrorActionData, DeleteErrorActionData, ErrorState, ModelObjectType } from "@/data";
import { CodeError } from "@/data/backend";
import { IdPrefix, generateId } from "@/utils";

export function createError<T extends keyof ErrorState, V extends ErrorState[T][keyof ErrorState[T]][number]>(errorKey: string, errorGroup: T, data: V): CreateErrorActionData {
	return {
		type: ActionType.createError,
		payload: {
			errorGroup,
			errorKey,
			data,
		},
	};
}

export function deleteError(errorKey: string, errorGroup: keyof ErrorState): DeleteErrorActionData {
	return {
		type: ActionType.deleteError,
		payload: {
			errorGroup,
			errorKey,
		},
	};
}

export function errorNameAlreadyExists(name: string, schemaType: ModelObjectType, schemaId: string, errorKey?: string) {
	return createError(
		errorKey ?? generateId(IdPrefix.unknown),
		"modelErrors",
		{
			schemaType,
			schemaId,
			errorKey,
			message: `${name} already exists`,
		}
	);
}

export function errorSchemaItemNotExists(schemaType: ModelObjectType, schemaId: string, notFoundSchemaType: ModelObjectType, notFoundSchemaId: string, errorKey?: string) {
	return createError(
		errorKey ?? generateId(IdPrefix.unknown),
		"modelErrors",
		{
			schemaType,
			schemaId,
			errorKey,
			message: `Missing: ${notFoundSchemaType} with ID ${notFoundSchemaId} not found`,
		}
	);
}

export function errorNoColumns(schemaType: ModelObjectType, schemaId: string, errorKey?: string) {
	return createError(
		errorKey ?? generateId(IdPrefix.unknown),
		"modelErrors",
		{
			schemaType,
			schemaId,
			errorKey,
			message: "Must have columns",
		}
	);
}

export function deleteSchemaError(schemaType: ModelObjectType, errorKey: string) {
	return deleteError(
		errorKey,
		"modelErrors",
	);
}

export function createJavascriptError(data: CodeError, errorKey: string) {
	return createError(
		errorKey,
		"javascriptErrors",
		data,
	);
}

export function deleteJavascriptError(key: string) {
	return deleteError(
		key,
		"javascriptErrors",
	);
}
