import { loadAppSettings, newModelState } from "@/actions";
import { ActionType, AppMode, ClearNotificationsActionData, CreateNotificationActionData, DeleteNotificationActionData, FunctionActionData, GeneralState, MessageAlertType, SetAppModeActionData, SetGeneralStateActionData, SetIsUnsavedActionData, SetProjectIdActionData } from "@/data";
import { IdPrefix, generateId } from "@/utils";

export function setGeneralState(data: GeneralState): SetGeneralStateActionData {
	return {
		type: ActionType.setGeneralState,
		payload: {
			data,
		},
	};
}

export function createNotification(alertType: MessageAlertType, title: string, message: string): CreateNotificationActionData {
	return {
		type: ActionType.createNotification,
		payload: {
			data: {
				id: generateId(IdPrefix.unknown),
				alertType,
				title,
				message,
			},
		},
	};
}

export function deleteNotification(id: string): DeleteNotificationActionData {
	return {
		type: ActionType.deleteNotification,
		payload: {
			id,
		},
	};
}

export function clearNotifications(): ClearNotificationsActionData {
	return {
		type: ActionType.clearNotifications,
	};
}

export function setCurrentAppDomain(appMode: AppMode): SetAppModeActionData {
	return {
		type: ActionType.setAppMode,
		payload: {
			appMode,
		},
	};
}

export function setProjectId(id: string): SetProjectIdActionData {
	return {
		type: ActionType.setProjectId,
		payload: {
			id,
		},
	};
}

export function setIsUnsaved(isUnsaved: boolean): SetIsUnsavedActionData {
	return {
		type: ActionType.setIsUnsaved,
		payload: {
			isUnsaved,
		},
	};
}

export function initializeApp(): FunctionActionData {
	return (dispatch) => {
		dispatch(newModelState(false));
		dispatch(loadAppSettings());
	};
}
