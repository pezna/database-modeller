import { setSettings } from "@/actions/setting-actions";
import { BackendRootState, FunctionActionData, MessageAlertType } from "@/data";
import { diagramService, fileService } from "@/services";
import { Dispatch } from "@/store";
import { IdPrefix, generateId } from "@/utils";
import { setBoardState } from "./board-actions";
import { setCodeState } from "./code-actions";
import { setExportState } from "./export-actions";
import { createNotification, setGeneralState } from "./general-actions";
import { setSchemaState } from "./schema-actions";
import { deselectBoard, setUiState } from "./ui-actions";

export function newModelState(copySchemaSettings: boolean): FunctionActionData {
	return async (dispatch) => {
		const state = await fileService.newModelState(
			generateId(IdPrefix.project),
			copySchemaSettings,
		);
		applyNewModelState(dispatch, state);
	};
}

export function saveModelState(newFile: boolean): FunctionActionData {
	return async (dispatch, getState) => {
		const state = getState();
		const rootState: BackendRootState = {
			schema: state.schema,
			board: state.board,
			export: state.export,
			code: state.code,
			general: state.general,
			ui: state.ui,
			settings: state.settings,
		};
		if (!rootState.general.projectId) {
			rootState.general.projectId = generateId(IdPrefix.project);
		}
		const filePath = rootState.general.modelFilePath ?? '';
		const res = await fileService.saveModellerFile(rootState, newFile ? undefined : filePath);
		
		if (res.ok) {
			dispatch(setGeneralState(res.ok.generalState));
		}
		else if (res.err && res.err.type !== 'userCancelled') {
			const message = res.err.type === 'noProjectId'
				? 'No project ID. This should never happen.'
				: res.err.data;
			dispatch(createNotification(
				MessageAlertType.error,
				"Failed Saving File",
				message,
			));
		}
	};
}

export function loadModelState(filePath: string | null): FunctionActionData {
	return async (dispatch) => {
		const res = await fileService.openModellerFile(filePath);
		
		if (res.ok) {
			diagramService.loadFromSaveData(res.ok.diagramData);
			applyNewModelState(dispatch, res.ok.rootState);
		}
		else if (res.err && res.err.type !== 'userCancelled') {
			dispatch(createNotification(
				MessageAlertType.error,
				'Could not load file',
				res.err.data,
			));
		}
	};
}

function applyNewModelState(dispatch: Dispatch, rootState: BackendRootState) {
	diagramService.clearAll();
	// First unset the model file path and project id (via the general state)
	// This is necessary because the ui state will change as different actions are applied
	// resulting in attempts to save the ui state to file.
	dispatch(deselectBoard(true));
	dispatch(setGeneralState(rootState.general));
	dispatch(setSettings(rootState.settings));
	dispatch(setUiState(rootState.ui));
	dispatch(setSchemaState(rootState.schema));
	dispatch(setBoardState(rootState.board));
	dispatch(setExportState(rootState.export));
	dispatch(setCodeState(rootState.code));
}
