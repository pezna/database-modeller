import { ActionType, AlterComponentPropertyActionData, AlterConnectorPropertyActionData, AlterMachinePropertyActionData, ComponentData, ConnectorData, CreateComponentActionData, CreateConnectorActionData, CreateMachineActionData, DeleteComponentActionData, DeleteConnectorActionData, DeleteMachineActionData, MachineData, MachineDataProperty } from "@/data";

export function createMachine(machine: MachineData, errorKey?: string): CreateMachineActionData {
	return {
		type: ActionType.createMachine,
		payload: {
			machine,
		},
		errorKey,
	};
}

export function deleteMachine(id: string, errorKey?: string): DeleteMachineActionData {
	return {
		type: ActionType.deleteMachine,
		payload: {
			id,
		},
		errorKey,
	};
}

export function alterMachineProperty<T extends MachineDataProperty>(machineId: string, propertyName: T, value: MachineData[T], errorKey?: string): AlterMachinePropertyActionData {
	return {
		type: ActionType.alterMachineProperty,
		payload: {
			machineId,
			propertyName,
			value,
		},
		errorKey,
	};
}

export function createComponent(component: ComponentData, errorKey?: string): CreateComponentActionData {
	return {
		type: ActionType.createComponent,
		payload: {
			component,
		},
		errorKey,
	};
}

export function deleteComponent(id: string, errorKey?: string): DeleteComponentActionData {
	return {
		type: ActionType.deleteComponent,
		payload: {
			id,
		},
		errorKey,
	};
}

export function alterComponentProperty<T extends ComponentData, K extends keyof T>(componentId: string, propertyName: K, value: T[K], errorKey?: string): AlterComponentPropertyActionData<T, K> {
	return {
		type: ActionType.alterComponentProperty,
		payload: {
			componentId,
			propertyName,
			value,
		},
		errorKey,
	};
}

export function createConnector(connector: ConnectorData, errorKey?: string): CreateConnectorActionData {
	return {
		type: ActionType.createConnector,
		payload: {
			connector,
		},
		errorKey,
	};
}

export function deleteConnector(id: string, errorKey?: string): DeleteConnectorActionData {
	return {
		type: ActionType.deleteConnector,
		payload: {
			id,
		},
		errorKey,
	};
}

export function alterConnectorProperty<T extends ConnectorData, K extends keyof T>(connectorId: string, propertyName: K, value: T[K], errorKey?: string): AlterConnectorPropertyActionData<T, K> {
	return {
		type: ActionType.alterConnectorProperty,
		payload: {
			connectorId,
			propertyName,
			value,
		},
		errorKey,
	};
}

