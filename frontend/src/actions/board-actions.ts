import { ActionType, AlterNotePropertyActionData, BoardState, CreateNoteActionData, DeleteNoteActionData, NoteData, SetBoardStateActionData, SetNotesActionData } from "@/data";
import { NoteDataProperty, NotesContainer } from "@/data/board";

export function setBoardState(data: BoardState): SetBoardStateActionData {
	return {
		type: ActionType.setBoardState,
		payload: {
			data,
		},
	};
}

export function createNote(note: NoteData): CreateNoteActionData {
	return {
		type: ActionType.createNote,
		payload: {
			note,
		},
	};
}

export function alterNoteProperty<T extends NoteDataProperty>(id: string, propertyName: T, value: NoteData[T]): AlterNotePropertyActionData {
	return {
		type: ActionType.alterNoteProperty,
		payload: {
			id,
			propertyName,
			value,
		},
	};
}

export function deleteNote(id: string): DeleteNoteActionData {
	return {
		type: ActionType.deleteNote,
		payload: {
			id,
		},
	};
}

export function setNotes(notes: NotesContainer): SetNotesActionData {
	return {
		type: ActionType.setNotes,
		payload: {
			notes,
		},
	};
}
