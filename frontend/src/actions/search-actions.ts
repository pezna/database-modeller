import { ActionType, DatabaseSearchResultItem, FunctionActionData, MachineSearchResultItem, SearchParameters, SearchResults, SetSearchParametersActionData, SetSearchResultsActionData } from "@/data";
import { getSelectedNamespaceId } from "@/selectors";
import { escapeRegex } from "@/utils";

export function setSearchParameters(parameters: SearchParameters): SetSearchParametersActionData {
	return {
		type: ActionType.setSearchParameters,
		payload: {
			parameters,
		},
	};
}

export function setSearchResults(results: SearchResults): SetSearchResultsActionData {
	return {
		type: ActionType.setSearchResults,
		payload: {
			results,
		},
	};
}

export function performSearch(): FunctionActionData {
	return async function(dispatch, getState) {
		const { schema, search, machine } = getState();
		const params = search.parameters;
		const namespaceId = params.currentNamespaceOnly ? getSelectedNamespaceId(getState()) : null;
		
		let re: RegExp;
		try {
			re = new RegExp(
				params.isRegex ? params.searchText : escapeRegex(params.searchText),
				params.isCaseSensitive ? undefined : 'i',
			);
		}
		catch (ex) {
			dispatch(setSearchResults({
				tables: [],
				columns: [],
				relationships: [],
				indexes: [],
				uniques: [],
				primaryKeys: [],
				machines: [],
				components: [],
				connectors: [],
			}));
			return;
		}
		
		const tables: DatabaseSearchResultItem[] = Object.values(schema.tables)
			.filter(t => t.namespaceId === namespaceId || !params.currentNamespaceOnly)
			.filter(t => t.name.match(re))
			.map(t => ({
				id: t.id,
				tableId: t.id,
				text: t.name,
				type: "tables",
			}));
		const columns: DatabaseSearchResultItem[] = Object.values(schema.columns)
			.filter(x => schema.tables[x.tableId].namespaceId === namespaceId || !params.currentNamespaceOnly)
			.filter(x => x.name.match(re))
			.map(x => ({
				id: x.id,
				tableId: x.tableId,
				text: x.name,
				type: "columns",
			}));
		const relationships: DatabaseSearchResultItem[] = Object.values(schema.relationships)
			.filter(x => schema.tables[x.childTableId].namespaceId === namespaceId || !params.currentNamespaceOnly)
			.filter(x => x.name.match(re))
			.map(x => ({
				id: x.id,
				tableId: x.childTableId,
				text: x.name,
				type: "relationships",
			}));
		const indexes: DatabaseSearchResultItem[] = Object.values(schema.indexes)
			.filter(x => schema.tables[x.tableId].namespaceId === namespaceId || !params.currentNamespaceOnly)
			.filter(x => x.name.match(re))
			.map(x => ({
				id: x.id,
				tableId: x.tableId,
				text: x.name,
				type: "indexes",
			}));
		const uniques: DatabaseSearchResultItem[] = Object.values(schema.uniques)
			.filter(x => schema.tables[x.tableId].namespaceId === namespaceId || !params.currentNamespaceOnly)
			.filter(x => x.name.match(re))
			.map(x => ({
				id: x.id,
				tableId: x.tableId,
				text: x.name,
				type: "uniques",
			}));
		const primaryKeys: DatabaseSearchResultItem[] = Object.values(schema.primaryKeys)
			.filter(x => schema.tables[x.tableId].namespaceId === namespaceId || !params.currentNamespaceOnly)
			.filter(x => x.name.match(re))
			.map(x => ({
				id: x.id,
				tableId: x.tableId,
				text: x.name,
				type: "primaryKeys",
			}));
		const machines: MachineSearchResultItem[] = Object.values(machine.machines)
			.filter(m => m.name.match(re))
			.map(m => ({
				id: m.id,
				machineId: m.id,
				text: m.name,
				type: "machines",
			}));
		const components: MachineSearchResultItem[] = Object.values(machine.components)
			.filter(x => x.name.match(re))
			.map(x => ({
				id: x.id,
				machineId: x.machineId,
				text: x.name,
				type: "components",
			}));
		const connectors: MachineSearchResultItem[] = Object.values(machine.connectors)
			.filter(x => x.name.match(re))
			.map(x => ({
				id: x.id,
				machineId: x.machineId,
				text: x.name,
				type: "connectors",
			}));
		
		dispatch(setSearchResults({
			tables,
			columns,
			relationships,
			indexes,
			uniques,
			primaryKeys,
			machines,
			components,
			connectors,
		}));
	};
}
