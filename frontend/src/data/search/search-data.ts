export interface SearchResults {
	tables: DatabaseSearchResultItem[];
	columns: DatabaseSearchResultItem[];
	relationships: DatabaseSearchResultItem[];
	indexes: DatabaseSearchResultItem[];
	uniques: DatabaseSearchResultItem[];
	primaryKeys: DatabaseSearchResultItem[];
	machines: MachineSearchResultItem[];
	components: MachineSearchResultItem[];
	connectors: MachineSearchResultItem[];
}

export interface SearchResultItem {
	id: string;
	text: string;
	type: keyof SearchResults;
}

export interface DatabaseSearchResultItem extends SearchResultItem {
	tableId: string;
	type: "tables" | "columns" | "relationships" | "indexes" | "uniques" | "primaryKeys";
}

export interface MachineSearchResultItem extends SearchResultItem {
	machineId: string;
	type: "machines" | "components" | "connectors";
}
