import { ComponentData } from "@/data/machine/component-data";
import { AngleUnit, ComponentType } from "@/data/machine/component-enums";
import { IdPrefix, generateId } from "@/utils";

export interface LSystemComponentData extends ComponentData {
	type: ComponentType.lsystem;
	rules: Record<string, string>;
	maxIterations: number;
	rotation: number; // delta
	rotationUnit: AngleUnit;
	initialPosition: [number, number]; // pos
	initialDirection: number; // theta
	initialDirectionUnit: AngleUnit;
	stepDistance: number;
}

export function newLSystemData(machineId: string): LSystemComponentData {
	return {
		id: generateId(IdPrefix.lsystem),
		name: "L-System",
		type: ComponentType.lsystem,
		machineId,
		connectorIds: [],
		rules: {
			s: "[f-f-f-f-f-f-f-f]",
			f: "f---f+f+f+f+f+f+f---f",
		},
		maxIterations: 5,
		rotation: Math.PI / 4,
		rotationUnit: AngleUnit.radians,
		initialPosition: [0, 0],
		initialDirection: 0,
		initialDirectionUnit: AngleUnit.degrees,
		stepDistance: 1,
	};
}
