export enum ConnectorType {
	channel = "channel",
	signal = "signal",
}

export const connectorTypeDisplayName: {[K in ConnectorType]: string} = {
	[ConnectorType.channel]: "Channel",
	[ConnectorType.signal]: "Signal",
};
