import { ComponentData } from "@/data/machine/component-data";
import { ComponentType } from "@/data/machine/component-enums";
import { SignalType } from "@/data/machine/signal-enums";
import { generateId, IdPrefix } from "@/utils";

export interface BucketComponentData extends ComponentData {
	type: ComponentType.bucket;
	maxAmount: number;
}

export function newBucketData(machineId: string): BucketComponentData {
	return {
		id: generateId(IdPrefix.bucket),
		name: "Bucket",
		type: ComponentType.bucket,
		machineId,
		connectorIds: [],
		maxAmount: -1,
	};
}

export const bucketSignalTypes: SignalType[] = [
	SignalType.onFull,
	SignalType.onReceive,
];