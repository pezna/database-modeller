import { ComponentData } from "@/data/machine/component-data";
import { ComponentType, IntervalFrequency } from "@/data/machine/component-enums";
import { IdPrefix, generateId } from "@/utils";

export interface ProducerComponentData extends ComponentData {
	type: ComponentType.producer;
	intervalFrequency: IntervalFrequency;
	intervalGap: number;
	amountProduced: number;
}

export function newProducerData(machineId: string): ProducerComponentData {
	return {
		id: generateId(IdPrefix.producer),
		name: "Producer",
		type: ComponentType.producer,
		machineId,
		connectorIds: [],
		intervalFrequency: IntervalFrequency.second,
		intervalGap: 1,
		amountProduced: 1,
	};
}
