import { IdPrefix, generateId } from "@/utils";

export interface MachineData {
	id: string;
	name: string;
	componentIds: string[];
	connectorIds: string[];
}

export type MachineDataProperty = keyof MachineData;

export function newMachineData(): MachineData {
	return {
		id: generateId(IdPrefix.machine),
		name: "New Machine",
		componentIds: [],
		connectorIds: [],
	};
}
