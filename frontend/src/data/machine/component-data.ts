import { ComponentType } from "@/data/machine/component-enums";

export interface ComponentData {
	id: string;
	name: string;
	type: ComponentType;
	machineId: string;
	connectorIds: string[];
}

export const defaultComponentSize = { width: 50, height: 50 };
