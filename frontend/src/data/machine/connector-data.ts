import { ConnectorType } from "@/data/machine/connector-enums";
import { IdPrefix, generateId } from "@/utils";

export interface ConnectorData {
	id: string;
	type: ConnectorType;
	name: string;
	machineId: string;
	sourceComponentId: string;
	destComponentId: string;
}

export interface ChannelConnectorData extends ConnectorData {
	type: ConnectorType.channel;
}

export function newChannelConnectorData(machineId: string, sourceComponentId: string, destComponentId: string): ChannelConnectorData {
	return {
		id: generateId(IdPrefix.channel),
		type: ConnectorType.channel,
		name: "Channel",
		machineId,
		sourceComponentId,
		destComponentId,
	};
}

export interface SignalConnectorData extends ConnectorData {
	type: ConnectorType.signal;
}

export function newSignalConnectorData(machineId: string, sourceComponentId: string, destComponentId: string): SignalConnectorData {
	return {
		id: generateId(IdPrefix.signal),
		type: ConnectorType.signal,
		name: "Signal",
		machineId,
		sourceComponentId,
		destComponentId,
	};
}
