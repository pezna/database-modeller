export enum ComponentType {
	producer = "producer",
	bucket = "bucket",
	destroyer = "destroyer",
	converter = "converter",
	lsystem = "lsystem",
}

export const componentTypeDisplayName: {[K in ComponentType]: string} = {
	[ComponentType.producer]: "Producer",
	[ComponentType.bucket]: "Bucket",
	[ComponentType.destroyer]: "Destroyer",
	[ComponentType.converter]: "Converter",
	[ComponentType.lsystem]: "L-System",
};

export enum IntervalFrequency {
	millisecond = "millisecond",
	second = "second",
	minute = "minute",
	hour = "hour",
}

export const intervalFrequencySelectData = Object.values(IntervalFrequency).map(x => ({
	label: x,
	value: x,
}));

export enum AngleUnit {
	degrees = "degrees",
	radians = "radians",
}

export const angleUnitSelectData = Object.values(AngleUnit).map(x => ({
	label: x,
	value: x,
}));
