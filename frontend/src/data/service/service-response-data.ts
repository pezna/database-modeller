export interface ServiceCallResponse {
	status: ServiceResponseStatus;
	statusMessage?: string;
}

export enum ServiceResponseStatus {
	success = "success",
	fail = "fail",
}
