import { ServiceCallResponse } from "./service-response-data";

export interface MigrationResult {
	sql: string;
	filePath?: string;
}

export interface GenerateSqlMigrationServiceResponse extends ServiceCallResponse {
	time: number;
	forwardResult?: MigrationResult;
	backwardResult?: MigrationResult;
}
