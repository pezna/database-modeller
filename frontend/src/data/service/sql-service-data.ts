import { SchemaUseItem } from "@/data/backend";
import { ServiceCallResponse } from "@/data/service";

export interface FindSchemaUsesServiceResponse extends ServiceCallResponse {
	uses: SchemaUseItem[][];
}
