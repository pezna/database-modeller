import { CustomExport, SchemaExportItem, SqlExportHistoryItem, SqlExportType, TableExportItem } from "@/data/backend";
import { MODEL_FILE_FOLDER_TEMPLATE } from "@/data/general";
import { RootState } from "@/data/state";
import { IdPrefix, generateId } from "@/utils";

export interface SqlGeneratorResultData {
	sql: string;
}

export function newCustomExport(): CustomExport {
	return {
		id: generateId(IdPrefix.export),
		name: "New Export",
		baseExportDirectory: MODEL_FILE_FOLDER_TEMPLATE,
		tableExports: [],
		fullSchemaExports: [],
	};
}

export function newTableExportItem(exportId: string): TableExportItem {
	return {
		id: `${exportId}${Math.random().toString(32).substring(2)}`,
		name: "",
		code: `
function getFileName() {
	return \`\${table.name}.json\`;
}

function generate() {
	return JSON.stringify(table, undefined, 4);
}
`,
		includedTableIds: [],
		excludedTableIds: [],
	};
}

export function newSchemaExportItem(exportId: string): SchemaExportItem {
	return {
		id: `${exportId}${Math.random().toString(32).substring(2)}`,
		name: "",
		code: `
function getFileName() {
	return '';
}

function generate() {
	return '';
}
`,
	};
}

export function newSqlExportHistory(type: SqlExportType, time: number, parentId: string, state: RootState): SqlExportHistoryItem {
	const {
		columns,
		columnTypes,
		indexes,
		namespaces,
		primaryKeys,
		relationships,
		tables,
		uniques,
		settings,
	} = state.schema;
	
	return {
		id: generateId(IdPrefix.export),
		name: '',
		time,
		parentId,
		type,
		columns,
		columnTypes,
		indexes,
		namespaces,
		primaryKeys,
		relationships,
		tables,
		uniques,
		settings,
	};
}
