import { SqlExportType } from "@/data/backend";

export const sqlExportDisplayName = {
	[SqlExportType.sqlite]: "SQLite",
	[SqlExportType.postgres]: "Postgres",
	[SqlExportType.mysql]: "MySQL",
};

export const sqlExportSelectData = Object.entries(SqlExportType).map(([k, v]) => ({
	label: k,
	value: v,
}));
