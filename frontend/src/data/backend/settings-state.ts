// This file was generated by [ts-rs](https://github.com/Aleph-Alpha/ts-rs). Do not edit this file manually.

export type SettingsState = {
  general: {
    files: { mostRecentModelFiles: Array<string> };
    startUp: { reopenLastModelFile: boolean };
  };
  databaseBoard: {
    table: {
      showKeys: boolean;
      backgroundColor: string;
      foregroundColor: string;
      borderColor: string;
    };
    column: {
      showType: boolean;
      stylePrimaryKeys: string;
      styleForeignKeys: string;
      indicatePrimaryKey: boolean;
      indicateForeignKey: boolean;
      indicateNotNull: boolean;
      indicateUnique: boolean;
      indicateIndex: boolean;
    };
    relationships: {
      showLines: boolean;
      lineColor: string;
      notExportedLineColor: string;
      lineWidth: number;
      lineConnectLocation: string;
      showOtherNamespaceTableName: boolean;
    };
    mixins: { mixinBackgroundColor: string; applyColorToColumns: boolean };
  };
  machineBoard: {
    components: { backgroundColor: string; foregroundColor: string };
    connectors: { lineColor: string; lineWidth: number };
  };
};
