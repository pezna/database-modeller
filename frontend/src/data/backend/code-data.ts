// This file was generated by [ts-rs](https://github.com/Aleph-Alpha/ts-rs). Do not edit this file manually.
import type { ItemId } from "./utils";

export type CodeContainer = {
  id: ItemId;
  code: string;
  type: CodeContainerType;
};

export enum CodeContainerType {
  schemaItemName = "schemaItemName",
  relationshipName = "relationshipName",
  perTableGenerator = "perTableGenerator",
  perSchemaGenerator = "perSchemaGenerator",
  structOutput = "structOutput",
}
