import { AppMode } from "@/data/backend";

export enum BoardTool {
	createTable = "createTable",
	createDatabaseNote = "createDatabaseNote",
	createRelationship = "createRelationship",
	createProducer = "createProducer",
	createBucket = "createBucket",
	createChannel = "createChannel",
	createSignal = "createSignal",
	createMachineNote = "createMachineNote",
	createLSystem = "createLSystem",
}

export function isToolConnector(tool: BoardTool | null): tool is BoardTool {
	return tool === BoardTool.createChannel || tool === BoardTool.createSignal;
}

export const boardToolDisplayName: {[key in BoardTool]: string} = {
	[BoardTool.createTable]: "Create Table",
	[BoardTool.createDatabaseNote]: "Create Note",
	[BoardTool.createRelationship]: "Create Relationship",
	[BoardTool.createProducer]: "Create Producer",
	[BoardTool.createMachineNote]: "Create Note",
	[BoardTool.createBucket]: "Create Bucket",
	[BoardTool.createChannel]: "Create Channel",
	[BoardTool.createSignal]: "Create Signal",
	[BoardTool.createLSystem]: "Create L-System",
};

export const boardToolAppMode: {[key in BoardTool]: AppMode} = {
	[BoardTool.createTable]: AppMode.database,
	[BoardTool.createDatabaseNote]: AppMode.database,
	[BoardTool.createRelationship]: AppMode.database,
	[BoardTool.createProducer]: AppMode.machine,
	[BoardTool.createMachineNote]: AppMode.machine,
	[BoardTool.createBucket]: AppMode.machine,
	[BoardTool.createChannel]: AppMode.machine,
	[BoardTool.createSignal]: AppMode.machine,
	[BoardTool.createLSystem]: AppMode.machine,
};
