import { DimensionsData } from "@/data/board/board-data";

export function createDimensionsObject(x: number, y: number, width: number, height: number): DimensionsData {
	return {
		x,
		y,
		width,
		height,
	};
}
