import { AppMode, NoteData } from "@/data";
import { IdPrefix, generateId } from "@/utils";

export type NoteDataProperty = keyof NoteData;

export type NotesContainer = {[id: string]: NoteData};

export function newNoteData(appMode: AppMode, sectionId: string): NoteData {
	return {
		id: generateId(IdPrefix.note),
		appMode,
		sectionId,
		text: "",
	};
}
