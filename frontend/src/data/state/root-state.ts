import { BoardState, CodeState, ExportState, GeneralState, SchemaState, SettingsState, UiState } from "@/data/backend";
import { ErrorState } from "./error-state";
import { MachineState } from "./machine-state";
import { SearchState } from "./search-state";


export interface RootState {
	board: BoardState;
	schema: SchemaState;
	errors: ErrorState;
	settings: SettingsState;
	export: ExportState;
	ui: UiState;
	search: SearchState;
	code: CodeState;
	general: GeneralState;
	machine: MachineState;
}
