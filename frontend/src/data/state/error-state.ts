import { CodeError } from "@/data/backend";
import { GenericErrorData, ModelItemErrorData } from "@/data/error";

export interface ErrorState {
	modelErrors: Record<string, ModelItemErrorData[]>;
	javascriptErrors: Record<string, CodeError[]>;
	structOutputErrors: Record<string, GenericErrorData[]>;
}

export function defaultErrorState(): ErrorState {
	return {
		modelErrors: {},
		javascriptErrors: {},
		structOutputErrors: {},
	};
}