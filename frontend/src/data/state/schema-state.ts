import { SchemaState, SqlExportHistoryItem } from "@/data/backend";
import { ModelObjectType } from "@/data/general";
import { defaultSchemaSettings } from "@/data/schema";

export const allSchemaItemKeys = ['tables', 'columns', 'columnTypes', 'relationships', 'indexes', 'uniques', 'primaryKeys', 'namespaces'] as const;

export type SchemaItemKey = Exclude<keyof SchemaState, 'settings'>

export type SimpleSchemaState = Pick<SchemaState, SchemaItemKey>

type X = Exclude<SchemaItemKey, typeof allSchemaItemKeys[number]> extends never
	? 'all-values-are-used'
	: never
const _x: X = 'all-values-are-used';

export function createSimpleSchemaState(state: SchemaState | SqlExportHistoryItem | SimpleSchemaState): SimpleSchemaState {
	return {
		tables: state.tables,
		columns: state.columns,
		columnTypes: state.columnTypes,
		primaryKeys: state.primaryKeys,
		relationships: state.relationships,
		indexes: state.indexes,
		uniques: state.uniques,
		namespaces: state.namespaces,
	};
}

export function getSchemaItemName(type: SchemaItemKey) {
	switch (type) {
		case 'tables':
			return 'table';
		case 'columns':
			return 'column';
		case 'columnTypes':
			return 'column type';
		case 'relationships':
			return 'relationship';
		case 'indexes':
			return 'index';
		case 'uniques':
			return 'unique';
		case 'primaryKeys':
			return 'primary key';
		case 'namespaces':
			return 'namespace';
		default:
			throw new Error(`could not get schema name for "${type}"`);
	}
}

export function getModelObjectType(type: SchemaItemKey): ModelObjectType {
	switch (type) {
		case 'tables':
			return ModelObjectType.table;
		case 'columns':
			return ModelObjectType.column;
		case 'columnTypes':
			return ModelObjectType.columnType;
		case 'relationships':
			return ModelObjectType.relationship;
		case 'indexes':
			return ModelObjectType.index;
		case 'uniques':
			return ModelObjectType.unique;
		case 'primaryKeys':
			return ModelObjectType.primaryKey;
		case 'namespaces':
			return ModelObjectType.namespace;
		default:
			throw new Error(`could not get model object type for "${type}"`);
	}
}

export function defaultSchemaState(): SchemaState {
	return {
		tables: {},
		columns: {},
		columnTypes: {},
		relationships: {},
		indexes: {},
		uniques: {},
		primaryKeys: {},
		namespaces: {},
		settings: defaultSchemaSettings(),
	};
}
