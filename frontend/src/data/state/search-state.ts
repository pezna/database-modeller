import { SearchResults } from "../search";

export interface SearchState {
	parameters: SearchParameters;
	results: SearchResults;
}

export interface SearchParameters {
	searchText: string;
	isRegex: boolean;
	isCaseSensitive: boolean;
	currentNamespaceOnly: boolean;
}
