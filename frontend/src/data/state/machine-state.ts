import { ComponentData, ConnectorData, MachineData } from "../machine";

export interface MachineState {
	machines: {[id: string]: MachineData};
	components: {[id: string]: ComponentData};
	connectors: {[id: string]: ConnectorData};
}

export function defaultMachineState(): MachineState {
	return {
		machines: {},
		components: {},
		connectors: {},
	};
}
