import { SettingsState } from "@/data/backend";
import { RelationshipConnectLocation, TextFormattingOption } from "@/data/general";
import { ArrayObjectPaths, ObjectPaths, SettingsPathDepth } from "../../utils";

export type SettingsStatePath = ObjectPaths<SettingsState, SettingsPathDepth>
export type SettingsStateArrayPath = ArrayObjectPaths<SettingsState, SettingsPathDepth>

export function getDefaultSettingsState(): SettingsState {
	return {
		general: {
			files: {
				mostRecentModelFiles: [],
			},
			startUp: {
				reopenLastModelFile: true,
			},
		},
		databaseBoard: {
			table: {
				showKeys: true,
				backgroundColor: "#ffffff",
				foregroundColor: "#171717",
				borderColor: "#111111",
			},
			column: {
				showType: false,
				stylePrimaryKeys: TextFormattingOption.none,
				styleForeignKeys: TextFormattingOption.none,
				indicatePrimaryKey: true,
				indicateForeignKey: true,
				indicateNotNull: true,
				indicateUnique: true,
				indicateIndex: true,
			},
			relationships: {
				showLines: true,
				lineColor: "#ff0000",
				notExportedLineColor: "#ff00ff",
				lineWidth: 2,
				lineConnectLocation: RelationshipConnectLocation.column,
				showOtherNamespaceTableName: true,
			},
			mixins: {
				mixinBackgroundColor: "#cbe8fb",
				applyColorToColumns: true,
			},
		},
		machineBoard: {
			components: {
				backgroundColor: "#ffffff",
				foregroundColor: "#2e2ed1",
			},
			connectors: {
				lineColor: "#5e6097",
				lineWidth: 2,
			},
		},
	};
}
