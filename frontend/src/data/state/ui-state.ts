import { UiState } from "@/data/backend";
import { ExportTab, ManageSchemaTab, SettingsTab } from "@/data/general";

export function defaultUiState(): UiState {
	return {
		boardSelection: {
			selectedTableId: null,
			selectedColumnId: null,
			selectedRelationshipId: null,
			selectedNoteId: null,
			selectedNamespaceId: null,
			selectedMachineId: null,
			selectedComponentId: null,
			selectedConnectorId: null,
		},
		tableSidebar: {
			scrollAmount: 0,
			isColumnGroupExpanded: false,
			isPrimaryKeyExpanded: false,
			isForeignKeyGroupExpanded: false,
			isIndexGroupExpanded: false,
			isUniqueGroupExpanded: false,
			isMixinGroupExpanded: false,
			isMixinGroupUsagesExpanded: false,
		},
		columnSidebar: {
			scrollAmount: 0,
			isForeignKeyGroupExpanded: false,
			isIndexGroupExpanded: false,
			isUniqueGroupExpanded: false,
		},
		relationshipSidebar: {
			scrollAmount: 0,
		},
		settingsSidebar: {
			generalSettingsExpanded: false,
			databaseBoardSettingsExpanded: false,
			schemaSchemaSettingsExpanded: false,
			filesSchemaSettingsExpanded: false,
			machineBoardSettingsExpanded: false,
			exportSchemaSettingsExpanded: false,
			activeTab: SettingsTab.appSettings,
			appSettingsScrollAmount: 0,
			schemaSettingsScrollAmount: 0,
		},
		manageSchemaSidebar: {
			activeTab: ManageSchemaTab.tables,
			openTableIds: [],
			openColumnTypeIds: [],
			openMachineIds: [],
			searchScrollAmount: 0,
			typesScrollAmount: 0,
		},
		consoleSidebar: {
			scrollAmount: 0,
		},
		exportSidebar: {
			activeTab: ExportTab.exportHistory,
			historyScrollAmount: 0,
			historyOpenExportIds: [],
			templatesScrollAmount: 0,
			templatesOpenExportId: "",
		},
		codeSidebar: {
			scrollAmount: 0,
			activeDocumentId: "",
			splitPanelDividerPosition: 75,
			expandedTreeIds: [],
		},
		namespaceSidebar: {
			scrollAmount: 0,
		},
		generalAppUi: {
			optionsSidebar: {
				visibleSection: null,
				sidebarWidth: 0,
			},
			propertySidebar: {
				visibleSection: null,
				sidebarWidth: 0,
			},
		},
		databaseModeUi: {
			openNamespaceTabs: [],
			namespaceListScrollAmount: 0,
		},
		machineModeUi: {
			openMachineTabs: [],
			machineListScrollAmount: 0,
		},
		machineSidebar: {
			scrollAmount: 0,
		},
		componentSidebar: {
			producerScrollAmount: 0,
			bucketScrollAmount: 0,
			destroyerScrollAmount: 0,
			converterScrollAmount: 0,
			lsystemScrollAmount: 0,
		},
		connectorSidebar: {
			channelScrollAmount: 0,
			signalScrollAmount: 0,
		},
	};
}
