import { ColumnIndexDirection, UniqueData } from "@/data/backend";
import { generateId, IdPrefix } from "@/utils";

export type UniqueDataProperty = keyof UniqueData

export function newUniqueData(tableId: string, columnId?: string): UniqueData {
	return {
		id: generateId(IdPrefix.unique),
		name: '',
		isIndex: false,
		tableId,
		columnIds: columnId ? [columnId] : [],
		directions: columnId ? [ColumnIndexDirection.ascending] : [],
	};
}