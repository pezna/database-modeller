import { ColumnData, ColumnTypeData } from "@/data/backend";
import { generateId, IdPrefix } from "@/utils";

export type ColumnDataProperty = keyof ColumnData;

export type ColumnTypeUsageData = {
	columnTypeId: string;
	namespaceId: string;
	namespaceName: string;
	tableId: string;
	tableName: string;
	columnId: string;
	columnName: string;
}

export function newColumnData(tableId: string): ColumnData {
	return {
		id: generateId(IdPrefix.column),
		tableId,
		name: 'Column',
		typeId: '',
		options: {},
	};
}

export function newColumnTypeData(): ColumnTypeData {
	return {
		id: generateId(IdPrefix.columnType),
		name: '',
		languages: [],
		columnIds: [],
	};
}
