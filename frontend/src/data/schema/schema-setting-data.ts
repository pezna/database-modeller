import { SchemaSettingsData, SqlExportType } from "@/data/backend";
import { ArrayObjectLeaves, ObjectPaths, SettingsPathDepth } from "../../utils";

export type SchemaSettingsDataPath = ObjectPaths<SchemaSettingsData, SettingsPathDepth>

export function defaultSchemaSettings(): SchemaSettingsData {
	const defaultGeneratorCodes = {
		primaryKeyNameGeneratorCode: 'function generate() {\n' +
			'\treturn `pk_${table.name}_${columns.map(c => c.name).join("_")}`;\n' +
		'}',
		relationshipNameGeneratorCode: 'function generate() {\n' +
			'\treturn `fk_${childTable.name}_${childColumns.map(c => c.name).join("_")}_${parentTable.name}`;\n' +
		'}',
		indexNameGeneratorCode: 'function generate() {\n' +
			'\treturn `ix_${table.name}_${columns.map(c => c.name).join("_")}`;\n' +
		'}',
		uniqueNameGeneratorCode: 'function generate() {\n' +
			'\treturn `uq_${table.name}_${columns.map(c => c.name).join("_")}`;\n' +
		'}',
	};
	
	return {
		schema: {
			mixinTables: {
				defaultMixinTableIds: [],
			},
			generatorCode: {
				...defaultGeneratorCodes,
			},
		},
		files: {
			saveFiles: {
				useSeparateSaveFiles: false,
			},
		},
		export: {
			sql: {
				type: SqlExportType.sqlite,
				fileNameTemplate: "{type}_{datetime}",
				separateDirectionFolders: false,
				forwardFolderPath: "",
				backwardFolderPath: "",
			},
		},
	};
}

export const schemaSpecificSettingsPath: ArrayObjectLeaves<SchemaSettingsData, SettingsPathDepth>[] = [
	["schema", "mixinTables", "defaultMixinTableIds"],
];
