import { ColumnIndexDirection, PrimaryKeyData } from "@/data/backend";
import { generateId, IdPrefix } from "@/utils";

export type PrimaryKeyDataProperty = keyof PrimaryKeyData

export function newPrimaryKey(tableId: string, columnId?: string): PrimaryKeyData {
	return {
		id: generateId(IdPrefix.primaryKey),
		name: '',
		tableId,
		columnIds: columnId ? [columnId] : [],
		directions: columnId ? [ColumnIndexDirection.ascending] : [],
	};
}
