import { ColumnIndexDirection, ColumnOptionType, ConflictClause, ForeignKeyChangeAction, ForeignKeyDeferrability } from "@/data/backend";

export const columnOptionTypeDisplayName: {[key in ColumnOptionType]: string} = {
	[ColumnOptionType.defaultValue]: "Default Value",
	[ColumnOptionType.index]: "Index",
	[ColumnOptionType.notNull]: "Not Null",
	[ColumnOptionType.unique]: "Unique",
};

const columnIndexDirectionDisplayName: {[key in ColumnIndexDirection]: string} = {
	[ColumnIndexDirection.ascending]: "Ascending",
	[ColumnIndexDirection.descending]: "Descending",
};

export const columnIndexDirectionSelectData = Object.entries(columnIndexDirectionDisplayName).map(([value, label]) => ({
	value,
	label,
}));

const foreignKeyChangeActionDisplayName: {[key in ForeignKeyChangeAction]: string} = {
	[ForeignKeyChangeAction.setNull]: 'Set Null',
	[ForeignKeyChangeAction.setDefault]: 'Set Default',
	[ForeignKeyChangeAction.cascade]: 'Cascade',
	[ForeignKeyChangeAction.restrict]: 'Restrict',
	[ForeignKeyChangeAction.noAction]: 'No Action',
};

export const foreignKeyChangeActionSelectData = Object.entries(foreignKeyChangeActionDisplayName).map(([value, label]) => ({
	value,
	label,
}));

const foreignKeyDeferrabilityDisplayName: {[key in ForeignKeyDeferrability]: string} = {
	[ForeignKeyDeferrability.notDeferrable]: 'Not Deferrable',
	[ForeignKeyDeferrability.initiallyDeferred]: 'Initially Deferred',
	[ForeignKeyDeferrability.initiallyImmediate]: 'Initially Immediate',
};

export const foreignKeyDeferrabilitySelectData = Object.entries(foreignKeyDeferrabilityDisplayName).map(([value, label]) => ({
	value,
	label,
}));

const conflictClauseDisplayName: {[key in ConflictClause]: string} = {
	[ConflictClause.rollback]: 'Rollback',
	[ConflictClause.abort]: 'Abort',
	[ConflictClause.fail]: 'Fail',
	[ConflictClause.ignore]: 'Ignore',
	[ConflictClause.replace]: 'Replace',
};

export const conflictClauseSelectData = Object.entries(conflictClauseDisplayName).map(([value, label]) => ({
	value,
	label,
}));
