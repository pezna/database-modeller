import { RelationshipData } from "@/data/backend";
import { generateId, IdPrefix } from "@/utils";

export type RelationshipDataProperty = keyof RelationshipData

export function newRelationshipData(childTableId: string, childColumnIds: string[], parentTableId: string, parentColumnIds: string[]): RelationshipData {
	return {
		id: generateId(IdPrefix.relationship),
		name: '',
		childTableId,
		childColumnIds,
		parentTableId,
		parentColumnIds,
	};
}
