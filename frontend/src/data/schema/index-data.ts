import { ColumnIndexDirection, IndexData } from "@/data/backend";
import { generateId, IdPrefix } from "@/utils";

export type IndexDataProperty = keyof IndexData

export function newIndexData(tableId: string, columnId?: string): IndexData {
	return {
		id: generateId(IdPrefix.index),
		name: '',
		tableId,
		columnIds: columnId ? [columnId] : [],
		directions: columnId ? [ColumnIndexDirection.ascending] : [],
	};
}