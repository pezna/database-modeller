import { NamespaceData } from "@/data/backend";
import { generateId, IdPrefix } from "@/utils";

export type NamespaceDataProperty = keyof NamespaceData

export function newNamespaceData(name: string | null): NamespaceData {
	return {
		id: generateId(IdPrefix.namespace),
		name: name ?? 'Namespace',
	};
}