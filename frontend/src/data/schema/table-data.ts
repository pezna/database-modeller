import { TableData } from "@/data/backend";
import { generateId, IdPrefix } from "@/utils";

export type TableDataProperty = keyof TableData

export function newTableData(namespaceId: string): TableData {
	const id = generateId(IdPrefix.table);
	return {
		id,
		name: 'Table',
		columnIds: [],
		relationshipIds: [],
		primaryKeyId: null,
		isMixin: false,
		mixinTableIds: [],
		namespaceId,
	};
}
