import { CodeState, SqlDocumentData, SqlDocumentSchemaUsage, StructOutputData } from "@/data/backend";
import { ActionData } from "./action-data";
import { ActionType } from "./action-enums";

export interface SetCodeStateActionData extends ActionData {
	type: ActionType.setCodeState,
	payload: {
		data: CodeState;
	};
}

export interface SetSqlDocumentActionData extends ActionData {
	type: ActionType.setSqlDocument,
	payload: {
		data: SqlDocumentData;
	};
}

export interface DeleteSqlDocumentActionData extends ActionData {
	type: ActionType.deleteSqlDocument,
	payload: {
		id: string;
	};
}

export interface SetSqlSchemaUsesActionData extends ActionData {
	type: ActionType.setSqlSchemaUses,
	payload: {
		id: string;
		uses: SqlDocumentSchemaUsage[];
	};
}

export interface DeleteSqlSchemaUsesActionData extends ActionData {
	type: ActionType.deleteSqlSchemaUses,
	payload: {
		id: string;
	};
}

export interface SetStructOutputActionData extends ActionData {
	type: ActionType.setStructOutput,
	payload: {
		data: StructOutputData;
	};
}

export interface DeleteStructOutputActionData extends ActionData {
	type: ActionType.deleteStructOutput,
	payload: {
		id: string;
	};
}
