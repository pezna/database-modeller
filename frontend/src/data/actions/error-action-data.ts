import { ActionData, ActionType } from "@/data/actions";
import { ErrorState } from "@/data/state";

export interface CreateErrorActionData extends ActionData {
	type: ActionType.createError;
	payload: {
		errorGroup: keyof ErrorState;
		errorKey: string;
		data: ErrorState[keyof ErrorState][string][number];
	};
}

export interface DeleteErrorActionData extends ActionData {
	type: ActionType.deleteError;
	payload: {
		errorGroup: keyof ErrorState;
		errorKey: string;
	};
}
