import { ActionType } from "@/data/actions/action-enums";
import { SearchResults } from "../search";
import { SearchParameters } from "../state";
import { ActionData } from "./action-data";

export interface SetSearchParametersActionData extends ActionData {
	type: ActionType.setSearchParameters,
	payload: {
		parameters: SearchParameters;
	};
}

export interface SetSearchResultsActionData extends ActionData {
	type: ActionType.setSearchResults;
	payload: {
		results: SearchResults;
	};
}
