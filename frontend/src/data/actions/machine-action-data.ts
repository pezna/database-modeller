import { ActionType } from "@/data/actions/action-enums";
import { ComponentData, ConnectorData, MachineData, MachineDataProperty } from "../machine";
import { ActionData } from "./action-data";

export interface CreateMachineActionData extends ActionData {
	type: ActionType.createMachine;
	payload: {
		machine: MachineData;
	};
}

export interface DeleteMachineActionData extends ActionData {
	type: ActionType.deleteMachine;
	payload: {
		id: string;
	};
}

export interface AlterMachinePropertyActionData extends ActionData {
	type: ActionType.alterMachineProperty;
	payload: {
		machineId: string;
		propertyName: MachineDataProperty;
		value: any;
	};
}

export interface CreateComponentActionData extends ActionData {
	type: ActionType.createComponent;
	payload: {
		component: ComponentData;
	};
}

export interface DeleteComponentActionData extends ActionData {
	type: ActionType.deleteComponent;
	payload: {
		id: string;
	};
}

export interface AlterComponentPropertyActionData<T extends ComponentData, K extends keyof T = keyof T> extends ActionData {
	type: ActionType.alterComponentProperty;
	payload: {
		componentId: string;
		propertyName: K;
		value: T[K];
	};
}

export interface CreateConnectorActionData extends ActionData {
	type: ActionType.createConnector;
	payload: {
		connector: ConnectorData;
	};
}

export interface DeleteConnectorActionData extends ActionData {
	type: ActionType.deleteConnector;
	payload: {
		id: string;
	};
}

export interface AlterConnectorPropertyActionData<T extends ConnectorData, K extends keyof T> extends ActionData {
	type: ActionType.alterConnectorProperty;
	payload: {
		connectorId: string;
		propertyName: K;
		value: T[K];
	};
}

