import { ActionData, ActionType } from "@/data/actions";
import { BoardState, NoteData } from "@/data/backend";
import { NoteDataProperty, NotesContainer } from "@/data/board";

export interface SetBoardStateActionData extends ActionData {
	type: ActionType.setBoardState;
	payload: {
		data: BoardState;
	};
}

export interface CreateNoteActionData extends ActionData {
	type: ActionType.createNote;
	payload: {
		note: NoteData;
	};
}

export interface AlterNotePropertyActionData extends ActionData {
	type: ActionType.alterNoteProperty;
	payload: {
		id: string;
		propertyName: NoteDataProperty;
		value: any;
	};
}

export interface DeleteNoteActionData extends ActionData {
	type: ActionType.deleteNote;
	payload: {
		id: string;
	};
}

export interface SetNotesActionData extends ActionData {
	type: ActionType.setNotes;
	payload: {
		notes: NotesContainer;
	};
}
