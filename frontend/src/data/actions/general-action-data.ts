import { ActionData, ActionType } from "@/data/actions";
import { AppMode, GeneralState, NotificationData } from "@/data/backend";

export interface SetGeneralStateActionData extends ActionData {
	type: ActionType.setGeneralState;
	payload: {
		data: GeneralState;
	};
}

export interface CreateNotificationActionData extends ActionData {
	type: ActionType.createNotification;
	payload: {
		data: NotificationData;
	};
}

export interface DeleteNotificationActionData extends ActionData {
	type: ActionType.deleteNotification;
	payload: {
		id: string;
	};
}

export interface ClearNotificationsActionData extends ActionData {
	type: ActionType.clearNotifications;
}

export interface SetAppModeActionData extends ActionData {
	type: ActionType.setAppMode;
	payload: {
		appMode: AppMode;
	};
}

export interface SetProjectIdActionData extends ActionData {
	type: ActionType.setProjectId;
	payload: {
		id: string;
	};
}

export interface SetIsUnsavedActionData extends ActionData {
	type: ActionType.setIsUnsaved;
	payload: {
		isUnsaved: boolean;
	};
}
