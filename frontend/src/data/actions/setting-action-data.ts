import { ActionType } from "@/data/actions/action-enums";
import { SettingsState } from "@/data/backend";
import { ActionData } from "./action-data";


export interface AlterSettingsPropertyActionData extends ActionData {
	type: ActionType.alterSettingsProperty;
	payload: {
		path: string[];
		value: any;
	};
}

export interface SetSettingsActionData extends ActionData {
	type: ActionType.setSettings;
	payload: {
		settings: SettingsState;
	};
}
