import { ActionType } from "@/data/actions/action-enums";
import { ColumnData, ColumnOptionType, ColumnOptionValueContainer, ColumnTypeData, IndexData, NamespaceData, PrimaryKeyData, RelationshipData, SchemaSettingsData, TableData, UniqueData } from "@/data/backend";
import { ColumnDataProperty, IndexDataProperty, NamespaceDataProperty, PrimaryKeyDataProperty, RelationshipDataProperty, TableDataProperty, UniqueDataProperty } from "../schema";
import { SimpleSchemaState } from "../state";
import { ActionData } from "./action-data";

export interface SetSchemaStateActionData extends ActionData {
	type: ActionType.setSchemaState;
	payload: {
		schema: SimpleSchemaState;
	}
}

export interface CreateTableActionData extends ActionData {
	type: ActionType.createTable;
	payload: {
		table: TableData;
	};
}

export interface DeleteTableActionData extends ActionData {
	type: ActionType.deleteTable;
	payload: {
		id: string;
	};
}

export interface AlterTablePropertyActionData extends ActionData {
	type: ActionType.alterTableProperty;
	payload: {
		tableId: string;
		propertyName: TableDataProperty;
		value: any;
	};
}

export interface CreateColumnActionData extends ActionData {
	type: ActionType.createColumn;
	payload: {
		column: ColumnData;
	};
}

export interface DeleteColumnActionData extends ActionData {
	type: ActionType.deleteColumn;
	payload: {
		columnId: string;
	}
}

export interface AlterColumnPropertyActionData extends ActionData {
	type: ActionType.alterColumnProperty;
	payload: {
		columnId: string;
		propertyName: ColumnDataProperty;
		value: any;
	}
}

export interface AlterColumnOptionPropertyActionData<T extends ColumnOptionType> extends ActionData {
	type: ActionType.alterColumnOptionProperty;
	payload: {
		columnId: string;
		propertyName: T;
		value: ColumnOptionValueContainer[T];
	}
}

export interface SetColumnTypeActionData extends ActionData {
	type: ActionType.setColumnType;
	payload: {
		columnType: ColumnTypeData;
	};
}

export interface DeleteColumnTypeActionData extends ActionData {
	type: ActionType.deleteColumnType;
	payload: {
		columnTypeId: string;
	};
}

export interface ReplaceColumnTypeUsageActionData extends ActionData {
	type: ActionType.replaceColumnTypeUsage;
	payload: {
		columnTypeId: string;
		replacementColumnTypeId: string;
	};
}

export interface CreateRelationshipActionData extends ActionData {
	type: ActionType.createRelationship;
	payload: {
		relationship: RelationshipData;
	};
}

export interface AlterRelationshipPropertyActionData extends ActionData {
	type: ActionType.alterRelationshipProperty;
	payload: {
		id: string;
		propertyName: RelationshipDataProperty;
		value: any;
	};
}

export interface DeleteRelationshipActionData extends ActionData {
	type: ActionType.deleteRelationship;
	payload: {
		relationshipId: string;
	};
}

export interface CreateIndexActionData extends ActionData {
	type: ActionType.createIndex;
	payload: {
		index: IndexData;
	};
}

export interface AlterIndexPropertyActionData extends ActionData {
	type: ActionType.alterIndexProperty;
	payload: {
		id: string;
		propertyName: IndexDataProperty;
		value: any;
	};
}

export interface DeleteIndexActionData extends ActionData {
	type: ActionType.deleteIndex;
	payload: {
		id: string;
	};
}

export interface CreateUniqueActionData extends ActionData {
	type: ActionType.createUnique;
	payload: {
		unique: UniqueData;
	};
}

export interface AlterUniquePropertyActionData extends ActionData {
	type: ActionType.alterUniqueProperty;
	payload: {
		id: string;
		propertyName: UniqueDataProperty;
		value: any;
	};
}

export interface DeleteUniqueActionData extends ActionData {
	type: ActionType.deleteUnique;
	payload: {
		id: string;
	};
}

export interface CreatePrimaryKeyActionData extends ActionData {
	type: ActionType.createPrimaryKey;
	payload: {
		primaryKey: PrimaryKeyData;
	};
}

export interface AlterPrimaryKeyPropertyActionData extends ActionData {
	type: ActionType.alterPrimaryKeyProperty;
	payload: {
		id: string;
		propertyName: PrimaryKeyDataProperty;
		value: any;
	};
}

export interface DeletePrimaryKeyActionData extends ActionData {
	type: ActionType.deletePrimaryKey;
	payload: {
		id: string;
	};
}

export interface SetSchemaSettingsActionData extends ActionData {
	type: ActionType.setSchemaSettings;
	payload: {
		settings: SchemaSettingsData;
	};
}

export interface AlterSchemaSettingsPropertyActionData extends ActionData {
	type: ActionType.alterSchemaSettingsProperty;
	payload: {
		path: string[];
		value: any;
	};
}

export interface CreateNamespaceActionData extends ActionData {
	type: ActionType.createNamespace;
	payload: {
		namespace: NamespaceData;
	};
}

export interface AlterNamespacePropertyActionData extends ActionData {
	type: ActionType.alterNamespaceProperty;
	payload: {
		id: string;
		propertyName: NamespaceDataProperty;
		value: any;
	};
}

export interface DeleteNamespaceActionData extends ActionData {
	type: ActionType.deleteNamespace;
	payload: {
		id: string;
	};
}
