import { ActionType } from "@/data/actions/action-enums";
import { CustomExport, ExportState, SqlExportHistoryItem } from "@/data/backend";
import { ActionData } from "./action-data";

export interface SetExportStateActionData extends ActionData {
	type: ActionType.setExportState;
	payload: {
		data: ExportState;
	};
}

export interface AddExportHistoryActionData extends ActionData {
	type: ActionType.addExportHistory;
	payload: {
		data: SqlExportHistoryItem;
	};
}

export interface DeleteExportHistoryActionData extends ActionData {
	type: ActionType.deleteExportHistory;
	payload: {
		time: number;
	};
}

export interface SetCustomExportActionData extends ActionData {
	type: ActionType.setCustomExport;
	payload: {
		data: CustomExport;
	};
}

export interface DeleteCustomExportActionData extends ActionData {
	type: ActionType.deleteCustomExport;
	payload: {
		id: string;
	};
}
