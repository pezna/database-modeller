import { ActionType } from "@/data/actions/action-enums";
import { CodeError } from "@/data/backend";
import { Action } from "redux";
import { ThunkAction } from "redux-thunk";
import { GenericErrorData, ModelItemErrorData } from "../error";
import { RootState } from "../state";


export interface ActionData extends Action<ActionType> {
	errorKey?: string;
	errors?: Array<ModelItemErrorData | CodeError | GenericErrorData>;
}

export type FunctionActionData = ThunkAction<any, RootState, any, ActionData>;
