import { ActionType } from "@/data/actions/action-enums";
import { UiState } from "@/data/backend";
import { SelectableModelObjectType } from "@/data/general";
import { ActionData } from "./action-data";

export interface SetUiStateActionData extends ActionData {
	type: ActionType.setUiState;
	payload: {
		state: UiState;
	};
}

export interface AlterUiStatePropertyActionData extends ActionData {
	type: ActionType.alterUiStateProperty;
	payload: {
		path: string[];
		value: any;
	};
}

export interface DeselectBoardActionData extends ActionData {
	type: ActionType.deselectBoard;
	payload: {
		everything?: boolean;
	};
}

export interface SelectModelObjectActionData extends ActionData {
	type: ActionType.selectModelObject;
	payload: {
		modelType: SelectableModelObjectType;
		id: string | null;
		otherIds: {
			machineId?: string;
			namespaceId?: string;
		};
	};
}
