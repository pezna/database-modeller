import { OutputLanguageOptions, SqlDocumentData, StructOutputData } from '@/data/backend';
import { MODEL_FILE_FOLDER_TEMPLATE } from '@/data/general';
import { generateId, IdPrefix } from '@/utils';
import { languageTypeNameMap } from './code-enums';

export function newSqlDocumentData(): SqlDocumentData {
	return {
		id: generateId(IdPrefix.code),
		name: 'Untitled',
		folderPath: '',
		code: 'SELECT t1.id, t1.yolo, t2.one_id FROM test_one t1\nINNER JOIN test_two t2 ON t2.one_id = t1.id;',
		schemaUsages: [],
	};
}

export function newStructOutputData(language: OutputLanguageOptions['type']): StructOutputData {
	let options: OutputLanguageOptions;
	if (language === 'cpp') {
		options = newCppOutputOptions();
	}
	else if (language === 'csharp') {
		options = newCsharpOutputOptions();
	}
	else if (language === 'java') {
		options = newJavaOutputOptions();
	}
	else if (language === 'javascript') {
		options = newJavascriptOutputOptions();
	}
	else if (language === 'rust') {
		options = newRustOutputOptions();
	}
	else if (language === 'typescript') {
		options = newTypescriptOutputOptions();
	}
	else {
		options = newPythonOutputOptions();
	}
	
	return {
		id: generateId(IdPrefix.code),
		name: `Untitled ${languageTypeNameMap[language]}`,
		folderPath: MODEL_FILE_FOLDER_TEMPLATE,
		code: '{code}\n',
		includedTableIds: [],
		excludedTableIds: [],
		singleFileOutput: false,
		options,
	};
}

export const caseChoices = {
	'': '<no change>',
	'camelCase': 'camelCase',
	'pascalCase': 'PascalCase',
	'snakeCase': 'snake_case',
	'upperSnakeCase': 'UPPER_SNAKE_CASE',
	'kebabCase': 'kebab-case',
} as const;
export const caseChoicesSelectData = Object.entries(caseChoices).map(([key, value]) => ({ label: value, value: key }));

function newCppOutputOptions(): Extract<OutputLanguageOptions, {type: 'cpp'}> {
	return {
		type: 'cpp',
		classNamePrefix: '',
		classNameSuffix: '',
		classNameCase: 'pascalCase',
		variableCase: 'camelCase',
		fileNameCase: 'snakeCase',
		fileNameSuffix: '',
		fileExtension: 'hpp',
		makeStruct: false,
		addConstructor: true,
	};
}

const csharpObjectKeywords = [
	'class',
	'abstract class',
	'interface',
	'struct',
	'record',
	'record struct',
	'readonly struct',
	'readonly record struct',
	'ref struct',
	'readonly ref struct',
] as const;
export const csharpObjectKeywordsSelectData = csharpObjectKeywords.map(item => ({ label: item, value: item }));

function newCsharpOutputOptions(): Extract<OutputLanguageOptions, {type: 'csharp'}> {
	return {
		type: 'csharp',
		classNamePrefix: '',
		classNameSuffix: '',
		variablesNotProperties: false,
		propertyInitNotSet: false,
		makeFieldsVirtual: false,
		objectKeyword: 'class',
		useRecordShorthand: false,
		addConstructor: true,
	};
}

const javaObjectKeywords = [
	'record',
	'class',
	'abstract class',
] as const;
export const javaObjectKeywordsSelectData = javaObjectKeywords.map(item => ({ label: item, value: item }));

function newJavaOutputOptions(): Extract<OutputLanguageOptions, {type: 'java'}> {
	return {
		type: 'java',
		classNamePrefix: '',
		classNameSuffix: '',
		objectKeyword: 'class',
		makeStatic: false,
		addConstructor: true,
	};
}

function newJavascriptOutputOptions(): Extract<OutputLanguageOptions, {type: 'javascript'}> {
	return {
		type: 'javascript',
		classNamePrefix: '',
		classNameSuffix: '',
		variableCase: 'camelCase',
		fileNameCase: 'kebabCase',
		fileNameSuffix: '',
		addConstructor: true,
	};
}

const typescriptObjectKeywords = [
	'class',
	'interface',
	'type',
] as const;
export const typescriptObjectKeywordsSelectData = typescriptObjectKeywords.map(item => ({ label: item, value: item }));

function newTypescriptOutputOptions(): Extract<OutputLanguageOptions, {type: 'typescript'}> {
	return {
		type: 'typescript',
		classNamePrefix: '',
		classNameSuffix: '',
		variableCase: 'camelCase',
		fileNameCase: 'kebabCase',
		fileNameSuffix: '',
		objectKeyword: 'interface',
	};
}

function newRustOutputOptions(): Extract<OutputLanguageOptions, {type: 'rust'}> {
	return {
		type: 'rust',
		structNamePrefix: '',
		structNameSuffix: '',
		fileNameSuffix: '',
		addConstructor: true,
	};
}

function newPythonOutputOptions(): Extract<OutputLanguageOptions, {type: 'python'}> {
	return {
		type: 'python',
		classNamePrefix: '',
		classNameSuffix: '',
		fileNameSuffix: '',
		makeDataclass: false,
	};
}
