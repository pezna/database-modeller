import { OutputLanguageOptions } from "@/data/backend";


export const languageTypeNameMap: Record<OutputLanguageOptions['type'], string> = {
	'cpp': "C++",
	'csharp': "C#",
	'java': "Java",
	'javascript': "Javascript",
	'python': "Python",
	'rust': "Rust",
	'typescript': "Typescript",
} as const;

export const languageTypeSelectData = Object.entries(languageTypeNameMap).map(([key, value]) => ({ value: key, label: value }));
