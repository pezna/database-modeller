import { prependToEveryLine } from "@/utils";

export const TYPE_FILE_NAME_TEMPLATE = "{type}";
export const DATE_TIME_FILE_NAME_TEMPLATE = "{datetime}";
export const UNIX_TIME_FILE_NAME_TEMPLATE = "{unixtime}";
export const MODEL_FILE_FOLDER_TEMPLATE = "{model_file_folder}";
export const OUTPUT_FILE_ID_TEMPLATE = "{output_file_id}";
export const CODE_TEMPLATE = "{code}";

export interface TemplateReplaceData {
	date?: Date;
	type?: string;
	modelFileFolder?: string;
	code?: string;
	outputFileId?: string;
}

const codeTagRegex = /([ \t]{2,})?(\{code\})/;

export function convertTemplateString(text: string, data: TemplateReplaceData): string {
	if (data.type !== undefined) {
		text = text.replaceAll(TYPE_FILE_NAME_TEMPLATE, data.type);
	}
	
	if (data.date !== undefined) {
		const date = data.date;
		const year = date.getFullYear().toString();
		const month = (date.getMonth() + 1).toString().padStart(2, '0');
		const day = date.getDate().toString().padStart(2, '0');
		const hours = date.getHours().toString().padStart(2, '0');
		const minutes = date.getMinutes().toString().padStart(2, '0');
		const seconds = date.getSeconds().toString().padStart(2, '0');
		const dateTime = `${year}_${month}_${day}_${hours}_${minutes}_${seconds}`;
		
		text = text.replaceAll(DATE_TIME_FILE_NAME_TEMPLATE, dateTime);
		
		text = text.replaceAll(UNIX_TIME_FILE_NAME_TEMPLATE, Math.floor(date.getTime() / 1000).toString());
	}
	
	if (data.modelFileFolder !== undefined) {
		text = text.replaceAll(MODEL_FILE_FOLDER_TEMPLATE, data.modelFileFolder);
	}
	
	if (data.outputFileId !== undefined) {
		text = text.replaceAll(OUTPUT_FILE_ID_TEMPLATE, data.outputFileId);
	}
	
	if (data.code !== undefined) {
		const match = codeTagRegex.exec(text);
		if (!match) {
			return text;
		}
		const spaceMatch = match[1];
		let givenCode = data.code;
		if (spaceMatch) {
			givenCode = prependToEveryLine(givenCode, spaceMatch);
		}
		text = text.replaceAll((spaceMatch ?? "") + "{code}", givenCode);
	}
	
	return text;
}
