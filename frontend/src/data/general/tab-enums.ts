export enum SettingsTab {
	appSettings = "appSettings",
	modelSettings = "modelSettings",
}

export enum ManageSchemaTab {
	tables = "tables",
	columnTypes = "columnTypes",
	machines = "machines",
}

export enum ExportTab {
	exportHistory = "exportHistory",
	customExportTemplates = "customExportTemplates",
}

export enum OptionsSidebarSection {
	manage = "manage",
	errors = "errors",
	exports = "exports",
	code = "code",
	settings = "settings",
}

export enum PropertySidebarSection {
	main = "main",
	details = "details",
}
