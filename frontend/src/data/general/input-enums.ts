export enum ValueInputType {
	checkbox = "checkbox",
	color = "color",
	number = "number",
	select = "select",
	text = "text",
	javascript = "javascript",
	folder = "folder",
};
