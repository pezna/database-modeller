export enum TextFormattingOption {
    none = "none",
    bold = "bold",
    italic = "italic",
    underline = "underline",
}

export const textFormattingOptionDisplayName = {
	[TextFormattingOption.none]: "None",
	[TextFormattingOption.bold]: "Bold",
	[TextFormattingOption.italic]: "Italic",
	[TextFormattingOption.underline]: "Underline",
};

export enum RelationshipConnectLocation {
    column = "column",
    table = "table",
}

export const relationshipConnectLocationDisplayName = {
	[RelationshipConnectLocation.column]: "column",
    [RelationshipConnectLocation.table]: "table",
};
