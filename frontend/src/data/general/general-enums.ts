import { AppMode } from "@/data/backend";

export const appModeDisplayName = {
	[AppMode.database]: "Database",
	[AppMode.machine]: "Machine",
};

export enum ModelObjectType {
	table = "table",
	column = "column",
	columnType = "columnType",
	relationship = "relationship",
	index = "index",
	unique = "unique",
	primaryKey = "primaryKey",
	namespace = "namespace",
	note = "note",
	machine = "machine",
	component = "component",
	connector = "connector",
}

export type SelectableModelObjectType =
	| ModelObjectType.table
	| ModelObjectType.column
	| ModelObjectType.relationship
	| ModelObjectType.note
	| ModelObjectType.namespace
	| ModelObjectType.machine
	| ModelObjectType.component
	| ModelObjectType.connector
