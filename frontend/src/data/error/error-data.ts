import { CodeError } from "@/data/backend";
import { ModelObjectType } from "../general/general-enums";

export interface ModelItemErrorData {
	errorKey?: string;
	schemaType: ModelObjectType;
	schemaId: string;
	message: string;
}

export interface GenericErrorData {
	errorKey: string;
	message: string;
}

export type ErrorData = ModelItemErrorData | CodeError | GenericErrorData;
