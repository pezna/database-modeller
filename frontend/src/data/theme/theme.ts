import { Theme } from "@emotion/react";

export const lightTheme: Theme = {
	board: {
		backgroundColor: "#e6e6e9",
		headerBackgroundColor: "#d6d6d9",
	},
	table: {
		backgroundColor: "#fff",
		foregroundColor: "#111",
		borderColor: "#111",
	},
	header: {
		backgroundColor: "#445",
		foregroundColor: "#f0f0f0",
	},
	sidebar: {
		expandedBackgroundColor: "#30303b",
		buttonBarBackgroundColor: "#3b3b49",
		foregroundColor: "#f0f0f0",
		minWidth: "320px",
		resizeBorderSize: "5px",
		plaintextBackground: "#25252d",
	},
	accordion: {
		bodyBackgroundColor: "#30303b",
		alternateBodyBackgroundColor: "#3b3b49",
	},
	colors: {
		baseBackgroundColor: "#3b3b49",
		altBackgroundColor: "#30303b",
	},
};
