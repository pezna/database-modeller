import { AppMode, SettingsState } from "@/data/backend";
import { relationshipConnectLocationDisplayName, textFormattingOptionDisplayName, ValueInputType } from "@/data/general";
import { SettingsDetailContainer } from "./settings-metadata";

export const appSettingsDetails: SettingsDetailContainer<SettingsState> = {
	"general": {
		title: "General",
		children: {
			"files": {
				title: "Files",
				appModeEditable: [],
				children: {
					"mostRecentModelFiles": {
						title: "Most Recent Model Files",
						isEnabled: () => false,
						appModeEditable: [],
					},
				},
			},
			"startUp": {
				title: "Start Up",
				children: {
					"reopenLastModelFile": {
						title: "Re-open the last model file",
						description: "Determine whether to re-open the last used model file when the application is run.",
						inputType: ValueInputType.checkbox,
					},
				},
			},
		},
	},
	"databaseBoard": {
		title: "Board",
		appModeEditable: [AppMode.database],
		children: {
			"table": {
				title: "Tables",
				children: {
					"showKeys": {
						title: "Show primary and foreign keys",
						description: "Show primary and foreign keys in the table diagram on the board.",
						inputType: ValueInputType.checkbox,
					},
					"backgroundColor": {
						title: "Background color",
						inputType: ValueInputType.color,
					},
					"foregroundColor": {
						title: "Foreground color",
						inputType: ValueInputType.color,
					},
					"borderColor": {
						title: "Border color",
						inputType: ValueInputType.color,
					},
				},
			},
			"column": {
				title: "Columns",
				children: {
					"showType": {
						title: "Show type of column",
						inputType: ValueInputType.checkbox,
					},
					"stylePrimaryKeys": {
						title: "Style of primary keys",
						isEnabled: (state) => state.databaseBoard.table.showKeys,
						disabledHint: 'Requires "Tables >> Show primary and foreign keys"',
						inputType: ValueInputType.select,
						allowedValues: textFormattingOptionDisplayName,
					},
					"styleForeignKeys": {
						title: "Style of foreign keys",
						isEnabled: (state) => state.databaseBoard.table.showKeys,
						disabledHint: 'Requires "Tables >> Show primary and foreign keys"',
						inputType: ValueInputType.select,
						allowedValues: textFormattingOptionDisplayName,
					},
					"indicatePrimaryKey": {
						title: "Indicate if a column is a primary key",
						isEnabled: (state) => state.databaseBoard.table.showKeys,
						disabledHint: 'Requires "Tables >> Show primary and foreign keys"',
						inputType: ValueInputType.checkbox,
					},
					"indicateForeignKey": {
						title: "Indicate if a column is a foreign key",
						isEnabled: (state) => state.databaseBoard.table.showKeys,
						disabledHint: 'Requires "Tables >> Show primary and foreign keys"',
						inputType: ValueInputType.checkbox,
					},
					"indicateIndex": {
						title: "Indicate if a column is indexed",
						inputType: ValueInputType.checkbox,
					},
					"indicateNotNull": {
						title: "Indicate if a column is not null",
						inputType: ValueInputType.checkbox,
					},
					"indicateUnique": {
						title: "Indicate if a column is unique",
						inputType: ValueInputType.checkbox,
					},
				},
			},
			"mixins": {
				title: "Mixins",
				children: {
					"mixinBackgroundColor": {
						title: "Mixin table background color",
						inputType: ValueInputType.color,
					},
					"applyColorToColumns": {
						title: "Apply color to cloned columns",
						description: "Apply the mixin background color to the columns on tables using the mixins.",
						inputType: ValueInputType.checkbox,
					},
				},
			},
			"relationships": {
				title: "Relationships",
				children: {
					"showLines": {
						title: "Show lines",
						description: "Draw relationship lines connecting tables.",
						inputType: ValueInputType.checkbox,
					},
					"lineColor": {
						title: "Line color",
						description: "The color of a relationship line.",
						isEnabled: (state) => state.databaseBoard.relationships.showLines,
						disabledHint: 'Requires "Relationships >> Show lines"',
						inputType: ValueInputType.color,
					},
					"notExportedLineColor": {
						title: "Non-exported line color",
						description: "The color of a relationship line that is excluded from SQL export.",
						isEnabled: (state) => state.databaseBoard.relationships.showLines,
						disabledHint: 'Requires "Relationships >> Show lines"',
						inputType: ValueInputType.color,
					},
					"lineWidth": {
						title: "Line width",
						description: "The width of a relationship connector line.",
						isEnabled: (state) => state.databaseBoard.relationships.showLines,
						disabledHint: 'Requires "Relationships >> Show lines"',
						inputType: ValueInputType.number,
					},
					"lineConnectLocation": {
						title: "Line Connect Location",
						description: "The location on a table that the relationship line should point towards. Only relevant if primary and foreign keys are visible.",
						isEnabled: (state) => state.databaseBoard.relationships.showLines && state.databaseBoard.table.showKeys,
						disabledHint: 'Requires "Relationships >> Show lines" and "Tables >> Show primary and foreign keys"',
						inputType: ValueInputType.select,
						allowedValues: relationshipConnectLocationDisplayName,
					},
					"showOtherNamespaceTableName": {
						title: "Show the table name when connected to another namespace",
						description: "When a relationship is pointing to a table in another namespace, this chnages whether or not to show that table's name.",
						isEnabled: (state) => state.databaseBoard.relationships.showLines && state.databaseBoard.table.showKeys,
						disabledHint: 'Requires "Relationships >> Show lines" and "Tables >> Show primary and foreign keys"',
						inputType: ValueInputType.checkbox,
					},
				},
			},
		},
	},
	machineBoard: {
		title: "Board",
		appModeEditable: [AppMode.machine],
		children: {
			"components": {
				title: "Components",
				children: {
					"backgroundColor": {
						title: "Background Color",
						inputType: ValueInputType.color,
					},
					"foregroundColor": {
						title: "Foreground Color",
						inputType: ValueInputType.color,
					},
				},
			},
			"connectors": {
				title: "Connectors",
				children: {
					"lineColor": {
						title: "Line Color",
						inputType: ValueInputType.color,
					},
					"lineWidth": {
						title: "Line width",
						description: "The width of a connector line.",
						inputType: ValueInputType.number,
					},
				},
			},
		},
	},
};
