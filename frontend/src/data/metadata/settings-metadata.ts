import { AppMode } from "@/data/backend";
import { ValueInputType } from "@/data/general";

export interface SettingDetail<T, O> {
	title: string | ((state: T) => string);
	description?: string;
	isEnabled?: (state: T) => boolean;
	isVisible?: (state: T) => boolean;
	disabledHint?: string;
	/**
	 * The app modes that this setting detail is editable from within.
	 * All children of this setting detail receive their parent's editability, unless otherwise specified.
	 * If undefined, it is editable everywhere.
	 * If an empty array, it is editable nowhere.
	 */
	appModeEditable?: AppMode[];
	children?: {[K in keyof O]: SettingDetail<T, O[K]>};
	/**
	 * If undefined, it must be a group setting detail or be manually rendered separately
	 */
	inputType?: ValueInputType | string;
	/**
	 * Key: value
	 * Value: label to be displayed
	 */
	allowedValues?: Record<string, string>;
}

export type SettingsDetailContainer<T extends object> = {[K in keyof T]: SettingDetail<T, T[K]>};
