import { AppMode, SchemaSettingsData } from "@/data/backend";
import { sqlExportDisplayName } from "@/data/export";
import { DATE_TIME_FILE_NAME_TEMPLATE, TYPE_FILE_NAME_TEMPLATE, UNIX_TIME_FILE_NAME_TEMPLATE, ValueInputType } from "@/data/general";
import { DIAGRAM_FILE_EXTENSION, EXPORT_FILE_EXTENSION, SETTINGS_FILE_EXTENSION } from "../../utils";
import { SettingsDetailContainer } from "./settings-metadata";

export const schemaSettingsDetails: SettingsDetailContainer<SchemaSettingsData> = {
	"files": {
		title: "Files",
		children: {
			"saveFiles": {
				title: "Save Files",
				children: {
					"useSeparateSaveFiles": {
						title: "Use Separate Save Files",
						description: `
						<p>
							When saving, create separate files that:
							<ol>
								<li>keep track of the export data and history (with the file extension ".${EXPORT_FILE_EXTENSION}")</li>
								<li>contain the positions and sizes of tables and notes on the board (with the file extension ".${DIAGRAM_FILE_EXTENSION}")</li>
								<li>contain the model-specific settings (with the file extension ".${SETTINGS_FILE_EXTENSION}")</li>
							</ol>
						</p>`,
						inputType: ValueInputType.checkbox,
					},
				},
			},
		},
	},
	"schema": {
		title: "Schema",
		appModeEditable: [AppMode.database],
		children: {
			"mixinTables": {
				title: "Mixin Tables",
				children: {
					"defaultMixinTableIds": {
						title: "Default Mixin Tables",
						description: "The default mixin for every new table",
					},
				},
			},
			"generatorCode": {
				title: "Generator Code",
				children: {
					"primaryKeyNameGeneratorCode": {
						title: "Primary Key Name Generator Code",
						description: getCodeDescription("primaryKeyNameGeneratorCode"),
						inputType: ValueInputType.javascript,
					},
					"relationshipNameGeneratorCode": {
						title: "Relationship Name Generator Code",
						description: getCodeDescription("relationshipNameGeneratorCode"),
						inputType: ValueInputType.javascript,
					},
					"indexNameGeneratorCode": {
						title: "Index Name Generator Code",
						description: getCodeDescription("indexNameGeneratorCode"),
						inputType: ValueInputType.javascript,
					},
					"uniqueNameGeneratorCode": {
						title: "Unique Name Generator Code",
						description: getCodeDescription("uniqueNameGeneratorCode"),
						inputType: ValueInputType.javascript,
					},
				},
			},
		},
	},
	export: {
		title: "Export",
		appModeEditable: [AppMode.database],
		children: {
			"sql": {
				title: "SQL Migration",
				children: {
					"type": {
						title: "Database Type",
						inputType: ValueInputType.select,
						allowedValues: sqlExportDisplayName,
					},
					"fileNameTemplate": {
						title: "File name template",
						description: `
						<p>
							The file name that will contain the sql code.<br />
							${TYPE_FILE_NAME_TEMPLATE} &mdash; the database type chosen above (ex: sqlite)<br />
							${DATE_TIME_FILE_NAME_TEMPLATE} &mdash; numeric date and time (ex: 2024_01_30_08_55_10)<br />
							${UNIX_TIME_FILE_NAME_TEMPLATE} &mdash; unix epoch timestamp (ex: 1674690098)<br />
						</p>`,
						inputType: ValueInputType.text,
					},
					"separateDirectionFolders": {
						title: "Separate Forward and Backward Migration Folders",
						inputType: ValueInputType.checkbox,
					},
					"forwardFolderPath": {
						title: (state) => state.export.sql.separateDirectionFolders
							? "Forward Migration Output Folder"
							: "Output Folder",
						inputType: ValueInputType.folder,
					},
					"backwardFolderPath": {
						title: "Backward Migration Output Folder",
						isVisible: (state) => state.export.sql.separateDirectionFolders,
						inputType: ValueInputType.folder,
					}
				},
			},
		},
	},
};

function getCodeDescription(codeKey: keyof SchemaSettingsData["schema"]["generatorCode"]) {
	let specific: string = '';
	if (codeKey === "indexNameGeneratorCode" || codeKey === "primaryKeyNameGeneratorCode" || codeKey === "uniqueNameGeneratorCode") {
		specific = `
			<p>
				<b>table</b> &mdash; the javascript object that represents a table
			</p>
			<p>
				<b>columns</b> &mdash; the javascript array that contains column objects
			</p>
		`;
	}
	else if (codeKey === "relationshipNameGeneratorCode") {
		specific = `
			<p>
				<b>childTable</b> &mdash; the javascript object for the table that contains the foreign key
			</p>
			<p>
				<b>parentTable</b> &mdash; the javascript object for the table that is being referenced
			</p>
			<p>
				<b>childColumns</b> &mdash; the javascript array that contains column objects for the child table
			</p>
			<p>
				<b>parentColumns</b> &mdash; the javascript array that contains column objects for the parent table
			</p>
		`;
	}
	
	return `
	<p>
		<span className="subtitle">FUNCTIONS TO FILL</span>
		<p>
			<b>generate()</b> &mdash; the function that will return the name for the SQL item
		</p>
		<span className="subtitle">AVAILABLE VARIABLES</span>
		${specific}
	</p>`;
}
