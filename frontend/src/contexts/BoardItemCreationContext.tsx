import { BoardTool } from "@/data";
import { PropsWithChildren, createContext, useContext, useState } from "react";

export interface SelectedBoardItem {
	id: string;
	isComponent: boolean;
	isColumn: boolean;
	tableId?: string;
};

type MousePos = { x: number; y: number }

interface BoardItemCreationContextData {
	tool: BoardTool | null;
	setTool: (tool: BoardTool | null) => void;
	firstSelectedItem: SelectedBoardItem | null;
	setFirstSelectedItem: (steps: SelectedBoardItem) => void;
	reset: () => void;
	mouseLocation: MousePos;
	setMouseLocation: (pos: MousePos) => void;
}

export const BoardItemCreationContext = createContext<BoardItemCreationContextData>({} as BoardItemCreationContextData);

interface BoardItemCreationProviderProps {}

export function BoardItemCreation({ children }: PropsWithChildren<BoardItemCreationProviderProps>) {
	const [tool, setTool] = useState<BoardTool | null>(null);
	const [firstSelectedItem, setFirstSelectedItem] = useState<SelectedBoardItem | null>(null);
	const [mouseLocation, setMouseLocation] = useState<MousePos>({ x: 0, y: 0 });
	
	const reset = () => {
		setTool(null);
		setFirstSelectedItem(null);
	};
	
	const setToolCallback = (t: BoardTool | null) => {
		reset();
		setTool(() => t);
	};
	
	const value: BoardItemCreationContextData = {
		tool,
		setTool: setToolCallback,
		firstSelectedItem,
		setFirstSelectedItem,
		reset,
		mouseLocation,
		setMouseLocation,
	};
	
	return (
		<BoardItemCreationContext.Provider value={value}>
			{children}
		</BoardItemCreationContext.Provider>
	);
}

export const useBoardItemCreation = () => useContext(BoardItemCreationContext);
