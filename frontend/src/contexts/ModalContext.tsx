import { PropsWithChildren, createContext, useContext, useState } from "react";

interface ModalContextData {
	showSqlExport: boolean;
	showMachineSelection: boolean;
	showNewProject: boolean;
	setShowSqlExport: (open: boolean) => void;
	setShowMachineSelection: (open: boolean) => void;
	setShowNewProject: (open: boolean) => void;
}

const ModalContext = createContext<ModalContextData>({} as ModalContextData);

interface ModalProviderProps {}

export function ModalProvider({ children }: PropsWithChildren<ModalProviderProps>) {
	const [showSqlExport, setShowSqlExport] = useState(false);
	const [showMachineSelection, setShowMachineSelection] = useState(false);
	const [showNewProject, setShowNewProject] = useState(false);
	
	const value: ModalContextData = {
		showSqlExport,
		setShowSqlExport,
		showMachineSelection,
		setShowMachineSelection,
		showNewProject,
		setShowNewProject,
	};
	
	return (
		<ModalContext.Provider value={value}>
			{children}
		</ModalContext.Provider>
	);
}

export const useModalContext = () => useContext(ModalContext);
