import { ModelObjectType } from "@/data";
import { PropsWithChildren, createContext, useContext, useState } from "react";

export type RightClickMousePos = { x: number; y: number; boardX: number; boardY: number; };

interface RightClickContextData {
	itemType?: ModelObjectType;
	itemId?: string;
	isMenuActive: boolean;
	mousePos: RightClickMousePos;
	showMenu: (pos: RightClickMousePos, itemType?: ModelObjectType, id?: string) => void;
	hideMenu: () => void;
}

const RightClickContext = createContext<RightClickContextData>({} as RightClickContextData);

interface RightClickProviderProps {}

export function RightClickProvider({ children }: PropsWithChildren<RightClickProviderProps>) {
	const [itemType, setItemType] = useState<ModelObjectType>();
	const [itemId, setItemId] = useState<string>();
	const [mousePos, setMousePos] = useState<RightClickMousePos>({ x: 0, y: 0, boardX: 0, boardY: 0 });
	const [isMenuActive, setIsMenuActive] = useState(false);
	
	const showMenu = (pos: RightClickMousePos, itemType?: ModelObjectType, id?: string) => {
		setItemType(itemType);
		setItemId(id);
		setMousePos(pos);
		setIsMenuActive(true);
	};
	
	const hideMenu = () => {
		setItemType(undefined);
		setItemId(undefined);
		setMousePos({ x: 0, y: 0, boardX: 0, boardY: 0 });
		setIsMenuActive(false);
	};
	
	const value: RightClickContextData = {
		itemType,
		itemId,
		isMenuActive,
		mousePos,
		showMenu,
		hideMenu,
	};
	
	return (
		<RightClickContext.Provider value={value}>
			{children}
		</RightClickContext.Provider>
	);
}

export const useRightClickContext = () => useContext(RightClickContext);
