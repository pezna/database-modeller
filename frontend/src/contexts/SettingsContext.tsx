import { InputComponentProps } from "@/components/widgets/types";
import { SchemaSettingsData, SettingsSidebarUiState } from "@/data";
import { PropsWithChildren, ReactNode, createContext, useContext } from "react";

export interface SettingsContextData {
	state: object;
	getExpandedGroupKey: (key: keyof SchemaSettingsData) => keyof SettingsSidebarUiState;
	setValue: (path: string[], value: any) => void;
	setExpandedGroup: (key: string, isExpanded: boolean) => void;
	getInputComponent: (path: string[]) => (props: InputComponentProps) => ReactNode | null;
}

export const SettingsContext = createContext<SettingsContextData>({
	state: {},
	getExpandedGroupKey: () => "activeTab",
	setValue: () => {},
	setExpandedGroup: () => {},
	getInputComponent: () => () => null,
});

interface SettingsProviderProps {
	contextData: SettingsContextData;
}

export function SettingsProvider({ contextData, children }: PropsWithChildren<SettingsProviderProps>) {
	return (
		<SettingsContext.Provider value={contextData}>
			{children}
		</SettingsContext.Provider>
	);
}

export const useSettingsContext = () => useContext(SettingsContext);
